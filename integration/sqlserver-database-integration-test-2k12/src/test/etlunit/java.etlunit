class super_test_class {
	@Database(id: 'db1', modes: ['tgt', 'src'])
	@Test
	loadDb1(){
		stage(
			connection-id: 'db1',
			mode: 'tgt',
			source: 'DATA',
			target-table: 'TEST'
		);

		stage(
			connection-id: 'db1',
			mode: 'tgt',
			source: 'DATA',
			target-table: 'TEST_ID'
		);

		stage(
			connection-id: 'db1',
			mode: 'src',
			source: 'DATA',
			target-table: 'TEST_ID'
		);

		stage(
			connection-id: 'db1',
			mode: 'src',
			source: 'DATA',
			target-table: 'TEST_ID_2'
		);

		stage(
			connection-id: 'db1',
			mode: 'tgt',
			source: 'DATA',
			target-table: 'TEST_ID_2'
		);

		assert(
			source-table: 'TEST2',
			connection-id: 'db1',
			mode: 'tgt',
			assertion-mode: 'empty'
		);

		assert(
			source-table: 'TEST',
			connection-id: 'db1',
			mode: 'tgt',
			assertion-mode: 'notEmpty'
		);

		assert(
			source-table: 'TEST_ID',
			connection-id: 'db1',
			mode: 'tgt',
			assertion-mode: 'notEmpty'
		);

		assert(
			source-table: 'TEST',
			target: 'DATA',
			connection-id: 'db1',
			mode: 'tgt',
			assertion-mode: 'equals'
		);
	}

	@Database(id: 'db1', modes: ['src', 'tgt'])
	@Test
	loadDb2(){
		stage(
			mode: 'src',
			source: 'DATA',
			target-table: 'TEST'
		);

		assert(
			source-table: 'TEST2',
			connection-id: 'db1',
			mode: 'src',
			assertion-mode: 'empty'
		);

		assert(
			source-table: 'TEST2',
			connection-id: 'db1',
			mode: 'tgt',
			assertion-mode: 'empty'
		);

		assert(
			source-table: 'TEST',
			connection-id: 'db1',
			mode: 'src',
			assertion-mode: 'notEmpty'
		);

		assert(
			source-table: 'TEST',
			connection-id: 'db1',
			mode: 'tgt',
			assertion-mode: 'empty'
		);

		assert(
			source-table: 'TEST',
			target: 'DATA',
			connection-id: 'db1',
			mode: 'src',
			assertion-mode: 'equals'
		);
	}

	@Database(id: 'db1', modes: ['tgt'])
	@Test
	extractDb1(){
		extract(
			connection-id: 'db1',
			mode: 'tgt',
			source-table: 'TEST',
			target: 'TEST'
		);
	}

	@Database(id: 'db1', modes: ['src'])
	@Test
	extractDb2(){
		extract(
			mode: 'src',
			source-table: 'TEST',
			target: 'TEST'
		);
	}

	@Database(id: 'db1', modes: ['tgt'])
	@Test
	extractDb3(){
		extract(
			connection-id: 'db1',
			mode: 'tgt',
			source-table: 'TEST',
			target-file: 'otheroutput.csv'
		);
	}

	@Database(id: 'db1', modes: ['src'])
	@Test
	extractDb4(){
		extract(
			mode: 'src',
			source-table: 'TEST',
			target: 'TEST_MASTER'
		);
	}

	@Database(id: 'db1')
	@Test
	loadTableWithSql()
	{
		stage(
			source: 'DATA',
			target-table: 'TEST'
		);

		assert(
			source-table: 'TEST',
			target: 'DATA',
			assertion-mode: 'equals'
		);

		execute(
			sql-script: 'script'
		);

		assert(
			source-table: 'TEST',
			target: 'DATA_POST_UPDATE',
			assertion-mode: 'equals'
		);

		execute(
			sql: 'DELETE FROM TEST'
		);

		assert(
			source-table: 'TEST',
			assertion-mode: 'empty'
		);
	}

	@Database(id: 'db1', modes: ['tgt'])
	@Test
	extractWithColumns(){
		stage(
			connection-id: 'db1',
			mode: 'tgt',
			source: 'DATA_2',
			target-table: 'TEST_COLS'
		);

		extract(
			connection-id: 'db1',
			mode: 'tgt',
			column-list: ['COL1', 'COL3'],
			column-list-mode: 'include',
			source-table: 'TEST_COLS',
			target: 'DATA_COL1_COL3_INCL'
		);

		extract(
			connection-id: 'db1',
			mode: 'tgt',
			column-list: ['COL1', 'COL3'],
			column-list-mode: 'exclude',
			source-table: 'TEST_COLS',
			target: 'DATA_COL1_COL3_EXCL'
		);

		extract(
			connection-id: 'db1',
			mode: 'tgt',
			source-table: 'TEST_COLS',
			target: 'DATA_ALL_COLS'
		);

		assert(
			source-table: 'TEST_COLS',
			target: 'DATA_2'
		);
	}
}

@Database(id: 'db1', modes: ['tgt', 'src'])
class test_view_truncate
{
	@Test
	viewEmpty()
	{
		assert(
			source-table: 'VW_TEST',
			assertion-mode: 'empty'
		);
	}

	@Test
	viewLoaded()
	{
		stage(
			source: 'DATA',
			target-table: 'VW_TEST'
		);

		assert(
			source-table: 'VW_TEST',
			assertion-mode: 'notEmpty'
		);
	}

	@Test
	viewEmptyAgain()
	{
		assert(
			source-table: 'VW_TEST',
			assertion-mode: 'empty'
		);
	}

	@Test
	viewIsTable()
	{
		stage(
			source: 'DATA',
			target-table: 'VW_TEST'
		);

		assert(assertion-mode: 'empty', source-table: 'TEST');
		assert(assertion-mode: 'empty', source-table: 'VW_TEST', source-schema: 'etlunit_dbo');
	}

	@Test
	realViewIsRenamed()
	{
		stage(
			source: 'DATA',
			target-table: 'TEST'
		);

		assert(assertion-mode: 'empty', source-table: 'VW_TEST');
		assert(source-table: 'VW_TEST', source-schema: 'etlunit_dbo', target: 'DATA');
	}

	@Test
	storedProcedure()
	{
		// load data into the test table for updating
		stage(
			source: 'DATA',
			target-table: 'TEST'
		);

		// execute the stored procedure which will update the table with the default parameter
		execute(
			sql-procedure: 'SP_TEST'
		);

		// verify that the table was updated properly
		assert(
			source-table: 'TEST',
			target: 'DATA_SP'
		);

		// execute the stored procedure which will update the table with a parameter override
		execute(
			sql-procedure: 'SP_TEST',
			parameters: ['2000']
		);

		// verify that the table was updated properly
		assert(
			source-table: 'TEST',
			target: 'DATA_SP_2'
		);
	}
}

@Database(id: 'db1')
class computedColumns
{
	@Test computed()
	{
		stage(
			source: 'COMPUTED_STAGE',
			reference-file-type: 'COMPUTED_STAGE',
			target-table: 'T_COMPUTED'
		);

		assert(source-table: 'T_COMPUTED', target: 'COMPUTED_ASSRT');
	}
}