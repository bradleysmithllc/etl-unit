CREATE TABLE TEST2
(
	ID_2 INT
);;


-- fail if we are not running 2k12
DECLARE @ver nvarchar(128)
SET @ver = CAST(serverproperty('ProductVersion') AS nvarchar)
SET @ver = SUBSTRING(@ver, 1, CHARINDEX('.', @ver) - 1)

IF ( @ver != '11' )
	raiserror('Incorrect SQL version.  This requires 2K12', 11, 1, N'');
;;
