package org.bitbucket.bradleysmithllc.etlunit.test.java_executor.integration;

import net.sourceforge.jtds.jdbc.Driver;
import org.bitbucket.bradleysmithllc.java_cl_parser.*;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@CLIEntry(
		description = "Tests connecting to a database",
		version = "1.0"
)
public class JdbcApplication {
	private String jdbcUrl;
	private String jdbcUser;
	private String jdbcPassword;

	@CLIOption(
			name = "jurl",
			longName = "jdbc-url",
			required = true
	)
	public void setJdbcUrl(String url) {
		jdbcUrl = url;
	}

	@CLIOption(
			name = "juser",
			longName = "jdbc-user",
			required = true
	)
	public void setJdbcUser(String user) {
		jdbcUser = user;
	}

	@CLIOption(
			name = "jpass",
			longName = "jdbc-password",
			required = true
	)
	public void setJdbcPassword(String password) {
		jdbcPassword = password;
	}

	@CLIMain
	public void main() throws SQLException {
		Class cl = Driver.class;

		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);

		try {
			PreparedStatement pst = conn.prepareStatement("INSERT INTO TEST(ID) VALUES (?)");

			try {
				for (int i = 0; i < 10; i++) {
					pst.setInt(1, i);
					pst.addBatch();
				}

				pst.executeBatch();
			} finally {
				pst.close();
			}
		} finally {
			conn.close();
		}

		System.out.println("Good to go . . .");
	}

	public static void main(String[] argv) throws MissingCLIEntryException, InvalidCLIEntryException, UsageException, InvocationTargetException {
		CommonsCLILauncher.main(argv);
	}
}