package org.bitbucket.bradleysmithllc.spark;

/*
 * #%L
 * etlunit-hadoop-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.JobConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkJob;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkProxy;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainSparkProxy implements SparkProxy {
	@Override
	public SparkJob sparkJob(final String id) {
		return new SparkJob(){
			@Override
			public String id() {
				return id;
			}

			@Override
			public List<String> requiredFilesystems() {
				return Collections.EMPTY_LIST;
			}

			@Override
			public void execute(JobConfiguration jc) {
			}
		};
	}

	public static void main(String [] argv)
	{
		System.out.println(MainSparkProxy.class.getName());
	}
}
