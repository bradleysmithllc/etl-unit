@echo off

IF EXIST target\etlunit-lib (
	del /Q target\etlunit-lib\* > NUL
)

IF EXIST lib (
	del /Q lib\* > NUL
) ELSE (
	mkdir lib > NUL
)

echo Updating meta data . . .
call mvn package -Dmaven.test.skip -U > NUL

echo Caching local copy . . .
copy /Y target\etlunit-lib\* lib > NUL

copy pom.xml target\pom_bak.xml > NUL