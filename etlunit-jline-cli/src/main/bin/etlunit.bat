@echo off

pushd ..

IF NOT EXIST target (
	MKDIR target
)

IF EXIST lib (
	java -Djava.ext.dirs=lib org.bitbucket.bradleysmithllc.etlunit.jline_cli.util.ProjectUpdateCheck
	IF NOT ERRORLEVEL 1 GOTO :NOUPDATE
)

:UPDATE
REM CREATE A COPY OF THE UPDATE SCRIPT IN THE TARGET FOLDER TO PROTECT AGAINST OVERWRITE FROM THE MVN UPDATE
copy bin\prvupdate.bat target\prvupdate.bat > NUL

call target\prvupdate.bat

:NOUPDATE
java -classpath target\classes;target\test-classes -Djava.ext.dirs=lib org.bitbucket.bradleysmithllc.etlunit.jline_cli.EtlUnitJLineCLI

popd