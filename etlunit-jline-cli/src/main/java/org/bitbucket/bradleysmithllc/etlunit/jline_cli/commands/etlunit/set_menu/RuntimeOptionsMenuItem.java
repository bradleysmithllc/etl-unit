package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.set_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOptionDescriptor;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenuItem;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.SetCommand;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

public class RuntimeOptionsMenuItem extends AbstractMenuItem {
	private final Feature feature;
	private final RuntimeOption runtimeOption;
	private final RuntimeOptionDescriptor runtimeOptionDescriptor;

	private final String defaultValueString;
	private final String overridenValueString;

	public RuntimeOptionsMenuItem(Feature feature, RuntimeOption ro) {
		this.feature = feature;
		runtimeOption = ro;

		runtimeOptionDescriptor = runtimeOption.getDescriptor();
		RuntimeOption overridenValue = SetCommand.statelessOptions.get(runtimeOption.getName());
		String ovr = null;

		switch (runtimeOptionDescriptor.getOptionType())
		{
			case bool:
				defaultValueString = String.valueOf(runtimeOption.isEnabled());

				if (overridenValue != null)
				{
					ovr = String.valueOf(overridenValue.isEnabled());
				}

				break;
			case integer:
				defaultValueString = String.valueOf(runtimeOption.getIntegerValue());

				if (overridenValue != null)
				{
					ovr = String.valueOf(overridenValue.getIntegerValue());
				}

				break;
			case string:
				defaultValueString = runtimeOption.getStringValue();

				if (overridenValue != null)
				{
					ovr = String.valueOf(overridenValue.getStringValue());
				}

				break;
			default:
				defaultValueString = null;
		}

		overridenValueString = ovr;
	}

	@Override
	public String name() {
		return runtimeOptionDescriptor.getName();
	}

	@Override
	public String contextInfo() {
		if (defaultValueString != null) {
			String built = new AttributedStringBuilder()
					.append("[", AttributedStyle.DEFAULT)
					.append(defaultValueString, AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN))
					.append("]", AttributedStyle.DEFAULT).toAttributedString().toAnsi();

			if (overridenValueString != null) {
				built += new AttributedStringBuilder()
						.append(" = ", AttributedStyle.DEFAULT)
						.append(overridenValueString, AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW))
						.toAttributedString().toAnsi();
			}

			return built;
		}

		return null;
	}

	public Feature feature() {
		return feature;
	}

	public RuntimeOption runtimeOption() {
		return runtimeOption;
	}

	@Override
	public String description() {
		return runtimeOptionDescriptor.getDescription();
	}
}
