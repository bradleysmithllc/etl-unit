package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ETLTestVM;
import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.cli.regexp.MavenSuccessExpression;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.util.DesktopAPI;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@CLIEntry(
	description = "Updates external binaries",
	version = "1.0",
	nickName = "update",
	commandGroup = "etlunit"
)
public class UpdateCommand extends ConsoleCommand {
	@CLIMain
	public void open() throws IOException, InterruptedException {
		File stdout = File.createTempFile("stdout", "log");
		File stderr = File.createTempFile("stdout", "log");
		ProcessBuilder proc = new ProcessBuilder(
				"mvn" + (DesktopAPI.getOs() == DesktopAPI.EnumOS.windows ? ".bat" : ""), "clean", "package", "-Dmaven.test.skip=true").redirectError(stderr).redirectOutput(stdout);

		Process i = proc.start();
		i.onExit().thenAccept((p) -> {
			if (p.exitValue() == 0) {
				println("Updated.  Exiting to allow for restarting the shell");
				System.exit(0);
			} else {
				println("Failure");
				System.exit(p.exitValue());
			}
		});

		if (!i.waitFor(15, TimeUnit.SECONDS)) {
			println("Failed.  Something isn't right.");
			System.exit(-1);
		} else {
		}
	}
}
