package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.system;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;

import java.io.File;

@CLIEntry(
	description = "Test menu system",
	version = "1.0",
	nickName = "menu-test"
)
public class MenuTestCommand extends ConsoleCommand {
	@CLIMain
	public void menu() throws Exception {
		menu(new FilesystemMenu(new File("/Users/bsmith")), (fileMenuItem) -> {
			printer().print("You chose ").println(fileMenuItem.name());
		});
	}
}
