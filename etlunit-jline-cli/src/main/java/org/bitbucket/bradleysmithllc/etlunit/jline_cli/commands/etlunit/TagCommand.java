package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.tags.TagsFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.io.IOException;

@CLIEntry(
	description = "Sets the name of the tag to be used for the history for the next run only",
	nickName = "tag",
	commandGroup = "etlunit",
	version = "1.0"
)
public class TagCommand extends ConsoleCommand {
	private String tagName = null;

	@CLIOption(
		description = "Name of the tag",
		name = "tn",
		longName = "tagName",
		unnamedArgumentOrdinal = 1,
		required = true
	)
	public void tagName(String name) {
		tagName = name;
	}

	@CLIMain
	public void setTag() throws IOException {
		if (tagName.equals(""))
		{
			println("Specify a tag name.");
			System.clearProperty(TagsFeatureModule.ETLUNIT_TAG_NAME);
		} else {
			System.setProperty(TagsFeatureModule.ETLUNIT_TAG_NAME, tagName);
			println("Next test execution will be tagged as '" + System.getProperty(TagsFeatureModule.ETLUNIT_TAG_NAME) + "'");
		}
	}
}
