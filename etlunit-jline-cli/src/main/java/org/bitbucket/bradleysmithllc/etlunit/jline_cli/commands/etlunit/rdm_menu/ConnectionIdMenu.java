package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DDLSourceRef;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.SQLAggregator;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.TerminalInterface;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ConnectionIdMenu extends AbstractMenu<DatabaseMenuItem> {
	private final DatabaseFeatureModule databaseFeatureModule;
	private final TerminalInterface terminalInterface;

	private final boolean selectSchema;

	private final List<DatabaseMenuItem> menuItems = new ArrayList<>();

	public ConnectionIdMenu(
			DatabaseFeatureModule databaseFeatureModule,
			TerminalInterface terminalInterface,
			String runIdentifier,
			boolean selectSchema
	) {
		this.selectSchema = selectSchema;
		this.databaseFeatureModule = databaseFeatureModule;
		this.terminalInterface = terminalInterface;

		Map<String, DatabaseConnection> connectionMap = databaseFeatureModule.getDatabaseConfiguration().getConnectionMap();

		ArrayList<String> stringArrayList = new ArrayList<>(connectionMap.keySet());
		Collections.sort(stringArrayList);

		for (String key : stringArrayList) {
			menuItems.add(new ConnectionIdMenuItem(databaseFeatureModule, connectionMap.get(key), terminalInterface, runIdentifier));
		}
	}

	@Override
	public boolean includeCancel() {
		return true;
	}

	@Override
	public boolean includeReset() {
		return false;
	}

	@Override
	public String title() {
		return "Registered database ids";
	}

	@Override
	public String prompt() {
		return "Select connection id";
	}

	@Override
	public List<DatabaseMenuItem> items() {
		return menuItems;
	}

	@Override
	public Menu<DatabaseMenuItem> choose(DatabaseMenuItem item) throws Exception {
		if (selectSchema) {
			return item.menu();
		}

		return null;
	}

	public void disposeAll() throws SQLException {
		for (DatabaseMenuItem menuItem : menuItems) {
			menuItem.disposeAll();
		}
	}
}
