package org.bitbucket.bradleysmithllc.etlunit.jline_cli.parser.ansi;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InEscapeCodeAPState implements APState {
	private TextAPState textAPState;

	private List<Integer> argumentsList = new ArrayList<>();

	private StringBuilder numberBuilder = new StringBuilder();
	@Override
	public APState consume(char ch, StringBuffer building) throws ParseException {
		switch(ch) {
			case AnsiEscapeParser.ANSI_ESCAPE:
				APStateMachine.parseException("Escape character in an escape code");
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				numberBuilder.append(ch);
				break;
			case ';':
				argumentsList.add(Integer.parseInt(numberBuilder.toString()));
				numberBuilder.setLength(0);
				break;
			case 'm':
				if (numberBuilder.length() > 0) {
					argumentsList.add(Integer.parseInt(numberBuilder.toString()));
				}

				// graphics mode.  Ignore if no modes specified.
				building.append("{mode");

				if (argumentsList.size() > 0) {
					building.append(" ");

					// sort
					Collections.sort(argumentsList);

					boolean first = true;

					for (int mode : argumentsList) {
						if (first) {
							first = false;
						} else {
							building.append(';');
						}

						switch (mode) {
							case 0:
								building.append("off");
								break;
							case 1:
								building.append("bold");
								break;
							case 4:
								building.append("underscore");
								break;
							case 5:
								building.append("blink");
								break;
							case 7:
								building.append("reverse");
								break;
							case 8:
								building.append("concealed");
								break;
							case 30:
								building.append("fg_black");
								break;
							case 31:
								building.append("fg_red");
								break;
							case 32:
								building.append("fg_green");
								break;
							case 33:
								building.append("fg_yellow");
								break;
							case 34:
								building.append("fg_blue");
								break;
							case 35:
								building.append("fg_magenta");
								break;
							case 36:
								building.append("fg_cyan");
								break;
							case 37:
								building.append("fg_white");
								break;
							case 40:
								building.append("bg_black");
								break;
							case 41:
								building.append("bg_red");
								break;
							case 42:
								building.append("bg_green");
								break;
							case 43:
								building.append("bg_yellow");
								break;
							case 44:
								building.append("bg_blue");
								break;
							case 45:
								building.append("bg_magenta");
								break;
							case 46:
								building.append("bg_cyan");
								break;
							case 47:
								building.append("bg_white");
								break;
							default:
								APStateMachine.parseException("Unknown mode " + mode);
						}
					}
				}

				building.append("}");
				return textAPState;
		}

		return null;
	}

	@Override
	public void enter() {
		argumentsList.clear();
		numberBuilder.setLength(0);
	}

	@Override
	public void eof(StringBuffer building) throws ParseException {
		APStateMachine.parseException("Dangling escape code");
	}

	public void link(TextAPState textAPState) {
		this.textAPState = textAPState;
	}

	@Override
	public String toString() {
		return "in-escape-code-state";
	}
}
