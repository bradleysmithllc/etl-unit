package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FilenameUtils;
import org.bitbucket.bradleysmithllc.etlunit.Configuration;
import org.bitbucket.bradleysmithllc.etlunit.ETLTestVM;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.cli.regexp.DelimitedFileNameExpression;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFile;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.maven.ETLUnitMojo;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import javax.inject.Inject;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@CLIEntry(
	description = "Migrates data files from one FML to another",
	version = "1.0",
	nickName = "migrate",
	commandGroup = "etlunit"
)
public class MigrateCommand extends ConsoleCommand {
	enum scan_type {files, data}

	private RuntimeSupport runtimeSupport;
	private FileRuntimeSupport fileRuntimeSupport;
	private DataFileManager dataFileManager;

	private String namePattern = null;

	@CLIOption(
		description = "Regular expression of the data set(s) to match.",
		name = "np",
		longName = "name-pattern",
		defaultValue = "*",
		unnamedArgumentOrdinal = 1
	)
	public void namePattern(String pat) {
		namePattern = pat;
	}

	@CLIMain
	public void migrate() throws IOException {
		String dataSetNamePattern = null;

		if (!namePattern.equals("*")) {
			dataSetNamePattern = namePattern;
		}

		try {
			File basedir = new File(".");

			Configuration config = ETLUnitMojo.loadConfiguration(basedir, "migrate", "1.0a1", null);

			ETLTestVM vm = new ETLTestVM(config);

			vm.addFeature(new AbstractFeature() {
				@Override
				public List<String> getPrerequisites() {
					return Arrays.asList("file");
				}

				@Inject
				public void receiveRuntimeSupport(RuntimeSupport rs) {
					runtimeSupport = rs;
				}

				@Inject
				public void receiveFileRuntimeSupport(FileRuntimeSupport rs) {
					fileRuntimeSupport = rs;
				}

				@Inject
				public void receiveDataFileManager(DataFileManager dfm) {
					dataFileManager = dfm;
				}

				@Override
				public String getFeatureName() {
					return "migrator";
				}
			});

			vm.installFeatures();

			// don't actually run the tests, I just wanted the support objects
			// loop through all packages and prompt to migrate a data set
			for (ETLTestPackage pack : runtimeSupport.getTestPackages()) {
				println("Processing package: " + pack.getPackageName());

				List<File> fileList = new ArrayList<File>();

				File files = new File(runtimeSupport.getTestSourceDirectory(pack), "files");
				scan(files, fileList, scan_type.files, dataSetNamePattern);
				File data = new File(runtimeSupport.getTestSourceDirectory(pack), "data");
				scan(data, fileList, scan_type.data, dataSetNamePattern);

				List<File> filesToMigrate;

				if (fileList.size() == 0)
				{
					continue;
				}
				else if (fileList.size() == 1)
				{
					filesToMigrate = new ArrayList<File>();
					filesToMigrate.addAll(fileList);
				}
				else
				{
					filesToMigrate = selectAnItem(fileList, "Select a file to migrate: ", true);
				}


				if (filesToMigrate == null) {
					return;
				}

				List<File> referenceFileSchemaFilesForPackage = fileRuntimeSupport.getReferenceFileSchemaFilesForPackage(ETLTestPackageImpl.getDefaultPackage());

				List<File> schemaSource = selectAnItem(referenceFileSchemaFilesForPackage, "Select the source schema: ", false);

				if (schemaSource == null || schemaSource.size() == 0) {
					return;
				}

				File sourceSchema = schemaSource.get(0);

				schemaSource = selectAnItem(referenceFileSchemaFilesForPackage, "Select the destination schema: ", false);

				if (schemaSource == null || schemaSource.size() == 0) {
					return;
				}

				File destSchema = schemaSource.get(0);

				for (File dataSet : filesToMigrate) {
					print("Migrating " + dataSet.getName() + " using " + sourceSchema.getName() + " as the source schema, and " + destSchema.getName() + " as the destination schema . . . ");

					try {
						DataFileSchema srcfml = dataFileManager.loadDataFileSchema(sourceSchema, FilenameUtils.removeExtension(sourceSchema.getName()));
						DataFileSchema dstfml = dataFileManager.loadDataFileSchema(destSchema, FilenameUtils.removeExtension(destSchema.getName()));

						DataFile srcData = dataFileManager.loadDataFile(dataSet, srcfml);

						File toMigBak = new File(dataSet.getParentFile(), dataSet.getName() + ".bak");
						File toMigTarg = new File(dataSet.getParentFile(), dataSet.getName() + ".tmp");

						toMigBak.delete();
						toMigTarg.delete();

						DataFile destData = dataFileManager.loadDataFile(toMigTarg, dstfml);

						dataFileManager.copyDataFile(srcData, destData);

						// copy target over the source
						dataSet.renameTo(toMigBak);
						toMigTarg.renameTo(dataSet);

						println("Done.");
					} catch (Exception exc) {
						println(exc.toString());
					}
				}

				println("Migration done.");
			}
		} catch (Exception exc) {
			exc.printStackTrace();
			println(exc.toString());
		}
	}

	private List<File> selectAnItem(List<File> fileList, String s, boolean multipleAllowed) throws IOException {
		List<File> selected = new ArrayList<File>();

		boolean done = false;

		int num = 0;

		System.out.println(s);
		for (File option : fileList) {
			println("[" + ++num + "] - " + IOUtils.removeExtension(option));
		}

		if (fileList.size() == 0) {
			println("No files available");
			return selected;
		}

		println("[x] - Cancel");
		println("[d] - Done");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		while (!done && selected.size() < fileList.size()) {
			String opt = readWithPrompt("Please select an option: ");

			if (opt.equals("x")) {
				return null;
			}

			if (opt.equals("d")) {
				done = true;

				if (selected.size() == 0)
				{
					return null;
				}
			} else {
				try {
					int select = Integer.parseInt(opt);

					if (select <= 0 || select > fileList.size()) {
						System.out.println("Numeric index out of range [1, " + fileList.size() + "]: " + select);
					} else {
						File e = fileList.get(select - 1);

						if (selected.contains(e))
						{
							System.out.println("Item [" + e.getName() + "] already selected.");
						}
						else
						{
							selected.add(e);
							if (!multipleAllowed)
							{
								done = true;
							}
						}
					}
				} catch (NumberFormatException e) {
					System.out.println("Bad option - not a number, 'x' or 'd': " + opt);
				}
			}
		}

		return selected;
	}

	private void scan(File files, final List<File> fileList, final scan_type data, String dataSetNamePattern) {
		final Pattern namePattern;

		if (dataSetNamePattern != null)
		{
			namePattern = Pattern.compile(dataSetNamePattern, Pattern.CASE_INSENSITIVE);
		}
		else
		{
			namePattern = null;
		}

		files.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				switch (data) {
					case files:
						if (!pathname.getName().equals(".svn")) {
							if (namePattern != null)
							{
								if (namePattern.matcher(pathname.getName()).find())
								{
									fileList.add(pathname);
								}
							}
							else
							{
								fileList.add(pathname);
							}
						}
						break;
					case data:
						DelimitedFileNameExpression dfne = new DelimitedFileNameExpression(pathname.getName());

						if (dfne.matches()) {
							if (namePattern != null)
							{
								if (namePattern.matcher(pathname.getName()).find())
								{
									fileList.add(pathname);
								}
							}
							else
							{
								fileList.add(pathname);
							}
						}

						break;
				}

				return false;
			}
		});
	}
}
