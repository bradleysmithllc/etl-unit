package org.bitbucket.bradleysmithllc.etlunit.jline_cli;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

public class ParsedLine {
	private String command;

	private List<ParsedLineComponent> components = new ArrayList<>();

	public ParsedLine() {}

	public ParsedLine(String text) {
		this.command = text;
	}

	public void command(String text) {
		this.command = text;
	}

	public String command() {
		return this.command;
	}

	public void addParsedLineComponent(ParsedLineComponent comp) {
		components.add(comp);
	}

	public void addParsedLineText(String text, ParsedLineComponent.type type) {
		components.add(new ParsedLineComponent(text, type));
	}

	public List<ParsedLineComponent> components() {
		return components;
	}
}

/*
* 			// arguments are yellow
			ParsedLineComponent
			return new AttributedString(token.image, 0, token.image.length(), AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.YELLOW));

		if (attributions.size() == 0) {
			return new AttributedString("", 0, 0, AttributedStyle.DEFAULT);
		} else {
			return AttributedString.join(new AttributedString("", 0, 0, AttributedStyle.DEFAULT), attributions);
		}
* */
