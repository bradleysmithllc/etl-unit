package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.system;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.MenuItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FilesystemMenu extends AbstractMenu<FileMenuItem> {
	private final File root;

	private final List<FileMenuItem> items = new ArrayList<>();

	public FilesystemMenu(File root) {
		this.root = root;

		root.listFiles((file) -> {
			items.add(new FileMenuItem(file));

			return false;
		});
	}

	@Override
	public boolean includeCancel() {
		return true;
	}

	@Override
	public boolean includeReset() {
		return true;
	}

	@Override
	public String prompt() {
		return root.getAbsolutePath();
	}

	@Override
	public List<FileMenuItem> items() {
		return items;
	}

	@Override
	public Menu choose(FileMenuItem item) {
		if (item.isLeaf()) {
			return null;
		} else {
			return new FilesystemMenu(new File(root, item.name()));
		}
	}
}
