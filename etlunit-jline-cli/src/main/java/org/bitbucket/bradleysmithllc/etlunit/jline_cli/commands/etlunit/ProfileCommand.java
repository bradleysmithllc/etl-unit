package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.maven.ETLUnitMojo;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.io.IOException;

@CLIEntry(
	description = "Changes to a specific profile or colon-separated list of profiles.  Similar to using the etlunit configuration flag in maven (Not a maven profile).  Leave profile name blank to switch to the default.",
	version = "1.0",
	nickName = 	"profile",
	commandGroup = "etlunit"
)
public class ProfileCommand extends ConsoleCommand {
	private String profileNames = null;
	private boolean list = false;

	@CLIOption(
		description = "Profile name or colon-separated list of names.  Blank to reset a previously selected profile.  Use -l to list effective profile(s) - but this cannot be used with other options.",
		name = "p",
		longName = "profile",
		unnamedArgumentOrdinal = 1
	)
	public void profileNames(String names) {
		profileNames = names;
	}

	@CLIOption(
			description = "List available profiles",
			name = "l",
			longName = "list",
			valueType = CLIOption.value_type.flag
	)
	public void listProfiles(boolean list) {
		this.list = list;
	}

	@CLIMain
	public void set() throws IOException {
		if (list)
		{
			println("Current etlunit configuration: ");
			for (String prof : ETLUnitMojo.getUserProfiles()) {
				println("\t" + prof);
			}
		} else if (profileNames == null)
		{
			print("Current etlunit configuration: ");
			for (String prof : ETLUnitMojo.getUserProfiles()) {
				println("\t" + prof);
			}

			ETLUnitMojo.setUserProfile(null);
			println("Etlunit configuration reset.");
		}
		else
		{
			ETLUnitMojo.setUserProfile(profileNames);

			println("Etlunit configuration changed to: ");
			for (String prof : ETLUnitMojo.getUserProfiles()) {
				println("\t" + prof);
			}
		}
	}
}
