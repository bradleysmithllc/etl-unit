package org.bitbucket.bradleysmithllc.etlunit.jline_cli.parser.ansi;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.ParseException;

public class AnsiEscapeParser {

	public static final char ANSI_ESCAPE = '\u001b';

	enum parserState {
		text,
		escapeStart,
		inEscapeCode
	}

	public static String decorateAnsi(String possibleAnsi) throws ParseException {
		StringBuffer outBuff = new StringBuffer();

		parserState currentParserState = AnsiEscapeParser.parserState.text;

		for (char ch : possibleAnsi.toCharArray()) {
			if (ch == ANSI_ESCAPE && currentParserState == parserState.escapeStart) {
				outBuff.append(ANSI_ESCAPE);
			}

			switch (ch) {
				// may be start of escape sequence.  Wait for trailing '['
				case ANSI_ESCAPE:
					failIfInState(currentParserState, parserState.inEscapeCode);

					if (currentParserState == parserState.text) {
						currentParserState = parserState.escapeStart;
					}
					continue;
				case '[':
					failIfInState(currentParserState, parserState.inEscapeCode);

					if (currentParserState == parserState.escapeStart) {
						currentParserState = parserState.inEscapeCode;
						continue;
					}
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					switch (currentParserState) {
						case text:
							break;
						case escapeStart:
							outBuff.append(ANSI_ESCAPE);
							currentParserState = parserState.text;
							break;
						case inEscapeCode:
							break;
					}
					break;
				case 'm':
					switch (currentParserState) {
						case text:
							break;
						case escapeStart:
							currentParserState = parserState.text;
							break;
						case inEscapeCode:
							break;
					}

					currentParserState = parserState.text;
					break;
				default:
					currentParserState = parserState.text;
			}

			outBuff.append((char) ch);
		}

		switch (currentParserState) {
			case escapeStart:
				outBuff.append(ANSI_ESCAPE);
				break;
			case inEscapeCode:
				throw new ParseException("Dangling escape code", 0);
		}

		return outBuff.toString();
	}

	private static void failIfInState(parserState currentParserState, parserState... inEscape) throws ParseException {
		for (parserState state : inEscape) {
			if (currentParserState == state) {
				throw new ParseException("Escape character encountered within an escape code", 0);
			}
		}
	}
}
