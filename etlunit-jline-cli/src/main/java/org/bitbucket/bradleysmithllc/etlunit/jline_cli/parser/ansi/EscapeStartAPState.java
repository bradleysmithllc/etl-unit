package org.bitbucket.bradleysmithllc.etlunit.jline_cli.parser.ansi;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.ParseException;

public class EscapeStartAPState implements APState {
	private TextAPState textAPState;
	private InEscapeCodeAPState inEscapeCodeAPState;

	@Override
	public APState consume(char ch, StringBuffer building) throws ParseException {
		switch (ch) {
			case AnsiEscapeParser.ANSI_ESCAPE:
				// preserve consumed escape token
				building.append(AnsiEscapeParser.ANSI_ESCAPE);
				break;
			case '[':
				return inEscapeCodeAPState;
			default:
				// preserve token and return to text
				building.append(AnsiEscapeParser.ANSI_ESCAPE);
				textAPState.consume(ch, building);
				return textAPState;
		}

		return null;
	}

	@Override
	public void enter() {
	}

	@Override
	public void eof(StringBuffer building) throws ParseException {
		// must put back the dangling escape
		building.append(AnsiEscapeParser.ANSI_ESCAPE);
	}

	public void link(TextAPState textAPState) {
		this.textAPState = textAPState;
	}

	public void link(InEscapeCodeAPState inEscapeCodeAPState) {
		this.inEscapeCodeAPState = inEscapeCodeAPState;
	}

	@Override
	public String toString() {
		return "escape-start-state";
	}
}
