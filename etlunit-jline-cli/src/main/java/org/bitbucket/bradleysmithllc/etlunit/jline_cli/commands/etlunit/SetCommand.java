package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ETLTestVM;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOptionDescriptor;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.set_menu.FeatureMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.set_menu.RuntimeOptionsMenuItem;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import javax.inject.Inject;
import java.util.*;

@CLIEntry(
		description = "Sets or unsets runtime options",
		version = "1.0",
		nickName = "set",
		commandGroup = "etlunit"
)
public class SetCommand extends ConsoleCommand {
	private String featureName;

	@CLIOption(
			description = "Optional name of the feature to (un)set an option for.",
			name = "fn",
			longName = "feature-name",
			unnamedArgumentOrdinal = 1
	)
	public void featureName(String name) {
		featureName = name;
	}

	public static Map<String, RuntimeOption> statelessOptions = new HashMap<String, RuntimeOption>();

	private MapList<Feature, RuntimeOption> mapList;

	public static void prepareVm(ETLTestVM vm) {
		vm.getRuntimeSupport().overrideRuntimeOptions(new ArrayList<>(statelessOptions.values()));
	}

	@CLIMain
	public void set() throws Exception {
		ETLTestVM vm = TestCommand.getEtlTestVM();

		vm.addFeature(new AbstractFeature() {

			private String name = "mapListGrabber." + System.currentTimeMillis();

			@Inject
			public void receiveRuntimeSupport(MapList<Feature, RuntimeOption> ml) {
				mapList = ml;
			}

			@Override
			public String getFeatureName() {
				return name;
			}
		});

		vm.installFeatures();

		if (statelessOptions.size() > 0) {
			println("Overridden options:");

			for (Map.Entry<String, RuntimeOption> optionEntry : statelessOptions.entrySet()) {
				RuntimeOption option = optionEntry.getValue();

				RuntimeOptionDescriptor descriptor = option.getDescriptor();
				println("\t" + option.getName() + " - " + descriptor.getDescription());

				switch (descriptor.getOptionType()) {
					case bool:
						println("\t\tEnabled: " + option.isEnabled());
						break;
					case integer:
						println("\t\tValue: " + option.getIntegerValue());
						break;
					case string:
						println("\t\tValue: " + option.getStringValue());
						break;
				}
			}
		} else {
			println("No options are overridden");
		}

		Feature targetFeature = null;

		if (featureName != null) {
			for (Feature feature : mapList.keySet()) {
				if (feature.getFeatureName().equals(featureName)) {
					targetFeature = feature;
				}
			}

			if (targetFeature == null) {
				println("Invalid feature '" + featureName + "'");
			}
		}

		menu(new FeatureMenu(mapList), (item) -> {
			RuntimeOptionsMenuItem romi = (RuntimeOptionsMenuItem) item;

			RuntimeOption runtimeOption = romi.runtimeOption();
			println("Updating - '" + runtimeOption.getName() + "'");
			// prompt for the new value
			switch (runtimeOption.getDescriptor().getOptionType())
			{
				case bool:
					boolean defBo = runtimeOption.getDescriptor().getDefaultBooleanValue();
					SetCommand.statelessOptions.put(runtimeOption.getName(), option(new RuntimeOption(runtimeOption.getName(), getBool(defBo)), runtimeOption));
					break;
				case integer:
					SetCommand.statelessOptions.put(runtimeOption.getName(), option(new RuntimeOption(runtimeOption.getName(), getInt(runtimeOption.getDescriptor().getDefaultIntegerValue())), runtimeOption));
					break;
				case string:
					String in = getString(runtimeOption.getDescriptor().getDefaultStringValue());
					SetCommand.statelessOptions.put(runtimeOption.getName(), option(new RuntimeOption(runtimeOption.getName(), in), runtimeOption));
					break;
			}
		}, () -> {
			println("Resetting in-memory options.");
			SetCommand.statelessOptions.clear();
		});
	}

	private RuntimeOption option(RuntimeOption runtimeOption, RuntimeOption ro) {
		runtimeOption.setDescriptor(ro.getDescriptor());
		runtimeOption.setFeature(ro.getFeature());

		return runtimeOption;
	}

	private boolean getBool(boolean defBo) {
		while (true) {
			String in = readWithPrompt("\tEnable " + (defBo ? "Y/n" : "y/N") + ": ").trim();

			if (in.equalsIgnoreCase("Y")) {
				return true;
			} else if (in.equalsIgnoreCase("N")) {
				return false;
			} else if (in.equals("")) {
				return defBo;
			}

			println("Invalid boolean - '" + in + "' - please enter 'y' or 'n' or blank for the default");
		}
	}

	private String getString(String defBo) {
		String in = readWithPrompt("\tValue [" + defBo + "]: ").trim();

		if (in.equals("")) {
			return defBo;
		}

		return in;
	}

	private int getInt(int def) {
		while (true) {
			String in = readWithPrompt("\tInteger Value [" + def + "]: ").trim();

			if (in.equals("")) {
				return def;
			}

			try {
				return Integer.parseInt(in);
			} catch (NumberFormatException exc) {
				println("Invalid int - '" + in + "' - please enter a valid integer or blank for the default");
			}
		}
	}
}
