package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.set_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenuItem;

import java.util.List;

public class FeatureMenuItem extends AbstractMenuItem {
	private final Feature feature;
	private final List<RuntimeOption> runtimeOptions;

	public FeatureMenuItem(Feature feature, List<RuntimeOption> runtimeOptions) {
		this.feature = feature;
		this.runtimeOptions = runtimeOptions;
	}

	@Override
	public String name() {
		return feature.getFeatureName();
	}

	public Feature feature() {
		return feature;
	}

	public List<RuntimeOption> runtimeOptions() {
		return runtimeOptions;
	}
}
