package org.bitbucket.bradleysmithllc.etlunit.jline_cli;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.CommandNamesUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.HashMapArrayList;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.*;
import java.util.stream.Collectors;

public class CommandLoader {
	private List<CommandReference> commandClasses = new ArrayList<>();
	private Map<String, CommandReference> commandNickNames = new HashMap<>();

	private MapList<String, CommandReference> commandGroupMap = new HashMapArrayList<>();

	private final Completer completer = new Completer() {
		@Override
		public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {
			for (CommandReference command : commandClasses) {
				candidates.add(new Candidate(command.nickName()));
			}
		}
	};

	public CommandLoader() throws Exception {
		// grab the commands.txt file listing available commands
		String text = ResourceUtils.loadResourceAsString(getClass(), "commands.txt");

		// read line-by-line and load each command class
		BufferedReader bread = new BufferedReader(new StringReader(text));

		String nextClassName = null;

		while ((nextClassName = bread.readLine()) != null)
		{
			if (nextClassName.equals("") || nextClassName.startsWith("#")) {
				continue;
			}

			Class cla = Class.forName(nextClassName);

			readAnnotation(cla);
		}

		// sort groups
		for (List<CommandReference> groupList : commandGroupMap.values()) {
			Collections.sort(groupList, (a, b) -> {return a.nickName().compareTo(b.nickName());});
		}

		Map<String, String> resultsM = CommandNamesUtils.createShortcutsFor(group("etlunit").stream().map((cr) -> {return cr.nickName();}).collect(Collectors.toSet()));

		// assign an implicit shortcut for help with '?'
		CommandReference help = commandByNickname("help");
		help.addShortcut("?");
		commandNickNames.put("?", help);

		// build shortcuts
		for (Map.Entry<String, String> entry : resultsM.entrySet()) {
			// map plain nickname
			CommandReference commandReference = commandNickNames.get(entry.getKey());
			commandReference.addShortcut(entry.getValue());
			commandNickNames.put(entry.getValue(), commandReference);
		}
	}

	private void readAnnotation(Class cla) {
		CLIEntry cli = (CLIEntry) cla.getAnnotation(CLIEntry.class);

		if (cli == null) {
			throw new IllegalStateException("Class is not a command " + cla.getName());
		}

		if (commandNickNames.containsKey(cli.nickName())) {
			throw new IllegalStateException("Command nickName " + cli.nickName() + " already used.");
		}

		if (!EtlUnitShellCommand.class.isAssignableFrom(cla)) {
			throw new IllegalStateException("Command nickName " + cli.nickName() + " is not a shell command.");
		}

		CommandReference cr = new CommandReference(
				cli,
				cla
		);

		commandClasses.add(cr);
		commandNickNames.put(cli.nickName().toLowerCase(), cr);
		commandGroupMap.add(cli.commandGroup().toLowerCase(), cr);
	}

	public Completer completer() {
		return completer;
	}

	public CommandReference commandByNickname(String command) {
		return commandNickNames.get(command.toLowerCase());
	}

	public Set<String> groupNames() {
		return commandGroupMap.keySet();
	}

	public List<CommandReference> commands() {
		return commandClasses;
	}

	public List<CommandReference> group(String name) {
		return commandGroupMap.get(name.toLowerCase());
	}
}
