package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.maven.model.Model;
import org.apache.maven.model.io.DefaultModelReader;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.debug.ConsoleFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.debug.RunAllFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.tags.TagsFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.EtlUnitJLineCLI;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.maven.ETLUnitMojo;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;
import org.bitbucket.bradleysmithllc.etlunit.util.MessageLine;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@CLIEntry(
	description = "Run etlunit tests",
	version = "1.0",
	nickName = "test",
	commandGroup = "etlunit"
)
public class TestCommand extends ConsoleCommand {
	private static final String TEST_SPECIFICATION_HELP = "Optional test specification:\n" +
			"  % - package name\n" +
			"  @ - class name\n" +
			"  | - beginning or end of whole word - E.G., |startsWith, endsWith|, |exactly|\n" +
			"  # - method name\n" +
			"  $ - operation name\n" +
			"  [] - suite.\n" +
			"  <> - tags.\n" +
			"conditions.  and, or, not, eor. in combination with parentheses.  E.G., %pack and @class, %pack and (@class or #method)" +
			"  and - both left and right sides must match." +
			"  or - either left and right sides can match." +
			"  not - the left value must be true and the right value must not be true.  E.G.," +
			"  %pack not @class means run every test in package like *pack* but not any class" +
			"  like *class*" +
			"  eor - accepts a test if only the left or right conditions match, but not either or neither.";

	private String testSpecification = null;

	@CLIOption(
		description = TestCommand.TEST_SPECIFICATION_HELP,
		name = "spec",
		longName = "test-specification",
		unnamedArgumentOrdinal = 1
	)
	public void testSpecification(String spec) {
		testSpecification = spec;
	}

	private MapList<Feature, RuntimeOption> mapList;

	@CLIMain
	public void test() {
		// trim after the first space
		try {
			ETLTestVM vm = getEtlTestVM();

			SetCommand.prepareVm(vm);

			// add a feature to select only the tests we were directed to
			if (testSpecification != null) {
				vm.addFeature(new UserDirectedClassDirectorFeature("cli-test-selector", testSpecification));
			}

			vm.addFeature(new AbstractFeature() {
				@Inject
				public void receiveRuntimeSupport(MapList<Feature, RuntimeOption> ml) {
					mapList = ml;
				}

				@Override
				public String getFeatureName() {
					return "mapListGrabber.0910281028100821309230";
				}
			});

			// 'activate' the console output
			ConsoleFeatureModule console1 = new ConsoleFeatureModule();
			vm.addFeature(console1);

			// install a dummy feature so a director can be installed which will accept all tests
			vm.addFeature(new RunAllFeatureModule());

			vm.installFeatures();

			writeLine(MessageLine.formatStandardMessage("Running with options:"));

			// alphabetize the list of features and options so the output is deterministic
			List<Feature> featureList = new ArrayList<Feature>(mapList.keySet());

			Collections.sort(featureList, new Comparator<Feature>() {
				@Override
				public int compare(Feature o1, Feature o2) {
					return o1.getFeatureName().compareTo(o2.getFeatureName());
				}
			});

			for (Feature feature : featureList) {
				writeLine(feature.getFeatureName());

				List<RuntimeOption> runtimeOptions = new ArrayList<RuntimeOption>(mapList.get(feature));
				Collections.sort(runtimeOptions, new Comparator<RuntimeOption>() {
					@Override
					public int compare(RuntimeOption o1, RuntimeOption o2) {
						return o1.getName().compareTo(o2.getName());
					}
				});

				for (RuntimeOption option : runtimeOptions) {
					writeLine("\t" + option.getDescriptor().getName() + ": " + option.printableValue());
				}
			}

			writeLine(MessageLine.formatStandardMessage(""));
			writeLine();

			//TEST!
			TestResults results = vm.runTests();

			TestResultMetrics metrics = results.getMetrics();

			List<TestClassResult> resultsByClass = results.getResultsByClass();

			for (TestClassResult testResult : resultsByClass) {
				for (TestMethodResult methodResult : testResult.getMethodResults()) {
					if (
							methodResult.getMetrics().getNumberOfAssertionFailures() != 0
									||
									methodResult.getMetrics().getNumberOfErrors() != 0
					) {

					}
				}
			}

			/**If a custom tag name was set, clear it so it doesn't get overwritten next time */
			System.clearProperty(TagsFeatureModule.ETLUNIT_TAG_NAME);

			if (metrics.getNumberOfErrors() > 0 || metrics.getNumberOfAssertionFailures() > 0) {
				throw new MojoExecutionException("Build failed with test errors");
			}
		} catch (MojoExecutionException exc) {
			// ignore this one - it's just the mojo letting us know there were
			// failures
		} catch (Exception exc) {
			exc.printStackTrace();
			writeLine(exc.toString());
		}
	}

	public static ETLTestVM getEtlTestVM() throws IOException {
		File basedir = new File(".");

		Model model = new DefaultModelReader().read(new File(basedir, "pom.xml"), null);

		MavenProject mavenProject = new MavenProject(model);

		final Configuration con = ETLUnitMojo.loadConfiguration(basedir, mavenProject.getName(), mavenProject.getVersion(), EtlUnitJLineCLI.class.getClassLoader());

		ServiceLocatorFeatureLocator loc = new ServiceLocatorFeatureLocator();

		ETLUnitMojo.getTempDirectoryRoot(basedir);

		return new ETLTestVM(loc, con);
	}
}
