package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupportImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.*;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CLIEntry(
	description = "Verifies all reference type catalogs",
	version = "1.0",
	nickName = "verify-ref-types",
	commandGroup = "etlunit"
)
public class VerifyRefTypesCommand extends ConsoleCommand {
	private final DataFileManager dataFileManager;

	{
		dataFileManager = new DataFileManagerImpl(new File("."));
	}

	private final ReferenceFileTypeManagerImpl manager = new ReferenceFileTypeManagerImpl();
	private final FileRuntimeSupportImpl fileRuntimeSupport = new FileRuntimeSupportImpl();

	@CLIMain
	public void open() {
		try {
			BasicRuntimeSupport rs = (BasicRuntimeSupport) TestCommand.getEtlTestVM().getRuntimeSupport();

			rs.setApplicationLogger(new PrintWriterLog());
			manager.receiveRuntimeSupport(rs);
			fileRuntimeSupport.receiveRuntimeSupport(rs);
			fileRuntimeSupport.receiveApplicationLog(rs.getApplicationLog());
			fileRuntimeSupport.receiveReferenceFileTypeManager(manager);
			fileRuntimeSupport.receiveDataFileManager(dataFileManager);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<ReferenceFileTypeCatalog> catalogs = manager.locateReferenceTypeCatalogs();

		if (catalogs.size() == 0)
		{
			println("No catalogs available." );
			return;
		}

		final List<ReferenceFileTypePackage> packList = new ArrayList<ReferenceFileTypePackage>();

		for (ReferenceFileTypeCatalog catalog : catalogs)
		{
			for (Map.Entry<String, ReferenceFileTypePackage> _package_ : catalog.getReferenceFileTypePackages().entrySet())
			{
				packList.add(_package_.getValue());
			}
		}

		// scan through each package, grab every type
		for (ReferenceFileTypePackage pack : packList)
		{
			for (Map.Entry<String, ReferenceFileType> typeEntry : pack.getReferenceFileTypesById().entrySet())
			{
				ReferenceFileTypeRef ref = ReferenceFileTypeRef.refWithId(typeEntry.getKey());

				println("Verifying reference: " + ref.toBriefString() );

				// for each type, verify every version
				ReferenceFileType value = typeEntry.getValue();
				if (value.hasVersions())
				{
					for (String version : value.getVersions())
					{
						// load each version and verify
						ref = ref.withVersion(version);

						println("Verifying sub version: " + ref.toBriefString() );

						try {
							verifyResource(ref);
						} catch (Exception e) {
							e.printStackTrace();
							println();
							println("=======================================");
							println("========= Error verifying ref : " + ref.toBriefString() + " {" + e + "} ========" );
						}
					}
				}
				else
				{
					// verify the base type
					try {
						verifyResource(ref);
					} catch (Exception e) {
						e.printStackTrace();
						println();
						println("=======================================");
						println("========= Error verifying ref : " + ref.toBriefString() + " {" + e + "} ========" );
					}
				}

				// verify each classifier
				for (Map.Entry<String, ReferenceFileTypeClassifier> classifierEntry : value.getClassifiers().entrySet())
				{
					// remove any version left over
					ref = ref.withClassifier(classifierEntry.getKey()).withoutVersion();

					println("Verifying classifier: " + ref.toBriefString() );

					// run through each version
					ReferenceFileTypeClassifier classifier = classifierEntry.getValue();
					if (classifier.hasVersions())
					{
						for (String clversion : classifier.getVersions())
						{
							ref = ref.withVersion(clversion);

							println("Verifying classifier sub version: " + ref.toBriefString() );

							// verify the classifier with version
							try {
								verifyResource(ref);
							} catch (Exception e) {
								e.printStackTrace();
								println();
								println("=======================================");
								println("========= Error verifying ref : " + ref.toBriefString() + " {" + e + "} ========" );
							}
						}
					}
					else
					{
						// verify the base classifier
						try {
							verifyResource(ref);
						} catch (Exception e) {
							e.printStackTrace();
							println();
							println("=======================================");
							println("========= Error verifying ref : " + ref.toBriefString() + " {" + e + "} ========" );
						}
					}
				}
			}
		}
	}

	private void verifyResource(ReferenceFileTypeRef ref) throws Exception {
		fileRuntimeSupport.locateReferenceFileType(ref, null);
	}
}
