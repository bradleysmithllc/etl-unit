package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.maven.ETLUnitMojo;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.io.File;
import java.io.IOException;
import java.util.List;

@CLIEntry(
	description = "Adds a known feature to the project (listed in list-installed-features)",
	version = "1.0",
	nickName = "add-available-feature",
	commandGroup = "etlunit"
)
public class AddAvailableFeatureCommand extends ConsoleCommand {
	private String featureName = null;

	@CLIOption(
		description = "Feature to add",
		name = "f",
		longName = "feature-name"
	)
	public void featureName(String name) {
		featureName = name;
	}

	@CLIMain
	public void add() throws IOException {
		File cfg = new File(".");

		if (featureName.equals("")) {
			println("Please specify exactly one feature to add");
			return;
		}

		try {
			List<String> installedFeatures = ListInstalledFeaturesCommand.getInstalledFeatures(cfg);

			if (installedFeatures != null && installedFeatures.contains(featureName)) {
				println("Feature " + featureName + " already added");
			} else {
				List<Feature> availableFeatures = ListAvailableFeaturesCommand.getAvailableFeatures();

				Feature toInstall = null;

				for (Feature feature : availableFeatures) {
					if (feature.getFeatureName().equals(featureName)) {
						toInstall = feature;
					}
				}

				if (toInstall != null) {
					// load the config file and pretty print it
					ObjectMapper mapper = new ObjectMapper();

					File etlunitConfiguration = ETLUnitMojo.getEtlunitConfiguration(cfg);
					JsonNode node = mapper.readTree("{" + IOUtils.readFileToString(etlunitConfiguration) + "}");

					((ArrayNode) node.get("install-features")).add(featureName);

					String config = mapper.writerWithDefaultPrettyPrinter()
							.writeValueAsString(node);

					int index = config.indexOf("{");

					config = config.substring(index + 1);

					index = config.lastIndexOf("}");

					config = config.substring(0, index - 1).trim();

					IOUtils.writeBufferToFile(etlunitConfiguration, new StringBuffer(config));

					println("Feature " + featureName + " added");
				} else {
					println("Feature " + featureName + " not available.  Run list-available-features for valid features to install");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
