package org.bitbucket.bradleysmithllc.etlunit.jline_cli.util;

/*
 * #%L
 * etlunit-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.MessageLine;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.*;

import javax.xml.transform.Source;
import java.io.File;

@CLIEntry(
		version = "2.0.16.2",
		description = "Checks a project to see if an update is needed.",
		nickName = "project-update-check"
)
public class ProjectUpdateCheck {
	public static final int EXIT_PROJECT_UP_TO_DATE = 0;
	public static final int EXIT_PROJECT_OUT_OF_DATE = 1;

	public static void main(String [] argv) {
		CommonsCLILauncher.mainClean(argv);
	}

	@CLIMain
	public void go() throws Exception {
		// compare the pom.xml with the target/pom_bak.xml
		File thisEffectivePom = new FileBuilder(new File("target")).name("effective_pom.xml").file();
		File lastEffectivePom = new FileBuilder(new File("target")).name("effective_pom_bak.xml").file();

		System.out.println(MessageLine.formatStandardMessage("Project Update Check"));

		if (!thisEffectivePom.exists()) {
			System.out.println(MessageLine.formatStandardMessage("Current effective pom does not exist.  Fail."));
		} else
		if (!lastEffectivePom.exists()) {
			System.out.println(MessageLine.formatStandardMessage("Prior effective pom does not exist.  Fail."));
		} else {
			System.out.println(MessageLine.formatStandardMessage("Effective poms exist.  Comparing..."));

			// compare
			Diff diff = DiffBuilder
					.compare(Input.fromFile(thisEffectivePom).build())
					.withTest(Input.fromFile(lastEffectivePom).build())
					.ignoreComments()
				.build();

			if (diff.hasDifferences())
			{
				for (Difference difference : diff.getDifferences()) {
					System.out.println(difference.toString());
				}
			} else {
				System.out.println(MessageLine.formatStandardMessage("Project is up to date"));
				System.exit(EXIT_PROJECT_UP_TO_DATE);
			}
		}

		// default result - project dirty
		System.out.println(MessageLine.formatStandardMessage("Project is out of date"));
		System.exit(EXIT_PROJECT_OUT_OF_DATE);
	}
}
