package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.TerminalInterface;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;

import java.sql.SQLException;

public class TableMenuItem extends DatabaseMenuItem {
	private final String catalog;
	private final String schema;
	private final String table;
	private final ETLTestClass etlTestClass;

	public TableMenuItem(
			DatabaseFeatureModule databaseFeatureModule,
			DatabaseConnection value,
			TerminalInterface terminalInterface,
			ETLTestClass etlTestClass,
			String catalog, String schema, String tableName) {
		super(databaseFeatureModule, value, terminalInterface, type.table);

		this.catalog = catalog;
		this.schema = schema;
		this.table = tableName;
		this.etlTestClass = etlTestClass;
	}

	@Override
	public Menu<DatabaseMenuItem> menu() throws Exception {
		throw new IllegalStateException();
	}

	@Override
	public String name() {
		return table.toLowerCase();
	}

	public String catalog() {
		return catalog;
	}

	public String schema() {
		return schema;
	}

	public String table() {
		return table;
	}

	public ETLTestClass etlTestClass() {
		return etlTestClass;
	}

	@Override
	public void disposeAll() throws SQLException {
	}

	public DatabaseConnection databaseConnection() {
		return databaseConnection;
	}
}
