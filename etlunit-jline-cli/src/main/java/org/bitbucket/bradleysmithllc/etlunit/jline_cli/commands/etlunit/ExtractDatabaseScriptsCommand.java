package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.ETLTestVM;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DDLSourceRef;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.SQLAggregator;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Table;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu.ConnectionIdMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu.ConnectionIdMenuItem;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu.DatabaseMenuItem;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu.TableMenuItem;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

@CLIEntry(
	description = "Grabs source SQL for a database.",
	version = "1.0",
	nickName = "extract-database-scripts",
	commandGroup = "etlunit"
)
public class ExtractDatabaseScriptsCommand extends ConsoleCommand {
	private File outputFolder = null;

	@CLIOption(
		description = "File to write the ddl files to",
		name = "d",
		longName = "directory"
	)
	public void writeToPath(String path) {
		outputFolder = new File(path);
	}

	@CLIMain
	public void refresh() throws Exception {
		ETLTestVM vm = TestCommand.getEtlTestVM();
		vm.installFeatures();

		Feature dbFeature = null;

		for (Feature feature : vm.getFeatures()) {
			if (feature.getFeatureName().equals("database")) {
				dbFeature = feature;
			}
		}

		RuntimeSupport runtimeSupport = vm.getRuntimeSupport();

		if (dbFeature == null) {
			println("Database feature is not available", AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.RED));
			return;
		}

		DatabaseFeatureModule databaseFeatureModule = (DatabaseFeatureModule) dbFeature;

		String runIdentifier = runtimeSupport.getRunIdentifier();

		ConnectionIdMenu connectionIdMenu = new ConnectionIdMenu(databaseFeatureModule, this, runIdentifier, false);
		menu(connectionIdMenu, (DatabaseMenuItem selectedItem) -> {
			if (outputFolder == null) {
				outputFolder = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory("database")).subdir(selectedItem.databaseConnection().getId()).subdir("schema-scripts").file();
			}

			try {
				for (String script : selectedItem.databaseConnection().getSchemaScripts()) {
					DDLSourceRef ddlref = new DDLSourceRef(selectedItem.databaseConnection().getImplementationId(), selectedItem.databaseConnection().getId(), script);
					SQLAggregator ddl = databaseFeatureModule.databaseRuntimeSupport().resolveDDLRef(ddlref, selectedItem.databaseConnection());

					File target = new File(outputFolder, script);

					FileUtils.write(target, ddl.getCleanText());
				}

				println(new AttributedStringBuilder().append("Schema files written to ").append(outputFolder.getAbsolutePath(), AttributedStyle.DEFAULT.italic().foreground(AttributedStyle.YELLOW)).toAttributedString());
			} catch (Exception exc) {
				println(exc.toString());
			}
		});

		connectionIdMenu.disposeAll();
	}
}
