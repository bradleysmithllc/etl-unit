package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;

import java.io.File;
import java.io.IOException;
import java.util.List;

@CLIEntry(
	description = "Lists all installed features",
	nickName = "list-installed-features",
	commandGroup = "etlunit",
	version = "1.0"
)
public class ListInstalledFeaturesCommand extends ConsoleCommand {
	@CLIMain
	public void open() throws IOException {
		File root = new File(".");

		try {
			List<String> list = getInstalledFeatures(root);

			if (list != null && list.size() != 0)
			{
				println("Installed features:");

				for (String feature : list)
				{
					println("\t" + feature);
				}
			}
			else
			{
				println("No features installed");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<String> getInstalledFeatures(File root) throws Exception {
		String text = ResourceUtils.loadResourceAsString(ListInstalledFeaturesCommand.class, "config/etlunit.json");

		// read the contents, and parse into an object
		ETLTestValueObject obj = ETLTestParser.loadObject(text);

		ETLTestValueObject feat = obj.query("install-features");

		if (feat != null)
		{
			return feat.getValueAsListOfStrings();
		}
		else
		{
			return null;
		}
	}
}
