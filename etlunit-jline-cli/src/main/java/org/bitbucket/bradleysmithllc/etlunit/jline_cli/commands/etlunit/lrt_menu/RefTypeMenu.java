package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.lrt_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileType;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypePackage;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RefTypeMenu extends AbstractMenu<MenuItem> {
	private final List<MenuItem> items = new ArrayList<>();

	public RefTypeMenu(ReferenceFileTypePackage referenceFileTypePackage) {
		for (Map.Entry<String, ReferenceFileType> entry : referenceFileTypePackage.getReferenceFileTypesById().entrySet()) {
			items.add(new RefTypeMenuItem(entry.getValue()));
		}
	}

	@Override
	public String prompt() {
		return "Select a reference type";
	}

	@Override
	public String title() {
		return "Reference types in package";
	}

	@Override
	public List<MenuItem> items() {
		return items;
	}
}
