package org.bitbucket.bradleysmithllc.etlunit.jline_cli;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;

import java.util.ArrayList;
import java.util.List;

public class CommandReference {
	private final CLIEntry entryAnnotation;
	private final Class entryImplementation;
	private final List<String> shortcuts = new ArrayList<>();

	public CommandReference(CLIEntry entryAnnotation, Class entryImplementation) {
		this.entryAnnotation = entryAnnotation;
		this.entryImplementation = entryImplementation;
	}

	//public CLIEntry entryAnnotation() {
	//	return entryAnnotation;
	//}

	public Class entryImplementation() {
		return entryImplementation;
	}

	public void addShortcut(String name) {
		shortcuts.add(name);
	}

	public List<String> shortcuts() {
		return shortcuts;
	}

	public String nickName() {
		return entryAnnotation.nickName();
	}

	public String description() {
		return entryAnnotation.description();
	}
}
