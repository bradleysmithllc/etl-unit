package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DDLSourceRef;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.SQLAggregator;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.TerminalInterface;

import java.sql.SQLException;

public class ConnectionIdMenuItem extends DatabaseMenuItem {
	private SchemaMenu schemaMenu = null;
	private final String runIdentifier;

	public ConnectionIdMenuItem(
			DatabaseFeatureModule databaseFeatureModule,
			DatabaseConnection value,
			TerminalInterface terminalInterface,
			String runIdentifier
	) {
		super(databaseFeatureModule, value, terminalInterface, type.connectionId);

		this.runIdentifier = runIdentifier;
	}

	@Override
	public void disposeAll() throws SQLException {
		if (schemaMenu != null) {
			schemaMenu.disposeAll();
		}
	}

	@Override
	public String name() {
		return databaseConnection.getId();
	}

	@Override
	public Menu<DatabaseMenuItem> menu() throws Exception {
		if (schemaMenu == null) {
			schemaMenu = new SchemaMenu(
					databaseFeatureModule, terminalInterface, databaseConnection, runIdentifier
			);
		}

		return schemaMenu;
	}
}
