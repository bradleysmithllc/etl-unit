package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Table;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu.ConnectionIdMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu.TableMenuItem;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.jline.utils.AttributedStyle;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

@CLIEntry(
	description = "Updates the meta data for a table in a database connection",
	version = "1.0",
	nickName = "refresh-database-metadata",
	commandGroup = "etlunit"
)
public class RefreshDatabaseMetadataCommand extends ConsoleCommand {
	private File target = null;
	private boolean useDesktopClipboard = true;

	@CLIOption(
		description = "File to write the schema to",
		name = "target",
		longName = "target-file"
	)
	public void writeToPath(String path) {
		target = new File(path);
	}

	@CLIOption(
			description = "Copy the schema to the system clipboard",
			name = "copy",
			longName = "use-desktop-clipboard",
			defaultValue = "true"
	)
	public void writeToPath(boolean useDesktopClipboard) {
		this.useDesktopClipboard = useDesktopClipboard;
	}

	@CLIMain
	public void refresh() throws Exception {
		ETLTestVM vm = TestCommand.getEtlTestVM();
		vm.installFeatures();

		Feature dbFeature = null;

		for (Feature feature : vm.getFeatures()) {
			if (feature.getFeatureName().equals("database")) {
				dbFeature = feature;
			}
		}

		RuntimeSupport runtimeSupport = vm.getRuntimeSupport();

		if (dbFeature == null) {
			println("Database feature is not available", AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.RED));
			return;
		}

		DatabaseFeatureModule databaseFeatureModule = (DatabaseFeatureModule) dbFeature;

		AtomicReference<TableMenuItem> tableMenuItem = new AtomicReference<>();

		String runIdentifier = runtimeSupport.getRunIdentifier();

		ConnectionIdMenu connectionIdMenu = new ConnectionIdMenu(databaseFeatureModule, this, runIdentifier, true);
		menu(connectionIdMenu, (selectedItem) -> {
			tableMenuItem.set((TableMenuItem) selectedItem);
		});

		connectionIdMenu.disposeAll();

		if (tableMenuItem.get() != null) {
			TableMenuItem tmi = tableMenuItem.get();

			DatabaseRuntimeSupport databaseRuntimeSupport = databaseFeatureModule.databaseRuntimeSupport();

			VariableContextImpl variableContext = new VariableContextImpl();

			databaseRuntimeSupport.refreshDatabaseMetadata(
					tmi.databaseConnection(),
					runIdentifier,
					databaseFeatureModule.getImplementation(tmi.databaseConnection().getId()),
					variableContext,
					databaseFeatureModule.getDatabaseMetaContext(),
					databaseFeatureModule.getJDBCClient()
			);

			Database database = databaseRuntimeSupport.getDatabase(
					tmi.databaseConnection(),
					variableContext
			);

			runtimeSupport.mapLocal().setCurrentlyProcessingTestClass(tmi.etlTestClass());
			runtimeSupport.mapLocal().setCurrentlyProcessingTestMethod(tmi.etlTestClass().getTestMethods().get(0));
			runtimeSupport.mapLocal().setCurrentlyProcessingTestOperation(tmi.etlTestClass().getTestMethods().get(0).getOperations().get(0));

			Table tbl = database.getCatalog(null).getSchema(tmi.schema()).getTable(tmi.table());
			databaseRuntimeSupport.loadTable(
					tbl,
					runIdentifier,
					databaseFeatureModule.getImplementation(tmi.databaseConnection().getId())
			);

			DataFileSchema newDfs = databaseRuntimeSupport.generateReferenceFileForTable(tbl);

			String dfsJsonString = newDfs.toJsonString();

			String targetName = (tableMenuItem.get().databaseConnection().getId() + "." + tableMenuItem.get().schema() + "." + tableMenuItem.get().table()).toLowerCase();

			if (useDesktopClipboard) {
				StringSelection stringSelection = new StringSelection(dfsJsonString);
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(stringSelection, null);

				println(targetName + " schema copied to system clipboard");
			}

			if (target != null) {
				FileUtils.write(target, dfsJsonString);
				println(targetName + " schema written to " + target.getAbsolutePath());
			}

			if (target == null && !useDesktopClipboard) {
				// write to console.
				println(targetName + " schema:");
				println(dfsJsonString);
			}
		}
	}
}
