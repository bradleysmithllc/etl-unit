package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@CLIEntry(
	description = "Displays meta information about a feature and / or operations",
	version = "1.0",
	nickName = "describe",
	commandGroup = "etlunit"
)
public class DescribeCommand extends ConsoleCommand {
	private String featureName = null;
	private String operationName = null;

	@CLIOption(
		description = "Optional feature name",
		name = "f",
		longName = "feature-name",
		unnamedArgumentOrdinal = 1
	)
	public void forFeature(String name) {
		featureName = name;
	}

	@CLIOption(
			description = "Optional operation name",
			name = "o",
			longName = "operation-name",
			unnamedArgumentOrdinal = 2
	)
	public void forOperation(String name) {
		operationName = name;
	}

	@CLIMain
	public void describe() throws IOException {
		// the first argument is the feature name
		if (featureName == null && operationName == null) {
			println("Must supply either a feature name or an operation name, or both.");
			return;
		}

		ServiceLocatorFeatureLocator locator = new ServiceLocatorFeatureLocator();

		List<Feature> list = locator.getFeatures(FeatureLocator.feature_type.external);

		if (list.size() != 0) {
			for (Feature feature : list) {
				if (featureName == null || feature.getFeatureName().equals(featureName)) {
					FeatureMetaInfo metaInfo = feature.getMetaInfo();

					String featureUsage = metaInfo.getFeatureUsage();
					if (featureUsage != null) {
						println(featureUsage);

						String config = metaInfo.getFeatureConfiguration();

						if (config != null) {
							println(config);
						} else {
							println("Feature does not provide configuration information");
						}

						Map<String, FeatureOperation> ops = metaInfo.getExportedOperations();

						if (ops != null) {
							println("exported operations:");

							for (Map.Entry<String, FeatureOperation> operationEntry : ops.entrySet()) {
								String operation = operationEntry.getKey();
								FeatureOperation featureOperation = operationEntry.getValue();

								if (operationName == null || operationName.equals(operation)) {
									String proto = featureOperation.getPrototype();
									String usage = featureOperation.getUsage();
									String descr = featureOperation.getDescription();

									if (proto == null && usage == null && descr == null) {
										println("Operation " + operationName + " does not provide any description");
									} else {
										if (proto == null) {
											proto = "NA";
										}
										if (usage == null) {
											usage = "NA";
										}
										if (descr == null) {
											descr = "NA";
										}

										println("Description:");
										println("\t" + descr);
										println("Prototype:");
										println("\t" + proto);
										println("Usage:");
										println("\t" + usage);
									}
								}
							}
						} else {
							println("Feature does not provide operation information");
						}
					} else {
						println("Feature " + featureName + " does not provide any description");
					}

					return;
				}
			}

			println("Feature " + featureName + " could not be located");
		} else {
			println("No features available");
		}
	}
}
