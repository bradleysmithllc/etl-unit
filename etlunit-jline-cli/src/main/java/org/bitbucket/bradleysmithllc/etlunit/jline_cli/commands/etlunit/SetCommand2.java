package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ETLTestVM;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOptionDescriptor;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.StringUtil;
import org.jline.utils.AttributedStyle;

import javax.inject.Inject;
import java.util.*;

@CLIEntry(
	description = "Sets or unsets runtime options (Deprecated version)",
	version = "1.0",
	nickName = "set2",
	commandGroup = "etlunit"
)
public class SetCommand2 extends ConsoleCommand {
	private String featureName;

	@CLIOption(
		description = "Optional name of the feature to (un)set an option for.",
		name = "fn",
		longName = "feature-name",
		unnamedArgumentOrdinal = 1
	)
	public void featureName(String name) {
		featureName = name;
	}

	private MapList<Feature, RuntimeOption> mapList;

	@CLIMain
	public void set() {
		try
		{
			ETLTestVM vm = TestCommand.getEtlTestVM();

			vm.addFeature(new AbstractFeature()
			{

				private String name = "mapListGrabber." + System.currentTimeMillis();

				@Inject
				public void receiveRuntimeSupport(MapList<Feature, RuntimeOption> ml)
				{
					mapList = ml;
				}

				@Override
				public String getFeatureName()
				{
					return name;
				}
			});

			vm.installFeatures();

			if (SetCommand.statelessOptions.size() > 0)
			{
				println("Overridden options:");

				for (Map.Entry<String, RuntimeOption> optionEntry : SetCommand.statelessOptions.entrySet())
				{
					RuntimeOption option = optionEntry.getValue();

					RuntimeOptionDescriptor descriptor = option.getDescriptor();
					println("\t" + option.getName() + " - " + descriptor.getDescription());

					switch (descriptor.getOptionType())
					{
						case bool:
							println("\t\tEnabled: " + option.isEnabled());
							break;
						case integer:
							println("\t\tValue: " + option.getIntegerValue());
							break;
						case string:
							println("\t\tValue: " + option.getStringValue());
							break;
					}
				}
			}
			else
			{
				println("No options are overridden");
			}

			Feature targetFeature = null;

			if (featureName != null)
			{
				for (Feature feature : mapList.keySet())
				{
					if (feature.getFeatureName().equals(featureName))
					{
						targetFeature = feature;
					}
				}

				if (targetFeature == null)
				{
					println("Invalid feature '" + featureName + "'");
				}
			}

			if (targetFeature == null)
			{
				println("Installed features:");

				int featureIndex = 0;
				Map<String, Feature> fmap = new HashMap<String, Feature>();

				// sort keys alphabetically
				List<Feature> featureArrayList = new ArrayList<Feature>(mapList.keySet());
				Collections.sort(featureArrayList, new Comparator<Feature>() {
					@Override
					public int compare(Feature o1, Feature o2)
					{
						return o1.getFeatureName().compareTo(o2.getFeatureName());
					}
				});

				for (Feature feature : featureArrayList)
				{
					String featureKey = String.valueOf(featureIndex++);

					fmap.put(featureKey, feature);

					printer().print("[").print(featureKey, AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.CYAN)).print("] - ").println(feature.getFeatureName(), AttributedStyle.DEFAULT.italic());
				}

				printer().print("[").print("x", AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.RED)).println("] - cancel");

				if (SetCommand.statelessOptions.size() > 0)
				{
					printer().print("[").print("r", AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.YELLOW)).println("] - reset options");
				}

				while (targetFeature == null)
				{
					String res = readWithPrompt("Select a feature:").trim();

					if (res.equalsIgnoreCase("x"))
					{
						return;
					}
					else if (SetCommand.statelessOptions.size() != 0 && res.equalsIgnoreCase("r"))
					{
						writeLine("Resetting in-memory options.");
						SetCommand.statelessOptions.clear();
						return;
					}

					targetFeature = fmap.get(res);

					if (targetFeature == null)
					{
						writeLine("Invalid selection - '" + res + "'");
					}
				}
			}

			RuntimeOption ro = null;

			for (Map.Entry<Feature, List<RuntimeOption>> featureListEntry : mapList.entrySet())
			{
				Feature featureKey = featureListEntry.getKey();
				String thisFeatureName = featureKey.getFeatureName();

				if (targetFeature != featureKey)
				{
					continue;
				}

				writeLine(thisFeatureName);

				int roIndex = 0;
				Map<String, RuntimeOption> romap = new HashMap<String, RuntimeOption>();

				// sort options alphabetically
				List<RuntimeOption> runtimeOptions = featureListEntry.getValue();
				Collections.sort(runtimeOptions, new Comparator<RuntimeOption>() {
					@Override
					public int compare(RuntimeOption o1, RuntimeOption o2)
					{
						return o1.getName().compareTo(o2.getName());
					}
				});

				boolean first = true;

				for (RuntimeOption option : runtimeOptions)
				{
					if (first)
					{
						first = false;
					}
					else
					{
						writeLine();
					}

					String roKey = String.valueOf(roIndex++);

					romap.put(roKey, option);

					RuntimeOptionDescriptor descriptor = option.getDescriptor();

					RuntimeOption overridenValue = SetCommand.statelessOptions.get(option.getName());

					String enDef = "";
					String enCurr = null;

					switch (descriptor.getOptionType())
					{
						case bool:
							enDef = String.valueOf(option.isEnabled());

							if (overridenValue != null)
							{
								enCurr = String.valueOf(overridenValue.isEnabled());
							}

							break;
						case integer:
							enDef = String.valueOf(option.getIntegerValue());

							if (overridenValue != null)
							{
								enCurr = String.valueOf(overridenValue.getIntegerValue());
							}

							break;
						case string:
							enDef = option.getStringValue();

							if (overridenValue != null)
							{
								enCurr = String.valueOf(overridenValue.getStringValue());
							}

							break;
					}

					String description = descriptor.getDescription();

					List<String> desclist = StringUtil.wrapTextToList(description, 60);

					printer().print("\t[")
							.print(roKey, AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.CYAN))
							.print("] - ")
							.print(descriptor.getName(), AttributedStyle.DEFAULT.italic().foreground(AttributedStyle.BLUE))
							.print(": [")
							.print(enDef, AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.GREEN))
							.print("]")
							.println((enCurr != null ? (": " + enCurr) : ""));

					for (String line : desclist)
					{
						writeLine("                  " + line);
					}
				}

				printer().print("[").print("x", AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.RED)).println("] - cancel");

				while (ro == null)
				{
					String res = readWithPrompt("Select an option:").trim();

					if (res.equalsIgnoreCase("x"))
					{
						return;
					}

					ro = romap.get(res);

					if (ro == null)
					{
						writeLine("Invalid selection - '" + res + "'");
					}
				}
			}

			writeLine("Updating - '" + ro.getName() + "'");

			// prompt for the new value
			switch (ro.getDescriptor().getOptionType())
			{
				case bool:
					boolean defBo = ro.getDescriptor().getDefaultBooleanValue();
					SetCommand.statelessOptions.put(ro.getName(), option(new RuntimeOption(ro.getName(), getBool(defBo)), ro));

					break;
				case integer:
					SetCommand.statelessOptions.put(ro.getName(), option(new RuntimeOption(ro.getName(), getInt(ro.getDescriptor().getDefaultIntegerValue())), ro));
					break;
				case string:
					String in = getString(ro.getDescriptor().getDefaultStringValue());
					SetCommand.statelessOptions.put(ro.getName(), option(new RuntimeOption(ro.getName(), in), ro));
					break;
			}
		}
		catch (Exception exc)
		{
			writeLine(exc.toString());
			exc.printStackTrace(System.out);
		}

		return;
	}

	private RuntimeOption option(RuntimeOption runtimeOption, RuntimeOption ro)
	{
		runtimeOption.setDescriptor(ro.getDescriptor());
		runtimeOption.setFeature(ro.getFeature());

		return runtimeOption;
	}

	private boolean getBool(boolean defBo)
	{
		while (true)
		{
			String in = readWithPrompt("\tEnable " + (defBo ? "Y/n" : "y/N")+ ": ").trim();

			if (in.equalsIgnoreCase("Y"))
			{
				return true;
			}
			else if (in.equalsIgnoreCase("N"))
			{
				return false;
			}
			else if (in.equals(""))
			{
				return defBo;
			}

			writeLine("Invalid boolean - '" + in + "' - please enter 'y' or 'n' or blank for the default");
		}
	}

	private String getString(String defBo)
	{
		String in = readWithPrompt("\tValue [" + defBo + "]: ").trim();

		if (in.equals(""))
		{
			return defBo;
		}

		return in;
	}

	private int getInt(int def)
	{
		while (true)
		{
			String in = readWithPrompt("\tInteger Value [" + def + "]: ").trim();

			if (in.equals(""))
			{
				return def;
			}

			try
			{
				return Integer.parseInt(in);
			}
			catch(NumberFormatException exc)
			{
				writeLine("Invalid int - '" + in + "' - please enter a valid integer or blank for the default");
			}
		}
	}
}
