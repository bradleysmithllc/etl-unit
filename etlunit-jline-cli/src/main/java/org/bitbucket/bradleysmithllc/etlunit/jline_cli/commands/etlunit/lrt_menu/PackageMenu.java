package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.lrt_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypePackage;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.MenuItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PackageMenu extends AbstractMenu<MenuItem> {
	private final List<MenuItem> items = new ArrayList<>();

	public PackageMenu(List<ReferenceFileTypePackage> packList) {
		List<ReferenceFileTypePackage> copy = new ArrayList<>(packList);
		Collections.sort(copy, (p1, p2) -> {
			return p1.getName().compareTo(p2.getName());
		});

		for (ReferenceFileTypePackage pack : copy) {
			items.add(new PackageMenuItem(pack));
		}
	}

	@Override
	public String title() {
		return "Available packages";
	}

	@Override
	public String prompt() {
		return "Select package";
	}

	@Override
	public List<MenuItem> items() {
		return items;
	}

	@Override
	public Menu choose(MenuItem item) throws Exception {
		PackageMenuItem pmi = (PackageMenuItem) item;

		//println("Exploring {" + pck.getName() + "}");

		return new RefTypeMenu(pmi.referenceFileTypePackage());
	}
}
