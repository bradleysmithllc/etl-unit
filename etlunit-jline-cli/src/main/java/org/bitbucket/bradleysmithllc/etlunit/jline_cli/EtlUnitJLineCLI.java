package org.bitbucket.bradleysmithllc.etlunit.jline_cli;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.specification.JLineCLIParser;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.specification.ParseException;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.bitbucket.bradleysmithllc.java_cl_parser.InvalidCLIEntryException;
import org.bitbucket.bradleysmithllc.java_cl_parser.UsageException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Size;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class EtlUnitJLineCLI {
	public static void main(String... argv) {
		Terminal terminal = null;
		try {
			terminal = TerminalBuilder.builder().build();
		} catch (IOException e) {
			throw new IllegalStateException("Could not create terminal");
		}

		if (terminal.getWidth() == 0 || terminal.getHeight() == 0) {
			terminal.setSize(new Size(120, 40));
		}

		Thread executeThread = Thread.currentThread();
		terminal.handle(Terminal.Signal.INT, signal -> executeThread.interrupt());

		CommandLoader commandLoader;
		try {
			commandLoader = new CommandLoader();
		} catch (Exception e) {
			throw new IllegalStateException("Could not load commands", e);
		}

		LineReader reader = LineReaderBuilder.builder()
				.terminal(terminal)
				.completer(commandLoader.completer())
				.highlighter(new EtlUnitHighlighter())
				.variable(LineReader.HISTORY_FILE, Paths.get(System.getProperty("user.home") + "/.etlunit_history"))
				.build();

		String promptString = new AttributedString("etl-unit %% ", 0, 12, AttributedStyle.DEFAULT.foreground(AttributedStyle.CYAN)).toAnsi();

		while (true) {
			String line = reader.readLine(promptString);

			ParsedLine pl = null;
			try {
				pl = new JLineCLIParser(line).parseLine();
			} catch (ParseException e) {
				System.out.println("Invalid command " + e.getMessage());
				continue;
			}

			if (pl.command() == null) {
				continue;
			} else {
				try {
					CommandReference reference = commandLoader.commandByNickname(pl.command());

					if (reference == null) {
						terminal.writer().println(new AttributedStringBuilder().append("Unknown command name ", AttributedStyle.DEFAULT.foreground(AttributedStyle.RED)).append("'").append(pl.command(), AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.RED)).append("'").toAnsi());
						continue;
					}

					Class cl = reference.entryImplementation();

					// create and invoke
					EtlUnitShellCommand shellCommand = (EtlUnitShellCommand) cl.newInstance();
					shellCommand.context(new CommandContext(){
						@Override
						public LineReader lineReader() {
							return reader;
						}

						@Override
						public CommandLoader commandLoader() {
							return commandLoader;
						}
					});

					List<String> commands = new ArrayList<>();

					for (ParsedLineComponent c : pl.components()) {
						String text = c.text();

						switch (c.type()) {
							case quoted_argument:
								// strip quotes off
								int endIndex = text.endsWith("'") ? text.length() - 1 : text.length();
								text = text.substring(1, endIndex);
							case argument:
							case flag:
								commands.add(text);
								break;
						}
					}

					try {
						CommonsCLILauncher.mainWithInstance(shellCommand, commands.toArray(new String[commands.size()]));
					} catch (UsageException exc) {
						if (exc.getCause() != null) {
							AttributedStringBuilder attrBuilder = new AttributedStringBuilder();

							String exceptionName = exc.getCause().getClass().getSimpleName();
							attrBuilder.append(exceptionName, AttributedStyle.DEFAULT.foreground(AttributedStyle.RED));
							attrBuilder.append(": ");
							attrBuilder.append(exc.getCause().getMessage(), AttributedStyle.DEFAULT.foreground(AttributedStyle.MAGENTA).italic());
							terminal.writer().println(attrBuilder.toAttributedString().toAnsi());
						} else {
							terminal.writer().println(exc.getFormattedUsageStatement());
						}
					} catch (Exception exc) {
						exc.printStackTrace(terminal.writer());
					}
				} catch (IllegalArgumentException exc) {
					terminal.writer().println(
							new AttributedStringBuilder()
									.append("Command '", AttributedStyle.DEFAULT.italic().foreground(AttributedStyle.RED))
									.append(pl.command(), AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.MAGENTA))
									.append("' not found", AttributedStyle.DEFAULT.italic().foreground(AttributedStyle.RED))
									.toAttributedString().toAnsi()
					);
				} catch (InstantiationException | IllegalAccessException exc) {
				}
			}
		}
	}
}
