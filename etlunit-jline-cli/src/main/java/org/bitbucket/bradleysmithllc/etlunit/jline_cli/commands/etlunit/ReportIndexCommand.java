package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.maven.ETLUnitMojo;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;

import java.awt.*;
import java.io.File;

@CLIEntry(
	description = "Open the index report from the last run",
	version = "1.0",
	nickName = "report-index",
	commandGroup = "etlunit"
)
public class ReportIndexCommand extends ConsoleCommand {
	@CLIMain
	public void report() {
		try
		{
			File basedir = new File(".");

			File src = ETLUnitMojo.getReportsDirectoryRoot(basedir);

			File reportIndex;

			reportIndex = new File(src, "html-reports/reports.html").getCanonicalFile();

			if (reportIndex.exists())
			{
				if (Desktop.isDesktopSupported())
				{
					Desktop dt = Desktop.getDesktop();

					dt.browse(reportIndex.toURI());
				}
				else
				{
					writeLine("Your platform does not support the Java Desktop - don't ask me why.");
				}
			}
			else
			{
				writeLine("No report file found (" + reportIndex.getAbsolutePath() + ")");
			}
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
			writeLine(exc.toString());
		}
	}
}
