package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.system;

/*
 * #%L
 * etlunit-spring-shell-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@CLIEntry(
	description = "Lists contents of current or specified directory",
	version = "1.0",
	nickName = "ls",
	commandGroup = "system"
)
public class LsCommand extends ConsoleCommand {
	private String path;

	@CLIOption(
		description = "Path to list, relative to the current working directory.  Default path is '.'",
		name = "p",
		longName = "path",
		unnamedArgumentOrdinal = 1,
		defaultValue = "."
	)
	public void path(String path) {
		this.path = path;
	}

	@CLIMain
	public void ls() throws IOException {
		File file = makeFilePath(path);

		println(file.getCanonicalPath() + ":");
		println();

		File[] a = file.listFiles();

		List<File> fileList = Collections.emptyList();

		if (a != null) {
			fileList = new ArrayList<>(Arrays.asList(a));
		}

		Collections.sort(fileList, (fileA, fileB) -> {
			// directory first,
			if (fileA.isDirectory() && !fileB.isDirectory()) {
				return -1;
			} else if (!fileA.isDirectory() && fileB.isDirectory()) {
				return 1;
			}

			// now alphabetize
			return fileA.getName().compareTo(fileB.getName());
		});

		for (File subFile : fileList) {
			StringBuilder out = new StringBuilder();

			if (subFile.isDirectory()) {
				out.append('d');
			} else {
				out.append('-');
			}

			out.append("   ").append(subFile.getName());

			println(out.toString());
		}
	}
}
