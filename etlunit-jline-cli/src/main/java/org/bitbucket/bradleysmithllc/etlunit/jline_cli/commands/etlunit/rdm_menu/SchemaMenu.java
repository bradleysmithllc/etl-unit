package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.TerminalInterface;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.util.jdbc.JdbcVisitors;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SchemaMenu extends AbstractMenu<DatabaseMenuItem> {
	private final List<DatabaseMenuItem> schemaItems = new ArrayList<>();
	private final DatabaseConnection databaseConnection;

	public SchemaMenu(
			DatabaseFeatureModule databaseFeatureModule,
			TerminalInterface terminalInterface,
			DatabaseConnection databaseConnection,
			String runIdentifier
	) throws Exception {
		this.databaseConnection = databaseConnection;

		//terminalInterface.printer().print("Preparing database '").print(databaseConnection.getId()).println("' for introspection");

		String s = "@Database(id: '" + databaseConnection.getId() + "', mode: '" + runIdentifier + "') class t { @Test extract() {extract(connection-id: '" + databaseConnection.getId() + "', target-schema: 'PUBLIC', target-table: 'TABLE_WITH_IDENTITY');}}";

		List<ETLTestClass> cl = ETLTestParser.load(s);
		VariableContextImpl variableContext = new VariableContextImpl();
		ETLTestClass etlTestClass = cl.get(0);
		databaseFeatureModule.getListener().begin(etlTestClass, variableContext, 0);
		databaseFeatureModule.getListener().begin(etlTestClass.getTestMethods().get(0), variableContext, 0);

		DatabaseImplementation databaseImplemetation = databaseFeatureModule.getImplementation(databaseConnection.getId());

		Connection conn = databaseImplemetation.getConnection(databaseConnection, runIdentifier);

		JdbcVisitors.withPersistentConnection(conn).withSchemas((metadataSchema -> {
			if (metadataSchema.schemaName().equalsIgnoreCase("information_schema")) {
				return;
			}

			schemaItems.add(new SchemaMenuItem(
					databaseFeatureModule,
					databaseConnection,
					terminalInterface,etlTestClass,
					conn, metadataSchema.catalogName(), metadataSchema.schemaName()
			));
		})).dispose();

		Collections.sort(schemaItems, (scha, schb) -> {
			return scha.name().compareTo(schb.name());
		});
	}

	@Override
	public boolean includeCancel() {
		return true;
	}

	@Override
	public boolean includeReset() {
		return false;
	}

	@Override
	public String title() {
		return "Schemas in database [" + databaseConnection.getId() + "]";
	}

	@Override
	public String prompt() {
		return "Select schema";
	}

	@Override
	public List<DatabaseMenuItem> items() {
		return schemaItems;
	}

	@Override
	public Menu choose(DatabaseMenuItem item) throws Exception {
		return item.menu();
	}

	public void disposeAll() throws SQLException {
		for (DatabaseMenuItem item : schemaItems) {
			item.disposeAll();
		}
	}
}
