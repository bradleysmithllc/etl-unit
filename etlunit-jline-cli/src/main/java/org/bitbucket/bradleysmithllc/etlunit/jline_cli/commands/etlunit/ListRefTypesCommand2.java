package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.*;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;

import java.io.IOException;
import java.util.*;

@CLIEntry(
	description = "Lists all reference type catalogs",
	version = "1.0",
	nickName = "list-reference-xypes",
	commandGroup = "etlunit"
)
public class ListRefTypesCommand2 extends ConsoleCommand {
	private static interface Selector<T>
	{
		List<T> selectFrom();
		String describe(T t);
		String name(T t);
		String getPrompt();
	}

	@CLIMain
	public void listReferenceTypes() throws Exception {
		ReferenceFileTypeManagerImpl manager = new ReferenceFileTypeManagerImpl();
		try {
			RuntimeSupport rs = TestCommand.getEtlTestVM().getRuntimeSupport();
			manager.receiveRuntimeSupport(rs);
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<ReferenceFileTypeCatalog> catalogs = manager.locateReferenceTypeCatalogs();

		if (catalogs.size() == 0)
		{
			println("No catalogs available.");
			return;
		}

		println("Available packages:");

		final Map<String, String> packs = new HashMap<String, String>();
		final List<ReferenceFileTypePackage> packList = new ArrayList<ReferenceFileTypePackage>();

		for (ReferenceFileTypeCatalog catalog : catalogs)
		{
			for (Map.Entry<String, ReferenceFileTypePackage> _package_ : catalog.getReferenceFileTypePackages().entrySet())
			{
				ReferenceFileTypePackage value = _package_.getValue();
				String name = value.getName();

				if (!packs.containsKey(name))
				{
					packs.put(name, "");
					packList.add(value);
				}
			}
		}

		ReferenceFileTypePackage package_ = menu(this, new Selector<ReferenceFileTypePackage>() {
			@Override
			public List<ReferenceFileTypePackage> selectFrom() {
				return packList;
			}

			@Override
			public String describe(ReferenceFileTypePackage referenceFileTypePackage) {
				return referenceFileTypePackage.getDescription();
			}

			@Override
			public String name(ReferenceFileTypePackage referenceFileTypePackage) {
				return referenceFileTypePackage.getName();
			}

			@Override
			public String getPrompt() {
				return "Package";
			}
		});

		final ReferenceFileTypePackage pck = package_;

		if (pck == null)
		{
			return;
		}

		println("Exploring {" + pck.getName() + "}");

		ReferenceFileType fileType = menu(this, new Selector<ReferenceFileType>() {
			@Override
			public List<ReferenceFileType> selectFrom() {
				return new ArrayList<ReferenceFileType>(pck.getReferenceFileTypesById().values());
			}

			@Override
			public String describe(ReferenceFileType referenceFileTypePackage) {
				referenceFileTypePackage.loadFromResource();
				return referenceFileTypePackage.getDescription();
			}

			@Override
			public String name(ReferenceFileType referenceFileType) {
				return referenceFileType.getId();
			}

			@Override
			public String getPrompt() {
				return "File Type";
			}
		});

		if (fileType == null)
		{
			return;
		}

		println("Exploring {" + fileType.getId() + "}");

		// write out the name, id and description
		println("                  id: " + fileType.getId());
		println("                name: " + fileType.getName());
		println("         description: " + fileType.getDescription());

		if (fileType.getDefaultVersion() != null)
		{
			println("     default version: " + fileType.getDefaultVersion());
		}
		if (fileType.getDefaultClassifier() != null)
		{
			println("  default classifier: " + fileType.getDefaultClassifier());
		}

		// write out all versions
		if (fileType.hasVersions())
		{
			println("  available versions: ");
			for (String version : fileType.getVersions())
			{
				println("        ");
				println(version);
				println(" {");
				println(ReferenceFileTypeRef.refWithIdAndVersion(fileType.getId(), version).toRefString());
				println("}");
				println();
				// TODO: Explode column info here
			}
		}

		// write out classifiers
		Map<String, ReferenceFileTypeClassifier> classifiers = fileType.getClassifiers();
		if (classifiers.size() > 0)
		{
			println("  available classifiers: ");
			for (Map.Entry<String, ReferenceFileTypeClassifier> classEntry : classifiers.entrySet())
			{
				println("       classifier: " + classEntry.getKey());
				ReferenceFileTypeClassifier value = classEntry.getValue();
				println("        full name: " + fileType.getId() + "~" + classEntry.getKey());
				println("      description: " + value.getDescription());

				if (value.getDefaultVersion() != null)
				{
					println("     default version: " + value.getDefaultVersion());
				}

				if (value.hasVersions())
				{
					println("  available versions: ");
					for (String version : value.getVersions())
					{
						println("        ");
						println(version);
						println(" {");
						println(ReferenceFileTypeRef.refWithIdAndVersion(fileType.getId(), version).toRefString());
						println("}");
						println();
						// TODO: Explode column info here
					}
				}
			}
		}

		return;
	}

	private static <T extends Comparable<T>> T menu(ListRefTypesCommand2 command, Selector<T> selector)
	{
		while (true)
		{
			command.println();

			List<T> objects = selector.selectFrom();

			Collections.sort(objects);

			for (int off = 0; off < objects.size(); off++)
			{
				command.println(off + ": " + selector.name(objects.get(off)));
				command.println("       " + selector.describe(objects.get(off)));
				command.println();
			}

			command.println();
			command.println("x: Cancel");
			command.println();

			String res = command.readWithPrompt(selector.getPrompt() + ": ");

			if (res.equals("x"))
			{
				return null;
			}

			try
			{
				int offset = Integer.parseInt(res);

				if (offset < 0 || offset >= objects.size())
				{
					command.println("Please enter a valid offset.");
				}
				else
				{
					return objects.get(offset);
				}
			}
			catch(NumberFormatException exc)
			{
				command.println("NaN.");
			}
		}
	}
}
