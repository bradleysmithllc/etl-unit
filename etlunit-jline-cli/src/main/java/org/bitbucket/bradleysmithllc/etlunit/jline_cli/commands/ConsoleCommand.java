package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands;

/*
 * #%L
 * etlunit-spring-shell-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang.StringUtils;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.CommandContext;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.CommandLoader;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.EtlUnitShellCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.StringUtil;
import org.jline.reader.LineReader;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

import java.io.File;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;

public class ConsoleCommand implements EtlUnitShellCommand, TerminalInterface {
	protected LineReader lineReader;
	protected CommandLoader commandLoader;

	protected int terminalWidth() {
		return lineReader.getTerminal().getWidth();
	}

	protected int terminalHeight() {
		return lineReader.getTerminal().getHeight();
	}

	protected void writeLine(String line)
	{
		println(line);
	}

	protected void write(String line)
	{
		print(line);
	}

	protected void writeLine()
	{
		println();
	}

	protected String readLine() {
		return readWithPrompt(null);
	}

	protected String readWithPrompt(String s)
	{
		try {
			lineReader.getVariables().put(LineReader.DISABLE_HISTORY, Boolean.TRUE);
			return lineReader.readLine(s);
		} finally {
			lineReader.getVariables().remove(LineReader.DISABLE_HISTORY);
		}
	}

	protected void println() {
		println("");
	}

	protected void println(String s) {
		lineReader.getTerminal().writer().println(s);
		lineReader.getTerminal().writer().flush();
	}

	protected void print(String s) {
		lineReader.getTerminal().writer().print(s);
		lineReader.getTerminal().writer().flush();
	}

	public Printer printer() {
		return new Printer(){

			@Override
			public Printer println() {
				ConsoleCommand.this.println();
				return this;
			}

			@Override
			public Printer println(String text) {
				ConsoleCommand.this.println(text);
				return this;
			}

			@Override
			public Printer print(String text) {
				ConsoleCommand.this.print(text);
				return this;
			}

			@Override
			public Printer println(String s, AttributedStyle style) {
				ConsoleCommand.this.println(s, style);
				return this;
			}

			@Override
			public Printer println(AttributedString attr) {
				ConsoleCommand.this.println(attr);
				return this;
			}

			@Override
			public Printer print(String s, AttributedStyle style) {
				ConsoleCommand.this.print(s, style);
				return this;
			}

			@Override
			public Printer print(AttributedString attr) {
				ConsoleCommand.this.print(attr);
				return this;
			}
		};
	}

	protected void println(String s, AttributedStyle style) {
		println(new AttributedStringBuilder().append(s, style).toAttributedString());
	}

	protected void println(AttributedString attr) {
		lineReader.getTerminal().writer().println(attr.toAnsi());
		lineReader.getTerminal().writer().flush();
	}

	protected void print(String s, AttributedStyle style) {
		print(new AttributedStringBuilder().append(s, style).toAttributedString());
	}

	protected void print(AttributedString attr) {
		lineReader.getTerminal().writer().print(attr.toAnsi());
		lineReader.getTerminal().writer().flush();
	}

	protected static File makeFilePath(String path) {
		if (path.startsWith("~")) {
			return new File(path.replace("~", System.getProperty("user.home")));
		}
		else if (path.startsWith("/")) {
			return new File(path);
		} else {
			return new File(System.getProperty("user.dir"), path);
		}
	}

	@Override
	public void context(CommandContext context) {
		lineReader = context.lineReader();
		commandLoader = context.commandLoader();
	}

	static MenuItem back = new AbstractMenuItem(){
		@Override
		public String name() {
			return "back";
		}

		@Override
		public String description() {
			return "Back";
		}
	};
	static MenuItem reset = new AbstractMenuItem(){
		@Override
		public String name() {
			return "reset";
		}

		@Override
		public String description() {
			return "Reset";
		}
	};
	static MenuItem cancel = new AbstractMenuItem(){
		@Override
		public String name() {
			return "cancel";
		}

		@Override
		public String description() {
			return "Cancel";
		}
	};

	protected <T extends MenuItem> void menu(
			Menu<T> menu,
			Consumer<T> terminalConsumer
	) throws Exception {
		menu(menu, terminalConsumer, () -> {});
	}

	protected <T extends MenuItem> void menu(
			Menu<T> menu,
			Consumer<T> terminalConsumer,
			Runnable resetOption
	) throws Exception {
		Stack<Menu<T>> menuStack = new Stack<>();

		Menu<T> currentMenu = menu;

		while (true) {
			MenuItem selection = presentMenu(currentMenu, menuStack.size());

			if (selection == cancel) {
				return;
			} else if (selection == back) {
				currentMenu = menuStack.pop();
			} else if (selection == reset) {
				resetOption.run();
				return;
			} else {
				menuStack.push(currentMenu);
				currentMenu = currentMenu.choose((T) selection);

				if (currentMenu == null) {
					// terminal
					terminalConsumer.accept((T) selection);
					return;
				}
			}
		}
	}

	private <T extends MenuItem> MenuItem presentMenu(Menu<T> menu, int nestedDepth) {
		List<T> items = menu.items();

		int maxNameLength = items.stream().reduce(6, (maxLength, item) -> {
			return Math.max(maxLength, item.name().length());
		}, Math::max);

		int digits = String.valueOf(items.size()).length();
		String prefix = StringUtils.rightPad("", nestedDepth, "   ");
		int maxLineLength = maxNameLength + prefix.length() + 2 + digits + 3;

		if (menu.title() != null) {
			println(menu.title());
		}

		for (int index = 0; index < items.size(); index++) {
			T menuItem = items.get(index);
			String name = menuItem.name();

			String formattedName = StringUtils.rightPad(name, maxNameLength + 1, " ");

			Printer printer = printer()
					.print(prefix).print("[")
					.print(StringUtils.rightPad(String.valueOf(index + 1), digits, " "), AttributedStyle.DEFAULT.foreground(AttributedStyle.CYAN))
					.print("] - ")
					.print(formattedName, AttributedStyle.DEFAULT.italic().foreground(AttributedStyle.GREEN));

			if (menuItem.contextInfo() != null) {
				printer.print(": ").println(menuItem.contextInfo());
			} else {
				printer.println();
			}

			if (menuItem.description() != null) {
				List<String> desclist = StringUtil.wrapTextToList(menuItem.description(), 60);

				for (String line : desclist)
				{
					printer.println(prefix + "   " + line, AttributedStyle.DEFAULT.italic());
				}

				printer.println();
			}
		}

		if (menu.includeReset()) {
			String formattedName = reset.name();
			printer()
					.print(prefix).print("[")
					.print(StringUtils.rightPad("r", digits, " "), AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW))
					.print("] - ")
					.println(formattedName);
		}

		if (nestedDepth > 0) {
			String formattedName = back.name();
			printer()
					.print(prefix).print("[")
					.print(StringUtils.rightPad("b", digits, " "), AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN))
					.print("] - ")
					.println(formattedName);
		}

		if (menu.includeCancel()) {
			String formattedName = cancel.name();
			printer()
					.print(prefix).print("[")
					.print(StringUtils.rightPad("x", digits, " "), AttributedStyle.DEFAULT.foreground(AttributedStyle.RED))
					.print("] - ")
					.println(formattedName);
		}
		println(StringUtils.rightPad("", maxLineLength, "-"));
		println("");

		while (true) {
			String val = readWithPrompt(menu.prompt() + ": ");

			if (val.equals("x")) {
				return cancel;
			} else if (val.equals("b")) {
				return back;
			} else if (val.equals("r")) {
				return reset;
			}

			try {
				int index = Integer.parseInt(val);

				if (index <= 0 || index > items.size())
				{
					println("Invalid index '" + index + "'");
				}

				return items.get(index - 1);
			} catch (NumberFormatException exc) {
				println("Invalid option '" + val + "'");
			}
		}
	}
}
