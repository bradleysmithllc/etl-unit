package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.TerminalInterface;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenuItem;

import java.sql.SQLException;

public abstract class DatabaseMenuItem extends AbstractMenuItem {
	protected final DatabaseFeatureModule databaseFeatureModule;
	protected final DatabaseConnection databaseConnection;
	protected final TerminalInterface terminalInterface;

	private final type type;

	public abstract void disposeAll() throws SQLException;

	enum type {
		connectionId,
		schema,
		table
	}

	public DatabaseMenuItem(
			DatabaseFeatureModule databaseFeatureModule,
			DatabaseConnection value,
			TerminalInterface terminalInterface,
			type type
	) {
		this.databaseFeatureModule = databaseFeatureModule;
		this.databaseConnection = value;
		this.terminalInterface = terminalInterface;
		this.type = type;
	}

	public DatabaseConnection databaseConnection() {
		return databaseConnection;
	}

	@Override
	public String name() {
		return databaseConnection.getId();
	}

	public abstract Menu<DatabaseMenuItem> menu() throws Exception;
}
