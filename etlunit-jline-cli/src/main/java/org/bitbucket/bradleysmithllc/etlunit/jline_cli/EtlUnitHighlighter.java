package org.bitbucket.bradleysmithllc.etlunit.jline_cli;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.specification.JLineCLIParser;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.specification.ParseException;
import org.jline.reader.Highlighter;
import org.jline.reader.LineReader;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class EtlUnitHighlighter implements Highlighter {
	@Override
	public AttributedString highlight(LineReader reader, String buffer) {
		try {
			ParsedLine pl = new JLineCLIParser(buffer).parseLine();

			// convert into ansi attribution
			if (pl.command() != null) {
				List<AttributedString> strList = new ArrayList<>();
				AttributedString lineString = new AttributedString(pl.command() , 0, pl.command().length(), AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN).bold());

				strList.add(lineString);

				// now append all components
				for (ParsedLineComponent comp : pl.components()) {
					switch (comp.type()) {
						case space:
							lineString = new AttributedString(comp.text() , 0, comp.text().length(), AttributedStyle.DEFAULT);
							break;
						case quoted_argument:
							lineString = new AttributedString(comp.text(), 0, comp.text().length(), AttributedStyle.DEFAULT.foreground(AttributedStyle.BLUE));
							break;
						case argument:
							lineString = new AttributedString(comp.text(), 0, comp.text().length(), AttributedStyle.DEFAULT.foreground(AttributedStyle.RED));
							break;
						case flag:
							lineString = new AttributedString(comp.text() , 0, comp.text().length(), AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.YELLOW));
							break;
						case classSpec:
							lineString = new AttributedString(comp.text() , 0, comp.text().length(), AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.GREEN));
							break;
						case methodSpec:
							lineString = new AttributedString(comp.text() , 0, comp.text().length(), AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.MAGENTA));
							break;
						case operationSpec:
							lineString = new AttributedString(comp.text() , 0, comp.text().length(), AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.CYAN));
							break;
						case packageSpec:
							lineString = new AttributedString(comp.text() , 0, comp.text().length(), AttributedStyle.DEFAULT.bold().foreground(AttributedStyle.BLUE));
							break;
					}

					strList.add(lineString);
				}

				return AttributedString.join(new AttributedString("", 0, 0, AttributedStyle.DEFAULT), strList);
			}
		} catch (ParseException e) {
		}

		return new AttributedStringBuilder().append(buffer).toAttributedString();
	}

	@Override
	public void setErrorPattern(Pattern errorPattern) {
	}

	@Override
	public void setErrorIndex(int errorIndex) {
	}
}
