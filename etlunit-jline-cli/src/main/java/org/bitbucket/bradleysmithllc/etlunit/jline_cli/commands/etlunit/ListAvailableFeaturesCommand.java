package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureLocator;
import org.bitbucket.bradleysmithllc.etlunit.feature.ServiceLocatorFeatureLocator;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;

import java.io.File;
import java.util.List;

@CLIEntry(
	description = "Lists all available features",
	version = "1.0",
	nickName = "list-available-features",
	commandGroup = "etlunit"
)
public class ListAvailableFeaturesCommand extends ConsoleCommand {
	@CLIMain
	public void list() throws Exception {
		List<Feature> list = getAvailableFeatures();

		List<String> installed = ListInstalledFeaturesCommand.getInstalledFeatures(new File("."));

		if (list.size() != 0) {
			println("Available features:");

			for (Feature feature : list) {
				if (installed == null || !installed.contains(feature.getFeatureName()))
				{
					println("\t" + feature.getFeatureName());
				}
			}
		} else {
			println("No features available");
		}
	}

	public static List<Feature> getAvailableFeatures() throws Exception {
		ServiceLocatorFeatureLocator locator = new ServiceLocatorFeatureLocator(ListAvailableFeaturesCommand.class.getClassLoader());

		return locator.getFeatures(FeatureLocator.feature_type.external);
	}
}
