package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.module_signer_mojo.IMavenProject;
import org.bitbucket.bradleysmithllc.module_signer_mojo.ModuleVersions;

import java.util.List;

@CLIEntry(
	description = "Prints version numbers for all modules",
	version = "1.0",
	nickName = "versions",
	commandGroup = "etlunit"
)
public class VersionsCommand extends ConsoleCommand {
	@CLIMain
	public void versions() {
		List<IMavenProject> mvlist = new ModuleVersions().getAvailableVersions();

		for (IMavenProject mv : mvlist)
		{
			writeLine("--------------------------------------------------------------");
			writeLine(
					mv.getMavenGroupId()
							+ "."
							+ mv.getMavenArtifactId());
			writeLine(mv.getMavenVersionNumber());
			writeLine("Git Branch:" + mv.getGitBranch());
			writeLine("Branch clean: " + mv.isGitBranchClean());
			writeLine("Git Hash: " + mv.getGitBranchChecksum());
		}
	}
}
