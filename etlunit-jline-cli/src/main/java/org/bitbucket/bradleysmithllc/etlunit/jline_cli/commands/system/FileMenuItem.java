package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.system;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenuItem;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.MenuItem;

import java.io.File;

public class FileMenuItem extends AbstractMenuItem {
	private final File item;

	public FileMenuItem(File item) {
		this.item = item;
	}

	public boolean isLeaf() {
		return item.isFile();
	}

	@Override
	public String name() {
		return item.getName();
	}

	@Override
	public String description() {
		return item.isDirectory() ? "dir" : "file";
	}
}
