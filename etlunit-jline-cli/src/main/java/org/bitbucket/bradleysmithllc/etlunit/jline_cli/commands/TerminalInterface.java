package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;

public interface TerminalInterface {
	interface Printer {
		Printer println();

		Printer println(String text);

		Printer print(String text);

		Printer println(String s, AttributedStyle style);

		Printer println(AttributedString attr);

		Printer print(String s, AttributedStyle style);

		Printer print(AttributedString attr);
	}

	Printer printer();
}
