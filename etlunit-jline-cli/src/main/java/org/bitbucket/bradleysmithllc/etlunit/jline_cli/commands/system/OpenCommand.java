package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.system;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.awt.*;
import java.io.File;
import java.io.IOException;

@CLIEntry(
	description = "Opens a project path in a file browser",
	version = "1.0",
	nickName = "open",
	commandGroup = "system"
)
public class OpenCommand extends ConsoleCommand {
	private String path = ".";

	@CLIOption(
		description = "Path to open, relative to the current working directory.  Default path is '.'",
		name = "p",
		longName = "path",
		unnamedArgumentOrdinal = 1,
		defaultValue = "."
	)
	public void path(String p) {
		path = p;
	}

	@CLIMain
	public void open() throws IOException {
		try {
			File basedir = makeFilePath(path).getCanonicalFile();

			if (Desktop.isDesktopSupported()) {
				Desktop dt = Desktop.getDesktop();
				dt.open(basedir);
			} else {
				println("Your platform does not support the Java Desktop - don't ask me why.");
			}
		} catch (
				Exception exc) {
			exc.printStackTrace();
			println(exc.toString());
		}
	}
}
