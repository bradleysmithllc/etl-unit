package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.system;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang.StringUtils;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.CommandReference;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.StringUtil;
import org.jline.utils.AttributedStyle;

import java.util.Arrays;
import java.util.List;

@CLIEntry(
		description = "Prints the command list",
		version = "1.0",
		nickName = "help",
		commandGroup = "system"
)
public class HelpCommand extends ConsoleCommand {
	@CLIMain
	public void helpMe() {
		int terminalWidth = terminalWidth();

		// determine the longest nickname
		int maxNameLength = 8;

		for (CommandReference command : commandLoader.commands()) {
			maxNameLength = Math.max(maxNameLength, command.nickName().length());
		}

		String lineProto = new StringBuilder().append("  ").append(StringUtils.leftPad("", maxNameLength, " ")).append("       ").toString();

		println("Command Groups:");

		for (String grp : Arrays.asList("etlunit", "system")) {
			println();

			println(" " + grp + ":", AttributedStyle.DEFAULT.italic());

			for (CommandReference command : commandLoader.group(grp)) {
				String nickName = StringUtils.leftPad(command.nickName(), maxNameLength, " ");

				print("  " + nickName, AttributedStyle.DEFAULT.bold());

				if (!command.shortcuts().isEmpty()) {
					String shortcuts = StringUtils.center(command.shortcuts().get(0), 5, " ");
					print(" " + shortcuts + " ", AttributedStyle.DEFAULT.italic().foreground(AttributedStyle.CYAN));
				} else {
					print("       ", AttributedStyle.DEFAULT);
				}

				List<String> formatted = StringUtil.wrapTextToList(command.description(), terminalWidth - lineProto.length());

				boolean first = true;

				for (String line : formatted) {
					if (first) {
						first = false;
					} else {
						print(lineProto);
					}
					println(line, AttributedStyle.DEFAULT);
				}
			}
		}
	}
}
