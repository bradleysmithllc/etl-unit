package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.system;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.ConsoleCommand;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;

import java.io.File;
import java.io.IOException;

@CLIEntry(
	description = "Changes current working directory",
	nickName = "cd",
	commandGroup = "system",
	version = "1.0"
)
public class CdCommand extends ConsoleCommand {
	private String path = "~";

	@CLIOption(
		description = "Path to change to, relative to the current working directory, or absolute.",
		name = "p",
		longName = "path",
		unnamedArgumentOrdinal = 1,
		defaultValue = "."
	)
	public void path(String path) {
		this.path = path;
	}

	public void ls() throws IOException {
		File file = makeFilePath(path);

		String absolutePath = file.getAbsolutePath();
		println(absolutePath);
		System.setProperty("user.dir", absolutePath);
	}
}
