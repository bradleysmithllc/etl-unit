package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.set_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.MenuItem;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.SetCommand;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;

import java.util.*;

public class FeatureMenu extends AbstractMenu {
	List<MenuItem> items = new ArrayList<>();

	public FeatureMenu(MapList<Feature, RuntimeOption> mapList)
	{
		// sort keys alphabetically
		List<Feature> featureArrayList = new ArrayList<Feature>(mapList.keySet());
		Collections.sort(featureArrayList, new Comparator<Feature>() {
			@Override
			public int compare(Feature o1, Feature o2)
			{
				return o1.getFeatureName().compareTo(o2.getFeatureName());
			}
		});

		for (Feature feature : featureArrayList)
		{
			items.add(new FeatureMenuItem(feature, mapList.get(feature)));
		}
	}

	@Override
	public boolean includeCancel() {
		return true;
	}

	@Override
	public boolean includeReset() {
		return SetCommand.statelessOptions.size() > 0;
	}

	@Override
	public String prompt() {
		return "Select a feature";
	}

	@Override
	public List<MenuItem> items() {
		return items;
	}

	@Override
	public Menu<MenuItem> choose(MenuItem item) throws Exception {
		FeatureMenuItem fmi = (FeatureMenuItem) item;
		return new RuntimeOptionsMenu(fmi.feature(), fmi.runtimeOptions());
	}
}
