package org.bitbucket.bradleysmithllc.etlunit.jline_cli.parser.ansi;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.ParseException;

public class APStateMachine {
	public static void parseException(String text) throws ParseException {
		throw new ParseException(text, 0);
	}

	public static String decorateAnsi(String possibleAnsi) throws ParseException {
		TextAPState textState = new TextAPState();
		InEscapeCodeAPState inEscapeCodeAPState = new InEscapeCodeAPState();
		EscapeStartAPState escapeStartAPState = new EscapeStartAPState();

		textState.link(escapeStartAPState);

		escapeStartAPState.link(textState);
		escapeStartAPState.link(inEscapeCodeAPState);

		inEscapeCodeAPState.link(textState);

		StringBuffer outBuff = new StringBuffer();

		APState currentState = textState;

		for (char ch : possibleAnsi.toCharArray()) {
			APState transitionTo = currentState.consume(ch, outBuff);

			if (transitionTo != null) {
				currentState = transitionTo;
				currentState.enter();
			}
		}

		currentState.eof(outBuff);

		return outBuff.toString();
	}
}
