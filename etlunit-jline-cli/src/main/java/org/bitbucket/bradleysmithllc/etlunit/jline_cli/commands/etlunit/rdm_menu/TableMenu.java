package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.rdm_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.TerminalInterface;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.util.jdbc.JdbcVisitors;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TableMenu extends AbstractMenu<DatabaseMenuItem> {
	private final String catalog;
	private final String schema;
	private final ETLTestClass etlTestClass;
	private DatabaseConnection databaseConnection;

	private final List<DatabaseMenuItem> items = new ArrayList<>();

	public TableMenu(
			DatabaseFeatureModule databaseFeatureModule,
			DatabaseConnection databaseConnection,
			TerminalInterface terminalInterface,
			ETLTestClass etlTestClass,
			String catalog,
			String schema,
			Connection sqlConnection
	) throws Exception {
		this.databaseConnection = databaseConnection;
		this.catalog = catalog;
		this.schema = schema;
		this.etlTestClass = etlTestClass;

		JdbcVisitors.withPersistentConnection(sqlConnection).withTables(catalog, schema, null, (metadataTable) -> {
			items.add(new TableMenuItem(
					databaseFeatureModule,
					databaseConnection,
					terminalInterface,
					etlTestClass,
					catalog,
					schema,
					metadataTable.tableName()
			));
		}).dispose();

		Collections.sort(items, (tbla, tblb) -> {
			return tbla.name().compareTo(tblb.name());
		});
	}

	@Override
	public boolean includeCancel() {
		return true;
	}

	@Override
	public boolean includeReset() {
		return false;
	}

	@Override
	public String title() {
		return "Tables in schema [" + databaseConnection.getId() + "." + schema.toLowerCase() + "]";
	}

	@Override
	public String prompt() {
		return "Select table";
	}

	@Override
	public List<DatabaseMenuItem> items() {
		return items;
	}

	@Override
	public Menu<DatabaseMenuItem> choose(DatabaseMenuItem item) {
		return null;
	}

	public void disposeAll() throws SQLException {
		for (DatabaseMenuItem item : items) {
			item.disposeAll();
		}
	}
}
