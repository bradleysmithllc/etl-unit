package org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.etlunit.set_menu;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.AbstractMenu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.Menu;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.commands.MenuItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RuntimeOptionsMenu extends AbstractMenu {
	private final Feature feature;
	private final List<RuntimeOption> runtimeOptions;

	private final List<MenuItem> items = new ArrayList<>();

	public RuntimeOptionsMenu(Feature feature, List<RuntimeOption> featureRuntimeOptions) {
		this.feature = feature;
		this.runtimeOptions = featureRuntimeOptions;

		// sort options alphabetically
		List<RuntimeOption> sortedRuntimeOptions = new ArrayList<>(featureRuntimeOptions);
		Collections.sort(sortedRuntimeOptions, new Comparator<RuntimeOption>() {
			@Override
			public int compare(RuntimeOption o1, RuntimeOption o2)
			{
				return o1.getName().compareTo(o2.getName());
			}
		});

		for (RuntimeOption ro : sortedRuntimeOptions) {
			items.add(new RuntimeOptionsMenuItem(feature, ro));
		}
	}

	@Override
	public boolean includeCancel() {
		return true;
	}

	@Override
	public boolean includeReset() {
		return false;
	}

	@Override
	public String prompt() {
		return "Select an option";
	}

	@Override
	public List<MenuItem> items() {
		return items;
	}

	@Override
	public Menu<MenuItem> choose(MenuItem item) throws Exception {
		return null;
	}
}
