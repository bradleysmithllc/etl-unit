package org.bitbucket.bradleysmithllc.etlunit.jline_cli.specification.test;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.EtlUnitHighlighter;
import org.bitbucket.bradleysmithllc.etlunit.jline_cli.parser.ansi.APStateMachine;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;

public class JLineCLIParserTests {
	private String ansiize(String input) throws java.text.ParseException {
		return APStateMachine.decorateAnsi(new EtlUnitHighlighter().highlight(null, input).toAnsi());
	}

	@Test
	public void testEmpty() throws ParseException {
		Assert.assertEquals("", ansiize(""));
	}

	@Test
	public void justCommand() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off}", ansiize("test"));
	}

	@Test
	public void justCommandWithDashes() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test-i-1_2{mode off}", ansiize("test-i-1_2"));
	}

	@Test
	public void commandDash() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off} {mode bold;fg_yellow}-{mode off}", ansiize("test -"));
	}

	@Test
	public void commandShortFlag() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off} {mode bold;fg_yellow}-flag{mode off}", ansiize("test -flag"));
	}

	@Test
	public void commandDoubleDash() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off} {mode bold;fg_yellow}--{mode off}", ansiize("test --"));
	}

	@Test
	public void commandLongFlag() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off} {mode bold;fg_yellow}--flaggy-flag{mode off}", ansiize("test --flaggy-flag"));
	}

	@Test
	public void commandAndArgument() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off} {mode fg_red}hi{mode off}", ansiize("test hi"));
	}

	@Test
	public void commandQuote() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off} {mode fg_blue}'hi{mode off}", ansiize("test 'hi"));
	}

	@Test
	public void commandQuotes() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off} {mode fg_blue}'hi'{mode off}", ansiize("test 'hi'"));
	}

	@Test
	public void commandAndArguments() throws ParseException {
		Assert.assertEquals("{mode bold;fg_green}test{mode off}   {mode fg_red}hi{mode off} {mode fg_red}h3{mode off}      {mode fg_red}h4{mode off}", ansiize("test   hi h3      h4"));
	}

	//@Test
	public void commandAndPackage() throws ParseException {
		Assert.assertEquals("\u001B[32;1mtest\u001B[0m   \u001B[31mhi\u001B[0m \u001B[31mh3\u001B[0m      \u001B[31mh4\u001B[0m", ansiize("test %te"));
	}
}
