package org.bitbucket.bradleysmithllc.etlunit.jline_cli.ansi.test;

/*
 * #%L
 * etlunit-jline-cli
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.jline_cli.parser.ansi.APStateMachine;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.ParseException;

public class AnsiEscapeParserTests {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void empty() throws ParseException {
		Assert.assertEquals("", APStateMachine.decorateAnsi(""));
	}

	@Test
	public void brackets() throws ParseException {
		Assert.assertEquals("[h]i[e]lo]", APStateMachine.decorateAnsi("[h]i[e]lo]"));
	}

	@Test
	public void startStartStartStart() throws ParseException {
		Assert.assertEquals("\u001b\u001b\u001b\u001b\u001b", APStateMachine.decorateAnsi("\u001b\u001b\u001b\u001b\u001b"));
	}

	@Test
	public void startStopStartStop() throws ParseException {
		Assert.assertEquals("\u001ba\u001bb\u001bc\u001bd\u001b", APStateMachine.decorateAnsi("\u001ba\u001bb\u001bc\u001bd\u001b"));
	}

	@Test
	public void justEscapeStart() throws ParseException {
		Assert.assertEquals("\u001b", APStateMachine.decorateAnsi("\u001b"));
		Assert.assertEquals("\u001bHello \u001b [ World \u001b", APStateMachine.decorateAnsi("\u001bHello \u001b [ World \u001b"));
	}

	@Test
	public void danglingCode() throws ParseException {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Dangling escape code");
		APStateMachine.decorateAnsi("\u001b[");
	}

	@Test
	public void baseModes() throws ParseException {
		Assert.assertEquals("{mode off;bold;underscore;blink;reverse;concealed}", APStateMachine.decorateAnsi("\u001b[4;1;5;0;7;8m"));
	}

	@Test
	public void colorModes() throws ParseException {
		Assert.assertEquals("{mode fg_black;fg_red;fg_green;fg_yellow;fg_blue;fg_magenta;fg_cyan;fg_white;bg_black;bg_red;bg_green;bg_yellow;bg_blue;bg_magenta;bg_cyan;bg_white}", APStateMachine.decorateAnsi("\u001b[40;41;42;43;44;45;46;47;37;30;36;31;35;32;34;33m"));
	}

	@Test
	public void graphicsMode() throws ParseException {
		Assert.assertEquals("{mode off}", APStateMachine.decorateAnsi("\u001b[0m"));
	}
}
