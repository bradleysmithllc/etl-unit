package org.bitbucket.bradleysmithllc.etlunit.feature.h2_database;

/*
 * #%L
 * etlunit-h2-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class Integration2Test extends BaseIntegrationTest
{
	@Test
	public void test()
	{
		startTest();
	}

	protected void postTest() {
		try {
			if (runtimeSupport.getExecutorCount() == 1) {
				File genSrc = getGeneratedSource("h2db", null, getDatabaseName(null, -1));
				Assert.assertTrue(genSrc.getName(), genSrc.exists());
			} else {
				boolean exists = false;

				if (runtimeSupport.getExecutorCount() > 1) {
					for (int i = 0; i < runtimeSupport.getExecutorCount(); i++) {
						genSrc = getGeneratedSource("h2db", null, getDatabaseName("a", i));
						exists |= genSrc.exists();
					}
				} else {
					genSrc = getGeneratedSource("h2db", null, getDatabaseName("a", -1));
					exists |= genSrc.exists();
				}

				Assert.assertTrue(genSrc.getName(), exists);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private String getDatabaseName(String mode, int exeId) {
		String exe = "";

		if (exeId >= 0) {
			exe = "_" + exeId;
		}

		return "db" + (mode == null ? "" : ("_" + mode)) + exe + ".mv.db";
	}
}
