CREATE SCHEMA cs;;

create table cs.FK_TABLE (
	ID INT NOT NULL
);;

create table cs.PK_TABLE (
	ID INT NOT NULL,
	primary key(id)
);;

ALTER TABLE cs.FK_TABLE
ADD CONSTRAINT FK_PersonOrder
FOREIGN KEY (ID) REFERENCES cs.PK_TABLE(ID);;