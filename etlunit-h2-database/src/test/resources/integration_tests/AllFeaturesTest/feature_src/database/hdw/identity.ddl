CREATE SCHEMA ident;;

create table ident.IdentityTable (
	ID IDENTITY PRIMARY KEY,
	DI int not null
);;