package org.bitbucket.bradleysmithllc.etlunit.feature.h2_database;

/*
 * #%L
 * etlunit-h2-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.*;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class H2DatabaseImplementation extends BaseDatabaseImplementation {
	private final H2DatabaseFeatureModule h2DatabaseFeatureModule;
	private DatabaseRuntimeSupport databaseRuntimeSupport;

	@Inject
	@Named("h2-database.use-static-storage")
	private RuntimeOption useStaticStorage = null;

	public H2DatabaseImplementation(H2DatabaseFeatureModule h2DatabaseFeatureModule) {
		this.h2DatabaseFeatureModule = h2DatabaseFeatureModule;
	}

	@Inject
	public void setDatabaseFeatureModule(DatabaseRuntimeSupport databaseRuntimeSupport) {
		this.databaseRuntimeSupport = databaseRuntimeSupport;
	}

	public database_state getDatabaseState(DatabaseConnection databaseConnection, String s) {
		return database_state.fail;
	}

	public String getImplementationId() {
		return "h2";
	}

	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException {
		switch (op) {
			case createDatabase:
				InitializeRequest initializeRequest = request.getInitializeRequest();
				maybeCreateDatabase(initializeRequest.getConnection(), initializeRequest.getMode());
				break;

			case preCreateSchema:
				break;
			case postCreateSchema:
				break;
			case completeDatabaseInitialization:
				break;
			case materializeViews:
				break;
			case dropConstraints:
				break;
			case resetIdentities:
				try {
					resetIdentities(request.getDatabaseRequest());
				} catch (TestExecutionError testExecutionError) {
					throw new RuntimeException(testExecutionError);
				}
				break;
			case preCleanTables:
				break;
			case postCleanTables:
				break;
		}

		return null;
	}

	private void resetIdentities(DatabaseRequest databaseRequest) throws TestExecutionError {
		applicationLog.info("Resetting identities");

		// execute a query against the information schema to find identities...
		List<Pair<String, String>> tables = new ArrayList<>();

		jdbcClient.useStatement(databaseRequest.getConnection(), databaseRequest.getMode(), new JDBCClient.StatementClient() {
			@Override
			public void connection(Connection conn, Statement st, DatabaseConnection connection, String mode, database_role id) throws Exception {
				ResultSet resultSet = conn.getMetaData().getColumns(null, null, null, null);
				try {
					while (resultSet.next()) {
						if (resultSet.getBoolean("IS_AUTOINCREMENT")) {
							tables.add(new ImmutablePair<>(resultSet.getString("TABLE_SCHEM") + "." + resultSet.getString("TABLE_NAME"), resultSet.getString("COLUMN_NAME")));
						}
					}
				} finally {
					resultSet.close();
				}

				for (Pair<String, String> columnPair : tables) {
					applicationLog.info("Resetting identity column " + columnPair.getLeft() + "." + columnPair.getRight());
					st.execute("ALTER TABLE " + columnPair.getLeft() + " ALTER COLUMN " + columnPair.getRight() + " RESTART WITH 1");
				}
			}
		});

		applicationLog.info("Identities reset");
	}

	@Override
	public String getPasswordImpl(DatabaseConnection dc, String mode) {
		return null;
	}

	@Override
	public String getLoginNameImpl(DatabaseConnection dc, String mode) {
		return null;
	}

	private synchronized void maybeCreateDatabase(DatabaseConnection dc, String mode) {
		String dbname = getDatabaseName(dc, mode);

		try {
			File databaseFile = new File(h2DatabaseFeatureModule.getDatabaseRootPath(), dbname);

			if (!databaseFile.exists()) {
				applicationLog.info("Creating H2 database " + dbname);
				Connection conn = DriverManager.getConnection("jdbc:h2:file:" + databaseFile.getAbsolutePath());
				conn.close();
			}
		} catch (Exception exc) {
			throw new IllegalStateException(exc);
		}
	}

	@Override
	public String getDatabaseName(DatabaseConnection dc, String mode) {
		String dbname = super.getDatabaseName(dc, mode);

		if (useStaticStorage.isEnabled()) {
			dbname =
					dc.getId() +
					(mode == null ? "" : ("_" + EtlUnitStringUtils.sanitize(mode, '_'))) +
					(runtimeSupport.getExecutorCount() > 1 ? ("_" + runtimeSupport.getExecutorId()) : "");
		}

		return dbname;
	}

	@Override
	public synchronized String getJdbcUrl(DatabaseConnection dc, String mode, database_role id) {
		// validate that this database has been initialized

		maybeCreateDatabase(dc, mode);

		return h2DatabaseFeatureModule.getBaseUrl() + "/" + getDatabaseName(dc, mode);
	}

	@Override
	public Class getJdbcDriverClass() {
		return org.h2.Driver.class;
	}

	public String getDefaultSchema(DatabaseConnection databaseConnection, String mode) {
		return "PUBLIC";
	}
}
