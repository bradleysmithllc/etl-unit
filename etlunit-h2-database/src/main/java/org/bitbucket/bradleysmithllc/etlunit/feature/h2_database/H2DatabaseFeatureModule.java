package org.bitbucket.bradleysmithllc.etlunit.feature.h2_database;

/*
 * #%L
 * etlunit-h2-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.FileFeatureModuleConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.feature.h2_database.json.H2DatabaseFeatureModuleConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.h2.tools.Server;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@FeatureModule
public class H2DatabaseFeatureModule extends AbstractFeature {
	private static final List<String> prerequisites = Arrays.asList("logging", "database");
	private DatabaseFeatureModule databaseFeatureModule;
	private H2DatabaseImplementation hsqldbDatabaseImplementation;

	protected RuntimeSupport runtimeSupport;

	private Server server;
	private File databaseRoot;

	@Inject
	@Named("h2-database.use-static-storage")
	private RuntimeOption useStaticStorage = null;

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	private void startH2Server() {
		try {
			if (useStaticStorage.isEnabled()) {
				databaseRoot = runtimeSupport.getGeneratedSourceDirectory("h2db");
			} else {
				databaseRoot = runtimeSupport.createTempFile("h2db");
			}

			// always clear root directory at server start.
			applicationLog.info("H2 server root directory " + getDatabaseRootPath() + "-  purging.");

			FileUtils.deleteDirectory(databaseRoot);
			databaseRoot.mkdirs();

			applicationLog.info("Starting H2 server in root directory " + getDatabaseRootPath());

			server = Server.createTcpServer("-baseDir", databaseRoot.getAbsolutePath()).start();
			applicationLog.info("Application base URL " + getBaseUrl());
		} catch(Exception exc) {
			throw new IllegalStateException(exc);
		}
	}

	@Inject
	public void setDatabaseFeatureModule(DatabaseFeatureModule module)
	{
		databaseFeatureModule = module;
	}

	@Override
	public List<String> getPrerequisites() {
		return prerequisites;
	}

	@Override
	public void dispose()
	{
		applicationLog.info("Stopping H2 server");
		server.stop();

		hsqldbDatabaseImplementation.dispose();
	}

	@Override
	public void initialize(Injector inj)
	{
		applicationLog.info("Starting H2 server");
		startH2Server();

		hsqldbDatabaseImplementation = postCreate(new H2DatabaseImplementation(this));
		databaseFeatureModule.addDatabaseImplementation(hsqldbDatabaseImplementation);
	}

	public String getBaseUrl() {
		return "jdbc:h2:tcp://localhost:" + server.getPort();
	}

	public File getDatabaseRootPath() {
		return databaseRoot;
	}

	@Override
	public long getPriorityLevel() {
		return -100;
	}
}
