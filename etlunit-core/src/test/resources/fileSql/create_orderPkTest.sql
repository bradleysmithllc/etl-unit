CREATE TABLE "orderPkTest-virtualtable-20131105-22-11-17-174-1"
(
	column VARCHAR(1024) NULL,
	PRIMARY KEY
	(
		column
	)
);

CREATE INDEX "orderPkTest-virtualtable-20131105-22-11-17-174-1_IDX" ON "orderPkTest-virtualtable-20131105-22-11-17-174-1"
(
	column
);