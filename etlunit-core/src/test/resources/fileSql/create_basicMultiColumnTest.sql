CREATE TABLE "basicMultiColumnTest-virtualtable-20131105-22-11-17-174-1"
(
	column1 VARCHAR(1024) NULL,
	column2 VARCHAR(1024) NULL,
	column3 VARCHAR(1024) NULL,
	column4 VARCHAR(1024) NULL,
	column5 VARCHAR(1024) NULL,
	PRIMARY KEY
	(
		column1,
		column5
	)
);

CREATE INDEX "basicMultiColumnTest-virtualtable-20131105-22-11-17-174-1_IDX" ON "basicMultiColumnTest-virtualtable-20131105-22-11-17-174-1"
(
	column2,
	column3
);