CREATE TABLE "basicTest_ex"
(
	col2 VARCHAR(1024) NULL
);

CREATE INDEX "basicTest_ex_IDX" ON "basicTest_ex"
(
	col2
);