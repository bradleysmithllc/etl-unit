CREATE TABLE "pkTest_ex"
(
	col1 VARCHAR(1024) NULL,
	col2 VARCHAR(1024) NULL,
	col3 VARCHAR(1024) NULL,
	col4 VARCHAR(1024) NULL,
	col5 VARCHAR(1024) NULL,
	col6 VARCHAR(1024) NULL,
	col7 VARCHAR(1024) NULL,
	PRIMARY KEY
	(
		col1,
		col2
	)
);

CREATE INDEX "pkTest_ex_IDX" ON "pkTest_ex"
(
	col2,
	col3,
	col4,
	col5,
	col6,
	col7
);