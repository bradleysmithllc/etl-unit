CREATE TABLE "basicTest_in"
(
	col1 VARCHAR(1024) NULL,
	col2 VARCHAR(1024) NULL
);

CREATE INDEX "basicTest_in_IDX" ON "basicTest_in"
(
	col1,
	col2
);