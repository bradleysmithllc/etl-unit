CREATE TABLE "pkObTest_mat_3"
(
	col1 VARCHAR(1024) NULL,
	col3 VARCHAR(1024) NULL,
	PRIMARY KEY
	(
		col3,
		col1
	)
);

CREATE INDEX "pkObTest_mat_3_IDX" ON "pkObTest_mat_3"
(
	col3,
	col1
);