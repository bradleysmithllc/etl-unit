CREATE TABLE "pkObTest_mat_1"
(
	col1 VARCHAR(1024) NULL,
	col2 VARCHAR(1024) NULL,
	col3 VARCHAR(1024) NULL,
	PRIMARY KEY
	(
		col3,
		col2,
		col1
	)
);

CREATE INDEX "pkObTest_mat_1_IDX" ON "pkObTest_mat_1"
(
	col3,
	col2,
	col1
);