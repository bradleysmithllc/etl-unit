CREATE TABLE "pkObTest_mat_2"
(
	col1 VARCHAR(1024) NULL,
	col2 VARCHAR(1024) NULL,
	col3 VARCHAR(1024) NULL,
	PRIMARY KEY
	(
		col3,
		col2,
		col1
	)
);

CREATE INDEX "pkObTest_mat_2_IDX" ON "pkObTest_mat_2"
(
	col3,
	col2,
	col1
);