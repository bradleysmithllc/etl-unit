package org.bitbucket.bradleysmithllc.etlunit.io.file.converter.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.*;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NegativeZeroPositiveNumericConverterTest extends BaseConverterSupport {

	private NumericConverter numericConverter = new NumericConverter();
	private IntegerConverter integerConverter = new IntegerConverter();
	private DoubleConverter doubleConverter = new DoubleConverter();
	private FloatConverter floatConverter = new FloatConverter();
	private DecimalConverter decimalConverter = new DecimalConverter();
	private RealConverter realConverter = new RealConverter();
	private SmallIntConverter smallIntConverter = new SmallIntConverter();
	private TinyIntConverter tinyIntConverter = new TinyIntConverter();
	private BigintConverter bigintConverter = new BigintConverter();
	private HexadecimalIntegerConverter hexadecimalIntegerConverter = new HexadecimalIntegerConverter();
	private HexadecimalBigIntegerConverter hexadecimalBigIntegerConverter = new HexadecimalBigIntegerConverter();

	@Test
	public void parse0()
	{
		Assert.assertEquals(new BigDecimal(0), numericConverter.parse("0", col));
		Assert.assertEquals(0, integerConverter.parse("0", col));
		Assert.assertEquals(0.0D, doubleConverter.parse("0", col));
		Assert.assertEquals(0.0f, floatConverter.parse("0", col));
		Assert.assertEquals(new BigDecimal(0), decimalConverter.parse("0", col));
		Assert.assertEquals(0.0f, realConverter.parse("0", col));
		Assert.assertEquals(new Short((short) 0), smallIntConverter.parse("0", col));
		Assert.assertEquals(new Byte((byte) 0), tinyIntConverter.parse("0", col));
		Assert.assertEquals(new Long(0L), bigintConverter.parse("0", col));
		Assert.assertEquals(0, hexadecimalIntegerConverter.parse("0", col));
		Assert.assertEquals(new Long(0), hexadecimalBigIntegerConverter.parse("0", col));
	}

	@Test
	public void parseNegative()
	{
		Assert.assertEquals(new BigDecimal(-1), numericConverter.parse("-1", col));
		Assert.assertEquals(-1, integerConverter.parse("-1", col));
		Assert.assertEquals(-1.0D, doubleConverter.parse("-1", col));
		Assert.assertEquals(-1.0f, floatConverter.parse("-1", col));
		Assert.assertEquals(new BigDecimal(-1), decimalConverter.parse("-1", col));
		Assert.assertEquals(-1.0f, realConverter.parse("-1", col));
		Assert.assertEquals(new Short((short) -1), smallIntConverter.parse("-1", col));
		Assert.assertEquals(new Byte((byte) -1), tinyIntConverter.parse("-1", col));
		Assert.assertEquals(new Long(-1L), bigintConverter.parse("-1", col));
		Assert.assertEquals(-1, hexadecimalIntegerConverter.parse("-1", col));
		Assert.assertEquals(new Long(-1), hexadecimalBigIntegerConverter.parse("-1", col));
	}

	@Test
	public void parsePositive()
	{
		Assert.assertEquals(new BigDecimal(1), numericConverter.parse("1", col));
		Assert.assertEquals(1, integerConverter.parse("1", col));
		Assert.assertEquals(1.0D, doubleConverter.parse("1", col));
		Assert.assertEquals(1.0f, floatConverter.parse("1", col));
		Assert.assertEquals(new BigDecimal(1), decimalConverter.parse("1", col));
		Assert.assertEquals(1.0f, realConverter.parse("1", col));
		Assert.assertEquals(new Short((short) 1), smallIntConverter.parse("1", col));
		Assert.assertEquals(new Byte((byte) 1), tinyIntConverter.parse("1", col));
		Assert.assertEquals(new Long(1L), bigintConverter.parse("1", col));
		Assert.assertEquals(1, hexadecimalIntegerConverter.parse("1", col));
		Assert.assertEquals(new Long(1), hexadecimalBigIntegerConverter.parse("1", col));
	}

	@Test
	public void format0()
	{
		Assert.assertEquals("0", numericConverter.format(new BigDecimal(0), col));
		Assert.assertEquals("0", integerConverter.format(0, col));
		Assert.assertEquals("0.0", doubleConverter.format(0.0D, col));
		Assert.assertEquals("0.0", floatConverter.format(0.0f, col));
		Assert.assertEquals("0", decimalConverter.format(new BigDecimal(0), col));
		Assert.assertEquals("0.0", realConverter.format(0.0f, col));
		Assert.assertEquals("0", smallIntConverter.format(0, col));
		Assert.assertEquals("0", tinyIntConverter.format(0, col));
		Assert.assertEquals("0", bigintConverter.format(new Long(0L), col));
		Assert.assertEquals("0", hexadecimalIntegerConverter.format(0, col));
		Assert.assertEquals("0", hexadecimalBigIntegerConverter.format(new Long(0), col));
	}

	@Test
	public void formatNegative()
	{
		Assert.assertEquals("-1", numericConverter.format(new BigDecimal(-1), col));
		Assert.assertEquals("-1", integerConverter.format(-1, col));
		Assert.assertEquals("-1.0", doubleConverter.format(-1.0D, col));
		Assert.assertEquals("-1.0", floatConverter.format(-1.0f, col));
		Assert.assertEquals("-1", decimalConverter.format(new BigDecimal(-1), col));
		Assert.assertEquals("-1.0", realConverter.format(-1.0f, col));
		Assert.assertEquals("-1", smallIntConverter.format(-1, col));
		Assert.assertEquals("-1", tinyIntConverter.format(-1, col));
		Assert.assertEquals("-1", bigintConverter.format(new Long(-1L), col));
		Assert.assertEquals("ffffffff", hexadecimalIntegerConverter.format(-1, col));
		Assert.assertEquals("ffffffffffffffff", hexadecimalBigIntegerConverter.format(new Long(-1), col));
	}

	@Test
	public void formatPositive()
	{
		Assert.assertEquals("1", numericConverter.format(new BigDecimal(1), col));
		Assert.assertEquals("1", integerConverter.format(1, col));
		Assert.assertEquals("1.0", doubleConverter.format(1.0D, col));
		Assert.assertEquals("1.0", floatConverter.format(1.0f, col));
		Assert.assertEquals("1", decimalConverter.format(new BigDecimal(1), col));
		Assert.assertEquals("1.0", realConverter.format(1.0f, col));
		Assert.assertEquals("1", smallIntConverter.format(1, col));
		Assert.assertEquals("1", tinyIntConverter.format(1, col));
		Assert.assertEquals("1", bigintConverter.format(new Long(1L), col));
		Assert.assertEquals("1", hexadecimalIntegerConverter.format(1, col));
		Assert.assertEquals("1", hexadecimalBigIntegerConverter.format(new Long(1), col));
	}

	@Test
	public void formatMax()
	{
		Assert.assertEquals("2147483647", integerConverter.format(Integer.MAX_VALUE, col));
		Assert.assertEquals("1.7976931348623157E308", doubleConverter.format(Double.MAX_VALUE, col));
		Assert.assertEquals("3.4028235E38", floatConverter.format(Float.MAX_VALUE, col));
		Assert.assertEquals("3.4028235E38", realConverter.format(Float.MAX_VALUE, col));
		Assert.assertEquals("32767", smallIntConverter.format(Short.MAX_VALUE, col));
		Assert.assertEquals("127", tinyIntConverter.format(Byte.MAX_VALUE, col));
		Assert.assertEquals("9223372036854775807", bigintConverter.format(new Long(Long.MAX_VALUE), col));
		Assert.assertEquals("7fffffff", hexadecimalIntegerConverter.format(Integer.MAX_VALUE, col));
		Assert.assertEquals("7fffffffffffffff", hexadecimalBigIntegerConverter.format(Long.MAX_VALUE, col));
	}

	@Test
	public void parseMax()
	{
		Assert.assertEquals(Integer.MAX_VALUE, integerConverter.parse("2147483647", col));
		Assert.assertEquals(Double.MAX_VALUE, doubleConverter.parse("1.7976931348623157E308", col));
		Assert.assertEquals(Float.MAX_VALUE, floatConverter.parse("3.4028235E38", col));
		Assert.assertEquals(Float.MAX_VALUE, realConverter.parse("3.4028235E38", col));
		Assert.assertEquals(Short.MAX_VALUE, smallIntConverter.parse("32767", col));
		Assert.assertEquals(Byte.MAX_VALUE, tinyIntConverter.parse("127", col));
		Assert.assertEquals(new Long(Long.MAX_VALUE), bigintConverter.parse("9223372036854775807", col));
		Assert.assertEquals(Integer.MAX_VALUE, hexadecimalIntegerConverter.parse("7fffffff", col));
		Assert.assertEquals(Long.MAX_VALUE, hexadecimalBigIntegerConverter.parse("7fffffffffffffff", col));
	}

	@Test
	public void formatMin()
	{
		Assert.assertEquals("-2147483648", integerConverter.format(Integer.MIN_VALUE, col));
		Assert.assertEquals("4.9E-324", doubleConverter.format(Double.MIN_VALUE, col));
		Assert.assertEquals("1.4E-45", floatConverter.format(Float.MIN_VALUE, col));
		Assert.assertEquals("1.4E-45", realConverter.format(Float.MIN_VALUE, col));
		Assert.assertEquals("-32768", smallIntConverter.format(Short.MIN_VALUE, col));
		Assert.assertEquals("-128", tinyIntConverter.format(Byte.MIN_VALUE, col));
		Assert.assertEquals("-9223372036854775808", bigintConverter.format(new Long(Long.MIN_VALUE), col));
		Assert.assertEquals("80000000", hexadecimalIntegerConverter.format(Integer.MIN_VALUE, col));
		Assert.assertEquals("8000000000000000", hexadecimalBigIntegerConverter.format(Long.MIN_VALUE, col));
	}

	@Test
	public void parseMin()
	{
		Assert.assertEquals(Integer.MIN_VALUE, integerConverter.parse("-2147483648", col));
		Assert.assertEquals(Double.MIN_VALUE, doubleConverter.parse("4.9E-324", col));
		Assert.assertEquals(Float.MIN_VALUE, floatConverter.parse("1.4E-45", col));
		Assert.assertEquals(Float.MIN_VALUE, realConverter.parse("1.4E-45", col));
		Assert.assertEquals(Short.MIN_VALUE, smallIntConverter.parse("-32768", col));
		Assert.assertEquals(Byte.MIN_VALUE, tinyIntConverter.parse("-128", col));
		Assert.assertEquals(new Long(Long.MIN_VALUE), bigintConverter.parse("-9223372036854775808", col));

		// these are not bi-directional since hex is absolute value
		//Assert.assertEquals(Integer.MIN_VALUE, hexadecimalIntegerConverter.parse("80000001", col));
		//Assert.assertEquals(Long.MIN_VALUE, hexadecimalBigIntegerConverter.parse("8000000000000000", col));
	}

	@Test
	public void matchMin()
	{
		Assert.assertTrue(integerConverter.getPattern(col).matcher("-2147483648").matches());
		Assert.assertTrue(doubleConverter.getPattern(col).matcher("1.7976931348623157E308").matches());
		Assert.assertTrue(floatConverter.getPattern(col).matcher("1.4E-45").matches());
		Assert.assertTrue(realConverter.getPattern(col).matcher("1.4E-45").matches());
		Assert.assertTrue(smallIntConverter.getPattern(col).matcher("-32768").matches());
		Assert.assertTrue(tinyIntConverter.getPattern(col).matcher("-128").matches());
		Assert.assertTrue(bigintConverter.getPattern(col).matcher("-9223372036854775808").matches());
		Assert.assertTrue(hexadecimalIntegerConverter.getPattern(col).matcher("00000000").matches());
		Assert.assertTrue(hexadecimalBigIntegerConverter.getPattern(col).matcher("0000000000000000").matches());
	}

	@Test
	public void matchZero()
	{
		Assert.assertTrue(integerConverter.getPattern(col).matcher("0").matches());
		Assert.assertTrue(doubleConverter.getPattern(col).matcher("0").matches());
		Assert.assertTrue(floatConverter.getPattern(col).matcher("0").matches());
		Assert.assertTrue(realConverter.getPattern(col).matcher("0").matches());
		Assert.assertTrue(smallIntConverter.getPattern(col).matcher("0").matches());
		Assert.assertTrue(tinyIntConverter.getPattern(col).matcher("0").matches());
		Assert.assertTrue(bigintConverter.getPattern(col).matcher("0").matches());
		Assert.assertTrue(hexadecimalIntegerConverter.getPattern(col).matcher("0").matches());
		Assert.assertTrue(hexadecimalBigIntegerConverter.getPattern(col).matcher("0").matches());
	}

	@Test
	public void matchMax()
	{
		Assert.assertTrue(integerConverter.getPattern(col).matcher("2147483647").matches());
		Assert.assertTrue(doubleConverter.getPattern(col).matcher("4.9E-324").matches());
		Assert.assertTrue(floatConverter.getPattern(col).matcher("3.4028235E38").matches());
		Assert.assertTrue(realConverter.getPattern(col).matcher("3.4028235E38").matches());
		Assert.assertTrue(smallIntConverter.getPattern(col).matcher("32767").matches());
		Assert.assertTrue(tinyIntConverter.getPattern(col).matcher("127").matches());
		Assert.assertTrue(bigintConverter.getPattern(col).matcher("9223372036854775807").matches());
		Assert.assertTrue(hexadecimalIntegerConverter.getPattern(col).matcher("FFFFFFFF").matches());
		Assert.assertTrue(hexadecimalBigIntegerConverter.getPattern(col).matcher("FFFFFFFFFFFFFFFF").matches());
	}
}
