package org.bitbucket.bradleysmithllc.etlunit.parser.datadiff;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.time.EtlTimeUtils;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateNearSpecificationTests {
	@Test
	public void currentDate() {
		DateNearSpecification spec = new DateNearSpecification(null, TimeUnit.NANOSECONDS, 0);

		Assert.assertTrue(spec.isReferenceDateCurrentDate());
	}

	@Test
	public void exact() {
		ZonedDateTime referenceDate = ZonedDateTime.now();
		DateNearSpecification spec = new DateNearSpecification(referenceDate, TimeUnit.NANOSECONDS, 0);

		Assert.assertEquals(localDateString(referenceDate), localDateString(spec.latestDate()));
		Assert.assertEquals(localDateString(referenceDate), localDateString(spec.earliestDate()));
	}

	static String localDateString(ZonedDateTime referenceDate) {
		return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(referenceDate);
	}

	@Test
	public void belowOldest() {
		ZonedDateTime referenceDate = EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000");
		DateNearSpecification spec = new DateNearSpecification(referenceDate, TimeUnit.MILLISECONDS, 100);

		Assert.assertEquals(zdt("2018-01-01T23:59:59.900"), spec.earliestDate());
		Assert.assertEquals(zdt("2018-01-02T00:00:00.100"), spec.latestDate());
	}

	@Test
	public void rangeMatchesLower() {
		DateNearSpecification spec = new DateNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000"), TimeUnit.DAYS, 1);
		NearDiffResult test = spec.test("2018-01-01");

		Assert.assertTrue(test.comparisonSucceeded());
		Assert.assertEquals("==", test.getExplanation());
	}

	@Test
	public void rangeMatchesUpper() {
		DateNearSpecification spec = new DateNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000"), TimeUnit.DAYS, 1);
		NearDiffResult test = spec.test("2018-01-03");

		Assert.assertTrue(test.comparisonSucceeded());
		Assert.assertEquals("==", test.getExplanation());
	}

	@Test
	public void rangeExceedsLower() {
		DateNearSpecification spec = new DateNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-03 00:00:00.000"), TimeUnit.DAYS, 1);
		NearDiffResult test = spec.test("2018-01-01");

		Assert.assertFalse(test.comparisonSucceeded());
		Assert.assertEquals("Comparison date 2018-01-01 00:00:00.000 is before earliest allowable date - 2018-01-02 00:00:00.000 <= ref <= 2018-01-04 00:00:00.000", test.getExplanation());
	}

	@Test
	public void rangeExceedsUpper() {
		DateNearSpecification spec = new DateNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000"), TimeUnit.DAYS, 1);
		NearDiffResult test = spec.test("2018-01-04");

		Assert.assertFalse(test.comparisonSucceeded());
		Assert.assertEquals("Comparison date 2018-01-04 00:00:00.000 is after latest allowable date - 2018-01-01 00:00:00.000 <= ref <= 2018-01-03 00:00:00.000", test.getExplanation());
	}

	@Test
	public void rangeSample() {
		DateNearSpecification spec = new DateNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2020-10-21 00:00:00.000"), TimeUnit.DAYS, 1);
		NearDiffResult test = spec.test("2020-10-21");

		Assert.assertTrue(test.comparisonSucceeded());
	}

	public static ZonedDateTime zdt(String sourceValueText) {
		return LocalDateTime.parse(sourceValueText, DateTimeFormatter.ISO_LOCAL_DATE_TIME).atZone(ZoneId.of("UTC"));
	}

	public static Date date(String s) {
		return Timestamp.valueOf(s);
	}
}
