package org.bitbucket.bradleysmithllc.etlunit.feature.reports.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.report.TestStatistics;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TestStatisticsTest
{
	@Test
	public void testZero()
	{
		TestStatistics ts = new TestStatistics();

		Assert.assertEquals("00.000", ts.getDurationFormatted());
	}

	@Test
	public void testOneMilli()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(1L);

		Assert.assertEquals("00.001", ts.getDurationFormatted());
	}

	@Test
	public void testOneMilliTwo()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(10L);

		Assert.assertEquals("00.010", ts.getDurationFormatted());
	}

	@Test
	public void testTwoMillis()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(11L);

		Assert.assertEquals("00.011", ts.getDurationFormatted());
	}

	@Test
	public void testThreeMillis()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(100L);

		Assert.assertEquals("00.100", ts.getDurationFormatted());
	}

	@Test
	public void testThreeMillisTwo()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(101L);

		Assert.assertEquals("00.101", ts.getDurationFormatted());
	}

	@Test
	public void testThreeMillisThree()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(111L);

		Assert.assertEquals("00.111", ts.getDurationFormatted());
	}

	@Test
	public void testOneSecond()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(1000L);

		Assert.assertEquals("01.000", ts.getDurationFormatted());
	}

	@Test
	public void testTwoSeconds()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(10000L);

		Assert.assertEquals("10.000", ts.getDurationFormatted());
	}

	@Test
	public void testTwoSecondsTwo()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(11000L);

		Assert.assertEquals("11.000", ts.getDurationFormatted());
	}

	@Test
	public void testOneMinute()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(60000L);

		Assert.assertEquals("01:00.000", ts.getDurationFormatted());
	}

	@Test
	public void testTwoMinutes()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(600000L);

		Assert.assertEquals("10:00.000", ts.getDurationFormatted());
	}

	@Test
	public void testTwoMinutesTwo()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(660000L);

		Assert.assertEquals("11:00.000", ts.getDurationFormatted());
	}

	@Test
	public void testOneHour()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(TimeUnit.HOURS.toMillis(1));

		Assert.assertEquals("01:00:00.000", ts.getDurationFormatted());
	}

	@Test
	public void testTwoHours()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(TimeUnit.HOURS.toMillis(10));

		Assert.assertEquals("10:00:00.000", ts.getDurationFormatted());
	}

	@Test
	public void testTwoHoursTwo()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(TimeUnit.HOURS.toMillis(11));

		Assert.assertEquals("11:00:00.000", ts.getDurationFormatted());
	}

	@Test
	public void testAll()
	{
		TestStatistics ts = new TestStatistics();
		ts.addDuration(TimeUnit.HOURS.toMillis(11) + TimeUnit.MINUTES.toMillis(11) + TimeUnit.SECONDS.toMillis(11) + 111);

		Assert.assertEquals("11:11:11.111", ts.getDurationFormatted());
	}
}
