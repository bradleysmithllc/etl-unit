package org.bitbucket.bradleysmithllc.etlunit.io.file.converter.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.HexadecimalBigIntegerConverter;
import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.HexadecimalIntegerConverter;
import org.junit.Assert;
import org.junit.Test;

public class HexadecimalBigIntegerConverterTest extends BaseConverterSupport {

	private HexadecimalBigIntegerConverter numericConverter = new HexadecimalBigIntegerConverter();

	@Test
	public void format()
	{
		Assert.assertEquals("10", numericConverter.format(new Long(16L), null));
		Assert.assertEquals("ffff", numericConverter.format(new Long(65535L), null));
	}

	@Test
	public void parse()
	{
		Assert.assertEquals(16L, numericConverter.parse("10", null));
		Assert.assertEquals(65535L, numericConverter.parse("ffff", null));
		Assert.assertEquals(65535L, numericConverter.parse("fFFf", null));
	}
}
