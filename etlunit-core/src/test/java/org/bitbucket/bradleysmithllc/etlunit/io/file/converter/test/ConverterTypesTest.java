package org.bitbucket.bradleysmithllc.etlunit.io.file.converter.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.DataConverter;
import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.*;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.regex.Pattern;

public class ConverterTypesTest extends BaseConverterSupport {
	private Object [][] CONVERTERS =
	{
			{new BigintConverter(), Long.class, "1"},
			{new BitConverter(), Boolean.class, "1"},
			{new BlobConverter(), String.class, "b"},
			{new BooleanConverter(), Boolean.class, "true"},
			{new CharConverter(), String.class, "hi"},
			{new ClobConverter(), Clob.class, "clob"},
			{new DateConverter(), LocalDate.class, "2011-01-01"},
			{new DecimalConverter(), BigDecimal.class, "1.0"},
			{new DoubleConverter(), Double.class, "1.0"},
			{new FloatConverter(), Float.class, "1.0"},
			{new HexadecimalBigIntegerConverter(), Long.class, "1"},
			{new HexadecimalIntegerConverter(), Integer.class, "1"},
			{new IntegerConverter(), Integer.class, "1"},
			{new NumericConverter(), BigDecimal.class, "1"},
			{new RealConverter(), Float.class, "1"},
			{new SmallIntConverter(), Short.class, "1"},
			{new TimeConverter(), LocalTime.class, "12:31:32"},
			{new TimeWithTimezoneConverter(), LocalTime.class, "12:31:32"},
			{new TimestampConverter(), LocalDateTime.class, "2011-01-01 11:11:22.000"},
			{new TimestampWithTimezoneConverter(), LocalDateTime.class, "2011-01-01 11:11:22.000"},
			{new TinyIntConverter(), Byte.class, "1"},
			{new VarcharConverter(), String.class, "str"}
	};

	@Test
	public void parse0() throws ParseException {
		for (Object [] i : CONVERTERS)
		{
			DataConverter dv = (DataConverter) i[0];
			Class resultType = (Class) i[1];
			String input = (String) i[2];

			// perform a matches, then parse, checking return type, then format
			Pattern pattern = dv.getPattern(col);
			if (pattern != null)
			{
				Assert.assertTrue("Pattern does not match.  " + dv.getClass() + ".", pattern.matcher(input).matches());
			}

			// parsethe input to an object and verify that the class type matches
			Object resObj = dv.parse(input, col);

			Assert.assertTrue("Converter returned the wrong object type: " + dv.getClass() + ", saw: " + resObj.getClass() + ", wanted: " + resultType, resultType.isAssignableFrom(resObj.getClass()));
		}
	}
}
