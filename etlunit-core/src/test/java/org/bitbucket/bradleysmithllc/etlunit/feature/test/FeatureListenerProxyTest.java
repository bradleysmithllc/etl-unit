package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureListenerProxy;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FeatureListenerProxyTest
{
	@Test
	public void noFeatures() throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		FeatureListenerProxy flp = new FeatureListenerProxy(Collections.EMPTY_LIST);

		flp.process(null, null, null, null, null, 0);
	}

	@Test
	public void noListeners() throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		FeatureListenerProxy flp = new FeatureListenerProxy(Arrays.asList(
					new Feature[] {new AbstractFeature(){
						@Override
						public List<ClassListener> getListenerList()
						{
							return Collections.EMPTY_LIST;
						}
					}})
		);

		flp.process(null, null, null, null, null, 0);
	}

	@Test
	public void nullListeners() throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		FeatureListenerProxy flp = new FeatureListenerProxy(Arrays.asList(
				new Feature[] {new AbstractFeature(){
					@Override
					public List<ClassListener> getListenerList()
					{
						return null;
					}
				}})
		);

		flp.process(null, null, null, null, null, 0);
	}

	@Test
	public void listenersListen() throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		FeatureListenerProxy flp = new FeatureListenerProxy(Arrays.asList(
				new Feature[] {new AbstractFeature(){
					@Override
					public List<ClassListener> getListenerList()
					{
						return Arrays.asList(new ClassListener[]{new HandleListener(1)});
					}
				}
				})
		);

		Assert.assertEquals(ClassResponder.action_code.defer, flp.process(null, null, null, null, null, 0));
		Assert.assertEquals(ClassResponder.action_code.handled, flp.process(null, null, null, null, null, 1));
		Assert.assertEquals(ClassResponder.action_code.defer, flp.process(null, null, null, null, null, 2));
	}

	@Test
	public void listenersListenInOrder() throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		FeatureListenerProxy flp = new FeatureListenerProxy(Arrays.asList(
				new Feature[] {new AbstractFeature(){
					@Override
					public List<ClassListener> getListenerList()
					{
						return Arrays.asList(new ClassListener[]{
										new HandleListener(1),
										new RejectListener(2),
									new HandleListener(3)
								}
						);
					}
				}
				})
		);

		Assert.assertEquals(ClassResponder.action_code.defer, flp.process(null, null, null, null, null, 0));
		Assert.assertEquals(ClassResponder.action_code.handled, flp.process(null, null, null, null, null, 1));
		Assert.assertEquals(ClassResponder.action_code.reject, flp.process(null, null, null, null, null, 2));
		Assert.assertEquals(ClassResponder.action_code.handled, flp.process(null, null, null, null, null, 3));
		Assert.assertEquals(ClassResponder.action_code.defer, flp.process(null, null, null, null, null, 4));
	}

	private class HandleListener extends NullClassListener
	{
		private final int texecutor;

		private HandleListener(int exe)
		{
			texecutor = exe;
		}

		@Override
		public action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			if (executor == texecutor)
			{
				return action_code.handled;
			}

			return action_code.defer;
		}

		@Override
		public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			return process(null, op, obj, context, econtext, executor);
		}
	}

	private class RejectListener extends NullClassListener
	{
		private final int texecutor;

		private RejectListener(int exe)
		{
			texecutor = exe;
		}

		@Override
		public action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			if (executor == texecutor)
			{
				return action_code.reject;
			}

			return action_code.defer;
		}

		@Override
		public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			return process(null, op, obj, context, econtext, executor);
		}
	}
}
