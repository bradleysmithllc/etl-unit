package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CoreAnnotationsTest extends BaseFeatureModuleTest
{
	@Test
	public void supportedAnnotations() throws IOException
	{
		startTest();
	}

	@Override
	protected void prepareTest()
	{
		// override start time to some stable value
		runtimeSupport.setTestStartTime(LocalDateTime.of(2014, 1, 2, 0, 0));
	}

	@Override
	protected void assertions(TestResults results, int pass)
	{
		Assert.assertEquals(16, results.getNumTestsSelected());
		Assert.assertEquals(16, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());
		Assert.assertEquals(3, results.getMetrics().getNumberIgnored());

		Map<String, TestClassResult> rbc = results.getResultsMapByClassName();

		TestClassResult testClassResult = rbc.get("[default].core_annotations_test_no_defaults");
		ETLTestClass tc = testClassResult.getTestClass();

		Assert.assertEquals("core_annotations_test_no_defaults", tc.getName());
		Assert.assertEquals("To do", tc.getDescription());

		Assert.assertEquals("null", tc.getTestMethods().get(0).getName());
		Assert.assertEquals("To do again", tc.getTestMethods().get(0).getDescription());
	}

	@Override
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList((Feature) new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "operationDefaultFeature";
			}

			@Override
			public ClassListener getListener()
			{
				return new NullClassListener()
				{
					@Override
					public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, final int executor)
							throws TestAssertionFailure, TestExecutionError, TestWarning
					{
						if (
							op.getOperationName().equals("operationOverridable") ||
							op.getOperationName().equals("operationOverriddenClass")
						)
						{
							// build in assertions here
							Map<String, ETLTestValueObject> mapValue = obj.getValueAsMap();

							for (Map.Entry<String, ETLTestValueObject> entry : mapValue.entrySet())
							{
								String key = entry.getKey();
								ETLTestValueObject value = entry.getValue();

								if (key.startsWith("assert_"))
								{
									String expected = value.getValueAsString();

									// lookup the assertion target
									String actualKey = key.substring(7);
									ETLTestValueObject actualObject = mapValue.get(actualKey);
									String actual = actualObject.getValueAsString();

									if (!expected.equals(actual))
									{
										throw new IllegalArgumentException(
											"Expected does not match actual:  expected-key: '" + key
													+ "', expected-value: '" + expected + ", actual-key: '"
													+ actualKey + "', actual-value: '" + actual + "'"
										);
									}
								}
							}

							return action_code.handled;
						}
						else
						{
							return action_code.defer;
						}
					}
				};
			}
		});
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSourceFromClasspath(null, "core_annotations_test.etlunit");
	}
}
