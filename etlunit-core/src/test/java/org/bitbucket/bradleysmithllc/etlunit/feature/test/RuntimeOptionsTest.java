package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class RuntimeOptionsTest extends BaseFeatureModuleTest
{
	@Override
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList(featureA, featureB);
	}

	@Override
	protected JSonBuilderProxy getConfigurationOverrides()
	{
		return new JSonBuilderProxy()
				.object()
					.key("options")
						.object()
							.key("feature")
								.object()
									.key("option1")
										.object()
											.key("enabled")
											.value(true)
										.endObject()
									.key("option2")
										.object()
											.key("integer-value")
											.value(1)
										.endObject()
									.key("option3")
										.object()
											.key("string-value")
											.value("Hello")
										.endObject()
									.key("optionB")
										.object()
											.key("string-value")
											.value("Options")
										.endObject()
								.key("optionD")
									.object()
										.key("string-value")
										.value("Options")
									.endObject()
								.endObject()
							.key("feature2")
								.object()
									.key("option1")
										.object()
											.key("string-value")
											.value("Hello2")
										.endObject()
								.endObject()
						.endObject()
				.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
	}

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides()
	{
		return Arrays.asList(new RuntimeOption("feature.optionC", "Overrides"), new RuntimeOption("feature.optionD", "Overrides"));
	}

	@Test
	public void options()
	{
		startTest();

		Assert.assertNotNull(mlist);

		Assert.assertTrue(mlist.containsKey(featureA));
		Assert.assertTrue(mlist.containsKey(featureB));

		Assert.assertEquals(9, mlist.get(featureA).size());
		Assert.assertEquals(1, mlist.get(featureB).size());

		Assert.assertSame(featureA, mlist.get(featureA).get(0).getFeature());
		Assert.assertSame(featureA, mlist.get(featureA).get(1).getFeature());
		Assert.assertSame(featureA, mlist.get(featureA).get(2).getFeature());
		Assert.assertSame(featureA, mlist.get(featureA).get(3).getFeature());
		Assert.assertSame(featureA, mlist.get(featureA).get(4).getFeature());

		Assert.assertSame(featureA, mlist.get(featureA).get(5).getFeature());
		Assert.assertSame(featureA, mlist.get(featureA).get(6).getFeature());
		Assert.assertSame(featureA, mlist.get(featureA).get(7).getFeature());
		Assert.assertSame(featureA, mlist.get(featureA).get(8).getFeature());

		Assert.assertNotNull(mlist.get(featureA).get(0).getDescriptor());
		Assert.assertNotNull(mlist.get(featureA).get(1).getDescriptor());
		Assert.assertNotNull(mlist.get(featureA).get(2).getDescriptor());

		Assert.assertSame(featureB, mlist.get(featureB).get(0).getFeature());
		Assert.assertNotNull(mlist.get(featureB).get(0).getDescriptor());

		RuntimeSupport rs = etlTestVM.getRuntimeSupport();

		RuntimeOption op = rs.getRuntimeOption("feature.option1");
		Assert.assertNotNull(op);
		Assert.assertEquals(true, op.isEnabled());

		op = rs.getRuntimeOption("feature.option2");
		Assert.assertNotNull(op);
		Assert.assertEquals(1, op.getIntegerValue());

		op = rs.getRuntimeOption("feature.option3");
		Assert.assertNotNull(op);
		Assert.assertEquals("Hello", op.getStringValue());

		List<RuntimeOption> opList = rs.getRuntimeOptions("feature");
		Assert.assertNotNull(opList);

		Assert.assertEquals(6, opList.size());

		opList = rs.getRuntimeOptions("feature2");
		Assert.assertNotNull(opList);

		Assert.assertEquals(1, opList.size());

		Assert.assertTrue(rof2o1 == opList.get(0));

		Assert.assertNotNull(rof2o1);
		Assert.assertNotNull(rof1o1);
		Assert.assertNotNull(rof1o2);
		Assert.assertNotNull(rof1o3);
		Assert.assertNotNull(rof1o4);
		Assert.assertNotNull(rof1o5);

		Assert.assertNotNull(rof1oA);
		Assert.assertNotNull(rof1oB);
		Assert.assertNotNull(rof1oC);
		Assert.assertNotNull(rof1oD);

		Assert.assertEquals("Descriptor", rof1oA.getStringValue());
		Assert.assertEquals("Options", rof1oB.getStringValue());
		Assert.assertEquals("Overrides", rof1oC.getStringValue());
		Assert.assertEquals("Overrides", rof1oD.getStringValue());

		Assert.assertEquals(10, rof1o4.getIntegerValue());
		Assert.assertEquals("Hello", rof1o5.getStringValue());
	}

	RuntimeOption rof2o1 = null;

	RuntimeOption rof1o1 = null;
	RuntimeOption rof1o2 = null;
	RuntimeOption rof1o3 = null;
	RuntimeOption rof1o4 = null;
	RuntimeOption rof1o5 = null;

	RuntimeOption rof1oA = null;
	RuntimeOption rof1oB = null;
	RuntimeOption rof1oC = null;
	RuntimeOption rof1oD = null;

	MapList<Feature, RuntimeOption> mlist = null;

	private final Feature featureB = new AbstractFeature()
	{
		@Inject
		public void setFeatureOptionsMap(MapList<Feature, RuntimeOption> mist)
		{
			mlist = mist;
		}

		@Override
		public String getFeatureName()
		{
			return "feature2";
		}

		@Override
		public FeatureMetaInfo getMetaInfo()
		{
			final Feature feature = this;

			return new FeatureMetaInfo()
			{
				@Override
				public String getFeatureName()
				{
					return null;
				}

				@Override
				public String getFeatureConfiguration()
				{
					return null;
				}

				@Override
				public JsonSchema getFeatureConfigurationValidator()
				{
					return null;
				}

				@Override
				public JsonNode getFeatureConfigurationValidatorNode() {
					return null;
				}

				@Override
				public Map<String, FeatureOperation> getExportedOperations()
				{
					return null;
				}

				@Override
				public Map<String, FeatureAnnotation> getExportedAnnotations()
				{
					return null;
				}

				@Override
				public boolean isInternalFeature()
				{
					return false;
				}

				@Override
				public String getFeatureUsage()
				{
					return null;
				}

				@Override
				public List<RuntimeOptionDescriptor> getOptions()
				{
					List<RuntimeOptionDescriptor> list = new ArrayList<RuntimeOptionDescriptor>();

					list.add(new RuntimeOptionDescriptorImpl("option1", "", ""));

					return list;
				}

				@Override
				public String getDescribingClassName()
				{
					return getDescribing().getClass().getName();
				}

				@Override
				public Feature getDescribing()
				{
					return feature;
				}
			};
		}
	};

	private final Feature featureA = new AbstractFeature()
	{
		@Override
		public String getFeatureName()
		{
			return "feature";
		}

		@Override
		public FeatureMetaInfo getMetaInfo()
		{
			final Feature feature = this;

			return new FeatureMetaInfo()
			{
				@Override
				public String getDescribingClassName()
				{
					return getDescribing().getClass().getName();
				}

				@Override
				public Feature getDescribing()
				{
					return feature;
				}

				@Override
				public String getFeatureName()
				{
					return null;
				}

				@Override
				public String getFeatureConfiguration()
				{
					return null;
				}

				@Override
				public JsonSchema getFeatureConfigurationValidator()
				{
					return null;
				}

				@Override
				public JsonNode getFeatureConfigurationValidatorNode() {
					return null;
				}

				@Override
				public Map<String, FeatureOperation> getExportedOperations()
				{
					return null;
				}

				@Override
				public Map<String, FeatureAnnotation> getExportedAnnotations()
				{
					return null;
				}

				@Override
				public boolean isInternalFeature()
				{
					return false;
				}

				@Override
				public String getFeatureUsage()
				{
					return null;
				}

				@Override
				public List<RuntimeOptionDescriptor> getOptions()
				{
					List<RuntimeOptionDescriptor> list = new ArrayList<RuntimeOptionDescriptor>();

					list.add(new RuntimeOptionDescriptorImpl("option1", "", false));
					list.add(new RuntimeOptionDescriptorImpl("option2", "", 7));
					list.add(new RuntimeOptionDescriptorImpl("option3", "", ""));
					list.add(new RuntimeOptionDescriptorImpl("option4", "", 10));
					list.add(new RuntimeOptionDescriptorImpl("option5", "", "Hello"));

					list.add(new RuntimeOptionDescriptorImpl("optionA", "", "Descriptor"));
					list.add(new RuntimeOptionDescriptorImpl("optionB", "", "Descriptor"));
					list.add(new RuntimeOptionDescriptorImpl("optionC", "", "Descriptor"));
					list.add(new RuntimeOptionDescriptorImpl("optionD", "", "Descriptor"));

					return list;
				}
			};
		}

		@Inject
		public void setOption21(@Named("feature2.option1") RuntimeOption option1)
		{
			rof2o1 = option1;
		}

		@Inject
		public void setOption11(@Named("feature.option1") RuntimeOption option1)
		{
			rof1o1 = option1;
		}

		@Inject
		public void setOption12(@Named("feature.option2") RuntimeOption option1)
		{
			rof1o2 = option1;
		}

		@Inject
		public void setOption13(@Named("feature.option3") RuntimeOption option1)
		{
			rof1o3 = option1;
		}

		@Inject
		public void setOption14(@Named("feature.option4") RuntimeOption option1)
		{
			rof1o4 = option1;
		}

		@Inject
		public void setOption15(@Named("feature.option5") RuntimeOption option1)
		{
			rof1o5 = option1;
		}

		@Inject
		public void setOption1A(@Named("feature.optionA") RuntimeOption option1)
		{
			rof1oA = option1;
		}

		@Inject
		public void setOption1B(@Named("feature.optionB") RuntimeOption option1)
		{
			rof1oB = option1;
		}

		@Inject
		public void setOption1C(@Named("feature.optionC") RuntimeOption option1)
		{
			rof1oC = option1;
		}

		@Inject
		public void setOption1D(@Named("feature.optionD") RuntimeOption option1)
		{
			rof1oD = option1;
		}
	};

	@Test
	public void printable()
	{
		Assert.assertNull(new RuntimeOption("Name").printableValue());

		Assert.assertEquals("true", new RuntimeOption("Name", true).printableValue());
		Assert.assertEquals("false", new RuntimeOption("Name", false).printableValue());

		Assert.assertEquals("-1", new RuntimeOption("Name", -1).printableValue());
		Assert.assertEquals("0", new RuntimeOption("Name", 0).printableValue());
		Assert.assertEquals("1", new RuntimeOption("Name", 1).printableValue());

		Assert.assertEquals("hi", new RuntimeOption("Name", "hi").printableValue());
	}
}
