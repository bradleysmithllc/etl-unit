package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.TestClassReference;
import org.bitbucket.bradleysmithllc.etlunit.TestClasses;
import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeCatalogImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeClassifierImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypePackageImpl;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;

public class JsonSerializableTest {
	enum testType
	{
		type,
		package_,
		catalog,
		classifier,
		referenceFileType,
		testClassReference,
		testClasses
	}

	@Test
	public void runAll() throws IOException {
		JsonNode json = JsonLoader.fromURL(getClass().getResource("/jsonSerializeTests.json"));

		ObjectNode on = (ObjectNode) json.get("tests");

		Iterator<String> it = on.fieldNames();

		while (it.hasNext())
		{
			String jtypeName = it.next();
			ArrayNode jnode = (ArrayNode) on.get(jtypeName);

			// this is an array
			for (JsonNode node : jnode) {
				// run bidirectional test
				JsonSerializable jser = null;

				switch (testType.valueOf(jtypeName)) {
					case type:
						jser = new ReferenceFileTypeImpl("a", "a.b");
						break;
					case package_:
						jser = new ReferenceFileTypePackageImpl("a");
						break;
					case catalog:
						jser = new ReferenceFileTypeCatalogImpl();
						break;
					case classifier:
						jser = new ReferenceFileTypeClassifierImpl("cls");
						break;
					case referenceFileType:
						jser = new ReferenceFileTypeImpl("name", "id");
						break;
					case testClassReference:
						jser = new TestClassReference("name");
						break;
					case testClasses:
						jser = new TestClasses();
						break;
					default:
						throw new UnsupportedOperationException();
				}

				// do from / to
				jser.fromJson(node);
				JsonNode newNode = toJson(jser);

				Assert.assertEquals(jtypeName, node, newNode);
			}
		}
	}

	private JsonNode toJson(JsonSerializable timpl1) throws IOException {
		StringWriter stw = new StringWriter();
		JsonWriter jsw = new JsonWriter(stw);

		jsw.beginObject();
		timpl1.toJson(jsw);
		jsw.endObject();

		jsw.close();

		return JsonLoader.fromString(stw.toString());
	}
}
