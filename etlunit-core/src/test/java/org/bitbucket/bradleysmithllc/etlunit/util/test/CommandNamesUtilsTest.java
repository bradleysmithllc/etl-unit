package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.CommandNamesUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class CommandNamesUtilsTest {
	@Test
	public void empty() {
		Assert.assertEquals(Collections.EMPTY_MAP, CommandNamesUtils.createShortcutsFor(Collections.emptySet()));
	}

	@Test
	public void noSolutionDashes() {
		Assert.assertEquals(Collections.EMPTY_MAP, CommandNamesUtils.createShortcutsFor(set("time-is-money", "tim-is-mushy")));
	}

	@Test
	public void noSolutionDashesAndWords() {
		Assert.assertEquals(Collections.EMPTY_MAP, CommandNamesUtils.createShortcutsFor(set("time-is-money", "tim", "ti", "t")));
	}

	@Test
	public void sameSame() {
		Assert.assertEquals(map("tim", "tim", "ti", "ti", "t", "t"), CommandNamesUtils.createShortcutsFor(set("tim", "ti", "t")));
	}

	@Test
	public void shortcuts() {
		Assert.assertEquals(map("tag", "t", "test", "te", "verify-reference-types", "vrt"), CommandNamesUtils.createShortcutsFor(set("tag", "test", "verify-reference-types")));
	}

	@Test
	public void dashing() {
		Assert.assertEquals(map("time-is-money", "tim", "left-right", "lr"), CommandNamesUtils.createShortcutsFor(set("time-is-money", "left-right")));
	}

	@Test
	public void dashify() {
		Assert.assertNull(CommandNamesUtils.dashify(""));
		Assert.assertNull(CommandNamesUtils.dashify("Hello"));
		Assert.assertNull(CommandNamesUtils.dashify("HelloWorld"));
		Assert.assertEquals("hw", CommandNamesUtils.dashify("Hello-World"));
	}

	private Map<String, String> map(String... data) {
		Map<String, String> out = new HashMap<>();

		for (int index = 0; index < data.length - 1; index += 2) {
		    out.put(data[index], data[index + 1]);
    }

		return out;
	}

	private Set<String> set(String... s) {
		Set<String> out = new HashSet<>();

		for (String name : s) {
			out.add(name);
		}

		return out;
	}
}
