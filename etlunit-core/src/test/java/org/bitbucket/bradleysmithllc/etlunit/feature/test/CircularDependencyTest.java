package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CircularDependencyTest extends BaseFeatureModuleTest
{
	@Override
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList(featureA, featureB);
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
	}

	@Test(expected = Exception.class)
	public void circularDependencies()
	{
		startTest();
	}

	private final Feature featureA = new AbstractFeature()
	{
		@Override
		public String getFeatureName()
		{
			return "a";
		}

		@Override
		public List<String> getPrerequisites()
		{
			return Arrays.asList("b");
		}
	};

	private final Feature featureB = new AbstractFeature()
	{
		@Override
		public String getFeatureName()
		{
			return "b";
		}

		@Override
		public List<String> getPrerequisites()
		{
			return Arrays.asList("a");
		}
	};
}
