package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.cli.Enumerations;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashSet;
import java.util.Set;

public class EnumerationsTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void negativeLength() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Length must be a positive integer");
		Enumerations.generateEnumerated(0, -1);
	}

	@Test
	public void zeroLength() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Length must be a positive integer");
		Enumerations.generateEnumerated(0, 0);
	}

	@Test
	public void oneLength() {
		Assert.assertEquals("0", Enumerations.generateEnumerated(0, 1));
		Assert.assertEquals("1", Enumerations.generateEnumerated(1, 1));
		Assert.assertEquals("2", Enumerations.generateEnumerated(2, 1));
		Assert.assertEquals("3", Enumerations.generateEnumerated(3, 1));
		Assert.assertEquals("4", Enumerations.generateEnumerated(4, 1));
		Assert.assertEquals("n", Enumerations.generateEnumerated(995, 1));
	}

	@Test
	public void twoLength() {
		Assert.assertEquals("00", Enumerations.generateEnumerated(0, 2));
		Assert.assertEquals("01", Enumerations.generateEnumerated(1, 2));
		Assert.assertEquals("02", Enumerations.generateEnumerated(2, 2));
		Assert.assertEquals("03", Enumerations.generateEnumerated(3, 2));
		Assert.assertEquals("04", Enumerations.generateEnumerated(4, 2));
		Assert.assertEquals("rn", Enumerations.generateEnumerated(995, 2));
	}

	@Test
	public void threeLength() {
		Assert.assertEquals("000", Enumerations.generateEnumerated(0, 3));
		Assert.assertEquals("001", Enumerations.generateEnumerated(1, 3));
		Assert.assertEquals("002", Enumerations.generateEnumerated(2, 3));
		Assert.assertEquals("003", Enumerations.generateEnumerated(3, 3));
		Assert.assertEquals("004", Enumerations.generateEnumerated(4, 3));
		Assert.assertEquals("0rn", Enumerations.generateEnumerated(995, 3));
	}

	@Test
	public void repeatsOne() {
		Assert.assertEquals(36, untilDups(1));
	}

	@Test
	public void repeatsTwo() {
		Assert.assertEquals(1296, untilDups(2));
	}

	@Test
	public void repeatsThree() {
		Assert.assertEquals(10000, untilDups(3));
	}

	private int untilDups(int length) {
		Set<String> set = new HashSet<>();

		for (int i = 0; i < 10000; i++) {
			String thisOne = Enumerations.generateEnumerated(i, length);

			if (!set.contains(thisOne)) {
				set.add(thisOne);
			} else {
				return i;
			}
		}

		return 10000;
	}
}
