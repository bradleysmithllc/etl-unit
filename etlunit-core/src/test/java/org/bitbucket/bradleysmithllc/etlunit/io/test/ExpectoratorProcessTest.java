package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.io.Expectorator;
import org.bitbucket.bradleysmithllc.etlunit.io.JavaForker;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

public class ExpectoratorProcessTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void expectoratorEcho() throws IOException, InterruptedException
	{
		BasicRuntimeSupport brs = new BasicRuntimeSupport();

		brs.setApplicationLogger(new PrintWriterLog());

		JavaForker forker = new JavaForker(brs);
		forker.setMainClass(Echo.class);
		forker.addSystemProperty("hi", "gi");

		File output = temporaryFolder.newFile("expectoratorEcho.process");

		forker.setOutput(output);
		ProcessFacade pr = forker.startProcess();

		pr.waitForStreams();

		Expectorator exp = new Expectorator(pr.getReader(), pr.getWriter());

		exp.expect(Echo.GREETING + "[\\n\\r]+(" + Echo.BLAH + "[\\n\\r]+)*" + Echo.PROMPT);

		for (int i = 0; i < 100; i++)
		{
			exp.sayAndExpect(System.currentTimeMillis() + "\n", "(" + Echo.BLAH + "[\\n\\r]+)*" + Echo.PROMPT, 100000L);

			if (i % 10 == 0)
			{
				System.out.println("Pass " + i / 10);
			}
		}

		exp.sayAndExpect("exit\n", "(" + Echo.BLAH + "[\\n\\r]+)*" + Echo.DEGREETING);

		pr.waitForCompletion();

		Assert.assertEquals(0, pr.getCompletionCode());

		String regex = Echo.GREETING
				+ "[\\n\\r]+("
				+ Echo.BLAH
				+ "[\\n\\r]+)*("
				+ Echo.PROMPT
				+ "\\d{1,20}[\\n\\r]+("
				+ Echo.BLAH
				+ "[\\n\\r]+)*){100}"
				+ Echo.PROMPT
				+ "exit[\\n\\r]+"
				+ Echo.DEGREETING
				+ "[\\n\\r]*";

		Pattern
				matchPattern =
				Pattern.compile(regex);

		String out = IOUtils.readFileToString(output);

		System.out.println(regex);
		Assert.assertTrue("Output fail", matchPattern.matcher(out).matches());
	}
}
