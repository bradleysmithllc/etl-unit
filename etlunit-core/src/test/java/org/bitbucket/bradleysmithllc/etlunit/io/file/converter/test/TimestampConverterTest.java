package org.bitbucket.bradleysmithllc.etlunit.io.file.converter.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.TimestampConverter;
import org.bitbucket.bradleysmithllc.etlunit.util.time.EtlTimeUtils;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.*;
import java.util.*;

public class TimestampConverterTest extends BaseConverterSupport
{
	TimestampConverter bcon = new TimestampConverter();

	@Test
	public void columnWithFormatHasNoPattern()
	{
		Assert.assertNotNull(bcon.getPattern(null));

		col.setFormat("");
		Assert.assertNull(bcon.getPattern(col));
	}

	@Test
	public void patternMatches()
	{
		Assert.assertTrue(bcon.getPattern(null).matcher("2013-01-01 00:00:00").matches());
		Assert.assertFalse(bcon.getPattern(null).matcher("2013-01-01 00:00:000").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("2013-01-01 00:00:00.0").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("2013-01-01 00:00:00.00").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("2013-01-01 00:00:00.000").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("2013-01-01 00:00:00.0000").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("2013-01-01 00:00:00.00000").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("2013-01-01 00:00:00.000000").matches());
		Assert.assertFalse(bcon.getPattern(null).matcher("2013-01-01 00:00:00.0000000").matches());
	}

	@Test
	public void lotsOfMillis()
	{
		List<String> in = Arrays.asList(
				"1970-01-24 01:10:10", "1970-01-24 01:10:10.000",
				"1970-01-24 01:10:10.1", "1970-01-24 01:10:10.100",
				"1970-01-24 01:10:10.01", "1970-01-24 01:10:10.010",
				"1970-01-24 01:10:10.001", "1970-01-24 01:10:10.001",
				"1970-01-24 01:10:10.00001", "1970-01-24 01:10:10.000"
		);

		for (int index = 0; index < in.size(); index += 2) {
			Assert.assertEquals(in.get(index + 1), bcon.format(EtlTimeUtils.localDateFromLocalDateTimeString(in.get(index)), col));
		}
	}

	//@Test
	public void timestampFormat()
	{
		ZonedDateTime zonedDateTime = ZonedDateTime.of(1970, 1, 24, 8, 10, 46, 10000,
				ZoneId.of("UTC")
		);

		Timestamp input = new Timestamp(
				zonedDateTime.toInstant().toEpochMilli()
		);

		Assert.assertEquals("1970-01-24 08:10:46.100", bcon.format(input, col));
	}

	@Test
	public void UTCtimestampFormat() throws ParseException {
		LocalDateTime parse = (LocalDateTime) bcon.parse("1968-10-09 09:01:34.000", col);

		Assert.assertEquals(EtlTimeUtils.localDateFromLocalDateTimeString("1968-10-09 09:01:34.000"), parse);
	}

	@Test
	public void customFormat()
	{
		LocalDateTime input = LocalDateTime.of(1970, 1, 24, 8, 10, 46, 10000);

		col.setFormat("yyyyMMdd");

		Assert.assertEquals("19700124", bcon.format(input, col));
	}

	@Test
	public void packedDate() throws ParseException {
		col.setFormat("yyyyMMdd");
		Assert.assertEquals(EtlTimeUtils.localDateFromLocalDateTimeString("1970-01-24 00:00:00.0"), bcon.parse("19700124", col));
	}

	@Test
	public void smallMillis() throws ParseException {
		Assert.assertEquals(EtlTimeUtils.localDateFromLocalDateTimeString("1970-01-24 01:10:10.1"), bcon.parse("1970-01-24 01:10:10.1", col));
	}

	@Test
	public void timestampParse() throws ParseException {
		Assert.assertEquals(EtlTimeUtils.localDateFromLocalDateTimeString("1970-01-24 01:10:10.100"), bcon.parse("1970-01-24 01:10:10.100", col));
	}

	@Test
	public void customParse() throws ParseException {
		col.setFormat("yyyyMMddHHmmssX");

		LocalDateTime parse = (LocalDateTime) bcon.parse("19700124000000Z", col);
		Assert.assertEquals(EtlTimeUtils.localDateFromLocalDateTimeString("1970-01-24 00:00:00.000"), parse);
	}

	/**
	 * Verifies that when the formatter is not lenient weird dates throw an exception.
	 * @throws ParseException
	 */
	@Test(expected = ParseException.class)
	public void customParseNotLenient() throws ParseException {
		Timestamp input = new Timestamp(
				ZonedDateTime.of(1970, 1, 24, 0, 0, 0, 0,
						ZoneId.of("UTC")
				).toInstant().toEpochMilli()
		);

		col.setFormat("yyyyMMdd");

		Assert.assertEquals(2008800000L, ( (Timestamp) bcon.parse("1970-01-24", col)).getTime());
	}

	/**
	 * Verifies that when the formatter is lenient weird dates are okay.
	 * @throws ParseException
	 */
	//@Test
	public void customParseLenient() throws ParseException {
		Timestamp input = new Timestamp(
				ZonedDateTime.of(1970, 1, 6, 0, 0, 0, 0,
						ZoneId.of("UTC")
				).toInstant().toEpochMilli()
		);

		col.setFormat("yyyyMMdd");
		col.setLenientFormatter(true);

		Assert.assertEquals(28879200000L, ( (Timestamp) bcon.parse("1971-01-06", col)).getTime());
	}
}
