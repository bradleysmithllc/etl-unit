package org.bitbucket.bradleysmithllc.etlunit.broadcaster.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadedVariableContextTest
{
	public static final int NUM_THREADS = 15;
	private VariableContext variableContext = new VariableContextImpl();

	private static int idCounter;

	private class VarThread implements Runnable
	{
		private final AtomicInteger failureCount;
		private final AtomicInteger runCount;
		private VariableContext subVariableContext = variableContext.createNestedScope(false);
		private VariableContext subSubVariableContext = subVariableContext.createNestedScope(true);

		private final CountDownLatch countDownLatch;
		private final String expected;

		VarThread(CountDownLatch cdl, AtomicInteger failureCount, AtomicInteger runCount)
		{
			this.runCount = runCount;
			this.failureCount = failureCount;
			countDownLatch = cdl;

			// spin and set some variables, then process a velocity template to verify
			String id1 = UUID.randomUUID().toString();
			String id2 = UUID.randomUUID().toString();
			String id3 = UUID.randomUUID().toString();
			String id4 = UUID.randomUUID().toString();

			expected = new StringBuilder(id1).append('-').append(id2).append('-').append(id3).append('-').append(id4).toString();

			subVariableContext.declareAndSetStringValue("var1", id1);
			subSubVariableContext.declareAndSetStringValue("var2", id2);
			subVariableContext.declareAndSetStringValue("var3", id3);
			subSubVariableContext.declareAndSetStringValue("var4", id4);
		}

		@Override
		public void run() {
			try
			{
				for (int i = 0; i < 10000; i++)
				{
					runCount.incrementAndGet();
					// run through a velocity context, looking for problems
					String actual = VelocityUtil.writeTemplate("${var1}-${var2}-${var3}-${var4}", subSubVariableContext);

					// assert
					if (!actual.equals(expected))
					{
						failureCount.incrementAndGet();
						throw new IllegalArgumentException(actual + ":" + expected);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally
			{
				countDownLatch.countDown();
			}
		}
	}

	@Test
	public void run() throws InterruptedException {
		CountDownLatch cdl = new CountDownLatch(NUM_THREADS);
		AtomicInteger failureCount = new AtomicInteger();
		AtomicInteger runCount = new AtomicInteger();

		for (int i = 0; i < NUM_THREADS; i++)
		{
			new Thread(new VarThread(cdl, failureCount, runCount)).start();
		}

		cdl.await();
		Assert.assertEquals(runCount.get() + " tests run", 0, failureCount.get());
	}
}
