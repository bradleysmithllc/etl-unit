package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.stream.JsonWriter;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.Configuration;
import org.bitbucket.bradleysmithllc.etlunit.SystemOutDiffManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.Incomplete;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

@Incomplete
public class ConfigurationTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private File userHome;
	private String currHome;

	@Before
	public void setupHome() throws IOException {
		userHome = temporaryFolder.newFolder();

		// set it in the properties so that the config class picks it up
		currHome = System.getProperty("user.home");
		System.setProperty("user.home", userHome.getAbsolutePath());
	}

	@After
	public void resetHome()
	{
		System.setProperty("user.home", currHome);
	}

	@Test
	public void userHome() throws IOException, ParseException {
		// create a basic configuration and verify that it loads from the home dir
		File config = Configuration.getEtlunitUserConfig();

		JsonWriter wr = new JsonWriter(new FileWriter(config));

		wr
		.beginObject()
			.name("options")
				.beginObject()
					.name("etlunit")
						.beginObject()
							.name("test")
								.beginObject()
									.name("integer-value").value(1)
								.endObject()
						.endObject()
				.endObject()
			.endObject();

		wr.close();

		Configuration cconfig = Configuration.loadFromEnvironment(null, (String []) null, null);

		Assert.assertEquals("1", cconfig.query("options.etlunit.test.integer-value").getValueAsString());
	}

	@Test
	public void userHomeProfile() throws IOException, ParseException {
		// create a basic configuration and verify that it loads from the home dir
		File config = Configuration.getEtlunitUserProfile("ci");

		JsonWriter wr = new JsonWriter(new FileWriter(config));

		wr
				.beginObject()
				.name("options")
				.beginObject()
				.name("etlunit")
				.beginObject()
				.name("test")
				.beginObject()
				.name("integer-value").value(1)
				.endObject()
				.endObject()
				.endObject()
				.endObject();

		wr.close();

		Configuration cconfig = Configuration.loadFromEnvironment(null, "ci", null);

		Assert.assertEquals("1", cconfig.query("options.etlunit.test.integer-value").getValueAsString());
	}

	@Test
	public void configurationFromStringEmpty()
	{
		Configuration con = new Configuration("");
	}

	@Test
	public void configurationFromStringBare()
	{
		Configuration con = new Configuration("source: 'data'");

		Assert.assertEquals("data", con.query("source").getValueAsString());
	}

	@Test
	public void configurationFromString()
	{
		Configuration con = new Configuration("{source: 'data'}");

		Assert.assertEquals("data", con.query("source").getValueAsString());
	}

	@Test
	public void configurationFromStringPadded()
	{
		Configuration con = new Configuration("      { source: 'data'    } ");

		Assert.assertEquals("data", con.query("source").getValueAsString());
	}

	@Test
	public void configurationFromJSONBuilderProxy()
	{
		Configuration
				con =
				new Configuration(new JSonBuilderProxy().object().key("source").value("data").endObject().toString());

		Assert.assertEquals("data", con.query("source").getValueAsString());
	}

	/**
	 * This is okay now - defaults to all empty
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test
	public void noConifguration() throws IOException, ParseException
	{
		Configuration.loadFromEnvironment(null, (String []) null, null);
	}

	File configRoot1;
	File configRoot2;

	File remoteUrlRoot;
	File remoteConfigRoot;

	URLClassLoader loader;

	@Before
	public void createTestRoots() throws IOException
	{
		configRoot1 = temporaryFolder.newFolder();
		configRoot1.mkdirs();

		configRoot2 = temporaryFolder.newFolder();
		configRoot2.mkdirs();

		remoteUrlRoot = temporaryFolder.newFolder();
		remoteUrlRoot.mkdirs();

		remoteConfigRoot = new File(remoteUrlRoot, "config");
		remoteConfigRoot.mkdirs();

		loader = new URLClassLoader(new URL[]{remoteUrlRoot.toURI().toURL()});
	}

	@Test
	public void twoConfigRoots() throws IOException, ParseException
	{
		File configFile = new File(configRoot1, "etlunit.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property: 'value'}"));

		configFile = new File(configRoot2, "etlunit.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property2: 'value2'}"));

		Configuration config = Configuration.loadFromEnvironment(new File [] {configRoot1, configRoot2}, (String []) null, null);

		Assert.assertEquals("value", config.query("property").getValueAsString());
		Assert.assertNull(config.query("property1"));
		Assert.assertEquals("value2", config.query("property2").getValueAsString());
	}

	@Test
	public void twoConfigRootsOneIgnored() throws IOException, ParseException
	{
		File configFile = new File(configRoot1, "etlunit.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property: 'value'}"));

		configFile = new File(configRoot2, "etlunit.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property2: 'value2'}"));

		Configuration config = Configuration.loadFromEnvironment(new File [] {configRoot1}, (String []) null, null);

		Assert.assertEquals("value", config.query("property").getValueAsString());
		Assert.assertNull(config.query("property2"));
	}

	@Test
	public void singleConfigFile() throws IOException, ParseException
	{
		File configFile = new File(configRoot1, "etlunit.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property: 'value'}"));

		configFile = new File(configRoot1, "override.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property1: 'value2'}"));

		Configuration config = Configuration.loadFromEnvironment(new File [] {configRoot1}, (String []) null, null);

		Assert.assertEquals("value", config.query("property").getValueAsString());
		Assert.assertNull(config.query("property1"));
	}

	@Test
	public void singleConfigFileWithOverride() throws IOException, ParseException
	{
		File configFile = new File(configRoot1, "etlunit.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property1: 'value1', property2: 'value2'}"));

		configFile = new File(configRoot1, "override.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property1: 'value2'}"));

		Configuration config = Configuration.loadFromEnvironment(new File [] {configRoot1}, "override", null);

		Assert.assertEquals("value2", config.query("property1").getValueAsString());
		Assert.assertEquals("value2", config.query("property2").getValueAsString());
	}

	@Test
	public void configFileWithRemoteBase() throws IOException, ParseException
	{
		File configFile = new File(configRoot1, "etlunit.json");

		IOUtils.writeBufferToFile(configFile,
				new StringBuffer("{ property1: 'value1', property2: 'value1', property3: 'value1'}"));

		configFile = new File(configRoot1, "override.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property2: 'value2'}"));

		configFile = new File(remoteConfigRoot, "etlunit.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property4: 'value3', property1: 'value3'}"));

		Configuration config = Configuration.loadFromEnvironment(new File [] {configRoot1}, "override", loader);

		Assert.assertEquals("value1", config.query("property1").getValueAsString());
		Assert.assertEquals("value2", config.query("property2").getValueAsString());
		Assert.assertEquals("value1", config.query("property3").getValueAsString());
		Assert.assertEquals("value3", config.query("property4").getValueAsString());
	}

	@Test
	public void configFileWithRemoteOverride() throws IOException, ParseException
	{
		File configFile = new File(configRoot1, "etlunit.json");

		IOUtils.writeBufferToFile(configFile,
				new StringBuffer("{ property1: 'value1', property2: 'value1', property3: 'value1'}"));

		configFile = new File(configRoot1, "override.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property2: 'value2'}"));

		configFile = new File(remoteConfigRoot, "etlunit.json");

		IOUtils.writeBufferToFile(configFile,
				new StringBuffer("{ property4: 'value3', property1: 'value3', property6: 'value3'}"));

		configFile = new File(remoteConfigRoot, "override.json");

		IOUtils.writeBufferToFile(configFile,
				new StringBuffer("{ property4: 'value4', property1: 'value3', property5: 'value4'}"));

		Configuration config = Configuration.loadFromEnvironment(new File [] {configRoot1}, "override", loader);

		Assert.assertEquals("value1", config.query("property1").getValueAsString());
		Assert.assertEquals("value2", config.query("property2").getValueAsString());
		Assert.assertEquals("value1", config.query("property3").getValueAsString());
		Assert.assertEquals("value4", config.query("property4").getValueAsString());
		Assert.assertEquals("value4", config.query("property5").getValueAsString());
		Assert.assertEquals("value3", config.query("property6").getValueAsString());
	}

	@Test
	public void configFileWithRemoteOverrideAndSuperOverride() throws IOException, ParseException
	{
		File configFile = new File(configRoot1, "etlunit.json");

		IOUtils.writeBufferToFile(configFile,
				new StringBuffer("{ property1: 'value1', property2: 'value1', property3: 'value1'}"));

		configFile = new File(configRoot1, "override.json");

		IOUtils.writeBufferToFile(configFile, new StringBuffer("{ property2: 'value2'}"));

		configFile = new File(remoteConfigRoot, "etlunit.json");

		IOUtils.writeBufferToFile(configFile,
				new StringBuffer("{ property4: 'value3', property1: 'value3', property6: 'value3'}"));

		configFile = new File(remoteConfigRoot, "override.json");

		IOUtils.writeBufferToFile(configFile,
				new StringBuffer("{ property4: 'value4', property1: 'value3', property5: 'value4'}"));

		Configuration
				config =
				Configuration.loadFromEnvironment(new File [] {configRoot1},
						"override",
						loader,
						ETLTestParser.loadObject("{ property2: 'super5'}"));

		Assert.assertEquals("value1", config.query("property1").getValueAsString());
		Assert.assertEquals("super5", config.query("property2").getValueAsString());
		Assert.assertEquals("value1", config.query("property3").getValueAsString());
		Assert.assertEquals("value4", config.query("property4").getValueAsString());
		Assert.assertEquals("value4", config.query("property5").getValueAsString());
		Assert.assertEquals("value3", config.query("property6").getValueAsString());
	}

	@Test
	public void localConfigOverridesHome() throws IOException, ParseException
	{
		// write to project-local config
		IOUtils.writeBufferToFile(new File(configRoot1, "etlunit.json"),
				new StringBuffer("{ property1: 'value1', property2: 'value1'}"));

		// write to home-directory config file
		IOUtils.writeBufferToFile(Configuration.getEtlunitUserConfig(), new StringBuffer("{ property2: 'value2', property3: 'value2'}"));

		Configuration
				config =
				Configuration.loadFromEnvironment(new File [] {configRoot1},
						(String []) null,
						loader,
						null
		);

		Assert.assertEquals("value1", config.query("property1").getValueAsString());
		Assert.assertEquals("value1", config.query("property2").getValueAsString());
		Assert.assertEquals("value2", config.query("property3").getValueAsString());
	}
}
