package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.MapLocal;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

public class BasicRuntimeSupportTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Rule
	public TemporaryFolder temporaryFolder2 = new TemporaryFolder();

	private BasicRuntimeSupport brs = null;
	private MapLocal mapLocal = new MapLocal();
	private ETLTestClass testClass;

	@Before
	public void createRS() throws ParseException {
		brs = new BasicRuntimeSupport(temporaryFolder.getRoot());
		brs.setMapLocal(mapLocal);

		testClass = new ETLTestParser(new StringReader("class clsz {@Test methodTest(){operation();}}")).loadClasses(ETLTestPackageImpl.getDefaultPackage().getSubPackage("a.b")).get(0);
	}

	@Test
	public void testPackagePaths()
	{
		File src = brs.getTestSourceDirectory();

		File pack = brs.getTestSourceDirectory(ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack"));
		Assert.assertEquals("_pack_", pack.getName());
		Assert.assertEquals(src, pack.getParentFile());

		File packpackpack = brs.getTestSourceDirectory(ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack.pick.pep"));
		Assert.assertEquals("_pep_", packpackpack.getName());
		Assert.assertEquals("_pick_", packpackpack.getParentFile().getName());
		Assert.assertEquals("_pack_", packpackpack.getParentFile().getParentFile().getName());

		Assert.assertEquals(src, packpackpack.getParentFile().getParentFile().getParentFile());
	}

	@Test
	public void testResourcePaths()
	{
		File src = brs.getTestSourceDirectory();

		File pack = brs.getTestResourceDirectory("pack");
		Assert.assertEquals("pack", pack.getName());
		Assert.assertEquals(src, pack.getParentFile());

		File packpackpack = brs.getTestResourceDirectory(ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack.pick.pep"), "pack");
		Assert.assertEquals("pack", packpackpack.getName());

		Assert.assertEquals(brs.getTestSourceDirectory(ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack.pick.pep")), packpackpack.getParentFile());
	}

	@Test(expected = IllegalArgumentException.class)
	public void subPathNotASubPath() throws IOException {
		brs.getProjectRelativePath(temporaryFolder2.getRoot());
	}

	@Test
	public void rootAsSubPath() throws IOException {
		File root = temporaryFolder.getRoot();
		BasicRuntimeSupport brs = new BasicRuntimeSupport(new File(root.getAbsolutePath() + File.separator));

		Assert.assertEquals(".", brs.getProjectRelativePath(root));
	}

	@Test
	public void subPathWithSlashes() throws IOException {
		Assert.assertEquals("path", brs.getProjectRelativePath(temporaryFolder.newFolder("path" + File.separator)));
		Assert.assertEquals("path2", brs.getProjectRelativePath(temporaryFolder.newFolder("path2")));
	}

	@Test
	public void tempFile() throws IOException {
		File f = brs.createTempFile("tar.txt");

		FileUtils.touch(f);

		Assert.assertTrue(f.exists());

		// verify file is where we expect
		File file = new File(brs.getTempDirectory(), "tar.txt");
		Assert.assertTrue(file.exists());
		Assert.assertEquals(file.getAbsolutePath(), f.getAbsolutePath());
	}

	@Test
	public void tempFileInSubdir() throws IOException {
		File f = brs.createTempFile("subdir", "tar.txt");

		FileUtils.touch(f);

		Assert.assertTrue(f.exists());

		// verify file is where we expect
		File file = new File(new FileBuilder(brs.getTempDirectory()).subdir("subdir").file(), "tar.txt");
		Assert.assertTrue(file.exists());
		Assert.assertEquals(file.getAbsolutePath(), f.getAbsolutePath());
	}

	@Test
	public void anonymousTemp() throws IOException {
		File f = brs.createAnonymousTempFile();

		FileUtils.touch(f);

		Assert.assertTrue(f.exists());

		// verify file is where we expect
		File file = new File(brs.getTempDirectory(), f.getName());
		Assert.assertTrue(file.exists());
		Assert.assertEquals(file.getAbsolutePath(), f.getAbsolutePath());
	}

	@Test
	public void anonymousTempWithExtension() throws IOException {
		File f = brs.createAnonymousTempFileWithExtension("txt");

		FileUtils.touch(f);

		Assert.assertTrue(f.exists());
		Assert.assertTrue(f.getName().endsWith(".txt"));
	}

	@Test
	public void anonymousTempInSubdir() throws IOException {
		File f = brs.createAnonymousTempFileInSubdir("subdir");

		FileUtils.touch(f);

		File expected = new FileBuilder(brs.getTempDirectory()).subdir("subdir").file();

		Assert.assertEquals(expected, f.getParentFile());
	}

	@Test
	public void anonymousTempWithExtensionInSubdir() throws IOException {
		File f = brs.createAnonymousTempFile("sabdir", "txt");

		FileUtils.touch(f);

		File expected = new FileBuilder(brs.getTempDirectory()).subdir("sabdir").file();

		Assert.assertEquals(expected, f.getParentFile());
		Assert.assertTrue(f.getName().endsWith(".txt"));
	}

	@Test
	public void createTempFolder() throws IOException {
		File f = brs.createTempFolder("dirn");

		Assert.assertTrue(f.exists());
		Assert.assertTrue(f.isDirectory());
	}

	@Test
	public void createTempFolderNullName() throws IOException {
		File f = brs.createTempFolder(null);

		Assert.assertTrue(f.exists());
		Assert.assertTrue(f.isDirectory());
	}

	@Test
	public void createAnonymousTempFolder() throws IOException {
		File f = brs.createAnonymousTempFolder();

		Assert.assertTrue(f.exists());
		Assert.assertTrue(f.isDirectory());
	}

	@Test
	public void resolvePathExists() throws IOException {
		File file = temporaryFolder.newFile();
		Assert.assertSame(file, brs.resolveFile(file));
	}

	@Test
	public void currentNoneNoReference() {
		Assert.assertEquals("none", brs.processReference("none"));
	}

	@Test
	public void currentNoneReference() {
		Assert.assertEquals("none_INVALID_OPERATION_REFERENCE_INVALID_METHOD_REFERENCE_INVALID_CLASS_REFERENCE_[default]", brs.processReference("none_${testOperationName}_${testMethodName}_${testClassName}_${testPackageName}"));
	}

	@Test
	public void currentClassNoReference() {
		mapLocal.setCurrentlyProcessingTestClass(testClass);
		Assert.assertEquals("none", brs.processReference("none"));
	}

	@Test
	public void currentClassReference() {
		mapLocal.setCurrentlyProcessingTestClass(testClass);
		Assert.assertEquals("none_INVALID_OPERATION_REFERENCE_INVALID_METHOD_REFERENCE_clsz_a.b", brs.processReference("none_${testOperationName}_${testMethodName}_${testClassName}_${testPackageName}"));
	}

	@Test
	public void currentMethodNoReference() {
		mapLocal.setCurrentlyProcessingTestClass(testClass);
		mapLocal.setCurrentlyProcessingTestMethod(testClass.getTestMethods().get(0));
		Assert.assertEquals("none", brs.processReference("none"));
	}

	@Test
	public void currentMethodReference() {
		mapLocal.setCurrentlyProcessingTestClass(testClass);
		mapLocal.setCurrentlyProcessingTestMethod(testClass.getTestMethods().get(0));
		Assert.assertEquals("none_INVALID_OPERATION_REFERENCE_methodTest_clsz_a.b", brs.processReference("none_${testOperationName}_${testMethodName}_${testClassName}_${testPackageName}"));
	}

	@Test
	public void currentOperationNoReference() {
		mapLocal.setCurrentlyProcessingTestClass(testClass);
		mapLocal.setCurrentlyProcessingTestMethod(testClass.getTestMethods().get(0));
		mapLocal.setCurrentlyProcessingTestOperation(testClass.getTestMethods().get(0).getOperations().get(0));
		Assert.assertEquals("none", brs.processReference("none"));
	}

	@Test
	public void currentOperationReference() {
		mapLocal.setCurrentlyProcessingTestClass(testClass);
		mapLocal.setCurrentlyProcessingTestMethod(testClass.getTestMethods().get(0));
		mapLocal.setCurrentlyProcessingTestOperation(testClass.getTestMethods().get(0).getOperations().get(0));
		Assert.assertEquals("none_operation_methodTest_clsz_a.b", brs.processReference("none_${testOperationName}_${testMethodName}_${testClassName}_${testPackageName}"));
	}

	@Test
	public void currentOperationReferenceNull() {
		mapLocal.setCurrentlyProcessingTestClass(testClass);
		mapLocal.setCurrentlyProcessingTestMethod(testClass.getTestMethods().get(0));
		mapLocal.setCurrentlyProcessingTestOperation(testClass.getTestMethods().get(0).getOperations().get(0));
		Assert.assertNull(brs.processReference(null));
	}
}
