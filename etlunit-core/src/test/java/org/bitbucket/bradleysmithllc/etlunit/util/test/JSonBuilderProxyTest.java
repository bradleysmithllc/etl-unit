package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class JSonBuilderProxyTest
{
	@Test
	public void buildBareObject()
	{
		JSonBuilderProxy proxy = new JSonBuilderProxy();

		Assert.assertEquals("{\"datasets\":\"data\"}", proxy.object().key("datasets").value("data").endObject().toString());
	}

	@Test
	public void buildConformantObject()
	{
		JSonBuilderProxy proxy = new JSonBuilderProxy();

		Assert.assertEquals("{\"datasets\":\"data\"}", proxy.object().key("datasets").value("data").endObject().toString());
	}

	@Test
	public void buildKeyObject()
	{
		JSonBuilderProxy proxy = new JSonBuilderProxy();

		Assert.assertEquals("{\"datasets\":{\"data\":\"1\"},\"set\":3}",
				proxy.object()
						.key("datasets")
						.object()
						.key("data")
						.value("1")
						.endObject()
						.key("set")
						.value(3)
						.endObject()
						.toString());
	}

	@Test
	public void buildArrayObject()
	{
		JSonBuilderProxy proxy = new JSonBuilderProxy();

		List<String> stlist = new ArrayList<String>();
		stlist.add("1");
		stlist.add("2");
		stlist.add("3");

		Assert.assertEquals("{\"datasets\":{\"data\":\"1\"},\"set\":3,\"list\":[\"1\",\"2\",\"3\"]}",
				proxy.object()
						.key("datasets")
						.object()
						.key("data")
						.value("1")
						.endObject()
						.key("set")
						.value(3)
						.key("list")
						.value(stlist)
						.endObject()
						.toString());
	}
}
