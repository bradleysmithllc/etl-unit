package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.time.EtlTimeUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FlatFileSchemaTimestampTests {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	@Before
	public void start() throws IOException {
		File tmpRoot = temporaryFolder.newFolder();
		dfm = new DataFileManagerImpl(tmpRoot);
		System.out.println("Using db root: " + tmpRoot.getAbsolutePath());
	}


	@Test
	public void files() throws IOException {
		DataFileSchema schema = dfm.loadDataFileSchemaFromResource("FlatFileSchemaTimestampTests/schema.ffml", "blart");

		File blart = temporaryFolder.newFile("blart");
		File blart2 = temporaryFolder.newFile("blart2");

		String input = ResourceUtils.loadResourceAsString(FlatFileSchemaTimestampTests.class, "FlatFileSchemaTimestampTests/filesInput.txt");
		IOUtils.writeStringToFile(blart, input);

		DataFile df1 = dfm.loadDataFile(blart, schema);
		DataFile df2 = dfm.loadDataFile(blart2, schema);

		dfm.copyDataFile(df1, df2);

		String res = IOUtils.readFileToString(blart2);

		Assert.assertEquals(input, res);
	}

	@Test
	public void ms() throws IOException {
		DataFileSchema schema = dfm.loadDataFileSchemaFromResource("FlatFileSchemaTimestampTests/schema.ffml", "blart");

		File blart = temporaryFolder.newFile("blart");

		IOUtils.writeBufferToFile(blart, new StringBuffer("1|2020-01-01 14:56:45.000|2019-01-09"));

		DataFile dataFile = dfm.loadDataFile(blart, schema);

		DataFileReader reader = dataFile.reader();
		Iterator<DataFileReader.FileRow> it = reader.iterator();

		while (it.hasNext()) {
			DataFileReader.FileRow row = it.next();

			Assert.assertEquals(1, row.getData().get("id"));
			Assert.assertEquals(EtlTimeUtils.localDateFromLocalDateTimeString("2020-01-01 14:56:45.000"), row.getData().get("ts"));
			Assert.assertEquals(LocalDate.parse("2019-01-09"), row.getData().get("dt"));
		}

		reader.dispose();

		Map<String, Object> row = new HashMap<>();
		row.put("id", 1);
		row.put("ts", Timestamp.valueOf("2020-01-01 14:56:45.000"));
		row.put("dt", Date.valueOf("2019-01-09"));

		DataFileWriter dataFileWriter = dataFile.writer();
		dataFileWriter.addRow(row);

		row.put("id", 2);
		row.put("ts", EtlTimeUtils.utcFromLocalDateTimeString("2020-01-01 14:56:45.000"));
		row.put("dt", null);

		dataFileWriter.addRow(row);

		dataFileWriter.close();

		String source = IOUtils.readFileToString(blart);

		reader = dataFile.reader();
		it = reader.iterator();

		it.hasNext();
		DataFileReader.FileRow vobj = it.next();

		it.hasNext();
		vobj = it.next();

		System.out.println(vobj);
	}

	@Test
	public void t() {
		Timestamp ts = Timestamp.valueOf("2020-01-01 15:00:00.000");
		LocalDateTime ldt = ts.toLocalDateTime();
		ZonedDateTime zdt = ldt.atZone(EtlTimeUtils.utcZoneId());

		Timestamp ts2 = Timestamp.valueOf(zdt.toLocalDateTime());

		System.out.println();
	}
}
