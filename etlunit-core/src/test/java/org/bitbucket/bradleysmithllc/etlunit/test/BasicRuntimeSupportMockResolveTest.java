package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

/**
 * This special test is for testing case-sensitive filesystem behavior on case-insensitive machines
 */
//@RunWith(MockitoJUnitRunner.class)
public class BasicRuntimeSupportMockResolveTest
{
	@Mock
	private File sourceDirectory;

	private BasicRuntimeSupport brs = null;

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Before
	public void createRS()
	{
		brs = new BasicRuntimeSupport(temporaryFolder.getRoot());
	}

	//@Test
	public void resolvePath() throws IOException {
		MockitoAnnotations.initMocks(this);

		// set up the return value for listFiles
		// return an upper case name
		Mockito.when(sourceDirectory.getName()).thenAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return "AAA";
			}
		});

		// return this object as the parent to avoid creating too many mocks
		Mockito.when(sourceDirectory.getParentFile()).thenAnswer(new Answer<File>() {
			@Override
			public File answer(InvocationOnMock invocation) throws Throwable {
				return sourceDirectory;
			}
		});

		Mockito.when(sourceDirectory.listFiles(Mockito.any(FileFilter.class))).thenAnswer(new Answer<File[]>() {
			@Override
			public File[] answer(InvocationOnMock invocation) throws Throwable {
				// return a file with a different case
				return new File [] {new File("aaa")};
			}
		});

		Assert.assertEquals("aaa", brs.resolveFile(sourceDirectory).getName());
	}

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void processNoPackageReference() {
		Pair<ETLTestPackage, String> result = brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage(), "file");

		Assert.assertEquals(ETLTestPackageImpl.getDefaultPackage(), result.getLeft());
		Assert.assertEquals("file", result.getRight());
	}

	@Test
	public void processDefaultPackageReference() {
		Pair<ETLTestPackage, String> result = brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage(), "~/file");

		Assert.assertEquals(ETLTestPackageImpl.getDefaultPackage(), result.getLeft());
		Assert.assertEquals("file", result.getRight());
	}

	@Test
	public void processDefaultPackageReferenceCannotAppearTwice() {
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Default, top-level package reference '~' may only appear as the very first path element.  Ref = '~/~/file'");
		brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage(), "~/~/file");
	}

	@Test
	public void processParentPackageReference() {
		Pair<ETLTestPackage, String> result = brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage().getSubPackage("sub"), "../file");

		Assert.assertEquals(ETLTestPackageImpl.getDefaultPackage(), result.getLeft());
		Assert.assertEquals("file", result.getRight());
	}

	@Test
	public void processSelfPackageReference() {
		Pair<ETLTestPackage, String> result = brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage(), "././././file");

		Assert.assertEquals(ETLTestPackageImpl.getDefaultPackage(), result.getLeft());
		Assert.assertEquals("file", result.getRight());
	}

	@Test
	public void processParentPackageReferenceAboveDefaultFails() {
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Cannot resolve path to package above the top-level package.  Ref = '~/../file'");
		brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage().getSubPackage("sub"), "~/../file");
	}

	@Test
	public void processParentPackageReferenceAboveDefaultFails2() {
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Cannot resolve path to package above the top-level package.  Ref = '../file'");
		brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage(), "../file");
	}

	@Test
	public void processAbsoluteSillyPath() {
		Pair<ETLTestPackage, String> result = brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage(), "~/./sub1/sub2/sub3/../file");

		Assert.assertEquals(ETLTestPackageImpl.getDefaultPackage().getSubPackage("sub1").getSubPackage("sub2"), result.getLeft());
		Assert.assertEquals("file", result.getRight());
	}

	@Test
	public void processRelativeSillyPath() {
		Pair<ETLTestPackage, String> result = brs.processPackageReference(ETLTestPackageImpl.getDefaultPackage().getSubPackage("sub1"), "./sub2/sub3/sub4/../file");

		Assert.assertEquals(ETLTestPackageImpl.getDefaultPackage().getSubPackage("sub1").getSubPackage("sub2").getSubPackage("sub3"), result.getLeft());
		Assert.assertEquals("file", result.getRight());
	}
}
