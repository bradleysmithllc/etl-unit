package org.bitbucket.bradleysmithllc.etlunit.broadcaster.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.BroadcasterCoordinator;
import org.bitbucket.bradleysmithllc.etlunit.listener.BroadcasterCoordinatorImpl;
import org.bitbucket.bradleysmithllc.etlunit.listener.BroadcasterExecutor;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Test;

/**
 * Tests that the Broadcaster Coordinator properly handles
 * an executor that blows up at every step.
 */
public class BadExecutorTest
{
	@Test
	public void run() throws ParseException {
		ETLTestCases cases = new ETLTestCases();

		cases.addTestClasses(ETLTestParser.load("class a{@Test a(){}} class b{@Test b(){}}"));
		cases.addTestClasses(ETLTestParser.load("class a{@Test a(){}} class b{@Test b(){}}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("subdir")));

		final BroadcasterCoordinator bc = new BroadcasterCoordinatorImpl(
			cases, new MapLocal(), null, new PrintWriterLog()
		);

		final BroadcasterExecutor be = new BroadcasterExecutor(){
			@Override
			public int getId() {
				return 0;
			}

			@Override
			public void beginExecution() {
				throw new RuntimeException("beginExecution");
			}

			@Override
			public void enterPackage(ETLTestPackage name, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {
				throw new RuntimeException("enterPackage");
			}

			@Override
			public void enterClass(ETLTestClass cl, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {
				throw new RuntimeException("enterClass");
			}

			@Override
			public void runTest(ETLTestMethod mt, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {
				throw new RuntimeException("runTest");
			}

			@Override
			public void leaveClass(ETLTestClass cl, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {
				throw new RuntimeException("leaveClass");
			}

			@Override
			public void leavePackage(ETLTestPackage name, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {
				throw new RuntimeException("leavePackage");
			}

			@Override
			public void endExecution() {
				throw new RuntimeException("endExecution");
			}
		};

		new Thread(new Runnable(){
			@Override
			public void run() {
				while (bc.ready(be));
			}
		}).start();

		bc.start();
		bc.waitForCompletion();
	}
}
