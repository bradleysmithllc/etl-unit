package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassBroadcasterImpl;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SuiteAggregatorTest
{
	@Test
	public void suiteAggregatorListAllSuites() throws ParseException
	{
		List<ETLTestClass>
				list =
				ETLTestParser.load(
						"@JoinSuite(name: 'agg') @JoinSuite(name: 'stg') class class_a {@Test b(){}} @JoinSuite(name: 'stg') class class_b {} @JoinSuite(name: 'dbo') class class_c {@Test c(){}}");

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		ClassBroadcasterImpl climpl = new ClassBroadcasterImpl(impl, new NullClassDirector()
		{
			@Override
			public response_code accept(ETLTestMethod mt) {
				return response_code.accept;
			}

			@Override
			public response_code accept(ETLTestClass cl)
			{
				return response_code.accept;
			}
		}, new NullClassListener(), new MapLocal(), new PrintWriterLog());

		SuiteAggregator statusReporter = new SuiteAggregator();
		ETLTestCases cases = climpl.preScan(statusReporter);

		List<String> slist = statusReporter.getSuiteList();

		Assert.assertEquals(3, slist.size());
		Assert.assertEquals("agg", slist.get(0));
		Assert.assertEquals("dbo", slist.get(1));
		Assert.assertEquals("stg", slist.get(2));
	}
}
