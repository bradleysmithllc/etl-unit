package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class IllbehavedTestsTest extends BaseFeatureModuleTest
{
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList(
				(Feature) new BadFeatureModule()
												);
	}

	protected void setUpSourceFiles() throws IOException
	{
		createSource("test.etlunit", "class test_me {" +
				"@Test execute1() {extend();}" +
				"@Test execute2() {extend();}" +
				"@Test execute3() {extend();}" +
				"}");
	}

	@Test
	public void theExecutorWhichAcceptsTheTestShouldRunIt() throws IOException
	{
		TestResults results = startTest();

		Assert.assertEquals(3, results.getNumTestsSelected());
		Assert.assertEquals(0, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(3, results.getMetrics().getNumberOfErrors());
	}

	private final class BadFeatureModule extends AbstractFeature
	{
		@Override
		public String getFeatureName()
		{
			return "bad-to-the-bone";
		}

		@Override
		public ClassListener getListener()
		{
			return new NullClassListener()
			{
				@Override
				public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, final int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					throw new Error();
				}
			};
		}
	}
}
