package org.bitbucket.bradleysmithllc.etlunit.parser.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class AnnotationsTest
{
	@Test
	public void annotatedsTrackCorrectly() throws ParseException
	{
		ETLTestAnnotatedImpl eto = new ETLTestAnnotatedImpl();

		Assert.assertEquals(0, eto.getAnnotations().size());

		ETLTestAnnotation eta = new ETLTestAnnotationImpl("Test");

		eto.addAnnotation(eta);

		Assert.assertEquals(1, eto.getAnnotations().size());

		List<ETLTestAnnotation> allList = eto.getAnnotations();
		List<ETLTestAnnotation> annList = eto.getAnnotations("Test");

		Assert.assertEquals(allList, annList);

		eta = new ETLTestAnnotationImpl("Test2", ETLTestParser.loadBareObject("dark_horse: 'me'"));
		eto.addAnnotation(eta);

		allList = eto.getAnnotations();
		annList = eto.getAnnotations("Test");
		List<ETLTestAnnotation> ann2List = eto.getAnnotations("Test2");

		Assert.assertEquals(2, allList.size());
		Assert.assertEquals(1, annList.size());
		Assert.assertEquals(1, ann2List.size());
	}
}
