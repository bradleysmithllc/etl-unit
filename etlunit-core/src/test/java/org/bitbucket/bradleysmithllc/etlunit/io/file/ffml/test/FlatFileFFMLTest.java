package org.bitbucket.bradleysmithllc.etlunit.io.file.ffml.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FlatFileFFMLTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	@Before
	public void start() throws IOException {
		dfm = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	@Test
	public void flatFileDelimited() throws IOException
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr-delimited.ffml", "dsr-delimited.ffml");

		File targ = new File("target-delimited.txt");

		DataFile ff = dfm.loadDataFile(targ, ffs);

		DataFileWriter writer = ff.writer();

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("store_number", "STORE");
		map.put("sales_date", "SALES");
		map.put("dsr_category", "DSR");
		map.put("account_number", "ACCO");
		map.put("sub_account_number", "SACCNT");
		map.put("amount", "AMOUNT");
		map.put("dsr_line_description", "DSR_LINE");
		map.put("original_pos_amount", "ORIGINAL_AMT");
		map.put("taxpayer_id", "TAX_ID");
		map.put("file_name_date", "FNAMEDAT");
		map.put("pos_type", "POS_TYPE");

		writer.addRow(map);

		writer.close();

		Iterator<DataFileReader.FileRow> it = ff.reader().iterator();

		boolean rows = false;

		while (it.hasNext())
		{
			Assert.assertFalse(rows);

			rows = true;
			Map<String, Object> data = it.next().getData();

			Assert.assertEquals("STORE", data.get("store_number"));
			Assert.assertEquals("SALES", data.get("sales_date"));
			Assert.assertEquals("DSR", data.get("dsr_category"));
			Assert.assertEquals("ACCO", data.get("account_number"));
			Assert.assertEquals("SACCNT", data.get("sub_account_number"));
			Assert.assertEquals("AMOUNT", data.get("amount"));
			Assert.assertEquals("DSR_LINE", data.get("dsr_line_description"));
			Assert.assertEquals("ORIGINAL_AMT", data.get("original_pos_amount"));
			Assert.assertEquals("TAX_ID", data.get("taxpayer_id"));
			Assert.assertEquals("FNAMEDAT", data.get("file_name_date"));
			Assert.assertEquals("POS_TYPE", data.get("pos_type"));
		}

		Assert.assertTrue(rows);

		Assert.assertTrue(targ.delete());
	}

	@Test
	public void flatFileFixed() throws IOException
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr.ffml", "dsr.ffml");

		File targ = new File("target-fixed.txt");

		DataFile ff = dfm.loadDataFile(targ, ffs);

		DataFileWriter writer = ff.writer();

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("store_number", "STOR|");
		map.put("sales_date", "..SALES|");
		map.put("dsr_category", "DSR......|");
		map.put("account_number", "ACCN|");
		map.put("sub_account_number", "SUB_A|");
		map.put("amount", "AMOUN|");
		map.put("dsr_line_description", "DSR_LINE.....................|");
		map.put("original_pos_amount", "ORIGINAL_AMT|");
		map.put("taxpayer_id", "TAX_ID.....|");
		map.put("file_name_date", "FILDATE|");
		map.put("pos_type", "POS_TYPE...........<");

		writer.addRow(map);
		writer.addRow(map);
		writer.addRow(map);

		writer.close();

		Iterator<DataFileReader.FileRow> it = ff.reader().iterator();

		int row = 0;

		while (it.hasNext())
		{
			row++;
			Map<String, Object> data = it.next().getData();

			Assert.assertEquals("STOR|", data.get("store_number"));
			Assert.assertEquals("..SALES|", data.get("sales_date"));
			Assert.assertEquals("DSR......|", data.get("dsr_category"));
			Assert.assertEquals("ACCN|", data.get("account_number"));
			Assert.assertEquals("SUB_A|", data.get("sub_account_number"));
			Assert.assertEquals("AMOUN|", data.get("amount"));
			Assert.assertEquals("DSR_LINE.....................|", data.get("dsr_line_description"));
			Assert.assertEquals("ORIGINAL_AMT|", data.get("original_pos_amount"));
			Assert.assertEquals("TAX_ID.....|", data.get("taxpayer_id"));
			Assert.assertEquals("FILDATE|", data.get("file_name_date"));
			Assert.assertEquals("POS_TYPE...........<", data.get("pos_type"));
		}

		Assert.assertEquals(3, row);

		Assert.assertTrue(targ.delete());
	}

	@Test
	public void copyDelimitedToFixed() throws IOException
	{
		DataFileSchema schema = dfm.createDataFileSchema("id");
		schema.setFormatType(DataFileSchema.format_type.delimited);
		schema.setColumnDelimiter("|");
		schema.setRowDelimiter("\n");

		schema.addColumn(schema.createColumn("id"));
		schema.addColumn(schema.createColumn("date"));
		schema.addColumn(schema.createColumn("num"));
		schema.addColumn(schema.createColumn("type"));

		DataFileSchema fixedSchema = dfm.createDataFileSchema("fixed.id");
		fixedSchema.setFormatType(DataFileSchema.format_type.delimited);
		fixedSchema.setRowDelimiter("\n");

		DataFileSchema.Column column = schema.createColumn("date");
		column.setLength(8);

		fixedSchema.addColumn(column);

		column = schema.createColumn("num");
		column.setLength(4);
		fixedSchema.addColumn(column);

		File source = temporaryFolder.newFile("source.delimited");
		File target = temporaryFolder.newFile("target.flat");

		IOUtils.writeBufferToFile(source,
				new StringBuffer("ID1|DATE1|NUM1|TYPE1\nID2|DATE2|NUM2|TYPE2\nID3|DATE3|NUM3|TYPE3"));

		DataFile dataFile = dfm.loadDataFile(source, schema);

		DataFileReader fd = dataFile.reader();

		try
		{
			DataFile dfTarget = dfm.loadDataFile(target, fixedSchema);
			DataFileWriter writer = dfTarget.writer();

			try
			{
				Iterator<DataFileReader.FileRow> it = fd.iterator();

				while (it.hasNext())
				{
					writer.addRow((Map) it.next().getData());
				}
			}
			finally
			{
				writer.close();
			}
		}
		finally
		{
			fd.dispose();
		}

		// open it up and see what we've got
		String targ = IOUtils.readFileToString(target);

		Assert.assertEquals("/*-- date\tnum  --*/\n" +
				"/*-- VARCHAR\tVARCHAR  --*/\n" +
				"DATE1\tNUM1\n" +
				"DATE2\tNUM2\n" +
				"DATE3\tNUM3", targ);

		// copy to a new file with the same schema
		DataFile df = dfm.loadDataFile(temporaryFolder.newFile("source.copy.delimited"), schema);

		fd = dataFile.reader();

		try
		{
			DataFileWriter writer = df.writer();

			try
			{
				Iterator<DataFileReader.FileRow> it = fd.iterator();

				while (it.hasNext())
				{
					writer.addRow((Map) it.next().getData());
				}
			}
			finally
			{
				writer.close();
			}
		}
		finally
		{
			fd.dispose();
		}

		String sour = IOUtils.readFileToString(df.getSource());

		Assert.assertEquals("/*-- id|date|num|type  --*/\n" +
				"/*-- VARCHAR|VARCHAR|VARCHAR|VARCHAR  --*/\n" +
				"ID1|DATE1|NUM1|TYPE1\n" +
				"ID2|DATE2|NUM2|TYPE2\n" +
				"ID3|DATE3|NUM3|TYPE3", sour);
	}

	@Test
	public void escapeColumnDelimiters() throws IOException
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr-delimited.ffml", "dsr-delimited.ffml");

		File target = temporaryFolder.newFile();

		DataFile ff = dfm.loadDataFile(target, ffs);

		DataFileWriter writer = ff.writer();

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("store_number", "\\\tt\ne");
		map.put("sales_date", "\tte\nt\t");

		writer.addRow(map);

		writer.close();

		System.out.println(FileUtils.readFileToString(target));

		DataFileReader fd = ff.reader();

		Iterator<DataFileReader.FileRow> it = fd.iterator();

		Assert.assertTrue(it.hasNext());

		DataFileReader.FileRow row = it.next();

		Map<String, Object> rd = row.getData();

		Assert.assertEquals("\\\tt\ne", rd.get("store_number"));
		Assert.assertEquals("\tte\nt\t", rd.get("sales_date"));
	}
}
