package org.bitbucket.bradleysmithllc.etlunit.parser.datadiff;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.time.EtlTimeUtils;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimestampNearSpecificationTests {
	@Test
	public void currentDate() {
		TimestampNearSpecification spec = new TimestampNearSpecification(null, TimeUnit.NANOSECONDS, 0);

		Assert.assertTrue(spec.isReferenceDateCurrentDate());
	}

	@Test
	public void exact() {
		ZonedDateTime referenceDate = ZonedDateTime.now();
		TimestampNearSpecification spec = new TimestampNearSpecification(referenceDate, TimeUnit.NANOSECONDS, 0);

		Assert.assertEquals(referenceDate, spec.latestDate());
		Assert.assertEquals(referenceDate, spec.earliestDate());
	}

	@Test
	public void belowOldest() {
		ZonedDateTime referenceDate = EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000");
		TimestampNearSpecification spec = new TimestampNearSpecification(referenceDate, TimeUnit.MILLISECONDS, 100);

		Assert.assertEquals(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-01 23:59:59.900"), spec.earliestDate());
		Assert.assertEquals(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.100"), spec.latestDate());
	}

	@Test
	public void rangeMatchesLower() {
		TimestampNearSpecification spec = new TimestampNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000"), TimeUnit.MILLISECONDS, 100);
		NearDiffResult test = spec.test("2018-01-01 23:59:59.900");

		Assert.assertTrue(test.comparisonSucceeded());
		Assert.assertEquals("==", test.getExplanation());
	}

	@Test
	public void rangeMatchesUpper() {
		TimestampNearSpecification spec = new TimestampNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000"), TimeUnit.MILLISECONDS, 100);
		NearDiffResult test = spec.test("2018-01-02 00:00:00.100");

		Assert.assertTrue(test.comparisonSucceeded());
		Assert.assertEquals("==", test.getExplanation());
	}

	@Test
	public void rangeExceedsLower() {
		TimestampNearSpecification spec = new TimestampNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000"), TimeUnit.MILLISECONDS, 100);
		NearDiffResult test = spec.test("2018-01-01 23:59:59.899");

		Assert.assertFalse(test.comparisonSucceeded());
		Assert.assertEquals("Comparison date 2018-01-01 23:59:59.899 is before earliest allowable date - 2018-01-01 23:59:59.900 <= ref <= 2018-01-02 00:00:00.100", test.getExplanation());
	}

	@Test
	public void rangeExceedsUpper() {
		TimestampNearSpecification spec = new TimestampNearSpecification(EtlTimeUtils.utcFromLocalDateTimeString("2018-01-02 00:00:00.000"), TimeUnit.MILLISECONDS, 100);
		NearDiffResult test = spec.test("2018-01-02 00:00:00.101");

		Assert.assertFalse(test.comparisonSucceeded());
		Assert.assertEquals("Comparison date 2018-01-02 00:00:00.101 is after latest allowable date - 2018-01-01 23:59:59.900 <= ref <= 2018-01-02 00:00:00.100", test.getExplanation());
	}

	public static Date date(String s) {
		return Timestamp.valueOf(s);
	}
}
