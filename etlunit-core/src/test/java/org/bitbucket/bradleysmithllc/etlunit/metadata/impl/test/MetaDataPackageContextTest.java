package org.bitbucket.bradleysmithllc.etlunit.metadata.impl.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.impl.MetaDataPackageContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;

public class MetaDataPackageContextTest extends JsonSerializableBase
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void test() throws IOException {
		MetaDataPackageContext con = new MetaDataPackageContextImpl(ETLTestPackageImpl.getDefaultPackage(), MetaDataPackageContext.path_type.test_source);

		con.createArtifact("rel1", "projRel", temporaryFolder.getRoot());
		con.createArtifact("rel2", "projRel", temporaryFolder.getRoot());

		testJson(con);
	}

	@Test
	public void createTwiceSameObject() throws IOException {
		MetaDataPackageContext con = new MetaDataPackageContextImpl(ETLTestPackageImpl.getDefaultPackage(), MetaDataPackageContext.path_type.test_source);

		MetaDataArtifact art = con.createArtifact("rel", "projRel", temporaryFolder.getRoot());
		Assert.assertSame(art, con.createArtifact("rel", "projRel", temporaryFolder.getRoot()));
	}

	@Override
	protected JsonSerializable newObject() {
		return new MetaDataPackageContextImpl();
	}
}
