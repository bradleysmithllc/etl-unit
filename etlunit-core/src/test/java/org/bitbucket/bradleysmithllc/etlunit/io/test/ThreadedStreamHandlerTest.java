package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.output.NullOutputStream;
import org.bitbucket.bradleysmithllc.etlunit.io.ThreadedStreamHandler;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.StringBufferInputStream;

public class ThreadedStreamHandlerTest
{
	@Test(expected = IllegalArgumentException.class)
	public void invalidIdentifier()
	{
		new ThreadedStreamHandler().addOutputStreamToHandle("invalid", new NullOutputStream());
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidIdentifierEnable()
	{
		new ThreadedStreamHandler().enableStreamHandle("");
	}

	//@Test
	public void stringCopy() throws InterruptedException {
		ThreadedStreamHandler trh = new ThreadedStreamHandler();

		StringBufferInputStream sis = new StringBufferInputStream("Blahblahblahbl;ah");

		trh.createStreamHandle("stream", sis);

		ByteArrayOutputStream stringOutputStream = new ByteArrayOutputStream();
		trh.addOutputStreamToHandle("stream", stringOutputStream);
		ByteArrayOutputStream stringOutputStream1 = new ByteArrayOutputStream();
		trh.addOutputStreamToHandle("stream", stringOutputStream1);

		trh.enableStreamHandle("stream");

		Thread.sleep(5000L);
		trh.disable();

		Thread.sleep(5000L);

		Assert.assertEquals(new String(stringOutputStream.toByteArray()), new String(stringOutputStream1.toByteArray()));
		Assert.assertEquals("Blahblahblahbl;ah", new String(stringOutputStream1.toByteArray()));
	}
}
