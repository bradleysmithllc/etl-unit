package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

@RunWith(Parameterized.class)
public class DiffHarness
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	@Parameterized.Parameters
	public static Collection<Object[]> data() throws Exception
	{
		Map<String, TestSpec> testSpecMap = new HashMap<String, TestSpec>();
		List<Object[]> testSpecArray = new ArrayList<Object[]>();

		URL url = DiffHarness.class.getResource("/fileDiff/DiffTestSpecs.json");

		Assert.assertNotNull(url);

		JsonNode jsonTests = JsonUtils.loadJson(IOUtils.readURLToString(url));

		JsonNode node = jsonTests.get("tests");

		Assert.assertNotNull(node);
		Assert.assertTrue(node.isArray());

		ArrayNode anode = (ArrayNode) jsonTests.get("tests");

		for (int i = 0; i < anode.size(); i++)
		{
			TestSpec tSpec = new TestSpec();

			final JsonNode testNode = anode.get(i);

			Assert.assertNotNull(testNode);
			Assert.assertTrue(testNode.isObject());

			final JsonNode idNode = testNode.get("id");
			Assert.assertNotNull(idNode);
			Assert.assertTrue(idNode.isTextual());

			tSpec.id = idNode.asText();

			final JsonNode executeNode = testNode.get("execute");

			if (executeNode != null)
			{
				Assert.assertTrue(executeNode.isBoolean());
				tSpec.execute = executeNode.asBoolean();
			}

			tSpec.id = idNode.asText();

			final JsonNode sourceNode = testNode.get("source");
			Assert.assertNotNull(sourceNode);
			Assert.assertTrue(sourceNode.isObject());

			tSpec.source = readSpec(sourceNode);

			final JsonNode targetNode = testNode.get("target");
			Assert.assertNotNull(targetNode);
			Assert.assertTrue(targetNode.isObject());

			tSpec.target = readSpec(targetNode);

			// gather the results
			final JsonNode resultsNode = testNode.get("results-file");
			Assert.assertNotNull(resultsNode);
			Assert.assertTrue(resultsNode.isTextual());

			tSpec.resultsFile = resultsNode.asText();

			// gather the order by columns
			final JsonNode orderByNode = testNode.get("orderBy");

			if (orderByNode != null)
			{
				Assert.assertTrue(orderByNode.isArray());

				tSpec.orderBy = new ArrayList<String>();

				Iterator<JsonNode> it = orderByNode.iterator();

				while (it.hasNext())
				{
					JsonNode arrn = it.next();

					Assert.assertTrue(arrn.isTextual());

					tSpec.orderBy.add(arrn.asText());
				}

				Assert.assertTrue(tSpec.orderBy.size() != 0);
			}

			// gather the optional columns
			final JsonNode columnsNode = testNode.get("columns");

			if (columnsNode != null)
			{
				Assert.assertTrue(columnsNode.isArray());

				ArrayNode acnode = (ArrayNode) columnsNode;

				tSpec.columns = new ArrayList<String>();

				Iterator<JsonNode> it = acnode.iterator();

				while (it.hasNext())
				{
					JsonNode arrn = it.next();

					Assert.assertTrue(arrn.isTextual());

					tSpec.columns.add(arrn.asText());
				}
			}

			Assert.assertFalse("Test Spec not unique: " + tSpec.id, testSpecMap.containsKey(tSpec.id));

			testSpecMap.put(tSpec.id, tSpec);
			testSpecArray.add(new TestSpec[]{tSpec});
		}

		return testSpecArray;
	}

	private final TestSpec testSpec;

	@Before
	public void start() throws IOException {
		dfm = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	public DiffHarness(TestSpec spec) {
		testSpec = spec;
	}

	@Test
	public void runDiffTests() throws Exception
	{
		if (testSpec.execute)
		{
			runTest(testSpec);
		}
	}

	private void runTest(TestSpec tSpec) throws IOException
	{
		System.out.println(tSpec.id);
		// locate the source and target files
		URL source = getClass().getResource("/fileDiff/" + tSpec.source.name);

		Assert.assertNotNull("Source data set not found: " + tSpec.id, source);

		URL target = getClass().getResource("/fileDiff/" + tSpec.target.name);

		Assert.assertNotNull("Target data set not found: " + tSpec.id, target);

		// copy the two into temporary files for the sake of the flatfile API
		File source1 = new File(tSpec.source.name);
		IOUtils.writeBufferToFile(source1, new StringBuffer(IOUtils.readURLToString(source)));

		try
		{
			File target1 = new File(tSpec.target.name);
			IOUtils.writeBufferToFile(target1, new StringBuffer(IOUtils.readURLToString(target)));

			try
			{
				if (tSpec.source.schema == null || tSpec.target.schema == null)
				{
					return;
				}

				Assert.assertNotNull("Must provide a source schema", tSpec.source.schema);
				Assert.assertNotNull("Must provide a target schema", tSpec.target.schema);

				// now read both in to flat files.  We'll hardcode options since this is a test
				DataFileSchema schemaSource = dfm.loadDataFileSchemaFromResource("fileDiff/" + tSpec.source.schema, tSpec.source.schema);
				DataFileSchema schemaTarget = dfm.loadDataFileSchemaFromResource("fileDiff/" + tSpec.target.schema, tSpec.source.schema);

				DataFile flatFileSource = dfm.loadDataFile(source1, schemaSource);

				DataFile flatFileTarget = dfm.loadDataFile(target1, schemaTarget);

				// set the order by if provided
				if (tSpec.orderBy != null)
				{
					flatFileSource.getDataFileSchema().setOrderColumns(tSpec.orderBy);
					flatFileTarget.getDataFileSchema().setOrderColumns(tSpec.orderBy);
				}

				// do the diff with optional columns
				List<FileDiff> diffList = dfm.diff(flatFileSource, flatFileTarget, tSpec.columns);

				// write the results into a delimited file
				File results1 = new File(tSpec.resultsFile);

				try
				{
					DataFileSchema schema = dfm.loadDataFileSchemaFromResource("fileDiff/diff.ffml", "");

					DataFile flatFileResults = dfm.loadDataFile(results1, schema);

					DataFileWriter writer = flatFileResults.writer();

					try
					{
						for (FileDiff diff : diffList)
						{
							Map<String, Object> dataMap = new HashMap<String, Object>();
							dataMap.put("source_record_num", new Integer(diff.getSourceRowNumber()));
							dataMap.put("target_record_num", new Integer(diff.getTargetRowNumber()));
							dataMap.put("diff_row_type",
									diff.getSourceRowNumber() == -1
											? "inserted"
											: (diff.getTargetRowNumber() == -1 ? "removed" : "changed"));
							dataMap.put("column_name",
									(diff.getSourceRowNumber() != -1 && diff.getTargetRowNumber() != -1) ? diff.getColumnName() : null);
							dataMap.put("order_key", diff.getOrderKey().getPrettyString());
							dataMap.put("source_value", diff.getSourceValue());
							dataMap.put("target_value", diff.getOtherValue());

							writer.addRow(dataMap);
						}
					}
					finally
					{
						writer.close();
					}

					// just do a brute-force comparison for exactness to the results file
					URL results = getClass().getResource("/fileDiff/" + tSpec.resultsFile);

					Assert.assertNotNull("Results data set not found: " + tSpec.id, results);

					String expectedResults = IOUtils.readURLToString(results);
					String actualResults = IOUtils.readFileToString(results1);

					Assert.assertEquals("Results do not match expectation: " + tSpec.id, expectedResults, actualResults);
				}
				finally
				{
					Assert.assertTrue("Could not delete results file", results1.delete());
				}
			}
			finally
			{
				Assert.assertTrue(target1.delete());
			}
		}
		finally
		{
			Assert.assertTrue("Could not delete source temporary file", source1.delete());
		}
	}

	private static final class TestSpec
	{
		String id;
		DataSetSpec source;
		DataSetSpec target;

		List<String> columns;

		List<String> orderBy;

		String resultsFile;

		boolean execute = false;
	}

	private static final class DataSetSpec
	{
		enum source_type
		{
			delimited, flat
		}

		String name;
		source_type type;
		String schema;
	}

	private static DataSetSpec readSpec(JsonNode sourceNode)
	{
		final JsonNode name = sourceNode.get("name");
		Assert.assertNotNull(name);
		Assert.assertTrue(name.isTextual());

		final JsonNode type = sourceNode.get("type");
		Assert.assertNotNull(type);
		Assert.assertTrue(type.isTextual());

		final DataSetSpec dataSetSpec = new DataSetSpec();

		dataSetSpec.name = name.asText();

		final String sType = type.asText();
		if (sType.equals("delimited"))
		{
			dataSetSpec.type = DataSetSpec.source_type.delimited;
		}
		else if (sType.equals("flat"))
		{
			dataSetSpec.type = DataSetSpec.source_type.flat;
		}
		else
		{
			Assert.fail("Bad data set type: " + sType);
		}

		final JsonNode schema = sourceNode.get("schema");

		if (schema != null)
		{
			Assert.assertTrue(schema.isTextual());
			dataSetSpec.schema = schema.asText();
		}

		// validate that any flat set also has a schema
		//Assert.assertTrue("Set type and schema mismatch: " + dataSetSpec.name,
		//		(dataSetSpec.type == DataSetSpec.source_type.delimited && dataSetSpec.schema == null)
		//				||
		//				(dataSetSpec.type == DataSetSpec.source_type.flat && dataSetSpec.schema != null)
		//								 );

		return dataSetSpec;
	}
}
