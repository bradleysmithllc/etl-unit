package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.PatternedClassDirectorFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.UserDirectedClassDirectorFeature;
import org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.regexp.TestExpression;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FeatureTestSelectionTest extends BaseFeatureModuleTest
{
	@Test
	public void unfiltered() throws IOException
	{
		// test unfiltered
		TestResults results = startTest("unfiltered");

		Assert.assertEquals(3, results.getNumTestsSelected());
		Assert.assertEquals(3, results.getMetrics().getNumberOfTestsPassed());
	}

	@Test
	public void filteredClass() throws IOException
	{
		TestResults results = startTest("filtered-class");

		Assert.assertEquals(1, results.getNumTestsSelected());
		Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());
	}

	@Test
	public void filteredMethod() throws IOException
	{
		TestResults results = startTest("filtered-method");

		Assert.assertEquals(2, results.getNumTestsSelected());
		Assert.assertEquals(2, results.getMetrics().getNumberOfTestsPassed());
	}

	@Override
	protected List<Feature> getTestFeatures()
	{
		List<Feature> slist = new ArrayList<Feature>();

		String clsName = ".*";
		String mthName = ".*";

		if (identifier != null)
		{
			if (identifier.equals("filtered-class"))
			{
				clsName = "@_accept";
			}
			if (identifier.equals("filtered-method"))
			{
				mthName = "_accept";
			}
		}

		// add a feature which will reject some tests / classes / operations
		try {
			slist.add(new UserDirectedClassDirectorFeature("filterer", clsName + "#" + mthName));
		} catch (ParseException e) {
			throw new IllegalStateException(e);
		}

		return slist;
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSource("test1.etlunit",
				"class test { @Test test() {log(message: '');} @Test test2_accept() {log(message: '');}}");
		createSource("test2.etlunit", "class test_accept { @Test test_accept() {log(message: '');}}");
	}
}
