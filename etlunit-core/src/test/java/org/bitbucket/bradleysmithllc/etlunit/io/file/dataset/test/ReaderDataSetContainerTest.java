package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.DataSet;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSet;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetContainer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.io.StringReader;

public class ReaderDataSetContainerTest
{
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dataFileManager;

	@Before
	public void setup() throws IOException {
		dataFileManager = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	@Test
	public void emptyHasNext() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager, new StringReader(""));
		Assert.assertFalse(rdsc.hasNext());
	}

	@Test
	public void notHasNext() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager, new StringReader("     \r\r\r"));
		Assert.assertFalse(rdsc.hasNext());
	}

	@Test
	public void tabHasNext() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager, new StringReader("\t{}" + ReaderDataSet.token));
		Assert.assertTrue(rdsc.hasNext());
	}

	@Test
	public void skipData() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/skipData.dataset"
		);

		//ReaderDataSetContainer.debug = true;

		Assert.assertTrue(rdsc.hasNext());
		rdsc.next();
		Assert.assertTrue(rdsc.hasNext());
		rdsc.next();
		Assert.assertTrue(rdsc.hasNext());
		rdsc.next();
		Assert.assertFalse(rdsc.hasNext());
	}

	@Test
	public void hasNextAllYouWant() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/skipData.dataset"
		);

		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		rdsc.next();
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		rdsc.next();
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		Assert.assertTrue(rdsc.hasNext());
		rdsc.next();
		Assert.assertFalse(rdsc.hasNext());
		Assert.assertFalse(rdsc.hasNext());
		Assert.assertFalse(rdsc.hasNext());
		Assert.assertFalse(rdsc.hasNext());
		Assert.assertFalse(rdsc.hasNext());
		Assert.assertFalse(rdsc.hasNext());
		Assert.assertFalse(rdsc.hasNext());
	}

	@Test
	public void cantCallHasNextIfReaderOpen() throws IOException {
		expectedEx.expect(IllegalStateException.class);
		expectedEx.expectMessage("There is an open data file reader.");

		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/skipData.dataset"
		);

		Assert.assertTrue(rdsc.hasNext());
		DataSet ne = rdsc.next();
		ne.open();
		rdsc.hasNext();
	}
}
