package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ClassLocator;
import org.bitbucket.bradleysmithllc.etlunit.PassThroughClassLocatorImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class PassThroughClassLocatorTest
{
	@Test
	public void passThroughWorksEmpty()
	{
		List<ETLTestClass> list = new ArrayList<ETLTestClass>();

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		Assert.assertFalse(impl.hasNext());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void passThroughBreaksWithRemove()
	{
		List<ETLTestClass> list = new ArrayList<ETLTestClass>();

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		impl.remove();
	}

	@Test(expected = NoSuchElementException.class)
	public void passThroughBreaksWithEmptyListNext()
	{
		List<ETLTestClass> list = new ArrayList<ETLTestClass>();

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		impl.next();
	}

	@Test
	public void passThroughWorks() throws ParseException
	{
		List<ETLTestClass> list = ETLTestParser.load("class class_a {} class class_b {} class class_c {}");

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_a", impl.next().getName());

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_b", impl.next().getName());

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_c", impl.next().getName());

		Assert.assertFalse(impl.hasNext());
	}

	@Test
	public void resetThroughWorks() throws ParseException
	{
		List<ETLTestClass> list = ETLTestParser.load("class class_a {} class class_b {} class class_c {}");

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_a", impl.next().getName());

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_b", impl.next().getName());

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_c", impl.next().getName());

		Assert.assertFalse(impl.hasNext());

		impl.reset();

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_a", impl.next().getName());

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_b", impl.next().getName());

		Assert.assertTrue(impl.hasNext());
		Assert.assertEquals("class_c", impl.next().getName());

		Assert.assertFalse(impl.hasNext());
	}
}
