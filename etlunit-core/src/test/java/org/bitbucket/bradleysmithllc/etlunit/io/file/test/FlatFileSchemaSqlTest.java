package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

@RunWith(Parameterized.class)
public class FlatFileSchemaSqlTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	@Parameterized.Parameters
	public static Collection<Object[]> data() throws Exception
	{
		Map<String, TestSpec> testSpecMap = new HashMap<String, TestSpec>();
		List<Object[]> testSpecArray = new ArrayList<Object[]>();

		URL url = FlatFileSchemaSqlTest.class.getResource("/fileSql/TestSpecs.json");

		Assert.assertNotNull(url);

		JsonNode jsonTests = JsonUtils.loadJson(IOUtils.readURLToString(url));

		JsonNode node = jsonTests.get("tests");

		Assert.assertNotNull(node);
		Assert.assertTrue(node.isObject());

		ObjectNode anode = (ObjectNode) jsonTests.get("tests");

		Iterator<String> it = anode.fieldNames();

		while (it.hasNext())
		{
			TestSpec tSpec = new TestSpec();
			tSpec.id = it.next();

			final JsonNode testNode = anode.get(tSpec.id);

			Assert.assertNotNull(testNode);
			Assert.assertTrue(testNode.isObject());

			final JsonNode executeNode = testNode.get("execute");

			if (executeNode != null)
			{
				Assert.assertTrue(executeNode.isBoolean());
				tSpec.execute = executeNode.asBoolean();
			}

			Assert.assertFalse("Test Spec not unique: " + tSpec.id, testSpecMap.containsKey(tSpec.id));

			testSpecMap.put(tSpec.id, tSpec);
			testSpecArray.add(new TestSpec[]{tSpec});
		}

		return testSpecArray;
	}

	private final TestSpec testSpec;

	@Before
	public void start() throws IOException {
		File tmpRoot = temporaryFolder.newFolder();
		dfm = new DataFileManagerImpl(tmpRoot);
		System.out.println("Using db root: " + tmpRoot.getAbsolutePath());
	}

	public FlatFileSchemaSqlTest(TestSpec spec) {
		testSpec = spec;
	}

	@Test
	public void runDiffTests() throws Exception
	{
		if (testSpec.execute)
		{
			runTest(testSpec);
		}
	}

	private void runTest(TestSpec tSpec) throws IOException, URISyntaxException {
		// locate the source and target files
		String name = "/fileSql/" + tSpec.id + ".fml";
		URL source = getClass().getResource(name);

		Assert.assertNotNull("Schema not found: " + name, source);

		String createSql = "/fileSql/create_" + tSpec.id + ".sql";
		URL target = getClass().getResource(createSql);

		Assert.assertNotNull("Results sql not found: " + createSql, target);

		String insertSql = "/fileSql/insert_" + tSpec.id + ".sql";
		URL insert = getClass().getResource(insertSql);

		Assert.assertNotNull("Results sql not found: " + insertSql, insert);

		String selectSql = "/fileSql/select_" + tSpec.id + ".sql";
		URL select = getClass().getResource(selectSql);

		Assert.assertNotNull("Select sql not found: " + selectSql, select);

		String expectSql = IOUtils.readURLToString(target);

		DataFileSchema schemaSource = dfm.loadDataFileSchema(new File(source.toURI()), tSpec.id);

		String actSqlName = schemaSource.createTemporaryDbTableName(1383689477174L, 1);

		String actSql = schemaSource.createTemporaryDbTableSql(actSqlName);

		Assert.assertEquals(tSpec.id, expectSql.replace("\r\n", "\n").replace("\r", ""), actSql);

		String expectInsSql = IOUtils.readURLToString(insert);
		String actInsSql = schemaSource.createInsertSql("tableName");

		Assert.assertEquals(tSpec.id, expectInsSql.replace("\r\n", "\n").replace("\r", ""), actInsSql);

		String expectSelectSql = IOUtils.readURLToString(select);
		String actSelSql = schemaSource.createSelectSql("tableName");

		Assert.assertEquals(tSpec.id, expectSelectSql.replace("\r\n", "\n").replace("\r", ""), actSelSql);
	}

	private static final class TestSpec
	{
		String id;
		boolean execute = false;
	}
}
