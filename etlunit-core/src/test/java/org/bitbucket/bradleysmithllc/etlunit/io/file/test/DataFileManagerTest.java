package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class DataFileManagerTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	DataFileManager dfm;

	@Before
	public void setup() throws IOException {
		File root = temporaryFolder.newFolder();
		dfm = new DataFileManagerImpl(root);
		System.out.println("Using root : " + root.getAbsolutePath());
	}

	@Test
	public void serialize() throws IOException {
		DataFileSchema sourceDFS = dfm.loadDataFileSchemaFromResource("DataFileManagerTest_serialize.fml", "sch");

		// make sure our thing is good first
		Assert.assertTrue(sourceDFS.getColumn("TICKET").hasTypeAnnotation());

		// serialize and compare to the original
		String json = sourceDFS.toJsonString();

		File output = temporaryFolder.newFile();

		FileUtils.write(output, json);

		// load it back in
		DataFileSchema dfs2 = dfm.loadDataFileSchema(output, "sch");
		Assert.assertEquals(sourceDFS.getColumn("TICKET").getTypeAnnotation(), dfs2.getColumn("TICKET").getTypeAnnotation());

		// compare to the json string from before
		Assert.assertEquals(json, dfs2.toJsonString());
	}

	@Test
	public void testDefaultTokens()
	{
		Assert.assertEquals(DataFileManager.DEFAULT_COLUMN_DELIMITER, dfm.getDefaultColumnDelimiter());
		Assert.assertEquals(DataFileManager.DEFAULT_ROW_DELIMITER, dfm.getDefaultRowDelimiter());
		Assert.assertEquals(DataFileManager.DEFAULT_NULL_TOKEN, dfm.getDefaultNullToken());
		Assert.assertEquals(DataFileManager.DEFAULT_FORMAT_TYPE, dfm.getDefaultFormatType());

		dfm.setDefaultColumnDelimiter("blah");
		Assert.assertEquals("blah", dfm.getDefaultColumnDelimiter());
		Assert.assertEquals(DataFileManager.DEFAULT_ROW_DELIMITER, dfm.getDefaultRowDelimiter());
		Assert.assertEquals(DataFileManager.DEFAULT_NULL_TOKEN, dfm.getDefaultNullToken());
		Assert.assertEquals(DataFileManager.DEFAULT_FORMAT_TYPE, dfm.getDefaultFormatType());

		dfm.setDefaultRowDelimiter("bloh");
		Assert.assertEquals("bloh", dfm.getDefaultRowDelimiter());
		Assert.assertEquals(DataFileManager.DEFAULT_NULL_TOKEN, dfm.getDefaultNullToken());
		Assert.assertEquals(DataFileManager.DEFAULT_FORMAT_TYPE, dfm.getDefaultFormatType());

		dfm.setDefaultNullToken("blih");
		Assert.assertEquals("blih", dfm.getDefaultNullToken());
		Assert.assertEquals(DataFileManager.DEFAULT_FORMAT_TYPE, dfm.getDefaultFormatType());

		dfm.setDefaultFormatType(DataFileSchema.format_type.fixed);
		Assert.assertEquals(DataFileSchema.format_type.fixed, dfm.getDefaultFormatType());

		Assert.assertEquals("blah", dfm.getDefaultColumnDelimiter());
		Assert.assertEquals("bloh", dfm.getDefaultRowDelimiter());
		Assert.assertEquals("blih", dfm.getDefaultNullToken());
		Assert.assertEquals(DataFileSchema.format_type.fixed, dfm.getDefaultFormatType());
	}

	@Test
	public void testDefaultTokensInDataFilesSchemas() throws IOException
	{
		DataFileSchema sch = dfm.createDataFileSchema("test");

		Assert.assertEquals(dfm.getDefaultColumnDelimiter(), sch.getColumnDelimiter());
		Assert.assertEquals(dfm.getDefaultRowDelimiter(), sch.getRowDelimiter());
		Assert.assertEquals(dfm.getDefaultNullToken(), sch.getNullToken());

		sch.setColumnDelimiter("blah");
		Assert.assertEquals("blah", sch.getColumnDelimiter());
		Assert.assertEquals(dfm.getDefaultRowDelimiter(), sch.getRowDelimiter());
		Assert.assertEquals(dfm.getDefaultNullToken(), sch.getNullToken());

		sch.setRowDelimiter("bloh");
		Assert.assertEquals("bloh", sch.getRowDelimiter());
		Assert.assertEquals(dfm.getDefaultNullToken(), sch.getNullToken());

		dfm.setDefaultNullToken("blih");
		dfm.setDefaultFormatType(DataFileSchema.format_type.fixed);
		dfm.setDefaultColumnDelimiter("c");
		dfm.setDefaultRowDelimiter("r");
		dfm.setDefaultNullToken("nil");

		sch = dfm.createDataFileSchema("test");

		Assert.assertEquals(null, sch.getColumnDelimiter());
		Assert.assertEquals(dfm.getDefaultRowDelimiter(), sch.getRowDelimiter());
		Assert.assertEquals(dfm.getDefaultNullToken(), sch.getNullToken());
		Assert.assertEquals(dfm.getDefaultFormatType(), sch.getFormatType());

		dfm.setDefaultFormatType(DataFileSchema.format_type.delimited);

		sch = dfm.createDataFileSchema("test");

		Assert.assertEquals(dfm.getDefaultColumnDelimiter(), sch.getColumnDelimiter());
		Assert.assertEquals(dfm.getDefaultRowDelimiter(), sch.getRowDelimiter());
		Assert.assertEquals(dfm.getDefaultNullToken(), sch.getNullToken());
		Assert.assertEquals(dfm.getDefaultFormatType(), sch.getFormatType());
	}

	@Test
	public void createSchemaAndDefaultState() throws IOException
	{
		dfm.setDefaultFormatType(DataFileSchema.format_type.fixed);

		DataFileSchema sch = dfm.createDataFileSchema("test");

		Assert.assertEquals(DataFileSchema.format_type.fixed, sch.getFormatType());

		// for fixed, column delimiter must be null
		Assert.assertNull(sch.getColumnDelimiter());

		// change the format type to delimited, the column delimiter must change to the default
		sch.setFormatType(DataFileSchema.format_type.delimited);

		Assert.assertEquals(dfm.getDefaultColumnDelimiter(), sch.getColumnDelimiter());

		sch.setFormatType(DataFileSchema.format_type.fixed);
		Assert.assertNull(sch.getColumnDelimiter());

		dfm.setDefaultColumnDelimiter("boo");

		sch.setFormatType(DataFileSchema.format_type.delimited);

		Assert.assertEquals(dfm.getDefaultColumnDelimiter(), sch.getColumnDelimiter());
	}

	@Test
	public void createDataSetUsesProperSchema() throws IOException
	{
		DataFileSchema sch = dfm.createDataFileSchema("test");

		DataFile set = dfm.loadDataFile(new File(""), sch);

		Assert.assertEquals(sch, set.getDataFileSchema());
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDataSetSchemaNoColumns() throws IOException
	{
		DataFileSchema sch = dfm.createDataFileSchema("test");

		sch.createSubViewIncludingColumns(Arrays.asList("ID"), "test.sub");
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDataSetSchemaInherits() throws IOException
	{
		dfm.setDefaultFormatType(DataFileSchema.format_type.fixed);
		DataFileSchema sch = dfm.createDataFileSchema("test");

		DataFileSchema.Column col = sch.createColumn("ID");
		col.setLength(5);
		sch.addColumn(col);

		col = sch.createColumn("ID2");
		col.setLength(5);
		sch.addColumn(col);
		col = sch.createColumn("ID2");
		col.setLength(5);
		sch.addColumn(col);

		col = sch.createColumn("ID3");
		col.setLength(5);
		sch.addColumn(col);

		DataFileSchema view = sch.createSubViewIncludingColumns(Arrays.asList("ID", "ID3"), "test.sub");
		Assert.assertEquals(2, view.getLogicalColumns().size());
		Assert.assertEquals("ID", view.getLogicalColumns().get(0).getId());
		Assert.assertEquals("ID3", view.getLogicalColumns().get(1).getId());
	}

	@Test
	public void orderedWriter() throws IOException
	{
		dfm.setDefaultFormatType(DataFileSchema.format_type.delimited);
		DataFileSchema sch = dfm.createDataFileSchema("test");

		DataFileSchema.Column col = sch.createColumn("id1");
		sch.addColumn(col);
		sch.addOrderColumn(col.getId());

		col = sch.createColumn("id2");
		sch.addColumn(col);
		sch.addOrderColumn(col.getId());

		File blah = temporaryFolder.newFile("blah");
		DataFile df = dfm.loadDataFile(blah, sch);

		DataFileWriter writer = df.writer();

		try
		{
			HashMap<String, Object> rowData = new HashMap<String, Object>();
			writer.addRow(rowData);

			rowData.put("id1", "2");
			writer.addRow(rowData);

			rowData.put("id1", "1");
			writer.addRow(rowData);

			rowData.put("id1", "11");
			writer.addRow(rowData);

			rowData.put("id2", "=7");
			rowData.put("id1", null);
			writer.addRow(rowData);

			rowData.put("id2", "=6");
			rowData.put("id1", null);
			writer.addRow(rowData);
		}
		finally
		{
			writer.close();
		}

		Assert.assertEquals("/*-- id1\tid2  --*/\n" +
				"/*-- VARCHAR\tVARCHAR  --*/\n" +
				"null\tnull\n" +
				"null\t=6\n" +
				"null\t=7\n" +
				"1\tnull\n" +
				"11\tnull\n" +
				"2\tnull", IOUtils.readFileToString(blah));
	}

	@Test
	public void jdbc()
	{
		dfm.getEmbeddedDatabase();
	}

	@Test
	public void testPersistentDataFile() throws IOException {
		DataFileSchema dsrSch = dfm.loadDataFileSchemaFromResource("fileSql/dataTypesTest.fml", "schema");

		File out = temporaryFolder.newFile();

		DataFile df = dfm.loadDataFile(out, dsrSch);

		DataFileWriter writer = df.writer();

		Map<String, Object> rowData = new HashMap<String, Object>();

		for (int i = 0; i < 100000; i++)
		{
			if (i % 20000 == 0)
			{
				System.out.println(i);
			}

			for (DataFileSchema.Column col : dsrSch.getLogicalColumns())
			{
				Object colVal = null;

				switch(col.getConverter().getJdbcType())
				{
					case Types.TIMESTAMP:
						colVal = new Timestamp(i);
						break;
					case Types.INTEGER:
						colVal = new Integer(i);
						break;
					case Types.SMALLINT:
						colVal = new Integer((i % 65535) + -32768);
						break;
					case Types.BIGINT:
						colVal = new Long(i * 1000000L);
						break;
					case Types.TINYINT:
						colVal = new Integer((i % 255) + -128);
						break;
					case Types.CHAR:
						colVal = String.valueOf(i).substring(0, 1);
						break;
					case Types.LONGVARCHAR:
						colVal = "LV" + String.valueOf(i);
						break;
					case Types.VARCHAR:
						colVal = "V" + String.valueOf(i);
						break;
					case Types.DATE:
						colVal = new Date(i);
						break;
					case Types.TIME:
						colVal = new Time(i);
						break;
					case Types.BIT:
						colVal = i % 2 == 0 ? Boolean.TRUE : Boolean.FALSE;
						break;
					case Types.FLOAT:
						colVal = new Float(Float.intBitsToFloat(i));
						break;
					case Types.DECIMAL:
						colVal = new BigDecimal(i * 2.2);
						break;
					case Types.DOUBLE:
						colVal = new Double(Double.longBitsToDouble(i * 3));
						break;
					case Types.NUMERIC:
						colVal = new BigDecimal((i * 3.3) % 100000);
						break;
					case Types.REAL:
						colVal = new Float(Float.intBitsToFloat(i * 2));
						break;
					case Types.BOOLEAN:
						colVal = i % 2 == 0 ? Boolean.TRUE : Boolean.FALSE;
						break;
					case Types.CLOB:
						break;
				}

				if (colVal != null)
				{
					rowData.put(col.getId(), colVal);
				}
			}

			writer.addRow(rowData);
		}

		writer.close();

		//System.out.println(FileUtils.readFileToString(out));
		dfm.dispose();
	}

	@Test
	public void orderByCaseInsensitive() throws IOException {
		DataFileSchema dsrSch = dfm.loadDataFileSchemaFromResource("fileSql/dataTypesTestCI.fml", "schema");

		Assert.assertEquals(Arrays.asList("real"), dsrSch.getKeyColumns().stream().map((col) -> {return col.getId();}).collect(Collectors.toList()));

		Assert.assertEquals(Arrays.asList("char", "bit", "boolean"), dsrSch.getOrderColumnNames());
	}

	@Test
	public void searchPath() {
		Assert.assertEquals("path/world.csv", DataFileManagerImpl.replaceLastPathElement("path/hello.csv", "world.csv"));
		Assert.assertEquals("hello/world.csv", DataFileManagerImpl.replaceLastPathElement("path.csv", "/hello/world.csv"));
		Assert.assertEquals("hello/world.csv", DataFileManagerImpl.replaceLastPathElement("path.csv", "hello/world.csv"));
		Assert.assertEquals("hello/world.csv", DataFileManagerImpl.replaceLastPathElement("/", "/hello/world.csv"));
		Assert.assertEquals("a/b/c/hello/world.csv", DataFileManagerImpl.replaceLastPathElement("/a/b/c/d.csv", "/hello/world.csv"));
	}
}
