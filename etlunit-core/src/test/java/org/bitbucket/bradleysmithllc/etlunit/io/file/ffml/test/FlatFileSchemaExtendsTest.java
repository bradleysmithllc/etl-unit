package org.bitbucket.bradleysmithllc.etlunit.io.file.ffml.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestName;

import java.io.*;

public class FlatFileSchemaExtendsTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Rule
	public TestName testName = new TestName();

	private DataFileManager dfm;

	@Before
	public void start() throws IOException {
		dfm = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	@Test
	public void schemaFromFileExtendsClasspath() throws IOException {
		String fml = ResourceUtils.loadResourceAsString(FlatFileSchemaExtendsTest.class, "fmlExtends/child.fml");

		File out = temporaryFolder.newFile("file.fml");

		FileUtils.write(out, fml);

		DataFileSchema finalSchema = dfm.loadDataFileSchema(out, "id");

		// verify it loaded the parent
		String res = finalSchema.toJsonString();
		System.out.println(res);
	}

	@Test
	public void schemaFromFileExtendsFile() throws IOException {
		String fml = ResourceUtils.loadResourceAsString(FlatFileSchemaExtendsTest.class, "fmlExtends/fileparent.fml");
		File out = temporaryFolder.newFile("fileparent.fml");
		FileUtils.write(out, fml);

		fml = ResourceUtils.loadResourceAsString(FlatFileSchemaExtendsTest.class, "fmlExtends/filechild.fml");
		out = temporaryFolder.newFile("filechild.fml");
		FileUtils.write(out, fml);

		DataFileSchema finalSchema = dfm.loadDataFileSchema(out, "id");

		// verify it loaded the parent
		String res = finalSchema.toJsonString();
		System.out.println(res);
	}

	@Test
	public void declaredColumns() throws IOException {
		testOverride();
	}

	@Test
	public void adjustPkNoneLeft() throws IOException {
		testOverride();
	}

	@Test
	public void adjustPk() throws IOException {
		testOverride();
	}

	@Test
	public void adjustOrderByNoneLeft() throws IOException {
		testOverride();
	}

	@Test
	public void adjustOrderBy() throws IOException {
		testOverride();
	}

	@Test
	public void columnNamesMismatch() throws IOException {
		testOverride();
	}

	@Test
	public void addColumns() throws IOException {
		testOverride();
	}

	public void testOverride() throws IOException {
		DataFileSchema finalSchema = dfm.loadDataFileSchemaFromResource("fmlExtends/" + testName.getMethodName() + ".fml", "res");

		// verify it loaded the parent
		String res = finalSchema.toJsonString();
		String expected = ResourceUtils.loadResourceAsString(getClass(), "fmlExtends/" + testName.getMethodName() + "_merged.fml");

		//System.out.println(res);
		Assert.assertEquals(expected, res);
	}

	@Test
	public void tree() throws IOException {
		testOverride();
	}

	@Test
	public void inheritedColumns() throws IOException {
		testOverride();
	}

	@Test
	public void parentHasEmptyColumns() throws IOException {
		testOverride();
	}

	@Test
	public void childHasEmptyColumns() throws IOException {
		testOverride();
	}

	@Test
	public void parentDoesNotDeclareColumns() throws IOException {
		testOverride();
	}

	@Test
	public void childDoesNotDeclareColumns() throws IOException {
		testOverride();
	}

	@Test
	public void noColumnOverlap() throws IOException {
		testOverride();
	}

	@Test
	public void resource() {
		Assert.assertNull(getClass().getResource("fmlExtends/parent.fml"));
		Assert.assertNotNull(getClass().getResource("/fmlExtends/parent.fml"));

		Assert.assertNotNull(getClass().getClassLoader().getResource("fmlExtends/parent.fml"));
		Assert.assertNull(getClass().getClassLoader().getResource("/fmlExtends/parent.fml"));
	}
}
