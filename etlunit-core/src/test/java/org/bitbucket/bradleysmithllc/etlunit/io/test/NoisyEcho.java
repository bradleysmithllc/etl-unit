package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class NoisyEcho
{
	public static void main(String [] argv)
	{
		for (int i = 0; i < 9; i++)
		{
			System.out.print("HelloHelloHelloGoodbye\n");
			System.out.print("HelloHelloHelloGoodbye\r");
			System.out.print("HelloHelloHelloGoodbye\r\n");
		}

		for (int i = 0; i < 8; i++)
		{
			System.err.print("uh-oh\n");
			System.err.print("uh-oh\r");
			System.err.print("uh-oh\r\n");
		}
	}
}
