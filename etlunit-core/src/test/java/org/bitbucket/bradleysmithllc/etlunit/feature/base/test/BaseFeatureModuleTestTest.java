package org.bitbucket.bradleysmithllc.etlunit.feature.base.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class BaseFeatureModuleTestTest extends BaseFeatureModuleTest
{
	private boolean assertVariableContextOperation;
	private boolean assertVariableContextClsPre;
	private boolean assertVariableContextMtPre;
	private boolean assertVariableContextMtPost;
	private boolean assertVariableContextClsPost;
	private boolean setUpSourceFiles;
	private boolean getTestFeatures;

	static int exitCode = 0;

	protected List<Feature> getTestFeatures()
	{
		getTestFeatures = true;

		return Arrays.asList((Feature) new RunnerAbstractFeature("processRunner"));
	}

	@Override
	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, int operationNumber)
	{
		assertVariableContextOperation = true;
	}

	@Override
	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context)
	{
		assertVariableContextMtPre = true;
	}

	@Override
	protected void assertVariableContextPre(ETLTestClass cl, VariableContext context)
	{
		assertVariableContextClsPre = true;
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed)
	{
		assertVariableContextMtPost = true;
	}

	@Override
	protected void assertVariableContextPost(ETLTestClass cl, VariableContext context, int testsProcessed)
	{
		assertVariableContextClsPost = true;
	}

	protected void setUpSourceFiles() throws IOException
	{
		setUpSourceFiles = true;
		createSource("test.etlunit", "class test { @Test setup(){warn(message: 'hi');boogie();}}");
	}

	@Test
	public void baseFeatureModuleCallbacksAreCalled() throws IOException
	{
		startTest();

		Assert.assertTrue(assertVariableContextClsPost);
		Assert.assertTrue(assertVariableContextOperation);
		Assert.assertTrue(assertVariableContextClsPre);
		Assert.assertTrue(assertVariableContextMtPre);
		Assert.assertTrue(assertVariableContextMtPost);
		Assert.assertTrue(setUpSourceFiles);
		Assert.assertTrue(getTestFeatures);
	}

	@Test
	public void runtimeOptionsAreAdded() throws IOException
	{
		final List<String> optionValue = new ArrayList<String>();
		final String stringValue = String.valueOf(System.currentTimeMillis());

		class OpTest extends BaseFeatureModuleTest
		{
			@Override
			protected List<Feature> getTestFeatures()
			{
				return Arrays.asList((Feature) new AbstractFeature(){
					@Inject
					public void setOption(@Named(value = "feature.option") RuntimeOption option)
					{
						if (!optionValue.contains(option.getStringValue()))
						{
							optionValue.add(option.getStringValue());
						}

						Assert.assertSame(this, option.getFeature());
					}

					@Override
					public FeatureMetaInfo getMetaInfo()
					{
						final Feature feature = this;

						return new FeatureMetaInfo() {
							@Override
							public String getDescribingClassName()
							{
								return getDescribing().getClass().getName();
							}

							@Override
							public Feature getDescribing()
							{
								return feature;
							}

							@Override
							public String getFeatureName()
							{
								return null;
							}

							@Override
							public String getFeatureConfiguration()
							{
								return null;
							}

							@Override
							public JsonSchema getFeatureConfigurationValidator()
							{
								return null;
							}

							@Override
							public JsonNode getFeatureConfigurationValidatorNode() {
								return null;
							}

							@Override
							public Map<String, FeatureOperation> getExportedOperations()
							{
								return null;
							}

							@Override
							public Map<String, FeatureAnnotation> getExportedAnnotations()
							{
								return null;
							}

							@Override
							public boolean isInternalFeature()
							{
								return false;
							}

							@Override
							public String getFeatureUsage()
							{
								return null;
							}

							@Override
							public List<RuntimeOptionDescriptor> getOptions()
							{
								return Arrays.asList((RuntimeOptionDescriptor) new RuntimeOptionDescriptorImpl("option", "", ""));
							}
						};
					}

					@Override
					public String getFeatureName()
					{
						return "feature";
					}
				});
			}

			@Override
			protected void setUpSourceFiles() throws IOException
			{
			}

			@Override
			protected List<RuntimeOption> getRuntimeOptionOverrides()
			{
				return Arrays.asList(new RuntimeOption("feature.option", stringValue));
			}
		}

		OpTest opt = new OpTest();

		opt.startTest();

		Assert.assertEquals(1, optionValue.size());
		Assert.assertEquals(stringValue, optionValue.get(0));
	}

	@Test
	public void processLogMatched() throws IOException
	{
		TestResults met = startTest("PROCESS_MATCHES_LOG");
		Assert.assertEquals(1, met.getNumTestsSelected());
		Assert.assertEquals(1, met.getMetrics().getNumberOfTestsPassed());
	}

	@Test(expected = AssertionError.class)
	public void processLogNotMatched() throws IOException
	{
		startTest("PROCESS_NOT_MATCHES_LOG");
	}

	@Test
	public void mockProcessMatched() throws IOException
	{
		TestResults met = startTest("PROCESS_INVOKES_MOCKER");
		Assert.assertEquals(1, met.getNumTestsSelected());
		Assert.assertEquals(1, met.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(736500, exitCode);
	}

	@Test
	public void mockProcessNotMatched() throws IOException
	{
		TestResults met = startTest("PROCESS_INVOKES_MOCKER_NOT_MATCHES");
		Assert.assertEquals(1, met.getNumTestsSelected());
		Assert.assertEquals(0, met.getMetrics().getNumberOfTestsPassed());
	}

	private static class RunnerAbstractFeature extends AbstractFeature
	{
		private final String name;

		private RuntimeSupport runtimeSupport;

		private RunnerAbstractFeature(String name)
		{
			this.name = name;
		}

		@Inject
		public void receiveRuntimeSupport(RuntimeSupport support)
		{
			runtimeSupport = support;
		}

		@Override
		public ClassListener getListener()
		{
			return new NullClassListener()
			{
				@Override
				public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, final int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					try
					{
						ProcessFacade facade = runtimeSupport.execute(new ProcessDescription("test.cmd"));
						facade.waitForCompletion();
						BaseFeatureModuleTestTest.exitCode = facade.getCompletionCode();
					}
					catch (IOException e)
					{
						throw new RuntimeException(e);
					}

					return action_code.handled;
				}
			};
		}

		@Override
		public List<String> getPrerequisites()
		{
			return Collections.EMPTY_LIST;
		}

		@Override
		public ClassDirector getDirector()
		{
			return new NullClassDirector()
			{
				@Override
				public response_code accept(ETLTestClass cl)
				{
					return response_code.accept;
				}

				@Override
				public response_code accept(ETLTestMethod mt)
				{
					return response_code.accept;
				}

				@Override
				public response_code accept(ETLTestOperation op)
				{
					return response_code.accept;
				}
			};
		}

		public String getFeatureName()
		{
			return name;
		}
	}
}
