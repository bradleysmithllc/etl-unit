package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.io.JavaForker;
import org.bitbucket.bradleysmithllc.etlunit.io.StreamProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.io.StreamProcessExecutorProcessFacade;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;

public class JavaForkerInteractiveTest
{
	enum spool_type {stdout, stderr}
	class Command
	{
		final spool_type type;
		final String message;

		Command(spool_type type, String message) {
			this.type = type;
			this.message = message;
		}
	}

	Command [] commands = new Command[]{
			new Command(spool_type.stdout, "Hello"),
			new Command(spool_type.stderr, "Herro"),
			new Command(spool_type.stderr, "Herro"),
			new Command(spool_type.stderr, "Herro"),
			new Command(spool_type.stderr, "Herro"),
			new Command(spool_type.stderr, "Herro"),
			new Command(spool_type.stdout, "Hello")
	};

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void t() throws IOException, InterruptedException {
		BasicRuntimeSupport brs = new BasicRuntimeSupport(temporaryFolder.newFolder());
		brs.installProcessExecutor(new StreamProcessExecutor());
		PrintWriterLog applicationLogger = new PrintWriterLog();

		brs.setApplicationLogger(applicationLogger);
		brs.setUserLogger(applicationLogger);

		JavaForker forker = new JavaForker(brs);
		forker.setMainClass(Echoer.class);
		//forker.setOutput(new File("/tmp/output"));

		StreamProcessExecutorProcessFacade proc = (StreamProcessExecutorProcessFacade) forker.startProcess();

		Writer stdin = proc.getWriter();

		BufferedReader brout = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		BufferedReader brerr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

		// loop and send interactive commands
		for (int i = 0; i < 10; i++)
		{
			for (Command command : commands)
			{
				Assert.assertEquals("on:", brout.readLine());

				stdin.write(command.type.name() + "\n");
				stdin.write(command.message + "\n");
				stdin.flush();

				switch(command.type)
				{
					case stdout:
						Assert.assertEquals(command.message, brout.readLine());
						break;
					case stderr:
						Assert.assertEquals(command.message, brerr.readLine());
						break;
				}
			}
		}

		Assert.assertEquals("on:", brout.readLine());

		stdin.write("exit\n");
		stdin.write("17\n");
		stdin.flush();

		Assert.assertEquals("bye", brout.readLine());

		Assert.assertNull(brout.readLine());
		Assert.assertNull(brerr.readLine());

		proc.waitForCompletion();
		int ec = proc.getCompletionCode();

		Assert.assertEquals(17, ec);
	}
}
