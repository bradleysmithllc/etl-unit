package org.bitbucket.bradleysmithllc.etlunit.test_support.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.test_support.MockProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.test_support.ScriptMockCallback;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

public class MockProcessExecutorTest
{
	@Test
	public void inputNoError() throws IOException
	{
		BasicRuntimeSupport rs = new BasicRuntimeSupport();
		rs.installProcessExecutor(new MockProcessExecutor(new ScriptMockCallback("mockProcessStreams")));
		rs.setApplicationLogger(new PrintWriterLog());

		ProcessFacade facade = rs.execute(new ProcessDescription("pmrep").argument("inputNoError"));

		BufferedReader bufferedReader = new BufferedReader(facade.getReader());

		long start = System.currentTimeMillis();

		String line = bufferedReader.readLine();
		Assert.assertEquals("prompt>one", line);

		long end = System.currentTimeMillis();

		long time = end - start;
		Assert.assertTrue(time >= 1750 && time <= 2250);

		start = System.currentTimeMillis();

		line = bufferedReader.readLine();
		Assert.assertEquals("prompt>two", line);

		end = System.currentTimeMillis();

		time = end - start;
		Assert.assertTrue(time >= 2250 && time <= 2750);

		start = System.currentTimeMillis();

		line = bufferedReader.readLine();
		Assert.assertEquals("prompt>three", line);

		end = System.currentTimeMillis();

		time = end - start;
		Assert.assertTrue(time >= 2750 && time <= 3250);

		Assert.assertEquals("", facade.getErrorBuffered().toString());
	}

	@Test
	public void ignoredScript() throws IOException
	{
		BasicRuntimeSupport rs = new BasicRuntimeSupport();
		rs.installProcessExecutor(new MockProcessExecutor(new ScriptMockCallback("mockProcessExitCodes")));
		rs.setApplicationLogger(new PrintWriterLog());

		ProcessFacade facade = rs.execute(new ProcessDescription("pmrep").argument("create"));
		facade.waitForCompletion();
		Assert.assertEquals(0, facade.getCompletionCode());
		Assert.assertEquals("prompt>blah" + IOUtils.LINE_SEPARATOR + "prompt>", facade.getInputBuffered().toString());
		Assert.assertEquals("IOException" + IOUtils.LINE_SEPARATOR +
				"AnotherIOException" + IOUtils.LINE_SEPARATOR, facade.getErrorBuffered().toString());
	}

	@Test
	public void exitCodes() throws IOException
	{
		BasicRuntimeSupport rs = new BasicRuntimeSupport();
		rs.installProcessExecutor(new MockProcessExecutor(new ScriptMockCallback("mockProcessExitCodes")));
		rs.setApplicationLogger(new PrintWriterLog());

		ProcessFacade facade = rs.execute(new ProcessDescription("pmrep"));
		Assert.assertEquals(-1, facade.getCompletionCode());

		facade = rs.execute(new ProcessDescription("pmrep").argument("exit_1"));
		Assert.assertEquals(1, facade.getCompletionCode());

		facade = rs.execute(new ProcessDescription("pmrep").argument("exit_default"));
		Assert.assertEquals(0, facade.getCompletionCode());

		facade = rs.execute(new ProcessDescription("pmrep").argument("create"));
		facade.waitForCompletion();
		Assert.assertEquals(0, facade.getCompletionCode());
	}

	@Test(expected = IllegalThreadStateException.class)
	public void dontCallExitCodeBeforeCompletion() throws IOException
	{
		BasicRuntimeSupport rs = new BasicRuntimeSupport();
		rs.installProcessExecutor(new MockProcessExecutor(new ScriptMockCallback("mockProcessExitCodes")));
		rs.setApplicationLogger(new PrintWriterLog());

		ProcessFacade facade = rs.execute(new ProcessDescription("pmrep").argument("exit_3000"));
		facade.getCompletionCode();
	}

	@Test
	public void runTime() throws IOException
	{
		BasicRuntimeSupport rs = new BasicRuntimeSupport();
		rs.installProcessExecutor(new MockProcessExecutor(new ScriptMockCallback("mockProcessRunTimes")));
		rs.setApplicationLogger(new PrintWriterLog());

		ProcessFacade facade = rs.execute(new ProcessDescription("pmrep"));

		long start = System.currentTimeMillis();

		facade.waitForCompletion();

		long end = System.currentTimeMillis();

		long time = end - start;
		Assert.assertTrue(time >= 2500 && time <= 3500);

		facade = rs.execute(new ProcessDescription("pmrep").argument("nowait"));

		start = System.currentTimeMillis();

		facade.waitForCompletion();

		end = System.currentTimeMillis();

		time = end - start;
		Assert.assertTrue(time >= 0 && time <= 100);
	}

	@Test
	public void streamsAvailable() throws IOException
	{
		BasicRuntimeSupport rs = new BasicRuntimeSupport();
		rs.installProcessExecutor(new MockProcessExecutor(new ScriptMockCallback("mockProcessRunTimes")));
		rs.setApplicationLogger(new PrintWriterLog());

		ProcessFacade facade = rs.execute(new ProcessDescription("pmrep").argument("waitStreams"));

		long start = System.currentTimeMillis();

		facade.waitForCompletion();

		long end = System.currentTimeMillis();

		long time = end - start;
		Assert.assertTrue(time >= 2500 && time <= 3500);

		facade = rs.execute(new ProcessDescription("pmrep").argument("waitStreams"));

		start = System.currentTimeMillis();

		facade.waitForStreams();

		end = System.currentTimeMillis();

		time = end - start;
		Assert.assertTrue(time >= 500 && time <= 1500);

		facade = rs.execute(new ProcessDescription("pmrep").argument("waitStreamsLonger"));

		start = System.currentTimeMillis();

		facade.waitForStreams();

		end = System.currentTimeMillis();

		time = end - start;
		Assert.assertTrue(time >= 3500 && time <= 4500);

		facade = rs.execute(new ProcessDescription("pmrep").argument("waitStreamsLonger"));

		start = System.currentTimeMillis();

		facade.waitForCompletion();

		end = System.currentTimeMillis();

		time = end - start;
		Assert.assertTrue(time >= 3500 && time <= 4500);
	}
}
