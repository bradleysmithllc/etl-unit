package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ClassUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.MapBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.*;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.*;

public class DataFileManagerCopyDateDiffsTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	private DataFileSchema srcSchema;
	private DataFileSchema src2Schema;
	private DataFileSchema destSchema;

	@Before
	public void start() throws IOException {
		File tmpRoot = temporaryFolder.newFolder();
		dfm = new DataFileManagerImpl(tmpRoot);

		srcSchema = dfm.loadDataFileSchemaFromResource("fileCopyDateDiffs/src.fml", "src");
		src2Schema = dfm.loadDataFileSchemaFromResource("fileCopyDateDiffs/src2.fml", "src2");
		destSchema = dfm.loadDataFileSchemaFromResource("fileCopyDateDiffs/dest.fml", "dest");
	}

	@Test
	public void copyWithNullDates() throws IOException {
		// create input file
		DataFile sdf = makeSourceFile(MapBuilder.makeMap("intcol", (Object) 1).putMap("timecol", null).putMap("timestampcol", null).putMap("datecol", null).map());

		// prepare destination file
		diffAndCompare(sdf, "copyWithNullDates");
	}

	private void diffAndCompare(DataFile sdf, String copyWithNullDates) throws IOException {
		File destFile = temporaryFolder.newFile("dest.delimited");
		DataFile ddf = dfm.loadDataFile(destFile, destSchema);

		// copy to output
		dfm.copyDataFile(sdf, ddf, new CopyOptionsBuilder().withHonorDateDiff(DataFileManager.CopyOptions.honor_date_diff.yes).options());

		String res = FileUtils.readFileToString(destFile);

		Assert.assertEquals(ResourceUtils.loadResourceAsString(getClass(), "fileCopyDateDiffs/" + copyWithNullDates + "_res.delimited"), res);

		List<FileDiff> diff = dfm.diff(sdf, ddf);

		Assert.assertEquals(0, diff.size());
	}

	private DataFile makeSourceFile(Map<String, Object> map) throws IOException {
		return makeSourceFile(map, srcSchema);
	}

	private DataFile makeSourceFile(Map<String, Object> map, DataFileSchema schema) throws IOException {
		File srcFile = temporaryFolder.newFile("src.delimited");
		DataFile sdf = dfm.loadDataFile(srcFile, schema);
		DataFileWriter dfw = sdf.writer();
		dfw.addRow(map);
		dfw.close();

		return sdf;
	}

	@Test
	public void copyNonDates() throws IOException {
		// create input file
		DataFile sdf = makeSourceFile(MapBuilder.makeMap("intcol", (Object) 1).putMap("timecol", "00:00:58.987").putMap("timestampcol", "2020-01-02 04:03:00.000").putMap("datecol", "2020-01-01").map(), src2Schema);

		// prepare destination file
		diffAndCompare(sdf, "copyNonDates");
	}

	@Test
	public void copyWithStaticDates() throws IOException {
		// create input file
		DataFile sdf = makeSourceFile(MapBuilder.makeMap("intcol", (Object) 1).putMap("timecol", LocalTime.of(4, 2, 4)).putMap("timestampcol", LocalDateTime.of(2020, 1, 2, 4, 3)).putMap("datecol", LocalDate.of(2020, 2, 1)).map());

		// prepare destination file
		diffAndCompare(sdf, "copyWithStaticDates");
	}

	@Test
	public void copyWithRecentDates() throws IOException {
		// create input file
		DataFile sdf = makeSourceFile(MapBuilder.makeMap("intcol", (Object) 1).putMap("timecol", LocalTime.now()).putMap("timestampcol", LocalDateTime.now()).putMap("datecol", LocalDate.now()).map());

		// prepare destination file
		try {
			diffAndCompare(sdf, "copyWithRecentDates");
		} catch (DateTimeParseException exc) {
			System.out.println();
		}
	}

	@Test
	public void copyWithFutureDates() throws IOException {
		// create input file
		DataFile sdf = makeSourceFile(MapBuilder.makeMap("intcol", (Object) 1).putMap("timecol", LocalTime.of(4, 2, 4)).putMap("timestampcol", LocalDateTime.of(2040, 1, 2, 4, 3)).putMap("datecol", LocalDate.of(2040, 2, 1)).map());

		// prepare destination file
		diffAndCompare(sdf, "copyWithFutureDates");
	}
}
