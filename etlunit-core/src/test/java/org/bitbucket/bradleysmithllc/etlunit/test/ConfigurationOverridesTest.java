package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.fge.jackson.JsonLoader;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.Configuration;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

@RunWith(Parameterized.class)
public class ConfigurationOverridesTest
{
	public static final class TestSpec
	{
		String id;
		String [] profiles;

		JsonNode homeConfig;
		Map<String, JsonNode> homeProfileConfigs = new HashMap<String, JsonNode>();

		JsonNode projectConfig;
		Map<String, JsonNode> projectProfileConfigs = new HashMap<String, JsonNode>();

		JsonNode classpathConfig;
		Map<String, JsonNode> classpathProfileConfigs = new HashMap<String, JsonNode>();

		JsonNode superOverride;

		JsonNode assertion;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> data() throws Exception
	{
		List<Object []> specArray = new ArrayList<Object []>();

		JsonNode testJson = JsonLoader.fromResource("/configurationOverrides/tests.json");

		Iterator<Map.Entry<String, JsonNode>> testFields = testJson.fields();

		while (testFields.hasNext())
		{
			Map.Entry<String, JsonNode> spec = testFields.next();
			JsonNode valueNode = spec.getValue();

			TestSpec sp = new TestSpec();
			sp.id = spec.getKey();

			sp.homeConfig = valueNode.get("homeConfig");
			sp.projectConfig = valueNode.get("projectConfig");
			sp.classpathConfig = valueNode.get("classpathConfig");
			sp.superOverride = valueNode.get("superOverride");

			JsonNode profilesNode = valueNode.get("profiles");

			ArrayNode anode = (ArrayNode) profilesNode;

			if (anode != null)
			{
				sp.profiles = new String[anode.size()];
				for (int i = 0; i < sp.profiles.length; i++)
				{
					sp.profiles[i] = anode.get(i).asText();
				}
			}

			JsonNode homeProfilesNode = valueNode.get("homeProfiles");

			if (homeProfilesNode != null)
			{
				Iterator<Map.Entry<String,JsonNode>> hpit = homeProfilesNode.fields();

				while (hpit.hasNext())
				{
					Map.Entry<String, JsonNode> hp = hpit.next();

					sp.homeProfileConfigs.put(hp.getKey(), hp.getValue());
				}
			}

			JsonNode projectProfilesNode = valueNode.get("projectProfiles");

			if (projectProfilesNode != null)
			{
				Iterator<Map.Entry<String,JsonNode>> hpit = projectProfilesNode.fields();

				while (hpit.hasNext())
				{
					Map.Entry<String, JsonNode> hp = hpit.next();

					sp.projectProfileConfigs.put(hp.getKey(), hp.getValue());
				}
			}

			JsonNode classpathProfilesNode = valueNode.get("classpathProfiles");

			if (classpathProfilesNode != null)
			{
				Iterator<Map.Entry<String,JsonNode>> hpit = classpathProfilesNode.fields();

				while (hpit.hasNext())
				{
					Map.Entry<String, JsonNode> hp = hpit.next();

					sp.classpathProfileConfigs.put(hp.getKey(), hp.getValue());
				}
			}

			sp.assertion = valueNode.get("assertion");

			specArray.add(new TestSpec[]{sp});
		}

		return specArray;
	}

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private final TestSpec test;
	public ConfigurationOverridesTest(TestSpec test)
	{
		this.test = test;
	}

	private String currHome;
	private File userHome;

	File configRoot1;
	File configRoot2;

	File remoteUrlRoot;
	File remoteConfigRoot;

	URLClassLoader loader;

	@Before
	public void setupHome() throws IOException {
		userHome = temporaryFolder.newFolder();

		// set it in the properties so that the config class picks it up
		currHome = System.getProperty("user.home");
		System.setProperty("user.home", userHome.getAbsolutePath());

		configRoot1 = temporaryFolder.newFolder();
		configRoot1.mkdirs();

		configRoot2 = temporaryFolder.newFolder();
		configRoot2.mkdirs();

		remoteUrlRoot = temporaryFolder.newFolder();
		remoteUrlRoot.mkdirs();

		remoteConfigRoot = new File(remoteUrlRoot, "config");
		remoteConfigRoot.mkdirs();

		loader = new URLClassLoader(
			new URL[]{
				remoteUrlRoot.toURI().toURL(),
				remoteConfigRoot.toURI().toURL()
			}
		);
	}

	@After
	public void resetHome()
	{
		System.setProperty("user.home", currHome);
	}

	@Test
	public void run() throws IOException, ParseException {
		// set up user home config
		writeConfig(test.homeConfig, Configuration.getEtlunitUserConfig());

		for (Map.Entry<String, JsonNode> profileEntry : test.homeProfileConfigs.entrySet())
		{
			writeConfig(profileEntry.getValue(), Configuration.getEtlunitUserProfile(profileEntry.getKey()));
		}

		// set up project config
		writeConfig(test.projectConfig, new File(configRoot1, "etlunit.json"));

		for (Map.Entry<String, JsonNode> profileEntry : test.projectProfileConfigs.entrySet())
		{
			writeConfig(profileEntry.getValue(), new File(configRoot1, profileEntry.getKey() + ".json"));
		}

		// set up classpath config
		writeConfig(test.classpathConfig, new File(remoteConfigRoot, "etlunit.json"));

		for (Map.Entry<String, JsonNode> profileEntry : test.classpathProfileConfigs.entrySet())
		{
			writeConfig(profileEntry.getValue(), new File(remoteConfigRoot, profileEntry.getKey() + ".json"));
		}

		// initialize configuration and compare results
		Configuration config = Configuration.loadFromEnvironment(
			new File [] {configRoot1, configRoot2},
			test.profiles,
			loader,
			test.superOverride == null ? null : ETLTestParser.loadObject(test.superOverride.toString())
		);

		// assertion
		Assert.assertEquals(test.id, test.assertion, config.getRoot().getJsonNode());
	}

	private void writeConfig(JsonNode homeConfig, File etlunitUserConfig) throws IOException {
		if (homeConfig != null)
		{
			FileUtils.write(etlunitUserConfig, homeConfig.toString());
		}
	}
}
