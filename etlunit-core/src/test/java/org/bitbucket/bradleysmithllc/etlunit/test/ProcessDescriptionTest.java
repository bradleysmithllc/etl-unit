package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ProcessDescriptionTest
{
	@Test
	public void deltaIsEmpty()
	{
		ProcessDescription pd = new ProcessDescription("");

		Assert.assertEquals("{}", pd.deltaEnvironment().toString());
	}

	@Test
	public void deltaGetsUpdates()
	{
		ProcessDescription pd = new ProcessDescription("");
		pd.environment("5", "1");
		pd.getEnvironment().put("6", "2");

		Assert.assertEquals("{5=1, 6=2}", pd.deltaEnvironment().toString());
	}

	@Test
	public void deltaGetsChangesToExistingVariables()
	{
		ProcessDescription pd = new ProcessDescription("");

		// ugly way to get a key from the environment
		Map.Entry<String, String> envvar = pd.getEnvironment().entrySet().iterator().next();

		String key = envvar.getKey();
		String value = envvar.getValue() + "._.";
		pd.environment(key, value);

		Assert.assertEquals("{" + key + "=" + value + "}", pd.deltaEnvironment().toString());
	}
}
