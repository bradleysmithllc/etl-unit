package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.io.file.SchemaColumn;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;

public class SchemaColumnTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void invalidSchemaColumn() throws IOException {
		expectedException.expect(IllegalArgumentException.class);
		new SchemaColumn(JsonLoader.fromString("{\"id\": \"ID\", \"lenient-formatter\": true}"), null);
	}

	@Test
	public void lenientSchemaColumnFormatter() throws IOException {
		Assert.assertTrue(new SchemaColumn(JsonLoader.fromString("{\"id\": \"ID\", \"lenient-formatter\": true, \"format\": \"Y\"}"), null).isFormatterLenient());
	}

	@Test
	public void illenientSchemaColumnFormatter() throws IOException {
		Assert.assertFalse(new SchemaColumn(JsonLoader.fromString("{\"id\": \"ID\", \"lenient-formatter\": false, \"format\": \"Y\"}"), null).isFormatterLenient());
	}

	@Test
	public void dateDiffMustString() throws IOException {
		expectedException.expect(IllegalArgumentException.class);
		new SchemaColumn(JsonLoader.fromString("{\"id\": \"ID\", \"type\": \"INTEGER\", \"diff-type\": \"date\"}"), null);
	}

	@Test
	public void dateDiffString() throws IOException {
		Assert.assertEquals(DataFileSchema.Column.diff_type.date, new SchemaColumn(JsonLoader.fromString("{\"id\": \"ID\", \"type\": \"VARCHAR\", \"diff-type\": \"date\"}"), null).diffType());
	}

	@Test
	public void timestampDiffString() throws IOException {
		Assert.assertEquals(DataFileSchema.Column.diff_type.timestamp, new SchemaColumn(JsonLoader.fromString("{\"id\": \"ID\", \"type\": \"VARCHAR\", \"diff-type\": \"timestamp\"}"), null).diffType());
	}

	@Test
	public void noneDiffString() throws IOException {
		Assert.assertEquals(DataFileSchema.Column.diff_type.none, new SchemaColumn(JsonLoader.fromString("{\"id\": \"ID\", \"type\": \"VARCHAR\", \"diff-type\": \"none\"}"), null).diffType());
	}

	@Test
	public void noneDiffString2() throws IOException {
		Assert.assertEquals(DataFileSchema.Column.diff_type.none, new SchemaColumn(JsonLoader.fromString("{\"id\": \"ID\", \"type\": \"VARCHAR\"}"), null).diffType());
	}
}
