package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Test;

public class FlatFileDiffTest
{
	/*
	@Test
	public void simpleFileDiff() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|b|c|d|\0"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|b|c|d|null"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "\0");
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft);

		Assert.assertNotNull(list);
		Assert.assertEquals(0, list.size());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void orderKeyOutOfOrder() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|b|c|d|1"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|b|c|d|2"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "\0");
		fs.setKeyColumns(Arrays.asList("2", "1"));

		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");
		ft.setKeyColumns(Arrays.asList("2", "1"));

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft);

		Assert.assertNotNull(list);
		Assert.assertEquals(1, list.size());

		Assert.assertEquals("b|a|", list.get(0).getOrderKey());

		fs = new FlatFile(f1, "\\|", "\n", "\0");
		fs.setKeyColumns(Arrays.asList("1", "2"));

		ft = new FlatFile(f2, "\\|", "\n", "null");
		ft.setKeyColumns(Arrays.asList("1", "2"));

		ffd = new FlatFileDiff(fs);
		list = ffd.diffFile(ft);

		Assert.assertNotNull(list);
		Assert.assertEquals(1, list.size());

		Assert.assertEquals("a|b|", list.get(0).getOrderKey());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void simpleFileDiffIgnoredColumns1() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|b|c|d|e"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1|2|3|5\nINTEGER|INTEGER|INTEGER|INTEGER\na|b|c|e"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "null");
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft, Arrays.asList("1", "2", "3", "5"));

		Assert.assertNotNull(list);
		Assert.assertEquals(0, list.size());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void simpleFileDiffIgnoredColumns2() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|b|c|d|e"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|b|c|g|e"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "null");
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft, Arrays.asList("1", "2", "3", "5"));

		Assert.assertNotNull(list);
		Assert.assertEquals(0, list.size());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void simpleFileDiffColumns() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\nb|b|c|d|e"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1|2|3|4|5\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|b|c|g|e"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "null");
		fs.setKeyColumns(Arrays.asList("2", "3", "5"));
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");
		ft.setKeyColumns(Arrays.asList("2", "3", "5"));

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft, Arrays.asList("1", "2", "3", "5"));

		Assert.assertNotNull(list);
		Assert.assertEquals(1, list.size());
		Assert.assertEquals("1", list.get(0).getColumnName());
		Assert.assertEquals(1, list.get(0).getSourceRowNumber());
		Assert.assertEquals(1, list.get(0).getTargetRowNumber());
		Assert.assertEquals("b", list.get(0).getSourceValue());
		Assert.assertEquals("a", list.get(0).getOtherValue());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void completelyMismatched() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1|3|7\nINTEGER|INTEGER|INTEGER\na|b|c"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1|2|3|4|5|6|7\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|2|b|4|5|6|c"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "null");
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft);

		Assert.assertNotNull(list);
		Assert.assertEquals(0, list.size());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void completelyMismatched2() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1|3|7\nINTEGER|INTEGER|INTEGER\na|b|c"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1|2|3|4|5|6|7\nINTEGER|INTEGER|INTEGER|INTEGER|INTEGER|INTEGER|INTEGER\na|2|b|4|5|6|c"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "null");
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft);

		Assert.assertNotNull(list);
		Assert.assertEquals(0, list.size());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void blankRowsAreIgnored() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1\nINTEGER\na\nb\n\n\nc"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1\nINTEGER\na\nb\nc\n\n"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "null");
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft);

		Assert.assertNotNull(list);
		Assert.assertEquals(0, list.size());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void commentsAreIgnored() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("1\nINTEGER\n/*Yabba* /\na"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("1\nINTEGER\na\n/*Dabba * /"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "null");
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft);

		Assert.assertNotNull(list);
		Assert.assertEquals(0, list.size());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}

	@Test
	public void insertsDeletesChanges() throws IOException {
		File f1 = new File("test1");
		File f2 = new File("test2");

		IOUtils.writeBufferToFile(f1, new StringBuffer("SK|ID|FLAG\nINTEGER|INTEGER|INTEGER\n1|a|Y\n2|b|N\n4|b|Y\n6|b|N\n7|a|Y"));
		IOUtils.writeBufferToFile(f2, new StringBuffer("SK|ID|FLAG\nINTEGER|INTEGER|INTEGER\n1|b|N\n2|b|N\n3|b|Y\n5|b|N\n7|a|N"));

		FlatFile fs = new FlatFile(f1, "\\|", "\n", "null");
		fs.setKeyColumns(Arrays.asList("SK"));
		FlatFile ft = new FlatFile(f2, "\\|", "\n", "null");
		ft.setKeyColumns(Arrays.asList("SK"));

		FlatFileDiff ffd = new FlatFileDiff(fs);
		List<FileDiff> list = ffd.diffFile(ft);

		Assert.assertNotNull(list);
		Assert.assertEquals(7, list.size());

		// we should see two diffs for the first row,
		// a remove for source id 3, an insert for target id 4,
		// a remove for source id 5, an insert for target id 6,
		// and a change for id 7

		Assert.assertEquals("1|", list.get(0).getOrderKey());
		Assert.assertEquals("ID", list.get(0).getColumnName());
		Assert.assertEquals("a", list.get(0).getSourceValue());
		Assert.assertEquals("b", list.get(0).getOtherValue());
		Assert.assertEquals(1, list.get(0).getSourceRowNumber());
		Assert.assertEquals(1, list.get(0).getTargetRowNumber());

		Assert.assertEquals("1|", list.get(1).getOrderKey());
		Assert.assertEquals("FLAG", list.get(1).getColumnName());
		Assert.assertEquals("Y", list.get(1).getSourceValue());
		Assert.assertEquals("N", list.get(1).getOtherValue());
		Assert.assertEquals(1, list.get(1).getSourceRowNumber());
		Assert.assertEquals(1, list.get(1).getTargetRowNumber());

		Assert.assertEquals("3|", list.get(2).getOrderKey());
		Assert.assertNull(list.get(2).getSourceValue());
		Assert.assertEquals(-1, list.get(2).getSourceRowNumber());
		Assert.assertEquals(3, list.get(2).getTargetRowNumber());

		Assert.assertEquals("4|", list.get(3).getOrderKey());
		Assert.assertNull(list.get(3).getOtherValue());
		Assert.assertEquals(3, list.get(3).getSourceRowNumber());
		Assert.assertEquals(-1, list.get(3).getTargetRowNumber());

		Assert.assertEquals("5|", list.get(4).getOrderKey());
		Assert.assertNull(list.get(4).getSourceValue());
		Assert.assertEquals(-1, list.get(4).getSourceRowNumber());
		Assert.assertEquals(4, list.get(4).getTargetRowNumber());

		Assert.assertEquals("6|", list.get(5).getOrderKey());
		Assert.assertNull(list.get(5).getOtherValue());
		Assert.assertEquals(4, list.get(5).getSourceRowNumber());
		Assert.assertEquals(-1, list.get(5).getTargetRowNumber());

		Assert.assertEquals("7|", list.get(6).getOrderKey());
		Assert.assertEquals("FLAG", list.get(6).getColumnName());
		Assert.assertEquals("Y", list.get(6).getSourceValue());
		Assert.assertEquals("N", list.get(6).getOtherValue());
		Assert.assertEquals(5, list.get(6).getSourceRowNumber());
		Assert.assertEquals(5, list.get(6).getTargetRowNumber());

		Assert.assertTrue(f1.delete());
		Assert.assertTrue(f2.delete());
	}
	*/
	@Test
	public void yuck()
	{
	}
}
