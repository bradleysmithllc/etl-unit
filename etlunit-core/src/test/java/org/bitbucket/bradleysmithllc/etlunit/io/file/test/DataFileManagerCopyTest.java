package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

@RunWith(Parameterized.class)
public class DataFileManagerCopyTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	@Parameterized.Parameters
	public static Collection<Object[]> data() throws Exception
	{
		Map<String, TestSpec> testSpecMap = new HashMap<String, TestSpec>();
		List<Object[]> testSpecArray = new ArrayList<Object[]>();

		URL url = DataFileManagerCopyTest.class.getResource("/fileCopy/TestSpecs.json");

		Assert.assertNotNull(url);

		JsonNode jsonTests = JsonUtils.loadJson(IOUtils.readURLToString(url));

		ObjectNode anode = (ObjectNode) jsonTests;

		Iterator<String> it = anode.fieldNames();

		while (it.hasNext())
		{
			TestSpec tSpec = new TestSpec();
			tSpec.id = it.next();

			final JsonNode testNode = anode.get(tSpec.id);

			Assert.assertNotNull(testNode);
			Assert.assertTrue(testNode.isObject());

			final JsonNode executeNode = testNode.get("execute");

			if (executeNode != null)
			{
				Assert.assertTrue(executeNode.isBoolean());
				tSpec.execute = executeNode.asBoolean();
			}

			Assert.assertFalse("Test Spec not unique: " + tSpec.id, testSpecMap.containsKey(tSpec.id));

			testSpecMap.put(tSpec.id, tSpec);
			testSpecArray.add(new TestSpec[]{tSpec});
		}

		return testSpecArray;
	}

	private final TestSpec testSpec;

	@Before
	public void start() throws IOException {
		File tmpRoot = temporaryFolder.newFolder();
		dfm = new DataFileManagerImpl(tmpRoot);
		System.out.println("Using db root: " + tmpRoot.getAbsolutePath());
	}

	public DataFileManagerCopyTest(TestSpec spec) {
		testSpec = spec;
	}

	@Test
	public void runDiffTests() throws Exception
	{
		if (testSpec.execute)
		{
			String name = "/fileCopy/" + testSpec.id + "_src.fml";
			URL source = getClass().getResource(name);
			Assert.assertNotNull("Src Schema not found: " + name, source);
			DataFileSchema srcSchema = dfm.loadDataFileSchema(new File(source.toURI()), testSpec.id);

			name = "/fileCopy/" + testSpec.id + "_src.delimited";
			source = getClass().getResource(name);
			Assert.assertNotNull("Src data set not found: " + name, source);

			File res = temporaryFolder.newFile();
			FileUtils.write(res, IOUtils.readURLToString(source));
			testSpec.srcData = dfm.loadDataFile(res, srcSchema);

			name = "/fileCopy/" + testSpec.id + "_dest.fml";
			source = getClass().getResource(name);
			Assert.assertNotNull("Dest Schema not found: " + name, source);
			DataFileSchema destSchema = dfm.loadDataFileSchema(new File(source.toURI()), testSpec.id);

			res = temporaryFolder.newFile();

			testSpec.destData = dfm.loadDataFile(res, destSchema);

			name = "/fileCopy/" + testSpec.id + "_res.delimited";
			source = getClass().getResource(name);
			Assert.assertNotNull("Results data file not found: " + name, source);

			res = temporaryFolder.newFile();

			FileUtils.write(res, IOUtils.readURLToString(source));
			testSpec.results = dfm.loadDataFile(res, destSchema);

			name = "/fileCopy/" + testSpec.id + "_res_default.delimited";
			source = getClass().getResource(name);

			Assert.assertNotNull("Results data file with defaults not found: " + name, source);

			res = temporaryFolder.newFile();

			FileUtils.write(res, IOUtils.readURLToString(source));
			testSpec.defResults = dfm.loadDataFile(res, destSchema);

			name = "/fileCopy/" + testSpec.id + "_extract.sql";
			source = getClass().getResource(name);
			if (source != null)
			{
				testSpec.extractSql = IOUtils.readURLToString(source);
			}

			runTest(testSpec);
		}
	}

	private void runTest(final TestSpec tSpec) throws Exception {
		// run twice - once using the null missing column policy and once using defaults
		dfm.copyDataFile(tSpec.srcData, tSpec.destData, new CopyOptionsBuilder().overrideExtractionSql(tSpec.extractSql).missingColumnPolicy(DataFileManager.CopyOptions.missing_column_policy.use_null).options());

		File actFile = tSpec.destData.getFile();
		File expFile = tSpec.results.getFile();

		Assert.assertEquals(testSpec.id + " - with nulls", FileUtils.readFileToString(expFile), FileUtils.readFileToString(actFile));

		dfm.copyDataFile(tSpec.srcData, tSpec.destData, new CopyOptionsBuilder().overrideExtractionSql(tSpec.extractSql).missingColumnPolicy(DataFileManager.CopyOptions.missing_column_policy.use_default).options());

		actFile = tSpec.destData.getFile();
		expFile = tSpec.defResults.getFile();

		Assert.assertEquals(testSpec.id + " - with defaults", FileUtils.readFileToString(expFile), FileUtils.readFileToString(actFile));
	}

	private static final class TestSpec
	{
		String id;
		boolean execute = false;

		DataFile srcData;
		DataFile destData;

		String extractSql;

		DataFile results;
		DataFile defResults;
	}
}
