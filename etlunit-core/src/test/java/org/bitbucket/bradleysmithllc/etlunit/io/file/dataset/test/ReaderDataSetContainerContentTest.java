package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileReader;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.DataSet;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetContainer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

public class ReaderDataSetContainerContentTest
{
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dataFileManager;

	@Before
	public void setup() throws IOException {
		dataFileManager = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	@Test
	public void emptyHasNext() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/columnList.dataset"
		);

		while (rdsc.hasNext())
		{
			IOUtils.copy(rdsc.next().read(), (OutputStream) System.out);

			DataFileReader ds = rdsc.next().open();

			Iterator<DataFileReader.FileRow> it = ds.iterator();

			while (it.hasNext())
			{
				System.out.println(it.next().getData());
			}
		}
	}

	@Test
	public void locate() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/locate.dataset"
		);

		DataSet ds = rdsc.locate("a");
		Assert.assertEquals("boo_a", ds.getProperties().get("$schema").asText());
		rdsc.close();

		rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/locate.dataset"
		);

		ds = rdsc.locate("b");
		Assert.assertEquals("boo_b", ds.getProperties().get("$schema").asText());
		rdsc.close();

		rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/locate.dataset"
		);

		ds = rdsc.locate("c");
		Assert.assertEquals("boo_c", ds.getProperties().get("$schema").asText());
		rdsc.close();
	}
}
