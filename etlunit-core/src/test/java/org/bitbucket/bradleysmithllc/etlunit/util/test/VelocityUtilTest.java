package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class VelocityUtilTest
{
	@Test
	public void testContext() throws Exception
	{
		Map map = new HashMap();

		map.put("variable", "value");

		String str = VelocityUtil.writeTemplate("${variable}", map);

		Assert.assertEquals("value", str);
	}

	@Test
	public void parseFiles() throws Exception
	{
		Map map = new HashMap();

		File root = new File(".");

		File base = new File(root, "base.vm");
		IOUtils.writeBufferToFile(base, new StringBuffer("value"));

		File base_i = new File(root, "base_i.vm");
		IOUtils.writeBufferToFile(base_i, new StringBuffer("$$$$$"));

		String str = VelocityUtil.writeTemplate("#parse(\"base.vm\")#include(\"base_i.vm\")", map, root);

		Assert.assertEquals("value$$$$$", str);

		Assert.assertTrue(base.delete());
		Assert.assertTrue(base_i.delete());
	}

	@Test
	public void staticHelperObjects() throws Exception {
		HashMap bean = new HashMap();
		bean.put("defaultValue", 77787);
		System.out.println(VelocityUtil.writeTemplateWithStaticSupport("$math.add(1, 2)", bean));
		System.out.println(VelocityUtil.writeTemplateWithStaticSupport("$math.multiply(1, 2)", bean));
		System.out.println(VelocityUtil.writeTemplateWithStaticSupport("$numbers.leftPad($math.modulo(1, 2), 5)", bean));
		System.out.println(VelocityUtil.writeTemplateWithStaticSupport("$strings.constrain(\"fff\", 5)", bean));
		System.out.println(VelocityUtil.writeTemplateWithStaticSupport("$strings.constrain(\"ffffffff\", 5)", bean));
		System.out.println(VelocityUtil.writeTemplateWithStaticSupport("$strings.constrain(\"IDIDID[${defaultValue}]\", 7)", bean));
	}
}
