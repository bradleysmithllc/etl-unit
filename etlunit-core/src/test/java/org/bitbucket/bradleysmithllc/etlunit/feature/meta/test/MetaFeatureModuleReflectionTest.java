package org.bitbucket.bradleysmithllc.etlunit.feature.meta.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class MetaFeatureModuleReflectionTest extends BaseFeatureModuleTest
{
	private MetaFeatureModule metaFeatureModule;

	@Override
	protected List<Feature> getTestFeatures()
	{
		metaFeatureModule = new MetaFeatureModule();
		return Arrays.asList((Feature) metaFeatureModule);
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSource("test.etlunit", "class test {@Test test(){meta(do-stuff: \"Once\");}}");
	}

	@Override
	protected JSonBuilderProxy getConfigurationOverrides()
	{
		return new JSonBuilderProxy()
				.object()
				.key("features")
				.object()
				.key("meta")
				.object()
				.key("useful-module").value("Rats")
				.endObject()
				.endObject()
				.endObject();
	}

	@Test
	public void testConfigurationInjection()
	{
		TestResults results = startTest();

		Assert.assertEquals(1, results.getNumTestsSelected());
		Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());

		Assert.assertNotNull(metaFeatureModule.getMetaFeatureModuleConfiguration());
		Assert.assertNotNull(metaFeatureModule.getMetaFeatureModuleConfiguration().getUsefulModule());
		Assert.assertEquals("Rats", metaFeatureModule.getMetaFeatureModuleConfiguration().getUsefulModule());
	}
}
