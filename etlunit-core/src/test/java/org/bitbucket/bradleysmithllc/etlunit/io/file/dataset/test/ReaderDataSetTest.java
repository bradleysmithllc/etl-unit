package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSet;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetContainer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.EOFException;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;

public class ReaderDataSetTest
{
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	private DataFileManager dataFileManager;

	@Before
	public void setup() throws IOException {
		dataFileManager = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	@Test
	public void emptyProperties() throws IOException {
		expectedEx.expect(EOFException.class);
		expectedEx.expectMessage("Stream ended before a token was reached");

		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(""))).getProperties();
	}

	@Test
	public void garbageProperties() throws IOException {
		expectedEx.expect(EOFException.class);
		expectedEx.expectMessage("Stream ended before a token was reached");

		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader("awefwf aefvwer gwefwaf v"))).getProperties();
	}

	@Test
	public void emptyJsonProperties() throws IOException {
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader("{}" + ReaderDataSet.token))).getProperties();
	}

	//@Test
	public void tokenInJsonValue() throws IOException {
		String s = "{ id: \"" + ReaderDataSet.token + "\"}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).getProperties();
	}

	@Test
	public void relaxedFormat() throws IOException {
		String s = "{ id: 'value', id2: \"value\", 'id3': 'value', \"id3\": 'value'}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).getProperties();
	}

	//@Test
	public void tokenInJsonKey() throws IOException {
		String s = "{ \"" + ReaderDataSet.token + "\": \"value\"}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).getProperties();
	}

	//@Test
	public void tokenInJsonBoth() throws IOException {
		String s = "{ \"" + ReaderDataSet.token + "\": \"" + ReaderDataSet.token + "\"}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).getProperties();
	}

	@Test
	public void nullSchema() throws IOException {
		expectedEx.expect(IllegalStateException.class);
		expectedEx.expectMessage("data-file-schema property not present and no schema provided");

		String s = "{}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void schemaNullObject() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-schema property is null");

		String s = "{\"data-file-schema\": null}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void schemaNotTextInteger() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-schema property is not a string type");

		String s = "{\"data-file-schema\": 1}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void schemaNotTextBoolean() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-schema property is not a string type");

		String s = "{\"data-file-schema\": true}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void schemaNotTextObject() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-schema property is not a string type");

		String s = "{\"data-file-schema\": {}}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void schemaNotTextArray() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-schema property is not a string type");

		String s = "{\"data-file-schema\": []}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsNotArrayText() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-columns is not an array type");

		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": \"\"}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsNotArrayNumber() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-columns is not an array type");

		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": 1}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsNotArrayBoolean() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-columns is not an array type");

		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": true}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsNotArrayObject() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-columns is not an array type");

		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": {}}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsElementNotTextObject() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-columns array element[0] is not a text type: '{}'");

		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": [{}]}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsElementNotTextBoolean() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-columns array element[0] is not a text type: 'true'");

		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": [true]}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsElementNotTextNumber() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-columns array element[0] is not a text type: '1'");

		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": [1]}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsElementNotTextArray() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("data-file-columns array element[0] is not a text type: '[]'");

		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": [[]]}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void columnsNotArrayNull() throws IOException {
		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\", \"data-file-columns\": null}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}

	@Test
	public void openWithImpliedSchema() throws IOException {
		String s = "{\"data-file-schema\": \"ffml/delimited-test.ffml\"}" + ReaderDataSet.token;
		new ReaderDataSet(dataFileManager, new PushbackReader(new StringReader(s))).open();
	}
}
