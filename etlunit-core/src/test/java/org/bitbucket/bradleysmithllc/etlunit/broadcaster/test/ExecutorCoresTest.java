package org.bitbucket.bradleysmithllc.etlunit.broadcaster.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ExecutorCoresTest extends BaseFeatureModuleTest
{
	@Test
	public void cores() throws IOException
	{
		startTest();
	}

	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
				.object()
					.key("options")
						.object()
							.key("etlunit")
								.object()
									.key("executorsPerCore")
										.object()
											.key("integer-value")
											.value(5)
										.endObject()
								.endObject()
						.endObject()
				.endObject();
	}

	@Override
	protected void assertions(TestResults results, int pass) {
		if (pass == 1)
		{
			Assert.assertEquals(Runtime.getRuntime().availableProcessors() * 5, runtimeSupport.getExecutorCount());
		}
	}

	@Override
	protected boolean multiPassSafe() {
		return false;
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
	}
}
