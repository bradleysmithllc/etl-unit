package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.SchemaColumn;
import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.NumericConverter;
import org.bitbucket.bradleysmithllc.etlunit.util.Numbers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

public class NumbersTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void makeIntegralDigits0() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Too few digits");
		Assert.assertEquals(0, Numbers.makeIntegralDigits(0, 0));
	}

	@Test
	public void makeIntegralDigitsAt0() {
		Assert.assertEquals("0", Numbers.makeIntegralDigits(0, 100));
		Assert.assertEquals("0", Numbers.makeIntegralDigits(0, 1));
	}

	@Test
	public void makeIntegralDigits() {
		for (int i = 0; i <= 400; i++) {
			System.out.print(i);
			System.out.print(" ");
			System.out.println(Numbers.makeIntegralDigits(i, 9));
		}

		//Assert.assertEquals("217", Numbers.makeIntegralDigits(62, 3));
	}

	@Test
	public void makeDecimalDigits() {
		for (int i = 0; i <= 400; i++) {
			System.out.print(i);
			System.out.print(" ");
			System.out.println(Numbers.makeDecimalDigits(i, 9));
		}

		//Assert.assertEquals("217", Numbers.makeIntegralDigits(62, 3));
	}

	@Test
	public void makeDecimal() {
		for (int i = 0; i <= 400; i++) {
			System.out.print(i);
			System.out.print(" ");
			System.out.print(Numbers.makeIntegralDigits(i, 9));
			System.out.print(".");
			System.out.println(Numbers.makeDecimalDigits(i + 1, 9));
		}

		//Assert.assertEquals("217", Numbers.makeIntegralDigits(62, 3));
	}

	@Test
	public void makeDecimalDigits0() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Too few digits");
		Assert.assertEquals(0, Numbers.makeDecimalDigits(0, 0));
	}

	@Test
	public void makeDecimalDigitsAt0() {
		Assert.assertEquals("0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", Numbers.makeDecimalDigits(0, 100));
		Assert.assertEquals("0", Numbers.makeDecimalDigits(0, 1));
	}

	@Test
	public void format() {
		Assert.assertEquals("0.0000000002", new NumericConverter().format(new BigDecimal("0.0000000002"), new SchemaColumn(null, null, 5, 2, null)));
		Assert.assertEquals("20000000000", new NumericConverter().format(new BigDecimal("20000000000"), new SchemaColumn(null, null, 5, 2, null)));
		Assert.assertEquals("2000000000", new NumericConverter().format(new BigDecimal("2E+9"), new SchemaColumn(null, null, 5, 2, null)));
	}
}
