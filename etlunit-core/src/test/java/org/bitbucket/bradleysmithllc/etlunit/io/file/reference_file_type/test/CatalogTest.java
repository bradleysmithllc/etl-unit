package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CatalogTest
{

	private ReferenceFileTypeManager rftc;

	@Before
	public void loadCatalogs()
	{
		rftc = new ReferenceFileTypeManagerImpl();
	}

	@Test
	public void testSchemas() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("edw/dbo/store~cl1+2v", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("edw.dbo.store")));
		Assert.assertEquals("pos-alx-mart/dbo/product~cl1+2v", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("edw.dbo.product")));
	}

	@Test
	public void catalogDescription() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("g", rftc.locateReferenceTypeCatalogs().get(0).description());
	}

	@Test
	public void packageDescription() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("edw tables", rftc.locateReferenceTypeCatalogs().get(0).getReferenceFileTypePackages().get("edw.dbo").getDescription());
	}

	@Test
	public void typeDescription() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("edw store table", rftc.locateReferenceFileType("edw.dbo.store").getDescription());
	}

	/* scenarios:
		A - request with defaults:
			1 - default version,
			2 - default classifier
			3 - no version and no default
		B	request with version
		C	request with classifier
			1 - no version and no default
			2 - use default version
			3 - use specific version
	*/
	@Test
	public void A1() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("edw/dbo/a1+4v", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("edw.dbo.A1")));
	}
	@Test
	public void A2() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("edw/dbo/a2~defcl", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("edw.dbo.A2")));
	}
	@Test
	public void A3() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("edw/dbo/a3", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("edw.dbo.A3")));
	}
	@Test
	public void B() throws RequestedFileTypeNotFoundException {
		ReferenceFileTypeRef v1 = ReferenceFileTypeRef.refWithIdAndVersion("pos-alx-mart.dbo.B", "V1");
		String actual = rftc.generateResourcePathForRef(v1);
		Assert.assertEquals("pos-alx-mart/dbo/b+v1", actual);
	}
	@Test
	public void C1() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("pos-alx-mart/agg/c1~cls1", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdAndClassifier("pos-alx-mart.agg.C1", "CLS1")));
	}
	@Test
	public void C2() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("pos-alx-mart/agg/c2~cls2+defvers", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdAndClassifier("pos-alx-mart.agg.C2", "CLS2")));
	}
	@Test
	public void C3() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("pos-alx-mart/agg/c3~cls3+vers3", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdClassifierAndVersion("pos-alx-mart.agg.C3", "CLS3", "VERS3")));
	}

	@Test(expected = RequestedFileTypeNotFoundException.class)
	public void requestBadVersion() throws RequestedFileTypeNotFoundException {
		rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdAndVersion("pos-alx-mart.agg.C3", "VERS4"));
	}

	@Test(expected = RequestedFileTypeNotFoundException.class)
	public void requestBadClassifier() throws RequestedFileTypeNotFoundException {
		rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdAndClassifier("pos-alx-mart.agg.C3", "BADCLASS"));
	}

	@Test
	public void caseInsensitiveLookups() throws RequestedFileTypeNotFoundException {
		Assert.assertEquals("pos-alx-mart/agg/c1~cls1", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdAndClassifier("pos-aLx-mart.Agg.c1", "ClS1")));
		Assert.assertEquals("pos-alx-mart/agg/c2~cls2+defvers", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdAndClassifier("POS-alx-marT.agg.C2", "CLs2")));
		Assert.assertEquals("pos-alx-mart/agg/c3~cls3+vers3", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdClassifierAndVersion("pos-alx-mARt.aGg.C3", "Cls3", "VerS3")));

		Assert.assertEquals("pos-alx-mart/dbo/b+v1", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithIdAndVersion("pos-alx-mArt.dBo.B", "v1")));

		Assert.assertEquals("edw/dbo/a1+4v", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("edw.dbo.a1")));
		Assert.assertEquals("edw/dbo/a2~defcl", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("EdW.dBo.a2")));
		Assert.assertEquals("edw/dbo/a3", rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("edW.DBo.a3")));
	}

	@Test(expected = IllegalArgumentException.class)
	public void defaultVersionNoVersions() throws RequestedFileTypeNotFoundException {
		rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("bad-types.defaultVersionNoVersions"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void defaultVersionUndeclaredVersion() throws RequestedFileTypeNotFoundException {
		rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("bad-types.defaultVersionUndeclaredVersion"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void defaultClassifierNoClassifiers() throws RequestedFileTypeNotFoundException {
		rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("bad-types.defaultClassifierNoClassifiers"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyVersions() throws RequestedFileTypeNotFoundException {
		rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("bad-types.emptyVersions"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void jsonFileDoesNotExist() throws RequestedFileTypeNotFoundException {
		rftc.generateResourcePathForRef(ReferenceFileTypeRef.refWithId("bad-types.jsonFileDoesNotExist"));
	}
}
