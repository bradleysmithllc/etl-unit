package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.io.Expectorator;
import org.bitbucket.bradleysmithllc.etlunit.io.JavaForker;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class ComplexExpectoratorProcessTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void expectoratorEchoPlusPlus() throws IOException, InterruptedException
	{
		BasicRuntimeSupport brs = new BasicRuntimeSupport();

		brs.setApplicationLogger(new PrintWriterLog());

		JavaForker forker = new JavaForker(brs);
		forker.setMainClass(EchoPlusPlus.class);

		File output = temporaryFolder.newFile("expectoratorEchoPlusPlus.process");

		forker.setOutput(output);
		ProcessFacade pr = forker.startProcess();

		pr.waitForStreams();

		Expectorator exp = new Expectorator(pr.getReader(), pr.getWriter());

		for (int i = 0; i < 20; i++)
		{
			exp.sayAndExpect("|5|Hello|", "(\\>Hello\\<){5}", 10000);
		}

		exp.sayAndExpect("|17|What Are You Doing?|", "(\\>What Are You Doing\\?\\<){17}", 10000);

		for (int i = 1; i <= 19; i+= 2)
		{
			exp.sayAndExpect("|" + i + "|Hey " + i  + " Buds|", "(\\>Hey " + i + " Buds\\<){" + i + "}", 100000);
		}

		// build a really big response
		StringBuilder stb = new StringBuilder();

		for (int i = 0; i < 1000; i++)
		{
			stb.append(System.currentTimeMillis());
		}

		String clob = stb.toString() + stb.toString() + stb.toString() + stb.toString() + stb.toString() + stb.toString();

		for (int i = 0; i < 1; i++)
		{
			exp.sayAndExpect("|5|" + clob + "|", "(\\>" + clob + "\\<){5}", 30000);
		}
	}
}
