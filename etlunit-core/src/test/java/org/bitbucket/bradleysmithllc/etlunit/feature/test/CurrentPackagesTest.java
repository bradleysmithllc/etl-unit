package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class CurrentPackagesTest extends BaseFeatureModuleTest
{
	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSourceFromClasspath(null, "cp.etlunit");
		createSourceFromClasspath("pack", "cp.etlunit");
		createSourceFromClasspath("pack.subpack", "cp.etlunit");
		createSourceFromClasspath("pack2", "cp.etlunit");
	}

	@Test
	public void run()
	{
		TestResults mets = startTest();

		Assert.assertEquals(4, mets.getMetrics().getNumberOfTestsPassed());
	}

	@Override
	protected void assertVariableContextPre(ETLTestClass cl, VariableContext context)
	{
		Assert.assertEquals(cl.getPackage(), runtimeSupport.getCurrentlyProcessingTestPackage());
	}
}
