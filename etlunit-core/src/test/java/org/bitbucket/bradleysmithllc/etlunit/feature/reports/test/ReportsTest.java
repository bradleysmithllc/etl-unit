package org.bitbucket.bradleysmithllc.etlunit.feature.reports.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ReportsTest extends BaseFeatureModuleTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void testReports() throws IOException
	{
		TestResults results = startTest();
	}

	@Override
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList((Feature) new FailureFeature(temporaryFolder));
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSource("test.bitbucket",
				"class test { @Test dummy() {radda(with: 'lots of dada'); ridda(); fail(message: 'nipe', failure-id: 'WHAT_A_FAIL'); } @Test dummy2() {radda(with: 'lots of dada'); ridda(); pidda(); fail(message: 'nope'); } }");
	}
}

class FailureFeature extends AbstractFeature
{
	private DiffManager diffManager;

	private LogFileManager logFileManager;
	private final TemporaryFolder temporaryFolder;

	FailureFeature(TemporaryFolder temporaryFolder)
	{
		this.temporaryFolder = temporaryFolder;
	}

	@Inject
	public void receiveLogFileManager(LogFileManager logFileManager)
	{
		this.logFileManager = logFileManager;
	}

	@Inject
	public void receiveDiffManager(DiffManager reporter)
	{
		diffManager = reporter;
	}

	@Override
	public String getFeatureName()
	{
		return "I-failure";
	}

	@Override
	public ClassListener getListener()
	{
		return new NullClassListener()
		{
			@Override
			public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				DiffGrid diffReport = diffManager.reportDiff(op.getTestMethod(), op, "TEST_FID", "exp", "act");

				DiffGridRow dRow = diffReport.addRow(0, 0, DiffGrid.line_type.changed);

				dRow.setColumnName("COLUMN_CHANGED");
				dRow.setOrderKey("KEY1");
				dRow.setSourceValue("SOURCE");
				dRow.setTargetValue("TARGET");

				dRow.done();

				dRow = diffReport.addRow(-1, 1, DiffGrid.line_type.added);

				dRow.setOrderKey("KEY2");
				dRow.setColumnName("NOSHOW");
				dRow.setSourceValue("NOSHOW");
				dRow.setTargetValue("TARGET_ADDED");

				dRow.done();

				dRow = diffReport.addRow(2, -1, DiffGrid.line_type.removed);

				dRow.setOrderKey("KEY3");
				dRow.setColumnName("NOSHOW");
				dRow.setSourceValue("SOURCE_REMOVED");
				dRow.setTargetValue("NOSHOW");

				dRow.done();

				dRow = diffReport.addRow(3, 7, DiffGrid.line_type.changed);

				dRow.setOrderKey("KEY3");
				dRow.setColumnName("DIFF");
				dRow.setSourceValue("\r\n\t\\\f\b\0");
				dRow.setTargetValue("Target");

				dRow.done();

				diffReport.done();

				try
				{
					File logFile = temporaryFolder.newFile("log.txt");

					IOUtils.writeStringToFile(logFile, "Messages");
					logFileManager.addLogFile(op, logFile, "log");
				}
				catch (IOException e)
				{
					throw new RuntimeException(e);
				}

				return action_code.handled;
			}
		};
	}
}
