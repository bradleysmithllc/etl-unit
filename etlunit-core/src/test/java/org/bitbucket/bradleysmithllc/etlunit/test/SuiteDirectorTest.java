package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassBroadcasterImpl;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SuiteDirectorTest
{
	private ClassLocator getTestClasses() throws ParseException {
		List<ETLTestClass>
				list =
				ETLTestParser.load(
					"@JoinSuite(name: 'agg') @JoinSuite(name: 'stg') " +
							"class class_a {@Test count1(){}@Test count2(){}@Test count3(){}} " +
							"@JoinSuite(name: 'stg') class class_b {@Test count1(){}@Test count2(){}@Test count3(){}@Test count4(){}@Test count5(){}} " +
							"@JoinSuite(name: 'dbo') class class_c {@Test count1(){}@Test count2(){}@Test count3(){}@Test count4(){}@Test count5(){}@Test count6(){}@Test count7(){}} " +
							"class class_d {@Test count(){}} @JoinSuite(names: ['agg', 'b', 'c']) class class_e{@Test count(){}}");

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		return impl;
	}

	@Test
	public void emptySuiteAnyList() throws ParseException
	{
		dboSuiteAllList(0, "suite()");
	}

	@Test
	public void aggSuiteAnyList() throws ParseException
	{
		dboSuiteAllList(4, "suite(Agg)");
	}

	@Test
	public void aggStgSuiteAllList() throws ParseException
	{
		dboSuiteAllList(3, "suite(AGG,stg)");
	}

	@Test
	public void aggStgSuiteAnyList() throws ParseException
	{
		dboSuiteAllList(9, "suite(AGG?,stg?)");
	}

	@Test
	public void stgSuiteAnyList() throws ParseException
	{
		dboSuiteAllList(8, "suite(stg)");
	}

	@Test
	public void dboSuiteAnyList() throws ParseException
	{
		dboSuiteAllList(7, "suite(dbo)");
	}

	@Test
	public void dboSuiteAllList() throws ParseException
	{
		dboSuiteAllList(7, "suite(dbo)");
	}

	@Test
	public void names() throws ParseException
	{
		dboSuiteAllList(1, "suite(agg,b,c)");
	}

	private void dboSuiteAllList(int result, String suite) throws ParseException
	{
		SuiteClassDirectorImpl pdirector = new SuiteClassDirectorImpl(suite);

		ClassBroadcasterImpl climpl = new ClassBroadcasterImpl(
				getTestClasses(),
				pdirector,
				new NullClassListener(),
				new MapLocal(),
				new PrintWriterLog()
		);

		// must be class_a only
		ETLTestCases cases = climpl.preScan(null);
		climpl.broadcast(null, cases);
		Assert.assertEquals(result, cases.getTestCount());
	}
}
