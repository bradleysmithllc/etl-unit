package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonSchemaResolver;
import org.junit.Assert;
import org.junit.Test;

import java.net.MalformedURLException;

public class JsonSchemaResolverTest
{
	@Test
	public void testEmpty() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference(JsonLoader.fromString("{}"));

		Assert.assertEquals(JsonLoader.fromString("{}"), node);
	}

	@Test
	public void testSimple() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference(JsonLoader.fromString("{\"a\": \"b\"}"));

		Assert.assertEquals(JsonLoader.fromString("{\"a\":\"b\"}"), node);
	}

	@Test(expected = MalformedURLException.class)
	public void testBadResourceType() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badRefType.json");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadResource() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badRef.json");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadRefNotText() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badRefNotText.json");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadResourceNotClasspathHost() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badRefNotClasspathHost.json");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadPatchNotText() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badPatchNotText.json");
	}

	@Test(expected = IllegalStateException.class)
	public void testBadPatchAndRef() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badPatchAndRef.json");
	}

	@Test(expected = IllegalStateException.class)
	public void testBadWithAndRef() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badWithAndRef.json");
	}

	@Test(expected = IllegalStateException.class)
	public void testBadWithNoPatch() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badWithNoPatch.json");
	}

	@Test(expected = IllegalStateException.class)
	public void testBadPatchNoWith() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badPatchNoWith.json");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testBadPatchWithNotObject() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/badPatchWithNotObject.json");
	}

	@Test
	public void testEmptyWithResource() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/empty.json");

		Assert.assertEquals(JsonLoader.fromString("{}"), node);
	}

	@Test
	public void testSimpleWithResource() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/simple.json");

		Assert.assertEquals(JsonLoader.fromString("{\"a\":\"b\"}"), node);
	}

	@Test
	public void testRef() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/ref.json");

		//load the referred node directly and compare
		JsonNode referred = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/referred.json");

		Assert.assertEquals(referred, node);
	}

	@Test
	public void testDeepRef() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/deepRef.json");

		//load the referred node directly and compare
		JsonNode referred = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/deepReferredFlattened.json");

		Assert.assertEquals(referred, node);
	}

	@Test
	public void testPatch() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/patch.json");

		//load the referred node directly and compare
		JsonNode referred = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/patchFlattened.json");

		Assert.assertEquals(referred, node);
	}

	@Test
	public void testRecursivePatch() throws Exception {
		JsonNode node = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/recursivePatch.json");

		//load the referred node directly and compare
		JsonNode referred = JsonSchemaResolver.resolveSchemaReference("schemaResolverTest/recursivePatchFlattened.json");

		Assert.assertEquals(referred, node);
	}
}
