package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchemaView;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

@RunWith(Parameterized.class)
public class DataFileSchemaViewSqlTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	@Parameterized.Parameters
	public static Collection<Object[]> data() throws Exception
	{
		Map<String, TestSpec> testSpecMap = new HashMap<String, TestSpec>();
		List<Object[]> testSpecArray = new ArrayList<Object[]>();

		URL url = DataFileSchemaViewSqlTest.class.getResource("/fileViewSql/TestSpecs.json");

		Assert.assertNotNull(url);

		JsonNode jsonTests = JsonUtils.loadJson(IOUtils.readURLToString(url));

		ObjectNode anode = (ObjectNode) jsonTests;

		Iterator<String> it = anode.fieldNames();

		while (it.hasNext())
		{
			TestSpec tSpec = new TestSpec();
			tSpec.id = it.next();

			final JsonNode testNode = anode.get(tSpec.id);

			Assert.assertNotNull(testNode);
			Assert.assertTrue(testNode.isObject());

			final JsonNode executeNode = testNode.get("execute");

			if (executeNode != null)
			{
				Assert.assertTrue(executeNode.isBoolean());
				tSpec.execute = executeNode.asBoolean();
			}

			Assert.assertFalse("Test Spec not unique: " + tSpec.id, testSpecMap.containsKey(tSpec.id));

			testSpecMap.put(tSpec.id, tSpec);
			testSpecArray.add(new TestSpec[]{tSpec});

			// load the actions
			JsonNode actionList = testNode.get("actions");

			Assert.assertNotNull(actionList);
			Assert.assertTrue(actionList.isArray());

			ArrayNode actionList1node = (ArrayNode) actionList;

			Iterator<JsonNode> elem = actionList1node.elements();

			while (elem.hasNext())
			{
				JsonNode acNode = elem.next();

				Assert.assertTrue(acNode.isObject());

				ObjectNode oactNode = (ObjectNode) acNode;

				JsonNode resNode = oactNode.get("results-id");

				TestAction testAc = new TestAction();
				testAc.id = resNode.asText();

				tSpec.actions.add(testAc);

				JsonNode incNode = oactNode.get("include");

				if (incNode != null)
				{
					Assert.assertTrue(incNode.isArray());

					testAc._type = TestAction.type.include;
				}
				else
				{
					incNode = oactNode.get("exclude");

					if (incNode == null)
					{
						Assert.fail();
					}

					Assert.assertTrue(incNode.isArray());

					testAc._type = TestAction.type.exclude;
				}

				Iterator<JsonNode> colElem = incNode.elements();

				while (colElem.hasNext())
				{
					JsonNode col = colElem.next();

					testAc.columns.add(col.asText());
				}
			}
		}

		return testSpecArray;
	}

	private final TestSpec testSpec;

	@Before
	public void start() throws IOException {
		File tmpRoot = temporaryFolder.newFolder();
		dfm = new DataFileManagerImpl(tmpRoot);
		System.out.println("Using db root: " + tmpRoot.getAbsolutePath());
	}

	public DataFileSchemaViewSqlTest(TestSpec spec) {
		testSpec = spec;
	}

	@Test
	public void runDiffTests() throws Exception
	{
		if (testSpec.execute)
		{
			runTest(testSpec);
		}
	}

	private void runTest(TestSpec tSpec) throws IOException, URISyntaxException {
		if (tSpec.execute)
		{
			// locate the source and target files
			String name = "/fileViewSql/" + tSpec.id + ".fml";
			URL source = getClass().getResource(name);

			Assert.assertNotNull("Schema not found: " + name, source);

			DataFileSchema schemaSource = dfm.loadDataFileSchema(new File(source.toURI()), tSpec.id);

			DataFileSchemaView schemaView = null;

			for (TestAction action : tSpec.actions)
			{
				// for each action, modify the view as described and evaluate the sql
				switch (action._type) {
					case include:
						schemaView = (schemaView == null ? schemaSource : schemaView).createSubViewIncludingColumns(action.columns, action.id);
						break;
					case exclude:
						schemaView = (schemaView == null ? schemaSource : schemaView).createSubViewExcludingColumns(action.columns, action.id);
						break;
				}
				// we now need to evaluate the three sql's for the view and for the materialized form
				String createSql = getSql("/fileViewSql/create_" + tSpec.id + "_" + action.id + ".sql");

				String actSql = schemaView.createTemporaryDbTableSql(tSpec.id + "_" + action.id);

				Assert.assertEquals(tSpec.id + "-create-" + action.id, createSql.replace("\r\n", "\n").replace("\r", ""), actSql);

				String insertSql = getSql("/fileViewSql/insert_" + tSpec.id + "_" + action.id + ".sql");

				String actInsSql = schemaView.createInsertSql(tSpec.id + "_" + action.id);

				Assert.assertEquals(tSpec.id + "-insert-" + action.id, insertSql.replace("\r\n", "\n").replace("\r", ""), actInsSql);

				String selectSql = getSql("/fileViewSql/select_" + tSpec.id + "_" + action.id + ".sql");
				String actSelSql = schemaView.createSelectSql(tSpec.id + "_" + action.id);

				Assert.assertEquals(tSpec.id + "-select-" + action.id, selectSql.replace("\r\n", "\n").replace("\r", ""), actSelSql);

				// now evaluate materialized forms
				DataFileSchema matSchema = schemaView.materialize();

				createSql = getSql("/fileViewSql/create_" + tSpec.id + "_" + action.id + "_materialized.sql");

				actSql = matSchema.createTemporaryDbTableSql(tSpec.id + "_" + action.id);

				Assert.assertEquals(tSpec.id + "-create-mat-" + action.id, createSql.replace("\r\n", "\n").replace("\r", ""), actSql);

				insertSql = getSql("/fileViewSql/insert_" + tSpec.id + "_" + action.id + "_materialized.sql");

				actInsSql = matSchema.createInsertSql(tSpec.id + "_" + action.id);

				Assert.assertEquals(tSpec.id + "-insert-mat-" + action.id, insertSql.replace("\r\n", "\n").replace("\r", ""), actInsSql);

				selectSql = getSql("/fileViewSql/select_" + tSpec.id + "_" + action.id + "_materialized.sql");
				actSelSql = matSchema.createSelectSql(tSpec.id + "_" + action.id);

				Assert.assertEquals(tSpec.id + "-select-mat-" + action.id, selectSql.replace("\r\n", "\n").replace("\r", ""), actSelSql);
			}
		}
	}

	private String getSql(String s) throws IOException {
		URL target = getClass().getResource(s);

		Assert.assertNotNull("Results sql not found: " + s, target);

		return IOUtils.readURLToString(target);
	}

	private static final class TestSpec
	{
		String id;
		boolean execute = false;

		List<TestAction> actions = new ArrayList<TestAction>();
	}

	private static class TestAction {
		enum type {include, exclude}

		String id;
		type _type;

		List<String> columns = new ArrayList<String>();
	}
}
