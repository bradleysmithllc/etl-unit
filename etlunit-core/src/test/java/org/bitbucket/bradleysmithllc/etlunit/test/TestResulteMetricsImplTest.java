package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResultMetrics;
import org.bitbucket.bradleysmithllc.etlunit.TestResultMetricsImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClassImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethodImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.junit.Assert;
import org.junit.Test;

public class TestResulteMetricsImplTest
{
	@Test
	public void metricsReset()
	{
		TestResultMetrics trm = new TestResultMetricsImpl();

		Assert.assertEquals(0, trm.getNumberOfAssertionFailures());
		Assert.assertEquals(0, trm.getNumberOfErrors());
		Assert.assertEquals(0, trm.getNumberOfTestsPassed());
		Assert.assertEquals(0, trm.getNumberOfWarnings());
		Assert.assertEquals(0, trm.getNumberOfTestsRun());

		trm.addAssertionFailures(5);
		trm.addErrors(3);

		ETLTestClassImpl cls = new ETLTestClassImpl(ETLTestPackageImpl.getDefaultPackage());
		cls.setName("test_cls");

		ETLTestMethodImpl impl = new ETLTestMethodImpl("test", ETLTestMethod.method_type.test, cls);

		trm.addStatus(impl, null);
		trm.addStatus(impl, null);
		trm.addStatus(impl, null);
		trm.addTestsPassed(6);
		trm.addTestWarnings(4);

		Assert.assertEquals(5, trm.getNumberOfAssertionFailures());
		Assert.assertEquals(3, trm.getNumberOfErrors());
		Assert.assertEquals(6, trm.getNumberOfTestsPassed());
		Assert.assertEquals(4, trm.getNumberOfWarnings());
		Assert.assertEquals(3, trm.getNumberOfTestsRun());

		trm.reset();

		Assert.assertEquals(0, trm.getNumberOfAssertionFailures());
		Assert.assertEquals(0, trm.getNumberOfErrors());
		Assert.assertEquals(0, trm.getNumberOfTestsPassed());
		Assert.assertEquals(0, trm.getNumberOfWarnings());
		Assert.assertEquals(0, trm.getNumberOfTestsRun());
	}
}
