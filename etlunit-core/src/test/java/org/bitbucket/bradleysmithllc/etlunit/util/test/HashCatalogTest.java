package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.HashCatalog;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.StringBufferInputStream;

public class HashCatalogTest
{
	private interface Tester
	{
		void test(HashCatalog hc) throws Exception;
	}

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void testCatalogMD5() throws Exception
	{
		StringBufferInputStream rin = new StringBufferInputStream("Lots of tools spinning");

		String md5 = HashCatalog.strHashStream(rin);

		Assert.assertEquals("25D796FA9428E097B0A5F7A9D1E622CA", md5);
	}

	@Test
	public void testEmptyDir() throws Exception
	{
		runTest(new Tester()
		{
			@Override
			public void test(HashCatalog hc) throws Exception
			{
				hc.persist();

				File md5 = hc.getHashFile();

				Assert.assertTrue(md5.exists());

				Assert.assertEquals(HashCatalog.DEFAULT_HASH, hc.getHash());
				Assert.assertEquals(HashCatalog.DEFAULT_HASH, IOUtils.readFileToString(md5));

				File cat = hc.getCatalogFile();

				Assert.assertTrue(cat.exists());

				Assert.assertEquals("NAME|MD5", IOUtils.readFileToString(cat));
			}
		});
	}

	@Test
	public void testFilesInDir() throws Exception
	{
		runTest(new Tester()
		{
			@Override
			public void test(HashCatalog hc) throws Exception
			{
				File test = new File(hc.getDir(), "TEST");
				IOUtils.writeBufferToFile(test, new StringBuffer("Test Data"));

				hc.persist();

				File md5 = hc.getHashFile();

				Assert.assertTrue(md5.exists());

				Assert.assertEquals(hc.getHash(), IOUtils.readFileToString(md5));

				File cat = hc.getCatalogFile();

				Assert.assertTrue(cat.exists());

				Assert.assertEquals("NAME|MD5\nTEST|F315202B28422ED5C2AF4F843B8C2764", IOUtils.readFileToString(cat));
			}
		});
	}

	@Test
	public void testDirInDir() throws Exception
	{
		runTest(new Tester()
		{
			@Override
			public void test(HashCatalog hc) throws Exception
			{
				File test = new File(hc.getDir(), "TEST");
				IOUtils.writeBufferToFile(test, new StringBuffer("Test Data"));

				File testdir = new File(hc.getDir(), "testdir");

				FileUtils.forceMkdir(testdir);

				test = new File(testdir, "TEST");
				IOUtils.writeBufferToFile(test, new StringBuffer("Test Data"));

				hc.persist();

				File md5 = hc.getHashFile();

				Assert.assertTrue(md5.exists());

				Assert.assertEquals(hc.getHash(), IOUtils.readFileToString(md5));

				File cat = hc.getCatalogFile();

				Assert.assertTrue(cat.exists());

				Assert.assertEquals("NAME|MD5\n" +
						"TEST|F315202B28422ED5C2AF4F843B8C2764\n" +
						"TESTDIR|79290B4CD95B95A0966960FE9A6E776F", IOUtils.readFileToString(cat));

				HashCatalog hc2 = HashCatalog.createFromDirectory(testdir);

				File dmd5 = hc2.getHashFile();

				Assert.assertTrue(dmd5.exists());

				Assert.assertEquals("79290B4CD95B95A0966960FE9A6E776F", hc2.getHash());
				Assert.assertEquals("79290B4CD95B95A0966960FE9A6E776F", IOUtils.readFileToString(dmd5));

				cat = hc2.getCatalogFile();

				Assert.assertEquals("NAME|MD5\n" +
						"TEST|F315202B28422ED5C2AF4F843B8C2764", IOUtils.readFileToString(cat));
			}
		});
	}

	@Test
	public void testDoesNotCreateFilesUntilPersist() throws Exception
	{
		runTest(new Tester()
		{
			@Override
			public void test(HashCatalog hc) throws Exception
			{
				File md5 = hc.getHashFile();
				File cat = hc.getCatalogFile();

				Assert.assertFalse(cat.exists());
				Assert.assertFalse(md5.exists());

				hc.persist();

				Assert.assertTrue(md5.exists());
				Assert.assertTrue(cat.exists());
			}
		});
	}

	private void runTest(Tester tester) throws Exception
	{
		File tmp = temporaryFolder.newFolder();

		HashCatalog hc = HashCatalog.createFromDirectory(tmp);

		tester.test(hc);
	}
}
