package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.ConditionalDirector;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.parser.specification.TestSpecificationParser;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ConditionalDirectorTest
{
	public List<ETLTestClass> tests() throws ParseException {
		return ETLTestParser.load("class test1 {@Test cal() {}} class test2 {@Test col() {}} class test3 {}");
	}

	@Test
	public void or() throws ParseException, org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException {
		List<ETLTestClass> tests = tests();

		ETLTestClass test1 = tests.get(0);
		ETLTestClass test2 = tests.get(1);
		ETLTestClass test3 = tests.get(2);

		ClassDirector cld = TestSpecificationParser.parse("test1 or test2");

		Assert.assertThat(cld, new IsInstanceOf(ConditionalDirector.class));

		ClassResponder.response_code res = cld.accept(test1);

		Assert.assertEquals(ClassResponder.response_code.accept, res);

		res = cld.accept(test2);

		Assert.assertEquals(ClassResponder.response_code.accept, res);

		res = cld.accept(test3);

		Assert.assertEquals(ClassResponder.response_code.reject, res);

		cld = TestSpecificationParser.parse("test1 or #col");

		res = cld.accept(test1.getTestMethods().get(0));

		Assert.assertEquals(ClassResponder.response_code.accept, res);

		res = cld.accept(test2.getTestMethods().get(0));

		Assert.assertEquals(ClassResponder.response_code.accept, res);
	}

	@Test
	public void and() throws ParseException, org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException {
		List<ETLTestClass> tests = tests();

		ETLTestClass test1 = tests.get(0);
		ETLTestClass test2 = tests.get(1);

		ClassDirector cld = TestSpecificationParser.parse("test and test");

		Assert.assertThat(cld, new IsInstanceOf(ConditionalDirector.class));

		ClassResponder.response_code res = cld.accept(test1);

		Assert.assertEquals(ClassResponder.response_code.accept, res);

		res = cld.accept(test2);

		Assert.assertEquals(ClassResponder.response_code.accept, res);

		cld = TestSpecificationParser.parse("(test and test)");

		Assert.assertThat(cld, new IsInstanceOf(ConditionalDirector.class));

		res = cld.accept(test1);

		Assert.assertEquals(ClassResponder.response_code.accept, res);

		res = cld.accept(test2);

		Assert.assertEquals(ClassResponder.response_code.accept, res);
	}
}
