package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestPackages extends BaseFeatureModuleTest
{
	private RuntimeSupport runtimeSupport;

	@Inject
	public void receive(RuntimeSupport runtimeSupport)
	{
		this.runtimeSupport = runtimeSupport;
	}

	@Override
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList((Feature)
				new AbstractFeature()
				{
					@Override
					protected List<String> getSupportedFolderNamesSub()
					{
						return Arrays.asList("ignored");
					}

					@Override
					public String getFeatureName()
					{
						return "blah";
					}
				},
				new AbstractFeature()
				{
					@Override
					protected List<String> getSupportedFolderNamesSub()
					{
						return Arrays.asList("ignored-also");
					}

					@Override
					public String getFeatureName()
					{
						return "blah1";
					}
				}
												);
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		File dir = runtimeSupport.getTestSourceDirectory();
		runtimeSupport.getTestSourceDirectory(ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack1")).mkdirs();
		runtimeSupport.getTestSourceDirectory(ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack1.subpack1")).mkdirs();

		// add some ignored directories
		new FileBuilder(dir).subdir("_pack1_").subdir("ignored").mkdirs();
		new FileBuilder(dir).subdir("_pack1_").subdir("ignored-also").mkdirs();
		new FileBuilder(dir).subdir("_pack1_").subdir("ignored-also").subdir("dontdoit").mkdirs();
		new FileBuilder(dir).subdir("ignored").mkdirs();
	}

	@Test
	public void run()
	{
		startTest();

		List<ETLTestPackage> packs = runtimeSupport.getTestPackages();

		List<ETLTestPackage> expectedPacks = new ArrayList<ETLTestPackage>();
		expectedPacks.add(ETLTestPackageImpl.getDefaultPackage());
		expectedPacks.add(ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack1"));
		expectedPacks.add(ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack1.subpack1"));

		Assert.assertEquals(expectedPacks.size(), packs.size());

		for (int i = 0; i < expectedPacks.size(); i++)
		{
			Assert.assertEquals(expectedPacks.get(i).getPackageName(), packs.get(i).getPackageName());
		}
	}
}
