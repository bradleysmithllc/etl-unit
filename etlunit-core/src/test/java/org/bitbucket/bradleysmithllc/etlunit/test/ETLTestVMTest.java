package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Inject;
import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ETLTestVMTest extends BaseFeatureModuleTest
{
	@Test
	public void testFeatureSortedByDependency()
	{
		Feature ftest1 = new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "test1";
			}

			@Override
			public long getPriorityLevel()
			{
				return Long.MIN_VALUE;
			}
		};

		Feature ftest2 = new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "test2";
			}

			@Override
			public List<String> getPrerequisites()
			{
				return Arrays.asList(new String[]{"test1"});
			}
		};

		Feature ftest3 = new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "test3";
			}

			@Override
			public List<String> getPrerequisites()
			{
				return Arrays.asList(new String[]{"test2"});
			}

			@Override
			public long getPriorityLevel()
			{
				return Long.MIN_VALUE;
			}
		};

		Feature ftest4 = new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "test4";
			}

			@Override
			public List<String> getPrerequisites()
			{
				return Arrays.asList(new String[]{"test1"});
			}
		};

		Feature ftest5 = new AbstractFeature()
		{
			@Override
			public long getPriorityLevel()
			{
				return -1L;
			}

			@Override
			public String getFeatureName()
			{
				return "test5";
			}
		};

		List<Feature> res = Arrays.asList(new Feature[]{ftest1});

		ETLTestVM.sortByDependency(res);

		Assert.assertEquals(1, res.size());
		Assert.assertEquals("test1", res.get(0).getFeatureName());

		res = Arrays.asList(new Feature[]{ftest2, ftest1});

		ETLTestVM.sortByDependency(res);

		Assert.assertEquals(2, res.size());
		Assert.assertEquals("test1", res.get(0).getFeatureName());
		Assert.assertEquals("test2", res.get(1).getFeatureName());

		res = Arrays.asList(new Feature[]{ftest2, ftest5, ftest1, ftest4, ftest3});

		ETLTestVM.sortByDependency(res);

		Assert.assertEquals(5, res.size());
		Assert.assertEquals("test1", res.get(0).getFeatureName());
		Assert.assertEquals("test5", res.get(1).getFeatureName());
		Assert.assertEquals("test2", res.get(2).getFeatureName());
		Assert.assertEquals("test4", res.get(3).getFeatureName());
		Assert.assertEquals("test3", res.get(4).getFeatureName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testMissingDependency()
	{
		Feature ftest2 = new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "test2";
			}

			@Override
			public List<String> getPrerequisites()
			{
				return Arrays.asList(new String[]{"test1"});
			}
		};

		ETLTestVM.sortByDependency(Arrays.asList(new Feature[]{ftest2}));
	}

	@Test
	public void testNormalUsage()
	{
		Feature ftest2 = new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "test2";
			}
		};

		List<Feature> res = Arrays.asList(new Feature[]{ftest2});
		ETLTestVM.sortByDependency(res);

		Assert.assertEquals(1, res.size());
		Assert.assertEquals("test2", res.get(0).getFeatureName());
	}

	@Test
	public void testBasicUsage()
	{
		startTest("basicUsage");
	}

	@Test(expected = RuntimeException.class)
	public void missingFeaturePrerequisitesCauseFailures()
	{
		startTest("missingFeaturePrerequisitesCauseFailures");
	}

	Feature f_1 = null;
	Feature f_2 = null;
	Feature f_3 = null;

	@Test
	public void featuresInject()
	{
		startTest("featuresInject");

		Assert.assertEquals(f_3, f_2);
		Assert.assertTrue(((AbstractFeature) f_1).isInjected());
	}

	@Test
	public void featureConfigPassesThroughToOperation() throws IOException
	{
		startTest("featureConfigPassesThroughToOperation");
	}

	@Override
	protected List<Feature> getTestFeatures()
	{
		List<Feature> list = new ArrayList<Feature>();

		if (identifier != null && identifier.equals("basicUsage"))
		{
			list.add(new AbstractFeature()
			{
				@Override
				public String getFeatureName()
				{
					return "testalizer";
				}

				@Override
				public ClassListener getListener()
				{
					return new NullClassListener()
					{
						@Override
						public action_code process(ETLTestOperation op, ETLTestValueObject obj, final VariableContext context, ExecutionContext econtext, final int executor)
								throws TestAssertionFailure, TestExecutionError, TestWarning
						{
							return action_code.handled;
						}
					};
				}

				@Override
				public ClassDirector getDirector()
				{
					return new NullClassDirector()
					{
						@Override
						public response_code accept(ETLTestClass cl)
						{
							return response_code.accept;
						}

						@Override
						public response_code accept(ETLTestMethod mt)
						{
							return response_code.accept;
						}

						@Override
						public response_code accept(ETLTestOperation op)
						{
							return response_code.accept;
						}
					};
				}
			});
		}
		else if (identifier != null && identifier.equals("missingFeaturePrerequisitesCauseFailures"))
		{
			list.add(new AbstractFeature()
			{
				@Override
				public String getFeatureName()
				{
					return "testalizer";
				}

				@Override
				public List<String> getPrerequisites()
				{
					return Arrays.asList("testalizer-3");
				}
			});
		}
		else if (identifier != null && identifier.equals("featuresInject"))
		{
			list.add(f_1 = new AbstractFeature()
			{
				@Override
				public String getFeatureName()
				{
					return "";
				}
			});

			class f3 extends AbstractFeature
			{
				@Override
				public String getFeatureName()
				{
					return "testalizer-2";
				}
			}

			list.add(new AbstractFeature()
			{
				@Override
				public String getFeatureName()
				{
					return "testalizer";
				}

				@Override
				protected void useInjector(Injector inj)
				{
					inj.injectMembers(f_1);
				}

				@Override
				public List<String> getPrerequisites()
				{
					return Arrays.asList("testalizer-2");
				}

				@Inject
				public void setTestalizer_2(f3 f)
				{
					f_2 = f;
				}
			});

			f_3 = new f3();

			list.add(f_3);
		}
		else if (identifier != null && identifier.equals("featureConfigPassesThroughToOperation"))
		{
			list.add(new AbstractFeature()
			{
				@Override
				public String getFeatureName()
				{
					return "testalizer";
				}

				@Override
				public ClassListener getListener()
				{
					return new NullClassListener()
					{
						@Override
						public action_code process(ETLTestOperation op, ETLTestValueObject obj, final VariableContext context, ExecutionContext econtext, final int executor)
								throws TestAssertionFailure, TestExecutionError, TestWarning
						{
							ETLTestValueObject operands = op.getOperands();

							String serverValue = "config-server";
							String databaseValue = "config-database";

							if (operands != null)
							{
								ETLTestValueObject server = operands.query("server");

								if (server != null)
								{
									serverValue = server.getValueAsString();
								}

								ETLTestValueObject database = operands.query("database");

								if (database != null)
								{
									databaseValue = database.getValueAsString();
								}
							}

							Assert.assertEquals(serverValue, obj.query("server").getValueAsString());
							Assert.assertEquals(databaseValue, obj.query("database").getValueAsString());

							return action_code.handled;
						}
					};
				}

				@Override
				public ClassDirector getDirector()
				{
					return new NullClassDirector()
					{
						@Override
						public response_code accept(ETLTestClass cl)
						{
							return response_code.accept;
						}

						@Override
						public response_code accept(ETLTestMethod mt)
						{
							return response_code.accept;
						}

						@Override
						public response_code accept(ETLTestOperation op)
						{
							return response_code.accept;
						}
					};
				}
			});
		}

		return list;
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		if (identifier != null && identifier.equals("featureConfigPassesThroughToOperation"))
		{
			createSource("test.bitbucket",
					"class test { @Test testOperation() {test(database: 'override-database'); test(server: 'override-server');test2();test3(server: 'override-server', database: 'override-database');} }");
		}
	}
}
