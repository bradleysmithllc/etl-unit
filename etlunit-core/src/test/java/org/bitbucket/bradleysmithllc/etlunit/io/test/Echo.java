package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Echo
{
	public static final String GREETING = "Welcome to jecho v1.09a";
	public static final String PROMPT = "P>";
	public static final String DEGREETING = "bye";
	public static final String BLAH = "blah";

	public static void main(String[] argv) throws Exception
	{
		if (System.getProperty("hi") == null)
		{
			throw new IllegalArgumentException("System property missing");
		}
		else if (!System.getProperty("hi").equals("gi"))
		{
			throw new IllegalArgumentException("System property wrong");
		}

		System.out.println(GREETING);

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		boolean broken = false;

		while (!broken)
		{
			System.out.print(PROMPT);
			System.out.flush();

			String line = null;

			if ((line = reader.readLine()) != null)
			{
				if (line.equals("exit"))
				{
					broken = true;
					break;
				}

				for (int i = 0; ((Math.random() * System.currentTimeMillis()) % 10) > 2; i++)
				{
					System.out.println(BLAH);
				}
			}
			else
			{
				System.out.println("Input stream closed");
			}
		}

		System.out.println(DEGREETING);
		System.out.flush();
	}
}
