package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.Strings;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtilsTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void testUsage()
	{
		Assert.assertEquals("@hi there |@cutey    |@pie      ",
				EtlUnitStringUtils.wrapToLength("hi there cutey pie", 9, "@", "|"));
		Assert.assertEquals("hi there |cutey    |pie      ", EtlUnitStringUtils.wrapToLength("hi there cutey pie", 9, null, "|"));
		Assert.assertEquals("@hi|@there|@cutey|@pie", EtlUnitStringUtils.wrapToLength("hi there cutey pie", 1, "@", "|"));
		Assert.assertEquals("hi|there|cutey|pie", EtlUnitStringUtils.wrapToLength("hi there cutey pie", 1, null, "|"));

		Assert.assertEquals("@  hi there|@ cutey pie",
				EtlUnitStringUtils.wrapToLength("hi there cutey pie", 10, "@", "|", EtlUnitStringUtils.RIGHT_JUSTIFIED));
		Assert.assertEquals("  hi there| cutey pie",
				EtlUnitStringUtils.wrapToLength("hi there cutey pie", 10, null, "|", EtlUnitStringUtils.RIGHT_JUSTIFIED));
	}

	@Test
	public void sanitize()
	{
		Assert.assertEquals("Hello", EtlUnitStringUtils.sanitize("Hello", "Helo", 'a'));
		Assert.assertEquals("aaaaa", EtlUnitStringUtils.sanitize("Hello", "", 'a'));
		Assert.assertEquals("H_e__l_l_o_", EtlUnitStringUtils.sanitize("H[e]+l+l#o#", "Helo", '_'));

		Assert.assertEquals("Hello", EtlUnitStringUtils.sanitize("Hello", 'a'));
		Assert.assertEquals("H_e__l__l_o_", EtlUnitStringUtils.sanitize("H[e]+l\0+l#o#", '_'));
	}

	@Test
	public void testPrepareForLog()
	{
		Assert.assertEquals("[INFO] Hi\r" +
				"[INFO] Hello\n" +
				"[INFO] Bye\r" +
				"[INFO] \n" +
				"[INFO] ", EtlUnitStringUtils.prepareForLog("Hi\rHello\nBye\r\n", "[INFO]"));
	}

	@Test
	public void testPropertyName()
	{
		junit.framework.Assert.assertEquals("test", EtlUnitStringUtils.makePropertyName("test"));
		junit.framework.Assert.assertEquals("testStore", EtlUnitStringUtils.makePropertyName("testStore"));
		junit.framework.Assert.assertEquals("testStore", EtlUnitStringUtils.makePropertyName("test-store"));
	}

	@Test
	public void testUnderscoreName()
	{
		junit.framework.Assert.assertEquals("test", EtlUnitStringUtils.makeUnderscoreName("Test"));
		junit.framework.Assert.assertEquals("test_store", EtlUnitStringUtils.makeUnderscoreName("testStore"));
		junit.framework.Assert.assertEquals("test_store", EtlUnitStringUtils.makeUnderscoreName("test-store"));
	}

	@Test
	public void testProperPropertyName()
	{
		junit.framework.Assert.assertEquals("Test", EtlUnitStringUtils.makeProperPropertyName("test"));
		junit.framework.Assert.assertEquals("TestStore", EtlUnitStringUtils.makeProperPropertyName("testStore"));
		junit.framework.Assert.assertEquals("TestStore", EtlUnitStringUtils.makeProperPropertyName("test-store"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void escapeTextsDanglingAtEnd()
	{
		EtlUnitStringUtils.getEscapeTexts("a\\");
	}

	@Test(expected = IllegalArgumentException.class)
	public void escapeTextsDanglingAtBegin()
	{
		EtlUnitStringUtils.getEscapeTexts("\\a");
	}

	@Test(expected = IllegalArgumentException.class)
	public void escapeTextsDanglingInMiddle()
	{
		EtlUnitStringUtils.getEscapeTexts("b\\a");
	}

	@Test
	public void escapeTextsEmpty()
	{
		List<EtlUnitStringUtils.EscapeTextReference> escapeTexts = EtlUnitStringUtils.getEscapeTexts("\\\\");
		Assert.assertEquals(1, escapeTexts.size());

		EtlUnitStringUtils.EscapeTextReference actual = escapeTexts.get(0);
		Assert.assertEquals("", actual.getText());
		Assert.assertEquals(0, actual.getBeginOffset());
		Assert.assertEquals(1, actual.getEndOffset());
	}

	@Test
	public void escapeTextsFull()
	{
		List<EtlUnitStringUtils.EscapeTextReference> escapeTexts = EtlUnitStringUtils.getEscapeTexts("\\aa\\");
		Assert.assertEquals(1, escapeTexts.size());

		EtlUnitStringUtils.EscapeTextReference actual = escapeTexts.get(0);
		Assert.assertEquals("aa", actual.getText());
		Assert.assertEquals(0, actual.getBeginOffset());
		Assert.assertEquals(3, actual.getEndOffset());
	}

	@Test
	public void escapeTextsMany()
	{
		List<EtlUnitStringUtils.EscapeTextReference> escapeTexts = EtlUnitStringUtils.getEscapeTexts("aa\\bb\\cc\\dd\\ff\\gg\\\\hh\\kk");
		Assert.assertEquals(4, escapeTexts.size());

		EtlUnitStringUtils.EscapeTextReference actual = escapeTexts.get(0);
		Assert.assertEquals("bb", actual.getText());
		Assert.assertEquals(2, actual.getBeginOffset());
		Assert.assertEquals(5, actual.getEndOffset());

		actual = escapeTexts.get(1);
		Assert.assertEquals("dd", actual.getText());
		Assert.assertEquals(8, actual.getBeginOffset());
		Assert.assertEquals(11, actual.getEndOffset());

		actual = escapeTexts.get(2);
		Assert.assertEquals("gg", actual.getText());
		Assert.assertEquals(14, actual.getBeginOffset());
		Assert.assertEquals(17, actual.getEndOffset());

		actual = escapeTexts.get(3);
		Assert.assertEquals("hh", actual.getText());
		Assert.assertEquals(18, actual.getBeginOffset());
		Assert.assertEquals(21, actual.getEndOffset());
	}

	@Test
	public void testWalkPath() throws IOException {
		File fold = temporaryFolder.newFolder();

		List<String> list = new ArrayList<String>();

		EtlUnitStringUtils.walkFilePath(list, fold);

		Assert.assertEquals(list.get(list.size() - 1), fold.getName());
		Assert.assertEquals(list.get(list.size() - 2), fold.getParentFile().getName());
	}

	@Test
	public void testRelativeSamePath() throws IOException {
		File fold = temporaryFolder.newFolder();

		List<String> list = EtlUnitStringUtils.convertToRelativePath(fold, fold);

		Assert.assertEquals(1, list.size());
		Assert.assertEquals(".", list.get(0));
	}

	@Test
	public void testRelativeNothingInCommon() throws IOException {
		List<String> relativePath = new ArrayList<String>();
		EtlUnitStringUtils.comparePaths(Arrays.asList(new String[]{"C:", "dev"}), Arrays.asList(new String[]{"D:"}), relativePath);
		Assert.assertEquals(0, relativePath.size());
	}

	@Test
	public void testRelativeFromDeeper() throws IOException {
		List<String> relativePath = new ArrayList<String>();

		List from = Arrays.asList(new String[]{"C:", "dev"});
		List to = Arrays.asList(new String[]{"C:"});

		EtlUnitStringUtils.comparePaths(from, to, relativePath);

		Assert.assertEquals(1, relativePath.size());
		Assert.assertEquals("..", relativePath.get(0));
	}

	@Test
	public void testRelativeToDeeper() throws IOException {
		List<String> relativePath = new ArrayList<String>();

		List from = Arrays.asList(new String[]{"C:"});
		List to = Arrays.asList(new String[]{"C:", "dev"});

		EtlUnitStringUtils.comparePaths(from, to, relativePath);

		Assert.assertEquals(1, relativePath.size());
		Assert.assertEquals("dev", relativePath.get(0));
	}

	@Test
	public void testDeep() throws IOException {
		List<String> relativePath = new ArrayList<String>();

		List from = Arrays.asList(new String[]{"C:", "dev", "from1", "from2", "from3"});
		List to = Arrays.asList(new String[]{"C:", "dev", "to1", "to2", "to3"});

		EtlUnitStringUtils.comparePaths(from, to, relativePath);

		Assert.assertEquals(6, relativePath.size());
		Assert.assertEquals("..", relativePath.get(0));
		Assert.assertEquals("..", relativePath.get(1));
		Assert.assertEquals("..", relativePath.get(2));

		Assert.assertEquals("to1", relativePath.get(3));
		Assert.assertEquals("to2", relativePath.get(4));
		Assert.assertEquals("to3", relativePath.get(5));
	}

	@Test
	public void testFromPathIdentical() throws IOException {
		File to = temporaryFolder.newFolder();

		String rel = EtlUnitStringUtils.createRelativePath(to, to);

		Assert.assertEquals(".", rel);

		Assert.assertEquals(new File(to, rel).getCanonicalPath(), to.getCanonicalPath());
	}

	@Test
	public void testFileFromDeeper() throws IOException {
		File to = temporaryFolder.newFolder();
		File from = new File(to, "blah");

		FileUtils.forceMkdir(from);

		String rel = EtlUnitStringUtils.createRelativePath(from, to);

		Assert.assertEquals(new File(from, rel).getCanonicalPath(), to.getCanonicalPath());
	}

	@Test
	public void testFileToDeeper() throws IOException {
		File from = temporaryFolder.newFolder();
		File to = new File(from, "blah");

		FileUtils.forceMkdir(to);

		String rel = EtlUnitStringUtils.createRelativePath(from, to);

		Assert.assertEquals(new File(from, rel).getCanonicalPath(), to.getCanonicalPath());
	}

	@Test
	public void testFileFrom() throws IOException {
		File from = temporaryFolder.newFile();
		File to = new File(temporaryFolder.newFolder(), "blah");

		FileUtils.forceMkdir(to);

		String rel = EtlUnitStringUtils.createRelativePath(from, to);

		Assert.assertEquals(new File(from.getParentFile(), rel).getCanonicalPath(), to.getCanonicalPath());
	}

	@Test
	public void testFileTo() throws IOException {
		File from = temporaryFolder.newFolder();
		File to = temporaryFolder.newFile();

		FileUtils.touch(to);

		String rel = EtlUnitStringUtils.createRelativePath(from, to);

		Assert.assertEquals(new File(from, rel).getCanonicalPath(), to.getCanonicalPath());
	}

	@Test
	public void testTwoFiles() throws IOException {
		File from = temporaryFolder.newFile();
		File to = temporaryFolder.newFile();

		FileUtils.touch(from);
		FileUtils.touch(to);

		String rel = EtlUnitStringUtils.createRelativePath(from, to);

		Assert.assertEquals(new File(from.getParentFile(), rel).getCanonicalPath(), to.getCanonicalPath());
	}

	@Test
	public void testSpecific() throws IOException {
		List<String> relativePath = new ArrayList<String>();

		List from = Arrays.asList(new String[]{"/", "Users", "from1"});
		List to = Arrays.asList(new String[]{"/", "Users", "to1"});

		EtlUnitStringUtils.comparePaths(from, to, relativePath);

		Assert.assertEquals(2, relativePath.size());
		Assert.assertEquals("..", relativePath.get(0));
		Assert.assertEquals("to1", relativePath.get(1));
	}

	@Test
	public void strings()
	{
		Assert.assertEquals("a", Strings.constrain("a", 7));
		Assert.assertEquals("CATEGORY[6]", Strings.constrain("CATEGORY[6]", 11));
		Assert.assertEquals("a3981a1f", Strings.constrain("CATEGORY[12]", 11));
		Assert.assertEquals("17", Strings.constrain("1[6]", 2));
		Assert.assertEquals("6bd29db2", Strings.constrain("sadfhasjdhfashjdfasjhdfjahsdfhasdjfhjashdfjasdjhfjasdfjhasdf", 20));
	}

	@Test(expected = EOFException.class)
	public void parseToTokenEmpty() throws IOException {
		EtlUnitStringUtils.parseToToken(new StringReader(""), "<>".toCharArray());
	}

	@Test(expected = EOFException.class)
	public void parseToTokenNoMatch() throws IOException {
		EtlUnitStringUtils.parseToToken(new StringReader("Hi<Not>Me<"), "<>".toCharArray());
	}

	@Test
	public void parseToTokenMatch() throws IOException {
		Assert.assertEquals("Hi<Not>Me", EtlUnitStringUtils.parseToToken(new StringReader("Hi<Not>Me<>"), "<>".toCharArray()));
	}

	@Test
	public void parseToTokenBinary() throws IOException {
		Assert.assertEquals("sadsdfasdfasdfasdf\n\r\n\r\t\r\n\t\b", EtlUnitStringUtils.parseToToken(new StringReader("sadsdfasdfasdfasdf\n\r\n\r\t\r\n\t\b\0\t\0"), "\0\t\0".toCharArray()));
	}

	@Test
	public void parseToTokenMiddle() throws IOException {
		Assert.assertEquals("Hello", EtlUnitStringUtils.parseToToken(new StringReader("Hello World"), " Wo".toCharArray()));
	}

	@Test
	public void parseToTokenBeginning() throws IOException {
		Assert.assertEquals("", EtlUnitStringUtils.parseToToken(new StringReader("Hello World"), "Hel".toCharArray()));
	}

	@Test
	public void tokenInQuotesIsIgnored() throws IOException {
		Assert.assertEquals("\"Hel\"lo World", EtlUnitStringUtils.parseToToken(new StringReader("\"Hel\"lo WorldHel"), "Hel".toCharArray()));
	}

	@Test(expected = EOFException.class)
	public void unbalancedQuotesHideToken() throws IOException {
		EtlUnitStringUtils.parseToToken(new StringReader("\"Hello WorldHel"), "Hel".toCharArray());
	}
}
