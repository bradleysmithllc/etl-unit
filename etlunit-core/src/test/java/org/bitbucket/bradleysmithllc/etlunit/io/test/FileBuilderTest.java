package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class FileBuilderTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test(expected = IllegalArgumentException.class)
	public void clearDirectoryFailOnFile() throws IOException {
		new FileBuilder(temporaryFolder.newFile()).clearDir();
	}

	@Test
	public void clearDirectoryEmpty() throws IOException {
		File path = temporaryFolder.newFolder();
		new FileBuilder(path).clearDir();

		Assert.assertTrue(path.exists());
		Assert.assertEquals(0, path.listFiles().length);
	}

	@Test
	public void clearDirectoryWorks() throws IOException {
		File path = temporaryFolder.newFolder();

		FileUtils.touch(new File(path, "garbage"));

		new FileBuilder(path).clearDir();

		Assert.assertTrue(path.exists());
		Assert.assertEquals(0, path.listFiles().length);
	}

	@Test
	public void clearDirectoryTree() throws IOException {
		File path = temporaryFolder.newFolder();

		FileUtils.touch(new File(path, "garbage"));
		FileUtils.touch(new FileBuilder(path).subdir("a").subdir("b").subdir("c").name("file").mkdirsToFile().file());

		new FileBuilder(path).clearDir();

		Assert.assertTrue(path.exists());
		Assert.assertEquals(0, path.listFiles().length);
	}
}
