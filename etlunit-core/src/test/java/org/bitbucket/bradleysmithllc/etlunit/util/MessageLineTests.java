package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MessageLineTests {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void formatRightPaddingNegative() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Negative length");
		MessageLine.formatRightPadding('r', -1);
	}

	@Test
	public void formatRightPaddingEmpty() {
		Assert.assertEquals("", MessageLine.formatRightPadding('r', 0));
	}

	@Test
	public void formatRightPadding() {
		Assert.assertEquals("rrrrrrr", MessageLine.formatRightPadding('r', 7));
	}

	@Test
	public void formatTrimmedMessageEmpty() {
		Assert.assertEquals("", MessageLine.formatTrimmedMessage("", 0, 0, 0));
	}

	@Test
	public void formatTrimmedMessageNoTrim() {
		Assert.assertEquals("Hello", MessageLine.formatTrimmedMessage("Hello", 25, 0, 0));
	}

	@Test
	public void formatTrimmedMessageMaxLength() {
		Assert.assertEquals("Hel", MessageLine.formatTrimmedMessage("Hello", 3, 0, 0));
	}

	@Test
	public void formatTrimmedMessageMinLinePadding() {
		Assert.assertEquals("He", MessageLine.formatTrimmedMessage("Hello", 3, 1, 0));
	}

	@Test
	public void formatTrimmedMessageRightPadding() {
		Assert.assertEquals("He", MessageLine.formatTrimmedMessage("Hello", 3, 0, 1));
	}

	@Test
	public void formatTrimmedMessageAll() {
		Assert.assertEquals("H", MessageLine.formatTrimmedMessage("Hello", 3, 1, 1));
	}

	@Test
	public void formatTrimmedMessageTooMuch() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Negative length");
		MessageLine.formatTrimmedMessage("Hello", 2, 2, 2);
	}

	@Test
	public void formatMessage() {
		Assert.assertEquals("Classxxxxyyyy", MessageLine.formatMessage("Class", 'x', 4, 'y', 4, 13));
	}

	@Test
	public void formatMessageTruncSpaces() {
		Assert.assertEquals("Clasxxxxxyyyy", MessageLine.formatMessage("Class", 'x', 5, 'y', 4, 13));
	}

	@Test
	public void formatMessageTruncMinLinePadChars() {
		Assert.assertEquals("Claxxxxxyyyyy", MessageLine.formatMessage("Class", 'x', 5, 'y', 5, 13));
	}

	@Test
	public void formatMessageTruncMaxLineLength() {
		Assert.assertEquals("Cxxxxxyyyyy", MessageLine.formatMessage("Class", 'x', 5, 'y', 5, 11));
	}

	@Test
	public void formatStandardMessage() {
		Assert.assertEquals("executing class   ----------------------------------------------------------------", MessageLine.formatStandardMessage("executing class"));
	}

	@Test
	public void formatStandardMessageEmpty() {
		Assert.assertEquals("----------------------------------------------------------------------------------", MessageLine.formatStandardMessage(""));
	}
}
