package org.bitbucket.bradleysmithllc.etlunit.feature.results.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class ResultsFeatureModuleTest extends BaseFeatureModuleTest
{
	@Test
	public void testResultAggregationEmptySet() throws IOException
	{
		TestResults results = startTest();

		Assert.assertEquals(0, results.getResultsByClass().size());

		Assert.assertEquals(0, results.getMetrics().getNumberOfTestsRun());
		Assert.assertEquals(0, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
	}

	@Test
	public void testResultAggregation() throws IOException
	{
		TestResults results = startTest("CreateSources");

		Assert.assertEquals(3, results.getNumTestsSelected());

		Assert.assertEquals(3, results.getMetrics().getNumberOfTestsRun());
		Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(4, results.getMetrics().getNumberOfWarnings());
		Assert.assertEquals(2, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(1, results.getMetrics().getNumberOfErrors());

		Assert.assertEquals(2, results.getResultsByClass().size());

		Assert.assertEquals("tester", results.getResultsByClass().get(0).testClassName());
		Assert.assertEquals(2, results.getResultsByClass().get(0).getMetrics().getNumberOfTestsRun());
		Assert.assertEquals(1, results.getResultsByClass().get(0).getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(4, results.getResultsByClass().get(0).getMetrics().getNumberOfWarnings());
		Assert.assertEquals(2, results.getResultsByClass().get(0).getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getResultsByClass().get(0).getMetrics().getNumberOfErrors());

		Assert.assertEquals("tester", results.getResultsByClass().get(0).getMethodResults().get(0).testClassName());

		Assert.assertEquals("warnings", results.getResultsByClass().get(0).getMethodResults().get(0).testMethodName());
		Assert.assertEquals(1,
				results.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getNumberOfTestsRun());
		Assert.assertEquals(1,
				results.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(3,
				results.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getNumberOfWarnings());
		Assert.assertEquals(0,
				results.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0,
				results.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getNumberOfErrors());

		Assert.assertEquals("failures", results.getResultsByClass().get(0).getMethodResults().get(1).testMethodName());
		Assert.assertEquals(1,
				results.getResultsByClass().get(0).getMethodResults().get(1).getMetrics().getNumberOfTestsRun());
		Assert.assertEquals(0,
				results.getResultsByClass().get(0).getMethodResults().get(1).getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(1,
				results.getResultsByClass().get(0).getMethodResults().get(1).getMetrics().getNumberOfWarnings());
		Assert.assertEquals(2,
				results.getResultsByClass().get(0).getMethodResults().get(1).getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0,
				results.getResultsByClass().get(0).getMethodResults().get(1).getMetrics().getNumberOfErrors());

		Assert.assertEquals("tester2", results.getResultsByClass().get(1).testClassName());
		Assert.assertEquals(1, results.getResultsByClass().get(1).getMetrics().getNumberOfTestsRun());
		Assert.assertEquals(0, results.getResultsByClass().get(1).getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getResultsByClass().get(1).getMetrics().getNumberOfWarnings());
		Assert.assertEquals(0, results.getResultsByClass().get(1).getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(1, results.getResultsByClass().get(1).getMetrics().getNumberOfErrors());

		Assert.assertEquals("errors", results.getResultsByClass().get(1).getMethodResults().get(0).testMethodName());
		Assert.assertEquals(1,
				results.getResultsByClass().get(1).getMethodResults().get(0).getMetrics().getNumberOfTestsRun());
		Assert.assertEquals(0,
				results.getResultsByClass().get(1).getMethodResults().get(0).getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0,
				results.getResultsByClass().get(1).getMethodResults().get(0).getMetrics().getNumberOfWarnings());
		Assert.assertEquals(0,
				results.getResultsByClass().get(1).getMethodResults().get(0).getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(1,
				results.getResultsByClass().get(1).getMethodResults().get(0).getMetrics().getNumberOfErrors());
	}

	@Override
	protected List<Feature> getTestFeatures()
	{
		return Collections.emptyList();
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		if (identifier != null)
		{
			// write out a test file
			StringBuffer test = new StringBuffer("class tester {");
			test.append("@Test warnings() {");
			test.append("warn(message: 'whatever');");
			test.append("warn(message: 'whatever');");
			test.append("warn(message: 'whatever');");
			test.append("}");
			test.append("@Test failures() {");
			test.append("fail(message: 'whatever');");
			test.append("fail(message: 'whatever');");
			test.append("warn(message: 'whatever');");
			test.append("}");
			test.append("}");

			test.append("class tester2 {");
			test.append("@Test errors() {");
			test.append("error(message: 'whatever');");
			test.append("}");
			test.append("}");

			createSource("test.etlunit", test.toString());
		}
	}

	@Override
	protected boolean multiPassSafe() {
		return false;
	}
}
