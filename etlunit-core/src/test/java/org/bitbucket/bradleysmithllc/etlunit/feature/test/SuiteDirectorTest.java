package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.UserDirectedClassDirectorFeature;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.SuiteClassDirectorImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

public class SuiteDirectorTest
{
	@Test
	public void include() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(agg)");

		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'gagg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'gagg') class a{}").get(0)));
	}

	@Test
	public void includeMore() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(agg,bag,sag)");

		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'sag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'sag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'bag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'sag') @JoinSuite(name: 'bag') class a{}").get(0)));
	}

	@Test
	public void exclude() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(agg-)");

		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'gagg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'gagg') class a{}").get(0)));
	}

	@Test
	public void excludeMore() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(agg-,sag-,bag-)");

		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'sag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'bag') class a{}").get(0)));
	}

	@Test
	public void optional() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(agg?)");

		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'gagg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'gagg') class a{}").get(0)));
	}

	@Test
	public void optionalMore() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(agg?,sag?,bag?)");

		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'sag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'sag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'bag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'sag') @JoinSuite(name: 'bag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'agg') @JoinSuite(name: 'sag') @JoinSuite(name: 'bag') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: '1agg') @JoinSuite(name: '1sag') @JoinSuite(name: '1bag') class a{}").get(0)));
	}

	@Test
	public void includeExclude() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(inc,excl-)");

		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'inc') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'excl') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'inc') @JoinSuite(name: 'excl') class a{}").get(0)));
	}

	@Test
	public void includeExcludeOptional() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(inc,excl-,opt?)");

		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'inc') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'excl') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'inc') @JoinSuite(name: 'excl') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'inc') @JoinSuite(name: 'opt') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'opt') class a{}").get(0)));
	}

	@Test
	public void excludeOptional() throws ParseException {
		SuiteClassDirectorImpl simpl = new SuiteClassDirectorImpl("suite(excl-,opt?)");

		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'inc') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'excl') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'inc') @JoinSuite(name: 'excl') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'inc') @JoinSuite(name: 'opt') class a{}").get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, simpl.accept(ETLTestParser.load("@JoinSuite(name: 'opt') class a{}").get(0)));
	}

	@Test
	public void userDirectorTag() throws org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException {
		UserDirectedClassDirectorFeature userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "@t and %e");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "[suite]");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "@t and <g>");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "<g>");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "<g> or [suite]");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "(<g> or [suite])");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "<g> and @t");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "<g, fail>");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "<g, error>");
		userDirectedClassDirectorFeature = new UserDirectedClassDirectorFeature("inst", "<g, error> or {type: 'dd'}");
	}

	@Test
	public void messedUp() throws org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException {
		new UserDirectedClassDirectorFeature("inst",
			"((@t and %e) or ($script or [suite])) and (<tag> or {prop: 'true'})"
		);
	}

	@Test(expected = IllegalArgumentException.class)
	public void badTagType() throws org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException {
		new UserDirectedClassDirectorFeature("inst", "<tag, hi>");
	}
}
