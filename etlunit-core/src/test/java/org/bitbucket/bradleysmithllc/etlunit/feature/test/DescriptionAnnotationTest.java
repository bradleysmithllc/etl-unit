package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class DescriptionAnnotationTest
{
	@Test
	public void multilineClassDescription() throws ParseException {
		List<ETLTestClass> tests = ETLTestParser.load("@Description(description: [\"Hello\", \"World\"]) class test{}");

		Assert.assertEquals("Hello\nWorld", tests.get(0).getDescription());
	}

	@Test
	public void singlelineClassDescription() throws ParseException {
		List<ETLTestClass> tests = ETLTestParser.load("@Description(description: \"Hello\\nWorld\") class test{}");

		Assert.assertEquals("Hello\nWorld", tests.get(0).getDescription());
	}

	@Test
	public void multilineMethodDescription() throws ParseException {
		List<ETLTestClass> tests = ETLTestParser.load("class test{@Description(description: [\"Hello\", \"World\"]) @Test test(){}}");

		Assert.assertEquals("Hello\nWorld", tests.get(0).getTestMethods().get(0).getDescription());
	}

	@Test
	public void singlelineMethodDescription() throws ParseException {
		List<ETLTestClass> tests = ETLTestParser.load("class test{@Description(description: \"Hello\\nWorld\") @Test test(){}}");

		Assert.assertEquals("Hello\nWorld", tests.get(0).getTestMethods().get(0).getDescription());
	}
}
