package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.Expectorator;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class ExpectoratorTest
{
	@Test
	public void echo() throws IOException
	{
		ExpectoratorScriptor es = new ExpectoratorScriptor();

		Expectorator exp = new Expectorator(es.getOutput(), es.getInput());

		Assert.assertEquals("hello", exp.sayAndExpect("hello", "hello"));
	}

	@Test
	public void echoRegExp() throws IOException
	{
		ExpectoratorScriptor es = new ExpectoratorScriptor();

		Expectorator exp = new Expectorator(es.getOutput(), es.getInput());

		Assert.assertEquals("hello", exp.sayAndExpect("hello", "h.llo"));
	}

	@Test(expected = InterruptedException.class)
	public void echoWTimeout() throws IOException, InterruptedException
	{
		ExpectoratorScriptor es = new ExpectoratorScriptor();

		Expectorator exp = new Expectorator(es.getOutput(), es.getInput());

		exp.sayAndExpect("hello", "hollo", 500L);
	}

	@Test
	public void context() throws IOException
	{
		ExpectoratorScript es = new ExpectoratorScript("helloworld");

		Expectorator exp = new Expectorator(es.getOutput(), es.getInput());

		Assert.assertEquals("hello", exp.sayAndExpect("hello", "hello"));
		Assert.assertEquals("world", exp.sayAndExpect("hello", "world"));
	}

	@Test(expected = InterruptedException.class)
	public void contextEndsWithoutMatching() throws IOException, InterruptedException
	{
		ExpectoratorScript es = new ExpectoratorScript("helloworld");

		Expectorator exp = new Expectorator(es.getOutput(), es.getInput());

		Assert.assertEquals("hello", exp.sayAndExpect("hello", "hello"));
		Assert.assertEquals("hello", exp.sayAndExpect("hello", "hello", 500L));
	}
}
