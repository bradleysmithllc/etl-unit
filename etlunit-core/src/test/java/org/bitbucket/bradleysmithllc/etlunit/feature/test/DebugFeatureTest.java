package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class DebugFeatureTest extends BaseFeatureModuleTest
{
	@Test
	public void testError() throws IOException
	{
		TestResults results = startTest("testError");

		Assert.assertEquals(4, results.getNumTestsSelected());
		Assert.assertEquals(0, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(4, results.getMetrics().getNumberOfErrors());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());

		StatusReporter.CompletionStatus
				completionStatus =
				results.getMetrics().getResultsMapByTest().get("[default].testerror.error");
		Assert.assertNotNull(completionStatus);

		Assert.assertEquals(1, completionStatus.getErrors().size());
		TestExecutionError error = completionStatus.getErrors().get(0);

		Assert.assertEquals(TestConstants.ERR_UNSPECIFIED, error.getErrorId());

		completionStatus = results.getMetrics().getResultsMapByTest().get("[default].testerror.error_ids");
		Assert.assertNotNull(completionStatus);

		Assert.assertEquals(1, completionStatus.getErrors().size());
		error = completionStatus.getErrors().get(0);

		Assert.assertEquals("ERR_EXPECTED", error.getErrorId());

		completionStatus = results.getMetrics().getResultsMapByTest().get("[default].testerror.die");
		Assert.assertNotNull(completionStatus);

		Assert.assertEquals(1, completionStatus.getErrors().size());
		error = completionStatus.getErrors().get(0);

		Assert.assertEquals(TestConstants.ERR_UNCAUGHT_EXCEPTION, error.getErrorId());
	}

	@Test
	public void testWarn() throws IOException
	{
		TestResults results = startTest("testWarn");

		Assert.assertEquals(2, results.getNumTestsSelected());
		Assert.assertEquals(2, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
		Assert.assertEquals(2, results.getMetrics().getNumberOfWarnings());

		StatusReporter.CompletionStatus completionStatus = results.getMetrics().getResultsMapByTest().get("[default].testwarn.warn");
		Assert.assertNotNull(completionStatus);

		List<TestWarning> warnings = completionStatus.getWarnings();
		Assert.assertNotNull(warnings);

		Assert.assertEquals(TestConstants.WARN_UNSPECIFIED, warnings.get(0).getWarningId());

		completionStatus = results.getMetrics().getResultsMapByTest().get("[default].testwarn.warn_ids");
		Assert.assertNotNull(completionStatus);

		warnings = completionStatus.getWarnings();
		Assert.assertNotNull(warnings);

		Assert.assertEquals("WARN_EXPECTED", warnings.get(0).getWarningId());
	}

	@Test
	public void testFail() throws IOException
	{
		TestResults results = startTest("testFail");

		Assert.assertEquals(2, results.getNumTestsSelected());
		Assert.assertEquals(0, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(2, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());

		StatusReporter.CompletionStatus completionStatus = results.getMetrics().getResultsMapByTest().get("[default].testfail.fail");
		Assert.assertNotNull(completionStatus);

		List<TestAssertionFailure> warnings = completionStatus.getAssertionFailures();
		Assert.assertNotNull(warnings);

		Assert.assertEquals(TestConstants.FAIL_UNSPECIFIED, warnings.get(0).getFailureIds()[0]);

		completionStatus = results.getMetrics().getResultsMapByTest().get("[default].testfail.fail_ids");
		Assert.assertNotNull(completionStatus);

		warnings = completionStatus.getAssertionFailures();
		Assert.assertNotNull(warnings);

		Assert.assertEquals("FAIL_EXPECTED", warnings.get(0).getFailureIds()[0]);
	}

	@Test
	public void testMultiple() throws IOException
	{
		TestResults results = startTest("testMultiple");

		Assert.assertEquals(1, results.getNumTestsSelected());
		Assert.assertEquals(0, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(3, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
		Assert.assertEquals(4, results.getMetrics().getNumberOfWarnings());
	}

	@Test
	public void testAssert() throws IOException
	{
		TestResults results = startTest("testAssert");

		Assert.assertEquals(1, results.getNumTestsSelected());
		Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());
	}

	@Override
	protected List<Feature> getTestFeatures()
	{
		return Collections.emptyList();
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		if (identifier != null && identifier.equals("testError"))
		{
			createSourceFromClasspath(null, "debug_error_test.etlunit");
		}
		else if (identifier != null && identifier.equals("testAssert"))
		{
			createSourceFromClasspath(null, "debug_assert_test.etlunit");
		}
		else if (identifier != null && identifier.equals("testWarn"))
		{
			createSourceFromClasspath(null, "debug_warning_test.etlunit");
		}
		else if (identifier != null && identifier.equals("testFail"))
		{
			createSourceFromClasspath(null, "debug_failure_test.etlunit");
		}
		else if (identifier != null && identifier.equals("testMultiple"))
		{
			createSourceFromClasspath(null, "debug_multiple_test.etlunit");
		}
	}
}
