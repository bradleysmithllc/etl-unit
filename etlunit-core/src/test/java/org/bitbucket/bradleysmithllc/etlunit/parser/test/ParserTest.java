package org.bitbucket.bradleysmithllc.etlunit.parser.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class ParserTest
{
	@Test(expected = IllegalArgumentException.class)
	public void badBooleanThrowsError() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("Sure_Enough");
		value.getValueAsBoolean();
	}

	@Test
	public void valueObjectParser() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("'1'");
		Assert.assertEquals("1", value.getValueAsString());
		Assert.assertEquals(ETLTestValueObject.value_type.quoted_string, value.getValueType());

		value = ETLTestParser.loadObject("\"1\"");
		Assert.assertEquals("1", value.getValueAsString());
		Assert.assertEquals(ETLTestValueObject.value_type.quoted_string, value.getValueType());

		// unquoted literals such as integers and floats are reported as quoted
		// identifiers so that references to other objects can be differentiated
		// without having to check for a numeric value
		value = ETLTestParser.loadObject("true");
		Assert.assertEquals("true", value.getValueAsString());
		Assert.assertTrue(value.getValueAsBoolean());
		Assert.assertEquals(ETLTestValueObject.value_type.literal, value.getValueType());

		value = ETLTestParser.loadObject("false");
		Assert.assertEquals("false", value.getValueAsString());
		Assert.assertFalse(value.getValueAsBoolean());
		Assert.assertEquals(ETLTestValueObject.value_type.literal, value.getValueType());

		value = ETLTestParser.loadObject("1");
		Assert.assertEquals("1", value.getValueAsString());
		Assert.assertEquals(ETLTestValueObject.value_type.literal, value.getValueType());

		value = ETLTestParser.loadObject("_happy_joy");
		Assert.assertEquals("_happy_joy", value.getValueAsString());
		Assert.assertEquals(ETLTestValueObject.value_type.literal, value.getValueType());

		value = ETLTestParser.loadObject("[1, 2, 3, 4]");

		Assert.assertEquals(ETLTestValueObject.value_type.list, value.getValueType());

		List<ETLTestValueObject> list = value.getValueAsList();

		Assert.assertEquals(4, list.size());

		Assert.assertEquals("1", list.get(0).getValueAsString());
		Assert.assertEquals("2", list.get(1).getValueAsString());
		Assert.assertEquals("3", list.get(2).getValueAsString());
		Assert.assertEquals("4", list.get(3).getValueAsString());
	}

	@Test
	public void arrayTypeHasArrayRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("[1]");

		List<ETLTestValueObject> list = value.getValueAsList();

		Assert.assertEquals(1, list.size());

		Assert.assertEquals("1", list.get(0).getValueAsString());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void arrayTypeDoesNotHaveStringRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("[1]");
		Assert.assertEquals(ETLTestValueObject.value_type.list, value.getValueType());
		value.getValueAsString();
	}

	@Test(expected = UnsupportedOperationException.class)
	public void arrayTypeDoesNotHaveMapRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("[1]");
		Assert.assertEquals(ETLTestValueObject.value_type.list, value.getValueType());
		value.getValueAsMap();
	}

	@Test
	public void mapTypeHasMapRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("{buckets: [1], 'nails': 20, \"coffins\": 30}");
		Assert.assertEquals(ETLTestValueObject.value_type.object, value.getValueType());
		Map<String, ETLTestValueObject> map = value.getValueAsMap();

		Assert.assertEquals(3, map.size());

		ETLTestValueObject val = map.get("nails");
		Assert.assertEquals("20", val.getValueAsString());
		Assert.assertEquals(ETLTestValueObject.value_type.literal, val.getValueType());

		val = map.get("coffins");
		Assert.assertEquals("30", val.getValueAsString());
		Assert.assertEquals(ETLTestValueObject.value_type.literal, val.getValueType());

		val = map.get("buckets");
		Assert.assertEquals(ETLTestValueObject.value_type.list, val.getValueType());

		List<ETLTestValueObject> list = val.getValueAsList();
		Assert.assertEquals(1, list.size());
		Assert.assertEquals("1", list.get(0).getValueAsString());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void mapTypeDoesNotHaveStringRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("{buckets: [1]}");
		Assert.assertEquals(ETLTestValueObject.value_type.object, value.getValueType());
		value.getValueAsString();
	}

	@Test(expected = UnsupportedOperationException.class)
	public void mapTypeDoesNotHaveListRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("{buckets: [1]}");
		Assert.assertEquals(ETLTestValueObject.value_type.object, value.getValueType());
		value.getValueAsList();
	}

	@Test
	public void stringTypeHasStringRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("1");
		Assert.assertEquals(ETLTestValueObject.value_type.literal, value.getValueType());
		Assert.assertEquals("1", value.getValueAsString());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void stringTypeDoesNotHaveListRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("1");
		Assert.assertEquals(ETLTestValueObject.value_type.literal, value.getValueType());
		value.getValueAsList();
	}

	@Test(expected = UnsupportedOperationException.class)
	public void stringTypeDoesNotHaveMapRepresentation() throws ParseException
	{
		ETLTestValueObject value = ETLTestParser.loadObject("1");
		Assert.assertEquals(ETLTestValueObject.value_type.literal, value.getValueType());
		value.getValueAsMap();
	}

	@Test
	public void compoundObjects() throws ParseException
	{
		ETLTestValueObject
				value =
				ETLTestParser.loadObject(
						"{boxes: {lids: 5, hinges: [{name: hinge1, material: brass}, {name: hinge2, material: copper}]}}");
	}

	@Test
	public void testPackageName() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class hello { @Test setup(){} }", ETLTestPackageImpl.getDefaultPackage().getSubPackage("com.package.name"));
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getName(), "hello");
		Assert.assertEquals("com.package.name", itcl.get(0).getPackage().getPackageName());
		Assert.assertEquals("com.package.name.hello", itcl.get(0).getQualifiedName());

		Assert.assertEquals(1, itcl.get(0).getBeginLineNumber());
		Assert.assertEquals(7, itcl.get(0).getBeginColumnOffset());
		Assert.assertEquals(1, itcl.get(0).getEndLineNumber());
		Assert.assertEquals(11, itcl.get(0).getEndColumnOffset());

		ETLTestMethod meth = itcl.get(0).getTestMethods().get(0);
		Assert.assertEquals("setup", meth.getName());
		Assert.assertEquals("com.package.name.hello.setup", meth.getQualifiedName());
		Assert.assertEquals(1, meth.getBeginLineNumber());
		Assert.assertEquals(21, meth.getBeginColumnOffset());
		Assert.assertEquals(1, meth.getEndLineNumber());
		Assert.assertEquals(25, meth.getEndColumnOffset());

		itcl = ETLTestParser.load("class hello {}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getName(), "hello");
		Assert.assertEquals("[default]", itcl.get(0).getPackage().getPackageName());
		Assert.assertEquals("[default].hello", itcl.get(0).getQualifiedName());
	}

	@Test
	public void testParserMinimum() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class hello {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("com.package.name"));
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getName(), "hello");
		Assert.assertEquals("com.package.name", itcl.get(0).getPackage().getPackageName());

		itcl = ETLTestParser.load("class _hello {}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getName(), "_hello");

		itcl =
				ETLTestParser.load(
						"@Description(description: hello1)class _hello1 {}@Description(description: hello2)class _hello2{}");
		Assert.assertEquals(itcl.size(), 2);
		Assert.assertEquals(itcl.get(0).getName(), "_hello1");
		Assert.assertEquals(itcl.get(1).getName(), "_hello2");
	}

	@Test
	public void testClassAnnotations() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("@JoinSuite(name: labor)class hello {}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getSuiteMemberships().size(), 1);
		Assert.assertEquals(itcl.get(0).getSuiteMemberships().get(0), "labor");

		itcl =
				ETLTestParser.load(
						" @JoinSuite ( name:labor )@JoinSuite ( name: labor2 ) @JoinSuite ( name: labor3 ) class _hello {}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getSuiteMemberships().size(), 3);
		Assert.assertEquals(itcl.get(0).getSuiteMemberships().get(0), "labor");
		Assert.assertEquals(itcl.get(0).getSuiteMemberships().get(1), "labor2");
		Assert.assertEquals(itcl.get(0).getSuiteMemberships().get(2), "labor3");

		itcl = ETLTestParser.load("class _hello {}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getSuiteMemberships().size(), 0);
	}

	@Test(expected = Exception.class)
	public void testParserBadIdentifier() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class 1hello {}");
	}

	@Test(expected = IllegalStateException.class)
	public void commentsRequireDescription() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("@Description class hello {}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getDescription(), "");
	}

	@Test
	public void testSuiteComments() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("@Description(description: '') class hello {}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getDescription(), "");

		itcl = ETLTestParser.load("@Description class hello {}");
		Assert.assertEquals(itcl.size(), 1);

		itcl = ETLTestParser.load("		 \r\n@Description class hello {}");
		Assert.assertEquals(itcl.size(), 1);

		itcl = ETLTestParser.load("@Description(description: Test) class hello {}");
		Assert.assertEquals(itcl.size(), 1);
		itcl =
				ETLTestParser.load(
						"@Description(description: ' Test  s s s s s sa 98 98  \\f\\b\\r\\t\\n&i ui78 *^%@#!@*^(QWIHDACZ ') class hello {}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getDescription(),
				" Test  s s s s s sa 98 98  \f\b\r\t\n&i ui78 *^%@#!@*^(QWIHDACZ ");
	}

	@Test
	public void testTestComments() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class hello {@Description@BeforeClass setup(){}}");
		Assert.assertEquals(itcl.size(), 1);
		itcl = ETLTestParser.load("class hello {@BeforeClass setup(){}}");
		Assert.assertEquals(itcl.size(), 1);
		itcl =
				ETLTestParser.load(
						"class hello {  \t\r@Description(description: 'Bugga\\t\\n\\t\\r\\r\\r\\nBoo')\t\n  @BeforeClass setup(){}}");
		Assert.assertEquals(itcl.size(), 1);

		itcl = ETLTestParser.load("class hello {@BeforeClass setup(){\r//Hello comments\r}}");
		itcl = ETLTestParser.load("class hello {@BeforeClass setup(){\r/*Hello comments*/\r}}");
		itcl = ETLTestParser.load("class hello /*Hello\r\n\n\n\rComents*/{@BeforeClass setup(){\r/*Hello comments*/\r}}");
	}

	@Test
	public void testTestAnnotations() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class hello {@BeforeClass setup(){}}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getBeforeClassMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getBeforeClassMethods().get(0).getName(), "setup");
		Assert.assertEquals(itcl.get(0).getAfterClassMethods().size(), 0);
		Assert.assertEquals(itcl.get(0).getBeforeTestMethods().size(), 0);
		Assert.assertEquals(itcl.get(0).getAfterTestMethods().size(), 0);
		Assert.assertEquals(itcl.get(0).getTestMethods().size(), 0);

		itcl = ETLTestParser.load("class hello {@AfterClass setup(){}}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getAfterClassMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getAfterClassMethods().get(0).getName(), "setup");

		itcl = ETLTestParser.load("class hello {@BeforeTest setup(){}}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getBeforeTestMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getBeforeTestMethods().get(0).getName(), "setup");

		itcl = ETLTestParser.load("class hello {@AfterTest setup(){}}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getAfterTestMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getAfterTestMethods().get(0).getName(), "setup");

		itcl = ETLTestParser.load("class hello {@Test setup(){}@Test @DoNotPurge setup2(){}}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getTestMethods().size(), 2);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getName(), "setup");
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).requiresPurge(), true);

		Assert.assertEquals(itcl.get(0).getTestMethods().get(1).getName(), "setup2");
		Assert.assertEquals(itcl.get(0).getTestMethods().get(1).requiresPurge(), false);

		itcl =
				ETLTestParser.load(
						"class hello {@Test test1(){}@AfterTest tear_test(){}@BeforeTest set_test(){}@AfterClass tear_class(){}@BeforeClass set_class(){}}");
		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getTestMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getName(), "test1");
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).requiresPurge(), true);

		Assert.assertEquals(itcl.get(0).getAfterTestMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getAfterTestMethods().get(0).getName(), "tear_test");

		Assert.assertEquals(itcl.get(0).getBeforeTestMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getBeforeTestMethods().get(0).getName(), "set_test");

		Assert.assertEquals(itcl.get(0).getBeforeClassMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getBeforeClassMethods().get(0).getName(), "set_class");

		Assert.assertEquals(itcl.get(0).getAfterClassMethods().size(), 1);
		Assert.assertEquals(itcl.get(0).getAfterClassMethods().get(0).getName(), "tear_class");

		itcl =
				ETLTestParser.load(
						"class hello {@Description(description: '')@Test setup1(){}@Description(description: '')@AfterTest setup2(){}@Description(description: '')@BeforeTest setup3(){}@Description(description: '')@AfterClass setup4(){}@Description(description: '')@BeforeClass setup5(){}}");
		Assert.assertEquals(itcl.size(), 1);

		itcl = ETLTestParser.load("class hello {//@Test\r\nsetup1(){}}");
		Assert.assertEquals(itcl.size(), 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTestDuplicateMethodName() throws ParseException
	{
		ETLTestParser.load("class hello {@BeforeClass setup(){}@BeforeClass setup(){}}");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTestDoNotPurgeFirstMethod() throws ParseException
	{
		ETLTestParser.load("class hello {@Test @DoNotPurge setup(){}}");
	}

	@Test
	public void testTestMethodOperations() throws ParseException
	{
		List<ETLTestClass>
				itcl =
				ETLTestParser.load(
						"class hello {@Test setup(){badger(\r\n\r\na: \"10\"\r\n\r\n, b: \"2\", c: \"3\", d: \"a\", e: a);badger2();}}");

		Assert.assertEquals(itcl.size(), 1);

		Assert.assertEquals(itcl.get(0).getTestMethods().size(), 1);

		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getName(), "setup");

		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().size(), 2);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperationName(), "badger");

		ETLTestOperation op = itcl.get(0).getTestMethods().get(0).getOperations().get(0);

		// test debug traceable properties
		Assert.assertEquals(1, op.getBeginLineNumber());
		Assert.assertEquals(28, op.getBeginColumnOffset());
		Assert.assertEquals(1, op.getEndLineNumber());
		Assert.assertEquals(33, op.getEndColumnOffset());

		Map<String, ETLTestValueObject>
				map =
				itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().getValueAsMap();

		Assert.assertEquals(5, map.size());

		Assert.assertEquals(map.get("a").getValueType(), ETLTestValueObject.value_type.quoted_string);
		Assert.assertEquals(map.get("a").getValueAsString(), "10");

		Assert.assertEquals(map.get("b").getValueType(), ETLTestValueObject.value_type.quoted_string);
		Assert.assertEquals(map.get("b").getValueAsString(), "2");

		Assert.assertEquals(map.get("c").getValueType(), ETLTestValueObject.value_type.quoted_string);
		Assert.assertEquals(map.get("c").getValueAsString(), "3");

		Assert.assertEquals(map.get("d").getValueType(), ETLTestValueObject.value_type.quoted_string);
		Assert.assertEquals(map.get("d").getValueAsString(), "a");

		Assert.assertEquals(map.get("e").getValueType(), ETLTestValueObject.value_type.literal);
		Assert.assertEquals(map.get("e").getValueAsString(), "a");

		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(1).getOperationName(), "badger2");

		op = itcl.get(0).getTestMethods().get(0).getOperations().get(1);

		// test debug traceable properties
		Assert.assertEquals(5, op.getBeginLineNumber());
		Assert.assertEquals(33, op.getBeginColumnOffset());
		Assert.assertEquals(5, op.getEndLineNumber());
		Assert.assertEquals(39, op.getEndColumnOffset());
	}

	@Test
	public void testTestMethodOperandTypes() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class "
				+ "hello {"
				+ "@Test setup(){"
				+ "badger(a: \"1\", b: 'a', c: {});"
				+ "}"
				+ "}");
		Assert.assertEquals(itcl.size(), 1);

		itcl = ETLTestParser.load("class "
				+ "hello {"
				+ "@Test setup(){"
				+ "badger(a: \"1\", b: a, c: {});"
				+ "}"
				+ "}");

		Assert.assertEquals(itcl.size(), 1);
	}

	@Test(expected = ParseException.class)
	public void testTestBadSeparators() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class "
				+ "hello {"
				+ "@Test setup(){"
				+ "badger(,\"1\", a, {a: b});"
				+ "}"
				+ "}");
	}

	@Test(expected = ParseException.class)
	public void testTestBadMapSeparators() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class "
				+ "hello {"
				+ "@Test setup(){"
				+ "badger(\"1\", a, {, a: b});"
				+ "}"
				+ "}");
	}

	@Test(expected = ParseException.class)
	public void testTestBadMapArrSeparators() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load("class "
				+ "hello {"
				+ "@Test setup(){"
				+ "badger(\"1\", a, {a: [,a]});"
				+ "}"
				+ "}");
	}

	@Test
	public void testTestMapMethodOperand() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load(
				"class hello {"
						+ "@Test setup(){"
						+ "badger(a: \"1\", b: a, c: {a: \"b1_2c.txt\"}, d: {c: d, h: [a, b, c, d, s]});"
						+ "}"
						+ "}");

		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getTestMethods().size(), 1);

		ETLTestMethod setup = itcl.get(0).getTestMethods().get(0);

		Assert.assertEquals(setup.getOperations().size(), 1);

		itcl = ETLTestParser.load(
				"class hello {"
						+ "@Test setup(){"
						+ "badger({a: \"1\", b: a, c: {a: \"b1_2c.txt\"}, d: {c: d, h: [a, b, c, d, s]}});"
						+ "}"
						+ "}");

		Assert.assertEquals(itcl.size(), 1);
		Assert.assertEquals(itcl.get(0).getTestMethods().size(), 1);

		setup = itcl.get(0).getTestMethods().get(0);

		Assert.assertEquals(setup.getOperations().size(), 1);

		/*
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().size(), 4);

		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(0).getValue().getValueType(), ETLTestValueObject.value_type.quoted_string);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(0).getOperandAsString(), "1");

		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(1).getValue().getValueType(), ETLTestValueObject.value_type.literal);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(1).getOperandAsString(), "a");

		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(2).getValue().getValueType(), ETLTestValueObject.value_type.object);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(2).getValue().getValueAsMap().size(), 1);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(2).getValue().getValueAsMap().get("a").getValueAsString(), "b1_2c.txt");

		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueType(), ETLTestValueObject.value_type.object);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueAsMap().size(), 2);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueAsMap().get("c").getValueAsString(), "d");
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueAsMap().get("h").getValueAsList().size(), 5);
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueAsMap().get("h").getValueAsList().get(0).getValueAsString(), "a");
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueAsMap().get("h").getValueAsList().get(1).getValueAsString(), "b");
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueAsMap().get("h").getValueAsList().get(2).getValueAsString(), "c");
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueAsMap().get("h").getValueAsList().get(3).getValueAsString(), "d");
		Assert.assertEquals(itcl.get(0).getTestMethods().get(0).getOperations().get(0).getOperands().get(3).getValue().getValueAsMap().get("h").getValueAsList().get(4).getValueAsString(), "s");
		 */
	}

	@Test
	public void testClassVariables() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load(
				"class hello {"
						+ "var i = 1;"
						+ "var i = '1';"
						+ "var i = {};"
						+ "var i = { name: 'Brad'};"
						+ "var i = brad;"
						+ "}");

		Assert.assertEquals(itcl.size(), 1);
		ETLTestClass tc = itcl.get(0);

		List<ETLTestVariable> varList = tc.getClassVariables();

		Assert.assertEquals(5, varList.size());

		ETLTestVariable var = varList.get(0);
		Assert.assertEquals("1", var.getValue().getValueAsString());
		Assert.assertEquals(ETLTestVariable.value_type.assignment, var.getValueType());
		Assert.assertEquals(ETLTestValueObject.value_type.literal, var.getValue().getValueType());

		var = varList.get(1);
		Assert.assertEquals("1", var.getValue().getValueAsString());
		Assert.assertEquals(ETLTestVariable.value_type.literal, var.getValueType());
		Assert.assertEquals(ETLTestValueObject.value_type.quoted_string, var.getValue().getValueType());

		var = varList.get(2);
		Assert.assertEquals(ETLTestVariable.value_type.literal, var.getValueType());
		Assert.assertEquals(ETLTestValueObject.value_type.object, var.getValue().getValueType());

		var = varList.get(3);
		Assert.assertEquals(ETLTestVariable.value_type.literal, var.getValueType());
		Assert.assertEquals(ETLTestValueObject.value_type.object, var.getValue().getValueType());
		Assert.assertEquals("Brad", var.getValue().getValueAsMap().get("name").getValueAsString());

		var = varList.get(4);
		Assert.assertEquals(ETLTestVariable.value_type.assignment, var.getValueType());
		Assert.assertEquals(ETLTestValueObject.value_type.literal, var.getValue().getValueType());
		Assert.assertEquals("brad", var.getValue().getValueAsString());
	}

	@Test
	public void ordinalsCount() throws ParseException
	{
		List<ETLTestClass> itcl = ETLTestParser.load(
				"class hello {"
						+ "@Test test(){a(); b(); c(); d();}}");

		Assert.assertEquals(itcl.size(), 1);

		ETLTestClass tc = itcl.get(0);

		List<ETLTestMethod> methods = tc.getTestMethods();
		Assert.assertEquals(methods.size(), 1);

		ETLTestMethod method = methods.get(0);

		List<ETLTestOperation> operations = method.getOperations();
		Assert.assertEquals(operations.size(), 4);

		Assert.assertEquals(0, operations.get(0).getOrdinal());
		Assert.assertEquals(10000, operations.get(0).createSibling("a.1").getOrdinal());
		Assert.assertEquals(20000, operations.get(0).createSibling("a.1").getOrdinal());
		Assert.assertEquals(1, operations.get(1).getOrdinal());
		Assert.assertEquals(10001, operations.get(1).createSibling("a.1").getOrdinal());
		Assert.assertEquals(20001, operations.get(1).createSibling("a.1").getOrdinal());
		Assert.assertEquals(2, operations.get(2).getOrdinal());
		Assert.assertEquals(10002, operations.get(2).createSibling("a.1").getOrdinal());
		Assert.assertEquals(20002, operations.get(2).createSibling("a.1").getOrdinal());
		Assert.assertEquals(3, operations.get(3).getOrdinal());
		Assert.assertEquals(10003, operations.get(3).createSibling("a.1").getOrdinal());
		Assert.assertEquals(20003, operations.get(3).createSibling("a.1").getOrdinal());
		Assert.assertEquals("[default].hello.test.a_1.30003", operations.get(3).createSibling("a_1").getQualifiedName());
	}
}
