package org.bitbucket.bradleysmithllc.etlunit.metadata.impl.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.impl.MetaDataContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;

public class MetaDataContextTest extends JsonSerializableBase
{
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void ser() throws IOException {
		MetaDataContext con = new MetaDataContextImpl("nombre");

		// create a couple of contexts
		con.createPackageContext(ETLTestPackageImpl.getDefaultPackage().getSubPackage("path"), MetaDataPackageContext.path_type.test_source);
		con.createPackageContext(ETLTestPackageImpl.getDefaultPackage().getSubPackage("path"), MetaDataPackageContext.path_type.feature_source);

		testJson(con);
	}

	@Test
	public void sameKeySameObject() throws IOException {
		MetaDataContext con = new MetaDataContextImpl("nombre");

		// same name and type == same object
		MetaDataPackageContext mpci = con.createPackageContext(ETLTestPackageImpl.getDefaultPackage().getSubPackage("path"), MetaDataPackageContext.path_type.test_source);
		Assert.assertSame(mpci, con.createPackageContext(ETLTestPackageImpl.getDefaultPackage().getSubPackage("path"), MetaDataPackageContext.path_type.test_source));
	}

	@Test
	public void samePathDifferentTypeDifferentObject() throws IOException {
		MetaDataContext con = new MetaDataContextImpl("nombre");

		MetaDataPackageContext mpci = con.createPackageContext(ETLTestPackageImpl.getDefaultPackage().getSubPackage("path"), MetaDataPackageContext.path_type.test_source);
		// same name different type == different object
		Assert.assertNotSame(mpci, con.createPackageContext(ETLTestPackageImpl.getDefaultPackage().getSubPackage("path"), MetaDataPackageContext.path_type.feature_source));
	}

	@Test
	public void noRuntimeSupport()
	{
		thrown.expect(IllegalStateException.class);
		thrown.expectMessage("Runtime support not available");
		new MetaDataContextImpl().createPackageContextForCurrentTest(MetaDataPackageContext.path_type.test_source);
	}

	@Override
	protected JsonSerializable newObject() {
		return new MetaDataContextImpl();
	}
}
