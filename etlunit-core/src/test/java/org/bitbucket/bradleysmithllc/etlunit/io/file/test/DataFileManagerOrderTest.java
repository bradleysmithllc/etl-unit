package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

@RunWith(Parameterized.class)
public class DataFileManagerOrderTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	private final TestSpec testSpec;

	@Before
	public void start() throws IOException {
		dfm = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	public DataFileManagerOrderTest(TestSpec spec)
	{
		testSpec = spec;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> data() throws Exception
	{
		Map<String, TestSpec> testSpecMap = new HashMap<String, TestSpec>();
		List<Object[]> testSpecArray = new ArrayList<Object[]>();

		URL url = DiffHarness.class.getResource("/dataFileOrder/TestSpecs.json");

		Assert.assertNotNull(url);

		JsonNode jsonTests = JsonUtils.loadJson(IOUtils.readURLToString(url));

		JsonNode node = jsonTests.get("tests");

		Assert.assertNotNull(node);
		Assert.assertTrue(node.isArray());

		ArrayNode anode = (ArrayNode) jsonTests.get("tests");

		for (int i = 0; i < anode.size(); i++)
		{
			TestSpec tSpec = new TestSpec();

			final JsonNode testNode = anode.get(i);

			Assert.assertNotNull(testNode);
			Assert.assertTrue(testNode.isObject());

			final JsonNode idNode = testNode.get("id");
			Assert.assertNotNull(idNode);
			Assert.assertTrue(idNode.isTextual());

			tSpec.id = idNode.asText();

			final JsonNode sourceNode = testNode.get("source");
			Assert.assertNotNull(sourceNode);
			Assert.assertTrue(sourceNode.isTextual());

			tSpec.source = sourceNode.asText();

			final JsonNode targetNode = testNode.get("ordered-results");
			Assert.assertNotNull(targetNode);
			Assert.assertTrue(targetNode.isTextual());

			tSpec.target = targetNode.asText();

			final JsonNode schemaNode = testNode.get("schema");
			Assert.assertNotNull(schemaNode);
			Assert.assertTrue(schemaNode.isTextual());

			tSpec.schema = schemaNode.asText();

			Assert.assertFalse("Test Spec not unique: " + tSpec.id, testSpecMap.containsKey(tSpec.id));

			testSpecMap.put(tSpec.id, tSpec);
			testSpecArray.add(new TestSpec[]{tSpec});
		}

		return testSpecArray;
	}

	//@Test
	public void orderedWriter() throws IOException
	{
		dfm.setDefaultFormatType(DataFileSchema.format_type.delimited);
		DataFileSchema sch = dfm.createDataFileSchema("test");

		DataFileSchema.Column col = sch.createColumn("ID1");
		sch.addColumn(col);

		col = sch.createColumn("ID2");
		sch.addColumn(col);

		File blah = temporaryFolder.newFile("blah");
		DataFile df = dfm.loadDataFile(blah, sch);

		DataFileWriter writer = df.writer();

		try
		{
			HashMap<String, Object> rowData = new HashMap<String, Object>();
			writer.addRow(rowData);

			rowData.put("ID1", "2");
			writer.addRow(rowData);

			rowData.put("ID1", "1");
			writer.addRow(rowData);

			rowData.put("ID1", "11");
			writer.addRow(rowData);

			rowData.put("ID2", "=7");
			rowData.put("ID1", null);
			writer.addRow(rowData);

			rowData.put("ID2", "=6");
			rowData.put("ID1", null);
			writer.addRow(rowData);
		}
		finally
		{
			writer.close();
		}

		Assert.assertEquals("", IOUtils.readFileToString(blah));
	}

	@Test
	public void testOne() throws IOException
	{
		System.out.println(testSpec);
		// locate the source and target files
		URL source = getClass().getResource("/dataFileOrder/" + testSpec.source);

		Assert.assertNotNull("Source data set not found: " + testSpec.id, source);

		URL target = getClass().getResource("/dataFileOrder/" + testSpec.target);

		Assert.assertNotNull("Target data set not found: " + testSpec.id, target);

		// get the schema
		DataFileSchema schemaSource = dfm.loadDataFileSchemaFromResource("dataFileOrder/" + testSpec.schema, testSpec.schema);

		// open the source and target and copy to temporary
		File source1 = temporaryFolder.newFile(testSpec.source);
		IOUtils.writeBufferToFile(source1, new StringBuffer(IOUtils.readURLToString(source)));

		String expected = IOUtils.readURLToString(target);

		File tempTarget = temporaryFolder.newFile(testSpec.source + ".copy");

		DataFile dfSource = dfm.loadDataFile(source1, schemaSource);
		DataFile dfTarget = dfm.loadDataFile(tempTarget, schemaSource);

		// stream from source into a new local file
		DataFileWriter writer = dfTarget.writer();

		try
		{
			DataFileReader rowData = dfSource.reader();

			try
			{
				Iterator<DataFileReader.FileRow> iter = rowData.iterator();

				while (iter.hasNext())
				{
					DataFileReader.FileRow fileRow = iter.next();

					writer.addRow((Map) fileRow.getData());
				}
			}
			finally
			{
				rowData.dispose();
			}
		}
		finally
		{
			writer.close();
		}

		Assert.assertEquals(testSpec.id, expected, IOUtils.readFileToString(tempTarget));
	}

	private static final class TestSpec
	{
		public String id;
		public String source;
		public String target;
		public String schema;

		@Override
		public String toString() {
			return "TestSpec{" +
					"id='" + id + '\'' +
					", source='" + source + '\'' +
					", target='" + target + '\'' +
					", schema='" + schema + '\'' +
					'}';
		}
	}
}
