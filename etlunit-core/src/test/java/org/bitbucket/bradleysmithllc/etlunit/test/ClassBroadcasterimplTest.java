package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassBroadcaster;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassBroadcasterImpl;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

public class ClassBroadcasterimplTest
{
	int testCount = 0;

	@Test
	public void broadcasting()
	{
		String file = getClass().getResource("/broadcastTests").getFile();
		File source = new File(file);

		source.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File file)
			{
				if (file.getName().endsWith(".etlunit"))
				{
					test(
							file,
							new File(file.getParentFile(), file.getName().substring(0, file.getName().length() - 7) + "reporter"),
							new File(file.getParentFile(), file.getName().substring(0, file.getName().length() - 7) + "events"),
							new File(file.getParentFile(), file.getName().substring(0, file.getName().length() - 7) + "scan_events"),
							new File(file.getParentFile(), file.getName().substring(0, file.getName().length() - 7) + "scan_reporter")
						);
				}

				return false;
			}

			private void test(File file, File reporter, File full_events, File scan_events, File scan_reporter)
			{
				Assert.assertTrue(file.getAbsolutePath(), file.exists());
				Assert.assertTrue(reporter.getAbsolutePath(), reporter.exists());
				Assert.assertTrue(full_events.getAbsolutePath(), full_events.exists());
				Assert.assertTrue(scan_events.getAbsolutePath(), scan_events.exists());
				Assert.assertTrue(scan_reporter.getAbsolutePath(), scan_reporter.exists());

				final StringBuilder buffer = new StringBuilder();
				final StringBuilder reporterBuffer = new StringBuilder();

				try
				{
					List<ETLTestClass> list = ETLTestParser.load(IOUtils.readFileToString(file));

					ClassBroadcaster broadcaster = new ClassBroadcasterImpl(new PassThroughClassLocatorImpl(list),
							new ClassDirector()
							{
								@Override
								public void beginBroadcast()
								{
									testCount = 0;
									buffer.append("classDirector.beginBroadcast\n");
								}

								@Override
								public response_code accept(ETLTestClass cl)
								{
									buffer.append("classDirector.accept(class): " + cl.getName() + "\n");
									return cl.getName().contains("skip") ? response_code.reject : response_code.accept;
								}

								@Override
								public response_code accept(ETLTestMethod mt)
								{
									buffer.append("classDirector.accept(method): " + mt.getName() + "\n");
									if (!mt.getName().contains("skip"))
									{
										testCount++;
										return response_code.accept;
									}
									else
									{
										return response_code.reject;
									}
								}

								@Override
								public response_code accept(ETLTestOperation op)
								{
									buffer.append("classDirector.accept(operation): " + op.getOperationName() + "\n");
									return op.getOperationName().contains("skip") ? response_code.reject : response_code.accept;
								}

								@Override
								public void endBroadcast()
								{
									buffer.append("classDirector.endBroadcast\n");
								}
							},
							new ClassListener()
							{
								@Override
								public void beginTests(VariableContext context, int executorId) {
									buffer.append("classListener.beginTests(exe)\n");
								}

								@Override
								public void beginTests(VariableContext context) throws TestExecutionError {
									buffer.append("classListener.beginTests()\n");
								}

								@Override
								public void beginPackage(ETLTestPackage name, VariableContext context, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
									buffer.append("classListener.beginPackage: " + name.getPackageName() + "\n");
								}

								@Override
								public void endPackage(ETLTestPackage name, VariableContext context, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
									buffer.append("classListener.endPackage: " + name.getPackageName() + "\n");
								}

								@Override
								public void endTests(VariableContext context, int executorId) {
									buffer.append("classListener.endTests(exe)\n");
								}

								@Override
								public void endTests(VariableContext context) {
									buffer.append("classListener.endTests()\n");
								}

								@Override
								public void begin(ETLTestClass cl, final VariableContext context, final int executor)
								{
									buffer.append("classListener.begin(class): " + cl.getName() + "\n");
								}

								@Override
								public void declare(ETLTestVariable var, final VariableContext context, final int executor)
								{
									buffer.append("classListener.declare: " + var.getName() + "\n");
								}

								@Override
								public void begin(ETLTestMethod mt, final VariableContext context, final int executor)
								{
									buffer.append("classListener.begin(method): " + mt.getName() + "\n");
								}

								@Override
								public void begin(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor)
										throws TestAssertionFailure, TestExecutionError, TestWarning
								{
									buffer.append("classListener.begin(operation): " + op.getOperationName() + "\n");
								}

								@Override
								public action_code process(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, final VariableContext context, ExecutionContext econtext, final int executor)
										throws TestAssertionFailure, TestExecutionError, TestWarning
								{
									String operationName = op.getOperationName();
									buffer.append("classListener.process(operation): " + operationName + "\n");

									if (operationName.equals("testAssertionFailure"))
									{
										throw new TestAssertionFailure(op.getOperands().getValueAsMap().get("message").getValueAsString());
									}
									else if (operationName.equals("testExecutionError"))
									{
										throw new TestExecutionError(op.getOperands().getValueAsMap().get("message").getValueAsString());
									}
									else if (operationName.equals("testWarning"))
									{
										throw new TestWarning(op.getOperands().getValueAsMap().get("message").getValueAsString());
									}

									return action_code.handled;
								}

								@Override
								public void end(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor)
										throws TestAssertionFailure, TestExecutionError, TestWarning
								{
									buffer.append("classListener.end(operation): " + op.getOperationName() + "\n");
								}

								@Override
								public void end(ETLTestMethod mt, final VariableContext context, final int executor)
								{
									buffer.append("classListener.end(method): " + mt.getName() + "\n");
								}

								@Override
								public void end(ETLTestClass cl, final VariableContext context, final int executor)
								{
									buffer.append("classListener.end(class): " + cl.getName() + "\n");
								}
							},
							new MapLocal(),
							new PrintWriterLog()
					);

					MyStatusReporter statusReporter = new MyStatusReporter(reporterBuffer);
					ETLTestCases etlCases = broadcaster.preScan(statusReporter);

					// assert on the pre scan events
					Assert.assertEquals(scan_events.getName(),
							EtlUnitStringUtils.convertEol(IOUtils.readFileToString(scan_events)),
							buffer.toString());

					Assert.assertEquals(scan_reporter.getName(),
							EtlUnitStringUtils.convertEol(IOUtils.readFileToString(scan_reporter)),
							reporterBuffer.toString());

					Assert.assertEquals("Test Count failed: " + file, testCount, etlCases.getTestCount());

					// clear the buffers for the actual test
					reporterBuffer.setLength(0);
					buffer.setLength(0);

					// broadcast for real
					broadcaster.broadcast(statusReporter, etlCases);

					Assert.assertEquals(full_events.getName(),
							EtlUnitStringUtils.convertEol(IOUtils.readFileToString(full_events)),
							buffer.toString());
					Assert.assertEquals(reporter.getName(),
							EtlUnitStringUtils.convertEol(IOUtils.readFileToString(reporter)),
							reporterBuffer.toString());
				}
				catch (Exception e)
				{
					throw new RuntimeException(e);
				}
			}
		});
	}

	private static class MyStatusReporter implements StatusReporter {
		private final StringBuilder reporterBuffer;

		public MyStatusReporter(StringBuilder reporterBuffer) {
			this.reporterBuffer = reporterBuffer;
		}

		@Override
		public void scanStarted()
		{
			reporterBuffer.append("statusReporter.scanStarted()\n");
		}

		@Override
		public void scanCompleted()
		{
			reporterBuffer.append("statusReporter.scanCompleted()\n");
		}

		@Override
		public void testsStarted(int numTestsSelected)
		{
			reporterBuffer.append("statusReporter.testsStarted(" + numTestsSelected + ")\n");
		}

		@Override
		public void testClassAccepted(ETLTestClass method)
		{
			reporterBuffer.append("statusReporter.testClassAccepted(" + method.getName() + ")\n");
		}

		@Override
		public void testMethodAccepted(ETLTestMethod method)
		{
			reporterBuffer.append("statusReporter.testMethodAccepted(" + method.getName() + ")\n");
		}

		@Override
		public void testBeginning(ETLTestMethod method)
		{
			reporterBuffer.append("statusReporter.testBeginning(" + method.getName() + ")\n");
		}

		@Override
		public void testCompleted(ETLTestMethod method, CompletionStatus status)
		{
			reporterBuffer.append("statusReporter.testCompleted(" + method.getName() + ", " + status.toString() + ")\n");
		}

		@Override
		public void testsCompleted()
		{
			reporterBuffer.append("statusReporter.testsCompleted()\n");
		}
	}
}
