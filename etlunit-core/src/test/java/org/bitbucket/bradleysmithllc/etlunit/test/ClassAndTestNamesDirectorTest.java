package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassBroadcasterImpl;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ClassAndTestNamesDirectorTest
{
	private ClassLocator getTestClasses() throws ParseException {
		List<ETLTestClass>
				list =
				ETLTestParser.load(
					"class class_a {@Test count1(){}@Test count2(){}@Test count3(){}} " +
					"class class_b {@Test count1(){}@Test count2(){}@Test count3(){}@Test count4(){}@Test count5(){}} " +
					"class class_c {@Test count1(){}@Test count2(){}@Test count3(){}@Test count4(){}@Test count5(){}@Test count6(){}@Test count7(){}} " +
					"class class_d {@Test count(){}}");

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		return impl;
	}

	@Test
	public void emptyList() throws ParseException
	{
		testClassNames(0);
	}

	@Test
	public void classBList() throws ParseException
	{
		testClassNames(5, "[default].class_b");
	}

	@Test
	public void classAList() throws ParseException
	{
		testClassNames(3, "[default].class_a");
	}

	@Test
	public void classCList() throws ParseException
	{
		testClassNames(7, "[default].class_c");
	}

	@Test
	public void classDList() throws ParseException
	{
		testClassNames(1, "[default].class_d");
	}

	@Test
	public void classAAndCList() throws ParseException
	{
		testClassNames(10, "[default].class_a", "[default].class_c");
	}

	@Test
	public void classAWildcard() throws ParseException
	{
		testClassNames(3, "[default].class_a");
	}

	private void testClassNames(int result, String... classes) throws ParseException
	{
		ClassAndTestNamesDirectorImpl pdirector = new ClassAndTestNamesDirectorImpl();

		for (String cls : classes)
		{
			pdirector.addClassNameWildcard(cls);
		}

		ClassBroadcasterImpl climpl = new ClassBroadcasterImpl(
				getTestClasses(),
				pdirector,
				new NullClassListener(),
				new MapLocal(),
				new PrintWriterLog()
		);

		ETLTestCases cases = climpl.preScan(null);
		climpl.broadcast(null, cases);
		Assert.assertEquals(result, cases.getTestCount());
	}

	@Test
	public void method1() throws ParseException
	{
		testMethodNames(1, "count1");
	}

	@Test
	public void method5() throws ParseException
	{
		testMethodNames(1, "count5");
	}

	@Test
	public void method1And5() throws ParseException
	{
		testMethodNames(2, "count1", "count5");
	}

	private void testMethodNames(int result, String... classes) throws ParseException
	{
		ClassAndTestNamesDirectorImpl pdirector = new ClassAndTestNamesDirectorImpl();

		for (String mth : classes)
		{
			pdirector.addClassName("[default].class_c").add(mth);
		}

		ClassBroadcasterImpl climpl = new ClassBroadcasterImpl(
				getTestClasses(),
				pdirector,
				new NullClassListener(),
				new MapLocal(),
				new PrintWriterLog()
		);

		ETLTestCases cases = climpl.preScan(null);
		climpl.broadcast(null, cases);
		Assert.assertEquals(result, cases.getTestCount());
	}
}
