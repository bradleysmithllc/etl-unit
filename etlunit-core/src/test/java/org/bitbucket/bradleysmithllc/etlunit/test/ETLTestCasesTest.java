package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ETLTestCases;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ETLTestCasesTest
{
	@Test
	public void testOne() throws Exception
	{
		// create the cases
		ETLTestCases cases = new ETLTestCases();

		List<ETLTestClass> classes = run(cases);

		List<ETLTestCases.ExecutorTestCases> caseMethods = cases.getEtlTestMethods();

		// class count should match the number of cases
		Assert.assertEquals(classes.size(), caseMethods.size());

		// verify order
		for (int i = 0; i < classes.size(); i++)
		{
			ETLTestClass etlC = classes.get(i);
			ETLTestCases.ExecutorTestCases casesE = caseMethods.get(i);

			Assert.assertEquals(etlC.getQualifiedName(), casesE.getEtlTestClass().getQualifiedName());
		}

		// verify that each list of methods matches
		for (ETLTestClass cls : classes)
		{
			ETLTestCases.ExecutorTestCases meths = cases.getTestCases(cls);

			Assert.assertSame(cls, meths.getEtlTestClass());
			Assert.assertEquals(cls.getTestMethods().size(), meths.getEtlTestMethods().size());
		}
	}

	private List<ETLTestClass> run(ETLTestCases cases) throws Exception {
		// add the test methods
		List<ETLTestClass> classes = ETLTestParser.load(IOUtils.readURLToString(getClass().getResource("/testCaseClasses")));

		cases.addTestClasses(classes);

		// in this case, there should be one executor with all methods and classes
		List<ETLTestCases.ExecutorTestCases> caseMethods = cases.getEtlTestMethods();

		Assert.assertEquals(9, cases.getTestCount());

		return classes;
	}
}
