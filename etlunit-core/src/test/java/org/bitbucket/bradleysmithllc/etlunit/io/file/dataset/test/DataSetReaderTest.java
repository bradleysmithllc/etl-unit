package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.DataSetReader;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSet;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.EOFException;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;

public class DataSetReaderTest {
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Test
	public void eofEmpty() throws IOException {
		expectedEx.expect(EOFException.class);
		expectedEx.expectMessage("Stream ended before token was reached");

		new DataSetReader(new PushbackReader(new StringReader(""))).read();
	}

	@Test
	public void eofNoToken() throws IOException {
		expectedEx.expect(EOFException.class);
		expectedEx.expectMessage("Stream ended before token was reached");

		new DataSetReader(new PushbackReader(new StringReader("Hello World"))).read(new char[42], 0, 42);
	}

	@Test
	public void tokenEnd() throws IOException {
		char[] cbuf = new char[20];
		new DataSetReader(new PushbackReader(new StringReader("Hello World" + ReaderDataSet.token))).read(cbuf, 0, cbuf.length);

		Assert.assertEquals("Hello World", new String(cbuf, 0, 11));
	}

	@Test
	public void tokenMiddle() throws IOException {
		char[] cbuf = new char[20];
		new DataSetReader(new PushbackReader(new StringReader("Hello World" + ReaderDataSet.token + "f"))).read(cbuf, 0, cbuf.length);

		Assert.assertEquals("Hello World", new String(cbuf, 0, 11));
	}

	@Test
	public void tokenBeginning() throws IOException {
		char[] cbuf = new char[11];
		Assert.assertEquals(-1, new DataSetReader(new PushbackReader(new StringReader(ReaderDataSet.token + "flyAway"))).read(cbuf, 0, cbuf.length));
	}

	@Test
	public void readReturn() throws IOException {
		char[] cbuf = new char[20];
		Assert.assertEquals(11, new DataSetReader(new PushbackReader(new StringReader("Hello World" + ReaderDataSet.token + "f"))).read(cbuf, 0, cbuf.length));

		Assert.assertEquals("Hello World", new String(cbuf, 0, 11));
	}

	@Test
	public void readReturnTruncated() throws IOException {
		char[] cbuf = new char[3];
		DataSetReader dataSetReader = new DataSetReader(new PushbackReader(new StringReader("Hello World" + ReaderDataSet.token + "f")));

		Assert.assertEquals(3, dataSetReader.read(cbuf, 0, cbuf.length));
		Assert.assertEquals("Hel", new String(cbuf));

		Assert.assertEquals(3, dataSetReader.read(cbuf, 0, cbuf.length));
		Assert.assertEquals("lo ", new String(cbuf));

		Assert.assertEquals(3, dataSetReader.read(cbuf, 0, cbuf.length));
		Assert.assertEquals("Wor", new String(cbuf));

		Assert.assertEquals(2, dataSetReader.read(cbuf, 0, cbuf.length));
		Assert.assertEquals("ld", new String(cbuf, 0, 2));
	}

	@Test
	public void justRight() throws IOException {
		new DataSetReader(new PushbackReader(new StringReader("" + ReaderDataSet.token))).read(new char[10], 0, 10);
	}

	@Test
	public void arrayNull() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Buffer cannot be null");

		new DataSetReader(new PushbackReader(new StringReader(""))).read(null, 0, 5);
	}

	@Test
	public void length0() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Length may not be 0");

		new DataSetReader(new PushbackReader(new StringReader(""))).read(null, 0, 0);
	}

	@Test
	public void lengthGreaterThanArray() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Offset + length cannot be greater than length of array");

		new DataSetReader(new PushbackReader(new StringReader(""))).read(new char[5], 1, 5);
	}

	@Test
	public void offsetGreaterThanArray() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Offset cannot be greater than length of array");

		new DataSetReader(new PushbackReader(new StringReader(""))).read(new char[1], 1, 1);
	}

	@Test
	public void offsetPLusLengthGreaterThanArray() throws IOException {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Offset + length cannot be greater than length of array");

		new DataSetReader(new PushbackReader(new StringReader(""))).read(new char[5], 3, 4);
	}

	@Test
	public void prodIssue() throws IOException {
		char[] cbuf = new char[5];
		Assert.assertEquals(4, new DataSetReader(new PushbackReader(new StringReader("_\n_\n" + ReaderDataSet.token), 1024)).read(cbuf, 0, cbuf.length));

		Assert.assertEquals("_\n_\n", new String(cbuf, 0, 4));
	}
}
