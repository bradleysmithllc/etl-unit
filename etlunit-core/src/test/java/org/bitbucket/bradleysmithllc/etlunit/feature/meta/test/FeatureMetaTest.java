package org.bitbucket.bradleysmithllc.etlunit.feature.meta.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureMetaInfo;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureOperation;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOptionDescriptor;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class FeatureMetaTest
{
	@Test
	public void configurationReadFromFile()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		Assert.assertNotNull(info.getFeatureConfiguration());
		Assert.assertEquals("{\"useful-module\": \"Rats\", \"required\": true}", info.getFeatureConfiguration());
	}

	@Test
	public void featureOptionsReadFromFile()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		Assert.assertTrue(info.isInternalFeature());

		String desc = info.getFeatureUsage();

		Assert.assertNotNull(desc);
		Assert.assertEquals("This is how you use me", desc);
	}

	@Test
	public void operationsFromFile()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		Map<String, FeatureOperation> exportedOperations = info.getExportedOperations();
		Assert.assertNotNull(exportedOperations);
		Assert.assertEquals(6, exportedOperations.size());

		Assert.assertTrue(exportedOperations.containsKey("assert"));
		Assert.assertTrue(exportedOperations.containsKey("connect"));
		Assert.assertTrue(exportedOperations.containsKey("execute"));
		Assert.assertTrue(exportedOperations.containsKey("process"));
		Assert.assertTrue(exportedOperations.containsKey("stage"));
		Assert.assertTrue(exportedOperations.containsKey("meta"));
	}

	@Test
	public void operationsUsageFromFile()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		FeatureOperation desc = info.getExportedOperations().get("assert");
		Assert.assertNotNull(desc);
		Assert.assertEquals("?", desc.getDescription());

		desc = info.getExportedOperations().get("connect");
		Assert.assertNotNull(desc);
		Assert.assertEquals("What I Also Do", desc.getDescription());

		desc = info.getExportedOperations().get("execute1");
		Assert.assertNull(desc);
	}

	@Test
	public void operationDescriptionsFromFile()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		FeatureOperation desc = info.getExportedOperations().get("stage");
		Assert.assertNotNull(desc);
		Assert.assertEquals("What I do", desc.getDescription());

		desc = info.getExportedOperations().get("connect");
		Assert.assertNotNull(desc);
		Assert.assertEquals("What I Also Do", desc.getDescription());

		desc = info.getExportedOperations().get("process");
		Assert.assertNotNull(desc);
		Assert.assertEquals("What Else", desc.getDescription());

		desc = info.getExportedOperations().get("execute");
		Assert.assertNotNull(desc);
		Assert.assertEquals("But me", desc.getDescription());

		desc = info.getExportedOperations().get("assert");
		Assert.assertNotNull(desc);
		Assert.assertEquals("?", desc.getDescription());
	}

	@Test
	public void operationsPrototypeFromFile()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		FeatureOperation desc = info.getExportedOperations().get("assert");
		Assert.assertNotNull(desc);
		Assert.assertEquals("assert prototype", desc.getPrototype());

		desc = info.getExportedOperations().get("connect");
		Assert.assertNotNull(desc);
		Assert.assertEquals("connect prototype", desc.getPrototype());

		desc = info.getExportedOperations().get("execute");
		Assert.assertNull(desc.getPrototype());
	}

	@Test
	public void options()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		List<RuntimeOptionDescriptor> optionList = info.getOptions();

		Assert.assertNotNull(optionList);

		Assert.assertEquals(5, optionList.size());

		Assert.assertEquals("loadWorkflow", optionList.get(0).getName());
		Assert.assertEquals("exportWorkflows", optionList.get(1).getName());
		Assert.assertEquals("exportData", optionList.get(2).getName());
		Assert.assertEquals("retryCount", optionList.get(3).getName());
		Assert.assertEquals("retryScope", optionList.get(4).getName());

		Assert.assertEquals("Determines whether the runtime will use the workflows in the repository.",
				optionList.get(0).getDescription());
		Assert.assertEquals(
				"If enabled, the runtime will export every workflow from the repository to the project if the test succeeds.",
				optionList.get(1).getDescription());
		Assert.assertEquals("If enabled, the runtime will export every dataset in the test.",
				optionList.get(2).getDescription());
		Assert.assertEquals("If enabled, the runtime will export every dataset in the test.",
				optionList.get(3).getDescription());
		Assert.assertEquals("If enabled, the runtime will export every dataset in the test.",
				optionList.get(4).getDescription());

		Assert.assertEquals(RuntimeOptionDescriptor.option_type.bool, optionList.get(0).getOptionType());
		Assert.assertEquals(RuntimeOptionDescriptor.option_type.bool, optionList.get(1).getOptionType());
		Assert.assertEquals(RuntimeOptionDescriptor.option_type.bool, optionList.get(2).getOptionType());
		Assert.assertEquals(RuntimeOptionDescriptor.option_type.integer, optionList.get(3).getOptionType());
		Assert.assertEquals(RuntimeOptionDescriptor.option_type.string, optionList.get(4).getOptionType());

		Assert.assertEquals(true, optionList.get(0).getDefaultBooleanValue());
		Assert.assertEquals(false, optionList.get(1).getDefaultBooleanValue());
		Assert.assertEquals(true, optionList.get(2).getDefaultBooleanValue());
		Assert.assertEquals(10, optionList.get(3).getDefaultIntegerValue());
		Assert.assertEquals("compile", optionList.get(4).getDefaultStringValue());
	}

	@Test(expected = IllegalArgumentException.class)
	public void optionsFailBadTypeInteger()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		List<RuntimeOptionDescriptor> optionList = info.getOptions();

		Assert.assertNotNull(optionList);

		Assert.assertEquals(5, optionList.size());

		Assert.assertEquals(RuntimeOptionDescriptor.option_type.bool, optionList.get(0).getOptionType());

		optionList.get(0).getDefaultIntegerValue();
	}

	@Test(expected = IllegalArgumentException.class)
	public void optionsFailBadTypeString()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		List<RuntimeOptionDescriptor> optionList = info.getOptions();

		Assert.assertNotNull(optionList);

		Assert.assertEquals(5, optionList.size());

		Assert.assertEquals(RuntimeOptionDescriptor.option_type.bool, optionList.get(0).getOptionType());

		optionList.get(0).getDefaultStringValue();
	}

	@Test(expected = IllegalArgumentException.class)
	public void optionsFailBadTypeBoolean()
	{
		MetaFeatureModule mfm = new MetaFeatureModule();

		FeatureMetaInfo info = mfm.getMetaInfo();

		Assert.assertNotNull(info);

		List<RuntimeOptionDescriptor> optionList = info.getOptions();

		Assert.assertNotNull(optionList);

		Assert.assertEquals(5, optionList.size());

		Assert.assertEquals(RuntimeOptionDescriptor.option_type.integer, optionList.get(3).getOptionType());

		optionList.get(3).getDefaultBooleanValue();
	}
}
