package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class EchoPlusPlus
{
	public static void main(String[] argv) throws IOException
	{
		BufferedInputStream bin = new BufferedInputStream(System.in);
		PrintStream bout = new PrintStream(new BufferedOutputStream(System.out));

		while (true)
		{
			int i = bin.read();

			if (i == -1)
			{
				throw new IOException();
			}

			if (i == '|')
			{
				StringBuffer repeatBuff = new StringBuffer();

				// read a line
				while ((i = bin.read()) != '|')
				{
					repeatBuff.append((char) i);
				}

				StringBuffer textBuff = new StringBuffer();

				// read text to repeat
				while ((i = bin.read()) != '|')
				{
					textBuff.append((char) i);
				}

				// blabber back a response
				int repeats = Integer.parseInt(repeatBuff.toString());

				for (int r = 0; r < repeats; r++)
				{
					bout.print('>');
					bout.print(textBuff.toString());
					bout.print('<');
					bout.flush();
				}
			}
			else
			{
				throw new IOException();
			}
		}
	}
}
