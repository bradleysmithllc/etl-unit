package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ExecutionContext;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.ResourceFeatureOperationSignature;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

/**
 * Test that the function which classifies methods by interface type works
 */
public class ResourceFeatureOperationSignatureMethodTypeTest
{
	@Test
	public void testInvalid()
	{
		test("invalidMethod", Object.class, ResourceFeatureOperationSignature.methodType.invalid);
	}

	@Test
	public void testInvalid2()
	{
		test("invalidMethod2", Object.class, ResourceFeatureOperationSignature.methodType.invalid);
	}

	@Test
	public void testLacksExecutor()
	{
		test("lacksExecutor", Float.class, ResourceFeatureOperationSignature.methodType.lacksExecutor);
	}

	@Test
	public void testLacksExecutorMissedType()
	{
		test("lacksExecutor", Long.class, ResourceFeatureOperationSignature.methodType.invalid);
	}

	@Test
	public void testRequiresExecutor()
	{
		test("requiresExecutor", Float.class, ResourceFeatureOperationSignature.methodType.requiresExecutor);
	}

	@Test
	public void testRequiresExecutorMissedType()
	{
		test("requiresExecutor", Long.class, ResourceFeatureOperationSignature.methodType.invalid);
	}

	@Test
	public void testRequiresExecutorInteger()
	{
		test("requiresExecutorInteger", Float.class, ResourceFeatureOperationSignature.methodType.invalid);
	}

	@Test
	public void testRequiresExecutorIntegerMissedType()
	{
		test("requiresExecutorInteger", Long.class, ResourceFeatureOperationSignature.methodType.invalid);
	}

	private void test(String invalidMethod, Class clType, ResourceFeatureOperationSignature.methodType invalid) {
		for (Method method : getClass().getDeclaredMethods())
		{
			if (method.getName().equals(invalidMethod))
			{
				// test this one
				Assert.assertEquals(invalid, ResourceFeatureOperationSignature.inspectOperationProcessorMethod(method, clType));
				return;
			}
		}

		Assert.fail("Bad test");
	}

	/**
	 * Method matching no executor
	 */
	public void lacksExecutor(Float userType, ETLTestMethod method, ETLTestOperation operation, ETLTestValueObject value, VariableContext vc, ExecutionContext ec){}

	/**
	 * Method matching no executor
	 */
	public void requiresExecutor(Float userType, ETLTestMethod method, ETLTestOperation operation, ETLTestValueObject value, VariableContext vc, ExecutionContext ec, int executor){}

	/**
	 * Method matching no executor
	 */
	public void requiresExecutorInteger(Float userType, ETLTestMethod method, ETLTestOperation operation, ETLTestValueObject value, VariableContext vc, ExecutionContext ec, Integer executor){}

	/**
	 * Method testing no parameters
	 */
	public void invalidMethod(){}

	/**
	 * Method testing arbitrary parameters
	 */
	public void invalidMethod2(String a, String b, String c, String d, String e, String f, String g, String h){}
}
