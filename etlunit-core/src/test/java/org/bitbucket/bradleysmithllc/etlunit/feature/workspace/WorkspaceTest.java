package org.bitbucket.bradleysmithllc.etlunit.feature.workspace;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class WorkspaceTest extends BaseFeatureModuleTest
{
	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSource("java.etlunit", "class test {@Test t(){} @Test f(){}}");
	}

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides() {
		return Arrays.asList(new RuntimeOption("workspace.purgeWorkspace", identifier.equals("purge") ? true : false));
	}

	//@Test
	public void purgeFiles()
	{
		TestResults res = startTest("purge");

		Assert.assertEquals(0, res.getMetrics().getNumberOfErrors());
	}

	//@Test
	public void wontPurgeFiles()
	{
		TestResults res = startTest("nopurge");

		Assert.assertEquals(0, res.getMetrics().getNumberOfErrors());
	}

	@Override
	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context) {
		Assert.assertTrue(context.hasVariableBeenDeclared("workspace"));

		// assert workspace empty
		String path = context.contextualize("${workspace.root.absolutePath}");
		Assert.assertNotNull(path);

		path = context.contextualize("${workspace.rootPath}");
		Assert.assertNotNull(path);

		File file = new File(path);

		if (mt.getName().equals("t"))
		{
			// this is the first method.  Always clear
			Assert.assertEquals(0, file.list().length);
		}
		else
		{
			// every pass must be empty if we are purging
			if (identifier.equals("purge"))
			{
				Assert.assertEquals(0, file.list().length);
			}
			else
			{
				Assert.assertNotEquals(0, file.list().length);
			}
		}

		// create some files
		try {
			FileUtils.touch(new File(file, "test"));
			FileUtils.touch(new File(file, "test1"));
			FileUtils.touch(new File(file, "test2"));
			FileUtils.touch(new File(file, "test3"));

			file = new File(file, "dir");
			FileUtils.forceMkdir(file);

			FileUtils.touch(new File(file, "test"));
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed) {
		String path = context.contextualize("${workspace.rootPath}");
		Assert.assertNotNull(path);

		File file = new File(path);

		Assert.assertTrue(new File(file, "test").exists());
		Assert.assertTrue(new File(file, "test1").exists());
		Assert.assertTrue(new File(file, "test2").exists());
		Assert.assertTrue(new File(file, "test3").exists());

		Assert.assertTrue(new File(file, "dir").exists());
		Assert.assertTrue(new File(file, "dir").isDirectory());

		Assert.assertTrue(new File(new File(file, "dir"), "test").exists());

		// create some more to get whacked before next run
		try {
			FileUtils.touch(new File(file, "test4"));
		} catch (IOException e) {
			Assert.fail();
		}
	}

	@Override
	protected boolean multiPassSafe() {
		return false;
	}
}
