package org.bitbucket.bradleysmithllc.etlunit.metadata.impl.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifactContent;
import org.bitbucket.bradleysmithllc.etlunit.metadata.impl.MetaDataArtifactImpl;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class MetaDataArtifactTest extends JsonSerializableBase
{
	@Test
	public void metaJson() throws IOException {
		MetaDataArtifactImpl jserRef = new MetaDataArtifactImpl(
				"relative",
				"project",
				new File("absolute")
		);
		// add content
		jserRef.createContent("blah1");
		jserRef.createContent("blah2");
		jserRef.createContent("blah3");

		testJson(
				jserRef
		);
	}

	@Test
	public void sameArtifactNameGivesSameObject()
	{
		MetaDataArtifactImpl jserRef = new MetaDataArtifactImpl(
				"relative",
				"project",
				new File("absolute")
		);

		MetaDataArtifactContent blah = jserRef.createContent("blah");
		Assert.assertSame(blah, jserRef.createContent("blah"));
	}

	@Override
	protected JsonSerializable newObject() {
		return new MetaDataArtifactImpl();
	}
}
