package org.bitbucket.bradleysmithllc.etlunit.parser.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class ETLTestValueObjectBuilderTest
{
	@Test(expected = IllegalStateException.class)
	public void builderNeedsContext()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.toObject();
	}

	@Test(expected = IllegalStateException.class)
	public void removeKeyNeedsContext()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.removeKey("");
	}

	@Test(expected = IllegalStateException.class)
	public void keyNeedsContext()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.key("");
	}

	@Test(expected = IllegalStateException.class)
	public void cantRemoveCurrentKey()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.key("A").removeKey("A");
	}

	@Test(expected = IllegalStateException.class)
	public void dontCallKeyTwiceInARow()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.object().key("").key("");
	}

	@Test(expected = IllegalStateException.class)
	public void valueNeedsContext()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.value("");
	}

	@Test(expected = IllegalStateException.class)
	public void pojoValueNeedsContext()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.pojoValue(builder);
	}

	@Test(expected = IllegalStateException.class)
	public void jsonValueNeedsContext()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.jsonValue("{}");
	}

	@Test(expected = IllegalStateException.class)
	public void listsDoNotHaveKeys()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.list().key("");
	}

	@Test(expected = IllegalStateException.class)
	public void listsDoNotRemoveKeys()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();

		builder.list().removeKey("");
	}

	@Test
	public void addKeyThenRemove()
	{
		ETLTestValueObject obj = new ETLTestValueObjectBuilder()
				.object()
				.key("key")
				.value("Hello1")
				.key("key2")
				.value("Hello2")
				.removeKey("key")
				.endObject()
				.toObject();

		Assert.assertEquals(ETLTestValueObject.value_type.object, obj.getValueType());
		Assert.assertEquals(1, obj.getValueAsMap().size());
		Assert.assertEquals("Hello2", obj.getValueAsMap().get("key2").getValueAsString());
		Assert.assertNull(obj.getValueAsMap().get("key"));
	}

	@Test
	public void simpleList()
	{
		ETLTestValueObject obj = new ETLTestValueObjectBuilder()
				.list()
				.value("Hello1")
				.value("Hello2")
				.value("Hello3")
				.endList()
				.toObject();

		Assert.assertEquals(ETLTestValueObject.value_type.list, obj.getValueType());
		Assert.assertEquals(3, obj.getValueAsListOfStrings().size());
		Assert.assertEquals("Hello1", obj.getValueAsListOfStrings().get(0));
		Assert.assertEquals("Hello2", obj.getValueAsListOfStrings().get(1));
		Assert.assertEquals("Hello3", obj.getValueAsListOfStrings().get(2));
	}

	@Test
	public void simpleMap()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.object();
		builder = builder.key("Hello");
		builder = builder.value("Hello1");
		builder = builder.endObject();
		ETLTestValueObject obj = builder.toObject();

		Assert.assertEquals(ETLTestValueObject.value_type.object, obj.getValueType());
		Assert.assertEquals(1, obj.getValueAsMap().size());
		Assert.assertEquals("Hello1", obj.getValueAsMap().get("Hello").getValueAsString());
	}

	@Test(expected = IllegalStateException.class)
	public void duplicateKeyValues()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.object();
		builder = builder.key("key1");
		builder = builder.value("value1");
		builder = builder.key("key1");
	}

	@Test
	public void allOverThePlace()
	{
		Object pojo = new Object();

		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.object();
		builder = builder.key("key1");
		builder = builder.value("value1");
		builder = builder.key("key2");
		builder = builder.list();
		builder = builder.value("listvalue1");
		builder = builder.value("listvalue2");
		builder = builder.value("listvalue3");
		builder = builder.jsonValue("{happy: 'days'}");
		builder = builder.pojoValue(pojo);
		builder = builder.endList();
		builder = builder.endObject();

		ETLTestValueObject obj = builder.toObject();

		Assert.assertEquals(ETLTestValueObject.value_type.object, obj.getValueType());
		Assert.assertEquals(2, obj.getValueAsMap().size());

		Assert.assertNotNull(obj.query("key1"));
		Assert.assertEquals("value1", obj.query("key1").getValueAsString());

		ETLTestValueObject key2 = obj.query("key2");
		Assert.assertNotNull(key2);

		Assert.assertEquals(ETLTestValueObject.value_type.list, key2.getValueType());

		List<ETLTestValueObject> valueAsList = key2.getValueAsList();
		Assert.assertEquals(5, valueAsList.size());
		Assert.assertEquals("listvalue1", valueAsList.get(0).getValueAsString());
		Assert.assertEquals("listvalue2", valueAsList.get(1).getValueAsString());
		Assert.assertEquals("listvalue3", valueAsList.get(2).getValueAsString());

		ETLTestValueObject listObject = valueAsList.get(3);
		Assert.assertEquals(ETLTestValueObject.value_type.object, listObject.getValueType());

		ETLTestValueObject happy = listObject.query("happy");
		Assert.assertNotNull(happy);
		Assert.assertEquals("days", happy.getValueAsString());

		ETLTestValueObject pojoObject = valueAsList.get(4);
		Assert.assertEquals(ETLTestValueObject.value_type.pojo, pojoObject.getValueType());
		Assert.assertEquals(pojo, pojoObject.getValueAsPojo());
	}

	@Test(expected = IllegalStateException.class)
	public void unbalancedEndsListToObject()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.object();
		builder = builder.key("key1");
		builder = builder.list();
		builder = builder.endObject();
	}

	@Test(expected = IllegalStateException.class)
	public void unbalancedEndsObjectToList()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.list();
		builder = builder.object();
		builder = builder.key("key1");
		builder = builder.value("key1");
		builder = builder.endList();
	}

	@Test(expected = IllegalStateException.class)
	public void danglingObjectKeys()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.object();
		builder = builder.key("key1");
		builder = builder.endObject();
	}

	@Test(expected = IllegalStateException.class)
	public void danglingListKeys()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.object();
		builder = builder.key("key1");
		builder = builder.endList();
	}

	@Test(expected = IllegalStateException.class)
	public void unbalancedEndsObject()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.object();
		builder = builder.endList();
	}

	@Test(expected = IllegalStateException.class)
	public void unbalancedEndsList()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.list();
		builder = builder.endObject();
	}

	@Test(expected = IllegalArgumentException.class)
	public void cantExtendAStringValue()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder(new ETLTestValueObjectImpl("Hi"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void cantExtendAPojoValue()
	{
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder(new ETLTestValueObjectImpl(new Object()));
	}

	@Test
	public void extendAListValue() throws ParseException
	{
		ETLTestValueObject list = new ETLTestValueObjectBuilder()
				.list()
				.value("a")
				.value("b")
				.endList()
				.toObject();

		ETLTestValueObject builder = new ETLTestValueObjectBuilder(list)
				.value("Hello1")
				.value("Hello2")
				.value("Hello3")
				.endList()
				.toObject();

		List<String> listValueAsListOfStrings = list.getValueAsListOfStrings();
		List<String> builderValueAsListOfStrings = builder.getValueAsListOfStrings();

		Assert.assertEquals(listValueAsListOfStrings.get(0), builderValueAsListOfStrings.get(0));
		Assert.assertEquals(listValueAsListOfStrings.get(1), builderValueAsListOfStrings.get(1));

		Assert.assertEquals(listValueAsListOfStrings.get(2), builderValueAsListOfStrings.get(2));
		Assert.assertEquals(listValueAsListOfStrings.get(3), builderValueAsListOfStrings.get(3));
		Assert.assertEquals(listValueAsListOfStrings.get(4), builderValueAsListOfStrings.get(4));
	}

	@Test
	public void extendAMapValue() throws ParseException
	{
		ETLTestValueObject map = ETLTestParser.loadObject("{a: 'a', b: 'b'}");

		ETLTestValueObject builder = new ETLTestValueObjectBuilder(map)
				.key("c")
				.value("c")
				.endObject()
				.toObject();

		Map<String, ETLTestValueObject> mapValue = builder.getValueAsMap();
		Map<String, ETLTestValueObject> builderMap = map.getValueAsMap();

		Assert.assertEquals(mapValue.get("a").getValueAsString(), builderMap.get("a").getValueAsString());
		Assert.assertEquals(mapValue.get("b").getValueAsString(), builderMap.get("b").getValueAsString());
		Assert.assertEquals(mapValue.get("c").getValueAsString(), builderMap.get("c").getValueAsString());
	}

	@Test
	public void extendAMapValueAndRemoveKeys() throws ParseException
	{
		ETLTestValueObject map = ETLTestParser.loadObject("{a: 'a', b: 'b'}");

		ETLTestValueObject builder = new ETLTestValueObjectBuilder(map)
				.key("c")
				.value("c")
				.removeKey("a")
				.endObject()
				.toObject();

		Map<String, ETLTestValueObject> mapValue = builder.getValueAsMap();
		Map<String, ETLTestValueObject> builderMap = map.getValueAsMap();

		Assert.assertNull(mapValue.get("a"));
		Assert.assertNull(builderMap.get("a"));
		Assert.assertEquals(mapValue.get("b").getValueAsString(), builderMap.get("b").getValueAsString());
		Assert.assertEquals(mapValue.get("c").getValueAsString(), builderMap.get("c").getValueAsString());
	}
}
