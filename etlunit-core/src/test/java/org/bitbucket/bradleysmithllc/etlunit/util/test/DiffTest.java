package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.diff_match_patch;
import org.junit.Test;

public class DiffTest
{
	@Test
	public void diff()
	{
		// Create an instance of the diff_match_patch object.
		diff_match_patch dmp = new diff_match_patch();

		System.out.println(dmp.diff_main("abc", "abc"));
		System.out.println(dmp.diff_main("abcd", "abed"));
	}
}
