package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ClassLocator;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureClassLocatorProxy;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FeatureClassLocatorProxyTest
{
	static class CountingFeature extends AbstractFeature
	{
		private final int limit;
		private int current;

		CountingFeature(int limit)
		{
			this.limit = limit;
		}

		@Override
		public String getFeatureName()
		{
			return "tester[" + limit + "]";
		}

		@Override
		public ClassLocator getLocator()
		{
			return new ClassLocator()
			{
				@Override
				public void reset()
				{
					current = 0;
				}

				@Override
				public boolean hasNext()
				{
					return current < limit;
				}

				@Override
				public ETLTestClass next()
				{
					if (current >= limit)
					{
						Assert.fail("Iterator moved beyond the end");
					}

					current++;
					return null;
				}

				@Override
				public void remove()
				{
					throw new RuntimeException("Must never be called");
				}
			};
		}
	}

	@Test
	public void proxyIteratesEmpty()
	{
		List<Feature> features = new ArrayList<Feature>();

		FeatureClassLocatorProxy fclp = new FeatureClassLocatorProxy(features);

		fclp.reset();

		while (fclp.hasNext())
		{
			Assert.fail();
		}
	}

	@Test
	public void proxyIteratesProperly()
	{
		List<Feature> features = new ArrayList<Feature>();

		features.add(new CountingFeature(0));
		features.add(new CountingFeature(10));

		FeatureClassLocatorProxy fclp = new FeatureClassLocatorProxy(features);

		for (int i = 0; i < 100; i++)
		{
			fclp.reset();

			int j = 0;

			while (fclp.hasNext())
			{
				j++;
				fclp.next();
			}

			Assert.assertEquals(10, j);
		}

		features.add(new CountingFeature(0));
		features.add(new CountingFeature(8));
		features.add(new CountingFeature(0));

		fclp = new FeatureClassLocatorProxy(features);

		for (int i = 0; i < 100; i++)
		{
			fclp.reset();

			int j = 0;

			while (fclp.hasNext())
			{
				j++;
				fclp.next();
			}

			Assert.assertEquals(18, j);
		}

		features.add(new CountingFeature(0));
		features.add(new CountingFeature(9));
		features.add(new CountingFeature(0));

		fclp = new FeatureClassLocatorProxy(features);

		for (int i = 0; i < 100; i++)
		{
			fclp.reset();

			int j = 0;

			while (fclp.hasNext())
			{
				j++;
				fclp.next();
			}

			Assert.assertEquals(27, j);
		}

		features.add(new CountingFeature(0));
		features.add(new CountingFeature(0));
		features.add(new CountingFeature(0));

		fclp = new FeatureClassLocatorProxy(features);

		for (int i = 0; i < 100; i++)
		{
			fclp.reset();

			int j = 0;

			while (fclp.hasNext())
			{
				j++;
				fclp.next();
			}

			Assert.assertEquals(27, j);
		}
	}
}
