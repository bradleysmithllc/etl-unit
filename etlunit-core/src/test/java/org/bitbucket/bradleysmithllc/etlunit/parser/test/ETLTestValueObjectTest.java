package org.bitbucket.bradleysmithllc.etlunit.parser.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObjectImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class ETLTestValueObjectTest
{
	@Test
	public void mergeObjectHierarchies() throws ParseException
	{
		ETLTestValueObject
				etlTestValueObjectA =
				ETLTestParser.loadObject(
						"{a: 'a', c: 'c', d: ['1', '2', '3'], map: {a: 'map.a', b: 'map.b', c: 'map.c', map: {f: 'map.map.f'}}}");
		ETLTestValueObject
				etlTestValueObjectB =
				ETLTestParser.loadObject("{b: 'b', map: {d: 'map.d', map: {g: 'map.map.g'}}}");
		ETLTestValueObject mergeRes =
				etlTestValueObjectA.merge(
						etlTestValueObjectB
																 );

		Assert.assertNull(etlTestValueObjectA.query("b"));
		Assert.assertNotNull(etlTestValueObjectA.query("a"));
		Assert.assertNotNull(etlTestValueObjectA.query("c"));
		Assert.assertNotNull(etlTestValueObjectA.query("d"));
		Assert.assertNotNull(etlTestValueObjectA.query("map"));
		Assert.assertNotNull(etlTestValueObjectA.query("map.a"));
		Assert.assertNotNull(etlTestValueObjectA.query("map.b"));
		Assert.assertNotNull(etlTestValueObjectA.query("map.c"));
		Assert.assertNotNull(etlTestValueObjectA.query("map.map"));
		Assert.assertNotNull(etlTestValueObjectA.query("map.map.f"));
		Assert.assertEquals("a", etlTestValueObjectA.query("a").getValueAsString());
		Assert.assertEquals("c", etlTestValueObjectA.query("c").getValueAsString());

		Assert.assertEquals("map.a", etlTestValueObjectA.query("map.a").getValueAsString());
		Assert.assertEquals("map.b", etlTestValueObjectA.query("map.b").getValueAsString());
		Assert.assertEquals("map.c", etlTestValueObjectA.query("map.c").getValueAsString());
		Assert.assertEquals("map.map.f", etlTestValueObjectA.query("map.map.f").getValueAsString());

		Assert.assertNull(etlTestValueObjectB.query("a"));
		Assert.assertNull(etlTestValueObjectB.query("c"));
		Assert.assertNull(etlTestValueObjectB.query("d"));
		Assert.assertNotNull(etlTestValueObjectB.query("b"));
		Assert.assertNotNull(etlTestValueObjectA.query("map"));
		Assert.assertNotNull(etlTestValueObjectB.query("map.d"));
		Assert.assertNotNull(etlTestValueObjectB.query("map.map"));
		Assert.assertNotNull(etlTestValueObjectB.query("map.d"));
		Assert.assertNotNull(etlTestValueObjectB.query("map.map.g"));
		Assert.assertEquals("b", etlTestValueObjectB.query("b").getValueAsString());

		Assert.assertEquals("map.d", etlTestValueObjectB.query("map.d").getValueAsString());
		Assert.assertEquals("map.map.g", etlTestValueObjectB.query("map.map.g").getValueAsString());

		Assert.assertNotNull(mergeRes.query("a"));
		Assert.assertEquals("a", mergeRes.query("a").getValueAsString());

		Assert.assertNotNull(mergeRes.query("b"));
		Assert.assertEquals("b", mergeRes.query("b").getValueAsString());

		Assert.assertNotNull(mergeRes.query("c"));
		Assert.assertEquals("c", mergeRes.query("c").getValueAsString());

		Assert.assertNotNull(mergeRes.query("map"));
		Assert.assertNotNull(mergeRes.query("map.a"));
		Assert.assertNotNull(mergeRes.query("map.b"));
		Assert.assertNotNull(mergeRes.query("map.c"));
		Assert.assertNotNull(mergeRes.query("map.d"));
		Assert.assertNotNull(mergeRes.query("map.map"));
		Assert.assertNotNull(mergeRes.query("map.map.f"));
		Assert.assertNotNull(mergeRes.query("map.map.g"));

		Assert.assertEquals("map.a", mergeRes.query("map.a").getValueAsString());
		Assert.assertEquals("map.b", mergeRes.query("map.b").getValueAsString());
		Assert.assertEquals("map.c", mergeRes.query("map.c").getValueAsString());
		Assert.assertEquals("map.d", mergeRes.query("map.d").getValueAsString());
		Assert.assertEquals("map.map.f", mergeRes.query("map.map.f").getValueAsString());
		Assert.assertEquals("map.map.g", mergeRes.query("map.map.g").getValueAsString());

		Assert.assertNotNull(etlTestValueObjectA.query("d"));
		Assert.assertEquals("[1, 2, 3]", mergeRes.query("d").getValueAsListOfStrings().toString());
	}

	@Test
	public void mismatchedTypesFail() throws ParseException
	{
		ETLTestValueObject etlTestValueObjectA = ETLTestParser.loadObject("{a: 'a'}");
		ETLTestValueObject etlTestValueObjectB = ETLTestParser.loadObject("{a: ['b']}");

		ETLTestValueObject mergeRes =
			etlTestValueObjectA.merge(
			etlTestValueObjectB,
			ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.nonrecursive
		);

		Assert.assertEquals(ETLTestValueObject.value_type.quoted_string, mergeRes.query("a").getValueType());

		mergeRes =
				etlTestValueObjectA.merge(
						etlTestValueObjectB,
						ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.nonrecursive
				);

		Assert.assertEquals(ETLTestValueObject.value_type.list, mergeRes.query("a").getValueType());
	}

	@Test
	public void duplicateTypesOtherThanMapFail() throws ParseException
	{
		ETLTestValueObject etlTestValueObjectA = ETLTestParser.loadObject("{a: 'a'}");
		ETLTestValueObject etlTestValueObjectB = ETLTestParser.loadObject("{a: 'b'}");

		ETLTestValueObject mergeRes =
			etlTestValueObjectA.merge(
			etlTestValueObjectB,
			ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.nonrecursive);

		Assert.assertEquals("b", mergeRes.query("a").getValueAsString());
	}

	@Test
	public void mergeLeftMerge() throws ParseException
	{
		ETLTestValueObject etlTestValueObjectLeft = ETLTestParser.loadObject("{a: 'a'}");
		ETLTestValueObject etlTestValueObjectRight = ETLTestParser.loadObject("{a: 'b'}");

		ETLTestValueObject
				merge =
				etlTestValueObjectLeft.merge(etlTestValueObjectRight, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.nonrecursive);

		Assert.assertEquals("a", merge.query("a").getValueAsString());
	}

	@Test
	public void mergeRightMerge() throws ParseException
	{
		ETLTestValueObject etlTestValueObjectLeft = ETLTestParser.loadObject("{a: 'a'}");
		ETLTestValueObject etlTestValueObjectRight = ETLTestParser.loadObject("{a: 'b'}");

		ETLTestValueObject
				merge =
				etlTestValueObjectLeft.merge(etlTestValueObjectRight, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.nonrecursive);

		Assert.assertEquals("b", merge.query("a").getValueAsString());
	}

	@Test
	public void directionalMergeOverridesMismatchedTypes() throws ParseException
	{
		ETLTestValueObject etlTestValueObjectLeft = ETLTestParser.loadObject("{a: 'a'}");
		ETLTestValueObject etlTestValueObjectRight = ETLTestParser.loadObject("{a: ['b']}");

		ETLTestValueObject
				merge =
				etlTestValueObjectLeft.merge(etlTestValueObjectRight, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.nonrecursive);

		Assert.assertEquals("[b]", merge.query("a").getValueAsListOfStrings().toString());
	}

	@Test
	public void listObjectsAreMutable() throws ParseException
	{
		ETLTestValueObject list = ETLTestParser.loadObject("['a', 'b', 'c']");

		list.getValueAsList().add(ETLTestParser.loadObject("'d'"));

		Assert.assertEquals("[a, b, c, d]", list.getValueAsListOfStrings().toString());
	}

	@Test
	public void pojoQueries()
	{
		ETLTestValueObject obj = new ETLTestValueObjectImpl(new File("test"));

		ETLTestValueObject me = obj.query("name");
		Assert.assertNotNull(me);
		Assert.assertEquals("test", me.getValueAsString());
	}

	@Test
	public void nullConvertToJson() throws ParseException {
		ETLTestValueObject list = ETLTestParser.loadObject("{a: null}");

		JsonNode node = list.getJsonNode();

		Assert.assertTrue(node.get("a").isNull());
	}

	@Test
	public void loadNull() throws ParseException {
		ETLTestValueObject lValueObject = ETLTestParser.loadObject("{a: null}");

		ETLTestValueObject testValueObject = lValueObject.query("a");
		Assert.assertNotNull(testValueObject);
		Assert.assertTrue(testValueObject.isNull());
	}

	@Test
	public void nullLeftMerge() throws ParseException {
		ETLTestValueObject lValueObject = ETLTestParser.loadObject("{a: null}");
		ETLTestValueObject rValueObject = ETLTestParser.loadObject("{a: 'hi'}");

		ETLTestValueObject res = lValueObject.merge(rValueObject, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.nonrecursive);
		Assert.assertTrue(res.query("a").isNull());

		res = lValueObject.merge(rValueObject, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.nonrecursive);
		Assert.assertEquals("hi", res.query("a").getValueAsString());
	}

	@Test
	public void leftMergeNoRecurseHideObject() throws ParseException {
		ETLTestValueObject lValueObject = ETLTestParser.loadObject("{a: 'hi'}");
		ETLTestValueObject rValueObject = ETLTestParser.loadObject("{a: {}}");

		ETLTestValueObject res = lValueObject.merge(rValueObject, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.nonrecursive);
		Assert.assertEquals("hi", res.query("a").getValueAsString());
	}

	@Test
	public void rightMergeNoRecurseHideObject() throws ParseException {
		ETLTestValueObject rValueObject = ETLTestParser.loadObject("{a: 'hi'}");
		ETLTestValueObject lValueObject = ETLTestParser.loadObject("{a: {}}");

		ETLTestValueObject res = lValueObject.merge(rValueObject, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.nonrecursive);
		Assert.assertEquals("hi", res.query("a").getValueAsString());
	}

	@Test
	public void deepLeftRecursiveMerge() throws ParseException {
		ETLTestValueObject lValueObject = ETLTestParser.loadObject(
			"{" +
				"a: {" +
					"b: {" +
					"}," +
					"c: {" +
						"lval: l," +
						"rval: l" +
					"}" +
				"}" +
			"}"
		);

		ETLTestValueObject rValueObject = ETLTestParser.loadObject(
			"{" +
				"a: {" +
					"c: {" +
						"rval: 'r'" +
					"}" +
				"}" +
			"}"
		);

		ETLTestValueObject res = lValueObject.merge(rValueObject, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);

		// validate that a.b made it over unchanged
		ETLTestValueObject l_a_b = lValueObject.query("a.b");
		Assert.assertNotNull(l_a_b);

		ETLTestValueObject res_a_b = res.query("a.b");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);

		// validate that a.c.lval made it over unchanged
		l_a_b = lValueObject.query("a.c.lval");
		Assert.assertNotNull(l_a_b);

		res_a_b = res.query("a.c.lval");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);

		// validate that a.c.rval got changed to 'l'
		l_a_b = lValueObject.query("a.c.rval");
		Assert.assertNotNull(l_a_b);

		res_a_b = res.query("a.c.rval");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);
	}

	@Test
	public void deepRightRecursiveMerge() throws ParseException {
		ETLTestValueObject rValueObject = ETLTestParser.loadObject(
				"{" +
					"a: {" +
						"b: {" +
						"}," +
						"c: {" +
							"lval: l," +
							"rval: l" +
						"}" +
					"}" +
				"}"
		);

		ETLTestValueObject lValueObject = ETLTestParser.loadObject(
				"{" +
					"a: {" +
						"c: {" +
							"rval: 'r'" +
						"}" +
					"}" +
				"}"
		);

		ETLTestValueObject res = lValueObject.merge(rValueObject, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.recursive);

		// validate that a.b made it over unchanged
		ETLTestValueObject l_a_b = rValueObject.query("a.b");
		Assert.assertNotNull(l_a_b);

		ETLTestValueObject res_a_b = res.query("a.b");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);

		// validate that a.c.lval made it over unchanged
		l_a_b = rValueObject.query("a.c.lval");
		Assert.assertNotNull(l_a_b);

		res_a_b = res.query("a.c.lval");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);

		// validate that a.c.rval got changed to 'l'
		l_a_b = rValueObject.query("a.c.rval");
		Assert.assertNotNull(l_a_b);

		res_a_b = res.query("a.c.rval");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);
	}

	@Test
	public void deepLeftNonRecursiveMerge() throws ParseException {
		ETLTestValueObject lValueObject = ETLTestParser.loadObject(
			"{" +
				"a: {" +
					"b: {" +
					"}," +
					"c: {" +
						"lval: l," +
						"rval: l" +
					"}" +
				"}" +
			"}"
		);

		ETLTestValueObject rValueObject = ETLTestParser.loadObject(
				"{" +
					"a: {" +
						"c: {" +
							"rval: 'r'" +
						"}" +
					"}," +
					"s: {}" +
				"}"
		);

		ETLTestValueObject res = lValueObject.merge(rValueObject, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.nonrecursive);

		// validate that a.b made it over unchanged
		ETLTestValueObject l_a_b = lValueObject.query("a");
		Assert.assertNotNull(l_a_b);

		ETLTestValueObject res_a_b = res.query("a");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);

		// validate that a.c.lval made it over unchanged
		l_a_b = rValueObject.query("s");
		Assert.assertNotNull(l_a_b);

		res_a_b = res.query("s");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);
	}

	@Test
	public void deepRightNonRecursiveMerge() throws ParseException {
		ETLTestValueObject rValueObject = ETLTestParser.loadObject(
				"{" +
					"a: {" +
						"b: {" +
						"}," +
						"c: {" +
							"lval: l," +
							"rval: l" +
						"}" +
					"}" +
				"}"
		);

		ETLTestValueObject lValueObject = ETLTestParser.loadObject(
				"{" +
					"a: {" +
						"c: {" +
							"rval: 'r'" +
						"}" +
					"}," +
					"s: {}" +
					"}"
		);

		ETLTestValueObject res = lValueObject.merge(rValueObject, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.nonrecursive);

		// validate that a.b made it over unchanged
		ETLTestValueObject l_a_b = rValueObject.query("a");
		Assert.assertNotNull(l_a_b);

		ETLTestValueObject res_a_b = res.query("a");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);

		// validate that a.c.lval made it over unchanged
		l_a_b = lValueObject.query("s");
		Assert.assertNotNull(l_a_b);

		res_a_b = res.query("s");
		Assert.assertNotNull(res_a_b);

		Assert.assertEquals(l_a_b, res_a_b);
	}
}
