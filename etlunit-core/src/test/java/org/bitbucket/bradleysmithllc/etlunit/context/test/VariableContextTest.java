package org.bitbucket.bradleysmithllc.etlunit.context.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObjectBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObjectImpl;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Vector;

public class VariableContextTest
{
	@Test
	public void declareVariablesAndSetValues()
	{
		VariableContextImpl impl = new VariableContextImpl();

		impl.declareVariable("Var1");
		Assert.assertNull(impl.getValue("Var1"));
		impl.setStringValue("Var1", "Value1");
		Assert.assertEquals("Value1", impl.getValue("Var1").getValueAsString());
		impl.setStringValue("Var1", null);
		Assert.assertNull(impl.getValue("Var1"));
	}

	@Test
	public void contextHasVariable()
	{
		VariableContextImpl impl = new VariableContextImpl();

		Assert.assertFalse(impl.hasVariableBeenDeclared("Var1"));
		impl.declareVariable("Var1");
		Assert.assertTrue(impl.hasVariableBeenDeclared("Var1"));
	}

	@Test
	public void setJSonValue()
	{
		VariableContextImpl impl = new VariableContextImpl();

		impl.declareAndSetJSONValue("Var1", "['a', 'b']");

		Assert.assertTrue(impl.hasVariableBeenDeclared("Var1"));

		ETLTestValueObject val = impl.getValue("Var1");

		Assert.assertEquals(ETLTestValueObject.value_type.list, val.getValueType());
		Assert.assertEquals("a", val.getValueAsListOfStrings().get(0));

		impl.declareAndSetJSONValue("Var1", "'a'");

		val = impl.getValue("Var1");

		Assert.assertEquals(ETLTestValueObject.value_type.quoted_string, val.getValueType());
		Assert.assertEquals("a", val.getValueAsString());

		impl.declareAndSetJSONValue("Var1", "a");

		val = impl.getValue("Var1");

		Assert.assertEquals(ETLTestValueObject.value_type.literal, val.getValueType());
		Assert.assertEquals("a", val.getValueAsString());

		impl.declareAndSetJSONValue("Var1", "a");

		val = impl.getValue("Var1");

		Assert.assertEquals(ETLTestValueObject.value_type.literal, val.getValueType());
		Assert.assertEquals("a", val.getValueAsString());

		impl.declareAndSetJSONValue("Var1", "{a: 'mine'}");

		val = impl.getValue("Var1");

		Assert.assertEquals(ETLTestValueObject.value_type.object, val.getValueType());
		Assert.assertEquals("mine", val.getValueAsMap().get("a").getValueAsString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void mustDeclareVariableBeforeGet()
	{
		VariableContextImpl impl = new VariableContextImpl();

		impl.getValue("Var1");
	}

	@Test(expected = IllegalArgumentException.class)
	public void mustDeclareVariableBeforeSet()
	{
		VariableContextImpl impl = new VariableContextImpl();

		impl.setStringValue("Var1", null);
	}

	@Test
	public void enclosingContextIsInherited()
	{
		VariableContextImpl impl = new VariableContextImpl();

		impl.declareVariable("Var1");
		impl.setStringValue("Var1", "Value1");

		VariableContext subContext = impl.createNestedScope();

		Assert.assertEquals("Value1", subContext.getValue("Var1").getValueAsString());

		impl.setStringValue("Var1", "Value2");
		Assert.assertEquals("Value2", subContext.getValue("Var1").getValueAsString());

		subContext.setStringValue("Var1", "Value3");
		Assert.assertEquals("Value3", subContext.getValue("Var1").getValueAsString());

		subContext.setStringValue("Var1", "Value4");
		Assert.assertEquals("Value4", impl.getValue("Var1").getValueAsString());
	}

	@Test
	public void enclosingContextIsHidden()
	{
		VariableContextImpl impl = new VariableContextImpl();

		impl.declareVariable("Var1");
		impl.setStringValue("Var1", "Value1");

		VariableContext subContext = impl.createNestedScope();

		Assert.assertEquals("Value1", subContext.getValue("Var1").getValueAsString());

		subContext.declareVariable("Var1");
		Assert.assertNull(subContext.getValue("Var1"));
		Assert.assertEquals("Value1", impl.getValue("Var1").getValueAsString());

		subContext.setStringValue("Var1", "Value2");

		Assert.assertEquals("Value2", subContext.getValue("Var1").getValueAsString());
		Assert.assertEquals("Value1", impl.getValue("Var1").getValueAsString());

		impl.setStringValue("Var1", "Value3");

		Assert.assertEquals("Value2", subContext.getValue("Var1").getValueAsString());
		Assert.assertEquals("Value3", impl.getValue("Var1").getValueAsString());
	}

	@Test
	public void velocityWrapper() throws Exception
	{
		VariableContext vbr = new VariableContextImpl();

		vbr.declareVariable("a");
		vbr.setStringValue("a", "b");

		String str = VelocityUtil.writeTemplate("${a}", vbr);

		Assert.assertEquals("b", str);

		vbr.declareVariable("b");
		vbr.setValue("b", ETLTestParser.loadBareObject("bdr: 'Hello', rdb: 'Goodbye'"));

		str = VelocityUtil.writeTemplate("${a} ${b.bdr} ${b.rdb}", vbr);

		Assert.assertEquals("b Hello Goodbye", str);
	}

	@Test
	public void declareAndSetImplicitDeclare()
	{
		VariableContextImpl impl = new VariableContextImpl();

		impl.declareAndSetValue("Var1", null);

		Assert.assertNull(impl.getValue("Var1"));
	}

	@Test
	public void testScopeClimbing()
	{
		VariableContextImpl impl = new VariableContextImpl();

		Assert.assertNull(impl.getEnclosingScope());
		Assert.assertTrue(impl == impl.getTopLevelScope());

		VariableContext nested = impl.createNestedScope();

		Assert.assertTrue(nested.getEnclosingScope() == impl);
		Assert.assertTrue(impl == nested.getTopLevelScope());

		VariableContext nested2 = nested.createNestedScope();

		Assert.assertTrue(nested2.getEnclosingScope() == nested);
		Assert.assertTrue(impl == nested2.getTopLevelScope());
	}

	@Test
	public void query()
	{
		VariableContext impl = new VariableContextImpl();

		ETLTestValueObject obj = new ETLTestValueObjectBuilder()
				.object()
				.key("s1")
				.object()
				.key("d1")
				.value("d1v")
				.key("d2")
				.value("d2v")
				.endObject()
				.key("s2")
				.object()
				.key("s2")
				.value("sv")
				.endObject()
				.endObject()
				.toObject();

		impl.declareAndSetValue("top", obj);
		impl.declareAndSetStringValue("p2", "pv");

		Assert.assertEquals("d1v", impl.query("top.s1.d1").getValueAsString());
		Assert.assertEquals("d2v", impl.query("top.s1.d2").getValueAsString());
		Assert.assertEquals("sv", impl.query("top.s2.s2").getValueAsString());
		Assert.assertEquals("pv", impl.query("p2").getValueAsString());
	}

	@Test
	public void removeWithScopes()
	{
		VariableContextImpl implSuper = new VariableContextImpl();

		implSuper.declareAndSetStringValue("Var", "implSuper");

		VariableContext implNested1 = implSuper.createNestedScope();
		implNested1.declareVariable("Var");
		implNested1.setStringValue("Var", "implNested1");

		VariableContext implNested2 = implNested1.createNestedScope();
		implNested2.declareVariable("Var");
		implNested2.setStringValue("Var", "implNested2");

		Assert.assertTrue(implNested1.hasVariableBeenDeclared("Var"));
		Assert.assertEquals("implNested1", implNested1.getValue("Var").getValueAsString());

		Assert.assertTrue(implNested2.hasVariableBeenDeclared("Var"));
		Assert.assertEquals("implNested2", implNested2.getValue("Var").getValueAsString());

		Assert.assertTrue(implSuper.hasVariableBeenDeclared("Var"));
		Assert.assertEquals("implSuper", implSuper.getValue("Var").getValueAsString());

		implNested2.removeVariable("Var");

		Assert.assertTrue(implNested2.hasVariableBeenDeclared("Var"));
		Assert.assertEquals("implNested1", implNested2.getValue("Var").getValueAsString());

		implNested2.removeVariable("Var");

		Assert.assertTrue(implNested2.hasVariableBeenDeclared("Var"));
		Assert.assertEquals("implSuper", implNested2.getValue("Var").getValueAsString());

		implNested2.setStringValue("Var", "new");

		Assert.assertEquals("new", implNested2.getValue("Var").getValueAsString());
		Assert.assertEquals("new", implNested1.getValue("Var").getValueAsString());
		Assert.assertEquals("new", implSuper.getValue("Var").getValueAsString());

		implNested2.removeVariable("Var");

		Assert.assertFalse(implNested2.hasVariableBeenDeclared("Var"));
		Assert.assertFalse(implNested1.hasVariableBeenDeclared("Var"));
		Assert.assertFalse(implSuper.hasVariableBeenDeclared("Var"));
	}

	@Test
	public void json() throws IOException {
		VariableContextImpl implSuper = new VariableContextImpl();
		getJsonNode(implSuper);
		implSuper.declareAndSetStringValue("var", "var");
		getJsonNode(implSuper);
		implSuper.declareVariable("varObj");

		ETLTestValueObject obj = new ETLTestValueObjectBuilder()
				.object()
				.key("name1")
				.value("val")
				.key("name2")
				.value("1")
				.key("name3")
				.list()
				.value("i")
				.value("o")
				.value("u")
				.endList()
				.endObject().toObject();

		implSuper.setValue("varObj", obj);

		ETLTestValueObject arr = new ETLTestValueObjectBuilder()
				.list()
				.value("i")
				.value("o")
				.value("u")
				.endList()
				.toObject();

		implSuper.declareAndSetValue("varList", arr);

		ETLTestValueObject etpoj = new ETLTestValueObjectImpl(this);
		implSuper.declareAndSetValue("varPojo", etpoj);

		etpoj = new ETLTestValueObjectImpl(new Vector());

		implSuper.declareAndSetValue("varPojo2", etpoj);

		obj.getValueAsMap();
		getJsonNode(implSuper);
	}

	private void getJsonNode(VariableContextImpl implSuper) throws IOException {
		System.out.println(JsonLoader.fromString(implSuper.getJsonNode().toString()));
	}
}
