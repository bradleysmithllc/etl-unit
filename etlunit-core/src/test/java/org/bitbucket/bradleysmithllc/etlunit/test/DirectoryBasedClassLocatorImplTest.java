package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ClassLocator;
import org.bitbucket.bradleysmithllc.etlunit.DirectoryBasedClassLocatorImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class DirectoryBasedClassLocatorImplTest
{
	File test = new File("testDir");

	@Before
	public void setUpRoot()
	{
		test.mkdirs();
	}

	@Test
	public void parseEmptyDirectory()
	{
		ClassLocator impl = new DirectoryBasedClassLocatorImpl(test);

		Assert.assertFalse(impl.hasNext());
	}

	@Test
	public void parseClassDirectory() throws IOException
	{
		IOUtils.writeBufferToFile(new File(test, "test.etlunit"), new StringBuffer("class class_a {} class class_b {}"));

		ClassLocator impl = new DirectoryBasedClassLocatorImpl(test);

		Assert.assertTrue(impl.hasNext());
		ETLTestClass cl = impl.next();

		Assert.assertEquals("class_a", cl.getName());
		Assert.assertTrue(impl.hasNext());

		cl = impl.next();

		Assert.assertEquals("class_b", cl.getName());
		Assert.assertEquals("[default]", cl.getPackage().getPackageName());
		Assert.assertFalse(impl.hasNext());
	}

	@Test
	public void ignoresExtraneous() throws IOException
	{
		IOUtils.writeBufferToFile(new File(test, "test.tlunit"), new StringBuffer("test class_a {} test class_b {}"));
		IOUtils.writeBufferToFile(new File(test, "test.infaunit"), new StringBuffer("test class_a {} test class_b {}"));

		ClassLocator impl = new DirectoryBasedClassLocatorImpl(test);

		Assert.assertFalse(impl.hasNext());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void neverCallRemove()
	{
		ClassLocator impl = new DirectoryBasedClassLocatorImpl(test);

		impl.remove();
	}

	@Test(expected = IllegalArgumentException.class)
	public void badRootDir()
	{
		ClassLocator impl = new DirectoryBasedClassLocatorImpl(new File("IDon'tExist"));
	}

	@Test
	public void parseDeepDirectory() throws Exception
	{
		File furl = new File(getClass().getResource("/etlTestDirectory").getFile());

		ClassLocator impl = new DirectoryBasedClassLocatorImpl(furl);

		Assert.assertTrue(impl.hasNext());

		ETLTestClass cl = impl.next();

		Assert.assertEquals("class_a", cl.getName());
		Assert.assertEquals("[default]", cl.getPackage().getPackageName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("class_b_1", cl.getName());
		Assert.assertEquals("POS", cl.getPackage().getPackageName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("class_b_2", cl.getName());
		Assert.assertEquals("POS", cl.getPackage().getPackageName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("class_c", cl.getName());
		Assert.assertEquals("subset.subset2", cl.getPackage().getPackageName());

		Assert.assertTrue(impl.hasNext());

		cl = impl.next();
		Assert.assertEquals("class_d", cl.getName());
		Assert.assertEquals("subset.subset3", cl.getPackage().getPackageName());

		Assert.assertFalse(impl.hasNext());
	}

	@Test
	public void resetDeepDirectory() throws Exception
	{
		File furl = new File(getClass().getResource("/etlTestDirectory").getFile());

		ClassLocator impl = new DirectoryBasedClassLocatorImpl(furl);

		Assert.assertTrue(impl.hasNext());

		ETLTestClass cl = impl.next();

		Assert.assertEquals("[default].class_a", cl.getQualifiedName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("POS.class_b_1", cl.getQualifiedName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("POS.class_b_2", cl.getQualifiedName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("subset.subset2.class_c", cl.getQualifiedName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("subset.subset3.class_d", cl.getQualifiedName());

		impl.reset();

		Assert.assertTrue(impl.hasNext());

		cl = impl.next();

		Assert.assertEquals("class_a", cl.getName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("class_b_1", cl.getName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("class_b_2", cl.getName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("class_c", cl.getName());

		Assert.assertTrue(impl.hasNext());
		cl = impl.next();
		Assert.assertEquals("class_d", cl.getName());
	}

	@After
	public void destroyRoot()
	{
		IOUtils.purge(test);
		test.delete();
	}
}
