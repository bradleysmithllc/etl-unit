package org.bitbucket.bradleysmithllc.etlunit.feature.meta.test.json.meta.meta;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bitbucket.bradleysmithllc.etlunit.feature.meta.test.json.MetaFeatureModuleConfiguration;

public class MetaRequest extends MetaFeatureModuleConfiguration
{
	private String doStuff;
	private boolean hasDoStuff;

	public boolean hasDoStuff()
	{
		return hasDoStuff;
	}

	public String getDoStuff()
	{
		return doStuff;
	}

	@JsonProperty(value = "do-stuff")
	public void setDoStuff(String doStuff)
	{
		hasDoStuff = true;
		this.doStuff = doStuff;
	}
}
