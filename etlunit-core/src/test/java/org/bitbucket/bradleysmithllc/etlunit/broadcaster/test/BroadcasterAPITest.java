package org.bitbucket.bradleysmithllc.etlunit.broadcaster.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlunit.ETLTestCases;
import org.bitbucket.bradleysmithllc.etlunit.MapLocal;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.listener.BroadcasterCoordinator;
import org.bitbucket.bradleysmithllc.etlunit.listener.BroadcasterCoordinatorImpl;
import org.bitbucket.bradleysmithllc.etlunit.listener.BroadcasterExecutor;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Tests the interface that the broadcaster must obey
 */
public class BroadcasterAPITest
{
	@Test
	public void testLifecycle() throws ParseException, IOException {
		// open the json test file
		JsonNode testObj = JsonLoader.fromResource("/broadcasterApi/test.json");

		JsonNode testsObj = testObj.get("tests");

		Assert.assertNotNull(testsObj);
		Assert.assertTrue(testsObj.isObject());

		Iterator<Map.Entry<String,JsonNode>> it = testsObj.fields();

		while (it.hasNext())
		{
			Map.Entry<String, JsonNode> testEntry = it.next();

			String testName = testEntry.getKey();
			JsonNode testNode = testEntry.getValue();

			Assert.assertTrue(testNode.isObject());

			// get the class input
			JsonNode inputNode = testNode.get("input");
			Assert.assertNotNull(inputNode);
			Assert.assertTrue(inputNode.isTextual());

			// get the lifecycle expectation
			JsonNode lifeCycle = testNode.get("lifeCycle");
			Assert.assertNotNull(lifeCycle);
			Assert.assertTrue(lifeCycle.isTextual());

			runTest(testName, inputNode.asText(), lifeCycle.asText());
		}
	}

	private void runTest(String testName, String s, String s1) throws ParseException {
		ETLTestCases cases = new ETLTestCases();

		final StringBuilder lifeBuilder = new StringBuilder();

		cases.addTestClasses(ETLTestParser.load(s));

		// create the coordinator
		final BroadcasterCoordinator coordinator = new BroadcasterCoordinatorImpl(cases, new MapLocal(), null, new PrintWriterLog());

		// check our executor in
		new Thread(new Runnable(){
			@Override
			public void run() {
				// create
				MyBroadcasterExecutor executor = new MyBroadcasterExecutor(lifeBuilder);

				// 'run' until exhausted
				while (coordinator.ready(executor)){}
			}
		}).start();

		// wait for it to register
		coordinator.waitForExecutorsToActivate(1);

		// kick it off
		coordinator.start();
		// pause until it finishes
		coordinator.waitForCompletion();

		Assert.assertEquals(testName, s1, lifeBuilder.toString());
	}

	private static class MyBroadcasterExecutor implements BroadcasterExecutor {
		private final StringBuilder lifeBuilder;

		public MyBroadcasterExecutor(StringBuilder lifeBuilder) {
			this.lifeBuilder = lifeBuilder;
		}

		@Override
		public int getId() {
			return 0;
		}

		@Override
		public void beginExecution() {
			lifeBuilder.append("beginExecution");
		}

		@Override
		public void enterPackage(ETLTestPackage name, StatusReporter.CompletionStatus status) {
			lifeBuilder.append("|enterPackage: " + name.getPackageName());
		}

		@Override
		public void leavePackage(ETLTestPackage name, StatusReporter.CompletionStatus status) {
			lifeBuilder.append("|leavePackage: " + name.getPackageName());
		}

		@Override
		public void enterClass(ETLTestClass cl, StatusReporter.CompletionStatus status) {
			lifeBuilder.append("|enterClass: " + cl.getQualifiedName());
		}

		@Override
		public void leaveClass(ETLTestClass cl, StatusReporter.CompletionStatus status) {
			lifeBuilder.append("|leaveClass: " + cl.getQualifiedName());
		}

		@Override
		public void runTest(ETLTestMethod mt, StatusReporter.CompletionStatus status) {
			lifeBuilder.append("|runTest: " + mt.getQualifiedName());
		}

		@Override
		public void endExecution() {
			lifeBuilder.append("|endExecution");
		}
	}
}
