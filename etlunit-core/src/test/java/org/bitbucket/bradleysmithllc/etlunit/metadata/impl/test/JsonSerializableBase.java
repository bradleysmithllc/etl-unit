package org.bitbucket.bradleysmithllc.etlunit.metadata.impl.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.junit.Assert;

import java.io.IOException;
import java.io.StringWriter;

public abstract class JsonSerializableBase
{
	protected abstract JsonSerializable newObject();

	protected JsonSerializable testJson(JsonSerializable jserRef) throws IOException {
		StringWriter writer = new StringWriter();
		JsonWriter jwr = new JsonWriter(writer);

		jwr.beginObject();
		jserRef.toJson(jwr);
		jwr.endObject();

		jwr.close();

		JsonSerializable impl2 = newObject();
		String json = writer.toString();
		impl2.fromJson(JsonLoader.fromString(json));

		Assert.assertEquals(jserRef, impl2);

		return impl2;
	}
}
