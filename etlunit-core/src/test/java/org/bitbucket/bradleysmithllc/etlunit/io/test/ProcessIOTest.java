package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.ApacheProcessRedirectExecutor;
import org.bitbucket.bradleysmithllc.etlunit.io.JavaForker;
import org.bitbucket.bradleysmithllc.etlunit.io.StreamProcessExecutor;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.util.concurrent.Semaphore;
import java.util.regex.Pattern;

public class ProcessIOTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private static final Object LOCK = new Object();

	public static final int PERMITS = 15;

	//@Test
	public void slowStreams() throws IOException, InterruptedException {
		BasicRuntimeSupport brs = new BasicRuntimeSupport();
		brs.installProcessExecutor(new StreamProcessExecutor());
		final RuntimeSupport rs = brs;

		PrintWriterLog applicationLogger = new PrintWriterLog();

		brs.setApplicationLogger(applicationLogger);
		brs.setUserLogger(applicationLogger);

		Semaphore sem = new Semaphore(PERMITS);

		File file = new File("/tmp/filelog");
		file.delete();

		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));

		for (int i = 0; i < PERMITS; i++)
		{
			new Thread(new MyRunnable(rs, sem, pw)).start();
		}

		Thread.sleep(1000L);

		sem.acquire(PERMITS);
	}

	private class MyRunnable implements Runnable {
		private final RuntimeSupport rs;
		private final Semaphore semaphore;
		private final PrintWriter printWriter;

		public MyRunnable(RuntimeSupport rs, Semaphore sem, PrintWriter pw) {
			this.rs = rs;
			semaphore = sem;
			printWriter = pw;
		}

		@Override
		public void run() {
			semaphore.acquireUninterruptibly();

			try {
				JavaForker forker = new JavaForker(rs);

				forker.setMainClass(SimpleEcho.class);
				for (int i = 0; i < 100; i++) {
					try {
						forker.setOutput(temporaryFolder.newFile());

						ProcessFacade prfac = forker.startProcess();

						prfac.waitForCompletion();

						StringBuffer buff = prfac.getInputBuffered();

						Pattern pat = Pattern.compile("(HelloHelloHelloGoodbye\n){100}");

						if (!pat.matcher(buff.toString()).matches())
						{
							System.out.println("#######################    Fail!   ##################");

							synchronized(LOCK)
							{
								printWriter.println("No match[" + System.identityHashCode(this) + "]: '" + buff.toString() + "'");
							}
							Thread.sleep(5000L);
							synchronized(LOCK)
							{
								printWriter.println("Later match[" + System.identityHashCode(this) + "]: '" + prfac.getInputBuffered() + "'");
							}
							Thread.sleep(15000L);
							synchronized(LOCK)
							{
								printWriter.println("Much later match[" + System.identityHashCode(this) + "]: '" + prfac.getInputBuffered() + "'");
								printWriter.flush();
							}
							return;
						}
					} catch (Exception e) {
						e.printStackTrace(System.out);
					}
				}
			} finally {
				semaphore.release();
			}
		}
	}
}
