package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Echoer
{
	public static void main(String [] argv) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String line;

		System.out.println("on:");

		while ((line = br.readLine()) != null)
		{
			line.toLowerCase();

			if (line.equals("exit"))
			{
				System.out.println("bye");
				System.exit(Integer.parseInt(br.readLine()));
			}
			else if (line.equals("stderr"))
			{
				System.err.println(br.readLine());
			}
			else if (line.equals("stdout"))
			{
				System.out.println(br.readLine());
			}

			System.out.println("on:");
		}
	}
}
