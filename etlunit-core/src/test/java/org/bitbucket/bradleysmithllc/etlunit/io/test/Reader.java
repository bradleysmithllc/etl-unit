package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Semaphore;

public class Reader implements Runnable
{
	private final Semaphore runSem;
	private final OutputStream dest;
	private final InputStream src;

	public Reader(InputStream is, OutputStream dest, Semaphore runSem)
	{
		this.runSem = runSem;
		this.dest = dest;
		this.src = is;

		runSem.acquireUninterruptibly();
	}

	@Override
	public void run()
	{
		int length = 0;

		int buffSize = 0;

		try
		{
			while (true)
			{
				byte[] buffer = new byte[buffSize++];

				try
				{
					length = src.read(buffer);

					if (length == -1)
					{
						dest.close();
						break;
					}

					dest.write(buffer, 0, length);
				}
				catch (IOException exc)
				{
					throw new RuntimeException("");
				}
			}
		}
		finally
		{
			runSem.release();
		}
	}
}
