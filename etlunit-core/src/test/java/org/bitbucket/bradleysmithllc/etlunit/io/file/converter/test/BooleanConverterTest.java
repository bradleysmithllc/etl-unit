package org.bitbucket.bradleysmithllc.etlunit.io.file.converter.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.BooleanConverter;
import org.junit.Assert;
import org.junit.Test;

public class BooleanConverterTest
{
	BooleanConverter bcon = new BooleanConverter();

	@Test
	public void testPattern()
	{
		Assert.assertTrue(bcon.getPattern(null).matcher("true").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("TRUE").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("True").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("TrUe").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("tRuE").matches());

		Assert.assertTrue(bcon.getPattern(null).matcher("false").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("FALSE").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("False").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("FaLsE").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("fAlSe").matches());

		Assert.assertFalse(bcon.getPattern(null).matcher("-1").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("0").matches());
		Assert.assertTrue(bcon.getPattern(null).matcher("1").matches());
		Assert.assertFalse(bcon.getPattern(null).matcher("2").matches());
	}

	@Test
	public void testConvert()
	{
		Assert.assertSame(Boolean.TRUE, bcon.parse("true", null));
		Assert.assertSame(Boolean.TRUE, bcon.parse("TRUE", null));
		Assert.assertSame(Boolean.TRUE, bcon.parse("True", null));
		Assert.assertSame(Boolean.TRUE, bcon.parse("TrUe", null));
		Assert.assertSame(Boolean.TRUE, bcon.parse("tRuE", null));

		Assert.assertSame(Boolean.FALSE, bcon.parse("false", null));
		Assert.assertSame(Boolean.FALSE, bcon.parse("FALSE", null));
		Assert.assertSame(Boolean.FALSE, bcon.parse("False", null));
		Assert.assertSame(Boolean.FALSE, bcon.parse("FaLsE", null));
		Assert.assertSame(Boolean.FALSE, bcon.parse("fAlSe", null));

		Assert.assertSame(Boolean.FALSE, bcon.parse("0", null));
		Assert.assertSame(Boolean.TRUE, bcon.parse("1", null));
		Assert.assertSame(Boolean.TRUE, bcon.parse("-1", null));
		Assert.assertSame(Boolean.TRUE, bcon.parse("2", null));
	}
}
