package org.bitbucket.bradleysmithllc.etlunit.misc.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.*;
import com.google.inject.Module;
import org.junit.Test;

import java.util.logging.Logger;

public class TestClass
{
	@Test
	public void pass()
	{
		//System.out.println("Hello\r\nWorld".replaceAll("\\r\\n|\\r|\\n", "-"));
		//System.out.println("Hello\nWorld".replaceAll("\\r\\n|\\r|\\n", "-"));
		//System.out.println("Hello\rWorld".replaceAll("\\r\\n|\\r|\\n", "-"));

		Module m = new Module()
		{
			@Override
			public void configure(Binder binder)
			{
				binder.bind(Runnable.class).toInstance(new Runnable()
				{
					@Override
					public void run()
					{
						System.out.println("Run");
					}
				});

				binder.bind(Runnable.class).annotatedWith(OtherRunnable.class).toInstance(new Runnable()
				{
					@Override
					public void run()
					{
						System.out.println("Run Annotated");
					}
				});
			}
		};

		Injector i = Guice.createInjector(m);

		i.injectMembers(new Object()
		{
			@Inject
			public void setLogger(Logger logger)
			{
				logger.info("Hello");
			}

			@Inject
			public void setRunnable(Runnable run)
			{
				run.run();
			}

			@Inject
			public void setOverrideRunnable(@OtherRunnable Runnable run)
			{
				run.run();
			}
		});
	}
}
