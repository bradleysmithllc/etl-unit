package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.io.FrameRandomAccessFileInputStream;
import org.bitbucket.bradleysmithllc.etlunit.io.JavaForker;
import org.bitbucket.bradleysmithllc.etlunit.io.StreamProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.io.StreamProcessExecutorProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.io.test.NoisyEcho;
import org.h2.util.IOUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;

public class JavaForkerTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void t() throws IOException {
		BasicRuntimeSupport brs = new BasicRuntimeSupport(temporaryFolder.newFolder());
		brs.installProcessExecutor(new StreamProcessExecutor());
		PrintWriterLog applicationLogger = new PrintWriterLog();

		brs.setApplicationLogger(applicationLogger);
		brs.setUserLogger(applicationLogger);

		JavaForker forker = new JavaForker(brs);
		forker.setMainClass(NoisyEcho.class);

		StreamProcessExecutorProcessFacade proc = (StreamProcessExecutorProcessFacade) forker.startProcess();
		proc.waitForCompletion();

		Assert.assertEquals(
				"HelloHelloHelloGoodbye\n" +
				"HelloHelloHelloGoodbye\r" +
				"HelloHelloHelloGoodbye\r\n" +
						"HelloHelloHelloGoodbye\n" +
						"HelloHelloHelloGoodbye\r" +
						"HelloHelloHelloGoodbye\r\n" +
						"HelloHelloHelloGoodbye\n" +
						"HelloHelloHelloGoodbye\r" +
						"HelloHelloHelloGoodbye\r\n" +
						"HelloHelloHelloGoodbye\n" +
						"HelloHelloHelloGoodbye\r" +
						"HelloHelloHelloGoodbye\r\n" +
						"HelloHelloHelloGoodbye\n" +
						"HelloHelloHelloGoodbye\r" +
						"HelloHelloHelloGoodbye\r\n" +
						"HelloHelloHelloGoodbye\n" +
						"HelloHelloHelloGoodbye\r" +
						"HelloHelloHelloGoodbye\r\n" +
						"HelloHelloHelloGoodbye\n" +
						"HelloHelloHelloGoodbye\r" +
						"HelloHelloHelloGoodbye\r\n" +
						"HelloHelloHelloGoodbye\n" +
						"HelloHelloHelloGoodbye\r" +
						"HelloHelloHelloGoodbye\r\n" +
						"HelloHelloHelloGoodbye\n" +
						"HelloHelloHelloGoodbye\r" +
						"HelloHelloHelloGoodbye\r\n",
		proc.getInputBuffered().toString());

		Assert.assertEquals(
				"uh-oh\n" +
				"uh-oh\r" +
				"uh-oh\r\n" +
						"uh-oh\n" +
						"uh-oh\r" +
						"uh-oh\r\n" +
						"uh-oh\n" +
						"uh-oh\r" +
						"uh-oh\r\n" +
						"uh-oh\n" +
						"uh-oh\r" +
						"uh-oh\r\n" +
						"uh-oh\n" +
						"uh-oh\r" +
						"uh-oh\r\n" +
						"uh-oh\n" +
						"uh-oh\r" +
						"uh-oh\r\n" +
						"uh-oh\n" +
						"uh-oh\r" +
						"uh-oh\r\n" +
						"uh-oh\n" +
						"uh-oh\r" +
						"uh-oh\r\n",
				proc.getErrorBuffered().toString());

		// compare to the raw files using the random access file streamer.  standard out and err.
		compareRaf(proc.getStreamOut(), proc.getInputStream());
		compareRaf(proc.getStreamErr(), proc.getErrorStream());

		// compare the reader to the input stream
		compareStreams(proc.getInputStream(), proc.getReader());
		compareStreams(proc.getErrorStream(), proc.getErrorReader());
	}

	private void compareStreams(InputStream inputStream, BufferedReader reader) throws IOException {
		String sreader = IOUtils.readStringAndClose(reader, -1);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		IOUtils.copy(inputStream, bout);

		String str = bout.toString();

		Assert.assertEquals(str, sreader);
	}

	private void compareRaf(File streamOut, InputStream inputStream) throws IOException {
		FrameRandomAccessFileInputStream frin = new FrameRandomAccessFileInputStream(streamOut);

		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		IOUtils.copy(frin, bout);

		byte [] bact = bout.toByteArray();
		Assert.assertNotEquals(0, bact.length);

		bout.reset();
		IOUtils.copy(inputStream, bout);

		byte [] bexp = bout.toByteArray();
		Assert.assertNotEquals(0, bexp.length);

		Assert.assertArrayEquals(bexp, bact);
	}
}
