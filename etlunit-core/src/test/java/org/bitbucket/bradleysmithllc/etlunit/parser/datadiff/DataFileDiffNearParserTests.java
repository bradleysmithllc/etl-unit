package org.bitbucket.bradleysmithllc.etlunit.parser.datadiff;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.time.EtlTimeUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

public class DataFileDiffNearParserTests {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void failFastEmpty() throws ParseException {
		expectedException.expect(ParseException.class);
		new DataFileDiffNearParser("").readSpecification();
	}

	@Test
	public void failFastInvalid() throws ParseException {
		expectedException.expect(TokenMgrException.class);
		new DataFileDiffNearParser("GahdhfjwyAFGJD").readSpecification();
	}

	@Test
	public void optionallyScanForDataDiffSpecificationEmpty() {
		Assert.assertNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification(""));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationInvalid() {
		Assert.assertNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("ksdjfsdkg d"));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationValidTimestampCT() {
		Assert.assertNotNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseUnit=SECONDS,unitQuantity=7)-->"));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationValidTimestampMillis() {
		DataFileDiffNearSpecification dataFileDiffNearSpecification = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseUnit=SECONDS,unitQuantity=7)-->");
		Assert.assertNotNull(dataFileDiffNearSpecification);

		Assert.assertEquals(DataFileDiffNearSpecification.specification_type.timestamp, dataFileDiffNearSpecification.getSpecificationType());
		Assert.assertNotNull(dataFileDiffNearSpecification.getTimestampNearSpecification());
		Assert.assertEquals(TimeUnit.SECONDS, dataFileDiffNearSpecification.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(7, dataFileDiffNearSpecification.getTimestampNearSpecification().getUnitQuantity());

		long nanos = ChronoUnit.NANOS.between(ZonedDateTime.now(), dataFileDiffNearSpecification.getTimestampNearSpecification().getReferenceDate());

		Assert.assertTrue(nanos < TimeUnit.NANOSECONDS.convert(100, TimeUnit.MILLISECONDS));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationValidTimestampSeconds() {
		Assert.assertNotNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018-01-01 00:00:00',baseUnit=SECONDS,unitQuantity=7)-->"));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationValidTimestampMinutes() {
		Assert.assertNotNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018-01-01 00:00',baseUnit=SECONDS,unitQuantity=7)-->"));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationValidTimestampHours() {
		Assert.assertNotNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018-01-01 00',baseUnit=SECONDS,unitQuantity=7)-->"));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationValidTimestampDays() {
		Assert.assertNotNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018-01-01',baseUnit=SECONDS,unitQuantity=7)-->"));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationValidTimestampMonths() {
		Assert.assertNotNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018-01',baseUnit=SECONDS,unitQuantity=7)-->"));
	}

	@Test
	public void optionallyScanForDataDiffSpecificationValidTimestampYears() {
		Assert.assertNotNull(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018',baseUnit=SECONDS,unitQuantity=7)-->"));
	}

	@Test
	public void minimalTimestamp() {
		DataFileDiffNearSpecification dataFileDiffNearSpecification = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts-->");
		Assert.assertNotNull(dataFileDiffNearSpecification);

		Assert.assertEquals(DataFileDiffNearSpecification.specification_type.timestamp, dataFileDiffNearSpecification.getSpecificationType());
		Assert.assertTrue(dataFileDiffNearSpecification.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(TimeUnit.MINUTES, dataFileDiffNearSpecification.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(1, dataFileDiffNearSpecification.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void alternativeMinimalTimestamp() {
		DataFileDiffNearSpecification dataFileDiffNearSpecification = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts()-->");
		DataFileDiffNearSpecification dataFileDiffNearSpecification1 = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts-->");

		Assert.assertEquals(dataFileDiffNearSpecification.getSpecificationType(), dataFileDiffNearSpecification1.getSpecificationType());
		Assert.assertEquals(dataFileDiffNearSpecification.getTimestampNearSpecification().isReferenceDateCurrentDate(), dataFileDiffNearSpecification1.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(dataFileDiffNearSpecification.getTimestampNearSpecification().getTimeUnit(), dataFileDiffNearSpecification1.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(dataFileDiffNearSpecification.getTimestampNearSpecification().getUnitQuantity(), dataFileDiffNearSpecification1.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void literalTimestampValueYearsOnly() throws Exception {
		Assert.assertEquals(year(2018), new DataFileDiffNearParser("'2018'").literalTimestampValue());
	}

	private ZonedDateTime year(int year) throws Exception {
		return LocalDateTime.of(year, 1, 1, 0, 0, 0, 0).atZone(EtlTimeUtils.utcZoneId());
	}

	@Test
	public void literalTimestampValueToMonths() throws Exception {
		Assert.assertEquals(yearMonth(2017, 11), new DataFileDiffNearParser("'2017-11'").literalTimestampValue());
	}

	private ZonedDateTime yearMonth(int year, int month) throws Exception {
		return LocalDateTime.of(year, month, 1, 0, 0, 0, 0).atZone(EtlTimeUtils.utcZoneId());
	}

	@Test
	public void literalTimestampValueToDays() throws Exception {
		Assert.assertEquals(yearMonthDay(2016, 10, 9), new DataFileDiffNearParser("'2016-10-09'").literalTimestampValue());
	}

	private ZonedDateTime yearMonthDay(int year, int month, int day) throws Exception {
		return LocalDateTime.of(year, month, day, 0, 0, 0, 0).atZone(EtlTimeUtils.utcZoneId());
	}

	@Test
	public void literalTimestampValueToHours() throws Exception {
		Assert.assertEquals(yearMonthDayHour(2015, 9, 8, 17), new DataFileDiffNearParser("'2015-09-08 17'").literalTimestampValue());
	}

	private ZonedDateTime yearMonthDayHour(int year, int month, int day, int hour) throws Exception {
		return LocalDateTime.of(year, month, day, hour, 0, 0, 0).atZone(EtlTimeUtils.utcZoneId());
	}

	@Test
	public void literalTimestampValueToMinutes() throws Exception {
		Assert.assertEquals(yearMonthDayHourMinute(2014, 8, 7, 16, 32), new DataFileDiffNearParser("'2014-08-07 16:32'").literalTimestampValue());
	}

	private ZonedDateTime yearMonthDayHourMinute(int year, int month, int day, int hour, int minute) throws Exception {
		return LocalDateTime.of(year, month, day, hour, minute, 0, 0).atZone(EtlTimeUtils.utcZoneId());
	}

	@Test
	public void literalTimestampValueToSeconds() throws Exception {
		Assert.assertEquals(yearMonthDayHourMinuteSecond(2013, 7, 6, 15, 31, 55), new DataFileDiffNearParser("'2013-07-06 15:31:55'").literalTimestampValue());
	}

	private ZonedDateTime yearMonthDayHourMinuteSecond(int year, int month, int day, int hour, int minute, int second) throws Exception {
		return LocalDateTime.of(year, month, day, hour, minute, second, 0).atZone(EtlTimeUtils.utcZoneId());
	}

	@Test
	public void literalTimestampValueToMilliseconds() throws Exception {
		Assert.assertEquals(yearMonthDayHourMinuteMillisecond(2012, 6, 5, 14, 30, 54, 42), new DataFileDiffNearParser("'2012-06-05 14:30:54.042'").literalTimestampValue());
	}

	private ZonedDateTime yearMonthDayHourMinuteMillisecond(int year, int month, int day, int hour, int minute, int second, int millisecond) throws Exception {
		return LocalDateTime.of(year, month, day, hour, minute, second, (int) TimeUnit.NANOSECONDS.convert(millisecond, TimeUnit.MILLISECONDS)).atZone(EtlTimeUtils.utcZoneId());
	}

	@Test
	public void numberMax() throws Exception {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Numeric value for blah must be at most 3 digits: 0900");
		DataFileDiffNearParser.assertNumberMaxLength("0900", "blah", 3);
	}

	@Test
	public void numberTooShort() throws Exception {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Numeric value for blah must be exactly 4 digits: 090");
		DataFileDiffNearParser.assertNumberLength("090", "blah", 4);
	}

	@Test
	public void numberTooLong() throws Exception {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Numeric value for bluh must be exactly 3 digits: 0900");
		DataFileDiffNearParser.assertNumberLength("0900", "bluh", 3);
	}

	@Test
	public void sample() throws ParseException {
		DataFileDiffNearSpecification specification = new DataFileDiffNearParser("<!--ts(baseValue='2018-01-01 00:00:30.000',baseUnit=SECONDS,unitQuantity=7)-->").readSpecification();

		Assert.assertNotNull(specification);
		Assert.assertEquals(DataFileDiffNearSpecification.specification_type.timestamp, specification.getSpecificationType());

		Assert.assertEquals(TimeUnit.SECONDS, specification.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(7, specification.getTimestampNearSpecification().getUnitQuantity());

		Assert.assertEquals(
				EtlTimeUtils.utcFromLocalDateTimeString("2018-01-01 00:00:30.000"),
				specification.getTimestampNearSpecification().getReferenceDate()
		);

		Assert.assertEquals(
				EtlTimeUtils.utcFromLocalDateTimeString("2018-01-01 00:00:23.000"),
				specification.getTimestampNearSpecification().earliestDate(specification.getTimestampNearSpecification().getReferenceDate())
		);

		Assert.assertEquals(
				EtlTimeUtils.utcFromLocalDateTimeString("2018-01-01 00:00:37.000"),
				specification.getTimestampNearSpecification().latestDate(specification.getTimestampNearSpecification().getReferenceDate())
		);
	}

	@Test
	public void noTrailingComma() throws ParseException {
		expectedException.expect(ParseException.class);
		new DataFileDiffNearParser("<!--ts(baseValue='2018',)-->").readSpecification();
	}

	@Test
	public void comma1() throws ParseException {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Do not lead with comma");
		new DataFileDiffNearParser("<!--ts(,baseUnit=MINUTES)-->").readSpecification();
	}

	@Test
	public void comma2() throws ParseException {
		expectedException.expect(ParseException.class);
		expectedException.expectMessage("Do not lead with comma");
		new DataFileDiffNearParser("<!--ts(,unitQuantity=1)-->").readSpecification();
	}

	@Test
	public void mixup1() {
		DataFileDiffNearSpecification dfs = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(unitQuantity=1)-->");

		// verify default to current date and timeunit minutes
		Assert.assertTrue(dfs.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(TimeUnit.MINUTES, dfs.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(1, dfs.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void mixup2() {
		DataFileDiffNearSpecification dfs = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseUnit=SECONDS)-->");

		// verify default to current date and timeunit minutes
		Assert.assertTrue(dfs.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(TimeUnit.SECONDS, dfs.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(1, dfs.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void mixup3() {
		DataFileDiffNearSpecification dfs = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseUnit=SECONDS,unitQuantity=7)-->");

		// verify default to current date and timeunit minutes
		Assert.assertTrue(dfs.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(TimeUnit.SECONDS, dfs.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(7, dfs.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void mixup4() {
		DataFileDiffNearSpecification dfs = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018')-->");

		// verify default to current date and timeunit minutes
		Assert.assertFalse(dfs.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(TimeUnit.MINUTES, dfs.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(1, dfs.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void mixup5() {
		DataFileDiffNearSpecification dfs = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018',baseUnit=SECONDS)-->");

		// verify default to current date and timeunit minutes
		Assert.assertFalse(dfs.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(TimeUnit.SECONDS, dfs.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(1, dfs.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void mixup6() {
		DataFileDiffNearSpecification dfs = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018',unitQuantity=99)-->");

		// verify default to current date and timeunit minutes
		Assert.assertFalse(dfs.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(TimeUnit.MINUTES, dfs.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(99, dfs.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void mixup7() {
		DataFileDiffNearSpecification dfs = DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2017',baseUnit=HOURS,unitQuantity=98)-->");

		// verify default to current date and timeunit minutes
		Assert.assertFalse(dfs.getTimestampNearSpecification().isReferenceDateCurrentDate());
		Assert.assertEquals(TimeUnit.HOURS, dfs.getTimestampNearSpecification().getTimeUnit());
		Assert.assertEquals(98, dfs.getTimestampNearSpecification().getUnitQuantity());
	}

	@Test
	public void scanBurst() {
		long beginMillis = System.nanoTime();
		for (int i = 0; i < 1000000; i++) {
			DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts(baseValue='2018-01-01 00:00:00',baseUnit=SECONDS,unitQuantity=7)-->");
		}
		long endMillis = System.nanoTime();

		System.out.println(1000000 + " matching scans in " + (endMillis - beginMillis) + "ns for " + ((endMillis - beginMillis) / 1000000) + "ns per scan");

		beginMillis = System.nanoTime();
		for (int i = 0; i < 1000000; i++) {
			DataFileDiffNearParser.optionallyScanForDataDiffSpecification("");
		}
		endMillis = System.nanoTime();

		System.out.println(1000000 + " empty scans in " + (endMillis - beginMillis) + "ns for " + ((endMillis - beginMillis) / 1000000) + "ns per scan");

		beginMillis = System.nanoTime();

		for (int i = 0; i < 1000000; i++) {
			DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--BooDhhsfdjsrqw rwergjsefg nsdfg ,sdmkf asdf ,asdf ");
		}
		endMillis = System.nanoTime();

		System.out.println(1000000 + " non-matching scans in " + (endMillis - beginMillis) + "ns for " + ((endMillis - beginMillis) / 1000000) + "ns per scan");
	}

	@Test
	public void cantCompareTimeToTimestamp() {
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("TimeNearSpecification !<> TimestampNearSpecification");
		DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--tm-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts-->"));
	}

	@Test
	public void cantCompareDateToTimestamp() {
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("DateNearSpecification !<> TimestampNearSpecification");
		DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--dt-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts-->"));
	}

	@Test
	public void cantCompareTimeToDate() {
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("TimeNearSpecification !<> DateNearSpecification");
		DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--tm-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--dt-->"));
	}

	@Test
	public void cantCompareTimestampToDate() {
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("TimestampNearSpecification !<> DateNearSpecification");
		DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--dt-->"));
	}

	@Test
	public void cantCompareDateToTime() {
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("DateNearSpecification !<> TimeNearSpecification");
		DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--dt-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--tm-->"));
	}

	@Test
	public void cantCompareTimestampToTime() {
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("TimestampNearSpecification !<> TimeNearSpecification");
		DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--ts-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--tm-->"));
	}

	@Test
	public void compareDateToDate() {
		Assert.assertEquals(0, DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--dt-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--dt-->")));
	}

	@Test
	public void compareTimeToTime() {
		Assert.assertEquals(0, DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--tm-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--tm-->")));
	}

	@Test
	public void compareLiteralTimeToLiteralTime() {
		Assert.assertEquals(0, DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--tm(baseValue='00:00:00')-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--tm(baseValue='00:00:00.000')-->")));
	}

	@Test
	public void compareLiteralDateToLiteralDate() {
		Assert.assertEquals(0, DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--dt(baseValue='2020-01-01')-->").compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification("<!--dt(baseValue='2020-01-01')-->")));
	}

	@Test
	public void allComparisons() throws IOException {
		String source = ResourceUtils.loadResourceAsString(getClass(), "allComparisons.txt");

		StringReader str = new StringReader(source);
		BufferedReader bstr = new BufferedReader(str);

		String next;

		while ((next = bstr.readLine()) != null) {
			if (next.equals("") || next.startsWith("#")) {
				continue;
			}
			String [] parts = next.split("\t");

			Assert.assertEquals(next, Integer.parseInt(parts[0]), DataFileDiffNearParser.optionallyScanForDataDiffSpecification(parts[1]).compareTo(DataFileDiffNearParser.optionallyScanForDataDiffSpecification(parts[2])));
		}
	}
}
