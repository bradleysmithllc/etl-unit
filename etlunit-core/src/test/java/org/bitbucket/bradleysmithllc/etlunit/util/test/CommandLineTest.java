package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.CommandLineSwitches;
import org.bitbucket.bradleysmithllc.etlunit.util.CommandLine;
import org.junit.Assert;
import org.junit.Test;

public class CommandLineTest
{
	@Test
	public void test()
	{
		CommandLine cl = new CommandLine(new String[]{"a", "b"});
		Assert.assertEquals(2, cl.getArgumentCount());
		Assert.assertEquals("a", cl.getArgument(0));
		Assert.assertEquals("b", cl.getArgument(1));

		cl = new CommandLine(new String[]{"b", "a", "--C"});
		Assert.assertEquals(2, cl.getArgumentCount());
		Assert.assertEquals("b", cl.getArgument(0));
		Assert.assertEquals("a", cl.getArgument(1));
		Assert.assertTrue(cl.hasSwitch("c"));
		Assert.assertFalse(cl.hasSwitchOption("c"));
		Assert.assertEquals("c", cl.getSwitch("c"));

		cl = new CommandLine(new String[]{"b", "a", "--c=f"});
		Assert.assertEquals(2, cl.getArgumentCount());
		Assert.assertEquals("b", cl.getArgument(0));
		Assert.assertEquals("a", cl.getArgument(1));
		Assert.assertTrue(cl.hasSwitch("c"));
		Assert.assertTrue(cl.hasSwitchOption("c"));
		Assert.assertEquals("f", cl.getSwitch("c"));

		cl = new CommandLine(new String[]{"b", "a", "--aCAc=f", "--list-failed=1"});
		Assert.assertEquals(2, cl.getArgumentCount());
		Assert.assertEquals("b", cl.getArgument(0));
		Assert.assertEquals("a", cl.getArgument(1));
		Assert.assertTrue(cl.hasSwitch("acac"));
		Assert.assertTrue(cl.hasSwitchOption("acac"));
		Assert.assertEquals("f", cl.getSwitch("acac"));
		Assert.assertEquals("1", cl.getSwitch("list-failed"));

		cl = new CommandLine(new String[]{"--a=true", "--b=false", "--c"});

		Assert.assertEquals(0, cl.getArgumentCount());

		Assert.assertTrue(cl.hasSwitch("a"));
		Assert.assertTrue(cl.hasSwitch("b"));
		Assert.assertTrue(cl.hasSwitch("c"));

		Assert.assertTrue(cl.getBooleanSwitch("a"));
		Assert.assertFalse(cl.getBooleanSwitch("b"));
		Assert.assertTrue(cl.getBooleanSwitch("c"));

		Assert.assertTrue(cl.getBooleanSwitch("a", true, true));
		Assert.assertTrue(cl.getBooleanSwitch("a", true, false));
		Assert.assertTrue(cl.getBooleanSwitch("a", false, true));
		Assert.assertTrue(cl.getBooleanSwitch("a", false, false));

		Assert.assertFalse(cl.getBooleanSwitch("b", true, true));
		Assert.assertFalse(cl.getBooleanSwitch("b", true, false));
		Assert.assertFalse(cl.getBooleanSwitch("b", false, true));
		Assert.assertFalse(cl.getBooleanSwitch("b", false, false));

		Assert.assertTrue(cl.getBooleanSwitch("c", false, true));
		Assert.assertFalse(cl.getBooleanSwitch("c", false, false));
		Assert.assertTrue(cl.getBooleanSwitch("c", true, true));
		Assert.assertFalse(cl.getBooleanSwitch("c", true, false));

		Assert.assertTrue(cl.getBooleanSwitch("d", true, true));
		Assert.assertFalse(cl.getBooleanSwitch("d", false, true));
		Assert.assertTrue(cl.getBooleanSwitch("d", true, false));
		Assert.assertFalse(cl.getBooleanSwitch("d", false, false));

		cl = new CommandLine(new String[]{"b", "a", "--aCAc=f", "--list-failed=1"});
		Assert.assertEquals(2, cl.getArgumentCount());
		Assert.assertEquals("b", cl.getArgument(0));
		Assert.assertEquals("a", cl.getArgument(1));
		Assert.assertTrue(cl.hasSwitch("acac"));
		Assert.assertTrue(cl.hasSwitchOption("acac"));
		//Assert.assertTrue(cl.hasSwitch("lf"));
		//Assert.assertTrue(cl.hasSwitchOption("lf"));
		Assert.assertEquals("f", cl.getSwitch("acac"));
		Assert.assertEquals("1", cl.getSwitch("list-failed"));
		//Assert.assertEquals("1", cl.getSwitch("lf"));
	}

	@Test
	public void testDeferred()
	{
		CommandLine cldef = new CommandLine(new String[]{"a", "b", "--test-deferred"});

		Assert.assertTrue(cldef.hasSwitch("test-deferred"));
		Assert.assertEquals(2, cldef.getArgumentCount());

		CommandLine clsub = new CommandLine(new String[]{"c", "d", "--test-deferred-sub"}, cldef);

		Assert.assertTrue(clsub.hasSwitch("test-deferred"));
		Assert.assertTrue(clsub.getBooleanSwitch("test-deferred", false, true));
		Assert.assertTrue(clsub.hasSwitch("test-deferred-sub"));
		Assert.assertEquals(2, clsub.getArgumentCount());
	}

	@Test
	public void testAlternate()
	{
		CommandLine cldef = new CommandLine("a b --test-deferred");

		Assert.assertTrue(cldef.hasSwitch("test-deferred"));
		Assert.assertEquals(2, cldef.getArgumentCount());

		CommandLine clsub = new CommandLine("c d --test-deferred-sub", cldef);

		Assert.assertTrue(clsub.hasSwitch("test-deferred"));
		Assert.assertTrue(clsub.hasSwitch("test-deferred-sub"));
		Assert.assertEquals(2, clsub.getArgumentCount());
	}

	@Test
	public void testAllowed()
	{
		CommandLine cldef = new CommandLine("--a");

		Assert.assertNull(cldef.verifyAllSwitches(new CommandLine.Switch[]{new CommandLine.Switch("a", "")}));
		Assert.assertEquals("a", cldef.verifyAllSwitches(new CommandLine.Switch[]{new CommandLine.Switch("b", "")}));
	}

	@Test
	public void switchesCalculateShortNameCorrectly()
	{
		CommandLine.Switch sw = new CommandLine.Switch("test-file", "Desc");

		Assert.assertEquals("test-file", sw.getName());
		Assert.assertEquals("tf", sw.getShortName());
		Assert.assertEquals("-tf", sw.getAbbreviatedCommandLineForm());
		Assert.assertEquals("--test-file", sw.getCommandLineForm());

		CommandLine.Switch swfoo = new CommandLine.Switch("foo-file", "Desc");

		CommandLine cl = new CommandLine("--test-file -ff=1");
		Assert.assertTrue(cl.hasSwitch(sw));
		Assert.assertFalse(cl.hasSwitchOption(sw));
		Assert.assertTrue(cl.hasSwitchOption(swfoo));
		Assert.assertTrue(cl.hasSwitch(swfoo));
		Assert.assertEquals("1", cl.getSwitch(swfoo));

		cl = new CommandLine("-tf");
		Assert.assertTrue(cl.hasSwitch(sw));
	}

	@Test
	public void switchesWork()
	{
		CommandLine cl = new CommandLine("-lew=false");
		Assert.assertTrue(cl.hasSwitch(CommandLineSwitches.loadInformaticaMapping));
		Assert.assertTrue(cl.hasSwitchOption(CommandLineSwitches.loadInformaticaMapping));

		Assert.assertNull(cl.verifyAllSwitches(CommandLineSwitches.OPTIONS));

		cl = new CommandLine("-lew");
		Assert.assertTrue(cl.hasSwitch(CommandLineSwitches.loadInformaticaMapping));
		Assert.assertFalse(cl.hasSwitchOption(CommandLineSwitches.loadInformaticaMapping));
		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.loadInformaticaMapping, true, false));
		Assert.assertTrue(cl.getBooleanSwitch(CommandLineSwitches.loadInformaticaMapping, false, true));

		Assert.assertNull(cl.verifyAllSwitches(CommandLineSwitches.OPTIONS));

		cl = new CommandLine("-ue -lew=false --debuggable -v");
		Assert.assertTrue(cl.hasSwitch(CommandLineSwitches.updateInformatica));
		Assert.assertFalse(cl.hasSwitchOption(CommandLineSwitches.updateInformatica));
		Assert.assertTrue(cl.getBooleanSwitch(CommandLineSwitches.updateInformatica, false, true));
		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.updateInformatica, true, false));

		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.loadInformaticaMapping, true, false));
		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.loadInformaticaMapping, false, true));
		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.loadInformaticaMapping, true, true));
		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.loadInformaticaMapping, false, false));

		Assert.assertFalse(cl.hasSwitchOption(CommandLineSwitches.debuggable));

		Assert.assertNull(cl.verifyAllSwitches(CommandLineSwitches.OPTIONS));

		cl = new CommandLine("-lew=true -v=false", cl);
		Assert.assertTrue(cl.hasSwitch(CommandLineSwitches.updateInformatica));
		Assert.assertFalse(cl.hasSwitchOption(CommandLineSwitches.updateInformatica));
		Assert.assertTrue(cl.getBooleanSwitch(CommandLineSwitches.updateInformatica, false, true));
		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.updateInformatica, true, false));

		Assert.assertTrue(cl.getBooleanSwitch(CommandLineSwitches.debuggable, false, true));
		Assert.assertTrue(cl.getBooleanSwitch(CommandLineSwitches.debuggable, true, true));
		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.debuggable, false, false));
		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.debuggable, true, false));

		Assert.assertTrue(cl.hasSwitchOption(CommandLineSwitches.loadInformaticaMapping));
		Assert.assertTrue(cl.getBooleanSwitch(CommandLineSwitches.loadInformaticaMapping));

		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.verbose));

		cl = new CommandLine("--load-bitbucket-workflow=true --debuggable");
		cl = new CommandLine("-lew=false -v", cl);

		Assert.assertFalse(cl.getBooleanSwitch(CommandLineSwitches.loadInformaticaMapping));
		Assert.assertEquals("false", cl.getSwitch(CommandLineSwitches.loadInformaticaMapping));
	}
}
