package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class IOUtilsTest
{
	@Test
	public void hasToLengthTest()
	{
		/*
		Assert.assertEquals("01234", IOUtils.hashToLength("01234", 5));
		Assert.assertEquals("02e", IOUtils.hashToLength("0123e4", 3));
		Assert.assertEquals("aiu", IOUtils.hashToLength("aeiou", 3));
		Assert.assertEquals("1_e", IOUtils.hashToLength("1_2_3_4_5aeiou", 3));
		Assert.assertEquals("__TRUNK_BI_B", IOUtils.hashToLength("__TRUNK_BI_BOO", 12));
		Assert.assertEquals("__TRUNK_BI", IOUtils.hashToLength("__TRUNK_BI_BOO", 10));

		Assert.assertEquals("BSMITH_WS__BI_branches_BI_1997_ORG_DBO@INFORMATICA_UNIT_TEST__BI_branches_BI_199",
				IOUtils.hashToLength("BSMITH_WS__BI_branches_BI_1997_ORG_DBO@INFORMATICA_UNIT_TEST__BI_branches_BI_1997_ORG_DBO",
						80));
		 */
	}

	@Test
	public void testCompareEmptyFiles() throws Exception
	{
		File src = File.createTempFile("AAA", "SRC");
		src.deleteOnExit();

		File trg = File.createTempFile("AAA", "TRG");
		trg.deleteOnExit();

		IOUtils.compareFiles(src, trg);
	}

	@Test(expected = TestAssertionFailure.class)
	public void testCompareDifferentFiles() throws Exception
	{
		File src = File.createTempFile("AAA", "SRC");
		src.deleteOnExit();

		IOUtils.writeBufferToFile(src, new StringBuffer("Line 1"));

		File trg = File.createTempFile("AAA", "TRG");
		trg.deleteOnExit();

		IOUtils.compareFiles(src, trg);
	}

	@Test
	public void testCompareEncodingFiles() throws Exception
	{
		File src = File.createTempFile("AAA", "SRC");
		src.deleteOnExit();

		IOUtils.writeBufferToFile(src, new StringBuffer("Line 1\nLine 2\rLine 3"));

		File trg = File.createTempFile("AAA", "TRG");
		trg.deleteOnExit();

		IOUtils.writeBufferToFile(trg, new StringBuffer("Line 1\r\nLine 2\r\nLine 3"));

		IOUtils.compareFiles(src, trg);
	}

	@Test
	public void testCompareSameFiles() throws Exception
	{
		File src = File.createTempFile("AAA", "SRC");
		src.deleteOnExit();

		IOUtils.writeBufferToFile(src, new StringBuffer("Line 1\nLine 2\rLine 3"));

		File trg = File.createTempFile("AAA", "TRG");
		trg.deleteOnExit();

		IOUtils.writeBufferToFile(trg, new StringBuffer("Line 1\nLine 2\nLine 3"));

		IOUtils.compareFiles(src, trg);
	}

	@Test(expected = TestAssertionFailure.class)
	public void testCompareDifferingRowsFiles() throws Exception
	{
		File src = File.createTempFile("AAA", "SRC");
		src.deleteOnExit();

		IOUtils.writeBufferToFile(src, new StringBuffer("Line 1\nLine 2.1\rLine 3"));

		File trg = File.createTempFile("AAA", "TRG");
		trg.deleteOnExit();

		IOUtils.writeBufferToFile(trg, new StringBuffer("Line 1\nLine 2\nLine 3"));

		IOUtils.compareFiles(src, trg);
	}

	@Test
	public void removeExtension()
	{
		Assert.assertEquals("file", IOUtils.removeExtension("file"));
		Assert.assertEquals("file", IOUtils.removeExtension("file.txt"));
		Assert.assertEquals("file.love", IOUtils.removeExtension("file.love.txt"));
	}

	@Test
	public void getExtension()
	{
		Assert.assertNull(IOUtils.getExtension("file"));
		Assert.assertEquals("txt", IOUtils.getExtension("file.txt"));
		Assert.assertEquals("txt", IOUtils.getExtension("file.love.txt"));
	}

	@Test
	public void fileHasExtension()
	{
		Assert.assertTrue(IOUtils.fileHasExtension("file.txt", "txt"));
		Assert.assertFalse(IOUtils.fileHasExtension("file", "txt"));
		Assert.assertFalse(IOUtils.fileHasExtension("file.html", "txt"));
		Assert.assertTrue(IOUtils.fileHasExtension("file.love.txt", "txt"));
	}
}
