package org.bitbucket.bradleysmithllc.etlunit.misc.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import utsupport.test_report;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestRegexp
{
	@Test
	public void testTblExp()
	{
		String s = "-----------\r\n          0\r\n\r\n(1 rows affected)";

		Pattern p = Pattern.compile("-----------\\r\\n          0\\r\\n\\r\\n\\(1 rows affected\\)");
		Matcher m = p.matcher(s);

		Assert.assertTrue(m.find());
	}

	@Test
	public void testSessionFileName()
	{
		Matcher m = test_report.FileFilterImpl.p.matcher("session.log.20100712164522");
		Assert.assertTrue(m.matches());

		m = test_report.FileFilterImpl.p.matcher("session.log");
		Assert.assertTrue(m.matches());
	}
}
