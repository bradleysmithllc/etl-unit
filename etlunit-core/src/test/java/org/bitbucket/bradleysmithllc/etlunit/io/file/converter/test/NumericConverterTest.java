package org.bitbucket.bradleysmithllc.etlunit.io.file.converter.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.NumericConverter;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class NumericConverterTest extends BaseConverterSupport {

	private NumericConverter numericConverter = new NumericConverter();

	@Test
	public void defaultPattern()
	{
		// default pattern
		Assert.assertSame(NumericConverter.getPattern(-1, -1), NumericConverter.P_PATTERN);
		Assert.assertSame(NumericConverter.getPattern(-1, 0), NumericConverter.UNBOUND_INTEGER);

		// length parameter changes the pattern
		Assert.assertNotSame(NumericConverter.getPattern(1, 0), NumericConverter.P_PATTERN);

		// this is a special case - can't constrain the length without scale
		Assert.assertSame(NumericConverter.getPattern(1, -1), NumericConverter.P_PATTERN);
	}

	@Test
	public void patternsWithLengthsAndScales()
	{
		// empty string does not match
		Assert.assertFalse(NumericConverter.getPattern(-1, -1).matcher("").matches());

		// defaults
		Assert.assertTrue(NumericConverter.getPattern(-1, -1).matcher("-20").matches());
		Assert.assertTrue(NumericConverter.getPattern(-1, -1).matcher("20").matches());
		Assert.assertTrue(NumericConverter.getPattern(-1, -1).matcher("20.0001").matches());
		Assert.assertTrue(NumericConverter.getPattern(-1, -1).matcher("0.00001").matches());

		// fixed lengths
		Assert.assertTrue(NumericConverter.getPattern(7, -1).matcher("-2012345").matches());
		Assert.assertTrue(NumericConverter.getPattern(7, -1).matcher("2012345").matches());
		Assert.assertTrue(NumericConverter.getPattern(7, -1).matcher("201234").matches());
		Assert.assertTrue(NumericConverter.getPattern(7, -1).matcher("20125").matches());
		Assert.assertTrue(NumericConverter.getPattern(7, -1).matcher("2013").matches());
		Assert.assertTrue(NumericConverter.getPattern(7, -1).matcher("201").matches());
		Assert.assertTrue(NumericConverter.getPattern(7, -1).matcher("20").matches());
		Assert.assertTrue(NumericConverter.getPattern(7, -1).matcher("0").matches());

		// fixed lengths and scales
		Assert.assertTrue(NumericConverter.getPattern(3, 2).matcher("-1.12").matches());
		Assert.assertTrue(NumericConverter.getPattern(3, 2).matcher("1.12").matches());
		Assert.assertTrue(NumericConverter.getPattern(3, 2).matcher("1.1").matches());
		Assert.assertTrue(NumericConverter.getPattern(3, 2).matcher("1").matches());

		// must have leading 0
		Assert.assertFalse(NumericConverter.getPattern(3, 2).matcher(".1").matches());

		// all decimals
		Assert.assertTrue(NumericConverter.getPattern(3, 3).matcher("-0.121").matches());
		Assert.assertTrue(NumericConverter.getPattern(3, 3).matcher("0.121").matches());
		Assert.assertTrue(NumericConverter.getPattern(3, 3).matcher("0.12").matches());
		Assert.assertTrue(NumericConverter.getPattern(3, 3).matcher("0.1").matches());
		Assert.assertTrue(NumericConverter.getPattern(3, 3).matcher("0").matches());
	}

	@Test(expected = IllegalArgumentException.class)
	public void length0NotAllowed()
	{
		NumericConverter.getPattern(0, -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void scaleExceedsLength()
	{
		NumericConverter.getPattern(3, 4);
	}

	@Test
	public void scaleEqualsLength()
	{
		NumericConverter.getPattern(3, 3);
	}

	@Test
	public void scaleLessThanLength()
	{
		NumericConverter.getPattern(3, 2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void scaleTooNegative()
	{
		NumericConverter.getPattern(3, -2);
	}

	@Test
	public void negatives()
	{
		Assert.assertEquals(new BigDecimal(-1), numericConverter.parse("-1", col));
		Assert.assertEquals("-1", numericConverter.format(new BigDecimal(-1), col));
	}

	@Test
	public void convert()
	{
		// vanilla
		Assert.assertEquals(new BigDecimal(0), numericConverter.parse("0", col));
		Assert.assertEquals(new BigDecimal(17), numericConverter.parse("17", col));
		Assert.assertEquals(new BigDecimal("223313123123412354135134513466"), numericConverter.parse("223313123123412354135134513466", col));

		// with length
		col.setLength(10);
		Assert.assertEquals(new BigDecimal(1234567898), numericConverter.parse("1234567898", col));

		// with scale
		col.setScale(5);
		Assert.assertEquals(new BigDecimal("12.32121"), numericConverter.parse("12.32121", col));
	}
}
