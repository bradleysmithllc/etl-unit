package org.bitbucket.bradleysmithllc.etlunit.metadata.impl.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.metadata.impl.MetaDataArtifactContentImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Test;

import java.io.IOException;

public class MetaDataArtifactContentTest extends JsonSerializableBase
{
	@Test
	public void metaJson() throws IOException, ParseException {
		MetaDataArtifactContentImpl nombre = new MetaDataArtifactContentImpl("nombre");
		// add one test reference
		nombre.referencedBy("informatica", ETLTestParser.load("class test { @Test method() {} }").get(0).getTestMethods().get(0));
		nombre.referencedBy("informatica-src", ETLTestParser.load("class test { @Test method1() {} }").get(0).getTestMethods().get(0));
		nombre.referencedBy("informatica-src", ETLTestParser.load("class test { @Test method2() {} }").get(0).getTestMethods().get(0));
		nombre.referencedBy("informatica-tgt", ETLTestParser.load("class test { @Test method3() {} }").get(0).getTestMethods().get(0));
		nombre.referencedBy("informatica-tgt", ETLTestParser.load("class test { @Test method4() {} }").get(0).getTestMethods().get(0));
		nombre.referencedBy("informatica-lkp", ETLTestParser.load("class test { @Test method5() {} }").get(0).getTestMethods().get(0));

		testJson(nombre);
	}

	@Test
	public void metaJsonWithPackages() throws IOException, ParseException {
		MetaDataArtifactContentImpl nombre = new MetaDataArtifactContentImpl("nombre");
		// add one test reference
		nombre.referencedBy("informatica", ETLTestParser.load("class test { @Test method() {} }", ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack")).get(0).getTestMethods().get(0));

		testJson(nombre);
	}

	@Override
	protected JsonSerializable newObject() {
		return new MetaDataArtifactContentImpl();
	}
}
