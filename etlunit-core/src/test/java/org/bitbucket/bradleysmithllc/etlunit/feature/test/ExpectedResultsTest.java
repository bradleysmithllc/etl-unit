package org.bitbucket.bradleysmithllc.etlunit.feature.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class ExpectedResultsTest extends BaseFeatureModuleTest
{
	@Test
	public void testFail() throws IOException
	{
		TestResults results = startTest();

		Assert.assertEquals("selected", 14, results.getNumTestsSelected());
		Assert.assertEquals("passed", 7, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals("failures", 10, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals("errors", 0, results.getMetrics().getNumberOfErrors());
		Assert.assertEquals("warnings", 1, results.getMetrics().getNumberOfWarnings());

		StatusReporter.CompletionStatus
				completionStatus =
				results.getMetrics().getResultsMapByTest().get("[default].expect_test.uncaught_error");
		Assert.assertNotNull(completionStatus);

		Assert.assertEquals(1, completionStatus.getErrors().size());
		TestExecutionError error = completionStatus.getErrors().get(0);

		Assert.assertEquals(TestConstants.ERR_UNCAUGHT_EXCEPTION, error.getErrorId());

		Assert.assertEquals(1, completionStatus.getErrors().size());
		completionStatus = results.getMetrics().getResultsMapByTest().get("[default].expect_test.error_should_fail");

		Assert.assertEquals(0, completionStatus.getErrors().size());

		completionStatus = results.getMetrics().getResultsMapByTest().get("[default].expect_test.error_ids");
		Assert.assertNotNull(completionStatus);

		Assert.assertEquals(1, completionStatus.getErrors().size());
		error = completionStatus.getErrors().get(0);

		Assert.assertEquals("ERR_EXPECTED", error.getErrorId());

		completionStatus = results.getMetrics().getResultsMapByTest().get("[default].expect_test.error_wrong_ids");
		Assert.assertNotNull(completionStatus);

		Assert.assertEquals(1, completionStatus.getErrors().size());
		error = completionStatus.getErrors().get(0);

		Assert.assertEquals("ERR_EXPECTED_WRONG", error.getErrorId());

		completionStatus = results.getMetrics().getResultsMapByTest().get("[default].expect_test.assert_wrong_ids");
		Assert.assertNotNull(completionStatus);

		List<TestAssertionFailure> failures = completionStatus.getAssertionFailures();
		Assert.assertNotNull(failures);

		Assert.assertEquals("FAIL_EXPECTED_WRONG", failures.get(0).getFailureIds()[0]);
	}

	@Override
	protected List<Feature> getTestFeatures()
	{
		return Collections.emptyList();
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSourceFromClasspath(null, "expect_test.etlunit");
	}
}
