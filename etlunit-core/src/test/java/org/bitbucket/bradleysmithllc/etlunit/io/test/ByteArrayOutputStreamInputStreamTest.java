package org.bitbucket.bradleysmithllc.etlunit.io.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.ByteArrayOutputStreamInputStream;
import org.bitbucket.bradleysmithllc.etlunit.io.ClosableByteArrayOutputStream;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.concurrent.Semaphore;

public class ByteArrayOutputStreamInputStreamTest
{
	@Test
	public void streamCopy()
	{
		String file = getClass().getResource("/streamTests").getFile();
		File source = new File(file);

		source.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File pathname)
			{
				Semaphore runSem = new Semaphore(2);

				ByteArrayOutputStream bcopy = new ByteArrayOutputStream();

				final ClosableByteArrayOutputStream bout = new ClosableByteArrayOutputStream();
				Semaphore sem = new Semaphore(1);

				ByteArrayOutputStreamInputStream baosis = new ByteArrayOutputStreamInputStream(bout, sem);

				try
				{
					InputStream in = new FileInputStream(pathname);

					new Reader(in, bout, runSem).run();
					new Reader(baosis, bcopy, runSem).run();

					runSem.acquireUninterruptibly(2);

					Assert.assertEquals(IOUtils.readFileToString(pathname).toString(), bcopy.toString());
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Assert.fail();
				}

				return false;
			}
		});
	}
}
