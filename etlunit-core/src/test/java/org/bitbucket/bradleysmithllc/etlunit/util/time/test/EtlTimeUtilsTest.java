package org.bitbucket.bradleysmithllc.etlunit.util.time.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.TimestampConverter;
import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.test.BaseConverterSupport;
import org.bitbucket.bradleysmithllc.etlunit.parser.datadiff.DateNearSpecificationTests;
import org.bitbucket.bradleysmithllc.etlunit.util.time.EtlTimeUtils;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EtlTimeUtilsTest extends BaseConverterSupport
{
	@Test
	public void utcFromLocalDateTimeStringZulu() {
		Assert.assertEquals("2020-01-01T00:11:22.333Z[UTC]", EtlTimeUtils.utcFromLocalDateTimeString("2020-01-01 00:11:22.333").toString());
	}

	@Test
	public void utcFromZonedDateTimeStringZulu() {
		Assert.assertEquals("2020-01-01T00:11:22.333Z[UTC]", EtlTimeUtils.utcFromZonedDateTimeString("2020-01-01 00:11:22.333Z").toString());
	}

	@Test
	public void utcFromZonedDateTimeStringCDT() {
		Assert.assertEquals("2020-01-01T00:11:22.333Z[UTC]", EtlTimeUtils.utcFromZonedDateTimeString("2020-01-01 00:11:22.333+0000").toString());
	}
}
