package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.DataSet;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetContainer;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.concurrent.atomic.AtomicInteger;

public class ReaderDataSetUtilsTest
{
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dataFileManager;

	@Before
	public void setup() throws IOException {
		dataFileManager = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	@Test
	public void copyNothing() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/skipData.dataset"
		);

		StringWriter target = new StringWriter();
		ReaderDataSetUtils.copy(rdsc, target, new ReaderDataSetUtils.CopyVisitor() {
			@Override
			public JsonNode getProperties(DataSet sourceDataSet, int index) {
				return null;
			}

			@Override
			public Reader read(DataSet set, int index) {
				return null;
			}

			@Override
			public void close(Reader reader, DataSet set, int dsIndex) throws IOException {
			}

			@Override
			public boolean includeDataSet(DataSet dataSet, int index) {
				return false;
			}
		});

		Assert.assertEquals("", target.toString());
	}

	@Test
	public void copy() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/copy.dataset"
		);

		StringWriter target = new StringWriter();
		ReaderDataSetUtils.copy(rdsc, target, new ReaderDataSetUtils.CopyVisitor() {
			@Override
			public JsonNode getProperties(DataSet sourceDataSet, int index) {
				return null;
			}

			@Override
			public Reader read(DataSet set, int index) {
				return null;
			}

			@Override
			public boolean includeDataSet(DataSet dataSet, int index) {
				return true;
			}

			@Override
			public void close(Reader reader, DataSet set, int dsIndex) throws IOException {
			}
		});

		Assert.assertEquals(IOUtils.readURLToString(getClass().getClassLoader().getResource("dataset/copyResult.dataset")), target.toString());
	}

	@Test
	public void copyOnly() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/copyOnly.dataset"
		);

		StringWriter target = new StringWriter();
		ReaderDataSetUtils.copy(rdsc, target, new ReaderDataSetUtils.CopyVisitor() {
			@Override
			public JsonNode getProperties(DataSet sourceDataSet, int index) {
				return null;
			}

			@Override
			public Reader read(DataSet set, int index) {
				return null;
			}

			@Override
			public boolean includeDataSet(DataSet dataSet, int index) {
				return true;
			}

			@Override
			public void close(Reader reader, DataSet set, int dsIndex) throws IOException {
			}
		});

		Assert.assertEquals(IOUtils.readURLToString(getClass().getClassLoader().getResource("dataset/copyOnlyResult.dataset")), target.toString());
	}

	@Test
	public void copyOdd() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/copy.dataset"
		);

		StringWriter target = new StringWriter();
		ReaderDataSetUtils.copy(rdsc, target, new ReaderDataSetUtils.CopyVisitor() {
			@Override
			public JsonNode getProperties(DataSet sourceDataSet, int index) {
				return null;
			}

			@Override
			public Reader read(DataSet set, int index) {
				return null;
			}

			@Override
			public boolean includeDataSet(DataSet dataSet, int index) {
				return index % 2 == 0;
			}

			@Override
			public void close(Reader reader, DataSet set, int dsIndex) throws IOException {
			}
		});

		Assert.assertEquals(IOUtils.readURLToString(getClass().getClassLoader().getResource("dataset/copyOddResult.dataset")), target.toString());
	}

	@Test
	public void copyEven() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/copy.dataset"
		);

		StringWriter target = new StringWriter();
		ReaderDataSetUtils.copy(rdsc, target, new ReaderDataSetUtils.CopyVisitor() {
			@Override
			public JsonNode getProperties(DataSet sourceDataSet, int index) {
				return null;
			}

			@Override
			public Reader read(DataSet set, int index) {
				return null;
			}

			@Override
			public boolean includeDataSet(DataSet dataSet, int index) {
				return index % 2 == 1;
			}

			@Override
			public void close(Reader reader, DataSet set, int dsIndex) throws IOException {
			}
		});

		Assert.assertEquals(IOUtils.readURLToString(getClass().getClassLoader().getResource("dataset/copyEvenResult.dataset")), target.toString());
	}

	@Test
	public void swapProperties() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/copy.dataset"
		);

		final AtomicInteger closeCount = new AtomicInteger();
		StringWriter target = new StringWriter();
		ReaderDataSetUtils.copy(rdsc, target, new ReaderDataSetUtils.CopyVisitor() {
			@Override
			public JsonNode getProperties(DataSet sourceDataSet, int index) {
				try {
					return JsonLoader.fromString("{\"id\": " + (index + 1) + "}");
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}

			@Override
			public Reader read(DataSet set, int index) {
				return null;
			}

			@Override
			public boolean includeDataSet(DataSet dataSet, int index) {
				return true;
			}

			@Override
			public void close(Reader reader, DataSet set, int dsIndex) throws IOException {
				closeCount.incrementAndGet();
			}
		});

		Assert.assertEquals(0, closeCount.get());
		Assert.assertEquals(IOUtils.readURLToString(getClass().getClassLoader().getResource("dataset/copySwapResult.dataset")), target.toString());
	}

	@Test
	public void swapBody() throws IOException {
		ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager,
				"dataset/copy.dataset"
		);

		final AtomicInteger closeCount = new AtomicInteger();
		StringWriter target = new StringWriter();
		ReaderDataSetUtils.copy(rdsc, target, new ReaderDataSetUtils.CopyVisitor() {
			@Override
			public JsonNode getProperties(DataSet sourceDataSet, int index) {
				return null;
			}

			@Override
			public Reader read(DataSet set, int index) {
				return new StringReader("" + (index + 1));
			}

			@Override
			public boolean includeDataSet(DataSet dataSet, int index) {
				return true;
			}

			@Override
			public void close(Reader reader, DataSet set, int dsIndex) throws IOException {
				closeCount.incrementAndGet();
			}
		});

		Assert.assertEquals(5, closeCount.get());
		Assert.assertEquals(IOUtils.readURLToString(getClass().getClassLoader().getResource("dataset/copySwapBodyResult.dataset")), target.toString());
	}

	@Test
	public void stringMapOrder() throws IOException
	{
		Assert.assertEquals("{ }", ReaderDataSetUtils.toFormattedJsonString(JsonLoader.fromString("{}")));
		Assert.assertEquals("{\n  b: 1,\n  a: 2\n}", ReaderDataSetUtils.toFormattedJsonString(JsonLoader.fromString("{\"b\": 1, \"a\": 2}")));
	}
}
