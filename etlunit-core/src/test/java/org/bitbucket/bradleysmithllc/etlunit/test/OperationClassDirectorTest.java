package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassBroadcasterImpl;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class OperationClassDirectorTest
{
	private ClassLocator getTestClasses() throws ParseException {
		List<ETLTestClass>
				list =
				ETLTestParser.load(
					"class a { @BeforeClass a() {info(message: 'hello');} @BeforeTest b() {fail(message: 'failh');} @Test bye() {test(grit: 'great');} @AfterTest c() {debug(message: 'debugh');} @AfterClass d() {error(message: 'errorh');}}");

		ClassLocator impl = new PassThroughClassLocatorImpl(list);

		return impl;
	}

	@Test
	public void parseProperty()
	{
		OperationClassDirector ocd = new OperationClassDirector();

		ocd.addOperation("a", "b");

		List<OperationClassDirector.OperationRef> operationList = ocd.getOperationList();

		Assert.assertEquals(1, operationList.size());

		Assert.assertNull(operationList.get(0).getOperationName());
		Assert.assertEquals("a", operationList.get(0).getPropertyName());
		Assert.assertEquals("b", operationList.get(0).getPropertyStringValue());

		ocd.addOperation("c.d", "e");

		Assert.assertEquals(2, operationList.size());

		Assert.assertEquals("c", operationList.get(1).getOperationName());
		Assert.assertEquals("d", operationList.get(1).getPropertyName());
		Assert.assertEquals("e", operationList.get(1).getPropertyStringValue());

		ocd.addOperation("a-e", "f");

		Assert.assertEquals(3, operationList.size());

		Assert.assertNull(operationList.get(2).getOperationName());
		Assert.assertEquals("a-e", operationList.get(2).getPropertyName());
		Assert.assertEquals("f", operationList.get(2).getPropertyStringValue());

		ocd.addOperation("x.y-z", "w");

		Assert.assertEquals(4, operationList.size());

		Assert.assertEquals("x", operationList.get(3).getOperationName());
		Assert.assertEquals("y-z", operationList.get(3).getPropertyName());
		Assert.assertEquals("w", operationList.get(3).getPropertyStringValue());
	}

	/** Tests for before class matches **/
	@Test
	public void beforeClass() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("info", "message", "hello");

		runTests(1, pdirector);
	}

	@Test
	public void beforeClassMissValue() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("info", "message", "hell1");

		runTests(0, pdirector);
	}

	@Test
	public void beforeClassMissProperty() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("info", "mesage", "hello");

		runTests(0, pdirector);
	}

	@Test
	public void beforeClassMissOperationName() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("debug", "message", "hello");

		runTests(0, pdirector);
	}

	@Test
	public void beforeClassMissOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "hell1");

		runTests(0, pdirector);
	}

	@Test
	public void beforeClassOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "hello");

		runTests(1, pdirector);
	}

	/** Tests for before test matches **/
	@Test
	public void before() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("fail", "message", "failh");

		runTests(1, pdirector);
	}

	@Test
	public void beforeMissValue() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("fail", "message", "fail1");

		runTests(0, pdirector);
	}

	@Test
	public void beforeMissProperty() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("fail", "mesage", "failh");

		runTests(0, pdirector);
	}

	@Test
	public void beforeMissOperationName() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("fail1", "message", "failh");

		runTests(0, pdirector);
	}

	@Test
	public void beforeOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "failh");

		runTests(1, pdirector);
	}

	@Test
	public void beforeMissOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "failh1");

		runTests(0, pdirector);
	}

	/** Tests for after test matches **/
	@Test
	public void after() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("debug", "message", "debugh");

		runTests(1, pdirector);
	}

	@Test
	public void afterMissValue() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("debug", "message", "debugh1");

		runTests(0, pdirector);
	}

	@Test
	public void afterMissProperty() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("debug", "mesage", "debugh");

		runTests(0, pdirector);
	}

	@Test
	public void afterMissOperationName() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("debug1", "message", "debugh");

		runTests(0, pdirector);
	}

	@Test
	public void afterOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "debugh");

		runTests(1, pdirector);
	}

	@Test
	public void afterMissOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "debugh1");

		runTests(0, pdirector);
	}

	/** Tests for this test matches **/
	@Test
	public void test() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("test", "grit", "great");

		runTests(1, pdirector);
	}

	@Test
	public void testMissValue() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("test", "grit", "great1");

		runTests(0, pdirector);
	}

	@Test
	public void testMissProperty() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("test", "grity", "great");

		runTests(0, pdirector);
	}

	@Test
	public void testMissOperationName() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("test1", "grit", "great");

		runTests(0, pdirector);
	}

	@Test
	public void testOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "grit", "great");

		runTests(1, pdirector);
	}

	@Test
	public void testMissOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "grit", "great1");

		runTests(0, pdirector);
	}

	/** Tests for after class matches **/
	@Test
	public void afterClass() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("error", "message", "errorh");

		runTests(1, pdirector);
	}

	@Test
	public void afterClassMissValue() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("error", "message", "errorh1");

		runTests(0, pdirector);
	}

	@Test
	public void afterClassMissProperty() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("error", "mesage", "errorh");

		runTests(0, pdirector);
	}

	@Test
	public void afterClassMissOperationName() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation("error1", "message", "errorh");

		runTests(0, pdirector);
	}

	@Test
	public void afterClassOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "errorh");

		runTests(1, pdirector);
	}

	@Test
	public void afterClassMissOperationNameMissing() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "errorh1");

		runTests(0, pdirector);
	}

	/** Tests regular expression matches for the value **/
	@Test
	public void regExp() throws ParseException {
		OperationClassDirector pdirector = new OperationClassDirector();

		pdirector.addOperation(null, "message", "rr.r");

		runTests(1, pdirector);
	}

	private void runTests(int result, OperationClassDirector dir) throws ParseException {
		ClassBroadcasterImpl climpl = new ClassBroadcasterImpl(
				getTestClasses(),
				dir,
				new NullClassListener(),
				new MapLocal(),
				new PrintWriterLog()
		);

		ETLTestCases cases = climpl.preScan(null);
		climpl.broadcast(null, cases);
		Assert.assertEquals(result, cases.getTestCount());
	}
}
