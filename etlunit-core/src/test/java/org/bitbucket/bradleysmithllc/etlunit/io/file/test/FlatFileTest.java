package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Test;

import java.io.IOException;

public class FlatFileTest
{
	public void requiresHeaders() throws IOException
	{
		loadFlatFile("", "\t", "\n");
	}

	public void requiresTypes() throws IOException
	{
		loadFlatFile("a\tb", "\t", "\n");
	}

	public void requiresValidTypes() throws IOException
	{
		loadFlatFile("a\tb\nint\tint", "\t", "\n");
	}

	public void requiresSameNumberOfTypesAndColumns() throws IOException
	{
		loadFlatFile("a\tb\nINTEGER", "\t", "\n");
	}

	@Test
	public void honorsDelimiters() throws IOException
	{
		loadFlatFile("a|b+INTEGER|INTEGER", "\\|", "+");
	}

	@Test
	public void columnsAndTypes() throws IOException
	{
		/*
		FlatFile ff = loadFlatFile("a\tb\nINTEGER\tBIGINT", "\t", "\n");

		Assert.assertEquals("a", ff.getLogicalColumns().get(0));
		Assert.assertEquals(Types.INTEGER, ff.getColumnType("a"));
		Assert.assertEquals("b", ff.getLogicalColumns().get(1));
		Assert.assertEquals(Types.BIGINT, ff.getColumnType("b"));
		 */
	}

	@Test
	public void columnsAndTypesWithData() throws IOException
	{
		/*
		FlatFile ff = loadFlatFile("a\tb\nINTEGER\tBIGINT\n1\t2\n3\t4", "\t", "\n");

		Assert.assertEquals("a", ff.getLogicalColumns().get(0));
		Assert.assertEquals(Types.INTEGER, ff.getColumnType("a"));
		Assert.assertEquals("b", ff.getLogicalColumns().get(1));
		Assert.assertEquals(Types.BIGINT, ff.getColumnType("b"));

		Iterator<Map<String, String>> it = ff.reader().iterator();

		Assert.assertTrue(it.hasNext());
		Map<String, String> next = it.next();

		// add 1 for the order key added by the impl
		Assert.assertEquals(3, next.size());
		Assert.assertEquals("1", next.get("a"));
		Assert.assertEquals("2", next.get("b"));
		Assert.assertEquals("1|2|", StringUtils.deleteWhitespace(next.get(FlatFile.ORDER_KEY)));

		Assert.assertTrue(it.hasNext());
		next = it.next();
		Assert.assertEquals(3, next.size());
		Assert.assertEquals("3", next.get("a"));
		Assert.assertEquals("4", next.get("b"));
		Assert.assertEquals("3|4|", StringUtils.deleteWhitespace(next.get(FlatFile.ORDER_KEY)));

		Assert.assertFalse(it.hasNext());
		 */
	}

	private Object loadFlatFile(String text, String colDelim, String rowDelim) throws IOException
	{
		/*
		File file = File.createTempFile("AAA", "TXT");

		IOUtils.writeBufferToFile(file, new StringBuffer(text));

		FlatFile ff = new FlatFile(file, colDelim, rowDelim, "null");

		//file.delete();

		return ff;
		*/
		return null;
	}

	@Test
	public void roundTripSqlTypeConversion()
	{
		/*
		Assert.assertEquals("INTEGER", FlatFile.getTypeName(FlatFile.getTypeValue("INTEGER")));
		Assert.assertEquals("BIGINT", FlatFile.getTypeName(FlatFile.getTypeValue("BIGINT")));
		 */
	}

	public void badTypeName()
	{
		/*
		FlatFile.getTypeValue("INTEGER1");
		 */
	}

	public void badTypeValue()
	{
		/*
		FlatFile.getTypeName(7167);
		 */
	}

	public void badKeyColumns() throws IOException
	{
		/*
		FlatFile ff = loadFlatFile("a\tb\nINTEGER\tBIGINT", "\t", "\n");
		ff.setKeyColumns(Arrays.asList("a", "c"));
		 */
	}

	@Test
	public void goodKeyColumns() throws IOException
	{
		/*
		FlatFile ff = loadFlatFile("a\tb\nINTEGER\tBIGINT", "\t", "\n");
		ff.setKeyColumns(Arrays.asList("a"));

		Assert.assertEquals(Arrays.asList("a"), ff.getKeyColumns());
		 */
	}

	@Test
	public void defaultKeyColumns() throws IOException
	{
		/*
		FlatFile ff = loadFlatFile("a\tb\nINTEGER\tBIGINT", "\t", "\n");

		Assert.assertEquals(Arrays.asList("a", "b"), ff.getKeyColumns());
		*/
	}
}
