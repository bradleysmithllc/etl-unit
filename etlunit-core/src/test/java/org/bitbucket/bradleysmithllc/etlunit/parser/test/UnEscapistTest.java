package org.bitbucket.bradleysmithllc.etlunit.parser.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.UnEscapist;
import org.junit.Assert;
import org.junit.Test;

public class UnEscapistTest
{
	@Test
	public void escapedCharactersAreUnEscaped()
	{
		Assert.assertEquals("", UnEscapist.unescape(""));
		Assert.assertEquals("abtnfr", UnEscapist.unescape("abtnfr"));
		Assert.assertEquals("\\", UnEscapist.unescape("\\\\"));
		Assert.assertEquals("abtnfr", UnEscapist.unescape("abtnfr"));
	}

	@Test
	public void specialCharactersAreEscaped()
	{
		Assert.assertEquals("", UnEscapist.unescape(""));
		Assert.assertEquals("\\b\\t\\n\\f\\r\\\\\\'\\\"", UnEscapist.escape("\b\t\n\f\r\\\'\""));
	}

	@Test
	public void dequotifyRemovesQuotes()
	{
		Assert.assertEquals("", UnEscapist.dequotify("''"));
		Assert.assertEquals("", UnEscapist.dequotify("\"\""));
		Assert.assertEquals("\'\"", UnEscapist.dequotify("\'\""));
		Assert.assertEquals("\"\'", UnEscapist.dequotify("\"\'"));
		Assert.assertEquals("abtnfr", UnEscapist.dequotify("abtnfr"));
		Assert.assertEquals("\\", UnEscapist.dequotify("\\"));
		Assert.assertEquals("abtnfr", UnEscapist.dequotify("abtnfr"));
		Assert.assertEquals("abtnfr", UnEscapist.dequotify("'abtnfr'"));
		Assert.assertEquals("\\", UnEscapist.dequotify("\"\\\""));
	}
}
