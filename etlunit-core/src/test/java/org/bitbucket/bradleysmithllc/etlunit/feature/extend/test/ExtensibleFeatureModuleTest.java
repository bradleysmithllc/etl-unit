package org.bitbucket.bradleysmithllc.etlunit.feature.extend.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.extend.Extender;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

public class ExtensibleFeatureModuleTest extends BaseFeatureModuleTest
{
	private RunnerAbstractFeature null_runner;
	private RunnerAbstractFeature abc_runner;
	private RunnerAbstractFeature def_runner;
	private RunnerAbstractFeature ghi_runner;

	protected List<Feature> getTestFeatures()
	{
		null_runner = new RunnerAbstractFeature("null-runner");
		abc_runner = new RunnerAbstractFeature("abc-runner");
		def_runner = new RunnerAbstractFeature("def-runner");
		ghi_runner = new RunnerAbstractFeature("ghi-runner");

		return Arrays.asList(
				(Feature) new TestExtendableModule(),
				null_runner,
				abc_runner,
				def_runner,
				ghi_runner
		);
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed)
	{
		Assert.assertEquals(4, operationsProcessed);
	}

	@Override
	protected void assertVariableContextPost(ETLTestClass cl, VariableContext context, int testsProcessed)
	{
		Assert.assertEquals(1, testsProcessed);
	}

	protected void setUpSourceFiles() throws IOException
	{
		createSourceFromClasspath(null, "test.etlunit");
	}

	@Test
	public void theExecutorWhichAcceptsTheTestShouldRunIt() throws IOException
	{
		startTest();

		Assert.assertEquals("[1, 4]", null_runner.getRunids());
		Assert.assertEquals("[2]", abc_runner.getRunids());
		Assert.assertEquals("[3]", def_runner.getRunids());
		Assert.assertEquals("[]", ghi_runner.getRunids());
	}

	private static class RunnerAbstractFeature extends AbstractFeature
	{
		private final List<String> runids = new ArrayList<String>();
		private final String name;

		private RunnerAbstractFeature(String name)
		{
			this.name = name;
		}

		@Override
		public FeatureMetaInfo getMetaInfo()
		{
			return new FeatureMetaInfo()
			{
				@Override
				public String getFeatureName()
				{
					return name;
				}

				@Override
				public String getFeatureConfiguration()
				{
					return null;
				}

				@Override
				public JsonSchema getFeatureConfigurationValidator()
				{
					return null;
				}

				@Override
				public JsonNode getFeatureConfigurationValidatorNode() {
					return null;
				}

				@Override
				public Map<String, FeatureOperation> getExportedOperations()
				{
					Map<String, FeatureOperation> map = new HashMap<String, FeatureOperation>();

					map.put("extend", new FeatureOperation()
					{
						@Override
						public String getName()
						{
							return null;
						}

						@Override
						public String getDescription()
						{
							return null;
						}

						@Override
						public String getUsage()
						{
							return null;
						}

						@Override
						public String getPrototype()
						{
							return null;
						}

						@Override
						public List<FeatureOperationSignature> getSignatures()
						{
							return null;
						}

						@Override
						public action_code process(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
						{
							ETLTestValueObject runner = parameters.query(getFeatureName());

							if (runner != null)
							{
								ETLTestValueObject id = parameters.query("id");

								runids.add(id.getValueAsString());

								return action_code.handled;
							}

							return action_code.defer;
						}
					});

					return map;
				}

				@Override
				public Map<String, FeatureAnnotation> getExportedAnnotations()
				{
					return null;
				}

				@Override
				public boolean isInternalFeature()
				{
					return false;
				}

				@Override
				public String getFeatureUsage()
				{
					return null;
				}

				@Override
				public List<RuntimeOptionDescriptor> getOptions()
				{
					return null;
				}

				@Override
				public String getDescribingClassName()
				{
					return getDescribing().getClass().getName();
				}

				@Override
				public Feature getDescribing()
				{
					return RunnerAbstractFeature.this;
				}
			};
		}

		public String getRunids()
		{
			return runids.toString();
		}

		@Inject
		public void installExecuteFeature(TestExtendableModule executeFeatureModule)
		{
			executeFeatureModule.extend(new Extender()
			{
				//@Override
				public boolean canHandleRequest(ETLTestOperation operation, ETLTestValueObject operands, VariableContext context, ExecutionContext econtext)
				{
					ETLTestValueObject runner = operands.query("runner");
					ETLTestValueObject id = operands.query("id");

					return
							runner != null && runner.getValueAsString().equals(getFeatureName()) && id != null;
				}

				@Override
				public ClassResponder.action_code process(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject operands, VariableContext context, ExecutionContext econtext, final int executor)
				{
					ETLTestValueObject runner = operands.query("runner");

					Assert.assertNotNull(runner);
					Assert.assertEquals(getFeatureName(), runner.getValueAsString());

					ETLTestValueObject id = operands.query("id");

					runids.add(id.getValueAsString());

					return action_code.handled;
				}

				@Override
				public Feature getFeature()
				{
					return RunnerAbstractFeature.this;
				}
			});
		}

		@Override
		public List<String> getPrerequisites()
		{
			return Arrays.asList("featureExtender");
		}

		@Override
		public ClassDirector getDirector()
		{
			return new NullClassDirector()
			{
				@Override
				public response_code accept(ETLTestClass cl)
				{
					return response_code.accept;
				}

				@Override
				public response_code accept(ETLTestMethod mt)
				{
					return response_code.accept;
				}

				@Override
				public response_code accept(ETLTestOperation op)
				{
					return response_code.accept;
				}
			};
		}

		public String getFeatureName()
		{
			return name;
		}
	}
}
