package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class DummyTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void dummy(){}

	//@Test
	public void migrate() throws IOException {
		DataFileManager dfm = new DataFileManagerImpl(temporaryFolder.newFolder());

		File legacyFmlFile = new File("/Users/bsmith/sonic-git/bi-legacy-sde/src/main/reference/file/fml/LEGACY_DISTRIBUTION_CENTER_ITEM.fml");
		DataFileSchema legacySrcFml = dfm.loadDataFileSchema(legacyFmlFile, "OLD_MP");

		File destFormat = new File("/Users/bsmith/sonic-git/bi-schemas/supplychain-schema/src/main/reference/file/fml/supplychain/supplychain/distribution_center_item+scm.4.fml");

		File sourceDir = new File("/Users/bsmith/sonic-git/bi-legacy-sde/src/test/etlunit/data/");
		for (File data : sourceDir.listFiles())
		{
			String name = data.getName();
			if (!name.endsWith(".bak") && name.contains("DISTRIBUTION_CENTER_ITEM"))
			{
				try
				{
					DataFile sdf = dfm.loadDataFile(data, legacySrcFml);

					DataFileSchema target_dfs = dfm.loadDataFileSchema(destFormat, "MP");

					File targetData = new File(sourceDir, name + ".tmp");
					DataFile tdf = dfm.loadDataFile(targetData, target_dfs);

					dfm.copyDataFile(sdf, tdf, new CopyOptionsBuilder().missingColumnPolicy(DataFileManager.CopyOptions.missing_column_policy.use_default).options());
					File sourceBak = new File(sourceDir, name + ".bak");

					data.renameTo(sourceBak);
					targetData.renameTo(data);
				}
				catch(Exception exc)
				{
					System.out.println(name + ": " + exc.toString());
				}
			}
		}
	}
}
