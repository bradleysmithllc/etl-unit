package org.bitbucket.bradleysmithllc.etlunit.util.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.HashMapArrayList;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;
import org.junit.Assert;
import org.junit.Test;

public class MapListTest
{
	@Test
	public void createsNewLists()
	{
		MapList<String, Boolean> hmap = new HashMapArrayList<String, Boolean>();

		Assert.assertNull(hmap.get("key"));
		Assert.assertNotNull(hmap.getOrCreate("key"));
		Assert.assertNotNull(hmap.get("key"));
		Assert.assertSame(hmap.get("key"), hmap.getOrCreate("key"));
	}
}
