package org.bitbucket.bradleysmithllc.etlunit.io.file.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.time.EtlTimeUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DataFileManagerTimezoneTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dataFileManager;

	@Before
	public void makeDFM() throws IOException {
		dataFileManager = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	@Test
	public void orderedSchema() throws IOException {
		DataFileSchema dataFileSchema = dataFileManager.loadDataFileSchemaFromResource("DataFileManagerTimezoneTest_orderedSchema.fml", "schema");

		DataFile dataFile = dataFileManager.loadDataFile(temporaryFolder.newFile(), dataFileSchema);

		DataFileWriter writer = dataFile.writer();

		Map<String, Object> data = new HashMap<>();

		data.put("id", 1);
		data.put("tmsz", EtlTimeUtils.localDateFromLocalDateTimeString("2020-01-01 00:00:00.000"));
		writer.addRow(data);

		data.put("id", 2);
		data.put("tmsz", EtlTimeUtils.localDateFromLocalDateTimeString("2020-01-01 00:00:00.000"));
		writer.addRow(data);

		data.put("id", 3);
		data.put("tmsz", EtlTimeUtils.localDateFromLocalDateTimeString("2020-01-01 00:00:00.000"));
		writer.addRow(data);

		data.put("id", 4);
		data.put("tmsz", EtlTimeUtils.localDateFromLocalDateTimeString("2020-01-01 00:00:00.000"));
		writer.addRow(data);

		writer.close();

		DataFileReader reader = dataFile.reader();

		Iterator<DataFileReader.FileRow> it = reader.iterator();

		while (it.hasNext()) {
			DataFileReader.FileRow rdata = it.next();

			Assert.assertEquals(rdata.getData().get("tmsz"), EtlTimeUtils.localDateFromLocalDateTimeString("2020-01-01 00:00:00.000"));
		}

		reader.dispose();
	}
}
