package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExecutionContextTest extends BaseFeatureModuleTest
{
	private final TestFeature testFeature = new TestFeature();

	@Override
	protected List<Feature> getTestFeatures()
	{
		return new ArrayList<Feature>(Arrays.asList(testFeature));
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSource("test.etlunit", "class test{ @Test run(){dispatch_a();}}");
	}

	/*
	@Override
	protected boolean echoLog() {
		return true;
	}
	 */

	@Test
	public void dispatch()
	{
		TestResults results = startTest();

		// These metrics are for one pass only because they are maintained by the test vm...
		Assert.assertEquals(1, results.getNumTestsSelected());
		Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());

		// These metrics are for the combined runs (Single threaded and multi threaded) since they are maintained by the test
		Assert.assertEquals(2, testFeature.a_count);
		Assert.assertEquals(2, testFeature.b_count);
		Assert.assertEquals(2, testFeature.c_count);
	}

	class TestFeature extends AbstractFeature
	{
		int a_count = 0;
		int b_count = 0;
		int c_count = 0;

		@Override
		public ClassListener getListener()
		{
			return new NullClassListener()
			{
				@Override
				public synchronized void beginTests(VariableContext context, int executorId) {
					System.out.println("beginTests: " + executorId);

					try {
						Thread.sleep(100L * executorId);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					//a_count = 0;
					//b_count = 0;
					//c_count = 0;
				}

				@Override
				public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, final int executorId)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					System.out.println("process: " + executorId);
					// dispatch an operation which this listener will handle
					if (op.getOperationName().equals("dispatch_a"))
					{
						a_count++;
						ETLTestOperation dispatch_b = op.createSibling("dispatch_b");
						econtext.process(dispatch_b, context);
					}
					else if (op.getOperationName().equals("dispatch_b"))
					{
						b_count++;
						ETLTestOperation dispatch_b = op.createSibling("dispatch_c");
						econtext.process(dispatch_b, context);
					}
					else
					{
						c_count++;
					}

					return action_code.handled;
				}
			};
		}

		@Override
		public String getFeatureName()
		{
			return "test";
		}
	}
}
