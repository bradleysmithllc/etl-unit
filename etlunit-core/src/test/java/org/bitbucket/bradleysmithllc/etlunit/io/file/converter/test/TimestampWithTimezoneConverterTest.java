package org.bitbucket.bradleysmithllc.etlunit.io.file.converter.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.TimestampConverter;
import org.bitbucket.bradleysmithllc.etlunit.io.file.converter.TimestampWithTimezoneConverter;
import org.bitbucket.bradleysmithllc.etlunit.parser.datadiff.DateNearSpecificationTests;
import org.bitbucket.bradleysmithllc.etlunit.util.time.EtlTimeUtils;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TimestampWithTimezoneConverterTest extends BaseConverterSupport
{
	TimestampWithTimezoneConverter bcon = new TimestampWithTimezoneConverter();

	@Test
	public void columnWithFormatHasNoPattern()
	{
		Assert.assertNull(bcon.getPattern(null));

		col.setFormat("");
		Assert.assertNull(bcon.getPattern(col));
	}

	@Test
	public void parseAndFormat() throws ParseException {
		String data = "2020-09-10 13:21:31.900";
		Object parse = bcon.parse(data, col);
		Assert.assertEquals(bcon.format(parse, col), data);
	}

	@Test
	public void zulu() throws ParseException {
		String data = "2020-09-10 13:21:31.900";
		Object parse = bcon.parse(data, col);
		Assert.assertEquals(bcon.format(parse, col), "2020-09-10 13:21:31.900");
	}
}
