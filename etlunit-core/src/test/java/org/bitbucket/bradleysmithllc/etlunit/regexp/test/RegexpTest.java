package org.bitbucket.bradleysmithllc.etlunit.regexp.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.regexp.*;
import org.junit.Assert;
import org.junit.Test;

public class RegexpTest
{
	@Test
	public void packNames()
	{
		PackNameExpression pne = new PackNameExpression("_pack_");

		Assert.assertTrue(pne.matches());
		Assert.assertEquals("pack", pne.getPackName());

		pne = new PackNameExpression("_pick_pack_");

		Assert.assertTrue(pne.matches());
		Assert.assertEquals("pick_pack", pne.getPackName());
	}

	@Test
	public void testClass()
	{
		TestSpecificationExpression tse = new TestSpecificationExpression("exp");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals("exp", tse.getTestClassPattern());
		Assert.assertFalse(tse.hasTestNamePattern());

		tse = new TestSpecificationExpression("exp*\\.1");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals("exp*\\.1", tse.getTestClassPattern());
		Assert.assertFalse(tse.hasTestNamePattern());
	}

	@Test
	public void testName()
	{
		TestSpecificationExpression tse = new TestSpecificationExpression("exp#all");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals("exp", tse.getTestClassPattern());
		Assert.assertEquals("all", tse.getTestNamePattern());

		tse = new TestSpecificationExpression("exp*\\.1#all");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals("exp*\\.1", tse.getTestClassPattern());
		Assert.assertEquals("all", tse.getTestNamePattern());
	}

	@Test
	public void operName()
	{
		TestSpecificationExpression tse = new TestSpecificationExpression("exp$all");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals("exp", tse.getTestClassPattern());
		Assert.assertFalse(tse.hasTestNamePattern());
		Assert.assertEquals("all", tse.getTestOperationPattern());

		tse = new TestSpecificationExpression("exp*\\.1$all");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals("exp*\\.1", tse.getTestClassPattern());
		Assert.assertFalse(tse.hasTestNamePattern());
		Assert.assertEquals("all", tse.getTestOperationPattern());
	}

	@Test
	public void testMethodOperName()
	{
		TestSpecificationExpression tse = new TestSpecificationExpression("exp#aud$all");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals("exp", tse.getTestClassPattern());
		Assert.assertEquals("aud", tse.getTestNamePattern());
		Assert.assertEquals("all", tse.getTestOperationPattern());
	}

	@Test
	public void methodOperName()
	{
		TestSpecificationExpression tse = new TestSpecificationExpression(".*#aud$all");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals(".*", tse.getTestClassPattern());
		Assert.assertEquals("aud", tse.getTestNamePattern());
		Assert.assertEquals("all", tse.getTestOperationPattern());
	}

	@Test
	public void bareOperName()
	{
		TestSpecificationExpression tse = new TestSpecificationExpression(".*$all");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals(".*", tse.getTestClassPattern());
		Assert.assertFalse(tse.hasTestNamePattern());
		Assert.assertEquals("all", tse.getTestOperationPattern());
	}

	@Test
	public void bareMethodName()
	{
		TestSpecificationExpression tse = new TestSpecificationExpression(".*#all");

		Assert.assertTrue(tse.matches());

		Assert.assertEquals(".*", tse.getTestClassPattern());
		Assert.assertEquals("all", tse.getTestNamePattern());
		Assert.assertFalse(tse.hasTestOperationPattern());
	}

	@Test
	public void testSpec()
	{
		TestExpression tse = new TestExpression("exp");

		Assert.assertTrue(tse.hasNext());

		TestSpecificationExpression ts = tse.getTestSpecification();

		Assert.assertTrue(ts.matches());

		Assert.assertTrue(ts.hasTestClassPattern());
		Assert.assertEquals("exp", ts.getTestClassPattern());
		Assert.assertFalse(ts.hasTestNamePattern());
		Assert.assertFalse(tse.hasNext());

		tse = new TestExpression("exp,pxe,did");

		Assert.assertTrue(tse.hasNext());

		ts = tse.getTestSpecification();

		Assert.assertTrue(ts.matches());

		Assert.assertTrue(ts.hasTestClassPattern());
		Assert.assertEquals("exp", ts.getTestClassPattern());
		Assert.assertFalse(ts.hasTestNamePattern());

		Assert.assertTrue(tse.hasNext());

		ts = tse.getTestSpecification();

		Assert.assertTrue(ts.matches());

		Assert.assertTrue(ts.hasTestClassPattern());
		Assert.assertEquals("pxe", ts.getTestClassPattern());
		Assert.assertFalse(ts.hasTestNamePattern());
		Assert.assertTrue(tse.hasNext());

		ts = tse.getTestSpecification();

		Assert.assertTrue(ts.matches());

		Assert.assertTrue(ts.hasTestClassPattern());
		Assert.assertEquals("did", ts.getTestClassPattern());
		Assert.assertFalse(ts.hasTestNamePattern());

		Assert.assertFalse(tse.hasNext());
	}

	@Test
	public void suiteName()
	{
		SuiteNameExpression se = new SuiteNameExpression("");

		Assert.assertFalse(se.matches());

		se = new SuiteNameExpression("aaa+");

		Assert.assertTrue(se.matches());
		Assert.assertEquals("aaa", se.getSuiteName());
		Assert.assertEquals("+", se.getOptionType());

		se = new SuiteNameExpression("aaa-");

		Assert.assertTrue(se.matches());
		Assert.assertEquals("aaa", se.getSuiteName());
		Assert.assertEquals("-", se.getOptionType());

		se = new SuiteNameExpression("aaa");

		Assert.assertTrue(se.matches());
		Assert.assertEquals("aaa", se.getSuiteName());
		Assert.assertFalse(se.hasOptionType());
	}

	@Test
	public void suiteNames()
	{
		SuiteNamesExpression sne = new SuiteNamesExpression("");

		Assert.assertFalse(sne.matches());

		sne = new SuiteNamesExpression("suite(aaa)");

		Assert.assertTrue(sne.matches());

		sne = new SuiteNamesExpression("suite(aaa+,bbb-,ddd)");

		Assert.assertTrue(sne.matches());

		sne = new SuiteNamesExpression("\tsuite\t(aaa\t+\t,bbb\t-\t,ccc\t)\t");

		Assert.assertTrue(sne.hasNext());

		SuiteNameExpression sn = sne.getSuiteName();

		Assert.assertTrue(sn.hasNext());

		Assert.assertEquals("aaa", sn.getSuiteName());
		Assert.assertEquals("+", sn.getOptionType());

		Assert.assertTrue(sn.hasNext());

		Assert.assertEquals("bbb", sn.getSuiteName());
		Assert.assertEquals("-", sn.getOptionType());

		Assert.assertTrue(sn.hasNext());

		Assert.assertEquals("ccc", sn.getSuiteName());
		Assert.assertFalse(sn.hasOptionType());

		Assert.assertFalse(sn.hasNext());
	}
}
