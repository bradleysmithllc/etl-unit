package org.bitbucket.bradleysmithllc.etlunit.io.file.ffml.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.json.validator.ResourceNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.sql.Types;
import java.util.*;

public class FlatFileSchemaTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private DataFileManager dfm;

	@Before
	public void start() throws IOException {
		dfm = new DataFileManagerImpl(temporaryFolder.newFolder());
	}

	@Test
	public void loadTestFFML()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr.ffml", "dsr");

		Assert.assertEquals("dsr", ffs.getId());
		Assert.assertEquals("\n", ffs.getRowDelimiter());
		Assert.assertNull(ffs.getColumnDelimiter());
		Assert.assertEquals(DataFileSchema.format_type.fixed, ffs.getFormatType());
		Assert.assertEquals(11, ffs.getLogicalColumns().size());

		DataFileSchema.Column col = ffs.getLogicalColumns().get(0);
		Assert.assertEquals("store_number", col.getId());
		Assert.assertEquals("VARCHAR", col.getType());
		Assert.assertEquals(5, col.getLength());
		Assert.assertEquals(0, col.getOffset());

		col = ffs.getLogicalColumns().get(1);
		Assert.assertEquals("sales_date", col.getId());
		Assert.assertEquals("VARCHAR", col.getType());
		Assert.assertEquals(Types.VARCHAR, col.jdbcType());
		Assert.assertEquals(8, col.getLength());
		Assert.assertEquals(5, col.getOffset());

		Assert.assertEquals(Arrays.asList("store_number",
				"sales_date",
				"dsr_category",
				"account_number",
				"sub_account_number",
				"amount",
				"dsr_line_description",
				"original_pos_amount",
				"taxpayer_id",
				"file_name_date",
				"pos_type"), ffs.getLogicalColumnNames());
		Assert.assertEquals(Arrays.asList("store_number", "sales_date"), ffs.getOrderColumnNames());

		ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr-delimited.ffml", "dsr-delimited");

		Assert.assertEquals("dsr-delimited", ffs.getId());
		Assert.assertEquals("\n", ffs.getRowDelimiter());
		Assert.assertEquals("\t", ffs.getColumnDelimiter());
		Assert.assertEquals(DataFileSchema.format_type.delimited, ffs.getFormatType());
		Assert.assertEquals(11, ffs.getLogicalColumns().size());
	}

	@Test
	public void defaultOrderBy()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/ticket.ffml", "ticket");
		Assert.assertEquals(ffs.getOrderColumns(), ffs.getLogicalColumns());
	}

	@Test(expected = ResourceNotFoundException.class)
	public void fixedMissingLength()
	{
		dfm.loadDataFileSchemaFromResource("ffml/fixed_missing_length.ffml", "fixed_missing_length");
	}

	@Test(expected = IllegalArgumentException.class)
	public void badColumnType()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/flat-invalid-type.ffml", "flat-invalid-type");
		ffs.getColumn("COL3").validateText("");
	}

	@Test
	public void delimitedMissingLength()
	{
		dfm.loadDataFileSchemaFromResource("ffml/delimited_missing_length.ffml", "delimited_missing_length");
	}

	@Test(expected = ResourceNotFoundException.class)
	public void fixedWithDelimiter()
	{
		dfm.loadDataFileSchemaFromResource("ffml/fixed_with_col_delimiter.ffml", "fixed_with_col_delimiter");
	}

	@Test(expected = ResourceNotFoundException.class)
	public void delimitedMissingDelimiter()
	{
		dfm.loadDataFileSchemaFromResource("ffml/delimited_missing_delimiter.ffml", "delimited_missing_delimiter");
	}

	@Test
	public void validFixedLines()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/flat-test.ffml", "flat-test");

		Assert.assertEquals("flat-test", ffs.getId());
		Assert.assertEquals(3, ffs.getLogicalColumns().size());

		Map<String, Object> map = ffs.validateAndSplitLine("12345123456780123456789");

		Assert.assertEquals("12345", map.get("col1"));
		Assert.assertEquals("12345678", map.get("col2"));
		Assert.assertEquals("0123456789", map.get("col3"));
	}

	@Test
	public void validDelimitedLines()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "delimited-test");

		Assert.assertEquals("delimited-test", ffs.getId());
		Assert.assertEquals(3, ffs.getLogicalColumns().size());

		Map<String, Object> map = ffs.validateAndSplitLine("12345|12345678|0123456789");

		Assert.assertEquals("12345", map.get("col1"));
		Assert.assertEquals("12345678", map.get("col2"));
		Assert.assertEquals("0123456789", map.get("col3"));
	}

	@Test
	public void validDelimitedLinesWithNulls()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "delimited-test");

		Map<String, Object> map = ffs.validateAndSplitLine("||");

		Assert.assertEquals("", map.get("col1"));
		Assert.assertEquals("", map.get("col2"));
		Assert.assertEquals("", map.get("col3"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidViewColumns()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "");
		ffs.createSubViewIncludingColumns(Arrays.asList("COL4"), "dsr2");
	}

	@Test
	public void validViewAttributes1()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "");
		DataFileSchemaView
				viewFFS =
				ffs.createSubViewIncludingColumns(Arrays.asList("COL1"), "dsr2", DataFileSchema.format_type.fixed);

		DataFileSchema matFFS = viewFFS.materialize();

		Assert.assertNull(matFFS.getColumnDelimiter());
		Assert.assertEquals("dsr2", matFFS.getId());
		Assert.assertEquals(ffs.getRowDelimiter(), matFFS.getRowDelimiter());
		Assert.assertEquals(DataFileSchema.format_type.fixed, matFFS.getFormatType());
	}

	@Test
	public void validViewAttributesColumns1()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "");
		DataFileSchema viewFFS = ffs.createSubViewIncludingColumns(Arrays.asList("COL1", "COL3"), "dsr2");

		Assert.assertEquals(ffs.getColumnDelimiter(), viewFFS.getColumnDelimiter());
		Assert.assertEquals("dsr2", viewFFS.getId());
		Assert.assertEquals(ffs.getRowDelimiter(), viewFFS.getRowDelimiter());
		Assert.assertEquals(ffs.getFormatType(), viewFFS.getFormatType());

		Assert.assertEquals(2, viewFFS.getLogicalColumns().size());
		Assert.assertEquals("col1", viewFFS.getLogicalColumns().get(0).getId());
		Assert.assertEquals("col3", viewFFS.getLogicalColumns().get(1).getId());

		Assert.assertEquals(2, viewFFS.getOrderColumns().size());
		Assert.assertEquals("col1", viewFFS.getOrderColumns().get(0).getId());
		Assert.assertEquals("col3", viewFFS.getOrderColumns().get(1).getId());
	}

	@Test
	public void validViewAttributesColumns2()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "");
		DataFileSchema viewFFS = ffs.createSubViewIncludingColumns(Arrays.asList("COL2"), "dsr2");

		Assert.assertEquals(1, viewFFS.getLogicalColumns().size());
		Assert.assertEquals("col2", viewFFS.getLogicalColumns().get(0).getId());

		Assert.assertEquals(Arrays.asList("col2"), viewFFS.getLogicalColumnNames());

		Assert.assertEquals(1, viewFFS.getOrderColumns().size());
		Assert.assertEquals("col2", viewFFS.getLogicalColumns().get(0).getId());
	}

	@Test
	public void validViewAttributesColumns3()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "");
		DataFileSchema viewFFS = ffs.createSubViewIncludingColumns(null, "dsr2");

		Assert.assertEquals(ffs.getLogicalColumns(), viewFFS.getLogicalColumns());
		Assert.assertEquals(ffs.getOrderColumns(), viewFFS.getOrderColumns());

		Assert.assertEquals(Arrays.asList("col1", "col2", "col3"), viewFFS.getLogicalColumnNames());
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidViewAttributes2()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "");
		ffs.createSubViewIncludingColumns(null, "dsr2", DataFileSchema.format_type.fixed).setColumnDelimiter("\\|");
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidViewAttributes3()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/flat-test.ffml", "");
		ffs.createSubViewIncludingColumns(null, "dsr2", DataFileSchema.format_type.fixed).setColumnDelimiter("\\|");
	}

	@Test
	public void sortTest() throws IOException
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/ticket.ffml", "");

		File source = new File("test.delimited");

		DataFile ff = dfm.loadDataFile(source, ffs);

		DataFileWriter writer = ff.writer();

		populate(writer);

		String text = IOUtils.readFileToString(source);

		Assert.assertEquals(
				"/*-- store_id|sales_date|ticket  --*/|/*-- VARCHAR|VARCHAR|VARCHAR  --*/|01000    z    1|02000    y    3|03000    x    2",
				text);

		ff = dfm.loadDataFile(source, ffs.createSubViewIncludingColumns(Arrays.asList("TICKET"), "ticket.TICKET"));

		writer = ff.writer();

		populate(writer);

		text = IOUtils.readFileToString(source);

		Assert.assertEquals("/*-- ticket  --*/|/*-- VARCHAR  --*/|    1|    2|    3", text);

		ff = dfm.loadDataFile(source, ffs.createSubViewIncludingColumns(Arrays.asList("sales_date"), "ticket.sales_date"));

		writer = ff.writer();

		populate(writer);

		text = IOUtils.readFileToString(source);

		Assert.assertEquals("/*-- sales_date  --*/|/*-- VARCHAR  --*/|    x|    y|    z", text);

		Assert.assertTrue(source.delete());
	}

	private void populate(DataFileWriter writer) throws IOException
	{
		Map<String, Object> row = new HashMap<String, Object>();

		row.put("store_id", "01000");
		row.put("sales_date", "z");
		row.put("ticket", "1");

		writer.addRow(row);

		row.put("store_id", "02000");
		row.put("sales_date", "y");
		row.put("ticket", "3");

		writer.addRow(row);

		row.put("store_id", "03000");
		row.put("sales_date", "x");
		row.put("ticket", "2");

		writer.addRow(row);

		writer.close();
	}

	@Test
	public void printJson()
	{
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/flat-test.ffml", "");

		String expected = "{\n" +
				"\t\"flat-file\": {\n" +
				"\t\t\"format-type\": \"fixed\",\n" +
				"\t\t\"row-delimiter\": \"|\",\n" +
				"\t\t\"null-token\": \"null\",\n" +
				"\t\t\"columns\": [\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col1\",\n" +
				"\t\t\t\t\"length\": 5,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t},\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col2\",\n" +
				"\t\t\t\t\"length\": 8,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t},\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col3\",\n" +
				"\t\t\t\t\"length\": 10,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t}\n" +
				"\t\t],\n" +
				"\t\t\"orderBy\": [\n" +
				"\t\t\t\"col1\",\n" +
				"\t\t\t\"col2\",\n" +
				"\t\t\t\"col3\"\n" +
				"\t\t]\n" +
				"\t}\n" +
				"}";
		Assert.assertEquals(expected, ffs.toJsonString());

		ffs.addKeyColumn("COL2");

		Assert.assertEquals("{\n" +
				"\t\"flat-file\": {\n" +
				"\t\t\"format-type\": \"fixed\",\n" +
				"\t\t\"row-delimiter\": \"|\",\n" +
				"\t\t\"null-token\": \"null\",\n" +
				"\t\t\"columns\": [\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col1\",\n" +
				"\t\t\t\t\"length\": 5,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t},\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col2\",\n" +
				"\t\t\t\t\"length\": 8,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t},\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col3\",\n" +
				"\t\t\t\t\"length\": 10,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t}\n" +
				"\t\t],\n" +
				"\t\t\"primaryKey\": [\n" +
				"\t\t\t\"col2\"\n" +
				"\t\t],\n" +
				"\t\t\"orderBy\": [\n" +
				"\t\t\t\"col1\",\n" +
				"\t\t\t\"col2\",\n" +
				"\t\t\t\"col3\"\n" +
				"\t\t]\n" +
				"\t}\n" +
				"}", ffs.toJsonString().replace("\r", ""));

		ffs.clearOrderColumns();

		Assert.assertEquals("{\n" +
				"\t\"flat-file\": {\n" +
				"\t\t\"format-type\": \"fixed\",\n" +
				"\t\t\"row-delimiter\": \"|\",\n" +
				"\t\t\"null-token\": \"null\",\n" +
				"\t\t\"columns\": [\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col1\",\n" +
				"\t\t\t\t\"length\": 5,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t},\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col2\",\n" +
				"\t\t\t\t\"length\": 8,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t},\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"id\": \"col3\",\n" +
				"\t\t\t\t\"length\": 10,\n" +
				"\t\t\t\t\"basic-type\": \"string\",\n" +
				"\t\t\t\t\"type\": \"VARCHAR\"\n" +
				"\t\t\t}\n" +
				"\t\t],\n" +
				"\t\t\"primaryKey\": [\n" +
				"\t\t\t\"col2\"\n" +
				"\t\t],\n" +
				"\t\t\"orderBy\": [\n" +
				"\t\t\t\"col1\",\n" +
				"\t\t\t\"col2\",\n" +
				"\t\t\t\"col3\"\n" +
				"\t\t]\n" +
				"\t}\n" +
				"}", ffs.toJsonString().replace("\r", ""));
	}

	@Test
	public void orderByAndKeys()
	{
		DataFileSchema schema = dfm.createDataFileSchema("test");
		schema.setFormatType(DataFileSchema.format_type.delimited);

		// add two columns and expect the order by keys to match, but keys to be empty
		DataFileSchema.Column column = schema.createColumn("COL1");
		schema.addColumn(column);

		column = schema.createColumn("COL2");
		schema.addColumn(column);

		Assert.assertEquals(Collections.emptyList(), schema.getKeyColumnNames());
		Assert.assertEquals(Arrays.asList("COL1", "COL2"), schema.getOrderColumnNames());

		DataFileSchemaView schemaSub = schema.createSubViewExcludingColumns(Arrays.asList("COL1"), "sub");

		Assert.assertEquals(Collections.emptyList(), schemaSub.getKeyColumnNames());
		Assert.assertEquals(Arrays.asList("COL2"), schemaSub.getOrderColumnNames());

		column = schema.createColumn("COL3");
		schema.addColumn(column);
		schema.addKeyColumn("COL3");

		Assert.assertEquals(Arrays.asList("COL3"), schema.getKeyColumnNames());
		Assert.assertEquals(Arrays.asList("COL1", "COL2", "COL3"), schema.getOrderColumnNames());

		schemaSub = schema.createSubViewIncludingColumns(Arrays.asList("COL2", "COL3"), "sub");

		Assert.assertEquals(Arrays.asList("COL3"), schemaSub.getKeyColumnNames());
		Assert.assertEquals(Arrays.asList("COL2", "COL3"), schemaSub.getOrderColumnNames());

		schemaSub = schema.createSubViewIncludingColumns(Arrays.asList("COL2"), "sub");

		Assert.assertEquals(Arrays.asList("COL3"), schemaSub.getKeyColumnNames());
		Assert.assertEquals(Arrays.asList("COL2"), schemaSub.getOrderColumnNames());

		DataFileSchema ssc = schemaSub.materialize();

		Assert.assertEquals(Collections.EMPTY_LIST, ssc.getKeyColumnNames());
		Assert.assertEquals(Arrays.asList("COL2"), ssc.getOrderColumnNames());
	}

	@Test
	public void subViewChangingType()
	{
		DataFileSchema schema = dfm.createDataFileSchema("fixed");
		schema.setFormatType(DataFileSchema.format_type.fixed);
		DataFileSchema.Column col = schema.createColumn("COL");
		col.setLength(1);
		schema.addColumn(col);

		Assert.assertNull(schema.getColumnDelimiter());

		DataFileSchemaView schemaView = schema.createSubViewIncludingColumns(null, "delimited", DataFileSchema.format_type.delimited);

		Assert.assertEquals(dfm.getDefaultColumnDelimiter(), schemaView.getColumnDelimiter());
	}

	@Test
	public void escapingCrud() throws IOException {
		String colData = getBinaryString();
		String colData2 = StringUtils.reverse(colData);

		DataFileSchema schema = dfm.createDataFileSchema("escaping");

		schema.setFormatType(DataFileSchema.format_type.delimited);
		schema.setColumnDelimiter("\t");
		schema.setRowDelimiter("\r\n");

		schema.addColumn(schema.createColumn("col1"));
		schema.addColumn(schema.createColumn("col2"));

		File dataFile = temporaryFolder.newFile();
		DataFile df = dfm.loadDataFile(dataFile, schema);

		DataFileWriter writer = df.writer();

		Map<String, Object> rowData = new HashMap<String, Object>();

		rowData.put("col1", colData);
		rowData.put("col2", colData2);

		writer.addRow(rowData);

		rowData.put("col2", colData);
		rowData.put("col1", colData2);

		writer.addRow(rowData);

		writer.close();

		DataFileReader fd = df.reader();

		/** Remember that the output is ordered, so it won't appear in the same order
		 * it was written, unless that happens to be in ascending string order already **/
		Iterator<DataFileReader.FileRow> it = fd.iterator();

		Assert.assertTrue(it.hasNext());
		DataFileReader.FileRow ne = it.next();

		Map<String, Object> data = ne.getData();
		Assert.assertEquals(colData, data.get("col1"));
		Assert.assertEquals(colData2, data.get("col2"));

		Assert.assertTrue(it.hasNext());
		it.next();

		Assert.assertEquals(colData2, ne.getData().get("col1"));
		Assert.assertEquals(colData, ne.getData().get("col2"));

		Assert.assertFalse(it.hasNext());
	}

	@Test
	public void escapingAndNonEscaping() throws IOException {
		String text = "i" + getBinaryString().substring(0, 18) + "i";

		DataFileSchema escDelimited = dfm.loadDataFileSchemaFromResource("ffml/escaping-delimited.ffml", "");
		DataFileSchema escFixed = dfm.loadDataFileSchemaFromResource("ffml/escaping-fixed.ffml", "");
		DataFileSchema nonEscDelimited = dfm.loadDataFileSchemaFromResource("ffml/non-escaping-delimited.ffml", "");
		DataFileSchema nonEscFixed = dfm.loadDataFileSchemaFromResource("ffml/non-escaping-fixed.ffml", "");

		File escDelimFile = temporaryFolder.newFile("escDelimFile");
		File escFixedFile = temporaryFolder.newFile("escFixedFile");
		File nonEscDelimFile = temporaryFolder.newFile("nonEscDelimFile");
		File nonEscFixedFile = temporaryFolder.newFile("nonEscFixedFile");

		DataFile escDelimDataFile = dfm.loadDataFile(escDelimFile, escDelimited);
		DataFile escFixedDataFile = dfm.loadDataFile(escFixedFile, escFixed);
		DataFile nonEscDelimDataFile = dfm.loadDataFile(nonEscDelimFile, nonEscDelimited);
		DataFile nonEscFixedDataFile = dfm.loadDataFile(nonEscFixedFile, nonEscFixed);

		// write the binary data into the escaped delimited file
		DataFileWriter writer = escDelimDataFile.writer();

		Map<String, Object> rowData = new HashMap<String, Object>();

		rowData.put("store_number", text);
		rowData.put("sales_date", text);

		writer.addRow(rowData);

		rowData.put("store_number", text);
		rowData.put("sales_date", text);

		writer.addRow(rowData);

		writer.close();

		//System.out.println(escFixedFile.getAbsolutePath());
		//System.out.println(FileUtils.readFileToString(escDelimFile));

		// transfer from escaped delimited into non-escaped delimited
		dfm.copyDataFile(escDelimDataFile, nonEscDelimDataFile);

		//System.out.println(FileUtils.readFileToString(nonEscDelimFile));

		// copy to non-escaped fixed
		dfm.copyDataFile(nonEscDelimDataFile, nonEscFixedDataFile);

		//System.out.println(FileUtils.readFileToString(nonEscFixedFile));

		// copy to escaped fixed
		dfm.copyDataFile(nonEscFixedDataFile, escFixedDataFile);

		//System.out.println(FileUtils.readFileToString(escFixedFile));

		Iterator<DataFileReader.FileRow> it = escFixedDataFile.reader().iterator();

		int count = 0;

		while (it.hasNext())
		{
			count++;

			DataFileReader.FileRow nextRow = it.next();
			Map<String, Object> data = nextRow.getData();

			// assert both columns
			Assert.assertEquals(text, data.get("store_number"));
			Assert.assertEquals(text, data.get("sales_date"));
		}

		Assert.assertEquals(2, count);
	}

	private String getBinaryString() throws IOException {
		File source = temporaryFolder.newFile();

		OutputStream fout = new BufferedOutputStream(new FileOutputStream(source));

		for (int i = 0; i <= 255; i++)
		{
			fout.write((byte) i & 0xFF);
		}

		fout.close();

		return FileUtils.readFileToString(source);
	}

	@Test
	public void testFileSchemaResolution() throws IOException {
		File f = temporaryFolder.newFile();

		FileUtils.copyInputStreamToFile(getClass().getResourceAsStream("/ffml/delimited-test.ffml"), f);

		DataFileSchema schemaFile = dfm.loadDataFileSchema(f, "id");
		DataFileSchema schemaCp = dfm.loadDataFileSchemaFromResource("ffml/delimited-test.ffml", "id");

		Assert.assertEquals(schemaCp.toString(), schemaFile.toString());
	}

	@Test
	public void formatTypeViews()
	{
		DataFileSchema dfs = dfm.createDataFileSchema("blah");

		DataFileSchema.Column col = dfs.createColumn("id");
		dfs.addColumn(col);

		// create a view of the schema to test which tracks the format type of the parent
		DataFileSchemaView dfv = dfs.createSubViewIncludingColumns(Arrays.asList("id"), "idview");
		Assert.assertEquals(dfs.getFormatType(), dfv.getFormatType());

		dfs.setFormatType(DataFileSchema.format_type.fixed);
		Assert.assertEquals(dfs.getFormatType(), dfv.getFormatType());

		dfs.setFormatType(DataFileSchema.format_type.delimited);
		Assert.assertEquals(dfs.getFormatType(), dfv.getFormatType());

		dfs.setFormatType(DataFileSchema.format_type.fixed);
		Assert.assertEquals(dfs.getFormatType(), dfv.getFormatType());
	}

	@Test
	public void attributesPassThroughMaterialization()
	{
		DataFileSchema dfs = dfm.createDataFileSchema("blah");

		DataFileSchema.Column col = dfs.createColumn("id");
		dfs.addColumn(col);

		// create a view of the schema to test
		DataFileSchemaView dfv = dfs.createSubViewIncludingColumns(Arrays.asList("id"), "idview");

		// verify that all properties match
		Assert.assertEquals(dfs.escapeNonPrintable(), dfv.escapeNonPrintable());
		Assert.assertEquals(dfs.getRowDelimiter(), dfv.getRowDelimiter());
		Assert.assertEquals(dfs.getColumnDelimiter(), dfv.getColumnDelimiter());
		Assert.assertEquals(dfs.getNullToken(), dfv.getNullToken());
		Assert.assertEquals(dfs.getFormatType(), dfv.getFormatType());

		// change them up and see that they still match through the view
		dfs.setEscapeNonPrintable(false);
		dfs.setColumnDelimiter("[");
		dfs.setRowDelimiter("]");
		dfs.setNullToken("nil");
		dfs.setFormatType(DataFileSchema.format_type.delimited);

		Assert.assertEquals(dfs.escapeNonPrintable(), dfv.escapeNonPrintable());
		Assert.assertEquals(dfs.getRowDelimiter(), dfv.getRowDelimiter());
		Assert.assertEquals(dfs.getColumnDelimiter(), dfv.getColumnDelimiter());
		Assert.assertEquals(dfs.getNullToken(), dfv.getNullToken());
		Assert.assertEquals(dfs.getFormatType(), dfv.getFormatType());

		dfs.setFormatType(DataFileSchema.format_type.fixed);

		Assert.assertEquals(dfs.escapeNonPrintable(), dfv.escapeNonPrintable());
		Assert.assertEquals(dfs.getRowDelimiter(), dfv.getRowDelimiter());
		Assert.assertEquals(dfs.getColumnDelimiter(), dfv.getColumnDelimiter());
		Assert.assertEquals(dfs.getNullToken(), dfv.getNullToken());

		dfs.setFormatType(DataFileSchema.format_type.delimited);
		dfs.setColumnDelimiter("[");

		// materialize the view, and see that attributes still match
		DataFileSchema dfmat = dfv.materialize();

		Assert.assertEquals(dfs.escapeNonPrintable(), dfmat.escapeNonPrintable());
		Assert.assertEquals(dfs.getRowDelimiter(), dfmat.getRowDelimiter());
		Assert.assertEquals(dfs.getColumnDelimiter(), dfmat.getColumnDelimiter());
		Assert.assertEquals(dfs.getNullToken(), dfmat.getNullToken());

		// now change the root and see that they are different now
		dfs.setEscapeNonPrintable(true);
		dfs.setColumnDelimiter("!");
		dfs.setRowDelimiter("@");
		dfs.setNullToken("not");
		dfs.setFormatType(DataFileSchema.format_type.fixed);

		Assert.assertEquals(false, dfmat.escapeNonPrintable());
		Assert.assertEquals("]", dfmat.getRowDelimiter());
		Assert.assertEquals("[", dfmat.getColumnDelimiter());
		Assert.assertEquals("nil", dfmat.getNullToken());
		Assert.assertEquals(DataFileSchema.format_type.delimited, dfmat.getFormatType());
	}

	@Test
	public void narrowIncludingCaseInsensitiveNull() {
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr.ffml", "dsr");
		DataFileSchemaView sview = ffs.createSubViewIncludingColumns(null, "newid");
	}

	@Test
	public void narrowIncludingCaseInsensitive() {
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr.ffml", "dsr");
		DataFileSchemaView sview = ffs.createSubViewIncludingColumns(Arrays.asList("store_number"), "newid");
	}

	@Test
	public void narrowExcludingCaseInsensitiveNull() {
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr.ffml", "dsr");
		DataFileSchemaView sview = ffs.createSubViewExcludingColumns(null, "newid");
	}

	@Test
	public void narrowExcludingCaseInsensitive() {
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/dsr.ffml", "dsr");
		DataFileSchemaView sview = ffs.createSubViewExcludingColumns(Arrays.asList("store_number"), "newid");
	}

	@Test
	public void orderByCaseInsensitive() {
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/order.ffml", "dsr");
		Assert.assertEquals(Arrays.asList("store_number", "sales_date"), ffs.getOrderColumnNames());
	}

	@Test
	public void ordinals() {
		DataFileSchema ffs = dfm.loadDataFileSchemaFromResource("ffml/ordinals.ffml", "dsr");
		Assert.assertEquals(4, ffs.getLogicalColumnNames().size());
		Assert.assertEquals("dsr_category", ffs.getLogicalColumnNames().get(0));
		Assert.assertEquals("store_number", ffs.getLogicalColumnNames().get(1));
		Assert.assertEquals("sales_date2", ffs.getLogicalColumnNames().get(2));
		Assert.assertEquals("sales_date", ffs.getLogicalColumnNames().get(3));
	}
}
