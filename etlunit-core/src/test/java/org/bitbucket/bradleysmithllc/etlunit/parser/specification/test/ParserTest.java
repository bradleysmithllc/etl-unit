package org.bitbucket.bradleysmithllc.etlunit.parser.specification.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.parser.specification.TestSpecificationParser;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ParserTest
{
	@Test(expected = ParseException.class)
	public void empty() throws ParseException {
		TestSpecificationParser.parse("");
	}

	@Test
	public void className() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void classAndMethodName() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test#meth");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void classAndOperationName() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test$oper");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void classMethodAndOperationName() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test#meth$oper");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void methodName() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("#test");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void methodAndOperationName() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("#test$oper");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void operationName() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("$test");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void binary() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test and tost");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.and, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void not() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test not #clast");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.not, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void eor() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test eor #clast");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.eor, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void and() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test and #clast");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.and, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void or() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("test or #clast");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.or, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void compound() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("(test and tost)");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));
	}

	@Test
	public void compoundBinary() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("(test and tost) or $oper");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(ConditionalDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.or, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(PatternClassDirector.class));

		cd = (ConditionalDirector) cd.getLdirector();

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.and, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void compoundBinaryInv() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("$oper or (test and tost)");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.or, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(ConditionalDirector.class));

		cd = (ConditionalDirector) cd.getRdirector();

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.and, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void compoundCompound() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("(#test and $tost) or (test#meth and tost#meth$oper)");

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(ConditionalDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.or, cd.getCondition());
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cdl = (ConditionalDirector) cd.getLdirector();

		Assert.assertThat(cdl.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.and, cdl.getCondition());
		Assert.assertThat(cdl.getRdirector(), new IsInstanceOf(PatternClassDirector.class));

		ConditionalDirector cdr = (ConditionalDirector) cd.getRdirector();

		Assert.assertThat(cdr.getLdirector(), new IsInstanceOf(PatternClassDirector.class));
		Assert.assertEquals(ConditionalDirector.condition.and, cdr.getCondition());
		Assert.assertThat(cdr.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test(expected = IllegalStateException.class)
	public void suiteLeadingCommaTest() throws ParseException {
		TestSpecificationParser.parse(
				"[,a]"
		);
	}

	@Test(expected = IllegalStateException.class)
	public void propertyLeadingCommaTest() throws ParseException {
		TestSpecificationParser.parse(
				"{,a}"
		);
	}

	@Test
	public void propertyTest() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse(
				"{a: 's' b.c: 'v' c.e: 'f' d.f-e: 'e'}"
		);

		Assert.assertThat(cl, new IsInstanceOf(OperationClassDirector.class));

		OperationClassDirector ocd = (OperationClassDirector) cl;

		Assert.assertEquals(4, ocd.getOperationList().size());

		Assert.assertNull(ocd.getOperationList().get(0).getOperationName());
		Assert.assertEquals("a", ocd.getOperationList().get(0).getPropertyName());
		Assert.assertEquals("s", ocd.getOperationList().get(0).getPropertyStringValue());

		Assert.assertEquals("b", ocd.getOperationList().get(1).getOperationName());
		Assert.assertEquals("c", ocd.getOperationList().get(1).getPropertyName());
		Assert.assertEquals("v", ocd.getOperationList().get(1).getPropertyStringValue());

		cl = TestSpecificationParser.parse(
				"{a: 's' b.c: 'v' c.e: 'f' d.f-e: 'e'} or [agg]"
		);

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cd = (ConditionalDirector) cl;

		Assert.assertThat(cd.getLdirector(), new IsInstanceOf(OperationClassDirector.class));
		Assert.assertThat(cd.getRdirector(), new IsInstanceOf(SuiteClassDirectorImpl.class));
	}

	@Test
	public void suiteTest() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse(
			"[a b c d]"
		);

		Assert.assertThat(cl, new IsInstanceOf(SuiteClassDirectorImpl.class));

		cl = TestSpecificationParser.parse(
				"[a,b,c,d] and test"
		);

		Assert.assertThat(cl, new IsInstanceOf(ConditionalDirector.class));

		ConditionalDirector cdr = (ConditionalDirector) cl;

		Assert.assertThat(cdr.getLdirector(), new IsInstanceOf(SuiteClassDirectorImpl.class));
		Assert.assertEquals(ConditionalDirector.condition.and, cdr.getCondition());
		Assert.assertThat(cdr.getRdirector(), new IsInstanceOf(PatternClassDirector.class));
	}

	@Test
	public void suiteConditionTest() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse(
				"[a b! c+ d?, e]"
		);

		Assert.assertThat(cl, new IsInstanceOf(SuiteClassDirectorImpl.class));
		SuiteClassDirectorImpl cdr = (SuiteClassDirectorImpl) cl;

		List<SuiteClassDirectorImpl.SuiteRef> sr = cdr.getSuiteRefs();

		Assert.assertEquals(5, sr.size());

		Assert.assertEquals("a", sr.get(0).getSuiteName());
		Assert.assertEquals(SuiteClassDirectorImpl.required_type.include, sr.get(0).getType());

		Assert.assertEquals("b", sr.get(1).getSuiteName());
		Assert.assertEquals(SuiteClassDirectorImpl.required_type.exclude, sr.get(1).getType());

		Assert.assertEquals("c", sr.get(2).getSuiteName());
		Assert.assertEquals(SuiteClassDirectorImpl.required_type.include, sr.get(2).getType());

		Assert.assertEquals("d", sr.get(3).getSuiteName());
		Assert.assertEquals(SuiteClassDirectorImpl.required_type.optional, sr.get(3).getType());

		Assert.assertEquals("e", sr.get(4).getSuiteName());
		Assert.assertEquals(SuiteClassDirectorImpl.required_type.include, sr.get(4).getType());
	}

	@Test
	public void classNameWithPipes() throws ParseException, org.bitbucket.bradleysmithllc.etlunit.parser.ParseException {
		ETLTestClass testClass = ETLTestParser.load("class test_class {}").get(0);
		ETLTestClass classTest = ETLTestParser.load("class class_test {}").get(0);
		ETLTestClass test = ETLTestParser.load("class test {}").get(0);
		ETLTestClass tost = ETLTestParser.load("class tost {}").get(0);

		// regression
		ClassDirector cl = TestSpecificationParser.parse("test");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(classTest));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(test));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(tost));

		// this director will match any class name which STARTS with 'test'
		cl = TestSpecificationParser.parse("|test");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(classTest));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(test));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(tost));

		// this director will match any class name which STARTS with 'test'
		cl = TestSpecificationParser.parse("test|");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));

		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(classTest));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(test));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(tost));

		// this director will match any class name which is exactly 'test'
		cl = TestSpecificationParser.parse("|test|");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));

		// this director will match any class name which STARTS with 'test'
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(classTest));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(test));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(tost));
	}

	@Test
	public void methodNameWithPipes() throws ParseException, org.bitbucket.bradleysmithllc.etlunit.parser.ParseException {
		ETLTestClass testClass = ETLTestParser.load("class test_class {@Test testMethod(){} @Test methodTest(){} @Test test(){} @Test tost(){}}").get(0);
		ETLTestMethod testMethod = testClass.getTestMethods().get(0);
		ETLTestMethod methodTest = testClass.getTestMethods().get(1);
		ETLTestMethod test = testClass.getTestMethods().get(2);
		ETLTestMethod tost = testClass.getTestMethods().get(3);

		// regression
		ClassDirector cl = TestSpecificationParser.parse("#test");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testMethod));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(methodTest));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(test));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(tost));

		// this director will match any class name which STARTS with 'test'
		cl = TestSpecificationParser.parse("#|test");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testMethod));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(methodTest));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(test));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(tost));

		// this director will match any class name which STARTS with 'test'
		cl = TestSpecificationParser.parse("#test|");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));

		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testMethod));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(methodTest));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(test));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(tost));

		// this director will match any class name which is exactly 'test'
		cl = TestSpecificationParser.parse("#|test|");

		Assert.assertThat(cl, new IsInstanceOf(PatternClassDirector.class));

		// this director will match any class name which STARTS with 'test'
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testMethod));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(methodTest));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(test));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(tost));
	}

	@Test(expected = ParseException.class)
	public void badClassNameWithPipes() throws ParseException {
		ClassDirector cl = TestSpecificationParser.parse("te|st");
	}

	@Test
	public void packageName() throws Exception {
		ETLTestClass testClass = ETLTestParser.load("class test_class {}").get(0);

		ClassDirector cl = TestSpecificationParser.parse("%pack");

		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("packageuy")).get(0);
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upack")).get(0);
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upacky")).get(0);
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class_pack {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upacy")).get(0);
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class_pack {}", ETLTestPackageImpl.getDefaultPackage()).get(0);
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));
	}

	@Test
	public void packageNamePipeBegin() throws Exception {
		ClassDirector cl = TestSpecificationParser.parse("%|pack");

		ETLTestClass testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("packageuy")).get(0);
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upack")).get(0);
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upacky")).get(0);
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));
	}

	@Test
	public void packageNamePipeEnd() throws Exception {
		ClassDirector cl = TestSpecificationParser.parse("%pack|");

		ETLTestClass testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("packageuy")).get(0);
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upack")).get(0);
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upacky")).get(0);
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));
	}

	@Test
	public void packageNamePipeBoth() throws Exception {
		ClassDirector cl = TestSpecificationParser.parse("%|pack|");

		ETLTestClass testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack")).get(0);
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upack")).get(0);
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));

		testClass = ETLTestParser.load("class test_class {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("upacky")).get(0);
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass));
	}

	@Test
	public void classNames() throws Exception {
		List<ETLTestClass> testClass = ETLTestParser.load("class cls {} class _cls {} class cls_ {} class _cls_ {}");

		// must match all three
		ClassDirector cl = TestSpecificationParser.parse("@cls");

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(1)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(2)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(3)));

		// must match just the first and third ones
		cl = TestSpecificationParser.parse("@|cls");

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(1)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(2)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(3)));

		// must match just the first and second ones
		cl = TestSpecificationParser.parse("@cls|");

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(1)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(2)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(3)));

		// must match just the first one
		cl = TestSpecificationParser.parse("@|cls|");

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(1)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(2)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(3)));
	}

	/**
	 * Once a class name is provided, package is disallowed
	 * @throws ParseException
	 */
	@Test(expected = ParseException.class)
	public void packageWithQClass() throws ParseException {
		TestSpecificationParser.parse("pack%pack");
	}

	/**
	 * Once a class qname is provided, class is disallowed
	 * @throws ParseException
	 */
	@Test(expected = ParseException.class)
	public void classWithQClass() throws ParseException {
		TestSpecificationParser.parse("pack@pack");
	}

	@Test
	public void packageAndClassNames() throws Exception {
		ClassDirector cl = TestSpecificationParser.parse("%pack@cls");

		List<ETLTestClass> testClass = ETLTestParser.load("class cls {} class csl {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("pack"));

		// accept class one
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(1)));

		testClass = ETLTestParser.load("class cls {} class csl {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("pick"));

		// accept class one
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(1)));
	}

	@Test
	public void matchDefaultPackage() throws Exception {
		ClassDirector cl = TestSpecificationParser.parse("%[default]");

		List<ETLTestClass> testClass = ETLTestParser.load("class cls {} class csl {}");

		// accept class one
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(1)));

		testClass = ETLTestParser.load("class cls {} class csl {}", ETLTestPackageImpl.getDefaultPackage().getSubPackage("name"));

		// accept class one
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(1)));
	}

	@Test
	public void match_eor() throws Exception {
		// match if the class name is cls, and the method name is NOT meth,
		// or if the method name is #meth and the class name is NOT cls
		ClassDirector cl = TestSpecificationParser.parse("#meth eor @cls");

		List<ETLTestClass> testClass = ETLTestParser.load(
			"class csl {@Test one(){} @Test two(){} @Test three(){}} class cls {}"
		);

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(1)));

		cl = TestSpecificationParser.parse("@csl eor @cls");

		testClass = ETLTestParser.load(
				"class csl {@Test one(){} @Test two(){} @Test three(){}} class cls {} class lsl{}"
		);

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(1)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(2)));
	}

	@Test
	public void match_not() throws Exception {
		// match every method named meth, except any class named cls
		ClassDirector cl = TestSpecificationParser.parse("#meth not @cls");

		List<ETLTestClass> testClass = ETLTestParser.load(
				"class cls { @Test meth1(){} @Test meth2(){}} class csl {@Test meth1(){} @Test meth2(){}}"
		);

		// classes
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(1)));

		//tests
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(0).getTestMethods().get(0)));
		Assert.assertEquals(ClassResponder.response_code.reject, cl.accept(testClass.get(0).getTestMethods().get(1)));

		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(1).getTestMethods().get(0)));
		Assert.assertEquals(ClassResponder.response_code.accept, cl.accept(testClass.get(1).getTestMethods().get(1)));
	}

	@Test
	public void limit() throws Exception {
		// match every method named meth, except any class named cls
		ClassDirector cl = TestSpecificationParser.parse("^limit 1");

		List<ETLTestClass> testClass = ETLTestParser.load(
				"class cls { @Test meth1(){} @Test meth2(){}} class csl {@Test meth1(){} @Test meth2(){}}"
		);
	}

	@Test
	public void tags() throws Exception {
		// match every method named meth, except any class named cls
		ClassDirector cl = TestSpecificationParser.parse("<failed>");

		Assert.assertTrue(cl instanceof TagDirector);
	}
}
