package org.bitbucket.bradleysmithllc.etlunit.metadata.impl.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.metadata.impl.TestReferenceImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class TestReferenceTest extends JsonSerializableBase
{
	@Test
	public void nullPackages() throws IOException {
		testJson(new TestReferenceImpl(
				"classi",
				ETLTestPackageImpl.getDefaultPackage(),
				"class",
				"name",
				"class.name"
		));
	}

	@Test
	public void packages() throws IOException {
		testJson(new TestReferenceImpl(
				"classi",
				ETLTestPackageImpl.getDefaultPackage().getSubPackage("package"),
				"class",
				"name",
				"package.class.name"
		));
	}

	@Override
	protected JsonSerializable newObject() {
		return new TestReferenceImpl();
	}

	@Test
	public void compareIgnoresClassification()
	{
		TestReferenceImpl tr1 = new TestReferenceImpl(
				"classi1",
				ETLTestPackageImpl.getDefaultPackage().getSubPackage("package"),
				"class",
				"name",
				"package.class.name"
		);

		TestReferenceImpl tr2 = new TestReferenceImpl(
				"classi2",
				ETLTestPackageImpl.getDefaultPackage().getSubPackage("package"),
				"class",
				"name",
				"package.class.name"
		);

		Assert.assertEquals(0, tr1.compareTo(tr2));
		Assert.assertEquals(0, tr2.compareTo(tr1));
	}

	@Test
	public void nullClassification()
	{
		TestReferenceImpl tr1 = new TestReferenceImpl(
				null,
				ETLTestPackageImpl.getDefaultPackage().getSubPackage("package"),
				"class",
				"name",
				"package.class.name"
		);

		TestReferenceImpl tr2 = new TestReferenceImpl(
				null,
				ETLTestPackageImpl.getDefaultPackage().getSubPackage("package"),
				"class",
				"name",
				"package.class.name"
		);

		Assert.assertEquals(0, tr1.compareTo(tr2));
		Assert.assertEquals(0, tr2.compareTo(tr1));
	}

	@Test
	public void compareFails()
	{
		TestReferenceImpl tr1 = new TestReferenceImpl(
				null,
				ETLTestPackageImpl.getDefaultPackage().getSubPackage("package"),
				"class",
				"name",
				"package.class.name1"
		);

		TestReferenceImpl tr2 = new TestReferenceImpl(
				null,
				ETLTestPackageImpl.getDefaultPackage().getSubPackage("package"),
				"class",
				"name",
				"package.class.name2"
		);

		Assert.assertEquals(-1, tr1.compareTo(tr2));
		Assert.assertEquals(1, tr2.compareTo(tr1));
	}
}
