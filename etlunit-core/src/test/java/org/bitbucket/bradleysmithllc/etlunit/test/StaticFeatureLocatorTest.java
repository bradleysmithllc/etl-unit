package org.bitbucket.bradleysmithllc.etlunit.test;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.StaticFeatureLocator;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StaticFeatureLocatorTest
{
	@Test
	public void worksEmpty()
	{
		StaticFeatureLocator locator = new StaticFeatureLocator(new ArrayList<Feature>());

		Assert.assertTrue(locator.getFeatures().isEmpty());
	}

	@Test
	public void elementsStayInOrder()
	{
		List<Feature> l = new ArrayList<Feature>();
		l.add(new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "one";
			}
		});

		l.add(new AbstractFeature()
		{
			@Override
			public String getFeatureName()
			{
				return "two";
			}
		});

		StaticFeatureLocator locator = new StaticFeatureLocator(l);

		Assert.assertEquals(2, locator.getFeatures().size());
		Assert.assertEquals("one", locator.getFeatures().get(0).getFeatureName());
		Assert.assertEquals("two", locator.getFeatures().get(1).getFeatureName());
	}
}
