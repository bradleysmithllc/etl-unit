PARSER_BEGIN(DataFileDiffNearParser)
package org.bitbucket.bradleysmithllc.etlunit.parser.datadiff;

import java.io.StringReader;
import java.util.concurrent.TimeUnit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.time.ZonedDateTime;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

/*
 * Summary:  	This parser has to be fail-very-fast, since it will get a shot
 *				at every single string value in table comparisons.  Data diff
 *				specifications can never be compared to each other, and both
 *				data points must be a string for this to work.
 *
 * Dates:		Similar to timestamp, but works on dates instead.
 *
 *				Prefix:
 *					dt
 *				Format:
 *
 *					(baseValue='yyyy-MM-dd')*(,baseUnit=DAYS)*,(unitQuantity=<number>)*
 *
 *					All values are optional.  baseValue defaults to current_date, baseUnit defaults
 *					to DAY, and unitQuantity defaults to 0.
 *
 *					baseUnit must match a value in java.util.concurrent.TimeUnit.
 *					unitQuantity is the number of those units the value can differ by and be considered valid.
 *
 * Timestamps:  A timestamp specification tells the diff to allow a timestamp in one
 *				table to differ from another one by some quantity of time units.
 * *				Prefix:
 *					ts
 *				Format:
 *					(baseValue='yyyy-MM-dd HH:mm:ss.SSS')*(,baseUnit=SECONDS)*,(unitQuantity=<number>)*
 *
 *					All values are optional.  baseValue defaults to current_timestamp, baseUnit defaults
 *					to MINUTE, and unitQuantity defaults to 1.
 *
 *					baseUnit must match a value in java.util.concurrent.TimeUnit.
 *					unitQuantity is the number of those units the value can differ by and be considered valid.
*/

public class DataFileDiffNearParser {
	public DataFileDiffNearParser(String input) {
		this(new StringReader(input));
	}

	public static DataFileDiffNearSpecification optionallyScanForDataDiffSpecification(String input) {
		DataFileDiffNearParser dataDiffSpec = new DataFileDiffNearParser(input);

		try {
			DataFileDiffNearSpecification dataFileDiffNearSpecification = dataDiffSpec.readSpecification();
			return dataFileDiffNearSpecification;
		} catch (ParseException parseExc) {
			return null;
		} catch (TokenMgrException parseExc) {
			return null;
		}
	}

	public static int assertNumberLength(String number, String type, int requiredExactLength) throws ParseException {
		if (number.length() != requiredExactLength) {
			throw new ParseException("Numeric value for " + type + " must be exactly " + requiredExactLength + " digits: " + number);
		}

		return Integer.parseInt(number);
	}

	public static int assertNumberMaxLength(String number, String type, int requiredMaxLength) throws ParseException {
		if (number.length() > requiredMaxLength) {
			throw new ParseException("Numeric value for " + type + " must be at most " + requiredMaxLength + " digits: " + number);
		}

		return Integer.parseInt(number);
	}
}
PARSER_END(DataFileDiffNearParser)

TOKEN :
{
		< DIFF_PREFIX: "<!--" >
|		< DIFF_SUFFIX: "-->" >
|		< TS_MARKER: "ts" >
|		< TM_MARKER: "tm" >
|		< DT_MARKER: "dt" >
|		< BODY_BEGIN_MARKER: "(" >
|		< BODY_END_MARKER: ")" >
|		< SPACE: " " >
|		< QUOTE: "'" >
|		< DASH: "-" >
|		< COLON: ":" >
|		< DOT: "." >
|		< EQUALS: "=" >
|		< COMMA: "," >
|		< CURRENT_TIMESTAMP: "current_timestamp" >
|		< BASEVALUE: "baseValue" >
|		< BASEUNIT: "baseUnit" >
|		< UNITQUANTITY: "unitQuantity" >
|		< MICROSECONDS: "MICROSECONDS" >
|		< MILLISECONDS: "MILLISECONDS" >
|		< SECONDS: "SECONDS" >
|		< MINUTES: "MINUTES" >
|		< HOURS: "HOURS" >
|		< DAYS: "DAYS" >
|		< #DIGIT: ["0"-"9"] >
|		< NUMBER: (<DIGIT>)+ >
}

DataFileDiffNearSpecification readSpecification() :
{
	DataFileDiffNearSpecification dataFileDiffNearSpecification;
}
{
	<DIFF_PREFIX>
	(
		dataFileDiffNearSpecification = timestamp()
		|
		dataFileDiffNearSpecification = date()
		|
		dataFileDiffNearSpecification = time()
	)
	<DIFF_SUFFIX> <EOF>
	{
		return dataFileDiffNearSpecification;
	}
}

DataFileDiffNearSpecification date():
{
	ZonedDateTime compareToDate = null;
	TimeUnit timeUnit = TimeUnit.DAYS;
	int quantity = 0;
	Token baseValueToken = null;
	Token baseUnitToken = null;
	Token commaToken = null;
	Token token = null;
}
{
	<DT_MARKER>
	(
		<BODY_BEGIN_MARKER>
			(
				baseValueToken = <BASEVALUE> <EQUALS> compareToDate = literalTimestampValue()
			)?
			(
				LOOKAHEAD(2)
				(commaToken = <COMMA>
				{
					if (baseValueToken == null) {
						// Illegal - no comma if this is the first expression
						throw new ParseException("Do not lead with comma");
					}
				}
				)?
				baseUnitToken = <BASEUNIT> <EQUALS> timeUnit = timeunit()
			)?
			(
				(
					<COMMA>
					{
						if (baseValueToken == null && baseUnitToken == null) {
							// Illegal - no comma if this is the first expression
							throw new ParseException("Do not lead with comma");
						}
					}
				)?
				<UNITQUANTITY> <EQUALS> token = <NUMBER> {quantity = Integer.parseInt(token.image);}
			)?
		<BODY_END_MARKER>
	)?
	{
		return new DataFileDiffNearSpecification(
			DataFileDiffNearSpecification.specification_type.date,
			new DateNearSpecification(
				compareToDate,
				timeUnit,
				quantity
			)
		);
	}
}

DataFileDiffNearSpecification time():
{
	LocalTime compareToDate = null;
	TimeUnit timeUnit = TimeUnit.MINUTES;
	int quantity = 1;
	Token baseValueToken = null;
	Token baseUnitToken = null;
	Token commaToken = null;
	Token token = null;
}
{
	<TM_MARKER>
	(
		<BODY_BEGIN_MARKER>
			(
				baseValueToken = <BASEVALUE> <EQUALS> compareToDate = literalTimeValue()
			)?
			(
				LOOKAHEAD(2)
				(commaToken = <COMMA>
				{
					if (baseValueToken == null) {
						// Illegal - no comma if this is the first expression
						throw new ParseException("Do not lead with comma");
					}
				}
				)?
				baseUnitToken = <BASEUNIT> <EQUALS> timeUnit = timeunit()
			)?
			(
				(
					<COMMA>
					{
						if (baseValueToken == null && baseUnitToken == null) {
							// Illegal - no comma if this is the first expression
							throw new ParseException("Do not lead with comma");
						}
					}
				)?
				<UNITQUANTITY> <EQUALS> token = <NUMBER> {quantity = Integer.parseInt(token.image);}
			)?
		<BODY_END_MARKER>
	)?
	{
		return new DataFileDiffNearSpecification(
			DataFileDiffNearSpecification.specification_type.date,
			new TimeNearSpecification(
				compareToDate,
				timeUnit,
				quantity
			)
		);
	}
}

DataFileDiffNearSpecification timestamp():
{
	ZonedDateTime compareToDate = null;
	TimeUnit timeUnit = TimeUnit.MINUTES;
	int quantity = 1;
	Token baseValueToken = null;
	Token baseUnitToken = null;
	Token commaToken = null;
	Token token = null;
}
{
	<TS_MARKER>
	(
		<BODY_BEGIN_MARKER>
			(
				baseValueToken = <BASEVALUE> <EQUALS> compareToDate = literalTimestampValue()
			)?
			(
				LOOKAHEAD(2)
				(commaToken = <COMMA>
				{
					if (baseValueToken == null) {
						// Illegal - no comma if this is the first expression
						throw new ParseException("Do not lead with comma");
					}
				}
				)?
				baseUnitToken = <BASEUNIT> <EQUALS> timeUnit = timeunit()
			)?
			(
				(
					<COMMA>
					{
						if (baseValueToken == null && baseUnitToken == null) {
							// Illegal - no comma if this is the first expression
							throw new ParseException("Do not lead with comma");
						}
					}
				)?
				<UNITQUANTITY> <EQUALS> token = <NUMBER> {quantity = Integer.parseInt(token.image);}
			)?
		<BODY_END_MARKER>
	)?
	{
		return new DataFileDiffNearSpecification(
			DataFileDiffNearSpecification.specification_type.timestamp,
			new TimestampNearSpecification(
				compareToDate,
				timeUnit,
				quantity
			)
		);
	}
}

ZonedDateTime literalTimestampValue():
{
	StringBuilder timestampBuilder = new StringBuilder();
	StringBuilder timestampFormatBuilder = new StringBuilder();
	int year = 0;
	int month = 1;
	int day = 1;
	int hour = 0;
	int minute = 0;
	int second = 0;
	int millisecond = 0;
	Token token;
}
{
		<QUOTE>
		/* Year */
		token = <NUMBER> {year = assertNumberLength(token.image, "years", 4);timestampBuilder.append(token.image);timestampFormatBuilder.append("yyyy");}
		(
			<DASH>
			/* Month */
			token = <NUMBER> {month = assertNumberLength(token.image, "months", 2);timestampBuilder.append("-").append(token.image);timestampFormatBuilder.append("-MM");}
			(
				<DASH>
				/* Day */
				token = <NUMBER> {day = assertNumberLength(token.image, "days", 2);timestampBuilder.append("-").append(token.image);timestampFormatBuilder.append("-dd");}
				(
					<SPACE>
					/* Hours */
					token = <NUMBER> {hour = assertNumberLength(token.image, "hours", 2);timestampBuilder.append(" ").append(token.image);timestampFormatBuilder.append(" HH");}
					(
						<COLON>
						/* Minutes */
						token = <NUMBER> {minute = assertNumberLength(token.image, "minutes", 2);timestampBuilder.append(":").append(token.image);timestampFormatBuilder.append(":mm");}
						(
							<COLON>
							/* Seconds */
							token = <NUMBER> {second = assertNumberLength(token.image, "seconds", 2);timestampBuilder.append(":").append(token.image);timestampFormatBuilder.append(":ss");}
							(
								<DOT>
								/* Milliseconds */
								token = <NUMBER> {millisecond = assertNumberMaxLength(token.image, "milliseconds", 3); timestampBuilder.append(".").append(token.image);timestampFormatBuilder.append(".SSS");}
							)?
						)?
					)?
				)?
			)?
		)?
		<QUOTE>
		{
			return LocalDateTime.of(year, month, day, hour, minute, second, (int) TimeUnit.NANOSECONDS.convert(millisecond, TimeUnit.MILLISECONDS)).atZone(ZoneId.of("UTC"));
		}
}


LocalTime literalTimeValue():
{
	StringBuilder timestampBuilder = new StringBuilder();
	StringBuilder timestampFormatBuilder = new StringBuilder();
	int hour = 0;
	int minute = 0;
	int second = 0;
	int millisecond = 0;
	Token token;
}
{
		<QUOTE>
		/* Hours */
		token = <NUMBER> {hour = assertNumberLength(token.image, "hours", 2);timestampBuilder.append(" ").append(token.image);timestampFormatBuilder.append(" HH");}
		(
			<COLON>
			/* Minutes */
			token = <NUMBER> {minute = assertNumberLength(token.image, "minutes", 2);timestampBuilder.append(":").append(token.image);timestampFormatBuilder.append(":mm");}
			(
				<COLON>
				/* Seconds */
				token = <NUMBER> {second = assertNumberLength(token.image, "seconds", 2);timestampBuilder.append(":").append(token.image);timestampFormatBuilder.append(":ss");}
				(
					<DOT>
					/* Milliseconds */
					token = <NUMBER> {millisecond = assertNumberMaxLength(token.image, "milliseconds", 3); timestampBuilder.append(".").append(token.image);timestampFormatBuilder.append(".SSS");}
				)?
			)?
		)?
		<QUOTE>
		{
			return LocalTime.of(hour, minute, second, (int) TimeUnit.NANOSECONDS.convert(millisecond, TimeUnit.MILLISECONDS));
		}
}

TimeUnit timeunit():
{}
{
	<MICROSECONDS> {return TimeUnit.MICROSECONDS;} |
	<MILLISECONDS> {return TimeUnit.MILLISECONDS;} |
	<SECONDS> {return TimeUnit.SECONDS;} |
	<MINUTES> {return TimeUnit.MINUTES;} |
	<HOURS> {return TimeUnit.HOURS;} |
	<DAYS> {return TimeUnit.DAYS;}
}