package utsupport;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class replace
{
	public static void main(String[] argv) throws Exception
	{
		System.out.println("Pre '" + argv[2] + "'");
		System.out.println("Post '" + argv[3] + "'");

		BufferedReader breader = new BufferedReader(new FileReader(argv[0]));
		BufferedWriter bwriter = new BufferedWriter(new FileWriter(argv[1]));

		String line = null;

		while ((line = breader.readLine()) != null)
		{
			bwriter.write(line.replaceAll(argv[2], argv[3]));
			bwriter.write("\n");
		}

		bwriter.close();
		breader.close();
	}
}
