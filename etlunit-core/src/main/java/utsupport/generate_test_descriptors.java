package utsupport;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class generate_test_descriptors
{

	public static void main(String[] argv) throws Exception
	{
		File in = new File("Z:\\EDW_DBO_SUBSET_TABLES.csv");
		File dir = new File("Z:\\EDWEN_TRUNK\\UnitTest\\test_descriptors");
		File pdir = new File("Z:\\EDWEN_TRUNK\\UnitTest\\parmlib");
		File sqldir = new File("Z:\\EDWEN_TRUNK\\UnitTest\\sql");

		BufferedReader br = new BufferedReader(new FileReader(in));

		String str = null;

		while ((str = br.readLine()) != null)
		{
			String[] tabs = str.split(",");

			if (tabs != null && tabs.length >= 2 && tabs[0].startsWith("wkf_") && tabs[0].contains("_AGG"))
			{
				String wkf = tabs[0].substring(4);

				System.out.println(tabs[1] + ":" + tabs[0]);

				File path = new File(dir, wkf + "_test.txt");

				String
						ntr =
						"[wkf_"
								+ wkf
								+ "]\r\n\r\n[s_m_"
								+ wkf
								+ "]\r\n\r\n$PMBadFileDir=F:\\unit_test\\BadFiles\r\n$PMLookupFileDir=F:\\unit_test\\LkpFiles\r\n$PMSourceFileDir=F:\\unit_test\\SrcFiles\r\n\r\n$DBConnectionSRC=EDWDEV01@INFORMATICA_UNIT_TEST\r\n$DBConnectionTGT=EDWDEV01@INFORMATICA_UNIT_TEST\r\n$DBConnectionLKP=EDWDEV01@INFORMATICA_UNIT_TEST\r\n\r\n$PMSessionLogFile=F:\\unit_test\\log\\session.log\r\n\r\n$$POPULATION_DATE=20100629\r\n\r\n\r\n$BadFileName1=SUBSET_"
								+ tabs[1]
								+ "_20100629.bad\r\n\r\n\r\n$Param_SRC1=TST_SRC\r\n$Param_TGT1=TST_TGT";
				writeFile(path,
						"[Description]\r\nCI Test\r\n\r\n[Suite]\r\nsubset "
								+ tabs[1]
								+ "\r\n\r\n[Data]\r\n"
								+ tabs[1]
								+ " "
								+ tabs[1]
								+ "_SUBSET_GM\r\n\r\n[Workflow]\r\n"
								+ wkf
								+ " DEV_Subset\r\n\r\n[Assert]\r\nTBL "
								+ tabs[1]
								+ " "
								+ tabs[1]
								+ "_SUBSET_GM");

				path = new File(pdir, tabs[0] + ".PRM");
				writeFile(path, ntr);

				path = new File(sqldir, "CREATE_DIM_MASTER_DATA_" + tabs[1] + ".SQL");
				writeFile(path,
						"DROP TABLE TST_MST."
								+ tabs[1]
								+ "_SUBSET_GM\r\nSELECT * INTO TST_MST."
								+ tabs[1]
								+ "_SUBSET_GM FROM EDW.DBO."
								+ tabs[1]);

				path = new File(dir, wkf + "_TRUNCATE_test.txt");

				writeFile(path,
						"[Description]\r\nCI truncation Test\r\n\r\n[Suite]\r\nsubset agg "
								+ tabs[1]
								+ "\r\n\r\n[Data]\r\n"
								+ tabs[1]
								+ " "
								+ tabs[1]
								+ "_SUBSET_GM\r\n"
								+ tabs[1]
								+ " "
								+ tabs[1]
								+ "_SUBSET_GM TST_TGT\r\n\r\n[PreSql]\r\nUPDATE\r\n\tTST_TGT."
								+ tabs[1]
								+ "\r\nSET\r\n\tPOPULATION_DATE = DATEADD(DAY, 1, POPULATION_DATE)\r\n\r\n[Workflow]\r\n"
								+ wkf
								+ " DEV_Subset\r\n\r\n[Assert]\r\nTBL "
								+ tabs[1]
								+ " "
								+ tabs[1]
								+ "_SUBSET_GM");
			}
		}
	}

	private static void writeFile(File f, String s) throws Exception
	{
		FileWriter fw = new FileWriter(f);

		fw.write(s);

		fw.close();
	}
}
