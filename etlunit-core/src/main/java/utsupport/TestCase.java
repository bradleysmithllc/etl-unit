package utsupport;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class TestCase
{
	private final String mapping;
	private final String testName;
	private final long time;
	private String failureType;
	private String failureMessage;
	private final StringBuilder stdout = new StringBuilder();

	public TestCase(String m, String tn, long t)
	{
		mapping = m;
		testName = tn;
		time = t;
	}

	public String getMapping()
	{
		return mapping;
	}

	public String getTestName()
	{
		return testName;
	}

	public boolean isFailure()
	{
		return failureType != null;
	}

	public String getFailureMessage()
	{
		return failureMessage;
	}

	public void setFailureMessage(String failureReason)
	{
		this.failureMessage = failureReason;
	}

	public String getFailureType()
	{
		return failureType;
	}

	public void setFailureType(String failureType)
	{
		this.failureType = failureType;
	}

	public double getTime()
	{
		return (time / 1000.0D);
	}

	public StringBuilder getStdout()
	{
		return stdout;
	}

	public String getStdoutString()
	{
		return stdout.toString();
	}
}
