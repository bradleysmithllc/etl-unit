package utsupport;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TestSuite
{
	private final String name;
	private long testTime = 0L;
	private int failures = 0;
	private List<TestCase> cases = new ArrayList<TestCase>();
	private final StringBuilder stdout = new StringBuilder();

	public TestSuite(String n)
	{
		name = n;
	}

	public String getName()
	{
		return name;
	}

	public Set getProperties()
	{
		return System.getProperties().entrySet();
	}

	public List<TestCase> getTestCases()
	{
		return cases;
	}

	public void addTestCase(String mapping, String testName, long millis)
	{
		addTestCase(mapping, testName, millis, null, null);
	}

	public void addTestCase(String mapping, String testName, long millis, String failureType, String failueMessage)
	{
		testTime += millis;

		TestCase ts = new TestCase(mapping, testName, millis);

		if (failureType != null)
		{
			failures++;
			ts.setFailureType(failureType);
			ts.setFailureMessage(failueMessage);
		}

		cases.add(ts);
	}

	public double getTestTime()
	{
		return testTime / 1000.0D;
	}

	public String getHostName() throws Exception
	{
		return InetAddress.getLocalHost().getHostAddress();
	}

	public int getErrors()
	{
		return 0;
	}

	public int getFailures()
	{
		return failures;
	}

	public String getTimestamp()
	{
		return new Timestamp(System.currentTimeMillis()).toString();
	}

	public StringBuilder getStdout()
	{
		return stdout;
	}

	public String getStdoutString()
	{
		return stdout.toString();
	}
}
