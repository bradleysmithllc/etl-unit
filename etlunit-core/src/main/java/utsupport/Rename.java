package utsupport;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;

public class Rename
{
	public static void main(String[] argv)
	{
		File root = new File("E:\\svn\\EDWEN_TRUNK_11_09_2009\\UnitTest\\src\\test");

		processDir(root);
	}

	private static void processDir(File root)
	{
		String name = root.getName();

		if (name.equals(".svn"))
		{
			return;
		}

		File[] files = root.listFiles();

		if (files != null)
		{
			for (int i = 0; i < files.length; i++)
			{
				if (files[i].isDirectory())
				{
					processDir(files[i]);
				}
				else
				{
					processFile(files[i]);
				}
			}
		}
	}

	private static void processFile(File file)
	{
		String name = file.getName();

		if (name.endsWith(".infatest"))
		{
			File newFile = new File(file.getParentFile(), name.substring(0, name.length() - 8) + "infaunit");

			System.out.println(newFile.getAbsolutePath());

			file.renameTo(newFile);
		}
	}
}
