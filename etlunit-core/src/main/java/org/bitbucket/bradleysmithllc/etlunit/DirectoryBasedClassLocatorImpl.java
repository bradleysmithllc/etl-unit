package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.regexp.PackNameExpression;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.NeedsTest;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

@NeedsTest
public class DirectoryBasedClassLocatorImpl implements ClassLocator
{
	private final File rootDir;
	private Log applicationLog;

	private final List<ETLTestClass> classesList = new ArrayList<ETLTestClass>();
	private int currentClassPointer = 0;

	private final Stack<File> dirStack = new Stack<File>();
	private final Stack<ETLTestClass> classStack = new Stack<ETLTestClass>();

	public DirectoryBasedClassLocatorImpl(File dir)
	{
		rootDir = dir;

		if (!rootDir.exists())
		{
			throw new IllegalArgumentException("Root directory " + rootDir + " does not exist");
		}

		// the implementation has changed.  It used to be a pass-through
		// implementation, but now it caches all classes and sorts after the fact
	}

	@Inject
	void setLogger(@Named("applicationLog") Log log)
	{
		applicationLog = log;
	}

	public boolean hasNext0()
	{
		if (classStack.size() != 0)
		{
			return true;
		}

		if (dirStack.size() == 0)
		{
			return false;
		}

		// at this point, the class stack is empty and the dir stack is not.
		// now pop the next dir and process until either the dir stack is
		// depleted or we have at least one class to process
		while (classStack.size() == 0)
		{
			if (dirStack.size() == 0)
			{
				return false;
			}

			File dir = dirStack.pop();

			// determine the package name
			List<String> _package = EtlUnitStringUtils.convertToRelativePath(rootDir, dir);

			final ETLTestPackage __package;

			if (_package == null || _package.size() == 0 || _package.get(0).equals("."))
			{
				// default package
				__package = ETLTestPackageImpl.getDefaultPackage();
			}
			else
			{
				ETLTestPackage defaultPackage = ETLTestPackageImpl.getDefaultPackage();

				for (String pname : _package)
				{
					PackNameExpression pne = new PackNameExpression(pname);

					if (pne.hasNext())
					{
						defaultPackage = defaultPackage.getSubPackage(pne.getPackName());
					}
				}

				__package = defaultPackage;
			}

			// scan the dir.  Every .etlunit class is loaded, parsed, and added to the
			// classes stack.  Every directory is pushed onto the stack.  This will have the
			// affect of a depth-first traversal of the directory tree
			dir.listFiles(new FileFilter()
			{
				@Override
				public boolean accept(File pathname)
				{
					if (pathname.isDirectory())
					{
						PackNameExpression pne = new PackNameExpression(pathname.getName());

						if (pne.hasNext())
						{
							dirStack.push(pathname);
						}
					}
					else if (pathname.isFile() && pathname.getName().toLowerCase().endsWith(".etlunit"))
					{
						if (applicationLog != null)
						{
							applicationLog.info("Parsing class file " + pathname.getAbsolutePath());
						}

						// parse the class file and add to the stack
						try
						{
							Reader fileReader = new BufferedReader(new FileReader(pathname));

							try
							{
								List<ETLTestClass> list = ETLTestParser.load(fileReader, __package);

								classStack.addAll(list);
							}
							finally
							{
								fileReader.close();
							}
						}
						catch (Exception e)
						{
							// log the error somewhere
							if (applicationLog != null)
							{
								applicationLog.severe(pathname.getName(), e);
							}
						}
					}

					return false;
				}
			});
		}

		return true;
	}

	public ETLTestClass next0()
	{
		return classStack.pop();
	}

	@Override
	public boolean hasNext()
	{
		if (classesList.size() == 0)
		{
			//seed the stack with the root dir
			dirStack.clear();
			classStack.clear();

			dirStack.push(rootDir);

			while (hasNext0())
			{
				ETLTestClass class_ = next0();

				classesList.add(class_);
			}

			Collections.sort(classesList);
		}

		return currentClassPointer < classesList.size();
	}

	@Override
	public ETLTestClass next()
	{
		return classesList.get(currentClassPointer++);
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void reset()
	{
		// just reset the current class pointer to 0;
		currentClassPointer = 0;
	}
}
