package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class TestMethodResultImpl implements TestMethodResult
{
	private final String className;
	private final String methodName;

	private final TestResultMetrics testResults = new TestResultMetricsImpl();

	public TestMethodResultImpl(String className, String methodName)
	{
		this.methodName = methodName;
		this.className = className;
	}

	@Override
	public String testClassName()
	{
		return className;
	}

	@Override
	public String testMethodName()
	{
		return methodName;
	}

	@Override
	public TestResultMetrics getMetrics()
	{
		return testResults;
	}
}
