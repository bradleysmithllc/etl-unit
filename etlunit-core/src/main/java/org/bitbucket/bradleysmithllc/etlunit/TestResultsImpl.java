package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestResultsImpl implements TestResults
{
	private final List<TestClassResult> results = new ArrayList<TestClassResult>();
	private final TestResultMetrics testResultMetrics = new TestResultMetricsImpl();
	private final int numTestsSelected;

	public TestResultsImpl(int numTestsSelected)
	{
		this.numTestsSelected = numTestsSelected;
	}

	@Override
	public TestResultMetrics getMetrics()
	{
		return testResultMetrics;
	}

	@Override
	public List<TestClassResult> getResultsByClass()
	{
		return results;
	}

	@Override
	public Map<ETLTestClass, TestClassResult> getResultsMapByClass() {
		Map<ETLTestClass, TestClassResult> resMap = new HashMap<ETLTestClass, TestClassResult>();

		for (TestClassResult classResults : getResultsByClass())
		{
			resMap.put(classResults.getTestClass(), classResults);
		}

		return resMap;
	}

	@Override
	public Map<String, TestClassResult> getResultsMapByClassName() {
		Map<String, TestClassResult> resMap = new HashMap<String, TestClassResult>();

		for (TestClassResult classResults : getResultsByClass())
		{
			resMap.put(classResults.getTestClass().getQualifiedName(), classResults);
		}

		return resMap;
	}

	@Override
	public void reset()
	{
		results.clear();
		testResultMetrics.reset();
	}

	@Override
	public int getNumTestsSelected()
	{
		return numTestsSelected;
	}
}
