package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.DiffManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

public interface DataFileManager
{
	String DEFAULT_COLUMN_DELIMITER = "\t";
	String DEFAULT_ROW_DELIMITER = "\n";
	String DEFAULT_NULL_TOKEN = "null";

	/**
	 * Disposes the manager and releases any resources held by it.
	 */
	void dispose();

	Connection getEmbeddedDatabase();

	DataFileSchema.format_type DEFAULT_FORMAT_TYPE = DataFileSchema.format_type.delimited;

	void setDefaultColumnDelimiter(String delimiter);

	void setDefaultRowDelimiter(String delimiter);

	void setDefaultNullToken(String token);

	void setDefaultFormatType(DataFileSchema.format_type type);

	String getDefaultColumnDelimiter();

	String getDefaultRowDelimiter();

	String getDefaultNullToken();

	DataFileSchema.format_type getDefaultFormatType();

	/**
	 * Loads the schema from the specified resource path.  The default class loader is to use
	 * the context class loader of the current thread.
	 * @param resourcePath
	 * @return
	 */
	DataFileSchema loadDataFileSchemaFromResource(String resourcePath, String resourceId);

	/**
	 * Loads the schema from the specified resource path.  The specified class loader is used.  Use this
	 * method sparingly since this build runs in a java application as well as a maven plugin.
	 * @param resourcePath
	 * @param classLoaderContext
	 * @return
	 */
	DataFileSchema loadDataFileSchemaFromResource(String resourcePath, String resourceId, Object classLoaderContext);

	DataFileSchema createDataFileSchema(String id);

	DataFileSchema loadDataFileSchema(File schema, String schemaId);
	void saveDataFileSchema(DataFileSchema schema, File output) throws IOException;

	DataFile loadDataFile(File file, DataFileSchema schema);

	/**
	 * Compares l-file to r-file, using the column list in lfile as the basis for comparison.  Any
	 * additional columns in the r-file will be ignored.   Both files must be ordered according to the
	 * order key (defined by the order columns in the schema).  Extra rows in r-file will be reported
	 * as 'added', extra rows in l-file will be reported as 'removed'.
	 *
	 * @param lfile - In practice this is the 'expected'
	 * @param rfile - This should be the 'actual'
	 * @return
	 */
	List<FileDiff> diff(DataFile lfile, DataFile rfile) throws IOException;

	/**
	 * Compares l-file to r-file, using the column list in lfile as the basis for comparison.  Any
	 * additional columns in the r-file will be ignored.   Both files must be ordered according to the
	 * order key (defined by the order columns in the schema).  Extra rows in r-file will be reported
	 * as 'added', extra rows in l-file will be reported as 'removed'.
	 *
	 * @param lfile - In practice this is the 'expected'
	 * @param rfile - This should be the 'actual'
	 * @param columns - A list of columns to constrain the comparison to
	 * @return
	 */
	List<FileDiff> diff(DataFile lfile, DataFile rfile, List<String> columns) throws IOException;

	/**
	 *
	 * @param dManager
	 * @param operation
	 * @param failureId
	 * @param diffs
	 * @param expectedId Identity of the expected data set
	 * @param actualId Identity of the actual data set
	 */
	void report(DiffManager dManager, ETLTestMethod context, ETLTestOperation operation, String failureId, List<FileDiff> diffs, String expectedId, String actualId);

	DataConverter resolveValidatorById(String id);
	void installConverter(DataConverter converter);

	/**
	 * Copies a data file from one schema to another.  These schemas must be compatible
	 * or else the copy will fail (E.G., if there is no intersection between the two schemas
	 * then the resulting file will be all nulls.
	 *
	 * @param source
	 * @param dest
	 */
	void copyDataFile(DataFile source, DataFile dest, CopyOptions options) throws IOException;

	void copyDataFile(DataFile source, DataFile dest) throws IOException;

	void copyFileDataToWriter(DataFileReader fd, DataFileWriter writer) throws IOException;

	interface CopyOptions
	{
		enum missing_column_policy
		{
			use_null,
			use_default
		}

		enum honor_date_diff
		{
			no, yes
		}

		boolean writeHeader();

		boolean preordered();
		missing_column_policy getMissingColumnPolicy();
		honor_date_diff honorDateDiff();

		Map<String, String> defaultValues();
		String overrideExtractionSql();
		DataFileSchema getTargetSchema();
	}
}
