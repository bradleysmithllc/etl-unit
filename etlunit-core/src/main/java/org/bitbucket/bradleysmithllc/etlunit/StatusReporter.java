package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.SerializationUtils;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public interface StatusReporter {
	enum test_completion_code {
		success,
		failure,
		warning,
		error,
		ignored
	}

	class CompletionStatus implements Serializable {
		private List<TestExecutionError> testExecutionErrors = new ArrayList<TestExecutionError>();

		public CompletionStatus cloneMe() {
			return SerializationUtils.clone(this);
		}

		private final List<TestAssertionFailure> assertionFailures = new ArrayList<TestAssertionFailure>();
		private final List<String> assertionFailureIds = new ArrayList<String>();
		private final List<TestWarning> testWarnings = new ArrayList<TestWarning>();
		private final List<String> testWarningIds = new ArrayList<String>();
		private final List<String> testIgnoreReasons = new ArrayList<String>();

		private String expectedErrorId;

		private List<String> expectedFailureIds = new ArrayList<String>();
		private List<String> expectedWarningIds = new ArrayList<String>();

		private boolean expectedErrorOverride = false;
		private boolean expectedFailureOverride = false;

		private boolean resolved = false;

		public void setExpectedErrorOverride(boolean expectedErrorOverride) {
			this.expectedErrorOverride = expectedErrorOverride;
		}

		public void setExpectedFailureOverride(boolean expectedFailureOverride) {
			this.expectedFailureOverride = expectedFailureOverride;
		}

		public String getExpectedErrorId() {
			return expectedErrorId;
		}

		public void setExpectedErrorId(String expectedErrorId) {
			this.expectedErrorId = expectedErrorId;
		}

		public void addExpectedFailureId(String expectedFailureId) {
			expectedFailureIds.add(expectedFailureId);
		}

		public void addExpectedFailureIds(List<String> expectedFailureIds) {
			this.expectedFailureIds.addAll(expectedFailureIds);
		}

		public void addExpectedWarningId(String expectedErrorId) {
			expectedWarningIds.add(expectedErrorId);
		}

		public void addExpectedWarningIds(List<String> expectedErrorIds) {
			expectedWarningIds.addAll(expectedErrorIds);
		}

		public List<TestExecutionError> getErrors() {
			return testExecutionErrors;
		}

		public boolean hasError() {
			return testExecutionErrors.size() != 0;
		}

		public boolean hasFailures() {
			return hasError() || assertionFailures.size() != 0;
		}

		public List<TestAssertionFailure> getAssertionFailures() {
			return assertionFailures;
		}

		public List<TestWarning> getWarnings() {
			return testWarnings;
		}

		public void addWarning(TestWarning warn) {
			String warningId = warn.getWarningId();
			if (!testWarningIds.contains(warningId)) {
				testWarningIds.add(warningId);
			}

			testWarnings.add(warn);
		}

		public void addIgnoreReason(String reason) {
			testIgnoreReasons.add(reason);
		}

		public void addFailure(TestAssertionFailure failure) {
			for (String failureId : failure.getFailureIds()) {
				if (!assertionFailureIds.contains(failureId)) {
					assertionFailureIds.add(failureId);
				}
			}

			assertionFailures.add(failure);
		}

		public void addError(TestExecutionError err) {
			testExecutionErrors.add(err);
		}

		public String toString() {
			return "errors=" + testExecutionErrors + ", failures=" + assertionFailures + ", warnings=" + testWarnings + ", ignored=" + testIgnoreReasons;
		}

		public test_completion_code getTestResult() {
			resolveStatus();

			// check for any status involving an error
			if (testExecutionErrors.size() != 0) {
				// check for expected error override
				if (!expectedErrorOverride) {
					// stop - this is an error
					return test_completion_code.error;
				}
			}

			if (assertionFailureIds.size() != 0) {
				// might be a failure
				if (!expectedFailureOverride) {
					return test_completion_code.failure;
				}
			}

			if (testIgnoreReasons.size() != 0) {
				return test_completion_code.ignored;
			}

			return test_completion_code.success;

			/*
			&& expectedErrorId == null)
			{
				testResult = test_completion_code.error;
			}
			else if (assertionFailures.size() != 0)
			{
				testResult = test_completion_code.failure;
			}
			else if (testWarnings.size() != 0)
			{
				testResult = test_completion_code.warning;
			}
			else
			{
				testResult = test_completion_code.success;
			}

			switch (testResult)
			{
				case success:
					return test_completion_code.success;
				case warning:
					return test_completion_code.success;
				case failure:
				case error:
					if (expectedErrorOverride)
					{
						// success *iif* there were no failures
						return assertionFailureIds.size() != 0 ? test_completion_code.failure : test_completion_code.success;
					}

					if (expectedFailureOverride)
					{
						// success *iif* there were no failures
						return test_completion_code.success;
					}

					return testResult;
				default:
					throw new UnsupportedOperationException();
			}
			 */
		}

		public List<String> getExpectedWarningIds() {
			return expectedWarningIds;
		}

		public List<String> getTestIgnoreReasons() {
			return testIgnoreReasons;
		}

		public List<String> getExpectedFailureIds() {
			return expectedFailureIds;
		}

		public List<String> getTestWarningIds() {
			return testWarningIds;
		}

		public List<String> getAssertionFailureIds() {
			return assertionFailureIds;
		}

		public void resolveStatus() {
			if (resolved) {
				return;
			}

			resolved = true;

			List<String> failureIds = getExpectedFailureIds();

			if (failureIds.size() != 0) {
				List<String> assertionFailureIds = new ArrayList<String>(getAssertionFailureIds());
				if (failureIds.containsAll(assertionFailureIds) && assertionFailureIds.containsAll(failureIds)) {
					// this test passes
					setExpectedFailureOverride(true);
				} else {
					// do not fail here if the test is ignored...
					if (testIgnoreReasons.size() == 0) {
						addFailure(new TestAssertionFailure("Failure(s) "
								+ failureIds
								+ " expected but "
								+ assertionFailureIds
								+ " caught", TestConstants.FAIL_EXPECTED_ASSERTION));
					}
				}
			}

			String expectedErrorId = getExpectedErrorId();

			if (expectedErrorId != null) {
				if (hasError()) {
					if (getErrors().size() != 0 && !getErrors().get(0).getErrorId().equals(expectedErrorId)) {
						// mark the error as okay so the assertion failure will win
						setExpectedErrorOverride(true);
						// make sure this is cancelled
						setExpectedFailureOverride(false);
						addFailure(new TestAssertionFailure("Error '"
								+ expectedErrorId
								+ "' expected but '"
								+ printErrors()
								+ "' caught", TestConstants.FAIL_EXPECTED_EXCEPTION));
					} else {
						// this test passes
						setExpectedErrorOverride(true);
					}
				} else {
					// do not fail here if the test is ignored...
					if (testIgnoreReasons.size() == 0) {
						addFailure(new TestAssertionFailure("Error '" + expectedErrorId + "' expected",
								TestConstants.FAIL_EXPECTED_EXCEPTION));
					}
				}
			}

			List<String> warningIds = getExpectedWarningIds();

			if (warningIds.size() != 0) {
				List<String> testWarningIds = getTestWarningIds();
				if (warningIds.containsAll(testWarningIds) && testWarningIds.containsAll(warningIds)) {
					// nothing to do here because warnings can't fail a method except for expected warnings not happening
				} else {
					addFailure(new TestAssertionFailure("Warning(s) "
							+ warningIds
							+ " expected but "
							+ testWarningIds
							+ " caught", TestConstants.FAIL_EXPECTED_WARNING));
				}
			}
		}

		private String printErrors() {
			StringWriter stw = new StringWriter();
			PrintWriter pw = new PrintWriter(stw);

			pw.println("Errors:");

			int index = 1;
			for (TestExecutionError error : getErrors()) {
				pw.println("[ERROR " + index + " BEGIN]");
				error.printStackTrace(pw);
				pw.println("[ERROR " + index++ + " END]");
			}

			pw.println(".");
			pw.flush();
			pw.close();

			return stw.toString();
		}
	}

	void scanStarted();

	void scanCompleted();

	void testsStarted(int numTestsSelected);

	void testClassAccepted(ETLTestClass method);

	void testMethodAccepted(ETLTestMethod method);

	void testBeginning(ETLTestMethod method);

	void testCompleted(ETLTestMethod method, CompletionStatus status);

	void testsCompleted();
}
