package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class TestAssertionFailure extends Exception
{
	private final String [] failureId;

	public TestAssertionFailure(String msg)
	{
		this(msg, TestConstants.FAIL_UNSPECIFIED);
	}

	public TestAssertionFailure(String msg, String failureId)
	{
		super(msg);

		this.failureId = new String[]{failureId};
	}

	public TestAssertionFailure(String msg, String [] failureIds)
	{
		super(msg);

		failureId = failureIds;
	}

	public TestAssertionFailure(String msg, Exception cause)
	{
		this(msg, TestConstants.FAIL_UNSPECIFIED, cause);
	}

	public TestAssertionFailure(String msg, String failureId, Exception cause)
	{
		this(msg, new String[]{failureId}, cause);
	}

	public TestAssertionFailure(String msg, String [] failureId, Exception cause)
	{
		super(msg, cause);

		this.failureId = failureId;
	}

	public String [] getFailureIds()
	{
		return failureId;
	}
}
