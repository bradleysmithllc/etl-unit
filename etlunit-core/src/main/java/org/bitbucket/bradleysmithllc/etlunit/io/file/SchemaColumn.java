package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import java.sql.Types;
import java.util.Map;
import java.util.regex.Pattern;

public class SchemaColumn implements DataFileSchema.Column
{
	private final String id;

	private String format;
	private boolean lenientFormatter = false;

	private diff_type diffType = diff_type.none;

	private boolean readOnly;

	private String type = "VARCHAR";
	private String typeAnnotation;
	private basic_type basicType = basic_type.string;

	private int length;
	private int scale;
	private int ordinal = Integer.MIN_VALUE;
	private int inferredOrdinal;
	private int jdbcType = -1000;

	private final DataFileManager dataFileManager;

	private int offset = -1;
	private Object defaultValue;

	public static class DefaultSequence
	{
		private int initialValue = Integer.MAX_VALUE;
		private int baseOffset = Integer.MAX_VALUE;
		private int increment = Integer.MAX_VALUE;
		private int modulus = Integer.MAX_VALUE;
		private String expression;

		public boolean hasInitialValue()
		{
			return initialValue != Integer.MAX_VALUE;
		}

		public boolean hasModulus()
		{
			return modulus != Integer.MAX_VALUE;
		}

		public boolean hasBaseOffset()
		{
			return baseOffset != Integer.MAX_VALUE;
		}

		public boolean hasIncrement()
		{
			return increment != Integer.MAX_VALUE;
		}

		public boolean hasExpression()
		{
			return expression != null;
		}

		public void setInitialValue(int initialValue) {
			this.initialValue = initialValue;
		}

		public void setBaseOffset(int baseOffset) {
			this.baseOffset = baseOffset;
		}

		public int getIncrement() {
			return increment;
		}

		public void setIncrement(int increment) {
			this.increment = increment;
		}

		public void setModulus(int modulus) {
			this.modulus = modulus;
		}

		public void setExpression(String expression) {
			this.expression = expression;
		}

		public int getInitialValue() {
			return initialValue;
		}

		public int getBaseOffset() {
			return baseOffset;
		}

		public int getModulus() {
			return modulus;
		}

		public String getExpression() {
			return expression;
		}
	}

	private DefaultSequence defaultExpression;

	public SchemaColumn(String id, int length, DataFileManager dataFileManager)
	{
		this(id, null, length, -1, dataFileManager);
	}

	public SchemaColumn(String id, String type, DataFileManager dataFileManager)
	{
		this(id, type, -1, -1, dataFileManager);
	}

	public SchemaColumn(String id, String type, int length, int scale, DataFileManager dataFileManager)
	{
		this.id = id;
		this.type = type;
		this.length = length;
		this.dataFileManager = dataFileManager;
		this.scale = scale;
		defaultExpression = null;
	}

	public SchemaColumn(JsonNode node, DataFileManager dataFileManager)
	{
		this.dataFileManager = dataFileManager;
		id = node.get("id").asText().toLowerCase();

		if (node.has("type"))
		{
			JsonNode type1 = node.get("type");
			if (!type1.isNull())
			{
				type = type1.asText();
			}
		}

		if (node.has("type-annotation"))
		{
			JsonNode type1 = node.get("type-annotation");
			if (!type1.isNull())
			{
				typeAnnotation = type1.asText();
			}
			else
			{
				typeAnnotation = null;
			}
		}
		else
		{
			typeAnnotation = null;
		}

		if (node.has("length"))
		{
			JsonNode length1 = node.get("length");
			if (!length1.isNull())
			{
				length = length1.asInt();
			}
			else
			{
				length = -1;
			}
		}
		else
		{
			length = -1;
		}

		if (node.has("scale"))
		{
			JsonNode length1 = node.get("scale");
			if (!length1.isNull())
			{
				scale = length1.asInt();
			}
			else
			{
				scale = -1;
			}
		}
		else
		{
			scale = -1;
		}

		if (node.has("ordinal"))
		{
			JsonNode length1 = node.get("ordinal");
			if (!length1.isNull())
			{
				ordinal = length1.asInt();
			}
			else
			{
				ordinal = Integer.MIN_VALUE;
			}
		}
		else
		{
			ordinal = Integer.MIN_VALUE;
		}

		if (node.has("format"))
		{
			JsonNode length1 = node.get("format");
			if (!length1.isNull())
			{
				format = length1.asText();
			}
			else
			{
				format = null;
			}
		}
		else
		{
			format = null;
		}

		if (node.has("lenient-formatter"))
		{
			JsonNode length1 = node.get("lenient-formatter");
			if (!length1.isNull())
			{
				if (format == null)
				{
					throw new IllegalArgumentException("Cannot specify lenient-formatter if format is not present");
				}

				lenientFormatter = length1.asBoolean();
			}
		}

		if (node.has("read-only"))
		{
			JsonNode length1 = node.get("read-only");
			if (!length1.isNull() && length1.isBoolean())
			{
				readOnly = length1.asBoolean();
			}
			else
			{
				readOnly = false;
			}
		}
		else
		{
			readOnly = false;
		}

		if (node.has("diff-type")) {
			JsonNode length1 = node.get("diff-type");
			if (!length1.isNull()) {
				diffType = diff_type.valueOf(length1.asText());

				if (
					ObjectUtils.notIn(jdbcType(), Types.VARCHAR, Types.LONGVARCHAR, Types.NVARCHAR, Types.LONGNVARCHAR)
				) {
					throw new IllegalArgumentException("Date diff columns must be a string type.  " + getId() + " " + jdbcType);
				}
			}
		}

		if (node.has("basic-type"))
		{
			JsonNode length1 = node.get("basic-type");
			if (!length1.isNull())
			{
				String bt = length1.asText();

				if (bt.equals(basic_type.integer.name()))
				{
					basicType = basic_type.integer;
				}
				else if (bt.equals(basic_type.string.name()))
				{
					basicType = basic_type.string;
				}
				else if (bt.equals(basic_type.numeric.name()))
				{
					basicType = basic_type.numeric;
				}
			}
			else
			{
				basicType = basic_type.string;
			}
		}
		else
		{
			basicType = basic_type.string;
		}

		if (node.has("default-value"))
		{
			JsonNode length1 = node.get("default-value");
			if (!length1.isNull())
			{
				if (!length1.isObject())
				{
					defaultExpression = null;
					switch (length1.getNodeType())
					{
						case STRING:
							defaultValue = length1.asText();
							break;
						case BOOLEAN:
							defaultValue = length1.asBoolean();
							break;
						case NUMBER:
							if (length1.isIntegralNumber())
							{
								defaultValue = length1.asInt();
							}
							else
							{
								defaultValue = length1.doubleValue();
							}
							break;
						case MISSING:
						case NULL:
						case ARRAY:
						case BINARY:
						case OBJECT:
						case POJO:
							break;
					}
				}
				else
				{
					defaultExpression = new DefaultSequence();


					if (length1.has("initial-value"))
					{
						defaultExpression.initialValue = length1.get("initial-value").asInt();
					}

					if (length1.has("increment"))
					{
						defaultExpression.increment = length1.get("increment").asInt();
					}

					if (length1.has("base-offset"))
					{
						defaultExpression.baseOffset = length1.get("base-offset").asInt();
					}

					if (length1.has("modulus"))
					{
						defaultExpression.modulus = length1.get("modulus").asInt();
					}

					if (length1.has("expression"))
					{
						defaultExpression.expression = length1.get("expression").asText();
					}
				}
			}
			else
			{
				defaultValue = null;
				defaultExpression = null;
			}
		}
		else
		{
			defaultValue = null;
			defaultExpression = null;
		}
	}

	public diff_type diffType() {
		return diffType;
	}

	public void setDiffType(diff_type type) {
		diffType = type;
	}

	public boolean validateText(String text)
	{
		DataConverter validator = getConverter();

		Pattern pattern = validator.getPattern(this);

		if (pattern != null)
		{
			return pattern.matcher(text).matches();
		}
		else
		{
			return true;
		}
	}

	public void setOffset(int offset)
	{
		this.offset = offset;
	}

	public String getId()
	{
		return id;
	}

	@Override
	public String getFormat() {
		return format;
	}

	@Override
	public void setFormat(String format) {
		this.format = format;
	}

	@Override
	public boolean hasTypeAnnotation()
	{
		return typeAnnotation != null;
	}

	@Override
	public String getTypeAnnotation()
	{
		return typeAnnotation;
	}

	public boolean hasType()
	{
		return type != null;
	}

	public String getType()
	{
		return type;
	}

	public DataConverter getConverter()
	{
		if (hasType())
		{
			return dataFileManager.resolveValidatorById(getType());
		}
		else
		{
			// default to string
			return dataFileManager.resolveValidatorById("VARCHAR");
		}
	}

	@Override
	public int jdbcType()
	{
		if (jdbcType == -1000)
		{
			jdbcType = JdbcTypeConverter.getTypeValue(type);
		}

		return jdbcType;
	}

	@Override
	public void setType(String type)
	{
		this.type = type;
	}

	@Override
	public void setLength(int length)
	{
		if (this.length != -1)
		{
			throw new IllegalArgumentException("Cannot change the length of a column");
		}

		this.length = length;
	}

	public int getOffset()
	{
		return offset;
	}

	public int getLength()
	{
		return length;
	}

	@Override
	public Object getDefaultValue() {
		return defaultValue;
	}

	@Override
	public Object generateColumnValue(Map<String, Object> columnValues, int rowSequence) {
		if (defaultExpression != null)
		{
			int value = rowSequence;

			// determine the base sequence value
			if (defaultExpression.hasIncrement())
			{
				columnValues.put("increment", defaultExpression.increment);
				value = defaultExpression.increment * rowSequence;
			}

			// any initial offset
			if (defaultExpression.hasInitialValue())
			{
				columnValues.put("initialValue", defaultExpression.initialValue);
				value += defaultExpression.initialValue;
			}

			// apply the modulo
			if (defaultExpression.hasModulus())
			{
				columnValues.put("modulus", defaultExpression.modulus);
				value %= defaultExpression.modulus;
			}

			// base offset happens last
			if (defaultExpression.hasBaseOffset())
			{
				columnValues.put("baseOffset", defaultExpression.baseOffset);
				value += defaultExpression.baseOffset;
			}

			columnValues.put("defaultValue", value);

			// check for an expression
			if (defaultExpression.hasExpression())
			{
				// this is a velocity template - process with the given data set
				try {
					return VelocityUtil.writeTemplateWithStaticSupport(defaultExpression.expression, columnValues);
				} catch (Exception e) {
					throw new RuntimeException("Error processing default value expression: " + defaultExpression.expression, e);
				}
			}

			return value;
		}

		return defaultValue;
	}

	@Override
	public boolean isReadOnly() {
		return readOnly;
	}

	@Override
	public int ordinal() {
		return ordinal;
	}

	@Override
	public int inferredOrdinal() {
		return inferredOrdinal;
	}

	@Override
	public void setInferredOrdinal(int ord) {
		inferredOrdinal = ord;
	}

	@Override
	public int actualOrdinal() {
		if (ordinal == Integer.MIN_VALUE) {
			return inferredOrdinal;
		} else {
			return ordinal;
		}
	}

	@Override
	public DataFileSchema.Column clone() {
		try {
			return (DataFileSchema.Column) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public int getScale() {
		return scale;
	}

	@Override
	public void setScale(int scale) {
		if (this.scale != -1)
		{
			throw new IllegalArgumentException("Cannot change the scale of a column");
		}

		this.scale = scale;
	}

	@Override
	public void setTypeAnnotation(String typeAnnotation) {
		this.typeAnnotation = typeAnnotation;
	}

	public basic_type getBasicType()
	{
		return basicType;
	}

	public void setBasicType(basic_type basicType)
	{
		if (basicType == null)
		{
			throw new IllegalArgumentException("Basic type cannot be null");
		}

		this.basicType = basicType;
	}

	@Override
	public DefaultSequence getDefaultSequence() {
		return defaultExpression;
	}

	@Override
	public DefaultSequence populateDefaultSequence()
	{
		if (defaultExpression == null)
		{
			defaultExpression = new DefaultSequence();
		}

		return defaultExpression;
	}

	public boolean isFormatterLenient() {
		return lenientFormatter;
	}

	@Override
	public void setLenientFormatter(boolean lenient)
	{
		lenientFormatter = lenient;
	}

	@Override
	public String toString() {
		return "SchemaColumn{" +
				"id='" + id + '\'' +
				", format='" + format + '\'' +
				", lenientFormatter=" + lenientFormatter +
				", readOnly=" + readOnly +
				", type='" + type + '\'' +
				", basicType=" + basicType +
				", length=" + length +
				", scale=" + scale +
				", jdbcType=" + jdbcType +
				", dataFileManager=" + dataFileManager +
				", offset=" + offset +
				", defaultExpression=" + defaultExpression +
				", defaultValue=" + defaultValue +
				", defaultExpression=" + defaultExpression +
				'}';
	}
}
