package org.bitbucket.bradleysmithllc.etlunit.metadata;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface MetaDataPackageContext extends JsonSerializable
{
	Map<String, MetaDataArtifact> getArtifactMap();

	MetaDataContext getMetaDataContext();

	enum path_type {
		feature_source,
		test_source,
		external_source
	}

	ETLTestPackage getPackageName();
	path_type getPathType();
	List<MetaDataArtifact> getArtifacts();

	MetaDataArtifact createArtifact(String relative, File path) throws IOException;
	MetaDataArtifact createArtifact(String relative, String projectRelative, File path);
}
