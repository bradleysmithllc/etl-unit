package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.regexp.TestExpression;
import org.bitbucket.bradleysmithllc.etlunit.regexp.TestSpecificationExpression;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PatternClassDirector extends NullClassDirector {
	private final List<NamePattern> matchList = new ArrayList<NamePattern>();

	private final class NamePattern {
		private final PatternClassName classPattern;

		private final Pattern methodName;
		private final Pattern operName;

		private final String methodNameText;
		private final String operNameText;

		private NamePattern(PatternClassName classPattern, String methodName, String operName) {
			this.classPattern = classPattern;
			methodNameText = methodName;
			operNameText = operName;

			this.methodName = methodName != null ? Pattern.compile(methodName, Pattern.CASE_INSENSITIVE) : null;
			this.operName = operName != null ? Pattern.compile(operName, Pattern.CASE_INSENSITIVE) : null;
		}

		boolean classMatches(ETLTestClass cls) {
			return classPattern == null || classPattern.matches(cls);
		}

		boolean methodMatches(ETLTestMethod cls) {
			return classMatches(cls.getTestClass()) &&
					(methodName == null || methodName.matcher(cls.getName()).find());
		}

		boolean operationMatches(ETLTestOperation ope) {
			// we don't want to cancel any before or after methods, so only filter the operations if this method accepts
			// If the method does not accept, that means it did NOT come through the method accept.
			return operName == null || operName.matcher(ope.getOperationName()).find();
		}

		@Override
		public String toString() {
			return "NamePattern{" +
					"classPattern='" + classPattern + '\'' +
					(methodNameText != null ? (", methodNameText='" + methodNameText + '\'') : "") +
					(operNameText != null ? (", operNameText='" + operNameText + '\'') : "") +
					'}';
		}
	}

	public PatternClassDirector(TestExpression tex) {
		while (tex.hasNext()) {
			TestSpecificationExpression tspec = tex.getTestSpecification();

			/*
			if (tspec.matches()) {
				matchList.add(
						new NamePattern(
								tspec.getTestClassPattern(),
								tspec.hasTestNamePattern() ? tspec.getTestNamePattern() : null,
								tspec.hasTestOperationPattern() ? tspec.getTestOperationPattern() : null
						)
				);
			}
			 */
			throw new UnsupportedOperationException();
		}
	}

	public PatternClassDirector(PatternClassName classPattern, String methodPattern, String operationPattern) {
		matchList.add(
			new NamePattern(
				classPattern,
				methodPattern,
				operationPattern
			)
		);
	}

	@Override
	public response_code accept(ETLTestClass cl) {
		for (NamePattern np : matchList) {
			if (np.classMatches(cl)) {
				return response_code.accept;
			}
		}

		return response_code.reject;
	}

	@Override
	public response_code accept(ETLTestMethod mt) {
		for (NamePattern np : matchList) {
			if (np.methodMatches(mt)) {
				return response_code.accept;
			}
		}

		return response_code.reject;
	}

	@Override
	public response_code accept(ETLTestOperation op) {
		for (NamePattern np : matchList) {
			if (np.operationMatches(op)) {
				return response_code.accept;
			}
		}

		return response_code.reject;
	}
}
