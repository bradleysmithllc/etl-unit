package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.SuiteClassDirectorImpl;

public class SuiteClassDirectorFeature extends AbstractFeature
{
	private final SuiteClassDirectorImpl classDirector;

	public SuiteClassDirectorFeature(String desc)
	{
		classDirector = new SuiteClassDirectorImpl(desc);
	}

	@Override
	public String getFeatureName()
	{
		return "suite-class-director_" + System.identityHashCode(this);
	}

	@Override
	public ClassDirector getDirector()
	{
		return classDirector;
	}

	@Override
	public long getPriorityLevel()
	{
		return -1L;
	}
}
