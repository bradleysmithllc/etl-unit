package org.bitbucket.bradleysmithllc.etlunit.util.jdbc;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class MetadataColumn extends MetadataSchema {
	private final String tableName;
	private final String columnName;

	private int dataType;
	private int columnSize;
	private int decimalDigits;
	private int numPrecRadix;
	private int nullable;
	private int sqlDataType;
	private int sqlDatetimeSub;
	private int charOctetLength;
	private int ordinalPosition;
	private int sourceDataType;

	private String typeName;
	private String remarks;
	private String columnDef;
	private bool isNullable;
	private String scopeCatalog;
	private String scopeSchema;
	private String scopeTable;
	private bool isAutoincrement;
	private bool isGeneratedColumn;

	public enum bool {
		yes, no, unknown
	}

	public MetadataColumn(String catalogName, String schemaName, String tableName, String columnName) {
		super(catalogName, schemaName);
		this.columnName = columnName;
		this.tableName = tableName;
	}

	public String columnName() {
		return columnName;
	}

	public String typeName() {
		return typeName;
	}

	public MetadataColumn withTypeName(String typeName) {
		this.typeName = typeName;
		return this;
	}

	public String columnDef() {
		return columnDef;
	}

	public MetadataColumn withColumnDef(String def) {
		this.typeName = def;
		return this;
	}

	public int dataType() {
		return dataType;
	}

	public MetadataColumn withDataType(int dataType) {
		this.dataType = dataType;
		return this;
	}

	public int columnSize() {
		return columnSize;
	}

	public MetadataColumn withColumnSize(int columnSize) {
		this.columnSize = columnSize;
		return this;
	}

	public int decimalDigits() {
		return decimalDigits;
	}

	public MetadataColumn withDecimalDigits(int decimalDigits) {
		this.decimalDigits = decimalDigits;
		return this;
	}

	public int numPrecRadix() {
		return numPrecRadix;
	}

	public MetadataColumn withNumPrecRadix(int numPrecRadix) {
		this.numPrecRadix = numPrecRadix;
		return this;
	}

	public int nullable() {
		return nullable;
	}

	public MetadataColumn withNullable(int nullable) {
		this.nullable = nullable;
		return this;
	}

	public int sqlDataType() {
		return sqlDataType;
	}

	public MetadataColumn withSqlDataType(int sqlDataType) {
		this.sqlDataType = sqlDataType;
		return this;
	}

	public int sqlDatetimeSub() {
		return sqlDatetimeSub;
	}

	public MetadataColumn withSqlDatetimeSub(int sqlDatetimeSub) {
		this.sqlDatetimeSub = sqlDatetimeSub;
		return this;
	}

	public int charOctetLength() {
		return charOctetLength;
	}

	public MetadataColumn withCharOctetLength(int charOctetLength) {
		this.charOctetLength = charOctetLength;
		return this;
	}

	public int ordinalPosition() {
		return ordinalPosition;
	}

	public MetadataColumn withOrdinalPosition(int ordinalPosition) {
		this.ordinalPosition = ordinalPosition;
		return this;
	}

	public int sourceDataType() {
		return sourceDataType;
	}

	public MetadataColumn withSourceDataType(int sourceDataType) {
		this.sourceDataType = sourceDataType;
		return this;
	}

	public String remarks() {
		return remarks;
	}

	public MetadataColumn withRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}

	public bool isNullable() {
		return isNullable;
	}

	public MetadataColumn withIsNullable(String yesNoMaybe) {
		this.isNullable = yesNoMaybe(yesNoMaybe);
		return this;
	}

	public String scopeCatalog() {
		return scopeCatalog;
	}

	public MetadataColumn withScopeCatalog(String scopeCatalog) {
		this.scopeCatalog = scopeCatalog;
		return this;
	}

	public String scopeSchema() {
		return scopeSchema;
	}

	public MetadataColumn withScopeSchema(String scopeSchema) {
		this.scopeSchema = scopeSchema;
		return this;
	}

	public String scopeTable() {
		return scopeTable;
	}

	public MetadataColumn withScopeTable(String scopeTable) {
		this.scopeTable = scopeTable;
		return this;
	}

	public bool isAutoincrement() {
		return isAutoincrement;
	}

	public MetadataColumn withIsAutoincrement(String yesNoMaybe) {
		this.isAutoincrement = yesNoMaybe(yesNoMaybe);
		return this;
	}

	private bool yesNoMaybe(String isAutoincrement) {
		if ("YES".equals(isAutoincrement)) {
			return bool.yes;
		} else if ("NO".equals(isAutoincrement)) {
			return bool.no;
		}

		return bool.unknown;
	}

	public bool isGeneratedColumn() {
		return isGeneratedColumn;
	}

	public MetadataColumn withIsGeneratedColumn(String yesNoMaybe) {
		this.isGeneratedColumn = yesNoMaybe(yesNoMaybe);
		return this;
	}
}
