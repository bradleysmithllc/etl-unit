package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.*;

public class ETLTestAnnotatedImpl extends ETLTestDebugTraceableImpl implements ETLTestAnnotated
{
	private String description;

	List<ETLTestAnnotation> annotations = new ArrayList<ETLTestAnnotation>();
	Map<String, List<ETLTestAnnotation>> annotationMap = new HashMap<String, List<ETLTestAnnotation>>();

	public ETLTestAnnotatedImpl(Token token)
	{
		super(token);
	}

	public ETLTestAnnotatedImpl()
	{
	}

	public void addAnnotations(List<ETLTestAnnotation> ano)
	{
		Iterator<ETLTestAnnotation> it = ano.iterator();

		while (it.hasNext())
		{
			addAnnotation(it.next());
		}
	}

	public void addAnnotation(ETLTestAnnotation ano)
	{
		annotations.add(ano);

		List<ETLTestAnnotation> li = annotationMap.get(ano.getName());

		if (li == null)
		{
			li = new ArrayList<ETLTestAnnotation>();
			annotationMap.put(ano.getName(), li);
		}

		li.add(ano);
	}

	@Override
	public List<ETLTestAnnotation> getAnnotations()
	{
		return Collections.unmodifiableList(annotations);
	}

	@Override
	public List<ETLTestAnnotation> getAnnotations(String name)
	{
		List<ETLTestAnnotation> list = annotationMap.get(name);

		if (list == null)
		{
			return Collections.EMPTY_LIST;
		}

		return Collections.unmodifiableList(list);
	}

	@Override
	public boolean hasAnnotation(String name)
	{
		return annotationMap.containsKey(name);
	}

	public String getDescription()
	{
		if (description != null)
		{
			return description;
		}

		if (!hasAnnotation("@Description"))
		{
			return "";
		}

		List<ETLTestAnnotation> desc = getAnnotations("@Description");

		ETLTestAnnotation dAnn = desc.get(0);

		if (!dAnn.hasValue())
		{
			throw new IllegalStateException("@Description annotation missing description attribute");
		}
		else if (dAnn.getValue().getValueType() != ETLTestValueObject.value_type.object)
		{
			throw new IllegalStateException("@Description annotation must have an object type argument");
		}

		ETLTestValueObject val = dAnn.getValue().getValueAsMap().get("description");

		if (val == null)
		{
			throw new IllegalStateException("@Description annotation missing description attribute");
		}
		else if (val.getValueType() == ETLTestValueObject.value_type.quoted_string)
		{
			description = val.getValueAsString();
		}
		else if (val.getValueType() == ETLTestValueObject.value_type.list)
		{
			List<ETLTestValueObject> strlist = val.getValueAsList();

			StringBuilder stb = new StringBuilder();

			int count = 0;
			for (ETLTestValueObject str : strlist)
			{
				if (str.getValueType() != ETLTestValueObject.value_type.quoted_string)
				{
					throw new IllegalStateException("@Description annotation has invalid entry");
				}

				if (count != 0)
				{
					stb.append("\n");
				}

				count++;
				stb.append(str.getValueAsString());
			}

			description = stb.toString();
		}
		else
		{
			throw new IllegalStateException("@Description annotation invalid");
		}

		return description;
	}
}
