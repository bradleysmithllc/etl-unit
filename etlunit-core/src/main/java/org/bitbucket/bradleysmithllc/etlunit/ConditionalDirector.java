package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

public class ConditionalDirector implements ClassDirector
{
	/**
	 * Condition types:
	 * 	and - both directors must accept or it is rejected
	 * 	or - if either director accepts it is accepted
	 * 	not - it is accepted IIF the ldirector accepts and the rdirector does not accept
	 * 	eor - it is accepted if EXCACTLY one director accepts, but not both or neither
	 */
	public enum condition
	{
		and,
		or,
		not,
		eor
	}

	private final ClassDirector ldirector;
	private final condition icondition;
	private final ClassDirector rdirector;

	public ConditionalDirector(ClassDirector ldirector, condition icondition, ClassDirector rdirector) {
		this.ldirector = ldirector;
		this.icondition = icondition;
		this.rdirector = rdirector;
	}

	@Override
	public void beginBroadcast() {
		try
		{
			ldirector.beginBroadcast();
		}
		finally
		{
			rdirector.beginBroadcast();
		}
	}

	@Override
	public response_code accept(ETLTestClass cl) {
		response_code lresponse = ldirector.accept(cl);
		response_code rresponse = rdirector.accept(cl);

		return check(lresponse, rresponse);
	}

	private response_code check(response_code lresponse, response_code rresponse) {
		switch (icondition) {
			case and:
				if (lresponse == response_code.reject || rresponse == response_code.reject)
				{
					return response_code.reject;
				}
				else if (lresponse == response_code.defer || rresponse == response_code.defer)
				{
					return response_code.defer;
				}

				return response_code.accept;
			case not:
				if (lresponse == response_code.reject || lresponse == response_code.defer)
				{
					return lresponse;
				}
				// ldirector has accepted.  accept if the rdirector has NOT accepted
				else if (rresponse != response_code.accept)
				{
					return response_code.accept;
				}

				return response_code.reject;
			case eor:
				// we accept if either has accepted, but not both
				if (
					(lresponse == response_code.accept || rresponse == response_code.accept)
					&&
					(lresponse != rresponse)
				)
				{
					return response_code.accept;
				}

				return response_code.reject;
			case or:
				if (lresponse == response_code.reject && rresponse == response_code.reject)
				{
					return response_code.reject;
				}
				else if (lresponse == response_code.defer && rresponse == response_code.defer)
				{
					return response_code.defer;
				}

				return response_code.accept;
			default:
				throw new IllegalStateException();
		}
	}

	@Override
	public response_code accept(ETLTestMethod mt) {
		response_code lresponse = ldirector.accept(mt);
		response_code rresponse = rdirector.accept(mt);

		return check(lresponse, rresponse);
	}

	@Override
	public response_code accept(ETLTestOperation op) {
		response_code lresponse = ldirector.accept(op);
		response_code rresponse = rdirector.accept(op);

		return check(lresponse, rresponse);
	}

	@Override
	public void endBroadcast() {
		try
		{
			ldirector.endBroadcast();
		}
		finally
		{
			rdirector.endBroadcast();
		}
	}

	public ClassDirector getLdirector() {
		return ldirector;
	}

	public condition getCondition() {
		return icondition;
	}

	public ClassDirector getRdirector() {
		return rdirector;
	}
}
