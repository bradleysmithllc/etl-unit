package org.bitbucket.bradleysmithllc.etlunit.parser.datadiff;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class NearDiffResult {
	private final int comparisonResult;
	private final String explanation;

	private static final NearDiffResult match = new NearDiffResult(0, "==");

	public NearDiffResult(int comparisonResult, String explanation) {
		this.comparisonResult = comparisonResult;
		this.explanation = explanation;
	}

	public static NearDiffResult matches() {
		return match;
	}

	public int getComparisonResult() {
		return comparisonResult;
	}

	public boolean comparisonSucceeded() {
		return comparisonResult == 0;
	}

	public String getExplanation() {
		return explanation;
	}
}
