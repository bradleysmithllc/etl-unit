package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FrameOutputStream extends OutputStream
{
	private final DataOutputStream dout;

	public FrameOutputStream(OutputStream out) {
		dout = new DataOutputStream(out);
	}

	@Override
	public void write(int b) throws IOException {
		write(new byte[]{(byte) (b & 0xff)});
	}

	@Override
	public void write(byte[] b) throws IOException {
		write(b, 0, b.length);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		StreamFrame sf = new StreamFrame(b, off, len);

		sf.write(dout);
	}

	@Override
	public void flush() throws IOException {
		dout.flush();
	}

	@Override
	public void close() throws IOException {
		// write out an empty frame to signify EOS
		new StreamFrame().write(dout);
		dout.close();
	}
}
