package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

public class MessageLine {
	/*
	* Format a message for console output.  The result will be: <message>, followed by <rightPaddingSpaces>
	* right_padding is the number of spaces required after the message
	* right_padding := spaceChar * rightPaddingSpaces
	*
	* trimmed_message is the maximum number of characters from the original message that can be displayed.
	* trimmed_message := left(message, maxLineLength - minLinePaddingChars - rightPaddingSpaces)
	*
	* actual_message := trimmed_message + right_padding
	*
	* line_padding := linePaddingChar * (maxLineLength - actual_message.length)
	*
	* line := actual_message + line_padding
	*/
	public static String formatMessage(String message, char spaceChar, int rightPaddingSpaces, char linePaddingChar, int minLinePaddingChars, int maxLineLength) {
		// special case.  If message is blank, return full line of line padding (no right spaces)
		if (message.equals("")) {
			return StringUtils.rightPad("", maxLineLength, linePaddingChar);
		}

		String rightPadding = formatRightPadding(spaceChar, rightPaddingSpaces);
		String trimmedMessage = formatTrimmedMessage(message, maxLineLength, minLinePaddingChars, rightPaddingSpaces);

		String actualMessage = trimmedMessage + rightPadding;

		return StringUtils.rightPad(actualMessage, maxLineLength, linePaddingChar);
	}

	public static String formatTrimmedMessage(String message, int maxLineLength, int minLinePaddingChars, int rightPaddingSpaces) {
		String result = message;
		int maxLength = maxLineLength - minLinePaddingChars - rightPaddingSpaces;

		if (maxLength < 0) {
			throw new IllegalArgumentException("Negative length");
		} else if (maxLength < message.length()) {
			result = message.substring(0, maxLength);
		}

		return result;
	}

	public static String formatRightPadding(char spaceChar, int rightPaddingSpaces) {
		if (rightPaddingSpaces < 0) {
			throw new IllegalArgumentException("Negative length");
		}

		return StringUtils.rightPad("", rightPaddingSpaces, spaceChar);
	}

	public static String formatStandardMessage(String message) {
		return formatMessage(message, ' ', 3, '-', 3, 82);
	}
}
