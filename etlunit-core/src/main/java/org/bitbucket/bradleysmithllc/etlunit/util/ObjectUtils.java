package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class ObjectUtils
{
	public static <T> T firstNotNull(T... types)
	{
		for (T t : types)
		{
			if (t != null)
			{
				return t;
			}
		}

		return null;
	}

	public static boolean notIn(Object baseValue, Object... searchList) {
		return !in(baseValue, searchList);
	}

	public static boolean in(Object baseValue, Object... searchList) {
		for (Object srch : searchList) {
			if (srch == baseValue) {
				return true;
			}
		}

		return false;
	}
}
