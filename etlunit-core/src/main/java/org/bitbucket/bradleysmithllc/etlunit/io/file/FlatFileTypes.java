package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

class FlatFileTypes {
	private Map<String,DataConverter> typeMap;

	FlatFileTypes()
	{
		ServiceLoader<DataConverter> services = ServiceLoader.load(DataConverter.class, Thread.currentThread().getContextClassLoader());

		typeMap = new HashMap<String, DataConverter>();

		for (DataConverter service : services)
		{
			installConverter(service);
		}
	}

	/**
	 * Test interface - just add a service type directly and bypass the classpath stuff
	 * @param converter
	 * @return
	 */
	public void installConverter(DataConverter converter)
	{
		if (typeMap.containsKey(converter.getId()))
		{
			throw new IllegalArgumentException("Converter already installed: " + converter.getId());
		}
		else
		{
			typeMap.put(converter.getId(), converter);
		}
	}

	public DataConverter getById(String id)
	{
		DataConverter dataConverter = typeMap.get(id);

		if (dataConverter == null)
		{
			throw new IllegalArgumentException("Converter not found for type: " + id);
		}

		return dataConverter;
	}
}
