package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import java.util.Map;

public interface TestResultMetrics
{
	int getNumberOfTestsRun();

	int getNumberOfTestsPassed();

	int getNumberOfAssertionFailures();

	int getNumberOfWarnings();

	int getNumberIgnored();

	int getNumberOfErrors();

	void addStatus(ETLTestMethod method, StatusReporter.CompletionStatus status);

	void addTestsPassed(int num);
	void addTestsIgnored(int num);

	void addAssertionFailures(int num);

	void addTestWarnings(int num);

	void addErrors(int num);

	void reset();

	Map<String, StatusReporter.CompletionStatus> getResultsMapByTest();
}
