package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

import java.io.File;
import java.util.List;

public class PlainTextDiffManagerImpl implements DiffManagerImpl
{
	@Override
	public DiffGrid reportDiff(ETLTestMethod context, ETLTestOperation method, String failureId, String expectedId, String actualId)
	{
		return new DiffGrid()
		{
			@Override
			public DiffGridRow addRow(int lineNo, int tarLine, line_type type)
			{
				return new DiffGridRow()
				{
					@Override
					public void setColumnName(String col)
					{
					}

					@Override
					public void setOrderKey(String key)
					{
					}

					@Override
					public void setSourceValue(String value)
					{
					}

					@Override
					public void setTargetValue(String value)
					{
					}

					@Override
					public void done()
					{
					}
				};
			}

			@Override
			public void done()
			{
			}
		};
	}

	@Override
	public DataSetGrid reportDataSet(ETLTestOperation method, List<String> columns, String failureId)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public File findDiffReport(ETLTestMethod method) {
		return null;
	}

	@Override
	public void setOutputDirectory(File file)
	{
	}

	@Override
	public void dispose()
	{
	}
}
