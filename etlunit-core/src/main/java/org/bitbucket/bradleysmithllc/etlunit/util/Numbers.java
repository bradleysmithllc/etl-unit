package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class Numbers {
	public static String DIGITS = "172531495763718694827967554341261572583592715689654372513867492";

	public static String leftPad(Object num, int numPads) {
		return org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(num), numPads, '0');
	}

	public static String makeIntegralDigits(int rowCurrentValue, int maxNumDigits) {
		return makeDigits(rowCurrentValue, maxNumDigits, false);
	}

	public static String makeDecimalDigits(int rowCurrentValue, int maxNumDigits) {
		return makeDigits(rowCurrentValue, maxNumDigits, true);
	}

	public static String makeDigits(int rowCurrentValue, int maxNumDigits, boolean pad)
	{
		StringBuilder builder = new StringBuilder();

		if (maxNumDigits <= 0) {
			throw new IllegalArgumentException("Too few digits");
		}

		// determine the number of digits required.
		int numDigits = (rowCurrentValue % maxNumDigits) + 1;

		String resultNum;

		if (rowCurrentValue % 20 == 0) {
			resultNum = "0";
		} else {
			// start with the currentValue index
			int startIndex = rowCurrentValue % DIGITS.length();

			StringBuilder stb = new StringBuilder();

			int digitsProvided = 0;

			while (digitsProvided < numDigits) {
				int theseDigits = Math.min(DIGITS.length() - startIndex, numDigits - digitsProvided);

				stb.append(DIGITS.substring(startIndex, startIndex + theseDigits));

				digitsProvided += theseDigits;
				startIndex = 0;
			}

			resultNum = stb.toString();
		}

		return pad ? leftPad(resultNum, maxNumDigits) : resultNum;
	}
}
