package org.bitbucket.bradleysmithllc.etlunit.context;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObjectImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import java.io.IOException;
import java.util.*;

public class VariableContextImpl implements VariableContext
{
	private final Map<String, ETLTestValueObject> variableMap = new HashMap<String, ETLTestValueObject>();
	private final VariableContext enclosingContext;
	private final boolean enclosingVisible;

	class MapRepresentation implements Map<String, ETLTestValueObject>
	{
		@Override
		public int size()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isEmpty()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean containsKey(Object o)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean containsValue(Object o)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public ETLTestValueObject get(Object o)
		{
			return getValue(String.valueOf(o));
		}

		@Override
		public ETLTestValueObject put(String s, ETLTestValueObject o)
		{
			setValue(s, o);

			return null;
		}

		@Override
		public ETLTestValueObject remove(Object o)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public void putAll(Map<? extends String, ? extends ETLTestValueObject> map)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public void clear()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public Set<String> keySet()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public Collection<ETLTestValueObject> values()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public Set<Map.Entry<String, ETLTestValueObject>> entrySet()
		{
			throw new UnsupportedOperationException();
		}
	}

	private final MapRepresentation mapRepresentation = new MapRepresentation();

	private static final VariableContext NULL_CONTEXT = new VariableContext()
	{
		@Override
		public void declareVariable(String variableName)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public boolean hasVariableBeenDeclared(String variableName)
		{
			return false;
		}

		@Override
		public void removeVariable(String variableName)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public ETLTestValueObject query(String variablePath)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public ETLTestValueObject getValue(String variableName)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public void setValue(String variableName, ETLTestValueObject value)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public void setStringValue(String variableName, String value)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public void setJSONValue(String variableName, String value)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public void declareAndSetValue(String variableName, ETLTestValueObject value)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public void declareAndSetStringValue(String variableName, String value)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public void declareAndSetJSONValue(String variableName, String value)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public VariableContext createNestedScope(boolean enclosing)
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public VariableContext createNestedScope() {
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public VariableContext getEnclosingScope()
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public VariableContext getTopLevelScope()
		{
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public Map<String, ETLTestValueObject> getMapRepresentation()
		{
			return Collections.emptyMap();
		}

		@Override
		public Map<String, Object> getVelocityWrapper()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public String contextualize(String argument) {
			throw new IllegalStateException("NULL Context is non-functional");
		}

		@Override
		public JsonNode getJsonNode() {
			try {
				return JsonLoader.fromString("{}");
			} catch (IOException e) {
				throw new IllegalArgumentException();
			}
		}
	};

	private static final ETLTestValueObject NULL = new ETLTestValueObjectImpl("null");
	private VariableVelocityWrapper variableVelocityWrapper = new VariableVelocityWrapper(this);

	private VariableContextImpl(VariableContext context, boolean enclosingVisible)
	{
		enclosingContext = context;
		this.enclosingVisible = enclosingVisible;
	}

	public VariableContextImpl()
	{
		enclosingContext = NULL_CONTEXT;
		enclosingVisible = false;
	}

	@Override
	public ETLTestValueObject getValue(String variableName)
	{
		if (!variableMap.containsKey(variableName))
		{
			if (enclosingVisible)
			{
				return enclosingContext.getValue(variableName);
			}
			else
			{
				throw new IllegalArgumentException("Variable " + variableName + " is undeclared");
			}
		}

		ETLTestValueObject v = variableMap.get(variableName);

		if (v == NULL)
		{
			return null;
		}

		//somehow convert the value using the velocity wrapper
		return v;
	}

	@Override
	public void setStringValue(String variableName, String value)
	{
		setValue(variableName, value == null ? null : new ETLTestValueObjectImpl(value));
	}

	@Override
	public void setJSONValue(String variableName, String value)
	{
		try
		{
			setValue(variableName, ETLTestParser.loadObject(value));
		}
		catch (ParseException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public void declareAndSetValue(String variableName, ETLTestValueObject value)
	{
		if (!hasVariableBeenDeclared(variableName))
		{
			declareVariable(variableName);
		}

		setValue(variableName, value);
	}

	@Override
	public void declareAndSetStringValue(String variableName, String value)
	{
		if (!hasVariableBeenDeclared(variableName))
		{
			declareVariable(variableName);
		}

		setStringValue(variableName, value);
	}

	@Override
	public void declareAndSetJSONValue(String variableName, String value)
	{
		if (!hasVariableBeenDeclared(variableName))
		{
			declareVariable(variableName);
		}

		setJSONValue(variableName, value);
	}

	@Override
	public void setValue(String variableName, ETLTestValueObject value)
	{
		if (!variableMap.containsKey(variableName))
		{
			if (enclosingVisible)
			{
				enclosingContext.setValue(variableName, value);
			}
			else
			{
				throw new IllegalArgumentException("Variable " + variableName + " is undeclared");
			}
		}
		else
		{
			variableMap.put(variableName, value == null ? NULL : value);
		}
	}

	@Override
	public void declareVariable(String variableName)
	{
		if (variableMap.containsKey(variableName))
		{
			throw new IllegalArgumentException("Variable " + variableName + " already exists in this scope");
		}

		variableMap.put(variableName, NULL);
	}

	@Override
	public boolean hasVariableBeenDeclared(String variableName)
	{
		return variableMap.containsKey(variableName) || enclosingContext.hasVariableBeenDeclared(variableName);
	}

	@Override
	public void removeVariable(String variableName)
	{
		if (!variableMap.containsKey(variableName))
		{
			if (enclosingVisible)
			{
				enclosingContext.removeVariable(variableName);
			}
			else
			{
				throw new IllegalArgumentException("Variable " + variableName + " not declared in this scope");
			}
		}

		variableMap.remove(variableName);
	}

	@Override
	public ETLTestValueObject query(String variablePath)
	{
		String[] pathElements = variablePath.split("\\.");

		ETLTestValueObject value = getValue(pathElements[0]);

		if (pathElements.length == 1)
		{
			return value;
		}
		else
		{
			return value.query(variablePath.substring(pathElements[0].length() + 1));
		}
	}

	@Override
	public VariableContext createNestedScope(boolean enclosingVisible)
	{
		return new VariableContextImpl(this, enclosingVisible);
	}

	@Override
	public VariableContext createNestedScope()
	{
		return createNestedScope(true);
	}

	@Override
	public VariableContext getEnclosingScope()
	{
		if (!enclosingVisible)
		{
			// pretend the path upward ends here . . .
			return null;
		}
		else if (enclosingContext == NULL_CONTEXT)
		{
			// the path upward really does end here . . .
			return null;
		}
		else
		{
			// onward and upward
			return  enclosingContext;
		}
	}

	@Override
	public VariableContext getTopLevelScope()
	{
		VariableContext scope = this;
		VariableContext nextScope = this;

		while ((nextScope = scope.getEnclosingScope()) != null)
		{
			scope = nextScope;
		}

		return scope;
	}

	@Override
	public Map<String, ETLTestValueObject> getMapRepresentation()
	{
		return mapRepresentation;
	}

	@Override
	public Map<String, Object> getVelocityWrapper()
	{
		return variableVelocityWrapper;
	}

	@Override
	public String contextualize(String argument) {
		try {
			return VelocityUtil.writeTemplate(argument, getVelocityWrapper());
		} catch (Exception e) {
			throw new IllegalArgumentException("Error processing value '" + argument + "' as a template", e);
		}
	}

	@Override
	public JsonNode getJsonNode() {
		StringBuilder stb = new StringBuilder();

		stb.append("{");
		int count = 0;
		for (Map.Entry<String, ETLTestValueObject> entry : variableMap.entrySet())
		{
			if (count != 0)
			{
				stb.append(',');
			}

			count++;
			stb.append("\"").append(entry.getKey()).append("\"").append(':');
			ETLTestValueObject vobj = entry.getValue();
			stb.append(vobj.getJsonNode().toString());
		}
		stb.append("}");

		String json = stb.toString();
		try {
			return JsonLoader.fromString(json);
		} catch (IOException e) {
			throw new IllegalArgumentException(json);
		}
	}

	@Override
	public String toString() {
		return "VariableContextImpl{" +
				"variableMap=" + variableMap +
				", enclosingVisible=" + enclosingVisible +
				'}';
	}
}
