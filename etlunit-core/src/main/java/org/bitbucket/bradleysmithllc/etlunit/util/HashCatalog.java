package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.*;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class HashCatalog
{
	public enum operationType
	{
		add,
		delete,
		modify
	}

	public interface Change
	{
		operationType getOperationType();

		File getFile();

		String getPathToFile();
	}

	private final File dir;
	public static final String DEFAULT_HASH = "00000000000000000000000000000000";
	private String hash = null;

	public interface Entry
	{
		String getName();

		String getHash();

		File getFile();

		HashCatalog getHashCatalog();
	}

	private final class ChangeIterator implements Iterator<Change>
	{
		private final HashCatalog otherCatalog;

		private ChangeIterator(HashCatalog other)
		{
			otherCatalog = other;
		}

		@Override
		public boolean hasNext()
		{
			if (otherCatalog.getHash().equals(getHash()))
			{
				return false;
			}

			return false;
		}

		@Override
		public Change next()
		{
			return null;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}

	private HashCatalog(File pDir)
	{
		if (!pDir.isDirectory())
		{
			throw new IllegalArgumentException("Bad file argument - not a directory");
		}

		dir = pDir;

		getHash();
	}

	public static HashCatalog createFromDirectory(File dir)
	{
		return new HashCatalog(dir);
	}

	public Iterator<Change> getChanges(HashCatalog other)
	{
		return new ChangeIterator(other);
	}

	public String getHash()
	{
		if (hash != null)
		{
			return hash;
		}

		try
		{
			File f = getHashFile();

			if (f.exists())
			{
				hash = IOUtils.readFileToString(f);
			}
		}
		catch (Exception exc)
		{
			throw new RuntimeException(exc);
		}

		if (hash != null)
		{
			return hash;
		}

		hash = DEFAULT_HASH;

		StringBuffer hashBuffer = new StringBuffer();

		List<Entry> li = getEntries();

		if (!li.isEmpty())
		{
			Iterator<Entry> it = li.iterator();

			while (it.hasNext())
			{
				hashBuffer.append(it.next().getHash());
			}

			try
			{
				hash = strHashStream(new StringBufferInputStream(hashBuffer.toString()));
			}
			catch (Exception exc)
			{
				throw new RuntimeException(exc);
			}
		}

		return hash;
	}

	public File getHashFile()
	{
		return new File(dir, ".infaunit_md5");
	}

	public void persist() throws Exception
	{
		FileWriter fw = new FileWriter(new File(dir, ".infaunit_md5"));

		try
		{
			fw.write(getHash());
		}
		finally
		{
			fw.close();
		}

		fw = new FileWriter(getCatalogFile());

		try
		{
			fw.write("NAME|MD5");

			List<Entry> li = getEntries();

			Iterator<Entry> i = li.iterator();

			while (i.hasNext())
			{
				Entry e = i.next();

				fw.write("\n" + e.getName() + "|" + e.getHash());

				if (e.getFile().isDirectory())
				{
					e.getHashCatalog().persist();
				}
			}
		}
		finally
		{
			fw.close();
		}
	}

	public File getCatalogFile()
	{
		return new File(dir, ".infaunit_catalog");
	}

	public List<Entry> getEntries()
	{
		class EntryImpl implements Entry
		{
			private final String name;
			private final String hash;
			private final File file;
			private final HashCatalog hashCatalog;

			EntryImpl(String n, String h, File f, HashCatalog hc)
			{
				name = n;
				hash = h;
				file = f;
				hashCatalog = hc;
			}

			@Override
			public String getName()
			{
				return name;
			}

			@Override
			public String getHash()
			{
				return hash;
			}

			@Override
			public File getFile()
			{
				return file;
			}

			@Override
			public HashCatalog getHashCatalog()
			{
				return hashCatalog;
			}
		}

		final List<Entry> list = new ArrayList<Entry>();

		File [] files = dir.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File pathname)
			{
				String name = pathname.getName().toUpperCase();

				if (name.startsWith(".INFAUNIT_"))
				{
					return false;
				}

				return true;
			}
		}
		);

		List<File> list1 = Arrays.asList(files);
		Collections.sort(list1);

		for (File file : list1)
		{
				String hash = null;

				HashCatalog hc = null;

				if (file.isDirectory()) {
					hc = HashCatalog.createFromDirectory(file);

					hash = hc.getHash();
				} else {
					try {
						FileInputStream fin = new FileInputStream(file);

						try {
							hash = strHashStream(fin);
						} finally {
							fin.close();
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}

			list.add(new EntryImpl(file.getName().toUpperCase(), hash, file, hc));
		}

		return list;
	}

	public File getDir()
	{
		return dir;
	}

	public void refresh() throws Exception
	{
		hash = null;

		getHash();

		persist();
	}

	public static String strHashStream(InputStream in) throws Exception
	{
		byte[] b = hashStream(in);

		return Hex.hexEncode(b);
	}

	private static byte[] hashStream(InputStream in) throws IOException, DigestException, NoSuchAlgorithmException
	{
		MessageDigest md5 = MessageDigest.getInstance("MD5");

		byte[] buffer = new byte[1024];

		int i = 0;

		while ((i = in.read(buffer)) != -1)
		{
			md5.update(buffer, 0, i);
		}

		return md5.digest();
	}
}
