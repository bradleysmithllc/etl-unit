package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.Incomplete;
import org.bitbucket.bradleysmithllc.etlunit.util.NeedsTest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

@Incomplete
@NeedsTest
public class Configuration {
	private final ETLTestValueObject mapping;

	public Configuration(String config) {
		this(init(config));
	}

	public Configuration(ETLTestValueObject pMapping) {
		if (pMapping.getValueType() != ETLTestValueObject.value_type.object) {
			throw new IllegalArgumentException("Configuration must be an object value type");
		}

		mapping = pMapping;
	}

	private static ETLTestValueObject init(String confObject) {
		try {
			String str2 = confObject.trim();

			if (str2.length() > 0 && (str2.charAt(0) == '{') && (str2.charAt(str2.length() - 1) == '}')) {
				return ETLTestParser.loadObject(confObject);
			} else {
				return ETLTestParser.loadBareObject(confObject);
			}
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public ETLTestValueObject getRoot() {
		return mapping;
	}

	public ETLTestValueObject query(String path) {
		return mapping.query(path);
	}

	public static Configuration loadFromEnvironment(File[] configDirs) throws IOException, ParseException {
		return loadFromEnvironment(configDirs, (String[]) null);
	}

	public static Configuration loadFromEnvironment(File[] configDirs, String override) throws IOException, ParseException {
		return loadFromEnvironment(configDirs, override, null);
	}

	public static Configuration loadFromEnvironment(File[] configDirs, String[] overrides) throws IOException, ParseException {
		return loadFromEnvironment(configDirs, overrides, null);
	}

	public static Configuration loadFromEnvironment(File[] configDirs, String override, ClassLoader clsLdr)
			throws IOException, ParseException {
		return loadFromEnvironment(configDirs, override, clsLdr, null);
	}

	public static Configuration loadFromEnvironment(File[] configDirs, String[] overrides, ClassLoader clsLdr)
			throws IOException, ParseException {
		return loadFromEnvironment(configDirs, overrides, clsLdr, null);
	}

	public static Configuration loadFromEnvironment(File[] configDirs, String override, ClassLoader clsLdr, ETLTestValueObject superOverride)
			throws IOException, ParseException {
		return loadFromEnvironment(configDirs, override == null ? null : new String[]{override}, clsLdr, superOverride);
	}

	public static Configuration loadFromEnvironment(File[] configDirs, String[] overrides, ClassLoader clsLdr, ETLTestValueObject superOverride)
			throws IOException, ParseException {
		if (clsLdr == null) {
			clsLdr = Thread.currentThread().getContextClassLoader();
		}

		/* Order should be:
				1 - local profile
				2 - local etlunit
				3 - remote profile
				4 - remote etlunit
				5 - user profile
				6 - user etlunit

				It is not ideal for there to be a very complex hierarchy of overrides - the mechanism is provided here
				so that it may be utilized as broadly as possible.
		*/
		ETLTestValueObject userHomeObject = null;
		ETLTestValueObject configDirObject = null;
		ETLTestValueObject classPathObject = null;

		// process in reverse order
		if (!isIgnoreUserHome()) {
			// look for a local config in the user home dir - it's the ultimate master
			File rootConfig = getEtlunitUserConfig();

			ETLTestValueObject overrideObject = ETLTestParser.loadObject(IOUtils.readFileToString(rootConfig));

			userHomeObject = overrideObject;

			// look for a local override profile
			if (overrides != null) {
				for (String override : overrides) {
					File overrideConfig = getEtlunitUserProfile(override);

					overrideObject = ETLTestParser.loadObject(IOUtils.readFileToString(overrideConfig));

					if (userHomeObject != null) {
						userHomeObject = userHomeObject.merge(overrideObject, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.recursive);
					} else {
						userHomeObject = overrideObject;
					}
				}
			}
		}

		// look for a config file in the local config directory
		if (configDirs != null) {
			// give each configuration source a shot at providing context
			for (File file : configDirs) {
				File configBase = new File(file, "etlunit.json");

				if (configBase.exists()) {
					ETLTestValueObject baseObject = ETLTestParser.loadObject(IOUtils.readFileToString(configBase));

					if (configDirObject != null) {
						configDirObject = configDirObject.merge(baseObject, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
					} else {
						configDirObject = baseObject;
					}
				}

				if (overrides != null) {
					for (String override : overrides) {
						File overrideBase = new File(file, override + ".json");

						if (overrideBase.exists()) {
							ETLTestValueObject overrideObject = ETLTestParser.loadObject(IOUtils.readFileToString(overrideBase));

							if (configDirObject != null) {
								configDirObject = configDirObject.merge(overrideObject, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.recursive);
							} else {
								configDirObject = overrideObject;
							}
						}
					}
				}
			}
		}

		// check the classpath for base configuration resources
		Enumeration<URL> enu = clsLdr.getResources("config/etlunit.json");

		while (enu.hasMoreElements()) {
			URL url = enu.nextElement();

			ETLTestValueObject remoteConfigObjectBase = ETLTestParser.loadObject(IOUtils.readURLToString(url));

			if (classPathObject != null) {
				classPathObject = classPathObject.merge(remoteConfigObjectBase, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
			} else {
				classPathObject = remoteConfigObjectBase;
			}
		}

		// check the classpath for override configuration resources
		if (overrides != null) {
			for (String override : overrides) {
				enu = clsLdr.getResources("config/" + override + ".json");

				while (enu.hasMoreElements()) {
					URL url = enu.nextElement();

					ETLTestValueObject remoteConfigObjectBase = ETLTestParser.loadObject(IOUtils.readURLToString(url));

					if (classPathObject != null) {
						classPathObject = classPathObject.merge(remoteConfigObjectBase, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.recursive);
					} else {
						classPathObject = remoteConfigObjectBase;
					}
				}
			}
		}

		ETLTestValueObject configObject = null;

		if (configDirObject != null)
		{
			configObject = configDirObject;

			if (classPathObject != null)
			{
				configObject = configObject.merge(classPathObject, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
			}

			if (userHomeObject != null)
			{
				configObject = configObject.merge(userHomeObject, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
			}
		}
		else if (classPathObject != null)
		{
			configObject = classPathObject;

			if (userHomeObject != null)
			{
				configObject = configObject.merge(userHomeObject, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
			}
		}
		else
		{
			configObject = userHomeObject;
		}

		if (configObject == null) {
			// can't survive this
			throw new IllegalArgumentException("No configuration found");
		}

		if (superOverride != null) {
			configObject = configObject.merge(superOverride, ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.recursive);
		}

		return new Configuration(configObject);
	}

	public static void ignoreUserHome(boolean state)
	{
		if (state)
		{
			System.setProperty("etlunit.ignore.user.home", "true");
		}
		else
		{
			System.clearProperty("etlunit.ignore.user.home");
		}
	}

	public static boolean isIgnoreUserHome()
	{
		return System.getProperty("etlunit.ignore.user.home") != null;
	}

	public static File getEtlunitUserHome() {
		File etlunitUserHome = new File(FileUtils.getUserDirectory(), ".etlunit");
		if (!etlunitUserHome.exists()) {
			etlunitUserHome.mkdirs();
		}

		return etlunitUserHome;
	}

	public static File getEtlunitUserConfig() throws IOException {
		File rootConfig = new File(getEtlunitUserHome(), "etlunit.json");

		if (!rootConfig.exists()) {
			FileUtils.write(rootConfig, "{}");
		}

		return rootConfig;
	}

	public static File getEtlunitUserProfile(String name) throws IOException {
		File overrideConfig = new File(getEtlunitUserHome(), name + ".json");

		if (!overrideConfig.exists()) {
			FileUtils.write(overrideConfig, "{}");
		}

		return overrideConfig;
	}
}
