package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

public class FeatureProxyBase<K>
{
	interface Getter<K>
	{
		K get(Feature f);
	}

	private final Getter<K> getter;
	private final List<Feature> list;

	private final List<K> klist = new ArrayList<K>();

	private int lastSize = -1;

	public FeatureProxyBase(List<Feature> list, Getter<K> getter)
	{
		this.list = list;
		this.getter = getter;
	}

	/**
	 * Not sure why this can grow - late-arriving features?
	 * @return
	 */
	public synchronized List<K> getList()
	{
		if (list.size() == lastSize)
		{
			return klist;
		}

		klist.clear();

		for (Feature feature : list)
		{
			K k = getter.get(feature);

			if (k != null)
			{
				klist.add(k);
			}
		}

		lastSize = list.size();

		return klist;
	}
}
