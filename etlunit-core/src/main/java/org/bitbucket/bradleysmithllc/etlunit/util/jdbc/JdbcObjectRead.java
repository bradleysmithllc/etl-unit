package org.bitbucket.bradleysmithllc.etlunit.util.jdbc;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class JdbcObjectRead {
	public static Object readObject(ResultSet resultSet, int index, int columnType) throws SQLException {
		switch (columnType) {
			case Types.DATE:
				return resultSet.getObject(index, LocalDate.class);

			case Types.TIME:
			case Types.TIME_WITH_TIMEZONE:
				return resultSet.getObject(index, LocalTime.class);

			case Types.TIMESTAMP:
			case Types.TIMESTAMP_WITH_TIMEZONE:
				return resultSet.getObject(index, LocalDateTime.class);

			default:
				return resultSet.getObject(index);
		}
	}
}
