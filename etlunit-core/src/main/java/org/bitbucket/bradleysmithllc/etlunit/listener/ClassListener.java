package org.bitbucket.bradleysmithllc.etlunit.listener;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ExecutionContext;
import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.TestWarning;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;

import javax.annotation.Nullable;

public interface ClassListener extends OperationProcessor
{
	void beginTests(VariableContext context, int executorId) throws TestExecutionError;
	void beginTests(VariableContext context) throws TestExecutionError;

	void beginPackage(ETLTestPackage name, VariableContext context, int executorId) throws TestAssertionFailure, TestExecutionError, TestWarning;
	void begin(ETLTestClass cl, VariableContext context, int executorId) throws TestAssertionFailure, TestExecutionError, TestWarning;

	void declare(ETLTestVariable var, VariableContext context, int executorId);

	void begin(ETLTestMethod mt, VariableContext context, int executorId) throws TestAssertionFailure, TestExecutionError, TestWarning;

	/**
	 *
	 * @param mt - the method about to be processed.  May differ from the test the operation belongs
	 *           to in the case this operation is in a before or after test method.  Null for
	 *           before and after class methods.
	 * @param op
	 * @param parameters
	 * @param vcontext
	 * @param econtext
	 * @throws TestAssertionFailure
	 * @throws TestExecutionError
	 * @throws TestWarning
	 */
	void begin(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executorId)
			throws TestAssertionFailure, TestExecutionError, TestWarning;

	void end(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executorId)
			throws TestAssertionFailure, TestExecutionError, TestWarning;

	void end(ETLTestMethod mt, VariableContext context, int executorId) throws TestAssertionFailure, TestExecutionError, TestWarning;

	void end(ETLTestClass cl, VariableContext context, int executorId) throws TestAssertionFailure, TestExecutionError, TestWarning;

	void endPackage(ETLTestPackage name, VariableContext context, int executorId) throws TestAssertionFailure, TestExecutionError, TestWarning;
	void endTests(VariableContext context, int executorId);
	void endTests(VariableContext context);
}
