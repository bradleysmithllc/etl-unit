package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

public class CopyOptionsBuilder
{
	private boolean writeHeader = true;
	private boolean preordered = false;
	private DataFileManager.CopyOptions.missing_column_policy missingColumnPolicy = DataFileManager.CopyOptions.missing_column_policy.use_null;
	private String overrideExtractionSql;
	private DataFileSchema targetSchema;
	private Map<String, String> defaultValues = new HashMap<String, String>();
	private DataFileManager.CopyOptions.honor_date_diff honorDateDiff = DataFileManager.CopyOptions.honor_date_diff.no;

	public DataFileManager.CopyOptions options()
	{
		return new DataFileManager.CopyOptions() {
			@Override
			public boolean writeHeader() {
				return writeHeader;
			}

			@Override
			public boolean preordered() {
				return preordered;
			}

			@Override
			public missing_column_policy getMissingColumnPolicy() {
				return missingColumnPolicy;
			}

			@Override
			public honor_date_diff honorDateDiff() {
				return honorDateDiff;
			}

			@Override
			public Map<String, String> defaultValues() {
				return defaultValues;
			}

			@Override
			public String overrideExtractionSql() {
				return overrideExtractionSql;
			}

			@Override
			public DataFileSchema getTargetSchema() {
				return targetSchema;
			}
		};
	}

	public CopyOptionsBuilder preordered(boolean val)
	{
		preordered = val;
		return this;
	}

	public CopyOptionsBuilder writeHeader(boolean val)
	{
		writeHeader = val;
		return this;
	}

	public CopyOptionsBuilder missingColumnPolicy(DataFileManager.CopyOptions.missing_column_policy policy)
	{
		missingColumnPolicy = policy;
		return this;
	}

	public CopyOptionsBuilder withHonorDateDiff(DataFileManager.CopyOptions.honor_date_diff honorDateDiff) {
		this.honorDateDiff = honorDateDiff;
		return this;
	}

	public CopyOptionsBuilder defaultValue(String name, String value)
	{
		defaultValues.put(name, value);
		return this;
	}

	public CopyOptionsBuilder defaultValues(Map<String, String> values)
	{
		defaultValues.putAll(values);
		return this;
	}

	public CopyOptionsBuilder overrideExtractionSql(String sql)
	{
		overrideExtractionSql = sql;
		return this;
	}

	public CopyOptionsBuilder getTargetSchema(DataFileSchema schema)
	{
		targetSchema = schema;
		return this;
	}
}
