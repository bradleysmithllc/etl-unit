package org.bitbucket.bradleysmithllc.etlunit.util.jdbc;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class MetadataPrimaryKey extends MetadataSchema {
	private final String tableName;
	private final String pkName;
	private String columnName;
	private short keySequence;

	public MetadataPrimaryKey(String catalogName, String schemaName, String tableName, String pkName) {
		super(catalogName, schemaName);

		this.tableName = tableName;
		this.pkName = pkName;
	}

	public String tableName() {
		return tableName;
	}

	public String pkName() {
		return pkName;
	}

	public String columnName() {
		return columnName;
	}

	public MetadataPrimaryKey withColumnName(String columnName) {
		this.columnName = columnName;

		return this;
	}

	public short keySequence() {
		return keySequence;
	}

	public MetadataPrimaryKey withKeySequence(short keySequence) {
		this.keySequence = keySequence;

		return this;
	}
}
