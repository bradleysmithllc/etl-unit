package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

public class InfaTestVisitor
{
	public Object accept(ETLTestOperation op)
	{
		return Boolean.TRUE;
	}

	/**
	 * Process the operation with the results from the accept method calls.  If this method is overridden, the
	 * process method without the result parameters must not be or else each operation will be processed twice.
	 */
	public void process(ETLTestOperation op, TestContext defaultTestContext, Object classResult, Object methodResult, Object operationResult)
			throws Exception
	{
	}

	public void process(ETLTestOperation op, TestContext defaultTestContext) throws Exception
	{
	}

	public Object accept(ETLTestMethod method)
	{
		return Boolean.TRUE;
	}

	public Object accept(ETLTestClass next)
	{
		return Boolean.TRUE;
	}
}
