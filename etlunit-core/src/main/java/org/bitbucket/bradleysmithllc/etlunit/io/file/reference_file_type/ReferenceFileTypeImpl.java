package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class ReferenceFileTypeImpl implements ReferenceFileType, JsonSerializable
{
	private final String name;
	private final String id;
	private String defaultVersion;
	private String defaultVersionLower;
	private String defaultClassifier;
	private String defaultClassifierLower;
	private boolean loaded = false;

	private List<String> versions;
	private List<String> versionsLower;

	private final Map<String, ReferenceFileTypeClassifier> classifiers = new HashMap<String, ReferenceFileTypeClassifier>();
	private final Map<String, ReferenceFileTypeClassifier> classifiersLower = new HashMap<String, ReferenceFileTypeClassifier>();

	private String alias;
	private String description;

	private RuntimeSupport runtimeSupport;

	public ReferenceFileTypeImpl(String name, String id) {
		this.name = name;
		this.id = id;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport supp)
	{
		runtimeSupport = supp;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		if (alias != null)
		{
			writer.name("alias").value(alias);
		}
	}

	@Override
	public void fromJson(JsonNode node) {
		JsonNode vnode = node.get("alias");

		if (vnode != null && !vnode.isNull())
		{
			alias = vnode.asText();
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public synchronized void loadFromResource() {
		if (loaded)
		{
			return;
		}

		// only attempt this once - even if it fails
		loaded = true;

		// locate by the path.  Use the alias if provided
		String name = ObjectUtils.firstNotNull(alias, id);

		String simpleName = name.replace('.', '/').toLowerCase() + ".json";
		// first try the filesystem, then the classpath

		URL fileURL = null;

		if (runtimeSupport != null)
		{
			File src = runtimeSupport.getReferenceDirectory("file/fml");

			File catalog = new File(src, simpleName);
			if (catalog.exists())
			{
				try {
					fileURL = catalog.toURL();
				} catch (MalformedURLException e) {
					runtimeSupport.getApplicationLog().severe("What??", e);
				}
			}
		}

		String name1 = "reference/file/fml/" + simpleName;
		URL url = fileURL != null ? fileURL : ResourceUtils.getResource(getClass(), name1);

		if (url == null)
		{
			throw new IllegalArgumentException("Bad reference file type path: " + name1);
		}

		try {
			JsonNode node = JsonLoader.fromURL(url);

			ProcessingReport result = ReferenceFileTypeManagerImpl.referenceTypeSchema.validate(node);

			if (!result.isSuccess())
			{
				System.out.println("Invalid reference catalog on classpath: " + result.toString());
				return;
			}

			description = node.get("description").asText();

			JsonNode jsonNode = node.get("default-version");
			if (jsonNode != null && !jsonNode.isNull())
			{
				defaultVersion = jsonNode.asText();
				defaultVersionLower = defaultVersion.toLowerCase();
			}

			jsonNode = node.get("default-classifier");
			if (jsonNode != null && !jsonNode.isNull())
			{
				defaultClassifier = jsonNode.asText();
				defaultClassifierLower = defaultClassifier.toLowerCase();
			}

			JsonNode versionsNode = node.get("versions");

			if (versionsNode != null && !versionsNode.isNull())
			{
				versions = new ArrayList<String>();
				versionsLower = new ArrayList<String>();

				ArrayNode versionsArrayNode = (ArrayNode) versionsNode;

				if (versionsArrayNode.size() == 0)
				{
					throw new IllegalArgumentException("Invalid versions attribute - empty list");
				}

				Iterator<JsonNode> it = versionsArrayNode.iterator();

				while (it.hasNext())
				{
					String version = it.next().asText();
					versions.add(version);
					versionsLower.add(version.toLowerCase());
				}
			}

			jsonNode = node.get("classifiers");

			if (jsonNode != null && !jsonNode.isNull())
			{
				Iterator<Map.Entry<String,JsonNode>> fieldsNode = jsonNode.fields();

				while (fieldsNode.hasNext())
				{
					Map.Entry<String, JsonNode> fieldEntryNode = fieldsNode.next();

					String clss = fieldEntryNode.getKey();
					JsonNode fieldNode = fieldEntryNode.getValue();

					ReferenceFileTypeClassifierImpl impl = new ReferenceFileTypeClassifierImpl(clss);
					impl.fromJson(fieldNode);

					classifiersLower.put(clss.toLowerCase(), impl);
					classifiers.put(clss, impl);
				}
			}
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		} catch (ProcessingException e) {
			throw new IllegalArgumentException(e);
		}

		// sanity checks.  Make sure default version exists as a version, and repeat for default classifier
		if (defaultVersionLower != null)
		{
			if (versionsLower == null)
			{
				throw new IllegalArgumentException("Default version specified but no versions declared");
			}
			else if (!versionsLower.contains(defaultVersionLower))
			{
				throw new IllegalArgumentException("Default version specified does not exist in declared versions");
			}
		}

		if (defaultClassifierLower != null)
		{
			if (!classifiersLower.containsKey(defaultClassifierLower))
			{
				throw new IllegalArgumentException("Default classifier specified does not exist in declared classifiers");
			}
		}
	}

	@Override
	public String generateResourcePathForRef(ReferenceFileTypeRef ref) throws RequestedFileTypeNotFoundException {
		// ensure we are loaded
		loadFromResource();

		// check for a classifier, in case we have a default
		String clss = ref.hasClassifier() ? ref.getClassifier() : null;

		if (clss == null)
		{
			clss = defaultClassifierLower;
		}
		else
		{
			clss = clss.toLowerCase();

			// validate the classifier
			if (!classifiersLower.containsKey(clss))
			{
				throw new RequestedFileTypeNotFoundException("Type {" + id + "} does not publish classifier {" + clss + "}");
			}
		}


		// look up the classifier, then find the default version if it has one
		ReferenceFileTypeClassifier classifier = clss != null ? classifiersLower.get(clss) : null;

		// check the ref for a version - if not found use our default for the classifier if specified
		String vers = ref.hasVersion() ? ref.getVersion() : null;

		if (vers == null)
		{
			if (classifier == null)
			{
				vers = getDefaultVersion();
			}
			else
			{
				vers = classifier.getDefaultVersion();
			}
		}
		else
		{
			vers = vers.toLowerCase();

			// validate the version with the classifier
			if (classifier != null)
			{
				if (!classifier.hasVersions() || !classifier.getVersions().contains(vers))
				{
					throw new RequestedFileTypeNotFoundException("Type {" + id + "} does not publish version {" + vers + "}" + classifier != null ? (" {" + classifier.getId() + "}") : (""));
				}
			}
			else
			{
				if (!hasVersions() || !getVersions().contains(vers))
				{
					throw new RequestedFileTypeNotFoundException("Type {" + id + "} does not publish version {" + vers + "}");
				}
			}
		}

		// validate the version against the available versions
		if (classifier != null)
		{
			if (classifier.hasVersions() && !classifier.getVersions().contains(vers))
			{
				throw new IllegalArgumentException("Undefined schema version {" + vers + "} in classifier {" + classifier + "} {" + toString() + "}");
			}
		}
		else
		{
			if (hasVersions() && !getVersions().contains(vers))
			{
				throw new IllegalArgumentException("Undefined schema version {" + vers + "} {" + toString() + "}");
			}
		}

		// construct a URL to the resource
		StringBuilder builder = new StringBuilder(ObjectUtils.firstNotNull(alias, id).replace('.', '/'));

		// add classifier and version, in that order
		if (classifier != null)
		{
			builder.append("~").append(classifier.getId());
		}

		if (vers != null)
		{
			builder.append("+").append(vers);
		}

		// always return lower case strings
		return builder.toString().toLowerCase();
	}

	@Override
	public boolean hasVersions()
	{
		loadFromResource();
		return versions != null;
	}

	@Override
	public List<String> getVersions() {
		loadFromResource();
		return versionsLower == null ? (List) Collections.emptyList() : (List)  Collections.unmodifiableList(versionsLower);
	}

	@Override
	public String getDefaultVersion() {
		loadFromResource();
		return defaultVersionLower;
	}

	@Override
	public String getDefaultClassifier() {
		loadFromResource();
		return defaultClassifierLower;
	}

	@Override
	public Map<String, ReferenceFileTypeClassifier> getClassifiers() {
		loadFromResource();
		return Collections.unmodifiableMap(classifiers);
	}

	@Override
	public String getId() {
		loadFromResource();
		return id;
	}

	@Override
	public String getDescription() {
		loadFromResource();
		return description;
	}

	@Override
	public String toString() {
		return "ReferenceFileTypeImpl{" +
				"description='" + description + '\'' +
				", name='" + name + '\'' +
				", id='" + id + '\'' +
				", defaultVersion='" + defaultVersion + '\'' +
				", defaultClassifier='" + defaultClassifier + '\'' +
				'}';
	}

	@Override
	public int compareTo(ReferenceFileType o) {
		return getId().compareTo(o.getId());
	}
}
