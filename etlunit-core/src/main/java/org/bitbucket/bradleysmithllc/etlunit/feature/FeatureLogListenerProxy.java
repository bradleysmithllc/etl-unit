package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.LogListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

import java.util.List;

public class FeatureLogListenerProxy implements LogListener
{
	private final FeatureProxyBase<LogListener> base;

	public FeatureLogListenerProxy(List<Feature> featureList)
	{
		base = new FeatureProxyBase<LogListener>(featureList, new FeatureProxyBase.Getter<LogListener>()
		{
			@Override
			public LogListener get(Feature f)
			{
				return f.getLogListener();
			}
		});
	}

	@Override
	public void log(message_type type, String text, ETLTestClass etlClass, ETLTestMethod method, ETLTestOperation operation)
	{
		for (LogListener listener : base.getList())
		{
			listener.log(type, text, etlClass, method, operation);
		}
	}

	@Override
	public void log(message_type type, Throwable error, String text, ETLTestClass etlClass, ETLTestMethod method, ETLTestOperation operation)
	{
		for (LogListener listener : base.getList())
		{
			listener.log(type, error, text, etlClass, method, operation);
		}
	}
}
