package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ReferenceFileTypeClassifierImpl implements ReferenceFileTypeClassifier, JsonSerializable {
	private String description;
	private String defaultVersion;
	private String defaultVersionLower;

	private List<String> versions;
	private List<String> versionsLower;
	private final String id;

	public ReferenceFileTypeClassifierImpl(String id) {
		this.id = id;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("description").value(description);

		if (defaultVersion != null)
		{
			writer.name("default-version").value(defaultVersion);
		}

		if (versions != null) {
			writer.name("versions");
			writer.beginArray();

			for (String version : versions) {
				writer.value(version);
			}

			writer.endArray();
		}
	}

	@Override
	public void fromJson(JsonNode node) {
		description = node.get("description").asText();

		JsonNode jsonNode = node.get("default-version");
		if (jsonNode != null && !jsonNode.isNull()) {
			defaultVersion = jsonNode.asText();
			defaultVersionLower = defaultVersion.toLowerCase();
		}
		JsonNode versionsNode = node.get("versions");

		if (versionsNode != null && !versionsNode.isNull()) {
			versions = new ArrayList<String>();
			versionsLower = new ArrayList<String>();

			ArrayNode versionsArrayNode = (ArrayNode) versionsNode;

			Iterator<JsonNode> it = versionsArrayNode.iterator();

			while (it.hasNext()) {
				String version = it.next().asText();
				versions.add(version);
				versionsLower.add(version.toLowerCase());
			}
		}
	}

	@Override
	public boolean hasVersions()
	{
		return versions != null;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public List<String> getVersions() {
		List<String> ls =  Collections.emptyList();

		if (versionsLower != null)
		{
			ls = Collections.unmodifiableList(versionsLower);
		}

		return ls;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getDefaultVersion() {
		return defaultVersionLower;
	}
}
