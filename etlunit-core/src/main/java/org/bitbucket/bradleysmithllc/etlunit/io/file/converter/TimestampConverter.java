package org.bitbucket.bradleysmithllc.etlunit.io.file.converter;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.regex.Pattern;

public class TimestampConverter extends BaseRegexpConverter {
	private static final String PATTERN = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}(\\.\\d{1,6})?";

	public static final String YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final DateTimeFormatter PRINT_YYYY_MM_DD_HH_MM_SS_SSS = new DateTimeFormatterBuilder()
			.appendPattern(YYYY_MM_DD_HH_MM_SS_SSS)
			.toFormatter();

	public static final DateTimeFormatter localDatetimeFormatter =
			new DateTimeFormatterBuilder()
					.appendPattern("yyyy-MM-dd HH:mm:ss")
					.appendFraction(ChronoField.MICRO_OF_SECOND, 0, 6, true)
					.toFormatter();

	public TimestampConverter() {
		super(PATTERN);
	}

	@Override
	public String format(Object data, DataFileSchema.Column column) {
		DateTimeFormatter formatter = PRINT_YYYY_MM_DD_HH_MM_SS_SSS;

		String format = column.getFormat();

		if (format != null) {
			formatter = DateTimeFormatter.ofPattern(format);
		}

		return ((LocalDateTime) data).format(formatter);
	}

	@Override
	public Object parse(String data, DataFileSchema.Column column) throws ParseException {
		DateTimeFormatter formatter = localDatetimeFormatter;

		if (column.getFormat() != null) {
			// use only the specified format
			formatter = DateTimeFormatter.ofPattern(column.getFormat());
		}

		try {
			return LocalDateTime.parse(data, formatter);
		} catch (DateTimeParseException exc) {
			// try next method
		}

		try {
			return LocalDate.parse(data, formatter).atStartOfDay();
		} catch (DateTimeParseException exc) {
			// try next method
		}

		throw new ParseException("Bad timestamp format", 0);
	}

	@Override
	public String getId() {
		return "TIMESTAMP";
	}

	@Override
	public Pattern getPattern(DataFileSchema.Column column) {
		if (column != null) {
			String format = column.getFormat();
			// don't validate custom formats
			if (format != null) {
				return null;
			}
		}

		return super.getPattern(column);
	}
}
