package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class FileDiff
{
	public enum diff_type
	{
		added,
		removed,
		changed
	}

	private final int sourceRowNumber;
	private final int targetRowNumber;

	private final String columnName;
	private final String sourceValue;
	private final String otherValue;
	private final OrderKey orderKey;

	private final diff_type diffType;

	public FileDiff(int row, OrderKey orderKey, String value, diff_type diffType)
	{
		columnName = null;
		this.orderKey = orderKey;

		this.diffType = diffType;

		switch (diffType)
		{
			case added:
				sourceRowNumber = -1;
				targetRowNumber = row;
				otherValue = value;
				sourceValue = null;
				break;
			case removed:
				sourceRowNumber = row;
				targetRowNumber = -1;
				sourceValue = value;
				otherValue = null;
				break;
			default:
				throw new IllegalStateException("Must use added or removed");
		}
	}

	public FileDiff(int sourceRow, int targetRow, OrderKey orderKey, String columnName, String sourceValue, String otherValue)
	{
		sourceRowNumber = sourceRow;
		targetRowNumber = targetRow;
		this.columnName = columnName;
		this.sourceValue = sourceValue;
		this.otherValue = otherValue;
		this.orderKey = orderKey;

		this.diffType = diff_type.changed;
	}

	public int getSourceRowNumber()
	{
		return sourceRowNumber;
	}

	public int getTargetRowNumber()
	{
		return targetRowNumber;
	}

	public String getColumnName()
	{
		return columnName;
	}

	public String getSourceValue()
	{
		return sourceValue;
	}

	public String getOtherValue()
	{
		return otherValue;
	}

	public OrderKey getOrderKey()
	{
		return orderKey;
	}

	public diff_type getDiffType()
	{
		return diffType;
	}
}
