package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class RuntimeOptionDescriptorImpl implements RuntimeOptionDescriptor
{
	private final String name;
	private final String description;
	private final option_type optionType;
	private final boolean booleanValue;
	private final String stringValue;
	private final int intValue;

	public RuntimeOptionDescriptorImpl(String name, String description, boolean booleanValue)
	{
		this.name = name;
		this.description = description;
		this.booleanValue = booleanValue;
		this.stringValue = null;
		this.intValue = -1;

		optionType = option_type.bool;
	}

	public RuntimeOptionDescriptorImpl(String name, String description, String stringValue)
	{
		this.name = name;
		this.description = description;
		this.booleanValue = false;
		this.stringValue = stringValue;
		this.intValue = -1;

		optionType = option_type.string;
	}

	public RuntimeOptionDescriptorImpl(String name, String description, int integerValue)
	{
		this.name = name;
		this.description = description;
		this.booleanValue = false;
		this.stringValue = null;
		this.intValue = integerValue;

		optionType = option_type.integer;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public String getDescription()
	{
		return description;
	}

	@Override
	public option_type getOptionType()
	{
		return optionType;
	}

	@Override
	public String getDefaultStringValue()
	{
		check(option_type.string);

		return stringValue;
	}

	@Override
	public boolean getDefaultBooleanValue()
	{
		check(option_type.bool);

		return booleanValue;
	}

	@Override
	public int getDefaultIntegerValue()
	{
		check(option_type.integer);

		return intValue;
	}

	private void check(option_type integer)
	{
		if (optionType != integer)
		{
			throw new IllegalArgumentException("Incorrect operation.");
		}
	}
}
