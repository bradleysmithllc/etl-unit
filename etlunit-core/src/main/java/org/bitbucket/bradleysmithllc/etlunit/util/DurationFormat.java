package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.concurrent.TimeUnit;

public class DurationFormat {
	public static String formatDuration(long milis) {
		long hours = TimeUnit.MILLISECONDS.toHours(milis);
		long minutes = TimeUnit.MILLISECONDS.toMinutes(milis - TimeUnit.HOURS.toMillis(hours));
		long seconds = TimeUnit.MILLISECONDS.toSeconds(milis - TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes));
		long millis = TimeUnit.MILLISECONDS.toMillis(milis - TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes) - TimeUnit.SECONDS.toMillis(seconds));

		StringBuilder stb = new StringBuilder();

		stb.append(org.apache.commons.lang.StringUtils.leftPad(String.valueOf(seconds), 2, '0'));
		stb.append(".");
		stb.append(org.apache.commons.lang.StringUtils.leftPad(String.valueOf(millis), 3, '0'));

		if (minutes != 0 || hours != 0) {
			stb.insert(0, ":");
			stb.insert(0, org.apache.commons.lang.StringUtils.leftPad(String.valueOf(minutes), 2, '0'));
		}

		if (hours != 0) {
			stb.insert(0, ":");
			stb.insert(0, org.apache.commons.lang.StringUtils.leftPad(String.valueOf(hours), 2, '0'));
		}

		return stb.toString();
	}
}
