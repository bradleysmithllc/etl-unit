package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.listener.ClassBroadcaster;

public class ETLTestCoordinator
{
	private final ClassBroadcaster broadcaster;
	private final StatusReporter statusReporter;

	public ETLTestCoordinator(ClassBroadcaster broadcaster, StatusReporter statusReporter)
	{
		this.broadcaster = broadcaster;
		this.statusReporter = statusReporter;
	}

	public void beginTesting()
	{
		// count the number of tests to run
		ETLTestCases testCases = broadcaster.preScan(statusReporter);

		// broadcast to the listeners
		statusReporter.testsStarted(testCases.getTestCount());

		broadcaster.reset();

		try
		{
			// what to do here?
			broadcaster.broadcast(statusReporter, testCases);
		}
		finally
		{
			statusReporter.testsCompleted();
		}
	}
}
