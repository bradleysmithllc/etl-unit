package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.BaseFeatureLocator;
import org.bitbucket.bradleysmithllc.etlunit.util.NeedsTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

@NeedsTest
public class ServiceLocatorFeatureLocator extends BaseFeatureLocator
{
	private final ClassLoader loader;

	public ServiceLocatorFeatureLocator()
	{
		this(null);
	}

	public ServiceLocatorFeatureLocator(ClassLoader loader)
	{
		this.loader = loader;
	}

	@Override
	public List<Feature> getFeatures()
	{
		List<Feature> features = new ArrayList<Feature>();

		ClassLoader cl = loader == null ? Thread.currentThread().getContextClassLoader() : loader;

		ServiceLoader<Feature> sl = ServiceLoader.load(Feature.class, cl);

		Iterator<Feature> it = sl.iterator();

		while (it.hasNext())
		{
			features.add(it.next());
		}

		return features;
	}
}
