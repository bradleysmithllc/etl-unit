package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;

import java.util.ArrayList;
import java.util.List;

public class TestClassResultImpl implements TestClassResult
{
	private final String className;
	private final ETLTestClass etlTestClass;

	private final List<TestMethodResult> testMethodResults = new ArrayList<TestMethodResult>();

	private final TestResultMetrics testResults = new TestResultMetricsImpl();

	public TestClassResultImpl(ETLTestClass etlc)
	{
		this.className = etlc.getName();
		etlTestClass = etlc;
	}

	@Override
	public String testClassName()
	{
		return className;
	}

	@Override
	public ETLTestClass getTestClass()
	{
		return etlTestClass;
	}

	@Override
	public TestResultMetrics getMetrics()
	{
		return testResults;
	}

	@Override
	public List<TestMethodResult> getMethodResults()
	{
		return testMethodResults;
	}
}
