package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

import java.io.File;
import java.util.List;

public class SystemOutDiffManagerImpl implements DiffManagerImpl
{
	@Override
	public DiffGrid reportDiff(ETLTestMethod context, ETLTestOperation method, String failureId, String expectedId, String actualId)
	{
		StringBuilder builder = new StringBuilder();

		builder.append("RECORD_NUM\tCHANGE_TYPE\tCOLUMN_NAME\tSOURCE_VALUE\tTARGET_VALUE");

		String x = builder.toString();
		System.out.println(x);
		System.out
				.println(
						"-------------------------------------------------------------------------------------------------------------------------------------------------------"
								.substring(0, x.length()));

		return new DiffGrid()
		{
			@Override
			public DiffGridRow addRow(final int lineNo, final int tarLine, final line_type type)
			{
				return new DiffGridRow()
				{
					private String columnName = null;
					private String sourceValue = null;
					private String targetValue = null;
					private String orderKey = null;

					StringBuilder stb = new StringBuilder();

					@Override
					public void setColumnName(String col)
					{
						columnName = col;
					}

					@Override
					public void setOrderKey(String key)
					{
						orderKey = key;
					}

					@Override
					public void setSourceValue(String value)
					{
						sourceValue = value;
					}

					@Override
					public void setTargetValue(String value)
					{
						targetValue = value;
					}

					@Override
					public void done()
					{
						stb.setLength(0);

						stb
								.append(lineNo)
								.append('\t')
								.append(type)
								.append('\t')
								.append(type == line_type.changed
										? columnName
										: (type == line_type.added ? "TARGET_ADDED" : "SOURCE_REMOVED"))
								.append('\t')
								.append(sourceValue)
								.append('\t')
								.append(targetValue);

						System.out.println(stb.toString());
					}
				};
			}

			@Override
			public void done()
			{
				System.out.println();
			}
		};
	}

	@Override
	public DataSetGrid reportDataSet(ETLTestOperation method, List<String> columns, String failureId)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public File findDiffReport(ETLTestMethod method) {
		return null;
	}

	@Override
	public void setOutputDirectory(File file)
	{
	}

	@Override
	public void dispose()
	{
		System.out.println();
	}
}
