package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Types;

public class JdbcTypeConverter //implements DataFile.DataConverter
{
	public String format(Object data, DataFileSchema.Column column)
	{
		String columnType = column.getType();
		int type = Types.VARCHAR;

		// if the type is null, varchar is the default
		if (columnType != null)
		{
			type = getTypeValue(columnType).intValue();
		}

		// grab the corresponding type code for this id
		switch (type)
		{
			case Types.REAL:
				return String.valueOf(data);
			default:
				throw new UnsupportedOperationException("SQL data type " + columnType + " not supported");
		}
	}

	public static Integer getTypeValue(String name)
	{
		try
		{
			Field field = Types.class.getDeclaredField(name);

			Object value = field.get(null);

			if (value instanceof Integer)
			{
				return (Integer) value;
			}
			else
			{
				throw new IllegalArgumentException("Bad column type name: "
						+ name
						+ ", not found in Types.class as an integer field");
			}
		}
		catch (NoSuchFieldException e)
		{
			throw new IllegalArgumentException("Colummn type '" + name + "' not found");
		}
		catch (IllegalAccessException e)
		{
			throw new IllegalArgumentException("Colummn type '" + name + "' not accessible");
		}
	}

	public static String getTypeName(Integer code)
	{
		try
		{
			Field[] fields = Types.class.getDeclaredFields();

			for (int i = 0; i < fields.length; i++)
			{
				int modifiers = fields[i].getModifiers();
				if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers))
				{
					// grab the value and see if it is an Integer.  If so, compare
					Object val = fields[i].get(null);

					if (val instanceof Integer)
					{
						if (((Integer) val).intValue() == code)
						{
							// this is the one
							return fields[i].getName();
						}
					}
				}
			}
		}
		catch (IllegalAccessException e1)
		{
			throw new IllegalArgumentException("", e1);
		}

		throw new IllegalArgumentException("Sql type code " + code + " not found in java.sql.Types.class");
	}
}
