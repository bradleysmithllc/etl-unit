package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

class FlatFile implements DataFile
{
	private final File source;

	private DataFileSchema dataFileSchema;
	private final DataFileManager dataFileManager;

	private static final String ORDER_KEY = "\0\0ORDER_KEY\0\0";

	public static final String KEY_NULL = "\0\0\0~~~\0\0\0";
	public static final String KEY_SEPARATOR = "\1\2\3";

	public static final String KEY_PADDING;
	private static final int KEY_PADDING_LENGTH;

	static
	{
		StringBuilder stb = new StringBuilder();

		for (int i = 0; i < 1024; i++)
		{
			stb.append(" ");
		}

		KEY_PADDING = stb.toString();
		KEY_PADDING_LENGTH = KEY_PADDING.length();
	}

	public FlatFile(DataFileSchema schema, File source, DataFileManager dataFManager)
	{
		dataFileSchema = schema;
		this.source = source;
		dataFileManager = dataFManager;
	}

	public DataFileWriter writer() throws IOException
	{
		return writer(new CopyOptionsBuilder().options());
	}

	@Override
	public File getFile() {
		return source;
	}

	@Override
	public DataFileWriter writer(DataFileManager.CopyOptions options) throws IOException {
		// if there is a flat file schema, return an ordered writer unless the caller requested that we not
		return (dataFileSchema != null)
				? new OrderedDataFileWriterImpl(this, options)
				: new RelationalDataFileWriterImpl(this, options);
	}

	public static String readLine(BufferedReader bread, String rowDelimiter) throws IOException
	{
		char[] rowDelimiterChars = rowDelimiter.toCharArray();

		StringBuffer lineBuffer = new StringBuffer();

		int delimOffset = 0;

		int charsRead = 0;

		while (true)
		{
			int i = bread.read();

			if (i == -1)
			{
				if (charsRead == 0)
				{
					return null;
				}

				if (delimOffset == 0)
				{
					break;
				}
				else
				{
					throw new IllegalStateException("Line ended in the middle of the delimiter");
				}
			}

			charsRead++;

			char d = (char) i;

			if (rowDelimiterChars[delimOffset] == d)
			{
				delimOffset++;

				if (delimOffset == rowDelimiterChars.length)
				{
					break;
				}
			}
			else
			{
				if (delimOffset != 0)
				{
					// broken delimiter - encountered a portion of the delimiter but not all of it
					lineBuffer.append(rowDelimiter.substring(0, delimOffset));
					delimOffset = 0;
				}

				lineBuffer.append(d);
			}
		}

		return lineBuffer.toString();
	}

	public Map<String, Map<String, String>> getIndex()
	{
		return Collections.EMPTY_MAP;
	}

	public DataFileReader reader() throws IOException
	{
		return reader(null);
	}

	public DataFileReader reader(final List<String> columns) throws IOException
	{
		return new FileDataImpl(FlatFile.this, columns);
	}

	protected static OrderKey addOrderKey(DataFileSchema schema, Map<String, Object> plineData, List<String> columnList)
	{
		if (columnList == null)
		{
			columnList = schema.getLogicalColumnNames();
		}

		// add the new row to the rowData, with a precomputed orderKey
		OrderKey ok = new OrderKey();

		for (String col : schema.getOrderColumnNames())
		{
			// only include columns referenced in the columnList
			if (!columnList.contains(col))
			{
				continue;
			}

			Object key = plineData.get(col);

			if (key == null)
			{
				key = KEY_NULL;
			}

			DataFileSchema.Column schCol = schema.getColumn(col);

			ok.addColumn(schCol, key);
		}

		return ok;
	}

	@Override
	public TemporaryTable createTemporaryDbTable() throws SQLException {
		if (dataFileManager == null)
		{
			throw new IllegalArgumentException("No data file manager");
		}

		if (dataFileSchema == null)
		{
			throw new IllegalArgumentException("No data file schema");
		}

		// let the schema create the table
		return dataFileSchema.createTemporaryDbTable(dataFileManager.getEmbeddedDatabase());
	}

	public File getSource()
	{
		return source;
	}

	public DataFileSchema getDataFileSchema()
	{
		return dataFileSchema;
	}

	@Override
	public DataFileManager getDataFileManager() {
		return dataFileManager;
	}

	public void setFlatFileSchema(DataFileSchema flatFileSchema)
	{
		this.dataFileSchema = flatFileSchema;
	}
}
