package org.bitbucket.bradleysmithllc.etlunit.feature.workspace;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifactContent;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;

import java.io.IOException;

public class WorkspaceRuntimeSupportImpl implements WorkspaceRuntimeSupport {
	private final RuntimeSupport runtimeSupport;
	private final MetaDataContext workspaceMetaContext;

	public WorkspaceRuntimeSupportImpl(RuntimeSupport runtimeSupport, MetaDataContext workspaceMetaContext) {
		this.runtimeSupport = runtimeSupport;
		this.workspaceMetaContext = workspaceMetaContext;
	}

	@Override
	public Workspace currentWorkspace() {
		Workspace workspace = currentWorkspaceImpl();

		MetaDataPackageContext pmdpc = workspaceMetaContext.createPackageContextForCurrentTest(MetaDataPackageContext.path_type.feature_source);

		try {
			MetaDataArtifact art = pmdpc.createArtifact(workspace.workspaceRoot().getName(), workspace.workspaceRoot());
			MetaDataArtifactContent content = art.createContent(workspace.root().getName());
			content.referencedByCurrentTest("workspace");
		} catch (IOException e) {
			e.printStackTrace();
		}

		return workspace;
	}

	public Workspace currentWorkspaceImpl() {
		Workspace workspace = runtimeSupport.retrieveFromExecutor("workspace", Workspace.class);

		return workspace;
	}
}
