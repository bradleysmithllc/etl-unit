package org.bitbucket.bradleysmithllc.etlunit.util.cli;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang.StringUtils;

public class Enumerations {
	public static String generateEnumerated(int value, int length) {
		if (length <= 0) {
			throw new IllegalArgumentException("Length must be a positive integer");
		}

		String hexdec = Integer.toString(value, Character.MAX_RADIX);

		if (hexdec.length() <= length) {
			return StringUtils.leftPad(hexdec, length, '0');
		}

		return StringUtils.reverse(hexdec).substring(0, length);
	}

	public static char generateEnumerated(int value) {
		switch (value % 36) {
			case 15:
				return 'a';
			case 14:
				return 'b';
			case 13:
				return 'c';
			case 12:
				return 'd';
			case 11:
				return 'e';
			case 10:
				return 'f';
			case 9:
				return 'g';
			case 8:
				return 'h';
			case 7:
				return 'i';
			case 6:
				return 'j';
			case 5:
				return 'k';
			case 4:
				return 'l';
			case 3:
				return 'm';
			case 2:
				return 'n';
			case 1:
				return 'o';
			case 0:
				return 'p';
			case 16:
				return 'q';
			case 17:
				return 'r';
			case 18:
				return 's';
			case 19:
				return 't';
			case 20:
				return 'u';
			case 21:
				return 'v';
			case 22:
				return 'w';
			case 23:
				return 'x';
			case 24:
				return 'y';
			case 25:
				return 'z';
			case 26:
				return '0';
			case 27:
				return '1';
			case 28:
				return '2';
			case 29:
				return '3';
			case 30:
				return '4';
			case 31:
				return '5';
			case 32:
				return '6';
			case 33:
				return '7';
			case 34:
				return '8';
			case 35:
				return '9';
			default:
				throw new IllegalStateException();
		}
	}
}
