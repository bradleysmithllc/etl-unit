package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ClassLocator;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;

import java.util.List;

public class FeatureClassLocatorProxy implements ClassLocator
{
	private int currentFeature = 0;
	private final FeatureProxyBase<ClassLocator> base;

	public FeatureClassLocatorProxy(List<Feature> features)
	{
		base = new FeatureProxyBase<ClassLocator>(features, new FeatureProxyBase.Getter<ClassLocator>()
		{
			@Override
			public ClassLocator get(Feature f)
			{
				return f.getLocator();
			}
		});
	}

	@Override
	public void reset()
	{
		currentFeature = 0;

		for (ClassLocator locator : base.getList())
		{
			locator.reset();
		}
	}

	@Override
	public boolean hasNext()
	{
		while (currentFeature < base.getList().size())
		{
			ClassLocator locator = base.getList().get(currentFeature);

			if (locator.hasNext())
			{
				return true;
			}

			currentFeature++;
		}

		return false;
	}

	@Override
	public ETLTestClass next()
	{
		return base.getList().get(currentFeature).next();
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException();
	}
}
