package org.bitbucket.bradleysmithllc.etlunit.metadata.impl;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.metadata.*;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

public class MetaDataImpl implements MetaData
{
	private RuntimeSupport runtimeSupport;

	private final List<MetaDataContext> contexts = new ArrayList<MetaDataContext>();
	private final Map<String, MetaDataContext> contextMap = new HashMap<String, MetaDataContext>();

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
	}

	@Override
	public synchronized MetaDataContext getOrCreateContext(String name) {
		if (contextMap.containsKey(name))
		{
			return contextMap.get(name);
		}

		MetaDataContextImpl mimpl = new MetaDataContextImpl(name);
		mimpl.setMetaData(this);
		mimpl.receiveRuntimeSupport(runtimeSupport);

		contexts.add(mimpl);
		contextMap.put(name, mimpl);

		return mimpl;
	}

	@Override
	public Map<String, MetaDataContext> getContextMap() {
		return Collections.unmodifiableMap(contextMap);
	}

	@Override
	public List<MetaDataArtifactContentReference> getTestArtifacts(TestReference ref) {
		final List<MetaDataArtifactContentReference> artifacts = new ArrayList<MetaDataArtifactContentReference>();

		// visit all child nodes and capture a reference to each artifact content that this test references
		for (MetaDataContext context : contexts)
		{
			for (MetaDataPackageContext packageContext : context.getPackageContexts())
			{
				for (MetaDataArtifact artifact : packageContext.getArtifacts())
				{
					for (MetaDataArtifactContent content : artifact.getContents())
					{
						for (TestReference reference : content.getReferences())
						{
							if (reference.compareTo(ref) == 0)
							{
								//if (!artifacts.contains(content))
								//{
									artifacts.add(new MetaDataArtifactContentReference(content, reference));
								//}

								break;
							}
						}
					}
				}
			}
		}

		return artifacts;
	}

	@Override
	public List<MetaDataArtifactContentReference> getTestArtifacts(ETLTestMethod method) {
		ETLTestClass testClass = method.getTestClass();

		TestReference tr = new TestReferenceImpl(null, testClass.getPackage(), testClass.getName(), method.getName(), method.getQualifiedName());

		return getTestArtifacts(tr);
	}

	@Override
	public List<MetaDataContext> getContexts() {
		return Collections.unmodifiableList(contexts);
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("meta-data");

		writer.beginArray();

		for (MetaDataContext ctx : contexts)
		{
			writer.beginObject();
			ctx.toJson(writer);
			writer.endObject();
		}

		writer.endArray();
	}

	@Override
	public synchronized void fromJson(JsonNode node) {
		contextMap.clear();
		contexts.clear();

		ArrayNode anode = (ArrayNode) node.get("meta-data");

		Iterator<JsonNode> it = anode.elements();

		while (it.hasNext())
		{
			MetaDataContextImpl mimpl = new MetaDataContextImpl();
			mimpl.receiveRuntimeSupport(runtimeSupport);

			mimpl.fromJson(it.next());

			contexts.add(mimpl);
			contextMap.put(mimpl.getContextName(), mimpl);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MetaDataImpl metaData = (MetaDataImpl) o;

		if (!contexts.equals(metaData.contexts)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return contexts.hashCode();
	}
}
