package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.bitbucket.bradleysmithllc.etlunit.DiffManager;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonProcessingUtil;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonSchemaLoader;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonSchemaResolver;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.bitbucket.bradleysmithllc.json.validator.ResourceNotFoundException;
import org.h2.Driver;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;

public class DataFileManagerImpl implements DataFileManager {
	private String defaultColumnDelimiter = DEFAULT_COLUMN_DELIMITER;
	private String defaultRowDelimiter = DEFAULT_ROW_DELIMITER;
	private String defaultNullToken = DEFAULT_NULL_TOKEN;
	private DataFileSchema.format_type defaultFormatType = DEFAULT_FORMAT_TYPE;
	private final File tmpDbRoot;
	private Connection embeddedDatabase;

	private final FlatFileTypes flatFileTypes = new FlatFileTypes();

	public DataFileManagerImpl(File tmpRoot) {
		tmpDbRoot = tmpRoot;
	}

	private static final JsonSchema flatFileSchema = JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/io/file/ffml/ffml.jsonSchema");

	@Override
	public DataFileSchema.format_type getDefaultFormatType() {
		return defaultFormatType;
	}

	@Override
	public void setDefaultFormatType(DataFileSchema.format_type defaultFormatType) {
		this.defaultFormatType = defaultFormatType;
	}

	@Override
	public String getDefaultColumnDelimiter() {
		return defaultColumnDelimiter;
	}

	@Override
	public void dispose() {
		if (embeddedDatabase != null) {
			try {
				embeddedDatabase.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Connection getEmbeddedDatabase() {
		return initDbIfNeeded();
	}

	private synchronized Connection initDbIfNeeded() {
		if (embeddedDatabase == null) {
			Driver.load();

			try {
				embeddedDatabase = DriverManager.getConnection("jdbc:h2:" + new FileBuilder(tmpDbRoot.getAbsolutePath()).name("embedded").file());
			} catch (SQLException e) {
				throw new IllegalArgumentException(e);
			}
		}

		return embeddedDatabase;
	}

	@Override
	public void setDefaultColumnDelimiter(String defaultColumnDelimiter) {
		this.defaultColumnDelimiter = defaultColumnDelimiter;
	}

	@Override
	public String getDefaultRowDelimiter() {
		return defaultRowDelimiter;
	}

	@Override
	public void setDefaultRowDelimiter(String defaultRowDelimiter) {
		this.defaultRowDelimiter = defaultRowDelimiter;
	}

	@Override
	public String getDefaultNullToken() {
		return defaultNullToken;
	}

	@Override
	public DataFileSchema loadDataFileSchemaFromResource(String resourcePath, String resourceId) {
		return loadDataFileSchemaFromResource(resourcePath, resourceId, this);
	}

	@Override
	public DataFileSchema loadDataFileSchemaFromResource(String resourcePath, String resourceId, Object classLoaderContext) {
		return loadFromResource(resourcePath, resourceId, classLoaderContext != null ? classLoaderContext.getClass().getClassLoader() : null, this);
	}

	@Override
	public DataFileSchema createDataFileSchema(String id) {
		return defaultFormatType == DataFileSchema.format_type.delimited ?
				new DataFileSchemaImpl(id, defaultFormatType, defaultRowDelimiter, defaultColumnDelimiter, defaultNullToken, this)
				:
				new DataFileSchemaImpl(id, defaultFormatType, defaultRowDelimiter, null, defaultNullToken, this)
				;
	}

	@Override
	public void setDefaultNullToken(String defaultNullToken) {
		this.defaultNullToken = defaultNullToken;
	}

	@Override
	public DataFileSchema loadDataFileSchema(File schema, String schemaId) {
		return loadFromFile(schema, schemaId, this);
	}

	@Override
	public void saveDataFileSchema(DataFileSchema schema, File output) throws IOException {
		FileUtils.write(output, schema.toJsonString());
	}

	@Override
	public DataFile loadDataFile(File file, DataFileSchema schema) {
		return new FlatFile(schema, file, this);
	}

	@Override
	public List<FileDiff> diff(DataFile lfile, DataFile rfile) throws IOException {
		return diff(lfile, rfile, null);
	}

	@Override
	public List<FileDiff> diff(DataFile lfile, DataFile rfile, List<String> columns) throws IOException {
		FlatFileDiff fdiff = new FlatFileDiff(lfile);

		return fdiff.diffFile(rfile, columns);
	}

	@Override
	public void report(DiffManager dManager, ETLTestMethod context, ETLTestOperation operation, String failureId, List<FileDiff> diffs, String expectedId, String actualId) {
		FlatFileDiff.report(dManager, context, operation, failureId, diffs, expectedId, actualId);
	}

	@Override
	public DataConverter resolveValidatorById(String id) {
		return flatFileTypes.getById(id);
	}

	@Override
	public void installConverter(DataConverter converter) {
		flatFileTypes.installConverter(converter);
	}

	@Override
	public void copyDataFile(DataFile source, DataFile dest, CopyOptions options) throws IOException {
		// in the case of an override extract sql, we must write the source to the temporary
		// table, then read to an override schema.

		String extractOverride = options.overrideExtractionSql();

		if (extractOverride != null) {
			// create a temporary table, and write the entire source into it.
			try {
				TemporaryTable tempTable = source.createTemporaryDbTable();

				// copy entire source into the database table
				copyFileDataToWriter(source.reader(), tempTable.getWriter());

				// now extract it back out using the override sql and the dest schema
				copyFileDataToWriter(tempTable.getData(extractOverride), dest.writer());
			} catch (SQLException e) {
				throw new IOException("Error writing source file to temporary table", e);
			}
		} else {
			DataFileReader read = source.reader();

			DataFileWriter writer = dest.writer(options);

			copyFileDataToWriter(read, writer);
		}
	}

	@Override
	public void copyFileDataToWriter(DataFileReader fd, DataFileWriter writer) throws IOException {
		try {
			Iterator<DataFileReader.FileRow> it = fd.iterator();

			try {
				while (it.hasNext()) {
					Map<String, Object> rowData = it.next().getData();
					writer.addRow(rowData);
				}
			} finally {
				writer.close();
			}
		} finally {
			fd.dispose();
		}
	}

	@Override
	public void copyDataFile(DataFile source, DataFile dest) throws IOException {
		copyDataFile(source, dest, DEFAULT_COPY_OPTIONS);
	}

	private static final CopyOptions DEFAULT_COPY_OPTIONS = new CopyOptionsBuilder().options();

	public static DataFileSchema loadFromFile(File schemaPath, String resourceId, DataFileManager dataFileManager) {
		try {
			return loadFromNode(JsonLoader.fromFile(schemaPath), resourceId, dataFileManager, (path) -> {
				JsonNode node = findFileResource(path, schemaPath);

				if (node != null) {
					return node;
				}

				return findClasspathResource(path, "", null);
			});
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private static JsonNode findFileResource(String path, File schemaPath) {
		File file = new File(schemaPath.getParentFile(), path + ".fml");

		if (file.exists()) {
			try {
				return JsonLoader.fromFile(file);
			} catch (IOException e) {
				throw new IllegalStateException(e);
			}
		}

		return null;
	}

	private static JsonNode findClasspathResource(String path, String basePath, ClassLoader loader1) {
		try {
			path = path + ".fml";

			// first try to split the last path element off and replace with this one
			String newPath = replaceLastPathElement(basePath, path);

			// try both paths with all three classloaders
			for (String searchPath : Arrays.asList(newPath, path)) {
				for (ClassLoader loader : Arrays.asList(Thread.currentThread().getContextClassLoader(), loader1, ClassLoader.getPlatformClassLoader(), ClassLoader.getSystemClassLoader()))
				{
					if (loader != null) {
						//LoggerFactory.getLogger(DataFileManagerImpl.class).info("Searching for classpath entry '{}' in class loader {}", searchPath, loader.getClass().getSimpleName());

						URL url = loader.getResource(searchPath);

						if (url != null) {
							//LoggerFactory.getLogger(DataFileManagerImpl.class).info("Classpath entry '{}' found", searchPath);
							return JsonLoader.fromURL(url);
						} else {
							//LoggerFactory.getLogger(DataFileManagerImpl.class).info("Classpath entry '{}' not found", searchPath);
						}
					}
				}
			}

			// just try to find the absolute path
			return JsonSchemaResolver.resolveSchemaReference(path);
		} catch (Exception exc) {
			throw new IllegalStateException(exc);
		}
	}

	public static String replaceLastPathElement(String path, String replace) {
		// first try to split the last path element off and replace with this one
		if (path.startsWith("/")) {
			path = path.substring(1);
		}

		int index = path.lastIndexOf("/");

		if (index != -1) {
			if (!replace.startsWith("/")) {
				replace = "/" + replace;
			}

			return path.substring(0, index) + replace;
		}

		if (replace.startsWith("/")) {
			replace = replace.substring(1);
		}

		return replace;
	}

	private static JsonNode loadSchemaNode(JsonNode schemaNode, Function<String, JsonNode> jsonLoader) {
		try {
			schemaNode = JsonSchemaResolver.resolveSchemaReference(schemaNode);

			if (!schemaNode.at("/flat-file/$inherit-from").isMissingNode() || !schemaNode.at("/flat-file/$inherit").isMissingNode()) {
				String inheritPath = null;
				DataFileSchemaDef.inherit_columns columnInherit = DataFileSchemaDef.inherit_columns.all;

				if (!schemaNode.at("/flat-file/$inherit/from").isMissingNode()) {
					inheritPath = schemaNode.at("/flat-file/$inherit/from").asText();

					if (!schemaNode.at("/flat-file/$inherit/columns").isMissingNode()) {
						columnInherit = DataFileSchemaDef.inherit_columns.valueOf(schemaNode.at("/flat-file/$inherit/columns").asText());
					}
				} else if (!schemaNode.at("/flat-file/$inherit-from").isMissingNode()) {
					inheritPath = schemaNode.at("/flat-file/$inherit-from").asText();
				}

				JsonNode parentDef = jsonLoader.apply(inheritPath);

				// load recursively
				JsonNode parent = loadSchemaNode(parentDef, jsonLoader);

				// before merge, remove and retain column nodes
				ArrayNode pcolumns = null;
				ArrayNode ccolumns = null;

				if (!parent.at("/flat-file/columns").isMissingNode()) {
					pcolumns = (ArrayNode) parent.at("/flat-file/columns");

					((ObjectNode) parent.get("flat-file")).remove("columns");
				}

				if (!schemaNode.at("/flat-file/columns").isMissingNode()) {
					ccolumns = (ArrayNode) schemaNode.at("/flat-file/columns");

					((ObjectNode) schemaNode.get("flat-file")).remove("columns");
				}


				// merge child into parent
				JsonNode newSchemaNode = JsonUtils.merge(schemaNode, parent, JsonUtils.merge_type.left_merge);

				// Resolve the columns
				if ((pcolumns == null || pcolumns.size() == 0) && (ccolumns == null || ccolumns.size() == 0)) {
					// this is invalid. Let the schema report
				} else if ((pcolumns == null || pcolumns.size() == 0) || (ccolumns == null || ccolumns.size() == 0)) {
					// just merge the one that is not null.
					if (pcolumns != null && pcolumns.size() != 0) {
						// only do this if column == all.  This will result in an error, no columns in the result.
						if (columnInherit == DataFileSchemaDef.inherit_columns.all) {
							((ObjectNode) newSchemaNode.get("flat-file")).put("columns", pcolumns);
						}
					} else if (ccolumns != null && ccolumns.size() != 0) {
						((ObjectNode) newSchemaNode.get("flat-file")).put("columns", ccolumns);
					}
				} else {
					// require merge
					// create a map of all named columns
					Map<String, MutablePair<JsonNode, JsonNode>> columns = new HashMap<>();
					List<String> columnOrder = new ArrayList<>();

					// parent columns first
					for (JsonNode anode : pcolumns) {
						String id = anode.get("id").asText().toLowerCase();
						columns.put(id, new MutablePair<>(anode, null));

						columnOrder.add(id);
					}

					// child columns last
					for (JsonNode anode : ccolumns) {
						String id = anode.get("id").asText().toLowerCase();
						if (columns.containsKey(id)) {
							columns.get(id).setRight(anode);
						} else {
							columns.put(id, new MutablePair<>(null, anode));
							columnOrder.add(id);
						}
					}

					// merge
					ArrayNode anodeanode = JsonNodeFactory.instance.arrayNode();

					for (String columnName : columnOrder) {
						MutablePair<JsonNode, JsonNode> columnDefs = columns.get(columnName);

						if (columnDefs.getLeft() == null) {
							anodeanode.add(columnDefs.getRight());
						} else if (columnDefs.getRight() == null) {
							// only provide parent-only columns if the mode == all
							if (columnInherit == DataFileSchemaDef.inherit_columns.all) {
								anodeanode.add(columnDefs.getLeft());
							}
						} else {
							anodeanode.add(JsonUtils.merge(columnDefs.getLeft(), columnDefs.getRight(), JsonUtils.merge_type.right_merge));
						}
					}

					( (ObjectNode) newSchemaNode.get("flat-file")).put("columns", anodeanode);
				}

				// determine if the primaryKey needs to be addressed
				// child does not declare - it is inherited and might need to be adjusted
				if (schemaNode.at("/flat-file/primaryKey").isMissingNode() && !newSchemaNode.at("/flat-file/primaryKey").isMissingNode()) {
					Set<String> names = new HashSet<>();

					JsonNode at = newSchemaNode.at("/flat-file/columns");
					for (JsonNode column : ((ArrayNode) at)) {
						names.add(column.get("id").asText().toLowerCase());
					}

					ArrayNode an = (ArrayNode) newSchemaNode.at("/flat-file/primaryKey");

					Iterator<JsonNode> it = an.iterator();

					while (it.hasNext()) {
						String keyName = it.next().asText().toLowerCase();

						if (!names.contains(keyName)) {
							it.remove();
						}
					}

					// check if there are none left
					if (an.size() == 0) {
						// remove primary key node
						( (ObjectNode) newSchemaNode.get("flat-file")).remove("primaryKey");
					}
				}

				// determine if the orderby needs to be addressed
				// child does not declare - it is inherited and might need to be adjusted
				if (schemaNode.at("/flat-file/orderBy").isMissingNode() && !newSchemaNode.at("/flat-file/orderBy").isMissingNode()) {
					Set<String> names = new HashSet<>();

					JsonNode at = newSchemaNode.at("/flat-file/columns");
					for (JsonNode column : ((ArrayNode) at)) {
						names.add(column.get("id").asText().toLowerCase());
					}

					ArrayNode an = (ArrayNode) newSchemaNode.at("/flat-file/orderBy");

					Iterator<JsonNode> it = an.iterator();

					while (it.hasNext()) {
						String keyName = it.next().asText().toLowerCase();

						if (!names.contains(keyName)) {
							it.remove();
						}
					}

					// check if there are none left
					if (an.size() == 0) {
						// remove primary key node
						( (ObjectNode) newSchemaNode.get("flat-file")).remove("orderBy");
					}
				}

				return newSchemaNode;
			}

			return schemaNode;
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	private static JsonNode loadSchemaDef(JsonNode schemaNode, DataFileManager dataFileManager, Function<String, JsonNode> jsonLoader) {
		try {
			schemaNode = loadSchemaNode(schemaNode, jsonLoader);

			return schemaNode;
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	private static DataFileSchema loadFromNode(JsonNode schemaNode, String resourceId, DataFileManager dataFileManager, Function<String, JsonNode> jsonLoader) throws IOException {
			// load the def object
		JsonNode schemaNodeDef = loadSchemaNode(schemaNode, jsonLoader);
			// preload the instance so it can be resolved before passing on

		return validate(schemaNodeDef, resourceId, dataFileManager);
	}

	public static DataFileSchema loadFromResource(String resourcePath, String resId, ClassLoader loader, DataFileManager dataFileManager) {
		try {
			return loadFromNode(JsonLoader.fromString(ResourceUtils.loadResourceAsString(loader, resourcePath)), resId, dataFileManager, (path) -> {
				return findClasspathResource(path, resourcePath, loader);
			});
		} catch (Exception e) {
			throw new ResourceNotFoundException(e);
		}
	}

	private static DataFileSchema validate(JsonNode node, String resId, DataFileManager dataFileManager) {
		try {
			ProcessingReport pr = flatFileSchema.validate(node);

			if (!pr.isSuccess()) {
				throw new IllegalArgumentException(JsonProcessingUtil.getProcessingReport(pr));
			}

			DataFileSchemaDef def2 = new DataFileSchemaDef(node, dataFileManager);
			def2.processDefaults();

			return new DataFileSchemaImpl(def2, resId, dataFileManager);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}
}
