package org.bitbucket.bradleysmithllc.etlunit.feature.results;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.util.DurationFormat;
import org.bitbucket.bradleysmithllc.etlunit.util.MessageLine;

import java.util.HashMap;
import java.util.Map;

@FeatureModule
public class ResultsFeatureModule extends AbstractFeature
{
	private final Map<String, TestClassResultImpl> classResultsMap = new HashMap<String, TestClassResultImpl>();
	private final Map<String, TestMethodResultImpl> methodResultsMap = new HashMap<String, TestMethodResultImpl>();

	private TestResults testResults = null;

	private final StatusReporter reporter = new ResultsReporter();

	private long startTime;

	private final class ResultsReporter extends NullStatusReporterImpl
	{
		@Override
		public synchronized void testsStarted(int numTestsSelected)
		{
			testResults = new TestResultsImpl(numTestsSelected);

			classResultsMap.clear();
			methodResultsMap.clear();
			testResults.reset();

			startTime = System.currentTimeMillis();

			userLog.info("Processing [" + numTestsSelected + "] tests");
		}

		@Override
		public void testsCompleted()
		{
			long runTime = System.currentTimeMillis() - startTime;
			TestResults results = getTestClassResults();
			TestResultMetrics metrics = results.getMetrics();

			StringBuilder messageB = new StringBuilder("Tests run: "
					+ results.getNumTestsSelected());

			appendIfNotZero(metrics.getNumberOfTestsPassed(), ", Successes: " + metrics.getNumberOfTestsPassed(), messageB);
			appendIfNotZero(metrics.getNumberOfAssertionFailures(), ", Failures: " + metrics.getNumberOfAssertionFailures(), messageB);
			appendIfNotZero(metrics.getNumberOfErrors(), ", Errors: " + metrics.getNumberOfErrors(), messageB);
			appendIfNotZero(metrics.getNumberOfWarnings(), ", Warnings: " + metrics.getNumberOfWarnings(), messageB);
			appendIfNotZero(metrics.getNumberIgnored(), ", Ignored: " + metrics.getNumberIgnored(), messageB);

			messageB.append(", Time elapsed: ").append(DurationFormat.formatDuration(runTime))
			.append(" sec");

			userLog.info(MessageLine.formatStandardMessage(""));
			userLog.info(MessageLine.formatStandardMessage(messageB.toString()));
		}

		private void appendIfNotZero(int numberOfTestsPassed, String s, StringBuilder messageB) {
			if (numberOfTestsPassed != 0)
			{
				messageB.append(s);
			}
		}

		@Override
		public synchronized void testCompleted(ETLTestMethod method, CompletionStatus status)
		{
			// update stats on the class
			ETLTestClass testClass = method.getTestClass();
			TestClassResultImpl clsrep = classResultsMap.get(testClass.getName());

			if (clsrep == null)
			{
				clsrep = new TestClassResultImpl(testClass);
				classResultsMap.put(testClass.getName(), clsrep);
				testResults.getResultsByClass().add(clsrep);
			}

			String qualifiedName = testClass.getName() + "." + method.getName();

			TestMethodResultImpl mthdrep = methodResultsMap.get(qualifiedName);

			if (mthdrep == null)
			{
				mthdrep = new TestMethodResultImpl(testClass.getName(), method.getName());
				methodResultsMap.put(qualifiedName, mthdrep);
				clsrep.getMethodResults().add(mthdrep);
			}

			// add metrics for all levels
			TestResultMetrics clsTestResults = clsrep.getMetrics();
			TestResultMetrics mthdTestResults = mthdrep.getMetrics();
			TestResultMetrics resultMetrics = testResults.getMetrics();

			clsTestResults.addStatus(method, status);
			mthdTestResults.addStatus(method, status);
			resultMetrics.addStatus(method, status);

			clsTestResults.addTestWarnings(status.getWarnings().size());
			mthdTestResults.addTestWarnings(status.getWarnings().size());
			resultMetrics.addTestWarnings(status.getWarnings().size());

			clsTestResults.addTestsIgnored(status.getTestIgnoreReasons().size());
			mthdTestResults.addTestsIgnored(status.getTestIgnoreReasons().size());
			resultMetrics.addTestsIgnored(status.getTestIgnoreReasons().size());

			switch (status.getTestResult())
			{
				case warning:
				case ignored:
				case success:
					clsTestResults.addTestsPassed(1);
					mthdTestResults.addTestsPassed(1);
					resultMetrics.addTestsPassed(1);
					break;
				case failure:
					clsTestResults.addAssertionFailures(status.getAssertionFailures().size());
					mthdTestResults.addAssertionFailures(status.getAssertionFailures().size());
					resultMetrics.addAssertionFailures(status.getAssertionFailures().size());
					break;
				case error:
					clsTestResults.addErrors(1);
					mthdTestResults.addErrors(1);
					resultMetrics.addErrors(1);
					break;
			}
		}
	}

	@Override
	public StatusReporter getStatusReporter()
	{
		return reporter;
	}

	public TestResults getTestClassResults()
	{
		return testResults;
	}

	public String getFeatureName()
	{
		return "results";
	}

	@Override
	public boolean requiredForSimulation() {
		return true;
	}
}
