package org.bitbucket.bradleysmithllc.etlunit.listener;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

public class BroadcasterCoordinatorImpl implements BroadcasterCoordinator {
	public static final int PERMITS = 100;
	private final ETLTestCases testCases;
	private List<ETLTestCases.ExecutorTestCases> thisExecutorTestCases;
	private final MapLocal mapLocal;
	private final StatusReporter statusReporter;
	private final Log applicationLog;

	private final LinkedList<ETLTestMethod> methodQueue = new LinkedList<ETLTestMethod>();
	private final CountDownLatch startMutex = new CountDownLatch(1);
	private final Semaphore waitingSemaphore = new Semaphore(PERMITS);

	private final CountDownLatch stopMutex = new CountDownLatch(1);

	private final Map<BroadcasterExecutor, Boolean> activeExecutors = Collections.synchronizedMap(new HashMap<BroadcasterExecutor, Boolean>());
	private final Map<BroadcasterExecutor, ClassStatus> classStatusMap = Collections.synchronizedMap(new HashMap<BroadcasterExecutor, ClassStatus>());
	private final Map<BroadcasterExecutor, ETLTestMethod> lastClassMap = Collections.synchronizedMap(new HashMap<BroadcasterExecutor, ETLTestMethod>());

	private static final class ClassStatus {
		private final Map<ETLTestClass, StatusReporter.CompletionStatus> completionStatusMap = new HashMap<ETLTestClass, StatusReporter.CompletionStatus>();
	}

	/**
	 * Use this map to track whether each executor has been initialized
	 */
	private final Map<BroadcasterExecutor, Boolean> beginMap = Collections.synchronizedMap(new HashMap<BroadcasterExecutor, Boolean>());

	public BroadcasterCoordinatorImpl(
			ETLTestCases testCases,
			MapLocal mapLocal,
			StatusReporter statusReporter,
			Log applicationLog
	) {
		this.statusReporter = statusReporter == null ? new NullStatusReporterImpl() : statusReporter;
		this.applicationLog = applicationLog;
		this.mapLocal = mapLocal;

		this.testCases = testCases;

		thisExecutorTestCases = testCases.getEtlTestMethods();

		// start off with all permits taken
		waitingSemaphore.acquireUninterruptibly(PERMITS);
	}

	@Override
	public boolean ready(final BroadcasterExecutor executor) {
		// fail if the stop mutex has already been shut
		if (stopMutex.getCount() == 0) {
			throw new IllegalStateException("BroadcasterCoordinator has shut down");
		}

		// check in before anything else - this will record our presence in line
		waitingSemaphore.release();

		// record that I have been here
		activeExecutors.put(executor, Boolean.TRUE);

		try {
			try {
				startMutex.await();
			} catch (InterruptedException e) {
				throw new IllegalStateException(e);
			}

			// check for necessity to 'begin'
			if (!beginMap.containsKey(executor)) {
				try {
					callFailSafe(new FailSafeCallback() {
						@Override
						public Object call() {
							try
							{
								executor.beginExecution();
							}
							catch(TestExecutionError tee)
							{
								throw new RuntimeException(tee);
							}
							return null;
						}
					});
				} catch (Throwable err) {
					applicationLog.severe("Error in beginExecution", err);
				}

				beginMap.put(executor, Boolean.TRUE);
			}

			final AtomicBoolean atb = new AtomicBoolean(true);

			callFailSafe(new FailSafeCallback() {
				@Override
				public Object call() {
					atb.set(ready1(executor));

					if (!atb.get()) {
						// end the executor run
						activeExecutors.remove(executor);

						if (activeExecutors.size() == 0) {
							stopMutex.countDown();
						}
					}

					return null;
				}
			});

			return atb.get();
		} finally {
			waitingSemaphore.acquireUninterruptibly();
		}
	}

	private boolean ready1(final BroadcasterExecutor executor) {
		/*	Remember, at this point we are using a 'borrowed' thread from the executor.  So,
				synchronization is required, but not thread isolation (protecting against hangs, etc).  If
				this thread blocks, it only affects the executor unless it happens in coordination.
		 */

		/* TODO - If a package or class begin fails, each test method must fail with the same status */
		// establish the current exexctor
		mapLocal.setExecutorId(executor.getId());

		// get the last test executed so we know what part of the contract to implement next
		ETLTestMethod lastMethod = lastClassMap.get(executor);

		ETLTestPackage lastMethodPackage = lastMethod != null ? lastMethod.getTestClass().getPackage() : null;

		// coordination required here.  Check next test status and grab the next method for this class
		ETLTestMethod nextMethod = nextRunnableMethod();

		// record in the mapLocal
		if (nextMethod != null) {
			mapLocal.setCurrentlyProcessingMethod(nextMethod);
			mapLocal.setCurrentlyProcessingTestClass(nextMethod.getTestClass());
		}

		if (nextMethod == null) {
			// nothing more to do - check for exits

			try {
				if (lastMethod != null) {
					// leave the class
					StatusReporter.CompletionStatus classStatuses = getClassStatus(executor, lastMethod.getTestClass());

					try {
						executor.leaveClass(lastMethod.getTestClass(), classStatuses);
						executor.leavePackage(lastMethod.getTestClass().getPackage(), classStatuses);
					} catch (TestExecutionError err) {
						classStatuses.addError(err);
					} catch (TestAssertionFailure err) {
						classStatuses.addFailure(err);
					} catch (TestWarning warning) {
						classStatuses.addWarning(warning);
					} catch (Throwable thr) {
						applicationLog.severe("Error in endExecution", thr);
						classStatuses.addError(new TestExecutionError("Error in endExecution", thr));
					}
				}
			} finally {
				callFailSafe(new FailSafeCallback() {
					@Override
					public Object call() {
						executor.endExecution();
						return null;
					}
				});

				return false;
			}
		}

		// check to see if there is a change in package or class
		ETLTestClass testClass = nextMethod.getTestClass();
		ETLTestPackage nextMethodPackage = testClass.getPackage();

		if (lastMethod == null) {
			// both - package and class must be broadcast
			StatusReporter.CompletionStatus classStatuses = getClassStatus(executor, testClass);

			try {
				executor.enterPackage(nextMethodPackage, classStatuses);
				executor.enterClass(testClass, classStatuses);
			} catch (TestExecutionError err) {
				classStatuses.addError(err);
			} catch (TestAssertionFailure err) {
				classStatuses.addFailure(err);
			} catch (TestWarning warning) {
				classStatuses.addWarning(warning);
			} catch (Throwable thr) {
				applicationLog.severe("Error in enterPackage or enterClass", thr);
				classStatuses.addError(new TestExecutionError("Error in enterPackage or enterClass", thr));
			}
		} else {
			if (lastMethodPackage.compareTo(nextMethodPackage) != 0) {
				// packages don't match - broadcast new package
				StatusReporter.CompletionStatus classStatuses = new StatusReporter.CompletionStatus();

				try {
					executor.leavePackage(lastMethodPackage, classStatuses);
				} catch (TestExecutionError err) {
					classStatuses.addError(err);
				} catch (TestAssertionFailure err) {
					classStatuses.addFailure(err);
				} catch (TestWarning warning) {
					classStatuses.addWarning(warning);
				} catch (Throwable thr) {
					applicationLog.severe("Error in leavePackage", thr);
					classStatuses.addError(new TestExecutionError("Error in leavePackage", thr));
				}

				classStatuses = new StatusReporter.CompletionStatus();
				try {
					executor.enterPackage(nextMethodPackage, classStatuses);
				} catch (TestExecutionError err) {
					classStatuses.addError(err);
				} catch (TestAssertionFailure err) {
					classStatuses.addFailure(err);
				} catch (TestWarning warning) {
					classStatuses.addWarning(warning);
				} catch (Throwable thr) {
					applicationLog.severe("Error in enterPackage", thr);
					classStatuses.addError(new TestExecutionError("Error in enterPackage", thr));
				}
			}

			// no else-if here - this condition is not exclusive of the change in package - in fact it will happen all the time
			if (ObjectUtils.compare(lastMethod.getTestClass(), testClass) != 0) {
				// new class
				// leave old
				StatusReporter.CompletionStatus classStatuses = getClassStatus(executor, lastMethod.getTestClass());

				try {
					executor.leaveClass(lastMethod.getTestClass(), classStatuses);
				} catch (TestExecutionError err) {
					classStatuses.addError(err);
				} catch (TestAssertionFailure err) {
					classStatuses.addFailure(err);
				} catch (TestWarning testWarning) {
					classStatuses.addWarning(testWarning);
				} catch (Throwable thr) {
					applicationLog.severe("Error in leaveClass", thr);
					classStatuses.addError(new TestExecutionError("Error in leaveClass", thr));
				}

				// enter new
				classStatuses = getClassStatus(executor, testClass);

				try {
					// whenever we enter a new class, if the before class fails the methods should all fail
					executor.enterClass(testClass, classStatuses);
				} catch (TestExecutionError err) {
					classStatuses.addError(err);
				} catch (TestAssertionFailure err) {
					classStatuses.addFailure(err);
				} catch (TestWarning testWarning) {
					classStatuses.addWarning(testWarning);
				} catch (Throwable thr) {
					applicationLog.severe("Error in enterClass", thr);
					classStatuses.addError(new TestExecutionError("Error in enterClass", thr));
				}
			}
		}

		// normal deal - just run the test
		StatusReporter.CompletionStatus methodStatuses = getClassStatus(executor, testClass).cloneMe();

		try {
			statusReporter.testBeginning(nextMethod);

			try {
				executor.runTest(nextMethod, methodStatuses);
			} finally {
				lastClassMap.put(executor, nextMethod);
			}
		} catch (TestExecutionError err) {
			methodStatuses.addError(err);
		} catch (TestAssertionFailure testAssertionFailure) {
			methodStatuses.addFailure(testAssertionFailure);
		} catch (TestWarning testWarning) {
			methodStatuses.addWarning(testWarning);
		} catch (Throwable thr) {
			applicationLog.severe("Error beginning or running test method", thr);
			methodStatuses.addError(new TestExecutionError("Error beginning or running test method", thr));
		} finally {
			statusReporter.testCompleted(nextMethod, methodStatuses);
		}

		return true;
	}

	private synchronized StatusReporter.CompletionStatus getClassStatus(BroadcasterExecutor executor, ETLTestClass testClass) {
		ClassStatus classStatus = classStatusMap.get(executor);

		if (classStatus == null) {
			classStatus = new ClassStatus();
			classStatusMap.put(executor, classStatus);
		}

		StatusReporter.CompletionStatus completionStatus = classStatus.completionStatusMap.get(testClass);

		if (completionStatus == null) {
			completionStatus = new StatusReporter.CompletionStatus();
			classStatus.completionStatusMap.put(testClass, completionStatus);
		}

		return completionStatus;
	}

	@Override
	public void waitForCompletion() {
		try {
			stopMutex.await();
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public void start() {
		startMutex.countDown();
	}

	@Override
	public int getActiveExecutorCount() {
		return waitingSemaphore.availablePermits();
	}

	@Override
	public void waitForExecutorsToActivate(int count) {
		// crappy polling
		while (true) {
			if (getActiveExecutorCount() == count) {
				return;
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new IllegalStateException(e);
			}
		}
	}

	private synchronized ETLTestMethod nextRunnableMethod() {
		if (methodQueue.size() == 0) {
			// check for another cases
			if (thisExecutorTestCases.size() == 0) {
				// all done
				return null;
			}

			ETLTestCases.ExecutorTestCases nextCases = thisExecutorTestCases.remove(0);

			// push all onto method queue
			for (ETLTestMethod _case : nextCases.getEtlTestMethods()) {
				methodQueue.addLast(_case);
			}
		}

		return methodQueue.removeFirst();
	}

	interface FailSafeCallback {
		Object call();
	}

	private Object callFailSafe(FailSafeCallback callback) {
		try {
			return callback.call();
		} catch (Throwable thr) {
			if (applicationLog != null) {
				applicationLog.severe("Error in failsafe callback", thr);
			}

			return null;
		}
	}
}
