package org.bitbucket.bradleysmithllc.etlunit.util.time;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;

public class EtlTimeUtils {

	public static final DateTimeFormatter LOCAL_TIME_FORMATTER = new DateTimeFormatterBuilder()
    .appendPattern("yyyy-MM-dd HH:mm:ss")
    .appendFraction(ChronoField.MICRO_OF_SECOND, 0, 6, true)
    .toFormatter();

	public static final DateTimeFormatter ZONED_TIME_FORMATTER = new DateTimeFormatterBuilder()
			.appendPattern("yyyy-MM-dd HH:mm:ss")
			.appendFraction(ChronoField.MICRO_OF_SECOND, 0, 6, true)
			.appendPattern("X")
			.toFormatter();

	public static ZoneId utcZoneId() {
		return ZoneId.of("UTC");
	}

	public static String format(ZonedDateTime zdt, DateTimeFormatter formatter) {
		return formatter.format(zdt);
	}

	public static String format(ZonedDateTime zdt, String pattern) {
		return format(zdt, DateTimeFormatter.ofPattern(pattern));
	}

	public static String format(ZonedDateTime zdt) {
		return format(zdt, LOCAL_TIME_FORMATTER);
	}

	public static ZonedDateTime utcFromLocalDateTimeString(String str, String pattern) {
		return utcFromLocalDateTimeString(str, DateTimeFormatter.ofPattern(pattern));
	}

	public static ZonedDateTime utcFromLocalDateTimeString(String str, DateTimeFormatter formatter) {
		return LocalDateTime.parse(str, formatter).atZone(utcZoneId());
	}

	public static ZonedDateTime utcFromZonedDateTimeString(String str) {
		return utcFromZonedDateTimeString(str, ZONED_TIME_FORMATTER);
	}

	public static ZonedDateTime utcFromZonedDateTimeString(String str, String pattern) {
		return utcFromZonedDateTimeString(str, DateTimeFormatter.ofPattern(pattern));
	}

	public static ZonedDateTime utcFromZonedDateTimeString(String str, DateTimeFormatter formatter) {
		return ZonedDateTime.parse(str, formatter).withZoneSameInstant(utcZoneId());
	}

	public static LocalDateTime localDateFromLocalDateTimeString(String str) {
		return LocalDateTime.parse(str, LOCAL_TIME_FORMATTER);
	}

	public static ZonedDateTime utcFromLocalDateTimeString(String str) {
		return utcFromLocalDateTimeString(str, LOCAL_TIME_FORMATTER);
	}

	public static ZonedDateTime utcFromTimestamp(Timestamp ts) {
		return ts.toLocalDateTime().atZone(utcZoneId());
	}

	public static ZonedDateTime utcFromDate(Date dt) {
		return dt.toInstant().atZone(utcZoneId());
	}
}
