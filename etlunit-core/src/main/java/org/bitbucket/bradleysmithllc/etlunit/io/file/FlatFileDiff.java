package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang.StringUtils;
import org.bitbucket.bradleysmithllc.etlunit.DiffGrid;
import org.bitbucket.bradleysmithllc.etlunit.DiffGridRow;
import org.bitbucket.bradleysmithllc.etlunit.DiffManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.datadiff.DataFileDiffNearParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.datadiff.DataFileDiffNearSpecification;
import org.bitbucket.bradleysmithllc.etlunit.parser.datadiff.NearDiffResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class FlatFileDiff
{
	private final DataFile source;
	private final List<DataFileSchema.Column> sourceColumns;

	public FlatFileDiff(DataFile source) throws IOException
	{
		this.source = source;
		this.sourceColumns = source.getDataFileSchema().getLogicalColumns();
	}

	public List<FileDiff> diffFile(DataFile target) throws IOException
	{
		return diffFile(target, null);
	}

	public List<FileDiff> diffFile(DataFile target, List<String> targetColumnsOfInterest) throws IOException
	{
		if (targetColumnsOfInterest == null)
		{
			targetColumnsOfInterest = source.getDataFileSchema().getLogicalColumnNames();
		}

		List<FileDiff> diffList = new ArrayList<FileDiff>();

		int sourceLineNumber = 0;
		int targetLineNumber = 0;

		DataFileReader sData = source.reader(targetColumnsOfInterest);

		try
		{
			DataFileReader tData = target.reader(targetColumnsOfInterest);

			try
			{

				Iterator<DataFileReader.FileRow> soit = sData.iterator();
				Iterator<DataFileReader.FileRow> tait = tData.iterator();

				DataFileReader.FileRow sourceData = null;
				DataFileReader.FileRow targetData = null;

				OrderKey keySource = null;
				OrderKey keyTarget = null;

				while (true)
				{
					if (sourceData == null && soit.hasNext())
					{
						sourceData = soit.next();

						// grab the key for each line and handle accordingly
						keySource = sourceData.getOrderKey();
						sourceLineNumber++;
					}

					if (targetData == null && tait.hasNext())
					{
						targetData = tait.next();

						// grab the key for each line and handle accordingly
						keyTarget = targetData.getOrderKey();
						targetLineNumber++;
					}

					if (sourceData == null && targetData == null)
					{
						break;
					}

					int compRes = 0;

					// fake this out a bit.  If the source or target are null (because we have run past the end of one but not
					// the other, create a fake compare result to trigger the correct action
					if (sourceData == null)
					{
						compRes = 1;
					}
					else if (targetData == null)
					{
						compRes = -1;
					}
					else
					{
						compRes = keySource.compareTo(keyTarget);
					}

					// typical 0, -1, 1 results
					if (compRes == 0)
					{
						compareLines(sourceData, targetData, targetColumnsOfInterest, diffList, sourceLineNumber, targetLineNumber);
						sourceData = null;
						targetData = null;
					}
					else if (compRes > 0)
					{
						diffList.add(new FileDiff(
								targetLineNumber,
								keyTarget,
								prettyPrintData(target, targetData.getData(), targetColumnsOfInterest),
								FileDiff.diff_type.added
						));

						// get rid of the target row so perhaps the next source row will match
						targetData = null;
					}
					else
					{
						// target missing row
						diffList.add(new FileDiff(
								sourceLineNumber,
								keySource,
								prettyPrintData(target, sourceData.getData(), targetColumnsOfInterest),
								FileDiff.diff_type.removed
						));

						// get rid of the source row so perhaps the next target row will match
						sourceData = null;
					}
				}
			}
			finally
			{
				tData.dispose();
			}
		}
		finally
		{
			sData.dispose();
		}

		return diffList;
	}

	private String prettyPrintData(DataFile file, Map<String, Object> sourceData, List<String> targetColumnsOfInterest)
	{
		StringBuilder stb = new StringBuilder("[");

		DataFileSchema schema = file.getDataFileSchema();

		boolean first = true;

		for (String col : targetColumnsOfInterest)
		{
			if (!first)
			{
				stb.append(", ");
			}
			else
			{
				first = false;
			}

			stb.append(col).append(": ");

			Object colData = sourceData.get(col);

			if (colData != null)
			{
				stb.append("'");
				if (schema != null)
				{
					DataFileSchema.Column column = schema.getColumn(col);

					// let the converter format
					stb.append(column.getConverter().format(colData, column));
				}
				else
				{
					// nothing to do but use the string value
					stb.append(colData);
				}
				stb.append("'");
			}
			else
			{
				stb.append("<<null>>");
			}
		}

		stb.append("]");

		return stb.toString();
	}

	private String readRow(BufferedReader targetReader) throws IOException
	{
		String line = null;

		while ((line = targetReader.readLine()) != null)
		{
			if (!lineIsIgnored(line))
			{
				break;
			}
		}

		return line;
	}

	public static boolean lineIsIgnored(String line)
	{
		String trim = line.trim();
		return trim.equals("") || (trim.startsWith("/*") && trim.endsWith("*/"));
	}

	private void compareLines(DataFileReader.FileRow soColumns, DataFileReader.FileRow taColumns, List<String> targetColumnsOfInterest, List<FileDiff> diffList, int sourceLine, int targetLineNumber)
	{
		Map<String, Object> soData = soColumns.getData();
		Map<String, Object> taData = taColumns.getData();

		for (String key : targetColumnsOfInterest)
		{
			// Grab the column data
			// get source and target and compare
			Object sourceValue = soData.get(key);
			Object targetValue = taData.get(key);

			//now go through the source columns and compare
			DataFileSchema.Column sourceColumn = soColumns.getColumn(key);
			DataFileSchema.Column targetColumn = taColumns.getColumn(key);

			String sourceValueText = null;

			if (sourceColumn != null && sourceValue != null)
			{
				sourceValueText = sourceColumn.getConverter().format(sourceValue, sourceColumn);
			}

			String targetValueText = null;

			if (targetColumn != null && targetValue != null)
			{
				targetValueText = targetColumn.getConverter().format(targetValue, targetColumn);
			}

			//Add a condition here that if two strings are being compared
			//Check for a special case to implement 'near' logic
			DataFileDiffNearSpecification sourceDataDiffSpec = sourceValueText != null ? DataFileDiffNearParser.optionallyScanForDataDiffSpecification(sourceValueText) : null;
			DataFileDiffNearSpecification targetDataDiffSpec = targetValueText != null ? DataFileDiffNearParser.optionallyScanForDataDiffSpecification(targetValueText) : null;

			OrderKey orderKey = soColumns.getOrderKey();

			if (sourceDataDiffSpec != null && targetDataDiffSpec != null) {
				// compare using the min and max of each spec
				int diffSpecComp = sourceDataDiffSpec.compareTo(targetDataDiffSpec);


				if (diffSpecComp != 0) {
					// fail
					diffList.add(new FileDiff(sourceLine,
							targetLineNumber,
							orderKey,
							key,
							sourceValueText,
							targetValueText
						)
					);
				}
			} else if (sourceDataDiffSpec != null || targetDataDiffSpec != null) {
				NearDiffResult results = null;

				if (sourceDataDiffSpec != null) {
					results = sourceDataDiffSpec.test(targetValueText);

					if (!results.comparisonSucceeded()) {
						// fail
						diffList.add(new FileDiff(sourceLine,
										targetLineNumber,
										orderKey,
										key,
										results.getExplanation(),
										targetValueText
								)
						);
					}
				} else {
					results = targetDataDiffSpec.test(sourceValueText);
					if (!results.comparisonSucceeded()) {
						// fail
						diffList.add(new FileDiff(sourceLine,
										targetLineNumber,
										orderKey,
										key,
										sourceValueText,
										results.getExplanation()
								)
						);
					}
				}
			} else if (!StringUtils.equals(sourceValueText, targetValueText))
			{
				diffList.add(new FileDiff(sourceLine,
						targetLineNumber,
						orderKey,
						key,
						sourceValueText,
						targetValueText));
			}
		}
	}

	public static void report(DiffManager dManager, ETLTestMethod context, ETLTestOperation operation, String failureId, List<FileDiff> diffs, String expectedId, String actualId)
	{
		DiffGrid diffGrid = dManager.reportDiff(context, operation, failureId, expectedId, actualId);

		try
		{
			for (FileDiff diff : diffs)
			{
				DiffGrid.line_type lt = DiffGrid.line_type.changed;

				switch (diff.getDiffType())
				{
					case added:
						lt = DiffGrid.line_type.added;
						break;
					case removed:
						lt = DiffGrid.line_type.removed;
						break;
					case changed:
						lt = DiffGrid.line_type.changed;
						break;
				}

				DiffGridRow diffRow = diffGrid.addRow(diff.getSourceRowNumber(), diff.getTargetRowNumber(), lt);

				try
				{
					diffRow.setOrderKey(String.valueOf(diff.getOrderKey().getPrettyString()));
					diffRow.setColumnName(String.valueOf(diff.getColumnName()));
					diffRow.setSourceValue(String.valueOf(diff.getSourceValue()));
					diffRow.setTargetValue(String.valueOf(diff.getOtherValue()));
				}
				finally
				{
					diffRow.done();
				}
			}
		}
		finally
		{
			diffGrid.done();
		}
	}
}
