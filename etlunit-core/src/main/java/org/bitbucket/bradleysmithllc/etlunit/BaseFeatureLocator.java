package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureLocator;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureMetaInfo;

import java.util.Iterator;
import java.util.List;

public abstract class BaseFeatureLocator implements FeatureLocator
{
	@Override
	public List<Feature> getFeatures(feature_type type)
	{
		List<Feature> list = getFeatures();

		if (type != feature_type.all)
		{
			Iterator<Feature> it = list.iterator();

			while (it.hasNext())
			{
				Feature feature = it.next();

				FeatureMetaInfo metaInfo = feature.getMetaInfo();

				switch (type)
				{
					case internal:
						if (!metaInfo.isInternalFeature())
						{
							it.remove();
						}
						break;
					case external:
						if (metaInfo.isInternalFeature())
						{
							it.remove();
						}
						break;
				}
			}
		}

		return list;
	}
}
