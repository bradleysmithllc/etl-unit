package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.*;

public class CommandNamesUtils {
	/*Given the names provided, create shortcuts with the fewest letters possible and return them in a map.  Names
	 * separated with a dash are automatically abbreviated as an abbreviation.
	 * E.G., given the input names 'tag', 'test', 'trist', 'tell-me-something', it will produce
	 * 'tag' = 't, 'test' = 'te', 'trist' = 'tr', 'tell-me-something' = 'tms' */
	public static Map<String, String> createShortcutsFor(Set<String> names) {
		// internally, the process is to handle all '-' separated values first.
		// secondly, sort the remaining keys alphabetically and shortest first, then going in order, try each value
		// starting with only the first char, then two chars, etc, until a combination that has not yet been
		// used and is unique is found.  In the case no completely unique solution is found, an empty map is returned.
		// This can happen for example with the input 'time-is-money' 'tim', 'ti' and 't'.  Since 'time-is-money' is first abbreviated
		// to 'tim', then the second input 'tim' cannot be made to be unique.  Another case is when two hyphenated names spell
		// the same word, E.G., 'time-is-money' and 'tim-is-mushy'.
		// Create a copy of the set and sort alphabetically.
		List<String> setCopy = new ArrayList<>(names);
		Map<String, String> map = new HashMap<>();

		Collections.sort(setCopy, new StringComparator().thenComparingInt((str) -> {return str.length();}));

		Iterator<String> listIt = setCopy.iterator();

		while (listIt.hasNext()) {
			String val = listIt.next();

			String dashed = dashify(val);

			if (dashed != null) {
				// matched a dash
				if (map.containsValue(dashed)) {
					return Collections.emptyMap();
				}

				map.put(val, dashed);

				// remove from processing
				listIt.remove();
			}
		}

		// run through the remaining words and attempt to abbreviate
		StringBuilder chars = new StringBuilder();
		for (String word : setCopy) {
			// go one char at a time

			chars.setLength(0);

			for (char nextChar : word.toCharArray()) {
				chars.append(nextChar);

				String provisional = chars.toString();

				if (!map.containsValue(provisional)) {
					map.put(word, provisional);
					break;
				}
			}

			// check for success
			if (!map.containsKey(word)) {
				return Collections.emptyMap();
			}
		}

		return map;
	}

	public static String dashify(String str) {
		if (str.contains("-")) {
			StringBuilder stb = new StringBuilder();

			String [] parts = str.split("-");

			for (String part : parts) {
				stb.append(part.charAt(0));
			}

			return stb.toString().toLowerCase();
		}

		return null;
	}
}
