package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class RelationalDataFileWriterImpl implements DataFileWriter
{
	private final DataFile flatFile;
	private final DataFileManager.CopyOptions options;

	BufferedWriter bwriter;
	int rowNum = 0;

	public RelationalDataFileWriterImpl(DataFile flatFile, DataFileManager.CopyOptions options) throws IOException
	{
		this.options = options;
		this.flatFile = flatFile;

		bwriter = new BufferedWriter(new FileWriter(flatFile.getSource()));

		if (options.writeHeader()) {
			// write out the column header
			List<DataFileSchema.Column> columns = flatFile.getDataFileSchema().getLogicalColumns();

			int size = columns.size();

			StringBuffer typeRow = new StringBuffer();

			// write this in a comment block
			bwriter.write("/*-- ");

			String columnDelimiter = flatFile.getDataFileSchema().getColumnDelimiter();

			if (columnDelimiter == null)
			{
				columnDelimiter = "|";
			}

			for (int i = 0; i < size; i++)
			{
				DataFileSchema.Column colu = columns.get(i);

				bwriter.write(colu.getId());

				typeRow.append(colu.getType());

				if (i != (size - 1))
				{
					bwriter.write(columnDelimiter);

					typeRow.append(columnDelimiter);
				}
			}

			bwriter.write("  --*/");
			bwriter.write(flatFile.getDataFileSchema().getRowDelimiter());

			// write this in a comment block
			bwriter.write("/*-- ");
			bwriter.write(typeRow.toString());
			bwriter.write("  --*/");
		}
	}

	/**
	 * Writes a row of data to the file.  In the case of a delimited file,
	 * escapes the delimiter.  In every case it escapes the row delimiter.
	 * @param rowData
	 * @throws IOException
	 */
	public void addRow(Map<String, Object> rowData) throws IOException
	{
		bwriter.write(flatFile.getDataFileSchema().getRowDelimiter());

		rowNum++;

		List<DataFileSchema.Column> columns = flatFile.getDataFileSchema().getLogicalColumns();

		for (int i = 0; i < columns.size(); i++)
		{
			DataFileSchema dataFileSchema = flatFile.getDataFileSchema();
			if (i != 0)
			{
				if (dataFileSchema.getColumnDelimiter() != null)
				{
					bwriter.write(dataFileSchema.getColumnDelimiter());
				}
			}

			DataFileSchema.Column col = columns.get(i);

			Object data = rowData.get(col.getId());

			String text = dataFileSchema.getNullToken();

			if (data != null)
			{
				text = col.getConverter().format(data, col);
			}

			// left space-pad fixed-length columns which are less than the required length
			if (col.getLength() != -1 && dataFileSchema.getFormatType() == DataFileSchema.format_type.fixed)
			{
				int colLength = col.getLength();

				int textLength = text.length();

				if (textLength < colLength)
				{
					text = StringUtils.leftPad(text, colLength, ' ');
				}
				else if (textLength > colLength)
				{
					// this is an exception
					throw new IOException("Line is greater than the column length: length[" + colLength + "], lineLength[" + textLength + "]");
				}
			}

			String etext = text;

			// escape the output text for row delimiters and column delimiters, if present
			if (dataFileSchema.escapeNonPrintable())
			{
				if (dataFileSchema.getColumnDelimiter() != null)
				{
					etext = escape(etext, dataFileSchema.getRowDelimiter(), dataFileSchema.getColumnDelimiter());
				}
				else
				{
					etext = escape(etext, dataFileSchema.getRowDelimiter());
				}
			}

			if (etext == null)
			{
				etext = flatFile.getDataFileSchema().getNullToken();
			}

			bwriter.write(etext);
		}
	}

	private String escape(String text, String... delimiters) {
		return EtlUnitStringUtils.escape(text, '\\', delimiters);
	}

	public void close() throws IOException
	{
		bwriter.close();
	}
}
