package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.OutputStream;

public class NfurcatingOutputStream extends OutputStream
{
	private final OutputStream stream;
	private final Object LOCK = new Object();

	private abstract class LockRunnable
	{
		public void run() throws IOException
		{
			synchronized (LOCK)
			{
				runLocked(stream);
			}
		}

		protected abstract void runLocked(OutputStream stream) throws IOException;
	}

	public NfurcatingOutputStream(OutputStream stream)
	{
		this.stream = stream;
	}

	@Override
	public void write(final int b) throws IOException
	{
		new LockRunnable()
		{
			@Override
			protected void runLocked(OutputStream stream1) throws IOException
			{
				stream1.write(b);
			}
		}.run();
	}

	@Override
	public void write(final byte[] b) throws IOException
	{
		new LockRunnable()
		{
			@Override
			protected void runLocked(OutputStream stream1) throws IOException
			{
				stream1.write(b);
			}
		}.run();
	}

	@Override
	public void write(final byte[] b, final int off, final int len) throws IOException
	{
		new LockRunnable()
		{
			@Override
			protected void runLocked(OutputStream stream1) throws IOException
			{
				stream1.write(b, off, len);
			}
		}.run();
	}

	@Override
	public void flush() throws IOException
	{
		new LockRunnable()
		{
			@Override
			protected void runLocked(OutputStream stream1) throws IOException
			{
				stream1.flush();
			}
		}.run();
	}

	@Override
	public void close() throws IOException
	{
		new LockRunnable()
		{
			@Override
			protected void runLocked(OutputStream stream1) throws IOException
			{
				stream1.close();
			}
		}.run();
	}

	public OutputStream bifurcate(final OutputStream target)
	{
		return new BifurcatingOutputStream(this, target)
		{
			@Override
			public void close() throws IOException
			{
				target.close();
			}
		};
	}
}
