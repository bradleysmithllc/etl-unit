package org.bitbucket.bradleysmithllc.etlunit.feature.tags;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlunit.TestClasses;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagRuntimeSupportImpl implements TagRuntimeSupport
{
	private final File tagDir;

	public TagRuntimeSupportImpl(File tagDir) {
		this.tagDir = tagDir;
	}

	@Override
	public TestClasses loadTagByNamePattern(String namePattern, String tagType) {
		final Pattern pat = Pattern.compile(namePattern);
		final String suffix = "-" + tagType + ".json";

		File [] files = tagDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				String name = pathname.getName();

				if (name.endsWith(suffix))
				{
					Matcher math = pat.matcher(pathname.getName());

					return math.find();
				}

				return false;
			}
		});

		if (files != null && files.length != 0)
		{
			return loadTagFile(files[0]);
		}

		throw new IllegalArgumentException("Tag not found: " + namePattern);
	}

	@Override
	public TestClasses loadTagByName(String namePattern, String tagType) {
		File source = getTagFile(namePattern, tagType);

		return loadTagFile(source);
	}

	@Override
	public File getTagFile(String namePattern, String tagType) {
		return new File(tagDir, namePattern + "-" + tagType + ".json");
	}

	private TestClasses loadTagFile(File source) {
		if (!source.exists())
		{
			throw new IllegalArgumentException("Tag file does not exist");
		}

		try {
			JsonNode node = JsonLoader.fromFile(source);

			TestClasses tc = new TestClasses();
			tc.fromJson(node);

			return tc;
		} catch (IOException e) {
			throw new IllegalArgumentException("Bad tag file", e);
		}
	}
}
