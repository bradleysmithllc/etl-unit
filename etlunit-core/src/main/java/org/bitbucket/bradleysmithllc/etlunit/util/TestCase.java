package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class TestCase
{
	private final String testName;
	private final long time;
	private boolean failure;
	private boolean error;

	private final StringBuilder stdout = new StringBuilder();

	public TestCase(String tn, long t)
	{
		testName = tn;
		time = t;
	}

	public String getTestName()
	{
		return testName;
	}

	public void setError()
	{
		error = true;
	}

	public void setFailure()
	{
		failure = true;
	}

	public boolean isFailure()
	{
		return failure;
	}

	public boolean isError()
	{
		return error;
	}

	public long getTimeInMillis()
	{
		return time;
	}

	public double getTime()
	{
		return (time / 1000.0D);
	}

	public StringBuilder getStdout()
	{
		return stdout;
	}

	public String getStdoutString()
	{
		return stdout.toString();
	}
}
