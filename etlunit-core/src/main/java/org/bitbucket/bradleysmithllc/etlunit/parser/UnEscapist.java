package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class UnEscapist
{
	public static String unescape(String s)
	{
		StringBuilder sb = new StringBuilder(s.length());

		for (int i = 0; i < s.length(); i++)
		{
			if (s.charAt(i) == '\\')
			{
				if (i + 1 < s.length())
				{
					i++;
					switch (s.charAt(i))
					{
						case 'n':
							sb.append('\n');
							break;
						case 'r':
							sb.append('\r');
							break;
						case '\\':
							sb.append('\\');
							break;
						case 'b':
							sb.append('\b');
							break;
						case 't':
							sb.append('\t');
							break;
						case 'f':
							sb.append('\f');
							break;
						case '\'':
							sb.append('\'');
							break;
						case '\"':
							sb.append('\"');
							break;
						default:
							sb.append(s.charAt(i));
					}
				}
			}
			else
			{
				sb.append(s.charAt(i));
			}
		}

		return sb.toString();
	}

	public static String escape(String s)
	{
		StringBuilder sb = new StringBuilder(s.length() * 2);

		for (int i = 0; i < s.length(); i++)
		{
			switch (s.charAt(i))
			{
				case '\n':
					sb.append("\\n");
					break;
				case '\r':
					sb.append("\\r");
					break;
				case '\\':
					sb.append("\\\\");
					break;
				case '\b':
					sb.append("\\b");
					break;
				case '\t':
					sb.append("\\t");
					break;
				case '\f':
					sb.append("\\f");
					break;
				case '\'':
					sb.append("\\'");
					break;
				case '\"':
					sb.append("\\\"");
					break;

				default:
					sb.append(s.charAt(i));
			}
		}

		return sb.toString();
	}

	public static String dequotify(String s)
	{
		// only process if the first and last characters are the correct
		// types of quotes.  E.G., don't strip 'Hello" or "Hello',
		// or "Hello, or Hello", etc.
		if (
				s.charAt(0) == '\'' && s.charAt(s.length() - 1) == '\''
						||
						s.charAt(0) == '\"' && s.charAt(s.length() - 1) == '\"'
				)
		{
			return s.substring(1, s.length() - 1);
		}

		return s;
	}
}
