package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.ClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.ConditionalDirector;
import org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.parser.specification.TestSpecificationParser;

import java.util.Arrays;
import java.util.List;

public class UserDirectedClassDirectorFeature extends AbstractFeature
{
	private final String instanceName;

	private final ClassDirector classDirector;

	public UserDirectedClassDirectorFeature(String instanceName, String desc) throws ParseException {
		// use the parser
		classDirector = TestSpecificationParser.parse(desc);

		this.instanceName = instanceName;
	}

	@Override
	public void initialize(Injector inj) {
		inj.injectMembers(classDirector);

		// clunky, check for conditional directors so that members can be injected
		if (classDirector instanceof ConditionalDirector)
		{
			ConditionalDirector cd = (ConditionalDirector) classDirector;

			inj.injectMembers(cd.getLdirector());
			inj.injectMembers(cd.getRdirector());
		}
	}

	@Override
	public String getFeatureName()
	{
		return "user-class-director_" + instanceName;
	}

	@Override
	public ClassDirector getDirector()
	{
		return classDirector;
	}

	@Override
	public List<String> getPrerequisites() {
		return Arrays.asList("tags");
	}

	@Override
	public long getPriorityLevel()
	{
		return -1L;
	}
}
