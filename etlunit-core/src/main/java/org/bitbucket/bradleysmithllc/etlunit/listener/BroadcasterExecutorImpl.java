package org.bitbucket.bradleysmithllc.etlunit.listener;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.HashMapArrayList;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BroadcasterExecutorImpl implements Runnable, BroadcasterExecutor {
	private final BroadcasterCoordinator coordinator;
	private final int id;
	private final MapLocal mapLocal;
	private final VariableContext executionContext;
	private VariableContext packageScope;
	private VariableContext classScope;
	private final ClassListener classListener;
	private final ClassDirector classDirector;
	private final StatusReporter statusReporter;
	private final Log applicationLog;
	private final LocalDateTime testRunDate;

	public BroadcasterExecutorImpl(
			BroadcasterCoordinator brc,
			int id,
			MapLocal mapLocal,
			VariableContext context,
			ClassDirector classDirector,
			ClassListener classListener,
			StatusReporter statusReporter,
			Log applicationLog,
			LocalDateTime trd
	) {
		this.statusReporter = statusReporter;
		this.applicationLog = applicationLog;
		this.classDirector = classDirector;
		this.classListener = classListener;

		// This execution context must be hidden from the others
		this.executionContext = context.createNestedScope(false);

		this.mapLocal = mapLocal;
		coordinator = brc;
		this.id = id;
		testRunDate = trd;
	}


	@Override
	public void run() {
		// poll the coordinator until it returns false which means done
		while (coordinator.ready(this)) ;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void beginExecution()  throws TestExecutionError {
		// use this for our global announcement since the executor 'owns' the class listenter
		classListener.beginTests(executionContext, getId());
	}

	@Override
	public void enterPackage(ETLTestPackage name, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {
		packageScope = executionContext.createNestedScope();

		classListener.beginPackage(name, packageScope, getId());
	}

	@Override
	public void enterClass(ETLTestClass cl, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {
		mapLocal.setCurrentlyProcessingTestClass(cl);

		classScope = packageScope.createNestedScope();
		classScope.declareAndSetValue("etlunit_test_class", new ETLTestValueObjectImpl(cl));

		// run through the before class stuff here
		try {
			classListener.begin(cl, classScope, getId());
		} catch (TestWarning warn) {
			status.addWarning(warn);
		}

		// broadcast the variables
		List<ETLTestVariable> varlist = cl.getClassVariables();
		Iterator<ETLTestVariable> varit = varlist.iterator();

		while (varit.hasNext()) {
			ETLTestVariable var = varit.next();
			classListener.declare(var, classScope, getId());
		}

		// before class methods
		List<ETLTestMethod> beforeClassMethods = cl.getBeforeClassMethods();
		Iterator<ETLTestMethod> bcit = beforeClassMethods.iterator();

		while (bcit.hasNext() && !status.hasFailures()) {
			ETLTestMethod bcmethod = bcit.next();

			broadcastMethod(null, bcmethod, status, classScope);
		}
	}

	private void broadcastMethod(final ETLTestMethod concerning, ETLTestMethod next, StatusReporter.CompletionStatus status, final VariableContext methodContext) throws TestExecutionError {
		List<ETLTestAnnotation> methodDefaults = next.getAnnotations("@OperationDefault");
		List<ETLTestAnnotation> classDefaults = next.getTestClass().getAnnotations("@OperationDefault");

		final MapList<String, ETLTestAnnotation> defaultMap = new HashMapArrayList<String, ETLTestAnnotation>();

		// grab the method defaults.  Overlay if a class default is present
		for (ETLTestAnnotation mdefault : methodDefaults) {
			Map<String, ETLTestValueObject> value = mdefault.getValue().getValueAsMap();

			// get the operation name
			ETLTestValueObject opName = value.get("operation");

			String opNameValueAsString = opName.getValueAsString();

			defaultMap.getOrCreate(opNameValueAsString).add(mdefault);
		}

		// grab the class defaults first.  Just drop these in
		for (ETLTestAnnotation cdefault : classDefaults) {
			Map<String, ETLTestValueObject> value = cdefault.getValue().getValueAsMap();

			// get the operation name
			ETLTestValueObject opName = value.get("operation");

			defaultMap.getOrCreate(opName.getValueAsString()).add(cdefault);
		}

		List<ETLTestOperation> ops = next.getOperations();

		Iterator<ETLTestOperation> it = ops.iterator();

		while (it.hasNext()) {
			ETLTestOperation op = it.next();

			mapLocal.setCurrentlyProcessingTestOperation(op);

			if (classDirector.accept(op) == ClassResponder.response_code.accept) {
				try {
					ETLTestValueObject operands = resolveMergedOperationValue(defaultMap, op);

					final ExecutionContext econtext = new ExecutionContext() {
						@Override
						public void process(ETLTestOperation op, VariableContext vcontext)
								throws TestAssertionFailure, TestExecutionError, TestWarning {
							ETLTestValueObject operands = resolveMergedOperationValue(defaultMap, op);

							// handled or reject are both acceptable
							if (classListener.process(concerning, op, operands, methodContext, this, 1) == ClassResponder.action_code.defer) {
								throw new TestExecutionError("Listener could not handle operation: "
										+ op.getOperationName()
										+ ": "
										+ (operands != null ? operands.getJsonNode() : "{}"), TestConstants.ERR_INVALID_OPERATION);
							}
						}
					};

					methodContext.declareAndSetValue("etlunit_test_operation", new ETLTestValueObjectImpl(op));
					classListener.begin(concerning, op, operands, methodContext, econtext, 1);

					try {
						if (classListener.process(concerning, op, operands, methodContext, econtext, 1) == ClassResponder.action_code.defer) {
							throw new TestExecutionError("Listener could not handle operation: " + op.getOperationName() + ": " + (
									operands != null
											? operands.getJsonNode()
											: "{}"), TestConstants.ERR_INVALID_OPERATION);
						}
					} finally {
						classListener.end(concerning, op, operands, methodContext, econtext, 1);
					}
				} catch (TestAssertionFailure asrt) {
					status.addFailure(asrt);
				} catch (TestWarning warn) {
					status.addWarning(warn);
				} catch (TestExecutionError thr) {
					throw thr;
				} catch (Throwable thr) {
					throw new TestExecutionError("Untrapped error", TestConstants.ERR_UNCAUGHT_EXCEPTION, thr);
				}
			} else {
				applicationLog.info("Test operation not accepted:  " + op.getQualifiedName());
			}
		}
	}

	private ETLTestValueObject resolveMergedOperationValue(MapList<String, ETLTestAnnotation> defaultMap, ETLTestOperation op) {
		// TODO - lookup this operation against the map and merge defaults if found
		List<ETLTestAnnotation> opDefaults = defaultMap.get(op.getOperationName());

		ETLTestValueObject toperands = op.getOperands();

		if (opDefaults != null) {
			for (ETLTestAnnotation opdef : opDefaults) {
				Map<String, ETLTestValueObject> annotValueAsMap = opdef.getValue().getValueAsMap();

				ETLTestValueObject defaultValue = annotValueAsMap.get("defaultValue");

				if (toperands != null) {
					Map<String, ETLTestValueObject> toperandsValueAsMap = toperands.getValueAsMap();

					// verify the matchWhen and doNotMatchWhen conditions

					boolean shouldMerge = true;

					// run through matchWhen first
					if (annotValueAsMap.containsKey("matchWhen")) {
						for (String matchProperty : annotValueAsMap.get("matchWhen").getValueAsListOfStrings()) {
							if (!toperandsValueAsMap.containsKey(matchProperty)) {
								shouldMerge = false;
								break;
							}
						}
					}

					if (annotValueAsMap.containsKey("doNotMatchWhen")) {
						for (String matchProperty : annotValueAsMap.get("doNotMatchWhen").getValueAsListOfStrings()) {
							if (toperandsValueAsMap.containsKey(matchProperty)) {
								shouldMerge = false;
								break;
							}
						}
					}

					if (shouldMerge) {
						// this is a left merge.  Defaults can't override actual arguments
						toperands = toperands.merge(defaultValue, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
					}
				} else {
					// if there is a matchwhen clause, then this always fails.  doNotMatchWhen is irrelevant
					if (!annotValueAsMap.containsKey("matchWhen")) {
						toperands = defaultValue;
					}
				}
			}
		}

		return toperands;
	}

	@Override
	public void runTest(ETLTestMethod mt, StatusReporter.CompletionStatus methodStatus) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// right off the bat, if we have already encountered a failure return with that status
		if (methodStatus.hasFailures()) {
			return;
		}

		mapLocal.setCurrentlyProcessingTestMethod(mt);

		VariableContext methodScope = classScope.createNestedScope();
		mapLocal.setCurrentlyProcessingVariableContext(methodScope);

		// store the test method object
		methodScope.declareAndSetValue("etlunit_test_method", new ETLTestValueObjectImpl(mt));

		// insulate from warnings - errors and failures should break out
		// set up the method status before notifying listener of intent to test
		List<ETLTestAnnotation> testAnnotList = mt.getAnnotations("@Test");

		// we only care about the first one - all others are bogus.
		// There must be at least one or this method would not be a test method
		ETLTestAnnotation testAnnot = testAnnotList.get(0);

		try {
			ETLTestValueObject value = testAnnot.getValue();

			if (value != null) {
				ETLTestValueObject expect = value.query("expected-error-id");

				if (expect != null && !expect.isNull()) {
					if (expect.getValueType() != ETLTestValueObject.value_type.quoted_string) {
						throw new TestExecutionError("Invalid expected-error-id - must be a string type",
								TestConstants.ERR_INVALID_TEST_ANNOTATION);
					}

					methodStatus.setExpectedErrorId(expect.getValueAsString());
				}

				expect = value.query("expected-failure-ids");

				if (expect != null && !expect.isNull()) {
					if (expect.getValueType() != ETLTestValueObject.value_type.list) {
						throw new TestExecutionError("Invalid expected-failure-ids - must be a list type",
								TestConstants.ERR_INVALID_TEST_ANNOTATION);
					}

					methodStatus.addExpectedFailureIds(expect.getValueAsListOfStrings());
				}

				expect = value.query("expected-failure-id");

				if (expect != null && !expect.isNull()) {
					if (expect.getValueType() != ETLTestValueObject.value_type.quoted_string) {
						throw new TestExecutionError("Invalid expected-failure-id - must be a string type",
								TestConstants.ERR_INVALID_TEST_ANNOTATION);
					}

					methodStatus.addExpectedFailureId(expect.getValueAsString());
				}

				expect = value.query("expected-warning-ids");

				if (expect != null && !expect.isNull()) {
					if (expect.getValueType() != ETLTestValueObject.value_type.list) {
						throw new TestExecutionError("Invalid expected-warning-ids - must be a list type",
								TestConstants.ERR_INVALID_TEST_ANNOTATION);
					}

					methodStatus.addExpectedWarningIds(expect.getValueAsListOfStrings());
				}
			}

			runTestImpl(mt,
					mt.getTestClass().getBeforeTestMethods(),
					mt.getTestClass().getAfterTestMethods(),
					statusReporter,
					methodStatus,
					methodScope
			);
		} catch (TestExecutionError err) {
			methodStatus.addError(err);
		} catch (TestAssertionFailure err) {
			methodStatus.addFailure(err);
		} catch (TestWarning err) {
			methodStatus.addWarning(err);
		} catch (Throwable err) {
			applicationLog.severe("", err);
			methodStatus.addError(new TestExecutionError(err.toString(),
					TestConstants.ERR_UNCAUGHT_EXCEPTION,
					err));
		}
	}

	private void runTestImpl(ETLTestMethod mt, List<ETLTestMethod> beforeTestMethods, List<ETLTestMethod> afterTestMethods, StatusReporter statusReporter, StatusReporter.CompletionStatus methodStatus, VariableContext methodScope) throws TestExecutionError, TestAssertionFailure, TestWarning {
		// check for ignores before processing
		try {
				String ignoreReason = ignored(mt.getTestClass());
				if (ignoreReason != null) {
					applicationLog.info("Test class is ignored: " + mt.getTestClass().getQualifiedName());
					// succeed quietly
					methodStatus.addIgnoreReason(ignoreReason);
					return;
				}

				ignoreReason = ignored(mt);
				if (ignoreReason != null) {
					applicationLog.info("Test method is ignored: " + mt.getQualifiedName());
					// succeed quietly
					methodStatus.addIgnoreReason(ignoreReason);
					return;
				}
		} catch (TestAssertionFailure tef) {
			// add to the status and return
			methodStatus.addFailure(tef);
		}

		classListener.begin(mt, methodScope, getId());

		try {
			runTest(mt,
					mt.getTestClass().getBeforeTestMethods(),
					mt.getTestClass().getAfterTestMethods(),
					statusReporter,
					methodStatus,
					methodScope
			);
		} finally {
			classListener.end(mt, methodScope, getId());
		}
	}

	private String check(VariableContext methodScope) {
		if (methodScope.hasVariableBeenDeclared("FILE_ASSERT_FILE_SCHEMA")) {
			return getId() + "_" + methodScope.getValue("FILE_ASSERT_FILE_SCHEMA").getValueAsString();
		}
		return getId() + "_" + "N/A";
	}

	private void runTest(ETLTestMethod method, List<ETLTestMethod> beforeTestMethods, List<ETLTestMethod> afterTestMethods, StatusReporter statusReporter, StatusReporter.CompletionStatus methodStatus, VariableContext methodScope)
			throws TestExecutionError {
		// store the test method object
		methodScope.declareAndSetValue("etlunit_test_method", new ETLTestValueObjectImpl(method));

		try {
			runTestBody(method, beforeTestMethods, afterTestMethods, methodStatus, methodScope);
		} catch (TestExecutionError err) {
			applicationLog.severe("", err);
			throw err;
		}
	}

	private void runTestBody(ETLTestMethod method, List<ETLTestMethod> beforeTestMethods, List<ETLTestMethod> afterTestMethods, StatusReporter.CompletionStatus methodStatus, VariableContext methodScope) throws TestExecutionError {
		// hit the before test methods
		Iterator<ETLTestMethod> bit = beforeTestMethods.iterator();

		while (bit.hasNext()) {
			ETLTestMethod bmethod = bit.next();

			mapLocal.setCurrentlyProcessingMethod(bmethod);

			broadcastMethod(method, bmethod, methodStatus, methodScope);
		}

		// test method
		if (!methodStatus.hasFailures()) {
			mapLocal.setCurrentlyProcessingMethod(method);

			broadcastMethod(method, method, methodStatus, methodScope);
		}

		// hit the after test methods
		Iterator<ETLTestMethod> ait = afterTestMethods.iterator();

		while (ait.hasNext()) {
			ETLTestMethod amethod = ait.next();

			broadcastMethod(method, amethod, methodStatus, methodScope);
		}
	}

	@Override
	public void leaveClass(ETLTestClass cl, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {

		try {
			// now broadcast all the after class methods.  Any failures here will break
			// the entire chain.
			List<ETLTestMethod> afterClassMethods = cl.getAfterClassMethods();
			Iterator<ETLTestMethod> acit = afterClassMethods.iterator();

			while (acit.hasNext() && !status.hasFailures()) {
				ETLTestMethod bcmethod = acit.next();

				broadcastMethod(null, bcmethod, status, classScope);
			}
		} catch (TestExecutionError err) {
			status.addError(err);
		} catch (Throwable thr) {
			status.addError(new TestExecutionError("", thr));
		} finally {
			try {
				classListener.end(cl, classScope, getId());
			} catch (Throwable thr) {
				applicationLog.severe("", thr);
			}
		}
	}

	@Override
	public void leavePackage(ETLTestPackage name, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning {
		classListener.endPackage(name, packageScope, getId());
	}

	@Override
	public void endExecution() {
		// use this for our global announcement since the executor 'owns' the class listenter
		classListener.endTests(executionContext, getId());
	}

	private String ignored(ETLTestClass cl) throws TestExecutionError, TestAssertionFailure {
		return ignored(cl.getAnnotations("@Ignore"));
	}

	private String ignored(ETLTestMethod mt) throws TestExecutionError, TestAssertionFailure {
		return ignored(mt.getAnnotations("@Ignore"));
	}

	private String ignored(List<ETLTestAnnotation> anns) throws TestExecutionError, TestAssertionFailure {
		// if there is an ignore annotation, check for an expiration
		if (anns.size() != 0) {
			for (ETLTestAnnotation ann : anns) {
				// check for expiration date
				ETLTestValueObject annv = ann.getValue();

				// grab the reason
				String reason = annv.query("reason").getValueAsString();

				ETLTestValueObject reactivateQuery = annv.query("reactivate-on");
				if (reactivateQuery != null) {
					String d = reactivateQuery.getValueAsString();

					// date must be in the form of 'yyyy-MM-dd'
					DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");

					try {
						LocalDate dueDate = LocalDate.parse(d, df);

						// check dates
						if (dueDate.isBefore(testRunDate.toLocalDate()) || dueDate.isEqual(testRunDate.toLocalDate())) {
							// check for an expiration error or failure
							ETLTestValueObject erroridQ = annv.query("error-id");

							if (erroridQ != null) {
								// fail with the specified error
								throw new TestExecutionError("Ignore expired", erroridQ.getValueAsString());
							}

							// check for an expiration error or failure
							ETLTestValueObject failureidQ = annv.query("failure-id");

							if (failureidQ != null) {
								// fail with the specified error
								throw new TestAssertionFailure("Ignore expired", failureidQ.getValueAsString());
							}

							// left open - use the generic error
							throw new TestExecutionError("Ignore expired", TestConstants.ERR_IGNORE_EXPIRED);
						} else {
							return reason;
						}
					} catch (DateTimeParseException e) {
						throw new TestExecutionError(e.toString() + "  Date format is yyyy-MM-dd", TestConstants.ERR_INVALID_ANNOTATION_PROPERTIES);
					}
				}

				return reason;
			}
		}

		return null;
	}
}
