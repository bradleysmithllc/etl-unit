package org.bitbucket.bradleysmithllc.etlunit.io.file.converter;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.regex.Pattern;

public class NumericConverter extends JdbcConverter {
	public static final String PATTERN = "-?\\d{1,}(\\.\\d{1,})?";
	public static final Pattern P_PATTERN = Pattern.compile(PATTERN);
	public static final Pattern UNBOUND_INTEGER = Pattern.compile(
			"-?\\d{1,}"
	);

	@Override
	public String format(Object data, DataFileSchema.Column column) {
		check(column);

		if (data instanceof BigDecimal) {
			return ((BigDecimal) data).toPlainString();
		} else {return String.valueOf(data);}
	}

	@Override
	public Object parse(String data, DataFileSchema.Column column) {
		check(column);

		MathContext mcon = MathContext.UNLIMITED;

		int scale = column.getScale();
		int length = column.getLength();
		if (length != -1) {
			mcon = new MathContext(length, RoundingMode.HALF_UP);
		}

		BigDecimal bigDecimal = new BigDecimal(data, mcon);

		if (scale != -1) {
			bigDecimal.setScale(scale);
		}

		return bigDecimal;
	}

	private static void check(DataFileSchema.Column column) {
		check(column.getLength(), column.getScale());
	}

	private static void check(int length, int scale) {
		if (length == 0) {
			throw new IllegalArgumentException("Length cannot be 0");
		}

		// -1 is a valid scale, it means no scale - any other negative is an error
		if (scale < -1) {
			throw new IllegalArgumentException("Scale can be either -1 for no scale, or >= 0.  Effective scale cannot be negative");
		}

		if (length != -1 && scale > length) {
			throw new IllegalArgumentException("Scale cannot exceed length: [" + length + ", " + scale + "]");
		}
	}

	@Override
	public String getId() {
		return "NUMERIC";
	}

	public static Pattern getPattern(int length, int scale) {
		check(length, scale);

		// scale without length is okay
		if (length != -1) {
			// length and 0 scale
			if (scale == 0) {
				return Pattern.compile(
						"-?(0|\\d{1," + length + "})"
				);
			}
			else if (scale == -1)
			{
				// length and unspecified scale - just resort to default
				return P_PATTERN;
			}
			else
			{
				// length and specified scale
				int leftLength = length - scale;

				return Pattern.compile("-?" + (leftLength == 0 ? "0" : "\\d{1," + leftLength + "}") + "(\\.\\d{1," + scale + "})?");
			}
		} else if (scale == 0) {
			// unspecified length with no scale.
			return UNBOUND_INTEGER;
		} else if (scale != -1) {
			// unspecified length with a scale component.
			return Pattern.compile(
					"-?\\d{1,}(\\.\\d{1," + scale + "})?"
			);
		} else {
			// unspecified length and scale
			return P_PATTERN;
		}
	}

	@Override
	public Pattern getPattern(DataFileSchema.Column column) {
		return getPattern(column.getLength(), column.getScale());
	}
}
