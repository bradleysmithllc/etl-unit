package org.bitbucket.bradleysmithllc.etlunit.metadata.impl;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifactContent;
import org.bitbucket.bradleysmithllc.etlunit.metadata.TestReference;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MetaDataArtifactContentImpl implements MetaDataArtifactContent
{
	private RuntimeSupport runtimeSupport;
	private MetaDataArtifact metaDataArtifact;
	private String name;
	private final List<TestReference> references = new ArrayList<TestReference>();

	public MetaDataArtifactContentImpl() {
	}

	public MetaDataArtifactContentImpl(String name) {
		this.name = name;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<TestReference> getReferences() {
		return Collections.unmodifiableList(references);
	}

	@Override
	public TestReference referencedByCurrentTest(String classification) {
		if (runtimeSupport == null)
		{
			throw new IllegalStateException("Runtime support not available");
		}

		return referencedBy(classification, runtimeSupport.getCurrentlyProcessingTestMethod());
	}

	@Override
	public MetaDataArtifact getMetaDataArtifact() {
		return metaDataArtifact;
	}

	public void setMetaDataArtifact(MetaDataArtifact metaDataArtifact) {
		this.metaDataArtifact = metaDataArtifact;
	}

	@Override
	public synchronized TestReference referencedBy(String classification, ETLTestMethod method) {
		// only add if it isn't there already
		TestReference reference = null;

		for (TestReference ref : references)
		{
			if (ref.getClassification().equals(classification) && ref.getQualifiedTestName().equals(method.getQualifiedName()))
			{
				reference = ref;
				break;
			}
		}

		if (reference == null)
		{
			reference = new TestReferenceImpl(
					classification,
					method.getTestClass().getPackage(),
					method.getTestClass().getName(),
					method.getName(),
					method.getQualifiedName()
			);

			references.add(reference);
		}

		return reference;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("name").value(name);

		writer.name("referencedBy");
		writer.beginArray();

		for (TestReference ref : references)
		{
			writer.beginObject();
			ref.toJson(writer);
			writer.endObject();
		}

		writer.endArray();
	}

	@Override
	public synchronized void fromJson(JsonNode node) {
		references.clear();

		name = node.get("name").asText();

		ArrayNode refsNode = (ArrayNode) node.get("referencedBy");

		Iterator<JsonNode> refs = refsNode.elements();

		while (refs.hasNext())
		{
			TestReferenceImpl timpl = new TestReferenceImpl();

			timpl.fromJson(refs.next());

			references.add(timpl);
		}
	}

	public boolean equals(Object other)
	{
		if (getClass() != other.getClass())
		{
			return false;
		}

		MetaDataArtifactContentImpl oImpl = (MetaDataArtifactContentImpl) other;

		return ObjectUtils.compare(name, oImpl.name) == 0
				&& references.equals(oImpl.references);
	}
}
