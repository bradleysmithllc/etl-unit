package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

import java.util.HashMap;
import java.util.Map;

public class MapLocal extends ThreadLocal<Map<String, Object>>
{

	public static final Integer NO_CURRENT_EXECUTOR = new Integer(-1);

	@Override
	protected Map<String, Object> initialValue()
	{
		return new HashMap<String, Object>();
	}

	public <T extends Object> T get(String name, Class<T> type)
	{
		return get(name, type, true);
	}

	public <T extends Object> T get(String name, Class<T> type, boolean optional)
	{
		Object ret = get().get(name);

		if (ret == null)
		{
			if (optional)
			{
				return null;
			}

			throw new IllegalArgumentException("Object named '" + name + "' not found in map");
		}

		if (!type.isAssignableFrom(ret.getClass()))
		{
			throw new ClassCastException("Stored object type does not match requested type");
		}

		return (T) ret;
	}

	public <T extends Object> void set(String name, T type)
	{
		get().put(name, type);
	}
	public void clear(String name)
	{
		get().remove(name);
	}

	public void setCurrentlyProcessingTestClass(ETLTestClass etlClass)
	{
		set("currentlyProcessingTestClass", etlClass);
	}

	public ETLTestClass getCurrentlyProcessingTestClass()
	{
		return get("currentlyProcessingTestClass", ETLTestClass.class);
	}

	public void setCurrentlyProcessingTestMethod(ETLTestMethod etlClass)
	{
		set("currentlyProcessingTestMethod", etlClass);
	}

	public ETLTestMethod getCurrentlyProcessingTestMethod()
	{
		return get("currentlyProcessingTestMethod", ETLTestMethod.class);
	}

	public void setCurrentlyProcessingMethod(ETLTestMethod etlClass)
	{
		set("currentlyProcessingMethod", etlClass);
	}

	public ETLTestMethod getCurrentlyProcessingMethod()
	{
		return get("currentlyProcessingMethod", ETLTestMethod.class);
	}

	public void setCurrentlyProcessingTestOperation(ETLTestOperation etlClass)
	{
		set("currentlyProcessingTestOperation", etlClass);
	}

	public ETLTestOperation getCurrentlyProcessingTestOperation()
	{
		return get("currentlyProcessingTestOperation", ETLTestOperation.class);
	}

	public void setCurrentlyProcessingVariableContext(VariableContext context)
	{
		set("currentlyProcessingVariableContext", context);
	}

	public VariableContext getCurrentlyProcessingVariableContext()
	{
		return get("currentlyProcessingVariableContext", VariableContext.class);
	}

	public void setExecutorId(int executorId)
	{
		set("executorId", new Integer(executorId));
	}

	public int getExecutorId()
	{
		Integer executorId = get("executorId", Integer.class);
		if (executorId == null)
		{
			return -1;
		}

		return executorId.intValue();
	}

	public void clearExecutorId()
	{
		set("executorId", NO_CURRENT_EXECUTOR);
	}
}
