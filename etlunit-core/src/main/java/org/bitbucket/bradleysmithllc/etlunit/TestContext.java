package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.CommandLine;

import java.io.IOException;
import java.util.Map;

public interface TestContext extends CommandLineSwitches
{
	Map<String, Object> getTestAttributeMap();

	Object getTestAttribute(String name);

	void setTestAttribute(String name, Object val);

	Map<String, Object> getClassAttributeMap();

	Object getClassAttribute(String name);

	void setClassAttribute(String name, Object val);

	Map<String, Object> getSystemAttributeMap();

	Object getSystemAttribute(String name);

	void setSystemAttribute(String name, Object val);

	CommandLine getCommandLine();

	void executeOperation(String operationName, ETLTestValueObject operands) throws Exception;

	void executeOperation(String operationName, String operands) throws Exception;

	ETLTestValueObject parseOperands(String map);

	void writeToProcessLog(byte[] bytes, int offset, int length) throws IOException;

	void writeToProcessLog(String msg) throws IOException;

	void writeToProcessLog(String[] msgs) throws IOException;

	void testClassRunning(ETLTestClass itc);

	void testMethodRunning(ETLTestMethod itm) throws TestExecutionError;

	void testMethodCompleted();

	void testFailed();

	void testErrored();

	void dispose();

	boolean checkRuntimeOption(CommandLine.Switch name);

	boolean checkRuntimeOption(CommandLine.Switch name, boolean optionDefault, boolean flagDefault);

	String getRuntimeOption(CommandLine.Switch name);
}
