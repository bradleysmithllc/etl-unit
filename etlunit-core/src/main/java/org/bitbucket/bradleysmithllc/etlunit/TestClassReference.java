package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestClassReference implements JsonSerializable
{
	private final String testClassQualifiedName;
	private final List<String> testMethodNames = new ArrayList<String>();

	public TestClassReference(String cl, List<ETLTestMethod> methods)
	{
		testClassQualifiedName = cl;
		addAllMethods(methods);
	}

	public TestClassReference(String cl)
	{
		testClassQualifiedName = cl;
	}

	public void addMethod(String name)
	{
		testMethodNames.add(name);
	}

	private void addAllMethods(List<ETLTestMethod> methods) {
		for (ETLTestMethod method : methods)
		{
			addMethod(method.getName());
		}
	}

	public String getTestClassQualifiedName()
	{
		return testClassQualifiedName;
	}

	public List<String> getTestMethods()
	{
		return testMethodNames;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("tests");
		writer.beginArray();

		for (String classRef : testMethodNames)
		{
			writer.value(classRef);
		}

		writer.endArray();
	}

	@Override
	public void fromJson(JsonNode node) {
		testMethodNames.clear();

		JsonNode clnode = node.get("tests");

		for (JsonNode field : clnode)
		{
			testMethodNames.add(field.asText());
		}
	}

	public boolean acceptTestMethod(String name) {
		return testMethodNames.contains(name);
	}
}
