package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class StreamProcessExecutor implements ProcessExecutor {
	@Override
	public ProcessFacade execute(final ProcessDescription pd) throws IOException {
		ProcessBuilder pb = new ProcessBuilder(pd.getCommand());

		List<String> args = new ArrayList<String>(pd.getArguments());
		args.add(0, pd.getCommand());

		pb.command(args);

		pb.redirectErrorStream(pd.isRedirectStandardError());

		pb.directory(pd.getWorkingDirectory());

		pb.environment().putAll(pd.getEnvironment());

		Process process = pb.start();

		int numStreams = 2;

		if (pd.isRedirectStandardError())
		{
			numStreams = 1;
		}

		// Plus one for the output file
		CountDownLatch clatch = new CountDownLatch(numStreams);

		// very primitive for now.  Create a thread to read the file until it reaches end of file
		File streamOut = StreamProcessExecutorProcessFacade.getStreamOut(pd);

		// touch in case the listener hits it first
		FileUtils.touch(streamOut);

		File output = pd.getOutputFile();

		if (output == null)
		{
			output = pd.getPrivateOutput();
		}

		FileUtils.touch(output);

		OutputStream logOutputStream = new SynchronizedFilterOutputStream(new BufferedOutputStream(new FileOutputStream(output)));

		new Thread(new StreamFramer(process.getInputStream(), streamOut, logOutputStream, clatch)).start();

		if (!pd.isRedirectStandardError())
		{
			File streamErr = StreamProcessExecutorProcessFacade.getStreamErr(pd);
			FileUtils.touch(streamErr);

			new Thread(new StreamFramer(process.getErrorStream(), streamErr, logOutputStream, clatch)).start();
		}

		File streamIn = StreamProcessExecutorProcessFacade.getStreamIn(pd);
		FileUtils.touch(streamIn);

		FrameOutputStream fot = new FrameOutputStream(new BufferedOutputStream(new FileOutputStream(streamIn)));

		NfurcatingOutputStream nfin = new NfurcatingOutputStream(fot);

		OutputStream os = nfin.bifurcate(process.getOutputStream());

		return new StreamProcessExecutorProcessFacade(pd, process, clatch, os, logOutputStream);
	}
}
