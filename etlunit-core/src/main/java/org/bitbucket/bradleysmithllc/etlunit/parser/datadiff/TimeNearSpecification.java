package org.bitbucket.bradleysmithllc.etlunit.parser.datadiff;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.concurrent.TimeUnit;

public class TimeNearSpecification implements NearSpecification {
	private final LocalTime referenceTime;
	private final TimeUnit timeUnit;
	private final int unitQuantity;

	private static final String timePattern = "HH:mm:ss.SSS";
	private static final String timePattern1 = "HH:mm:ss";

	private static final DateTimeFormatter timeFormatter = new DateTimeFormatterBuilder()
		.appendPattern(timePattern1)
		.appendFraction(ChronoField.MICRO_OF_SECOND, 0, 6, true)
		.toFormatter();

	protected TimeNearSpecification(
		LocalTime referenceDate,
		TimeUnit timeUnit,
		int unitQuantity
	) {
		if (referenceDate == null) {
			this.referenceTime = null;
		} else {
			this.referenceTime = referenceDate;
		}

		this.timeUnit = timeUnit;
		this.unitQuantity = unitQuantity;
	}

	public boolean isReferenceDateCurrentTime() {
		return referenceTime == null;
	}

	public LocalTime getReferenceTime() {
		if (referenceTime == null) {
				return LocalTime.now();
		} else {
			return referenceTime;
		}
	}

	public TimeUnit getTimeUnit() {
		return timeUnit;
	}

	public int getUnitQuantity() {
		return unitQuantity;
	}

	public NearDiffResult test(String sourceValueText) {
		// this string must be a timestamp
		LocalTime ldt = getLocalTime(sourceValueText);

		LocalTime referenceDate = getReferenceTime();

		// generate value range
		LocalTime oldest = earliestTime(referenceDate);
		LocalTime newest = latestTime(referenceDate);

		int diff = ldt.compareTo(oldest);

		if (diff < 0) {
			return new NearDiffResult(diff, explanation(oldest, ldt, newest));
		}

		diff = ldt.compareTo(newest);

		if (diff > 0) {
			return new NearDiffResult(diff, explanation(oldest, ldt, newest));
		}

		return NearDiffResult.matches();
	}

	private LocalTime getLocalTime(String sourceValueText) {
		return LocalTime.parse(sourceValueText, timeFormatter);
	}

	public static String explanation(LocalTime oldest, LocalTime referenceDate, LocalTime newest) {
		StringBuilder builder = new StringBuilder();

		builder.append("Comparison date ").append(timeFormatter.format(referenceDate));

		if (referenceDate.isBefore(oldest)) {
			builder.append(" is before earliest allowable time");
		} else if (referenceDate.isAfter(newest)) {
			builder.append(" is after latest allowable time");
		}

		builder
			.append(" - ")
			.append(timeFormatter.format(oldest))
			.append(" <= ref <= ")
			.append(timeFormatter.format(newest));

		return builder.toString();
	}

	public LocalTime earliestTime() {
		return earliestTime(getReferenceTime());
	}

	public LocalTime latestTime() {
		return latestTime(getReferenceTime());
	}

	public LocalTime earliestTime(LocalTime ref) {
		return relativeDate(ref, unitQuantity * -1, timeUnit);
	}

	public LocalTime latestTime(LocalTime ref) {
		return relativeDate(ref, unitQuantity, timeUnit);
	}

	private static LocalTime relativeDate(LocalTime referenceDate, int unitQuantity, TimeUnit timeUnit) {
		// convert quantity to total number of millis.
		long millis = timeUnit.toNanos(unitQuantity);

		return unitQuantity < 0 ? referenceDate.minusNanos(millis * -1) : referenceDate.plusNanos(millis);
	}

	@Override
	public int compareTo(NearSpecification nearSpecification) {
		// disallow contradictory spec types
		if (nearSpecification.getClass() != getClass()) {
			throw new UnsupportedOperationException("Cannot compare differing near specifications " + getClass().getSimpleName() + " !<> " + nearSpecification.getClass().getSimpleName());
		}

		// reference date first.
		TimeNearSpecification otherTSSpec = (TimeNearSpecification) nearSpecification;

		if (getReferenceTime().compareTo(otherTSSpec.getReferenceTime()) == 0) {
			return 0;
		}

		// not equal.  if my latest date is before the other oldest date, the result is less than (<)
		LocalTime thisEarliestDate = earliestTime();
		LocalTime otherLatestDate = otherTSSpec.latestTime();
		LocalTime thisLatestDate = latestTime();
		LocalTime otherEarliestDate = otherTSSpec.earliestTime();

		if (thisLatestDate.compareTo(otherEarliestDate) < 0) {
			return -1;
		}
		// if my newest date is after the other oldest date, result is greater than (>)
		else if (thisEarliestDate.compareTo(otherLatestDate) > 0) {
			return 1;
		}

		// times overlap.  No matter how long the overlap, times are equal
		return 0;
	}
}
