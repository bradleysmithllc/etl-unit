package org.bitbucket.bradleysmithllc.etlunit.feature.debug;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.name.Named;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.results.ResultsFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.MessageLine;

import javax.inject.Inject;
import java.util.List;

public class ConsoleStatusReporter implements StatusReporter
{
	private Log applicationLog;
	private Log userLog;
	private RuntimeSupport runtimeSupport;

	private ETLTestClass currentClass;

	private ResultsFeatureModule resultsFeatureModule;

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
	}

	@Inject
	public void setResultsFeature(ResultsFeatureModule feature)
	{
		resultsFeatureModule = feature;
	}

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log)
	{
		this.applicationLog = log;
		log.info("Console Status Reporter online");
	}

	@Inject
	public void setUserLog(@Named("userLog") Log log)
	{
		this.userLog = log;
	}

	@Override
	public void scanStarted()
	{
		applicationLog.info("scanStarted");
	}

	@Override
	public void scanCompleted()
	{
		applicationLog.info("scanCompleted");
	}

	@Override
	public void testsStarted(int numTestsSelected)
	{
		applicationLog.info("testsStarted(" + numTestsSelected + ")");
	}

	@Override
	public void testClassAccepted(ETLTestClass method)
	{
		applicationLog.info("testClassAccepted(" + method.getQualifiedName() + ")");
	}

	@Override
	public void testMethodAccepted(ETLTestMethod method)
	{
		applicationLog.info("testMethodAccepted(" + method.getName() + ")");
	}

	int numTestsBegun = 0;
	@Override
	public synchronized void testBeginning(ETLTestMethod method)
	{
		if (currentClass == null || currentClass != method.getTestClass())
		{
			logUserInfo(MessageLine.formatStandardMessage("class " + method.getTestClass().getQualifiedName()));
			currentClass = method.getTestClass();
		}

		TestResults testClassResults = resultsFeatureModule.getTestClassResults();

		String testNum = String.valueOf(numTestsBegun++ + 1);
		String testsSelected = String.valueOf(testClassResults.getNumTestsSelected());

		// pad test num to the length of tests selected
		testNum = org.apache.commons.lang3.StringUtils.leftPad(testNum, testsSelected.length(), ' ');

		String
				text =
				testNum
						+ '/'
						+ testsSelected
						+ "           ";

		logUserInfo(text.substring(0, 11) + "." + method.getName());
	}

	@Override
	public synchronized void testCompleted(ETLTestMethod method, CompletionStatus status)
	{
		applicationLog.info("testCompleted(" + method.getQualifiedName() + ", " + status + ")");

		String state = "  Passed";

		List<TestAssertionFailure> assertionFailures = status.getAssertionFailures();

		switch (status.getTestResult())
		{
			case ignored:
				for (String failure : status.getTestIgnoreReasons())
				{
						logUserInfo("      " + failure);
				}

				state = "  Ignored";
				break;

			case success:
				break;
			case failure:
				state = "  Failed";

				if (assertionFailures.size() != 0)
				{
					for (TestAssertionFailure failure : assertionFailures)
					{
						for (String failureId : failure.getFailureIds())
						{
							logUserInfo("      " + failureId);
						}
					}
				}

				break;
			case warning:
				state = "  *Passed*";
				break;
			case error:
				state = "  Caused an error";

				TestExecutionError error = status.getErrors().get(0);
				if (error != null)
				{
					applicationLog.severe("Exception during testing", error);
				}

				logUserInfo("      " + (error == null ? "" : (error.getErrorId())));
				break;
		}

		TestResults testClassResults = resultsFeatureModule.getTestClassResults();
		TestResultMetrics metrics = testClassResults.getMetrics();

		// print the pretty format
		StringBuilder sb = new StringBuilder('[');

		if (metrics.getNumberOfTestsPassed() != 0)
		{
			sb.append(" P[")
				.append(metrics.getNumberOfTestsPassed())
				.append("]");
		}

		if (metrics.getNumberOfAssertionFailures() != 0)
		{
			sb.append(" F[")
				.append(metrics.getNumberOfAssertionFailures())
				.append("]");
		}

		if (metrics.getNumberOfErrors() != 0)
		{
			sb.append(" E[")
				.append(metrics.getNumberOfErrors())
				.append("]");
		}

		if (metrics.getNumberOfWarnings() != 0)
		{
			sb.append(" W[")
				.append(metrics.getNumberOfWarnings())
				.append("]");
		}

		if (metrics.getNumberIgnored() != 0)
		{
			sb.append(" I[")
				.append(metrics.getNumberIgnored())
				.append("]");
		}

		String text = EtlUnitStringUtils.wrapToLength(sb.toString(), 80, ">>", "<<\n", EtlUnitStringUtils.RIGHT_JUSTIFIED);

		text = state + text.substring(state.length());

		logUserInfo(text);
		logUserInfo("");
	}

	private void logUserInfo(String text) {
		String message = null;

		int executorCount = runtimeSupport.getExecutorCount();
		if (executorCount > 1)
		{
			// subtract 1 from count so we don't add an extra space
			String executeCount = String.valueOf(runtimeSupport.getExecutorCount() - 1);
			String executorId = String.valueOf(runtimeSupport.getExecutorId());

			// determine the number of spaces to pad
			// pad to match
			executorId = org.apache.commons.lang3.StringUtils.leftPad(executorId, executeCount.length(), '0');

			message = executorId + ": " + text;
		}
		else
		{
			message = text;
		}

		userLog.info(message);
	}

	@Override
	public void testsCompleted()
	{
		applicationLog.info("testsCompleted()");
	}
}
