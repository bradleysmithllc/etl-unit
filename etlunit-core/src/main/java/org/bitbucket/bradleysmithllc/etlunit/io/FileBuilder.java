package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

public class FileBuilder
{
	private File path;

	public FileBuilder(File path)
	{
		this.path = path;
	}

	public FileBuilder(String path)
	{
		this.path = new File(path);
	}

	public FileBuilder()
	{
		this.path = new File("");
	}

	public FileBuilder subdir(String name)
	{
		path = new File(path, name);

		return this;
	}

	public FileBuilder subdirs(String name)
	{
		String [] paths = name.split("/");

		for (String path : paths) {
			subdir(path);
		}

		return this;
	}

	public FileBuilder name(String name)
	{
		path = new File(path, name);

		return this;
	}

	public FileBuilder mkdirs()
	{
		if (!path.exists())
		{
			if (!path.mkdirs())
			{
				// double check in case it got created by another executor
				if (!path.exists())
				{
					throw new RuntimeException("Could not make directory: " + path.getAbsolutePath());
				}
			}
		}
		else if (!path.isDirectory())
		{
			throw new RuntimeException("Path is not a directory: " + path.getAbsolutePath());
		}

		return this;
	}

	public FileBuilder mkdirsToFile()
	{
		new FileBuilder(path.getParentFile()).mkdirs();

		return this;
	}

	public File file()
	{
		return path;
	}

	/**
	 * Clears the current paths contents. Will fail if this path is not a directory
	 * @return
	 */
	public FileBuilder clearDir() {
		if (!path.isDirectory())
		{
			throw new IllegalArgumentException("Current path is not a directory");
		}

		path.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				try {
					if (pathname.isDirectory())
					{
						FileUtils.deleteDirectory(pathname);
					}
					else
					{
						FileUtils.forceDelete(pathname);
					}
				} catch (IOException e) {
				}

				return false;
			}
		});

		return this;
	}
}
