package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

public class JavaForker
{
	private Class mainClass;
	private String mainClassName;

	private int startingHeapSizeInMegabytes = -1;
	private int maximumHeapSizeInMegabytes = -1;
	private File workingDirectory = new File(".");
	private List<File> classpathEntries = new ArrayList<File>();
	private List<String> mainClassArguments = new ArrayList<String>();
	private Map<String, String> systemProperties = new HashMap<String, String>();

	private String javaRuntime = "java";
	private File output;

	private final RuntimeSupport runtimeSupport;

	public JavaForker(RuntimeSupport runtimeSupport)
	{
		this.runtimeSupport = runtimeSupport;
	}

	public void setOutput(File output)
	{
		this.output = output;
	}

	public String getMainClassName() {
		return mainClassName;
	}

	public void setMainClassName(String mainClassName) {
		if (mainClass != null)
		{
			throw new IllegalArgumentException("Cannot specify main class name when main class is already set.");
		}

		this.mainClassName = mainClassName;
	}

	public Class getMainClass()
	{
		return mainClass;
	}

	public void setMainClass(Class mainClass)
	{
		if (mainClassName != null)
		{
			throw new IllegalArgumentException("Cannot specify main class when main class name is already set.");
		}

		this.mainClass = mainClass;
	}

	public int getStartingHeapSizeInMegabytes()
	{
		return startingHeapSizeInMegabytes;
	}

	public void setStartingHeapSizeInMegabytes(int startingHeapSizeInMegabytes)
	{
		this.startingHeapSizeInMegabytes = startingHeapSizeInMegabytes;
	}

	public int getMaximumHeapSizeInMegabytes()
	{
		return maximumHeapSizeInMegabytes;
	}

	public void setMaximumHeapSizeInMegabytes(int maximumHeapSizeInMegabytes)
	{
		this.maximumHeapSizeInMegabytes = maximumHeapSizeInMegabytes;
	}

	private String getClasspath() throws IOException
	{
		List<File> cpEntries = new ArrayList<File>(classpathEntries);

		if (cpEntries.size() == 0)
		{
			// use the system class path
			String systemCp = System.getProperty("java.class.path");

			String[] pathArray = systemCp.split(File.pathSeparator);

			for (String pathEntry : pathArray)
			{
				cpEntries.add(new File(pathEntry));
			}
		}

		StringBuilder builder = new StringBuilder();
		int count = 0;
		final int totalSize = cpEntries.size();
		for (File classpathEntry : cpEntries)
		{
			builder.append(classpathEntry.getCanonicalPath());
			count++;
			if (count < totalSize)
			{
				builder.append(File.pathSeparator);
			}
		}

		return builder.toString();
	}

	public void setWorkingDirectory(File workingDirectory)
	{
		this.workingDirectory = workingDirectory;
	}

	public void addClasspathEntry(File classpathEntry)
	{
		this.classpathEntries.add(classpathEntry);
	}

	public void addSystemProperty(String key, String val)
	{
		systemProperties.put(key, val);
	}

	public void propogateSystemProperties()
	{
		Properties properties = System.getProperties();
		for (String prop : properties.stringPropertyNames())
		{
			this.systemProperties.put(prop, properties.getProperty(prop));
		}
	}

	public void addSystemProperties(Map<String, String> systemProperties)
	{
		this.systemProperties.putAll(systemProperties);
	}

	public void addArgument(String argument)
	{
		this.mainClassArguments.add(argument);
	}

	public void setJavaRuntime(String javaRuntime)
	{
		this.javaRuntime = javaRuntime;
	}

	public ProcessFacade startProcess() throws IOException
	{
		String className = mainClass != null ? mainClass.getName() : mainClassName;

		ProcessDescription pd = new ProcessDescription(this.javaRuntime);

		if (startingHeapSizeInMegabytes != -1)
		{
			pd.argument(MessageFormat.format("-Xms{0}M", String.valueOf(startingHeapSizeInMegabytes)));
		}

		if (maximumHeapSizeInMegabytes != -1)
		{
			pd.argument(MessageFormat.format("-Xmx{0}M", String.valueOf(maximumHeapSizeInMegabytes)));
		}

		addJavaExtDirs();

		String classpath = getClasspath();
		pd
			.argument("-classpath")
			.argument(classpath);

		for (Map.Entry<String, String> property : systemProperties.entrySet())
		{
			pd.argument("-D" + property.getKey() + "=" + property.getValue());
		}

		pd.argument(className)
			.workingDirectory(this.workingDirectory.getCanonicalFile())
			.output(output);

		for (String arg : mainClassArguments)
		{
			pd.argument(arg);
		}

		return runtimeSupport.execute(pd);
	}

	public void addJavaExtDirs() {
		String extDirs = System.getProperty("java.ext.dirs");

		if (extDirs != null) {
			// convert this into an absolute path so that it resolves properly
			// split the dirs by separator and re-add with absolute paths

			String[] paths = extDirs.split(File.pathSeparator);

			StringBuilder stb = new StringBuilder();

			int index = 0;

			for (String path : paths) {
				if (index != 0)
				{
					stb.append(File.pathSeparator);
				}

				stb.append(new File(path).getAbsolutePath());
				index++;
			}

			addSystemProperty("java.ext.dirs", stb.toString());
		}
	}
}
