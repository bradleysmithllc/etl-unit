package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

public interface ETLTestMethod extends ETLTestAnnotated, ETLTestDescribed
{
	enum method_type
	{
		before_class,
		after_class,
		before_test,
		after_test,
		test
	}

	String getName();

	String getQualifiedName();

	boolean requiresPurge();

	method_type getMethodType();

	ETLTestClass getTestClass();

	List<ETLTestOperation> getOperations();
}
