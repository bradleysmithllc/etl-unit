package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DataFileSchemaDef {
	enum inherit_columns {all, declared}

	String rowDelimiter = null;
	String columnDelimiter = null;
	String nullToken = null;
	String inheritSchema = null;
	inherit_columns inheritColumns = inherit_columns.all;
	Boolean escapeNonPrintable = null;
	Integer lineLength = null;
	DataFileSchema.format_type formatType = null;

	final List<DataFileSchema.Column> columns = new ArrayList<>();
	final List<String> columnNames = new ArrayList<>();

	final List<DataFileSchema.Column> orderColumns = new ArrayList<>();
	final List<String> orderColumnNames = new ArrayList<>();

	final List<DataFileSchema.Column> keyColumns = new ArrayList<>();
	final List<String> keyColumnNames = new ArrayList<>();

	public DataFileSchemaDef(JsonNode rootNode, DataFileManager dataFileManager) {
		JsonNode flatFileNode = rootNode.get("flat-file");

		if (flatFileNode.has("row-delimiter")) {
			rowDelimiter = flatFileNode.get("row-delimiter").asText();
		}

		if (flatFileNode.has("$inherit-from")) {
			inheritSchema = flatFileNode.get("$inherit-from").asText();
		}

		if (flatFileNode.has("$inherit")) {
			JsonNode inheritNode = flatFileNode.get("$inherit");

			if (!inheritNode.isNull()) {

				if (inheritNode.path("from").isMissingNode()) {
					throw new IllegalArgumentException("$inherit is missing from");
				}

				JsonNode fromNode = inheritNode.get("from");
				if (!fromNode.isNull()) {
					inheritSchema = fromNode.asText();
				}

				// check for column
				JsonNode columnsNode = inheritNode.at("/columns");

				if (!columnsNode.isMissingNode() && !columnsNode.isNull()) {
					inheritColumns = inherit_columns.valueOf(columnsNode.asText());
				}
			}
		}

		if (flatFileNode.has("format-type")) {
			String form = flatFileNode.get("format-type").asText();

			formatType = DataFileSchema.format_type.valueOf(form);
		}

		if (flatFileNode.has("column-delimiter")) {
			JsonNode jsonNodes = flatFileNode.get("column-delimiter");

			if (!jsonNodes.isNull()) {
				columnDelimiter = jsonNodes.asText();
			} else {
				columnDelimiter = null;
			}
		} else {
			columnDelimiter = null;
		}

		if (flatFileNode.has("null-token")) {
			JsonNode nDem = flatFileNode.get("null-token");
			if (!nDem.isNull()) {
				withNullToken(nDem.asText());
			}
		}

		if (flatFileNode.has("escape-non-printable")) {
			JsonNode nDem = flatFileNode.get("escape-non-printable");
			escapeNonPrintable = nDem.asBoolean();
		}

		if (flatFileNode.has("columns")) {
			ArrayNode anode = (ArrayNode) flatFileNode.get("columns");

			for (int i = 0; i < anode.size(); i++) {
				JsonNode node = anode.get(i);

				SchemaColumn schemaColumn = new SchemaColumn(node, dataFileManager);

				addColumn(schemaColumn);
			}
		}

		sortColumns();

		ArrayNode anode = (ArrayNode) flatFileNode.get("orderBy");

		if (anode != null) {
			for (int i = 0; i < anode.size(); i++) {
				JsonNode node = anode.get(i);

				// this will fail if the column does not exist
				addOrderColumn(node.asText());
			}
		} else {
			// default is to order by all columns
			orderColumns.addAll(columns);
			orderColumnNames.addAll(columnNames);
		}

		anode = (ArrayNode) flatFileNode.get("primaryKey");

		if (anode != null) {
			for (int i = 0; i < anode.size(); i++) {
				JsonNode node = anode.get(i);

				// this will fail if the column does not exist
				addKeyColumn(node.asText());
			}
		} else {
			// default is no key . . .
		}
	}

	private void sortColumns() {
		// sort columns by ordinal first, then inferred ordinal
		columnNames.clear();
		Collections.sort(columns, Comparator.comparing(DataFileSchema.Column::actualOrdinal));

		// update names
		for (DataFileSchema.Column column : columns) {
			columnNames.add(column.getId());
		}
	}

	public DataFileSchemaDef() {
	}

	public void processDefaults() {
		if (nullToken == null) {
			nullToken = "null";
		}

		if (lineLength == null) {
			lineLength = new Integer(-1);
		}

		if (escapeNonPrintable == null) {
			escapeNonPrintable = Boolean.TRUE;
		}
	}

	public List<String> orderColumnNames() {
		if (orderColumnNames.size() != 0) {
			return orderColumnNames;
		}

		return columnNames;
	}

	public List<DataFileSchema.Column> orderColumns() {
		if (orderColumns.size() != 0) {
			return orderColumns;
		}

		return columns;
	}

	public DataFileSchemaDef withNullToken(String nullToken) {
		this.nullToken = nullToken == null ? "null" : nullToken;
		return this;
	}

	public void addOrderColumn(String name) {
		if (orderColumnNames.contains(name)) {
			throw new IllegalArgumentException("Column already added to order clause: " + name);
		}

		DataFileSchema.Column col = getColumn(name);

		// verify that it is not a diff column
		if (col.diffType() != DataFileSchema.Column.diff_type.none) {
			throw new IllegalArgumentException("Date diff columns may not be used in order by clauses " + col.getId());
		}

		orderColumns.add(col);
		orderColumnNames.add(col.getId());
	}

	public void addKeyColumn(String name) {
		if (keyColumnNames.contains(name)) {
			throw new IllegalArgumentException("Column already added to primary key: " + name);
		}

		DataFileSchema.Column col = getColumn(name);

		keyColumns.add(col);
		keyColumnNames.add(col.getId());
	}

	public DataFileSchema.Column getColumn(String name) {
		for (DataFileSchema.Column sch : columns) {
			if (sch.getId().equalsIgnoreCase(name)) {
				return sch;
			}
		}

		throw new IllegalArgumentException("Column [" + name + "] not found");
	}

	public void addColumn(DataFileSchema.Column column) {
		if (columnNames.contains(column.getId())) {
			throw new IllegalArgumentException("Column already added: " + column.getId());
		}

		// validate that if this is a fixed file, lengths are provided, and not provided for delimited files
		if (formatType == DataFileSchema.format_type.fixed) {
			if (column.getLength() == -1) {
				throw new IllegalArgumentException("Columns added to flat files must have a length provided");
			} else {
				int size = columns.size();

				if (size == 0) {
					column.setOffset(0);
					lineLength = 0;
				} else {
					DataFileSchema.Column lastCol = columns.get(size - 1);
					column.setOffset(lastCol.getOffset() + lastCol.getLength());
				}

				lineLength += column.getLength();
			}
		}

		// scale must be less than or equal to length
		if (column.getLength() != -1 && (column.getScale() > column.getLength())) {
			throw new IllegalArgumentException("Column scale cannot exceed length: " + column.getId());
		}

		column.setInferredOrdinal(columns.size());

		columns.add(column);
		columnNames.add(column.getId());
	}

	public String toJsonString() {
		try {
			StringWriter out = new StringWriter();
			JsonWriter jwriter = new JsonWriter(out);

			jwriter.setIndent("\t");

			jwriter.beginObject()
				.name("flat-file")
				.beginObject()
				.name("format-type")
				.value(formatType.toString())
				.name("row-delimiter")
				.value(rowDelimiter);

			if (formatType != DataFileSchema.format_type.fixed) {
				jwriter = jwriter.name("column-delimiter")
					.value(columnDelimiter);
			}

			jwriter = jwriter.name("null-token");
			if (nullToken == null) {
				jwriter = jwriter.nullValue();
			} else {
				jwriter = jwriter.value(nullToken);
			}

			jwriter = jwriter.name("columns")
				.beginArray();

			for (DataFileSchema.Column col : columns) {
				jwriter =
					jwriter.beginObject()
						.name("id")
						.value(col.getId())
						.name("length")
						.value(col.getLength());
				if (col.getScale() != -1) {
					jwriter = jwriter.name("scale")
						.value(col.getScale());
				}

				jwriter = jwriter.name("basic-type")
					.value(col.getBasicType().name());

				if (col.hasType()) {
					jwriter.name("type").value(col.getType());
				}

				if (col.ordinal() != Integer.MIN_VALUE) {
					jwriter.name("ordinal").value(col.ordinal());
				}

				if (col.diffType() != null && col.diffType() != DataFileSchema.Column.diff_type.none) {
					jwriter.name("diff-type").value(col.diffType().name());
				}

				if (col.hasTypeAnnotation()) {
					jwriter.name("type-annotation").value(col.getTypeAnnotation());
				}

				// simple default value
				if (col.getDefaultValue() != null) {
					jwriter.name("default-value")
						.value(col.getDefaultValue().toString());
				}

				// default sequence
				if (col.getDefaultSequence() != null) {
					jwriter.name("default-value")
						.beginObject();

					SchemaColumn.DefaultSequence colSeq = col.getDefaultSequence();

					if (colSeq.hasInitialValue()) {
						jwriter.name("initial-value")
							.value(colSeq.getInitialValue());
					}

					if (colSeq.hasIncrement()) {
						jwriter.name("increment")
							.value(colSeq.getIncrement());
					}

					if (colSeq.hasModulus()) {
						jwriter.name("modulus")
							.value(colSeq.getModulus());
					}

					if (colSeq.hasBaseOffset()) {
						jwriter.name("base-offset")
							.value(colSeq.getBaseOffset());
					}

					if (colSeq.hasExpression()) {
						jwriter.name("expression")
							.value(colSeq.getExpression());
					}

					jwriter.endObject();
				}

				jwriter.endObject();
			}

			jwriter = jwriter.endArray();

			if (keyColumnNames.size() != 0) {
				jwriter = jwriter.name("primaryKey").beginArray();
				for (String col : keyColumnNames) {
					jwriter.value(col);
				}

				jwriter.endArray();
			}

			if (orderColumnNames().size() != 0) {
				jwriter = jwriter.name("orderBy").beginArray();
				for (String col : orderColumnNames()) {
					jwriter.value(col);
				}

				jwriter.endArray();
			}

			jwriter = jwriter.endObject().endObject();

			jwriter.close();

			// load into Jackson and pretty-print
			return out.toString();//JsonUtils.printJson(JsonLoader.fromString(out.toString()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void clearOrderColumns() {
		orderColumns.clear();
		orderColumnNames.clear();
	}

	public DataFileSchemaDef extend(DataFileSchemaDef defParent) {
		DataFileSchemaDef newDef = new DataFileSchemaDef();

		// basic options
		newDef.rowDelimiter = ObjectUtils.firstNotNull(rowDelimiter, defParent.rowDelimiter);
		newDef.columnDelimiter = ObjectUtils.firstNotNull(columnDelimiter, defParent.columnDelimiter);
		newDef.nullToken = ObjectUtils.firstNotNull(nullToken, defParent.nullToken);
		newDef.inheritSchema = ObjectUtils.firstNotNull(inheritSchema, defParent.inheritSchema);
		newDef.escapeNonPrintable = ObjectUtils.firstNotNull(escapeNonPrintable, defParent.escapeNonPrintable);
		newDef.lineLength = ObjectUtils.firstNotNull(lineLength, defParent.lineLength);
		newDef.formatType = ObjectUtils.firstNotNull(formatType, defParent.formatType);

		// columns
		// add all parent columns
		for (DataFileSchema.Column column : defParent.columns) {
			newDef.addColumn(column.clone());
		}

		// process child columns
		// every new column gets appended, every duplicate column overwrites attributes of the parent
		for (DataFileSchema.Column column : columns) {
			try {
				SchemaColumn parentColumn = (SchemaColumn) newDef.getColumn(column.getId());
				// update column definition in place since the column def has already been cloned
			} catch (IllegalArgumentException exc) {
				newDef.addColumn(column);
			}
		}

		if (keyColumnNames.size() == 0) {
			for (String keyColumnName : defParent.keyColumnNames) {
				newDef.addKeyColumn(keyColumnName);
			}
		}

		// order columns.  Use all child or all parent
		if (orderColumnNames.size() == 0) {
			for (String orderColumnName : defParent.orderColumnNames) {
				newDef.addOrderColumn(orderColumnName);
			}
		}

		return newDef;
	}
}
