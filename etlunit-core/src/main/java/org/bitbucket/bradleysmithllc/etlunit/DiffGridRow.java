package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface DiffGridRow
{
	/**
	 * For changed rows only.  For source added or target added
	 * disregard this method.
	 *
	 * @param col
	 */
	void setColumnName(String col);

	/**
	 * Sets the order key for this row
	 *
	 * @param key
	 */
	void setOrderKey(String key);

	/**
	 * Source value, or null if the target row is added
	 *
	 * @param value
	 */
	void setSourceValue(String value);

	/**
	 * Target value (always different), unless
	 * source row is unmatched.
	 *
	 * @param value
	 */
	void setTargetValue(String value);

	void done();
}
