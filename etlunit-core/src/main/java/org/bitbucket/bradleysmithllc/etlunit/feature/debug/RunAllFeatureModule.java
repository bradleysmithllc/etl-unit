package org.bitbucket.bradleysmithllc.etlunit.feature.debug;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.NullClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

@FeatureModule
public class RunAllFeatureModule extends AbstractFeature
{

	private final ClassDirector nullClassDirector = new NullClassDirector()
	{
		@Override
		public response_code accept(ETLTestClass cl)
		{
			return response_code.accept;
		}

		@Override
		public response_code accept(ETLTestOperation op)
		{
			return response_code.accept;
		}

		@Override
		public response_code accept(ETLTestMethod mt)
		{
			return response_code.accept;
		}
	};

	@Override
	public long getPriorityLevel() {
		return 1L;
	}

	@Override
	public ClassDirector getDirector()
	{
		return nullClassDirector;
	}

	public String getFeatureName()
	{
		return "run-all-director";
	}

	@Override
	public boolean requiredForSimulation() {
		return true;
	}
}
