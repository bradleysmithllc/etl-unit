package org.bitbucket.bradleysmithllc.etlunit.feature.tags;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.stream.JsonWriter;
import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Module;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.NullStatusReporterImpl;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.TestClasses;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;

import javax.inject.Inject;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@FeatureModule
public class TagsFeatureModule extends AbstractFeature
{
	public static final String ETLUNIT_TAG_NAME = "etlunit.tagName";
	private TagRuntimeSupport tagRuntimeSupport;
	private final StatusReporter reporter = new ResultsReporter();
	private RuntimeSupport runtimeSupport;
	private File tag;
	private TestClasses allClasses;
	private TestClasses successClasses;
	private TestClasses failClasses;
	private TestClasses errorClasses;
	private TestClasses failErrorClasses;
	private String overrideName = System.getProperty(ETLUNIT_TAG_NAME);

	@Override
	protected Injector preCreateSub(Injector inj) {
		return inj.createChildInjector(new Module() {
			@Override
			public void configure(Binder binder) {
				binder.bind(TagRuntimeSupport.class).toInstance(tagRuntimeSupport);
			}
		});
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport rs) throws IOException {
		runtimeSupport = rs;
		tag = runtimeSupport.getGlobalReportDirectory("tag").getCanonicalFile();
		tagRuntimeSupport = new TagRuntimeSupportImpl(tag);
	}

	private final class ResultsReporter extends NullStatusReporterImpl
	{
		@Override
		public void testsCompleted() {
			// write each log to it's corresponding log file
			write(allClasses, "all");
			write(successClasses, "success");
			write(failClasses, "fail");
			write(errorClasses, "error");
			write(failErrorClasses, "failError");
		}

		@Override
		public void testsStarted(int numTestsSelected) {
			// create 5 different test classes - all, success, fail, error, failError
			allClasses = new TestClasses();
			successClasses = new TestClasses();
			failClasses = new TestClasses();
			errorClasses = new TestClasses();
			failErrorClasses = new TestClasses();
		}

		@Override
		public void testCompleted(ETLTestMethod method, CompletionStatus status)
		{
			allClasses.addMethod(method);
			switch (status.getTestResult())
			{
				case warning:
				case success:
					successClasses.addMethod(method);
					break;
				case failure:
					failClasses.addMethod(method);
					failErrorClasses.addMethod(method);
					break;
				case error:
					errorClasses.addMethod(method);
					failErrorClasses.addMethod(method);
					break;
			}
		}
	}

	private void write(TestClasses failErrorClasses, String failError) {
		try {
			File tagFile = new File(tag, ObjectUtils.firstNotNull(overrideName, runtimeSupport.getRunIdentifier()) + "-" + failError + ".json");
			JsonWriter jwriter = new JsonWriter(new BufferedWriter(new FileWriter(tagFile)));

			try
			{
				jwriter.beginObject();
				failErrorClasses.toJson(jwriter);
				jwriter.endObject();
			}
			finally
			{
				jwriter.close();
			}

			// each test type gets copied into the default 'last' tag
			FileUtils.copyFile(tagFile, new File(tag, "last-" + failError + ".json"));
		} catch (IOException e) {
			runtimeSupport.getApplicationLog().severe("Error writing json tag log", e);
		}
	}

	@Override
	public StatusReporter getStatusReporter()
	{
		return reporter;
	}

	public String getFeatureName()
	{
		return "tags";
	}

	@Override
	public long getPriorityLevel() {
		return -100L;
	}

	@Override
	public boolean requiredForSimulation() {
		return true;
	}
}
