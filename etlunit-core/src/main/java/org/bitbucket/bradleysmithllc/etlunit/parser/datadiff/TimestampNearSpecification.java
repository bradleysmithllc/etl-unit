package org.bitbucket.bradleysmithllc.etlunit.parser.datadiff;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.concurrent.TimeUnit;

public class TimestampNearSpecification implements NearSpecification {
	private final ZonedDateTime referenceDate;
	private final TimeUnit timeUnit;
	private final int unitQuantity;

	enum dateType {
		date,
		datetime
	}

	private final dateType type;

	private static final String datePattern = "yyyy-MM-dd";
	private static final String timePattern = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String timePattern1 = "yyyy-MM-dd HH:mm:ss";

	private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(datePattern);
	private static final DateTimeFormatter timeFormatter = new DateTimeFormatterBuilder()
			.appendPattern(timePattern1)
			.appendFraction(ChronoField.MICRO_OF_SECOND, 0, 6, true)
					.toFormatter();

	public TimestampNearSpecification(
			ZonedDateTime referenceDate,
			TimeUnit timeUnit,
			int unitQuantity
	) {
		this(referenceDate, timeUnit, unitQuantity, dateType.datetime);
	}

	protected TimestampNearSpecification(
			ZonedDateTime referenceDate,
			TimeUnit timeUnit,
			int unitQuantity,
			dateType typ
	) {
		if (referenceDate == null) {
			this.referenceDate = null;
		} else {
			this.referenceDate = referenceDate;
		}

		this.timeUnit = timeUnit;
		this.unitQuantity = unitQuantity;

		this.type = typ;
	}

	public boolean isReferenceDateCurrentDate() {
		return referenceDate == null;
	}

	public ZonedDateTime getReferenceDate() {
		if (referenceDate == null) {
			if (type == dateType.datetime) {
				ZonedDateTime now = ZonedDateTime.now();
				LocalDateTime localDateTime = now.toLocalDateTime();
				ZonedDateTime utc = localDateTime.atZone(ZoneId.of("UTC"));

				return utc;
			} else {
				LocalDate now  = LocalDate.now();
				ZonedDateTime utc = now.atStartOfDay().atZone(ZoneId.of("UTC"));

				return utc;
			}
		} else {
			return referenceDate;
		}
	}

	public TimeUnit getTimeUnit() {
		return timeUnit;
	}

	public int getUnitQuantity() {
		return unitQuantity;
	}

	public NearDiffResult test(String sourceValueText) {
		// this string must be a timestamp
			LocalDateTime ldt = getLocalDateTime(sourceValueText);

			ZonedDateTime sourceValue = ldt.atZone(ZoneId.of("UTC"));

			ZonedDateTime referenceDate = getReferenceDate();

			// generate value range
			ZonedDateTime oldest = earliestDate(referenceDate);
			ZonedDateTime newest = latestDate(referenceDate);

			int diff = sourceValue.compareTo(oldest);

			if (diff < 0) {
				return new NearDiffResult(diff, explanation(oldest, sourceValue, newest, datePattern));
			}

			diff = sourceValue.compareTo(newest);

			if (diff > 0) {
				return new NearDiffResult(diff, explanation(oldest, sourceValue, newest, datePattern));
			}

			return NearDiffResult.matches();
	}

	private LocalDateTime getLocalDateTime(String sourceValueText) {
		switch (type) {
			case date:
				return LocalDate.parse(sourceValueText, dateFormatter).atStartOfDay();
			case datetime:
			default:
				return LocalDateTime.parse(sourceValueText, timeFormatter);
		}
	}

	public static String explanation(ZonedDateTime oldest, ZonedDateTime referenceDate, ZonedDateTime newest, String datePattern) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timePattern).withZone(ZoneId.of("UTC"));

		StringBuilder builder = new StringBuilder();

		builder.append("Comparison date ").append(formatter.format(referenceDate));

		if (referenceDate.isBefore(oldest)) {
			builder.append(" is before earliest allowable date");
		} else if (referenceDate.isAfter(newest)) {
			builder.append(" is after latest allowable date");
		}

		builder
				.append(" - ")
				.append(formatter.format(oldest))
				.append(" <= ref <= ")
				.append(formatter.format(newest));

		return builder.toString();
	}

	public ZonedDateTime earliestDate() {
		return earliestDate(getReferenceDate());
	}

	public ZonedDateTime latestDate() {
		return latestDate(getReferenceDate());
	}

	public ZonedDateTime earliestDate(ZonedDateTime ref) {
		return relativeDate(ref, unitQuantity * -1, timeUnit);
	}

	public ZonedDateTime latestDate(ZonedDateTime ref) {
		return relativeDate(ref, unitQuantity, timeUnit);
	}

	private static ZonedDateTime relativeDate(ZonedDateTime referenceDate, int unitQuantity, TimeUnit timeUnit) {
		// convert quantity to total number of millis.
		long millis = timeUnit.toNanos(unitQuantity);

		return unitQuantity < 0 ? referenceDate.minusNanos(millis * -1) : referenceDate.plusNanos(millis);
	}

	@Override
	public int compareTo(NearSpecification nearSpecification) {
		// disallow contradictory spec types
		if (nearSpecification.getClass() != getClass()) {
			throw new UnsupportedOperationException("Cannot compare differing near specifications " + getClass().getSimpleName() + " !<> " + nearSpecification.getClass().getSimpleName());
		}

		// reference date first.
		TimestampNearSpecification otherTSSpec = (TimestampNearSpecification) nearSpecification;

		if (getReferenceDate().compareTo(otherTSSpec.getReferenceDate()) == 0) {
			return 0;
		}

		// not equal.  if my latest date is before the other oldest date, the result is less than (<)
		ZonedDateTime thisEarliestDate = earliestDate();
		ZonedDateTime otherLatestDate = otherTSSpec.latestDate();
		ZonedDateTime thisLatestDate = latestDate();
		ZonedDateTime otherEarliestDate = otherTSSpec.earliestDate();

		if (thisLatestDate.compareTo(otherEarliestDate) < 0) {
			return -1;
		}
		// if my newest date is after the other oldest date, result is greater than (>)
		else if (thisEarliestDate.compareTo(otherLatestDate) > 0) {
				return 1;
		}

		// times overlap.  No matter how long the overlap, times are equal
		return 0;
	}
}
