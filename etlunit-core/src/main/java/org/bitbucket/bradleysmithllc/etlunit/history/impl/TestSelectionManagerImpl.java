package org.bitbucket.bradleysmithllc.etlunit.history.impl;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlunit.history.TestSelection;
import org.bitbucket.bradleysmithllc.etlunit.history.TestSelectionManager;

import java.util.List;

public class TestSelectionManagerImpl implements TestSelectionManager
{
	@Override
	public JsonNode serialize() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deserialize(JsonNode node) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<TestSelection> getRunHistory() {
		throw new UnsupportedOperationException();
	}

	@Override
	public TestSelection getSelectById(String id) {
		throw new UnsupportedOperationException();
	}
}
