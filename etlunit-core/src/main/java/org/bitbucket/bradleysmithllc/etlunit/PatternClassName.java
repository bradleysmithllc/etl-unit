package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;

import java.util.regex.Pattern;

public class PatternClassName {
	private final String classQName;
	private final String packageName;
	private final String className;

	private final Pattern classQPattern;
	private final Pattern packagePattern;
	private final Pattern classPattern;

	public PatternClassName(String classQName, String packageName, String className) {
		this.classQName = classQName;
		this.packageName = packageName;
		this.className = className;

		if (classQName != null)
		{
			classQPattern = Pattern.compile(classQName, Pattern.CASE_INSENSITIVE);
		}
		else
		{
			classQPattern = null;
		}

		if (packageName != null)
		{
			packagePattern = Pattern.compile(packageName, Pattern.CASE_INSENSITIVE);
		}
		else
		{
			packagePattern = null;
		}

		if (className != null)
		{
			classPattern = Pattern.compile(className, Pattern.CASE_INSENSITIVE);
		}
		else
		{
			classPattern = null;
		}
	}

	public boolean matches(ETLTestClass cls)
	{
		String qname = "";

		if (cls.getPackage().isDefaultPackage())
		{
			qname = cls.getName();
		}
		else
		{
			qname = cls.getQualifiedName();
		}

		// check qname first

		if (classQPattern != null)
		{
			return classQPattern.matcher(qname).find();
		}

		boolean packMatches = true;
		boolean classMatches = true;

		// check package
		if (packagePattern != null)
		{
			packMatches = packagePattern.matcher(cls.getPackage().getPackageName()).find();
		}

		// class
		if (classPattern != null)
		{
			classMatches = classPattern.matcher(cls.getName()).find();
		}

		// pack and class both must be true
		return packMatches && classMatches;
	}
}
