package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import java.io.IOException;
import java.util.*;

public class TestClasses implements JsonSerializable
{
	private final Map<String, TestClassReference> classes = new HashMap<String, TestClassReference>();
	private final List<TestClassReference> classList = new ArrayList<TestClassReference>();
	private int testCount = 0;

	public synchronized void addMethod(ETLTestMethod method)
	{
		addMethod(method.getTestClass().getQualifiedName(), method.getName());
	}

	public synchronized void addMethod(String classQN, String method)
	{
		String qualifiedName = classQN;
		TestClassReference classRef = classes.get(qualifiedName);

		if (classRef == null)
		{
			classRef = new TestClassReference(qualifiedName);
			classes.put(qualifiedName, classRef);
			classList.add(classRef);
		}

		testCount++;
		classRef.addMethod(method);
	}

	public int getTestCount()
	{
		return testCount;
	}

	public List<TestClassReference> getTestClassReferences()
	{
		return classList;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("classes");
		writer.beginObject();

		for (TestClassReference classRef : classList)
		{
			writer.name(classRef.getTestClassQualifiedName());
			writer.beginObject();
			classRef.toJson(writer);
			writer.endObject();
		}

		writer.endObject();
	}

	@Override
	public void fromJson(JsonNode node) {
		JsonNode clnode = node.get("classes");

		Iterator<Map.Entry<String,JsonNode>> clfields = clnode.fields();

		while (clfields.hasNext())
		{
			Map.Entry<String, JsonNode> clfield = clfields.next();

			TestClassReference tcref = new TestClassReference(clfield.getKey());
			tcref.fromJson(clfield.getValue());
			classes.put(tcref.getTestClassQualifiedName(), tcref);
			classList.add(tcref);
		}
	}

	public boolean acceptTestMethod(ETLTestMethod mt) {
		TestClassReference cls = classes.get(mt.getTestClass().getQualifiedName());

		if (cls == null)
		{
			return false;
		}

		return cls.acceptTestMethod(mt.getName());
	}

	public boolean acceptTestClass(ETLTestClass cl) {
		return classes.containsKey(cl.getQualifiedName());
	}
}
