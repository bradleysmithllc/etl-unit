package org.bitbucket.bradleysmithllc.etlunit.feature.logging;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import java.io.File;
import java.io.IOException;

public class LogManagerRuntimeSupportImpl implements LogManagerRuntimeSupport
{
	private RuntimeSupport runtimeSupport;

	public LogManagerRuntimeSupportImpl(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	@Override
	public File getApplicationLogFile() {
		return new FileBuilder(getLogDirectory()).mkdirs().name("application.log").file();
	}

	@Override
	public File getLogFile(ETLTestClass _class) {
		return getLogFile(_class, runtimeSupport.getExecutorId());
	}

	private File getLogFile(ETLTestClass _class, int executor) {
		return new FileBuilder(getLogDirectory()).mkdirs().name(_class.getQualifiedName() + (executor == -1 ? "" : ("_" + executor)) + ".txt").file();
	}

	@Override
	public File getLogFile(ETLTestMethod _method) {
		return getLogFile(_method, runtimeSupport.getExecutorId());
	}

	private File getLogFile(ETLTestMethod _method, int executor) {
		return new FileBuilder(getLogDirectory()).mkdirs().name(_method.getQualifiedName() + (executor == -1 ? "" : ("_" + executor)) + ".txt").file();
	}

	@Override
	public File getAggregatedLogFile(ETLTestClass _class) {
		File combined = getLogFile(_class, -1);

		// blank it if it exists
		try {
			FileUtils.write(combined, "");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		int executorCount = runtimeSupport.getExecutorCount();
		for (int i = 0; i < executorCount; i++)
		{
			try {
				// aggregate each file in turn into the result file
				File logFile = getLogFile(_class, i);
				if (logFile.exists())
				{
					FileUtils.write(combined, "//////////////// executor log[" + i + "] \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n", true);
					FileUtils.write(combined, FileUtils.readFileToString(logFile), true);
					FileUtils.write(combined, "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ executor log[" + i + "] ////////////////\n", true);
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		return combined;
	}

	@Override
	public File getAggregatedLogFile(ETLTestMethod _method) {
		File combined = getLogFile(_method, -1);

		// blank it if it exists
		try {
			FileUtils.write(combined, "");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		int executorCount = runtimeSupport.getExecutorCount();
		for (int i = 0; i < executorCount; i++)
		{
			try {
				// aggregate each file in turn into the result file
				File logFile = getLogFile(_method, i);
				if (logFile.exists())
				{
					FileUtils.write(combined, "//////////////// executor log[" + i + "] \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n", true);
					FileUtils.write(combined, FileUtils.readFileToString(logFile), true);
					FileUtils.write(combined, "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ executor log[" + i + "] ////////////////\n", true);
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		return combined;
	}

	@Override
	public File getLogDirectory()
	{
		return getLogDirectory(0);
	}

	@Override
	public File getLogDirectory(int executor)
	{
		return new FileBuilder(runtimeSupport.getReportDirectory("log")).mkdirs().file();
	}
}
