package org.bitbucket.bradleysmithllc.etlunit.util.jdbc;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class MetadataTable extends MetadataSchema {
	private final String tableName;

	private String tableType;
	private String remarks;
	private String selfReferencingColName;
	private String refGeneration;

	private String typeCatalog;
	private String typeSchema;
	private String typeName;

	public MetadataTable(String catalogName, String schemaName, String tableName) {
		super(catalogName, schemaName);

		this.tableName = tableName;
	}

	public String tableName() {
		return tableName;
	}

	public String tableType() {
		return tableType;
	}

	public MetadataTable withTableType(String tableType) {
		this.tableType = tableType;
		return this;
	}

	public String remarks() {
		return remarks;
	}

	public MetadataTable withRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}

	public String selfReferencingColName() {
		return selfReferencingColName;
	}

	public MetadataTable withSelfReferencingColName(String selfReferencingColName) {
		this.selfReferencingColName = selfReferencingColName;
		return this;
	}

	public String refGeneration() {
		return refGeneration;
	}

	public MetadataTable withRefGeneration(String refGeneration) {
		this.refGeneration = refGeneration;
		return this;
	}

	public String typeCatalog() {
		return typeCatalog;
	}

	public MetadataTable withTypeCatalog(String typeCatalog) {
		this.typeCatalog = typeCatalog;
		return this;
	}

	public String typeSchema() {
		return typeSchema;
	}

	public MetadataTable withTypeSchema(String typeSchema) {
		this.typeSchema = typeSchema;
		return this;
	}

	public String typeName() {
		return typeName;
	}

	public MetadataTable withTypeName(String typeName) {
		this.typeName = typeName;
		return this;
	}
}
