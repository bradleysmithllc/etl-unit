package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.CommandLine;

public interface CommandLineSwitches
{
	CommandLine.Switch
			forceDbUpdate =
			new CommandLine.Switch("force-db-update", "Forces rebuild of the unit test database regardless of age.");
	CommandLine.Switch help = new CommandLine.Switch("help", "Prints this help text.");
	CommandLine.Switch
			hostProperties =
			new CommandLine.Switch("host-properties", "Specifies the name of the properties file to use.");
	CommandLine.Switch
			interactive =
			new CommandLine.Switch("interactive",
					"For each test case selected for running, the user will be prompted for each test case to run.");
	CommandLine.Switch
			interactiveOperations =
			new CommandLine.Switch("interactive-operation",
					"For each test case selected for running, the user will be prompted for operation to run.");
	CommandLine.Switch
			listFailed =
			new CommandLine.Switch("list-failed", "Lists all tests which failed or caused an error in the last run.");
	CommandLine.Switch listTests = new CommandLine.Switch("list-test", "Lists all tests which executed in the last run.");
	CommandLine.Switch resetHistory = new CommandLine.Switch("reset-history", "Deletes the history file at startup.");
	CommandLine.Switch
			runFailed =
			new CommandLine.Switch("run-failed", "Runs all tests which failed or caused an error in the last run.");
	CommandLine.Switch
			simulate =
			new CommandLine.Switch("simulate", "Lists all tests which match the criteria, but does not execute them.");
	CommandLine.Switch
			yesToAll =
			new CommandLine.Switch("yes-to-all",
					"If any option asks for confirmation interactively, supply yes without pausing.");
	CommandLine.Switch
			updateBcpData =
			new CommandLine.Switch("update-bcp-data",
					"Exports data from the database into a bcp masterdata file.  Legal SOURCE values include: master, source, target.  The default is master.  SOURCE determines which schema the data is bcp'd FROM.  For source, TST_SRC is used, for target TST_TGT is used, and for master (the default) TST_MST is used.  In all cases the data ends up in data\\SrcData\\TST_MST.TABLE_NAME.Table.bcp.  The program will prompt for each table to export so it will be determined at run time which tables to export.");
	CommandLine.Switch
			updateFileData =
			new CommandLine.Switch("update-file-data",
					"Copies data from the remote unit test environment to the local working copy.  Legal values are: lookup, source and target.  Target is the default.");
	CommandLine.Switch
			updateInformatica =
			new CommandLine.Switch("update-etlunit",
					"Exports workflow from etlunit into local svn.  All named tests are interrogated and every executeWorkflow statement is exported.");
	CommandLine.Switch
			updateInformaticaAll =
			new CommandLine.Switch("update-etlunit-all", "Exports all workflows from etlunit into local svn.");
	CommandLine.Switch
			bcpNoFail =
			new CommandLine.Switch("bcp-no-fail",
					"Runs the test, but does not fail if the bcp data is not found or does not load. Useful for bootstraping test tables.");
	CommandLine.Switch
			deleteInformaticaConnection =
			new CommandLine.Switch("delete-etlunit-connection",
					"Specifies whether infaunit should delete the connection it creates in etlunit when the tests are done.  Default is true.");
	CommandLine.Switch
			createInformaticaConnection =
			new CommandLine.Switch("create-etlunit-connection",
					"Specifies whether infaunit should create the connection in etlunit before tests are run.  Default is true.");
	CommandLine.Switch
			createInformaticaFolder =
			new CommandLine.Switch("create-etlunit-folder",
					"Specifies whether infaunit should create the folder in etlunit before tests are run.  Default is true.");
	CommandLine.Switch
			deleteInformaticaFolder =
			new CommandLine.Switch("delete-etlunit-folder",
					"Specifies whether infaunit should delete the folder in etlunit before tests are run.  Default is true.");
	CommandLine.Switch
			loadInformaticaMapping =
			new CommandLine.Switch("load-etlunit-workflow",
					"Specifies whether infaunit should load the workflow in etlunit before tests are run.  Default is true.");
	CommandLine.Switch
			debuggable =
			new CommandLine.Switch("debuggable",
					"Tells infaunit to leave the relational connection in tact and copy the prm file to the global parmlib directory.  Default is false");
	CommandLine.Switch
			verbose =
			new CommandLine.Switch("verbose",
					"Directs infaunit to dump out runtime information like which switches are loaded.  Default is false");
	CommandLine.Switch
			refreshData =
			new CommandLine.Switch("refresh-data",
					"If true, infaunit will truncate and reload all file and database data used by the test.  Otherwise, no change to the database or files will be made.  Default is True.");
	CommandLine.Switch
			purgeTestFolders =
			new CommandLine.Switch("purge-test-folder", "Purges all test folders from etlunit [folders starting with __].");
	CommandLine.Switch
			deployWorkflows =
			new CommandLine.Switch("deploy-workflow", "Deploys all workflows which match the test specification and mode.");
	CommandLine.Switch
			helpOperations =
			new CommandLine.Switch("help-operations", "Prints the usage statement for every operation.");
	CommandLine.Switch mode = new CommandLine.Switch("mode", "Identifies the mode of operation.");
	CommandLine.Switch
			env =
			new CommandLine.Switch("environment",
					"Specifies an environment name.  This is appended to the host name of the properties file.");

	CommandLine.Switch[] OPTIONS = new CommandLine.Switch[]{
			forceDbUpdate,
			help,
			helpOperations,
			hostProperties,
			interactive,
			interactiveOperations,
			listFailed,
			listTests,
			resetHistory,
			runFailed,
			simulate,
			yesToAll,
			updateBcpData,
			updateFileData,
			updateInformatica,
			updateInformaticaAll,
			bcpNoFail,
			createInformaticaConnection,
			deleteInformaticaConnection,
			createInformaticaFolder,
			deleteInformaticaFolder,
			loadInformaticaMapping,
			debuggable,
			refreshData,
			purgeTestFolders,
			deployWorkflows,
			verbose,
			mode,
			env
	};
}
