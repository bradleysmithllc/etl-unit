package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrderKey implements Comparable<OrderKey>
{
	private final List<Object> colValues = new ArrayList<Object>();
	private final List<DataFileSchema.Column> cols = new ArrayList<DataFileSchema.Column>();

	public OrderKey()
	{
	}

	public void addColumn(DataFileSchema.Column col, Object data)
	{
		colValues.add(data == FlatFile.KEY_NULL ? data : col.getConverter().format(data, col));
		cols.add(col);
	}

	public String getPrettyString()
	{
		StringBuilder stb = new StringBuilder();

		boolean first = true;

		for (Object data : colValues)
		{
			if (!first)
			{
				stb.append("|");
			}
			else
			{
				first = false;
			}

			if (data == FlatFile.KEY_NULL)
			{
				stb.append("<<null>>");
			}
			else
			{
				stb.append(data);
			}
		}

		return stb.toString();
	}

	@Override
	public int compareTo(OrderKey o)
	{
		for (int i = 0; i < cols.size(); i++)
		{
			int i1 = compareKeys(o, i);

			if (i1 != 0)
			{
				return i1;
			}
		}

		return 0;
	}

	public int compareKeys(OrderKey keyOther, int colIndex)
	{
		Object dataThis = colValues.get(colIndex);
		Object dataThat = keyOther.colValues.get(colIndex);

		// handle nulls
		if (dataThis == FlatFile.KEY_NULL && dataThat == FlatFile.KEY_NULL)
		{
			return 0;
		}
		else if (dataThis == FlatFile.KEY_NULL)
		{
			return -1;
		}
		else if (dataThat == FlatFile.KEY_NULL)
		{
			return 1;
		}

		DataFileSchema.Column colThis = cols.get(colIndex);
		DataFileSchema.Column colThat = keyOther.cols.get(colIndex);

		if (
				colThis.getBasicType() == DataFileSchema.Column.basic_type.integer
						&&
						colThat.getBasicType() == DataFileSchema.Column.basic_type.integer
				)
		{
			// try as integers
			try
			{
				long l1 = Long.parseLong(String.valueOf(dataThis));
				long l2 = Long.parseLong(String.valueOf(dataThat));

				if (l1 == l2)
				{
					return 0;
				}
				else if (l1 < l2)
				{
					return -1;
				}
				else
				{
					return 1;
				}
			}
			catch (NumberFormatException exc)
			{
				// not numbers
			}
		}
		else if (
				colThis.getBasicType() == DataFileSchema.Column.basic_type.numeric
						&&
						colThat.getBasicType() == DataFileSchema.Column.basic_type.numeric
				)
		{
			// try as decimals
			try
			{
				BigDecimal l1 = new BigDecimal(String.valueOf(dataThis));
				BigDecimal l2 = new BigDecimal(String.valueOf(dataThat));

				return l1.compareTo(l2);
			}
			catch (NumberFormatException exc)
			{
				// not numbers
			}
		}

		char[] key1Chars = String.valueOf(dataThis).toCharArray();
		char[] key2Chars = String.valueOf(dataThat).toCharArray();

		int minComp = Math.min(key1Chars.length, key2Chars.length);

		for (int i = 0; i < minComp; i++)
		{
			if (key1Chars[i] != key2Chars[i])
			{
				if (key1Chars[i] < key2Chars[i])
				{
					return -1;
				}
				else
				{
					return 1;
				}
			}
		}

		if (key1Chars.length != key2Chars.length)
		{
			if (key1Chars.length < key2Chars.length)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}

		return 0;
	}
}
