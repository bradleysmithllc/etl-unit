package org.bitbucket.bradleysmithllc.etlunit.parser.datadiff;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class DataFileDiffNearSpecification implements Comparable<DataFileDiffNearSpecification> {
	public enum specification_type {
		timestamp,
		date,
		time
	}

	private final specification_type specificationType;
	private final NearSpecification timestampNearSpecification;

	public DataFileDiffNearSpecification(specification_type specificationType, NearSpecification timestampNearSpecification) {
		this.specificationType = specificationType;
		this.timestampNearSpecification = timestampNearSpecification;
	}

	public specification_type getSpecificationType() {
		return specificationType;
	}

	public TimestampNearSpecification getTimestampNearSpecification() {
		return (TimestampNearSpecification) timestampNearSpecification;
	}

	public NearDiffResult test(String sourceValueText) {
		return timestampNearSpecification.test(sourceValueText);
	}

	@Override
	public int compareTo(DataFileDiffNearSpecification dataFileDiffNearSpecification) {
		return timestampNearSpecification.compareTo(dataFileDiffNearSpecification.timestampNearSpecification);
	}
}
