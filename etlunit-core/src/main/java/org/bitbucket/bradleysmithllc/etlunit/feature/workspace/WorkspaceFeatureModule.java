package org.bitbucket.bradleysmithllc.etlunit.feature.workspace;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Module;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestConstants;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * The purpose of this class is to (1) provide a workspace for temporary
 * files, etc, which will get cleared with each test.  It will also (2)
 * expose an option to purge the workspace between tests.
 */
public class WorkspaceFeatureModule extends AbstractFeature {
	private MetaDataManager metaDataManager;
	private MetaDataContext workspaceMetaContext;

	@Inject
	public void receiveMetaDataManager(MetaDataManager manager)
	{
		metaDataManager = manager;
		workspaceMetaContext = metaDataManager.getMetaData().getOrCreateContext("workspace");
	}

	private RuntimeSupport runtimeSupport;
	private WorkspaceRuntimeSupportImpl workspaceRuntimeSupport;

	@Override
	protected Injector preCreateSub(Injector inj) {
		workspaceRuntimeSupport = postCreate(new WorkspaceRuntimeSupportImpl(runtimeSupport, workspaceMetaContext));

		Injector childInjector = inj.createChildInjector(new Module() {
			public void configure(Binder binder) {
				binder.bind(WorkspaceRuntimeSupport.class).toInstance(workspaceRuntimeSupport);
			}
		});

		return childInjector;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport rs) {
		runtimeSupport = rs;
	}

	@Override
	public ClassListener getListener() {
		return new NullClassListener() {
			@Override
			public void beginTests(VariableContext context, int executorId) throws TestExecutionError {
				try {
					if (!context.hasVariableBeenDeclared("workspace")) {
						// create and install a new one
						Workspace workspace = new Workspace(
								new File(runtimeSupport.getGeneratedSourceDirectory(getFeatureName()), "workspace-" + UUID.randomUUID().toString())
						);

						runtimeSupport.storeInExecutor("workspace", workspace);
					}
				} catch (IOException exc) {
					throw new TestExecutionError("", TestConstants.ERR_IO_ERROR, exc);
				}
			}

			@Override
			public void begin(ETLTestMethod mt, VariableContext context, final int executor) throws TestExecutionError {
				try {
					workspaceRuntimeSupport.currentWorkspaceImpl().checkPoint(mt.getQualifiedName().replace("[", "_").replace("]", "_"));
				} catch (IOException exc) {
					throw new TestExecutionError("", TestConstants.ERR_IO_ERROR, exc);
				}
			}
		};
	}
}
