package org.bitbucket.bradleysmithllc.etlunit.test_support;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.io.File;

public class TestTempFileNamingPolicy implements RuntimeSupport.TempFileNamingPolicy
{
	private int counter = 0;

	@Override
	public String makeAnonymousFileName() {
		return "anon_" + nextCounter();
	}

	private synchronized String nextCounter() {
		return String.valueOf(counter++);
	}

	@Override
	public String makeAnonymousFileNameWithExtension(String ext) {
		return "anon_" + nextCounter() + "." + ext;
	}

	@Override
	public String makeAnonymousFileNameWithPrefix(String pref) {
		return pref + makeAnonymousFileName();
	}

	@Override
	public String makeAnonymousFolderName() {
		return "anon_" + nextCounter();
	}

	@Override
	public String nameTempFolder(String name) {
		return name + makeAnonymousFolderName();
	}

	@Override
	public String nameTempFile(String name) {
		return name + makeAnonymousFileName();
	}

	@Override
	public File mkTempDirectoryRoot(File tempRoot, int executor) {
		// ignore threads
		return new File(tempRoot, "0");
	}
}
