package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.regexp.SuiteNameExpression;
import org.bitbucket.bradleysmithllc.etlunit.regexp.SuiteNamesExpression;

import java.util.ArrayList;
import java.util.List;

public class SuiteClassDirectorImpl extends NullClassDirector {
	public static enum required_type {
		include,
		exclude,
		optional
	}

	public static final class SuiteRef {
		enum accept {yes, no, maybe}

		private final required_type type;
		private final String suiteName;

		public SuiteRef(SuiteNameExpression sn) {
			suiteName = sn.getSuiteName().toLowerCase();

			String ot = sn.hasOptionType() ? sn.getOptionType() : null;

			if (ot == null || ot.equals("+")) {
				type = required_type.include;
			} else if (ot.equals("?")) {
				type = required_type.optional;
			} else {
				type = required_type.exclude;
			}
		}

		public SuiteRef(String name, required_type type) {
			suiteName = name.toLowerCase();

			this.type = type != null ? type : required_type.include;
		}

		public accept accept(List<ETLTestAnnotation> annoList) {
			// check against this name
			for (ETLTestAnnotation anno : annoList) {
				List<String> suiteNames = new ArrayList<>();

				ETLTestValueObject value = anno.getValue();

				// annotation will either be name or names
				if (value.query("name") != null) {
					suiteNames.add(value.query("name").getValueAsString());
				} else if (value.query("names") != null) {
					suiteNames.addAll(value.query("names").getValueAsListOfStrings());
				}

				for (String sName : suiteNames) {
					if (suiteName.equals(sName.toLowerCase())) {
						// decide if this is good or bad
						switch (type) {
							case optional:
								// match means include
							case include:
								// match means good
								return accept.yes;
							case exclude:
								// match means bad
								return accept.no;
						}
					}
				}
			}

			// no match can go either way
			switch (type) {
				case include:
					// no match means bad
					return accept.no;
				case optional:
					// match means maybe include.  An all maybe response means exclude
					return accept.maybe;
				case exclude:
					// no match means good
				default:
					return accept.yes;
			}
		}

		public required_type getType() {
			return type;
		}

		public String getSuiteName() {
			return suiteName;
		}
	}

	private final List<SuiteRef> suiteRefs = new ArrayList<SuiteRef>();

	public SuiteClassDirectorImpl(String text) {
		SuiteNamesExpression sne = new SuiteNamesExpression(text);

		if (sne.hasNext()) {
			SuiteNameExpression sn = sne.getSuiteName();

			while (sn.hasNext()) {
				suiteRefs.add(new SuiteRef(sn));
			}
		}
	}

	public SuiteClassDirectorImpl() {
	}

	public void addSuite(String name, required_type type) {
		suiteRefs.add(new SuiteRef(name, type));
	}

	@Override
	public response_code accept(ETLTestClass cl) {
		List<ETLTestAnnotation> annoList = cl.getAnnotations("@JoinSuite");

		if (annoList.size() == 0 || suiteRefs.size() == 0) {
			return response_code.reject;
		}

		SuiteRef.accept bestAccept = SuiteRef.accept.no;

		for (SuiteRef ref : suiteRefs) {
			SuiteRef.accept accept = ref.accept(annoList);
			switch (accept) {

				case yes:
					bestAccept = SuiteRef.accept.yes;
					break;
				case no:
					return response_code.reject;
				case maybe:
					if (bestAccept != SuiteRef.accept.yes) {
						bestAccept = SuiteRef.accept.maybe;
					}

					break;
			}
		}

		return bestAccept == SuiteRef.accept.yes ? response_code.accept : response_code.reject;
	}

	@Override
	public response_code accept(ETLTestMethod mt) {
		return response_code.accept;
	}

	@Override
	public response_code accept(ETLTestOperation op) {
		return response_code.accept;
	}

	public List<SuiteRef> getSuiteRefs() {
		return suiteRefs;
	}
}
