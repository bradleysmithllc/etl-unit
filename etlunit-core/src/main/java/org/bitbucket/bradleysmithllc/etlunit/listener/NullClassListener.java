package org.bitbucket.bradleysmithllc.etlunit.listener;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ExecutionContext;
import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.TestWarning;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;

import javax.annotation.Nullable;

public class NullClassListener implements ClassListener
{
	@Override
	public void beginTests(VariableContext context, int executorId) throws TestExecutionError
	{
	}

	@Override
	public void beginTests(VariableContext context) throws TestExecutionError {
	}

	@Override
	public void beginPackage(ETLTestPackage name, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	@Override
	public void begin(ETLTestClass cl, VariableContext context, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	@Override
	public void declare(ETLTestVariable var, VariableContext context, int executor)
	{
	}

	@Override
	public void begin(ETLTestMethod mt, VariableContext context, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	@Override
	public void begin(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		begin(op, parameters, vcontext, econtext, executor);
	}

	public void begin(ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	@Override
	public action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		return process(op, obj, context, econtext, executor);
	}

	public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		return action_code.defer;
	}

	@Override
	public void end(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		end(op, parameters, vcontext, econtext, executor);
	}

	public void end(ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	@Override
	public void end(ETLTestMethod mt, VariableContext context, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	@Override
	public void end(ETLTestClass cl, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	@Override
	public void endPackage(ETLTestPackage name, VariableContext context, int executorId) throws TestAssertionFailure, TestExecutionError, TestWarning {
	}

	@Override
	public void endTests(VariableContext context, int executorId)
	{
	}

	@Override
	public void endTests(VariableContext context) {
	}
}
