package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.DataInput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.zip.CRC32;

/**
 * A stream frame.  Looks thus:
 * [data length]			4 bytes
 * [data block]				[length] bytes
 * [crc]							8 bytes
 */
public class StreamFrame
{
	static final long STREAM_CONST = 0xCAFEBABEEBABEFACL;
	private static byte [] NULL_ARRAY = new byte[]{};

	public static final int MINIMUM_FRAME_BYTES = 18;

	private final int length;
	private final int offset;
	private final byte [] data;

	public byte[] getData() {
		return data;
	}

	/**
	 * EOS frame
	 */
	public StreamFrame() {
		length = 0;
		offset = 0;
		data = NULL_ARRAY;
	}

	public StreamFrame(byte[] data, int offset, int length) {
		this.length = length;
		this.offset = offset;
		this.data = data;

		if (length == 0)
		{
			throw new IllegalArgumentException("Cannot stream 0-byte frame");
		}

		if (length + offset > data.length)
		{
			throw new IllegalArgumentException("Byte buffer too small");
		}
	}

	public void write(DataOutputStream output) throws IOException
	{
		output.writeLong(STREAM_CONST);
		output.writeInt(length);

		long crcVal = 0L;

		if (length > 0)
		{
			output.write(data, offset, length);

			CRC32 crc = new CRC32();
			crc.update(data, offset, length);

			crcVal = crc.getValue();
		}

		output.writeLong(crcVal);
		output.flush();
	}

	public static StreamFrame nextFrame(DataInput in) throws IOException {
		StreamHeader header = StreamHeader.readHeader(in);

		// read buffer
		byte [] buff = new byte[header.length];

		in.readFully(buff);

		// get checksum
		long chk = in.readLong();

		// verify checksum
		CRC32 crc = new CRC32();
		crc.update(buff, 0, header.length);

		if (crc.getValue() != chk)
		{
			throw new IOException("Bad checksum");
		}

		if (header.length == 0)
		{
			return new StreamFrame();
		}
		else
		{
			return new StreamFrame(buff, 0, header.length);
		}
	}

	public static boolean frameAvailable(RandomAccessFile raf) throws IOException {
		long fp = raf.getFilePointer();
		long length = raf.length();

		// check for minimum space
		if (length - fp < MINIMUM_FRAME_BYTES)
		{
			return false;
		}

		// have to actually read the header to verify
		StreamHeader header = StreamHeader.readHeader(raf);

		// verify that at least enough bytes to read the buffer + 8 for the crc
		long fpn = raf.getFilePointer();

		// reset the pointer
		raf.seek(fp);

		return (length - fpn) >= header.length + 8;
	}
}

class StreamHeader
{
	final long signature;
	final int length;

	public StreamHeader(long sig, int length) {
		signature = sig;
		this.length = length;;
	}

	public static StreamHeader readHeader(DataInput din) throws IOException {
		// read the length prefix
		long sig = din.readLong();

		if (sig != StreamFrame.STREAM_CONST)
		{
			throw new IOException("Bad stream frame constant: " + Long.toString(sig, 16));
		}

		// read the length
		int length = din.readInt();

		// bounds
		if (length < 0)
		{
			throw new IOException("Bad frame length - negative: [" + length + "]");
		}

		// arbitrary limit
		if (length > 1000000)
		{
			throw new IOException("Bad frame length - too big: [" + length + "]");
		}

		return new StreamHeader(sig, length);
	}
}
