package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import java.util.ArrayList;
import java.util.List;

public class FeatureStatusReporterProxy implements StatusReporter
{
	private final List<StatusReporter> reporters = new ArrayList<StatusReporter>();

	public FeatureStatusReporterProxy(List<Feature> features)
	{
		for (Feature feature : features)
		{
			StatusReporter direct = feature.getStatusReporter();

			if (direct != null)
			{
				reporters.add(direct);
			}
		}
	}

	@Override
	public void scanStarted()
	{
		broadcast(new FeatureReporter()
		{
			@Override
			public void broadcast(StatusReporter director)
			{
				director.scanStarted();
			}
		});
	}

	@Override
	public void scanCompleted()
	{
		broadcast(new FeatureReporter()
		{
			@Override
			public void broadcast(StatusReporter director)
			{
				director.scanCompleted();
			}
		});
	}

	@Override
	public void testsStarted(final int numTestsSelected)
	{
		broadcast(new FeatureReporter()
		{
			@Override
			public void broadcast(StatusReporter director)
			{
				director.testsStarted(numTestsSelected);
			}
		});
	}

	@Override
	public void testClassAccepted(final ETLTestClass method)
	{
		broadcast(new FeatureReporter()
		{
			@Override
			public void broadcast(StatusReporter director)
			{
				director.testClassAccepted(method);
			}
		});
	}

	@Override
	public void testMethodAccepted(final ETLTestMethod method)
	{
		broadcast(new FeatureReporter()
		{
			@Override
			public void broadcast(StatusReporter director)
			{
				director.testMethodAccepted(method);
			}
		});
	}

	@Override
	public void testBeginning(final ETLTestMethod method)
	{
		broadcast(new FeatureReporter()
		{
			@Override
			public void broadcast(StatusReporter director)
			{
				director.testBeginning(method);
			}
		});
	}

	@Override
	public void testCompleted(final ETLTestMethod method, final CompletionStatus status)
	{
		broadcast(new FeatureReporter()
		{
			@Override
			public void broadcast(StatusReporter director)
			{
				director.testCompleted(method, status);
			}
		});
	}

	@Override
	public void testsCompleted()
	{
		broadcast(new FeatureReporter()
		{
			@Override
			public void broadcast(StatusReporter director)
			{
				director.testsCompleted();
			}
		});
	}

	private static interface FeatureReporter
	{
		void broadcast(StatusReporter director);
	}

	private void broadcast(FeatureReporter dire)
	{
		boolean ret = false;

		for (StatusReporter direct : reporters)
		{
			dire.broadcast(direct);
		}
	}
}
