package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class ETLTestAnnotationImpl extends ETLTestDebugTraceableImpl implements ETLTestAnnotation
{
	private final String name;
	private final ETLTestValueObject value;

	public ETLTestAnnotationImpl(String n, ETLTestValueObject v)
	{
		name = n;

		if (v.getValueType() != ETLTestValueObject.value_type.object)
		{
			throw new IllegalArgumentException("Cannot construct an annotation with any value type except for object");
		}

		value = v;
	}

	public ETLTestAnnotationImpl(String n, ETLTestValueObject v, Token t)
	{
		super(t);
		name = n;

		if (v.getValueType() != ETLTestValueObject.value_type.object)
		{
			throw new IllegalArgumentException("Cannot construct an annotation with any value type except for object");
		}

		value = v;
	}

	public ETLTestAnnotationImpl(String n)
	{
		name = n;
		value = null;
	}

	public ETLTestAnnotationImpl(String n, Token t)
	{
		super(t);
		name = n;
		value = null;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public boolean hasValue()
	{
		return value != null;
	}

	@Override
	public ETLTestValueObject getValue()
	{
		return value;
	}

	@Override
	public String toString()
	{
		return value != null ? (name + '(' + value.getJsonNode() + ')') : name;
	}
}
