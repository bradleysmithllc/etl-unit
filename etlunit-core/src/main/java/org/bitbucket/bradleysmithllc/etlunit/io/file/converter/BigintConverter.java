package org.bitbucket.bradleysmithllc.etlunit.io.file.converter;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;

public class BigintConverter extends BaseRegexpConverter {
	private static final String PATTERN = "-?\\d{1,}";

	public BigintConverter() {
		super(PATTERN);
	}

	@Override
	public String format(Object data, DataFileSchema.Column column) {
		return String.valueOf(data);
	}

	@Override
	public Object parse(String data, DataFileSchema.Column column) {
		return Long.parseLong(data);
	}

	@Override
	public String getId() {
		return "BIGINT";
	}
}
