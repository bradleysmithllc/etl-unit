package org.bitbucket.bradleysmithllc.etlunit.feature.debug;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@FeatureModule
public class ConsoleFeatureModule extends AbstractFeature
{
	private static final List<String> prerequisites = new ArrayList<String>(Arrays.asList("results"));

	private final ConsoleStatusReporter consoleStatusReporter = new ConsoleStatusReporter();

	@Override
	public List<String> getPrerequisites()
	{
		return prerequisites;
	}

	@Override
	public void initialize(Injector inj)
	{
		inj.injectMembers(consoleStatusReporter);
	}

	@Override
	public StatusReporter getStatusReporter()
	{
		return consoleStatusReporter;
	}

	@Override
	public String getFeatureName()
	{
		return "console";
	}

	@Override
	public boolean requiredForSimulation() {
		return true;
	}
}
