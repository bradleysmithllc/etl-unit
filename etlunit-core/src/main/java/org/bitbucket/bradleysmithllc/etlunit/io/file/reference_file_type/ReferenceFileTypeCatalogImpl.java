package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ReferenceFileTypeCatalogImpl implements ReferenceFileTypeCatalog, JsonSerializable
{
	private String description;
	private final Map<String, ReferenceFileTypePackage> packages = new HashMap<String, ReferenceFileTypePackage>();
	private final Map<String, ReferenceFileTypePackage> lowerPackages = new HashMap<String, ReferenceFileTypePackage>();

	private RuntimeSupport runtimeSupport;

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport supp)
	{
		runtimeSupport = supp;
	}

	@Override
	public String description() {
		return description;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("reference-file-types");
		writer.beginObject();

		writer.name("description").value(description);

		writer.name("packages");
		writer.beginObject();

		for (Map.Entry<String, ReferenceFileTypePackage> pack : packages.entrySet())
		{
			writer.name(pack.getKey());
			writer.beginObject();
			ReferenceFileTypePackageImpl impl = (ReferenceFileTypePackageImpl) pack.getValue();
			impl.toJson(writer);
			writer.endObject();
		}

		writer.endObject();

		writer.endObject();
	}

	@Override
	public void fromJson(JsonNode node) {
		JsonNode typesNode = node.get("reference-file-types");

		description = typesNode.get("description").asText();

		JsonNode packs = typesNode.get("packages");

		Iterator<Map.Entry<String, JsonNode>> fields = packs.fields();

		while (fields.hasNext())
		{
			Map.Entry<String, JsonNode> nextField = fields.next();

			String name = nextField.getKey();

			ReferenceFileTypePackageImpl pack = new ReferenceFileTypePackageImpl(name);
			pack.receiveRuntimeSupport(runtimeSupport);
			pack.fromJson(nextField.getValue());

			lowerPackages.put(name.toLowerCase(), pack);
			packages.put(name, pack);
		}
	}

	@Override
	public ReferenceFileType getReferenceFileType(String id) {
		String idLower = id.toLowerCase();

		for (Map.Entry<String, ReferenceFileTypePackage> package_ : lowerPackages.entrySet())
		{
			ReferenceFileType referenceFileType = package_.getValue().getReferenceFileTypesById().get(idLower);
			if (referenceFileType != null)
			{
				return referenceFileType;
			}
		}

		return null;
	}

	@Override
	public Map<String, ReferenceFileTypePackage> getReferenceFileTypePackages() {
		return Collections.unmodifiableMap(lowerPackages);
	}
}
