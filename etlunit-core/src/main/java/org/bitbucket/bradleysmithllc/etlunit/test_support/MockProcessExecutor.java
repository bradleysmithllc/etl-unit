package org.bitbucket.bradleysmithllc.etlunit.test_support;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;

import java.io.*;

public class MockProcessExecutor implements ProcessExecutor
{
	public MockProcessExecutor(ProcessCallback processCallback)
	{
		this.processCallback = processCallback;
	}

	public static interface ProcessCallback
	{
		public MockProcessFacade mock(ProcessDescription pd) throws IOException;
	}

	public static interface MockProcessFacade
	{
		void mockWaitForProcess();

		int mockCompletionCode();

		void mockKill();

		void mockWaitForStreams();

		Writer getWriter();

		BufferedReader getReader();

		BufferedReader getErrorReader();

		StringBuffer getInputBuffer();

		StringBuffer getErrorBuffer();

		OutputStream getOutputStream();

		InputStream getInputStream();

		InputStream getErrorStream();
	}

	private final ProcessCallback processCallback;

	@Override
	public ProcessFacade execute(final ProcessDescription pd) throws IOException
	{
		final MockProcessFacade mpf = processCallback.mock(pd);

		return new ProcessFacade()
		{
			@Override
			public ProcessDescription getDescriptor()
			{
				return pd;
			}

			@Override
			public void waitForCompletion()
			{
				mpf.mockWaitForProcess();
			}

			@Override
			public int getCompletionCode()
			{
				return mpf.mockCompletionCode();
			}

			@Override
			public void kill()
			{
				mpf.mockKill();
			}

			@Override
			public void waitForStreams()
			{
				mpf.mockWaitForStreams();
			}

			@Override
			public void waitForOutputStreamsToComplete() {
			}

			@Override
			public Writer getWriter()
			{
				return mpf.getWriter();
			}

			@Override
			public BufferedReader getReader()
			{
				return mpf.getReader();
			}

			@Override
			public BufferedReader getErrorReader()
			{
				return mpf.getErrorReader();
			}

			@Override
			public StringBuffer getInputBuffered() throws IOException
			{
				return mpf.getInputBuffer();
			}

			@Override
			public StringBuffer getErrorBuffered() throws IOException
			{
				return mpf.getErrorBuffer();
			}

			@Override
			public OutputStream getOutputStream()
			{
				return mpf.getOutputStream();
			}

			@Override
			public InputStream getInputStream()
			{
				return mpf.getInputStream();
			}

			@Override
			public InputStream getErrorStream()
			{
				return mpf.getErrorStream();
			}
		};
	}
}
