package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OperationClassDirector extends NullClassDirector
{
	public static class OperationRef
	{
		private final String operationName;
		private final String propertyName;
		private final String propertyStringValue;
		private final Pattern propertyValue;

		private OperationRef(String operationName, String propertyName, String propertyValue) {
			this.operationName = operationName;
			this.propertyName = propertyName.toLowerCase();
			propertyStringValue = propertyValue;
			this.propertyValue = Pattern.compile(propertyStringValue);
		}

		public String getOperationName() {
			return operationName;
		}

		public String getPropertyName() {
			return propertyName;
		}

		public String getPropertyStringValue() {
			return propertyStringValue;
		}

		public Pattern getPropertyValue() {
			return propertyValue;

		}
	}

	private final List<OperationRef> operationList = new ArrayList<OperationRef>();

	public void addOperation(String property, String value)
	{
		int index = property.indexOf('.');

		String operName = null;

		if (index != -1)
		{
			operName = property.substring(0, index);

			property = property.substring(index + 1);
		}

		addOperation(operName, property, value);
	}

	public void addOperation(String operation, String property, String value)
	{
		operationList.add(new OperationRef(operation, property, value));
	}

	@Override
	public response_code accept(ETLTestClass cl) {
		// we don't care about classes.
		return response_code.accept;
	}

	@Override
	public response_code accept(ETLTestMethod mt) {
		if (checkMethods(mt.getTestClass().getBeforeClassMethods()) == response_code.accept)
		{
			return response_code.accept;
		}

		if (checkMethods(mt.getTestClass().getBeforeTestMethods()) == response_code.accept)
		{
			return response_code.accept;
		}

		if (checkMethod(mt) == response_code.accept)
		{
			return response_code.accept;
		}

		if (checkMethods(mt.getTestClass().getAfterTestMethods()) == response_code.accept)
		{
			return response_code.accept;
		}

		if (checkMethods(mt.getTestClass().getAfterClassMethods()) == response_code.accept)
		{
			return response_code.accept;
		}

		return response_code.reject;
	}

	private response_code checkMethod(ETLTestMethod mt) {
		for (ETLTestOperation op : mt.getOperations())
		{
			if (checkOperation(op) == response_code.accept)
			{
				return response_code.accept;
			}
		}

		return response_code.reject;
	}

	private response_code checkOperation(ETLTestOperation op) {
		for (OperationRef opref : operationList)
		{
			if (!matches(opref, op))
			{
				return response_code.reject;
			}
		}

		return response_code.accept;
	}

	private boolean matches(OperationRef opref, ETLTestOperation operation) {
		// optionally check the operation name
		if (opref.operationName != null)
		{
			if (!opref.operationName.equals(operation.getOperationName()))
			{
				return false;
			}
		}

		// check the property name and value referenced
		ETLTestValueObject operands = operation.getOperands();

		if (operands == null)
		{
			return false;
		}

		ETLTestValueObject result = operands.query(opref.propertyName);

		if (result == null)
		{
			return false;
		}

		// property found, check the value
		Matcher matcher = opref.propertyValue.matcher(result.getValueAsString());

		return matcher.find();
	}

	private response_code checkMethods(List<ETLTestMethod> beforeClassMethods) {
		for (ETLTestMethod test : beforeClassMethods)
		{
			if (checkMethod(test) == response_code.accept)
			{
				return response_code.accept;
			}
		}

		return response_code.reject;
	}

	@Override
	public response_code accept(ETLTestOperation op) {
		// all operations are good.
		return response_code.accept;
	}

	public List<OperationRef> getOperationList() {
		return operationList;
	}
}
