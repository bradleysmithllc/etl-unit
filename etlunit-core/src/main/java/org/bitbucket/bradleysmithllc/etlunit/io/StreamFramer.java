package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.*;
import java.util.concurrent.CountDownLatch;

/**
 * Read a stream until it is exhausted into a frame output.
 * Created by bsmith on 1/6/14.
 */
public class StreamFramer implements Runnable {
	private final InputStream in;
	private final File target;
	private final CountDownLatch countDownLatch;
	private final OutputStream outputStream;

	public StreamFramer(InputStream stream, File streamOut, OutputStream outputStream, CountDownLatch clatch) {
		in = stream;
		target = streamOut;
		this.outputStream = outputStream;
		countDownLatch = clatch;
	}

	@Override
	public void run() {
		// pretty simple stuff
		try
		{
			try {
				NfurcatingOutputStream output = new NfurcatingOutputStream(new FrameOutputStream(
						new BufferedOutputStream(
								new FileOutputStream(target)
						)
					)
				);

				OutputStream zout = output.bifurcate(outputStream);

				try
				{
					byte [] buffer = new byte[4096];
					int res;

					while ((res = in.read(buffer)) != -1)
					{
						zout.write(buffer, 0, res);
						zout.flush();
					}
				}
				finally
				{
					output.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		finally
		{
			countDownLatch.countDown();
		}
	}
}
