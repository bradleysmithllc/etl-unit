package org.bitbucket.bradleysmithllc.etlunit.listener;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.TestWarning;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;

public interface BroadcasterExecutor
{
	int getId();

	/**
	 * Called before any other methods.
	 */
	void beginExecution()  throws TestExecutionError;

	/**
	 * Broadcaster is handling a new package.  This will be called exactly once for every package and
	 * before any newTest methods are called for classes in THIS package.
	 * @param name
	 */
	void enterPackage(ETLTestPackage name, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning;

	/**
	 * Broadcaster is handling a new class.  This will be called exactly once for every class, and before any test methods.
	 * @param cl
	 */
	void enterClass(ETLTestClass cl, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning;

	/**
	 * This is a method annotated with @Test - a testable, runnable (director-approved) method.
	 * @param mt
	 */
	void runTest(ETLTestMethod mt, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning;

	/**
	 * Broadcaster is through with a class.  This will be called exactly once for every class, and after any test methods.
	 * @param cl
	 */
	void leaveClass(ETLTestClass cl, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning;

	/**
	 * Broadcaster is through handling a package.  This will be called exactly once for every package and
	 * after any newTest methods are called for classes in THIS package.
	 * @param name
	 */
	void leavePackage(ETLTestPackage name, StatusReporter.CompletionStatus status) throws TestAssertionFailure, TestExecutionError, TestWarning;

	/**
	 * Called when there is no more work to perform.  Not that in some cases this executor may not perform
	 * any work.  E.G., when there are four executors and 3 tests.
	 */
	void endExecution();
}
