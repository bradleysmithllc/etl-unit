package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ETLTestClassImpl extends ETLTestAnnotatedImpl implements ETLTestClass
{
	private List<ETLTestVariable> classVariables = new ArrayList<ETLTestVariable>();

	private final ETLTestPackage _package;
	private String className;
	private List<String> methodNames = new ArrayList<String>();

	private List<String> suiteMemberships = new ArrayList<String>();
	private List<String> suiteMembershipsPub = Collections.unmodifiableList(suiteMemberships);

	private List<ETLTestMethod> testMethods = new ArrayList<ETLTestMethod>();
	private List<ETLTestMethod> testMethodsPub = Collections.unmodifiableList(testMethods);

	private List<ETLTestMethod> beforeClassMethods = new ArrayList<ETLTestMethod>();
	private List<ETLTestMethod> beforeClassMethodsPub = Collections.unmodifiableList(beforeClassMethods);

	private List<ETLTestMethod> afterClassMethods = new ArrayList<ETLTestMethod>();
	private List<ETLTestMethod> afterClassMethodsPub = Collections.unmodifiableList(afterClassMethods);

	private List<ETLTestMethod> beforeTestMethods = new ArrayList<ETLTestMethod>();
	private List<ETLTestMethod> beforeTestMethodsPub = Collections.unmodifiableList(beforeTestMethods);

	private List<ETLTestMethod> afterTestMethods = new ArrayList<ETLTestMethod>();
	private List<ETLTestMethod> afterTestMethodsPub = Collections.unmodifiableList(afterTestMethods);

	public ETLTestClassImpl(ETLTestPackage _package)
	{
		this(_package, null);
	}

	public ETLTestClassImpl(ETLTestPackage _package, Token t)
	{
		super(t);
		this._package = _package;

		if (_package == null)
		{
			throw new IllegalArgumentException("Package cannot be a null object");
		}
	}

	public void addSuiteMembership(String s)
	{
		suiteMemberships.add(s.toLowerCase());
	}

	@Override
	public List<ETLTestVariable> getClassVariables()
	{
		return classVariables;
	}

	@Override
	public ETLTestPackage getPackage()
	{
		return _package;
	}

	public void setName(String className)
	{
		this.className = className;
	}

	@Override
	public String getName()
	{
		return className;
	}

	@Override
	public String getQualifiedName()
	{
		return _package.getPackageName() + "." + className;
	}

	public List<String> getSuiteMemberships()
	{
		if (suiteMemberships.size() == 0 && hasAnnotation("@JoinSuite"))
		{
			List<ETLTestAnnotation> annList = getAnnotations("@JoinSuite");

			Iterator<ETLTestAnnotation> annIt = annList.iterator();

			while (annIt.hasNext())
			{
				ETLTestAnnotation ann = annIt.next();

				suiteMemberships.add(ann.getValue().getValueAsMap().get("name").getValueAsString());
			}
		}

		return suiteMembershipsPub;
	}

	public List<ETLTestMethod> getTestMethods()
	{
		return testMethodsPub;
	}

	public void addClassVariable(ETLTestVariable var)
	{
		classVariables.add(var);
	}

	public void addMethod(ETLTestMethod method)
	{
		if (methodNames.contains(method.getName()))
		{
			throw new IllegalArgumentException("method name "
					+ getName()
					+ "."
					+ method.getName()
					+ " specified more than once");
		}

		if (!method.requiresPurge() && testMethods.size() == 0)
		{
			throw new IllegalArgumentException("First test method does not support @DoNotPurge annotation");
		}

		methodNames.add(method.getName());

		if (method.hasAnnotation("@BeforeClass"))
		{
			beforeClassMethods.add(method);
		}

		if (method.hasAnnotation("@AfterClass"))
		{
			afterClassMethods.add(method);
		}

		if (method.hasAnnotation("@BeforeTest"))
		{
			beforeTestMethods.add(method);
		}

		if (method.hasAnnotation("@AfterTest"))
		{
			afterTestMethods.add(method);
		}

		if (method.hasAnnotation("@Test"))
		{
			testMethods.add(method);
		}
	}

	public List<ETLTestMethod> getBeforeClassMethods()
	{
		return beforeClassMethodsPub;
	}

	public List<ETLTestMethod> getAfterClassMethods()
	{
		return afterClassMethodsPub;
	}

	public List<ETLTestMethod> getBeforeTestMethods()
	{
		return beforeTestMethodsPub;
	}

	public List<ETLTestMethod> getAfterTestMethods()
	{
		return afterTestMethodsPub;
	}

	/**
	 * Ordering is specified as the alpha comparison of the package name first (null package is first),
	 * then the alpha of the class name
	 * This won't necessarily match the file names, but will make the test ordering predictable
	 *
	 * @param o
	 * @return
	 */
	@Override
	public int compareTo(ETLTestClass o)
	{
		// defer to the class name
		int pck = _package.compareTo(o.getPackage());

		if (pck != 0)
		{
			return pck;
		}

		return getName().compareTo(o.getName());
	}
}
