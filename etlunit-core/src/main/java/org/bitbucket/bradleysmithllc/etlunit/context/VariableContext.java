package org.bitbucket.bradleysmithllc.etlunit.context;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.util.Map;

public interface VariableContext
{
	void declareVariable(String variableName);

	boolean hasVariableBeenDeclared(String variableName);

	void removeVariable(String variableName);

	ETLTestValueObject query(String variablePath);

	ETLTestValueObject getValue(String variableName);

	void setValue(String variableName, ETLTestValueObject value);

	void setStringValue(String variableName, String value);

	void setJSONValue(String variableName, String value);

	void declareAndSetValue(String variableName, ETLTestValueObject value);

	void declareAndSetStringValue(String variableName, String value);

	void declareAndSetJSONValue(String variableName, String value);

	/**
	 * Create a nested sub scope.
	 * @param enclosingVisible if true, the enclosing contexts above this one are visible.  If false, the new sub context will be it's own top level scope
	 * @return
	 */
	VariableContext createNestedScope(boolean enclosingVisible);

	VariableContext createNestedScope();

	VariableContext getEnclosingScope();

	VariableContext getTopLevelScope();

	Map<String, ETLTestValueObject> getMapRepresentation();

	Map<String, Object> getVelocityWrapper();

	/**
	 * Processes the given value against this map as a velocity context.
	 * @param argument
	 * @return
	 */
	String contextualize(String argument);

	JsonNode getJsonNode();
}
