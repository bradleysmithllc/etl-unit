package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class JsonSchemaValidator
{
	public static void validateV4Schema(JsonNode node)
	{
		JsonSchemaFactory fact = JsonSchemaFactory.byDefault();

		try {
			JsonNode draftNode = JsonLoader.fromURL(JsonSchemaValidator.class.getResource("/draftv4/schema"));

			JsonSchema schema = fact.getJsonSchema(draftNode);

			ProcessingReport report = schema.validate(node);

			if (!report.isSuccess())
			{
				throw new IllegalArgumentException(JsonProcessingUtil.getProcessingReport(report));
			}
		} catch (ProcessingException e) {
			throw new RuntimeException(JsonProcessingUtil.getProcessingReport(e));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
