package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.NeedsTest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Feature types include lifecycle support (such as preparing a database environment),
 * vm options such as new operations, etc, and features can provide a base
 * for other features to build on, such as a database feature which is built
 * upon by specific database implementations.
 */
public interface Feature
{
	@Inject
	void setConfiguration(Configuration conf);

	void setFeatureConfiguration(ETLTestValueObject obj);

	ETLTestValueObject getFeatureConfiguration();

	FeatureMetaInfo getMetaInfo();

	LogListener getLogListener();

	/**
	 * Make any changes the feature needs to to the injector.
	 * If this method returns non-null, that injector will
	 * be used for further feature initialization.  This allows features
	 * to add bindings to the guice injector to be made available to other downstream
	 * features.
	 */
	Injector preCreate(Injector nj);

	/**
	 * Initialize this module with the dependency injector provided by the
	 * test vm.
	 *
	 * @param nj
	 * @return
	 */
	void initialize(Injector nj);

	/**
	 * The class director interface gives the feature a say in which tests are run.
	 * @return
	 */
	ClassDirector getDirector();

	/**
	 * Listener used to process all messages.  This is the lowest level listener
	 * the Feature has.  Use the meta-info for events tied to operations, validators and signatures.  Some messages
	 * are only available through this interface, such as begin, end, etc.
	 * @return
	 */
	//ClassListener getListener();

	/**
	 * Retrieves a list of listeners for handling messages.  Convenience method for isolating
	 * implementation into separate listener/handlers.
	 * @return
	 */
	List<ClassListener> getListenerList();

	ClassLocator getLocator();

	StatusReporter getStatusReporter();

	String getFeatureName();

	List<String> getPrerequisites();

	/**
	 * The priority of this feature.  The lower the priority the earlier in the
	 * chain the feature will be, the higher priority means later in the chain.
	 * 0 is effectively no priority, <0 for high priority, and >0 for low priority.
	 *
	 * @return the priority level
	 */
	long getPriorityLevel();

	/**
	 * Called when this feature is no longer needed for this test
	 */
	@NeedsTest
	void dispose();

	@Inject
	void setApplicationLog(@Named("applicationLog") Log log);

	@Inject
	void setUserLog(@Named("userLog") Log log);

	/**
	 * Returns a list of the folders created in the test hierarchy which support
	 * tests.  This is used to locate feature resources, and to differentiate support
	 * folders from test packages.
	 *
	 * @return
	 */
	List<String> getTestSupportFolderNames();

	/**
	 * Indicates if this feature is used during a simulation.
	 * @return
	 */
	boolean requiredForSimulation();
}
