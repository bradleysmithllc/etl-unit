package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.name.Named;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class FeatureListenerProxy implements ClassListener
{
	private Log applicationLog;
	private boolean simulateOnly = false;

	private static final class ClassListenerFeature
	{
		final List<ClassListener> listener;
		final Feature feature;

		private ClassListenerFeature(List<ClassListener> listener, Feature feature)
		{
			this.listener = listener;
			this.feature = feature;
		}

		public Feature getFeature()
		{
			return feature;
		}

		public List<ClassListener> getOperationProcessor()
		{
			return listener;
		}
	}

	private final FeatureProxyBase<ClassListenerFeature> base;

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log)
	{
		this.applicationLog = log;
	}

	@Inject
	public void setSimulateOnly(@Named("etlunit.simulateOnly") RuntimeOption ro)
	{
		this.simulateOnly = ro.isEnabled();
	}

	public FeatureListenerProxy(List<Feature> features)
	{
		base = new FeatureProxyBase<ClassListenerFeature>(features, new FeatureProxyBase.Getter<ClassListenerFeature>()
		{
			@Override
			public ClassListenerFeature get(Feature feature)
			{
				List<ClassListener> classListener = feature.getListenerList();

				if (classListener != null)
				{
					return new ClassListenerFeature(classListener, feature);
				}

				return null;
			}
		});
	}

	private static interface FeatureListener
	{
		action_code broadcast(ClassListener director, Feature feature)
				throws TestAssertionFailure, TestExecutionError, TestWarning;
	}

	private action_code broadcast(FeatureListener dire) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		action_code ret = action_code.defer;

		for (ClassListenerFeature list : base.getList())
		{
			if (list != null)
			{
				if (!simulateOnly || list.feature.requiredForSimulation())
				{
					for (ClassListener listener : list.listener)
					{
						ret = dire.broadcast(listener, list.feature);

						if (ret == action_code.handled || ret == action_code.reject)
						{
							return ret;
						}
					}
				}
			}
		}

		return ret;
	}

	@Override
	public void beginTests(final VariableContext context, final int executorId) {
		try {
			broadcast(new FeatureListener()
			{
				@Override
				public action_code broadcast(ClassListener director, Feature feature)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					director.beginTests(context, executorId);

					return action_code.defer;
				}
			});
		} catch (Exception exc) {
			throw new RuntimeException(exc);
		}
	}

	@Override
	public void beginTests(final VariableContext context) {
		try {
			broadcast(new FeatureListener()
			{
				@Override
				public action_code broadcast(ClassListener director, Feature feature)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					director.beginTests(context);

					return action_code.defer;
				}
			});
		} catch (Exception exc) {
			throw new RuntimeException(exc);
		}
	}

	@Override
	public void endTests(final VariableContext context, final int executorId) {
		try {
			broadcast(new FeatureListener()
			{
				@Override
				public action_code broadcast(ClassListener director, Feature feature)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					director.endTests(context, executorId);

					return action_code.defer;
				}
			});
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	@Override
	public void endTests(final VariableContext context) {
		try {
			broadcast(new FeatureListener()
			{
				@Override
				public action_code broadcast(ClassListener director, Feature feature)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					director.endTests(context);

					return action_code.defer;
				}
			});
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	@Override
	public void beginPackage(final ETLTestPackage name, final VariableContext context, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		broadcast(new FeatureListener()
		{
			@Override
			public action_code broadcast(ClassListener director, Feature feature)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				director.beginPackage(name, context, executor);

				return action_code.defer;
			}
		});
	}

	@Override
	public void endPackage(final ETLTestPackage name, final VariableContext context, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		broadcast(new FeatureListener()
		{
			@Override
			public action_code broadcast(ClassListener director, Feature feature)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				director.endPackage(name, context, executor);

				return action_code.defer;
			}
		});
	}

	@Override
	public void begin(final ETLTestClass cl, final VariableContext context, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		broadcast(new FeatureListener()
		{
			@Override
			public action_code broadcast(ClassListener director, Feature feature)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				director.begin(cl, context, executor);

				return action_code.defer;
			}
		});
	}

	@Override
	public void declare(final ETLTestVariable var, final VariableContext context, final int executor)
	{
		try
		{
			broadcast(new FeatureListener()
			{
				@Override
				public action_code broadcast(ClassListener director, Feature feature)
				{
					director.declare(var, context, executor);

					return action_code.defer;
				}
			});

			// these are empty because the deferred method won't ever throw this
		}
		catch (TestAssertionFailure testAssertionFailure)
		{
		}
		catch (TestExecutionError testExecutionError)
		{
		}
		catch (TestWarning testWarning)
		{
		}
	}

	@Override
	public void begin(final ETLTestMethod mt, final VariableContext context, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		broadcast(new FeatureListener()
		{
			@Override
			public action_code broadcast(ClassListener director, Feature feature)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				director.begin(mt, context, executor);

				return action_code.defer;
			}
		});
	}

	@Override
	public void begin(final ETLTestMethod mt, final ETLTestOperation op, final ETLTestValueObject parameters, final VariableContext vcontext, final ExecutionContext econtext, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		broadcast(new FeatureListener()
		{
			@Override
			public action_code broadcast(ClassListener director, Feature feature)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				director.begin(mt, op, parameters, vcontext, econtext, executor);

				return action_code.defer;
			}
		});
	}

	@Override
	public action_code process(final ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		return processListenerList(base.getList(), mt, op, obj, context, econtext, applicationLog, simulateOnly, executor);
	}

	@Override
	public void end(final ETLTestMethod mt, final ETLTestOperation op, final ETLTestValueObject parameters, final VariableContext vcontext, final ExecutionContext econtext, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		broadcast(new FeatureListener()
		{
			@Override
			public action_code broadcast(ClassListener director, Feature feature)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				director.end(mt, op, parameters, vcontext, econtext, executor);

				return action_code.defer;
			}
		});
	}

	@Override
	public void end(final ETLTestMethod mt, final VariableContext context, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		broadcast(new FeatureListener()
		{
			@Override
			public action_code broadcast(ClassListener director, Feature feature)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				director.end(mt, context, executor);

				return action_code.defer;
			}
		});
	}

	@Override
	public void end(final ETLTestClass cl, final VariableContext context, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		broadcast(new FeatureListener()
		{
			@Override
			public action_code broadcast(ClassListener director, Feature feature)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				director.end(cl, context, executor);

				return action_code.defer;
			}
		});
	}

	public static action_code processListenerList(List<ClassListenerFeature> processorList, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, Log applicationLog, boolean simulateOnly, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		action_code ret = action_code.defer;

		for (ClassListenerFeature processorObj : processorList)
		{
			if (!simulateOnly || processorObj.feature.requiredForSimulation())
			{
				ret = checkProcessor(processorObj, mt, op, obj, context, econtext, applicationLog, executor);

				if (ret == action_code.handled || ret == action_code.reject)
				{
					return ret;
				}
			}
		}

		if (ret == action_code.defer && simulateOnly)
		{
			ret = action_code.handled;
		}

		return ret;
	}

	private static action_code checkProcessor(ClassListenerFeature processor, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, Log applicationLog, final int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		ETLTestValueObject obj_new = obj;

		ETLTestValueObject featureConfig = processor.getFeature().getFeatureConfiguration();

		if (featureConfig != null)
		{
			// merge it into the supplied parameters, preferring the operation parameters
			if (obj_new != null)
			{
				obj_new = obj_new.merge(featureConfig, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
			}
			else
			{
				obj_new = featureConfig;
			}
		}

		// always call the lowest-level process handler - which is optional
		List<ClassListener> operationProcessor = processor.getOperationProcessor();
		if (operationProcessor != null)
		{
			for (ClassListener o_processor : operationProcessor)
			{
				action_code res = o_processor.process(mt, op, obj, context, econtext, executor);

				if (res == action_code.reject || res == action_code.handled)
				{
					return res;
				}
			}
		}

		// check if validation is possible
		FeatureMetaInfo meta = processor.getFeature().getMetaInfo();

		// this is a change from 1.2 - utilize the meta-data to determine if a feature can use an operation
		Map<String, FeatureOperation> operations = meta.getExportedOperations();

		if (operations != null)
		{
			FeatureOperation featureOperation = operations.get(op.getOperationName());

			if (featureOperation != null)
			{
				return featureOperation.process(mt, op, obj_new, context, econtext, executor);
			}
		}

		return action_code.defer;
	}
}
