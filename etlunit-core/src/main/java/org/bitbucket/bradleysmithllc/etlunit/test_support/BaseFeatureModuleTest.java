package org.bitbucket.bradleysmithllc.etlunit.test_support;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.apache.commons.lang.ClassUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.debug.ConsoleFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.debug.RunAllFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class BaseFeatureModuleTest
{
	public static boolean ECHO_LOG = false;

	protected File root;
	protected File tmp;
	protected File rpt;
	protected File src;
	protected File genSrc;
	protected File cfg;
	protected File rsc;
	protected ETLTestVM etlTestVM;
	private TestResults results;
	protected String identifier;
	private int executorCount = 1;

	private final String thisClassName;

	protected RuntimeSupport runtimeSupport;

	protected BaseFeatureModuleTest()
	{
		thisClassName = ClassUtils.getShortClassName(getClass());
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
	}

	public final TestResults startTest()
	{
		return startTest(null);
	}

	protected final TestResults startTest(String identifier)
	{
		executorCount = 1;

		try
		{
			Configuration.ignoreUserHome(true);

			TestResults results = startTestImpl(identifier, 1);

		if (multiPassSafe())
		{
			executorCount = 10;
			results = startTestImpl(identifier, 2);
		}

		return results;
		}
		finally
		{
			Configuration.ignoreUserHome(false);
		}
	}

	private final TestResults startTestImpl(String identifier, int pass)
	{
		initializeTests();

		String classKey = thisClassName;

		if (identifier != null)
		{
			classKey += "_" + identifier;
		}

		if (ECHO_LOG)
		{
			System.setProperty("org.bitbucket.bradleysmithllc.etlunit.feature.logging.echo", "true");
		}
		else
		{
			System.clearProperty("org.bitbucket.bradleysmithllc.etlunit.feature.logging.echo");
		}

		this.identifier = identifier;

		try
		{
			if (root == null || cleanWorkspace())
			{
				root = useStaticTestRoot();

				if (root == null)
				{
					File tempFile = null;

					String prop = System.getProperty("projectBuildDirectory");

					File target = new File("target");

					if (prop != null)
					{
						tempFile = new File(prop, "etlunit-test").getAbsoluteFile();
					}
					else if (target.exists())
					{
						tempFile = new File(target, "etlunit-test").getAbsoluteFile();
					}
					else
					{
						tempFile = File.createTempFile("AAA", "AAA").getParentFile().getAbsoluteFile();
					}

					root = new File(tempFile, getClass().getName().replace('.', '_') + ".working");
				}

				src = new FileBuilder(root).subdir("src").subdir("test").subdir("etlunit").file();
				cfg = new FileBuilder(root).subdir("src").subdir("test").subdir("resources").subdir("config").file();
				rsc = new FileBuilder(root).subdir("src").subdir("main").file();
				tmp = new FileBuilder(root).subdir("target").subdir("etlunit-temp").mkdirs().file();
				rpt = new FileBuilder(root).subdir("target").file();
				genSrc = new FileBuilder(rpt).subdir("generated-sources").file();

				// create a test source root
				if (root.exists())
				{
					IOUtils.purge(root, true);
				}

				//assertTrue(root.mkdirs());
				assertTrue(src.mkdirs());
				assertTrue(cfg.mkdirs());
				assertTrue(tmp.mkdirs());
				assertTrue(rsc.mkdirs());
				assertTrue(genSrc.mkdirs());
			}

			assertTrue(src.exists());
			assertTrue(cfg.exists());
			assertTrue(rsc.exists());
			assertTrue(tmp.exists());
			assertTrue(genSrc.exists());

			JSonBuilderProxy bpf =
					new JSonBuilderProxy()
							.object()
							.key("project-root-directory")
							.value(root.getAbsolutePath())
							.key("test-sources-directory")
							.value(src.getAbsolutePath())
							.key("vendor-binary-directory")
							.value(tmp.getAbsolutePath())
							.key("configuration-directory")
							.value(cfg.getAbsolutePath())
							.key("resource-directory")
							.value(rsc.getAbsolutePath())
							.key("reference-directory")
							.value(src.getAbsolutePath())
							.key("temp-directory")
							.value(tmp.getAbsolutePath())
							.key("reports-directory")
							.value(rpt.getAbsolutePath())
							.key("generated-source-directory")
							.value(genSrc.getAbsolutePath())
							.key("runtimeSupport")
							.object()
							.key("processExecutor")
							.value(LoggingProcessExecutor.class.getName())
							.endObject()
							.key("project-name")
							.value("feature-test")
							.key("project-version")
							.value("1.0-test")
							.key("project-user")
							.value("buildUser")
							.key("project-uid")
							.value("buildUid")
							.key("features")
							.object()
							.endObject()
							.key("options")
								.object()
									.key("etlunit")
										.object()
											.key("executorCount")
												.object()
													.key("integer-value").value(getExecutorCount())
												.endObject()
									.endObject()
								.endObject()
							.endObject();

			ETLTestValueObject etlValue = ETLTestParser.loadObject(bpf.toString());

			bpf = getConfigurationOverrides();

			if (bpf != null)
			{
				etlValue = etlValue.merge(ETLTestParser.loadObject(bpf.toString()), ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.recursive);
			}

			bpf = getRuntimeOptions();

			if (bpf != null)
			{
				ETLTestValueObject optionsValue = ETLTestParser.loadObject(bpf.toString());

				ETLTestValueObjectBuilder etvob = new ETLTestValueObjectBuilder(etlValue);
				etvob.key("options").value(optionsValue);
			}

			etlTestVM = new ETLTestVM(
					new Configuration(etlValue)
			);

			RuntimeSupport runtimeSupport1 = etlTestVM.getRuntimeSupport();
			runtimeSupport1.activateTempFileNamingPolicy(new TestTempFileNamingPolicy());

			List<RuntimeOption> ro = getRuntimeOptionOverrides();

			if (ro != null)
			{
				runtimeSupport1.overrideRuntimeOptions(ro);
			}

			etlTestVM.addFeature(new ConsoleFeatureModule());
			etlTestVM.addFeature(new RunAllFeatureModule());
			// add another feature to provide a context callback
			etlTestVM.addFeature(new DebugCallbacker());

			List<Feature> features = getTestFeatures();

			if (features != null)
			{
				Iterator<Feature> it = features.iterator();

				while (it.hasNext())
				{
					etlTestVM.addFeature(it.next());
				}
			}

			etlTestVM.installFeatures();

			URL processUrl = getClass().getResource("/" + classKey);
			if (processUrl != null)
			{
				runtimeSupport.installProcessExecutor(new MockProcessExecutor(new ScriptMockCallback(classKey)));
			}

			setUpSourceFiles();

			prepareTest();

			results = etlTestVM.runTests();

			assertions(results, pass);

			// before returning, save the process log to a local file
			if (processUrl == null)
			{
				LoggingProcessExecutor proc = (LoggingProcessExecutor) etlTestVM.getRuntimeSupport().getProcessExecutor();

				// before writing, attempt to sanitize the log by replacing the path to the
				// workspace with a static path

				String logText = proc.getProcessLog().toString();

				logText = logText.replace(root.getAbsolutePath(), "[root]/").replace("\\", "/").replace("//", "/");

				File log = new File(tmp, "process.log." + identifier);

				IOUtils.writeBufferToFile(log, new StringBuffer(logText));

				// look for a test resource that matches this test class name

				String logPath = "/" + classKey + ".processLog";

				URL url = getClass().getResource(logPath);
				if (url != null)
				{
					String assertionLog = IOUtils.readURLToString(url);

					if (!assertionLog.equals(logText))
					{
						throw new AssertionError("Process log not correct:\n\nexpected:\n"
								+ assertionLog
								+ "\n\nactual:\n\n"
								+ logText);
					}
				}
			}

			return results;
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	protected void assertions(TestResults results, int pass) {
	}

	protected void initializeTests() {
	}

	protected File useStaticTestRoot()
	{
		return null;
	}

	protected boolean cleanWorkspace()
	{
		return false;
	}

	protected void prepareTest()
	{
	}

	private void assertFalse(boolean exists)
	{
		assertTrue(!exists);
	}

	public final void tearDown() throws Exception
	{
		IOUtils.purge(root, true);
		assertTrue(root.delete());
	}

	private void assertTrue(boolean delete)
	{
		if (!delete)
		{
			throw new IllegalStateException("");
		}
	}

	protected final void createFromClasspath(File target, String resource) throws IOException
	{
		URL url = getClass().getResource("/resource/" + resource);

		IOUtils.writeURLToFile(url, target);
	}

	protected final void createResourceFromClasspath(String feature, String path, String name) throws IOException
	{
		URL url = getClass().getResource("/resource/" + name);

		createResource(feature, path, name, IOUtils.readURLToString(url));
	}

	protected final void createSourceFromClasspath(String _package, String sub, String name) throws IOException
	{
		URL url = getClass().getResource("/source/" + name);

		createSource(_package, sub, name, IOUtils.readURLToString(url));
	}

	protected final void createSourceFromClasspath(String _package, String name) throws IOException
	{
		createSourceFromClasspath(_package, null, name);
	}

	protected void createVendor(String name, String path, String content) throws IOException
	{
		// this looks just like resources.  I can't remember the difference
		//runtimeSupport.//createResource(name, path, content);
		throw new UnsupportedOperationException();
	}

	protected final void createVendorConfig(String vendorName, String name, String content) throws IOException
	{
		StringBuffer stb = new StringBuffer(content);

		File file = new FileBuilder(runtimeSupport.getFeatureConfigurationDirectory(vendorName)).name(name).file();

		IOUtils.writeBufferToFile(file, stb);
	}

	protected final void createTemp(String name, String path, String content) throws IOException
	{
		StringBuffer stb = new StringBuffer(content);

		FileBuilder file = new FileBuilder(runtimeSupport.getTempDirectory());

		if (path != null)
		{
			file = file.subdir(path).mkdirs();
		}
		file.name(name);

		IOUtils.writeBufferToFile(file.file(), stb);
	}

	protected final void createTemp(String name, String content) throws IOException
	{
		createTemp(name, null, content);
	}

	protected final void createSource(String name, String content) throws IOException
	{
		StringBuffer stb = new StringBuffer(content);

		File file = getSource(null, null, name);

		IOUtils.writeBufferToFile(file, stb);
	}

	protected final void createGeneratedSource(String feature, String name, String content) throws IOException
	{
		StringBuffer stb = new StringBuffer(content);
		IOUtils.writeBufferToFile(getGeneratedSource(feature, null, name), stb);
	}

	protected final File getGeneratedSource(String feature, String subdir, String name) throws IOException
	{
		FileBuilder fb = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory(feature));
		if (subdir != null)
		{
			fb = fb.subdir(subdir).mkdirs();
		}

		fb = fb.name(name);

		return fb.file();
	}

	protected final void createGeneratedSource(String feature, String subdir, String name, String content)
			throws IOException
	{
		StringBuffer stb = new StringBuffer(content);
		IOUtils.writeBufferToFile(getGeneratedSource(feature, subdir, name), stb);
	}

	protected final void createSource(String package_, String name, String content) throws IOException
	{
		createSource(package_, null, name, content);
	}

	protected final void createSource(String package_, String subdir, String name, String content) throws IOException
	{
		StringBuffer stb = new StringBuffer(content);

		IOUtils.writeBufferToFile(getSource(package_, subdir, name), stb);
	}

	protected final File getSource(String package_, String subdir, String name) throws IOException
	{
		ETLTestPackage etltEstPackage = ETLTestPackageImpl.getDefaultPackage();

		if (package_ != null && !package_.equals(""))
		{
			etltEstPackage = etltEstPackage.getSubPackage(package_);
		}

		FileBuilder fb = new FileBuilder(
				package_ != null ? runtimeSupport.getTestSourceDirectory(etltEstPackage) : runtimeSupport.getTestSourceDirectory()
		);

		if (subdir != null)
		{
			fb = fb.subdir(subdir);
		}

		File dir = fb.mkdirs().file();

		File file = new File(dir, name);

		return file;
	}

	protected final void createResource(String feature, String relativePath, String name, String content)
			throws IOException
	{
		File
				featSource =
				new FileBuilder(runtimeSupport.getFeatureSourceDirectory(feature)).subdir(relativePath).mkdirs().file();

		StringBuffer stb = new StringBuffer(content);

		File file = new File(featSource, name);

		IOUtils.writeBufferToFile(file, stb);
	}

	protected List<Feature> getTestFeatures()
	{
		return Collections.emptyList();
	}

	protected abstract void setUpSourceFiles() throws IOException;

	protected JSonBuilderProxy getConfigurationOverrides()
	{
		return null;
	}

	@Deprecated
	protected JSonBuilderProxy getRuntimeOptions()
	{
		return null;
	}

	protected List<RuntimeOption> getRuntimeOptionOverrides()
	{
		return null;
	}

	private class DebugCallbacker extends AbstractFeature
	{
		int operationNumber = 0;
		int testNumber = 0;

		@Override
		public StatusReporter getStatusReporter()
		{
			return new NullStatusReporterImpl()
			{
				@Override
				public void testCompleted(ETLTestMethod method, CompletionStatus status)
				{
					assertMethodCompletionStatus(method, status, testNumber, operationNumber);
				}
			};
		}

		@Override
		public ClassListener getListener()
		{
			return new NullClassListener()
			{
				@Override
				public void begin(ETLTestClass cl, VariableContext context, final int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					testNumber = 0;
					assertVariableContextPre(cl, context);
				}

				@Override
				public void begin(ETLTestMethod mt, VariableContext context, final int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					operationNumber = 0;
					assertVariableContextPre(mt, context, testNumber);
					assertVariableContextPre(mt, context);
					testNumber++;
				}

				@Override
				public void end(ETLTestMethod mt, VariableContext context, final int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					assertVariableContextPost(mt, context, testNumber, operationNumber);
					assertVariableContextPost(mt, context, operationNumber);
				}

				@Override
				public void end(ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					assertVariableContextOperation(op, parameters, vcontext, operationNumber);
					operationNumber++;
				}

				@Override
				public void end(ETLTestClass cl, VariableContext context, final int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning
				{
					assertVariableContextPost(cl, context, testNumber);
				}
			};
		}

		@Override
		public void initialize(Injector inj)
		{
			inj.injectMembers(BaseFeatureModuleTest.this);
		}

		@Override
		public String getFeatureName()
		{
			return "debug-caller";
		}

		@Override
		public long getPriorityLevel()
		{
			return Long.MAX_VALUE;
		}
	}

	protected void assertMethodCompletionStatus(ETLTestMethod method, StatusReporter.CompletionStatus status, int testNumber, int operationNumber)
	{
	}

	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, int operationNumber)
	{
	}

	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context)
	{
	}

	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context, int methodNumber)
	{
	}

	protected void assertVariableContextPre(ETLTestClass cl, VariableContext context)
	{
	}

	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int methodNumber, int operationsProcessed)
	{
	}

	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed)
	{
	}

	protected void assertVariableContextPost(ETLTestClass cl, VariableContext context, int testsProcessed)
	{
	}

	/**
	 * If true, the test will be run twice, once with a single executor and once with multiple.
	 * @return
	 */
	protected boolean multiPassSafe()
	{
		return true;
	}

	protected int getExecutorCount()
	{
		return executorCount;
	}
}
