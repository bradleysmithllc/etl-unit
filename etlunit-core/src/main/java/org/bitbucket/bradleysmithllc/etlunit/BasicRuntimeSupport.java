package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.StreamProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.regexp.PackNameExpression;
import org.bitbucket.bradleysmithllc.etlunit.util.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.util.*;

public class BasicRuntimeSupport implements RuntimeSupport {
	private static final TempFileNamingPolicy defaultTempFileNamingPolicy = new TempFileNamingPolicy(){
		@Override
		public String makeAnonymousFileName() {
			return UUID.randomUUID().toString();
		}

		@Override
		public String makeAnonymousFileNameWithExtension(String ext) {
			return makeAnonymousFileName() + "." + ext;
		}

		@Override
		public String makeAnonymousFileNameWithPrefix(String pref) {
			return pref + makeAnonymousFileName();
		}

		@Override
		public String makeAnonymousFolderName() {
			return makeAnonymousFileName();
		}

		@Override
		public String nameTempFolder(String name) {
			return name;
		}

		@Override
		public String nameTempFile(String name) {
			return name;
		}

		@Override
		public File mkTempDirectoryRoot(File tempRoot, int executor) {
			return new FileBuilder(tempRoot).subdir("executor_temp").subdir(String.valueOf(executor)).file();
		}
	};

	private TempFileNamingPolicy tempFileNamingPolicy = defaultTempFileNamingPolicy;

	private final File root;
	private final File tempDir;
	private final File resourceDir;
	private final File referenceDir;
	private final File configDir;
	private final File generatedSourcesDir;
	private final File reportRootDir;

	private MapLocal mapLocal;

	private Log applicationLog;
	private Log userLog;

	private ProcessExecutor processExecutor;
	private final File srcRoot;
	private final String projectName;
	private final String projectVersion;
	private final String projectUser;
	private final File vendorBinaryDir;
	private final String projectUid;
	private LocalDateTime testStartTime;

	private String runId = UUID.randomUUID().toString();

	private final List<Feature> featureList;

	private final Map<String, RuntimeOption> runtimeOptions = new HashMap<String, RuntimeOption>();
	private int executorCount = 1;

	private static final JsonSchema configurationJsonSchema = JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/Configuration.jsonSchema");;

	public BasicRuntimeSupport() {
		this(getSystemTempDir());
	}

	public BasicRuntimeSupport(File proot) {
		featureList = Collections.EMPTY_LIST;

		try {
			this.root = proot.getCanonicalFile();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		this.tempDir = new File(root, "temp");
		tempDir.mkdirs();

		this.reportRootDir = new File(root, "reports");
		reportRootDir.mkdirs();

		vendorBinaryDir = tempDir;

		this.resourceDir = new File(root, "resource");
		resourceDir.mkdirs();

		this.referenceDir = new File(root, "reference");
		referenceDir.mkdirs();

		configDir = new File(resourceDir, "config");
		configDir.mkdirs();

		generatedSourcesDir = new File(root, "build");
		generatedSourcesDir.mkdirs();

		projectName = "proj";
		projectVersion = "1_0";
		projectUser = "builduser";

		// use the my implementation by default
		installProcessExecutor(new StreamProcessExecutor());
		srcRoot = new File(this.root, "src");
		projectUid = readProjectUid(System.getProperty("project.uid")).toUpperCase();

		// use system property for log dir
		System.setProperty("etlunit-log-dir", getReportDirectory("log").getAbsolutePath());

		testStartTime = LocalDateTime.now();
	}

/*
	public BasicRuntimeSupport(File root, File src, File temp, File rsrc, File cfg) {
		this.root = root;
		this.temp = temp;
		this.srcRoot = src;
		resourceDir = rsrc;
		configDir = cfg;

		// use the apache commons exec implementation by default
		installProcessExecutor(new ApacheCommonsStreamsExecutor());
	}
	 */

	public BasicRuntimeSupport(Configuration configuration, ETLTestVM vm) {
		featureList = vm.getFeatures();

		// validate against the schema
		JsonNode configJson = configuration.getRoot().getJsonNode();
		try {
			ProcessingReport report = configurationJsonSchema.validate(configJson);

			if (!report.isSuccess()) {
				throw new RuntimeException("Configuration schema does not validate: " + JsonProcessingUtil.getProcessingReport(report));
			}
		} catch (ProcessingException e) {
			throw new RuntimeException("Configuration schema does not validate: " + JsonProcessingUtil.getProcessingReport(e), e);
		}

		// configuration is valid - proceed
		projectName = EtlUnitStringUtils.sanitize(configuration.query("project-name").getValueAsString(), '_').toLowerCase();
		projectVersion = EtlUnitStringUtils.sanitize(configuration.query("project-version").getValueAsString(), '_').toLowerCase();
		projectUser = EtlUnitStringUtils.sanitize(configuration.query("project-user").getValueAsString(), '_').toLowerCase();

		try {
			root = new File(configuration.query("project-root-directory").getValueAsString()).getCanonicalFile();

			srcRoot = new File(configuration.query("test-sources-directory").getValueAsString()).getCanonicalFile();
			configDir = new File(configuration.query("configuration-directory").getValueAsString()).getCanonicalFile();
			generatedSourcesDir =
					new File(configuration.query("generated-source-directory").getValueAsString()).getCanonicalFile();
			resourceDir = new File(configuration.query("resource-directory").getValueAsString()).getCanonicalFile();
			referenceDir = new File(configuration.query("reference-directory").getValueAsString()).getCanonicalFile();
			tempDir = new File(configuration.query("temp-directory").getValueAsString()).getCanonicalFile();
			reportRootDir = new File(configuration.query("reports-directory").getValueAsString()).getCanonicalFile();

			vendorBinaryDir = new File(configuration.query("vendor-binary-directory").getValueAsString()).getCanonicalFile();

			// use system property for log dir
			System.setProperty("etlunit-log-dir", getReportDirectory("java-logging").getAbsolutePath());
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}

		if (!tempDir.exists()) {
			tempDir.mkdirs();
		}

		if (!vendorBinaryDir.exists()) {
			vendorBinaryDir.mkdirs();
		}

		installProcessExecutor(new StreamProcessExecutor());
		//installProcessExecutor(new Java7ProcessExecutor());

		ETLTestValueObject query = configuration.query("project-uid");
		projectUid = readProjectUid(query != null ? query.getValueAsString() : null);

		ETLTestValueObject optionsNode = configuration.query("options");
		if (optionsNode != null) {
			ObjectMapper om = new ObjectMapper();

			Map<String, ETLTestValueObject> map = optionsNode.getValueAsMap();

			for (Map.Entry<String, ETLTestValueObject> featureEntry : map.entrySet()) {
				// at this level read the actual properties
				String featureName = featureEntry.getKey();
				ETLTestValueObject featureObject = featureEntry.getValue();

				Map<String, ETLTestValueObject> featureMap = featureObject.getValueAsMap();

				for (Map.Entry<String, ETLTestValueObject> optionEntry : featureMap.entrySet()) {
					// at this level read the actual properties
					String optionName = optionEntry.getKey();
					ETLTestValueObject optionObject = optionEntry.getValue();

					if (optionObject != null) {
						JsonNode optionObjectJsonNode = optionObject.getJsonNode();

						if (!optionObjectJsonNode.isNull())
						{
							String json = optionObjectJsonNode.toString();

							try {
								RuntimeOption runtimeOption = om.readValue(json, RuntimeOption.class);
								runtimeOption.setName(featureName + "." + optionName);
								runtimeOption.validate();

								runtimeOptions.put(runtimeOption.getName(), runtimeOption);
							} catch (IOException e) {
								throw new ExceptionInInitializerError(e);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public MapLocal mapLocal() {
		return mapLocal;
	}

	private String readProjectUid(String uid) {
		if (uid != null) {
			return uid;
		}

		try {
			StringBuffer
					buffer =
					new StringBuffer(root.getAbsolutePath()).append("|")
							.append(projectName)
							.append("|")
							.append(projectVersion)
							.append("|");

			InetAddress inet = InetAddress.getLocalHost();

			if (!inet.isLoopbackAddress()) {
				NetworkInterface ni = NetworkInterface.getByInetAddress(inet);

				String mac = Hex.hexEncode(ni.getHardwareAddress());

				buffer.append(mac);
			} else {
				buffer.append(inet.getCanonicalHostName());
			}

			return digestIdentifier(buffer.toString());
		} catch (Exception e) {
			throw new RuntimeException("Error reading local host information", e);
		}
	}

	@Override
	public String processReference(String pathName) {
		if (pathName == null) {
			return pathName;
		}

		// standard variables -
		// 1 - testOperationName
		// 2 - testMethodName
		// 3 - testClassName
		// 4 - testPackageName
		String replacement = "INVALID_OPERATION_REFERENCE";
		ETLTestOperation op = getCurrentlyProcessingTestOperation();
		if (op != null) {
			replacement = op.getOperationName();
		}
		pathName = pathName.replaceAll("\\$\\{testOperationName}", replacement);

		replacement = "INVALID_METHOD_REFERENCE";
		ETLTestMethod meth = getCurrentlyProcessingTestMethod();
		if (meth != null) {
			replacement = meth.getName();
		}
		pathName = pathName.replaceAll("\\$\\{testMethodName}", replacement);

		replacement = "INVALID_CLASS_REFERENCE";
		ETLTestClass clss = getCurrentlyProcessingTestClass();
		if (clss != null) {
			replacement = clss.getName();
		}
		pathName = pathName.replaceAll("\\$\\{testClassName}", replacement);

		ETLTestPackage pkg = getCurrentlyProcessingTestPackage();
		if (pkg != null) {
			pathName = pathName.replaceAll("\\$\\{testPackageName}", pkg.getPackageName());
		}

		return pathName;
	}

	@Override
	public Pair<ETLTestPackage, String> processPackageReference(ETLTestPackage etlTestPackage, String name) {
		// simplistic implementation.  Allow reference to top-level package only

		// this condition isn't here (name == null) because it makes sense.  It's to allow the correct error
		// to be handled downstream.  E.G., ERR_MISSING_TARGET_FILE instead of NPE.
		if (name == null) {
			return ImmutablePair.of(etlTestPackage, name);
		}

		/* Allow a few meaningful expressions here.  First off, all paths are delimited like files, I.E., with '/'
		* Secondly, the default, top-level package is '~', and ~ can only refer to the default package.
		* Thirdly, '..' can refer to one level up from the current package.
		* Thirdly++, '.' refers to the current package.
		* Fourthly, '~' can only appear as the very first package.
		* There are some bizarre but correct possibilities here, such as ~/pack/.., but the linux
		* filesystem allows this as well and so will this framework.
		* The first path element MUST be '~', for the root package, or '..' for a parent package, a subpackage name, or no path
		* which means the current package*/
		String [] pathElements = name.split("/");

		if (pathElements.length > 1) {
			// package path is everything up to the last element

			ETLTestPackage resolvedPackage = etlTestPackage;

			for (int index = 0; index < (pathElements.length - 1); index++) {
				// check for reference to default...
				String reference = pathElements[index].trim();

				// handle root reference
				if (reference.equals("~")) {
					if (index > 0) {
						throw new IllegalStateException("Default, top-level package reference '~' may only appear as the very first path element.  Ref = '" + name + "'");
					}

					resolvedPackage = ETLTestPackageImpl.getDefaultPackage();
				} else if (reference.equals("..")) {
					// handle parent reference
					if (resolvedPackage == ETLTestPackageImpl.getDefaultPackage()) {
						throw new IllegalStateException("Cannot resolve path to package above the top-level package.  Ref = '" + name + "'");
					}

					resolvedPackage = resolvedPackage.getParentPackage();
				} else if (reference.equals(".")) {
					// only here for symmetry.
					// handle self reference.  Legal always
				} else {
					// handle child reference
					resolvedPackage = resolvedPackage.getSubPackage(reference);
				}
			}

			return ImmutablePair.of(resolvedPackage, pathElements[pathElements.length - 1]);
		}

		// defer validation of actual package to downstream processes.  This is similarly to allow for each caller to report the
		// error in a meaningful context.
		return ImmutablePair.of(etlTestPackage, name);
	}

	@Override
	public String digestIdentifier(String id)
	{
		try {
			byte[] bytesOfMessage = id.getBytes("UTF-8");

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);

			// turn it into a number and format as base 36
			BigInteger bd = new BigInteger(thedigest);

			return Long.toString(Math.abs(bd.longValue()), 36);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Inject
	public void setApplicationLogger(@Named("applicationLog") Log log) {
		this.applicationLog = log;

		if (processExecutor != null)
		{
			applicationLog.debug("Using process executor type: " + processExecutor.getClass());
		}
		else
		{
			applicationLog.debug("Using no process executor");
		}
	}

	@Inject
	public void setUserLogger(@Named("userLog") Log log) {
		this.userLog = log;
	}

	public static File getSystemTempDir() {
		return new File(System.getProperty("java.io.tmpdir"));
	}

	@Override
	public void activateTempFileNamingPolicy(TempFileNamingPolicy policy) {
		if (policy == null)
		{
			tempFileNamingPolicy = defaultTempFileNamingPolicy;
		}
		else
		{
			tempFileNamingPolicy = policy;
		}
	}

	@java.lang.Override
	public File resolveFile(File file) {
		// best-case - file exists so return it.
		if (file.exists())
		{
			return file;
		}

		final String name = file.getName().toLowerCase();

		// scan the parent directory of this file and look for a file with a matching name.
		File [] files = file.getParentFile().listFiles(new FileFilter(){
			public boolean accept(File path)
			{
				if (path.getName().toLowerCase().equals(name))
				{
					return true;
				}

				return false;
			}
		});

		// a matching file came back
		if (files != null && files.length == 1)
		{
			// log a nag message
			Log applicationLog1 = getApplicationLog();
			if (applicationLog1 != null)
			{
				applicationLog1.severe("File name case mismatch.  You asked for " + file.getName() + ", but the name on disk was " + files[0].getName());
			}
			return files[0];
		}

		return file;
	}

	@Override
	public File getProjectRoot() throws IOException
	{
		return root;
	}

	@Override
	public String getProjectRelativePath(File fke) throws IOException {
		// determine if the path parameter is truly a subpath of the project root
		String absolutePath = fke.getCanonicalPath();
		String rootAbsolutePath = root.getAbsolutePath();

		// special deal - if the path IS the project root, return '.'
		if (absolutePath.equals(rootAbsolutePath))
		{
			return ".";
		}

		if (!absolutePath.startsWith(rootAbsolutePath))
		{
			throw new IllegalArgumentException("Path is not a subpath of the project root");
		}

		String subPath = absolutePath.substring(rootAbsolutePath.length());

		// unixify the path
		subPath = subPath.replace(File.separator, "/");

		// make sure that this path fragment does not begin with a slash
		if (subPath.startsWith("/"))
		{
			subPath = subPath.substring(1);
		}

		// we don't want it to end with one either
		if (subPath.endsWith("/"))
		{
			subPath = subPath.substring(0, subPath.length() - 1);
		}

		return subPath;
	}

	@Override
	public File getTempRoot() {
		return new FileBuilder(tempDir).mkdirs().file();
	}

	@Override
	public File getTempDirectory() {
		return getTempDirectory(getExecutorId());
	}

	@Override
	public File getTempDirectory(int executor) {
		File path = tempFileNamingPolicy.mkTempDirectoryRoot(getTempRoot(), executor);

		return new FileBuilder(path).mkdirs().file();
	}

	@Override
	public File createTempFolder(String dirname) {
		String name = dirname == null ? tempFileNamingPolicy.makeAnonymousFolderName() : tempFileNamingPolicy.nameTempFolder(dirname);

		File dir = createTempFile(name);

		dir.mkdirs();

		return dir;
	}

	@Override
	public File createAnonymousTempFolder() {
		return createTempFolder(null);
	}

	@Override
	public Log getApplicationLog() {
		return applicationLog;
	}

	@Override
	public Log getUserLog() {
		return userLog;
	}

	@Override
	public String getLocalAddress() {
		InetAddress serverA = null;
		try {
			serverA = InetAddress.getLocalHost();
			return serverA.getHostAddress();
		} catch (UnknownHostException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public int getExecutorCount() {
		return executorCount;
	}

	@Override
	public int getExecutorId() {
		if (getExecutorCount() == 1 || mapLocal == null)
		{
			return 0;
		}
		else
		{
			int executorId = mapLocal.getExecutorId();

			if (executorId == -1)
			{
				return 0;
			}

			return executorId;
		}
	}

	@Override
	public <T> void storeInExecutor(String id, T t) {
		mapLocal.set(id, t);
	}

	@Override
	public <T> T retrieveFromExecutor(String id, Class<T> cls) {
		return mapLocal.get(id, cls);
	}

	@Override
	public <T> void storeInExecutor(Feature owner, String id, T t) {
		mapLocal.set(owner.getFeatureName() + "." + id, t);
	}

	@Override
	public <T> T retrieveFromExecutor(Feature owner, String id, Class<T> cls) {
		return mapLocal.get(owner.getFeatureName() + "." + id, cls);
	}

	@Override
	public void removeStoredFromExecutor(String id) {
		mapLocal.clear(id);
	}

	@Override
	public void removeStoredFromExecutor(Feature owner, String id) {
		mapLocal.clear(owner.getFeatureName() + "." + id);
	}

	@Override
	public String getRunIdentifier() {
		return runId;
	}

	@Override
	public void setTestStartTime()
	{
		setTestStartTime(LocalDateTime.now());
	}

	@Override
	public void setTestStartTime(LocalDateTime testStartTime) {
		this.testStartTime = testStartTime;
	}

	@Override
	public LocalDateTime getTestStartTime() {
		return testStartTime;
	}

	@Override
	public File createTempFile(String name) {
		return createTempFile(null, name);
	}

	@Override
	public File createTempFile(String subdir, String name) {
		FileBuilder fb = new FileBuilder(getTempDirectory());

		if (subdir != null)
		{
			fb.subdir(tempFileNamingPolicy.nameTempFolder(subdir)).mkdirs();
		}

		File tmp;

		if (name == null)
		{
			tmp = fb.name(tempFileNamingPolicy.makeAnonymousFileName()).file();
		}
		else
		{
			tmp = fb.name(tempFileNamingPolicy.nameTempFile(name)).file();
		}

		// make sure the filename doesn't happen to point to a real file
		try {
			if (tmp.exists())
			{
				FileUtils.forceDelete(tmp);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return tmp;
	}

	@Override
	public File createAnonymousTempFileWithPrefix(String name) {
		return createTempFile(tempFileNamingPolicy.makeAnonymousFileNameWithPrefix(name));
	}

	@Override
	public File createAnonymousTempFile() {
		return createAnonymousTempFile(null, null);
	}

	@Override
	public File createAnonymousTempFileWithExtension(String ext) {
		return createAnonymousTempFile(null, ext);
	}

	@Override
	public File createAnonymousTempFileInSubdir(String subdir) {
		return createAnonymousTempFile(subdir, null);
	}

	@Override
	public File createAnonymousTempFile(String subdir, String ext) {
		return createTempFile(subdir, tempFileNamingPolicy.makeAnonymousFileNameWithExtension(ext));
	}

	@Override
	public File getGeneratedSourceDirectoryForCurrentTest(String feature) {
		FileBuilder fb = new FileBuilder(getGeneratedSourceDirectory(getCurrentlyProcessingTestClass().getPackage()));

		fb.subdir(getCurrentlyProcessingTestOperation().getTestClass().getName());
		fb.subdir(getCurrentlyProcessingTestOperation().getTestMethod().getName());
		fb.subdir(getCurrentlyProcessingTestOperation().getOperationName() + "_" + getCurrentlyProcessingTestOperation().getOrdinal());

		return fb.subdir(feature).mkdirs().file();
	}

	@Override
	public File getGeneratedSourceDirectory() {
		return new FileBuilder(generatedSourcesDir).mkdirs().file();
	}

	@Override
	public File getGeneratedSourceDirectory(String feature) {
		return new FileBuilder(getGeneratedSourceDirectory()).subdir(feature).mkdirs().file();
	}

	@Override
	public File createGeneratedSourceFile(String feature, String name) {
		return new File(getGeneratedSourceDirectory(feature), name);
	}

	@Override
	public File getFeatureSourceDirectory(String feature) {
		return new FileBuilder(resourceDir).subdir(feature).mkdirs().file();
	}

	@Override
	public File getSourceDirectory() {
		return srcRoot;
	}

	@Override
	public File getTestSourceDirectory() {
		return getTestSourceDirectory(null);
	}

	@Override
	public File getGeneratedSourceDirectory(ETLTestPackage _package) {
		FileBuilder fb = new FileBuilder(getGeneratedSourceDirectory());

		if (_package != null && !_package.isDefaultPackage()) {
			// split so deep packages can be handled
			String[] packageNames = _package.getPackagePath();

			for (String pack : packageNames) {
				fb = fb.subdir("_" + pack + "_");
			}
		}

		return fb.file();
	}

	@Override
	public File getTestSourceDirectory(ETLTestPackage _package) {
		FileBuilder fb = new FileBuilder(getSourceDirectory());

		if (_package != null && !_package.isDefaultPackage()) {
			// split so deep packages can be handled
			String[] packageNames = _package.getPackagePath();

			for (String pack : packageNames) {
				fb = fb.subdir("_" + pack + "_");
			}
		}

		return fb.file();
	}

	@Override
	public File getCurrentTestSourceDirectory() {
		return getTestSourceDirectory(getCurrentlyProcessingTestPackage());
	}

	@Override
	public ETLTestPackage
	getCurrentlyProcessingTestPackage() {
		ETLTestClass currentlyProcessingTestClass = getCurrentlyProcessingTestClass();
		return currentlyProcessingTestClass != null ? currentlyProcessingTestClass.getPackage() : ETLTestPackageImpl.getDefaultPackage();
	}

	@Override
	public ETLTestClass getCurrentlyProcessingTestClass() {
		return mapLocal.getCurrentlyProcessingTestClass();
	}

	@Override
	public ETLTestMethod getCurrentlyProcessingTestMethod() {
		return mapLocal.getCurrentlyProcessingTestMethod();
	}

	@Override
	public ETLTestOperation getCurrentlyProcessingTestOperation() {
		return mapLocal.getCurrentlyProcessingTestOperation();
	}

	@Override
	public VariableContext getCurrentlyProcessingVariableContext() {
		return mapLocal.getCurrentlyProcessingVariableContext();
	}

	@Override
	public File getTestResourceDirectory(String subdir) {
		return getTestResourceDirectory(null, subdir);
	}

	@Override
	public File getTestResourceDirectory(ETLTestPackage _package, String subdir) {
		File packRoot = getTestSourceDirectory(_package);

		return new FileBuilder(packRoot).subdir(subdir).mkdirs().file();
	}

	@Override
	public File getCurrentTestResourceDirectory(String subdir) {
		return getTestResourceDirectory(getCurrentlyProcessingTestClass().getPackage(), subdir);
	}

	@Override
	public File getGlobalReportDirectory(String type) {
		return new FileBuilder(reportRootDir).subdir(type + "-reports").mkdirs().file();
	}

	@Override
	public File getReportDirectory(String type) {
		return new FileBuilder(getGlobalReportDirectory(type)).subdir(getRunIdentifier()).mkdirs().file();
	}

	@Override
	public String getProjectName() {
		return projectName;
	}

	@Override
	public String getProjectVersion() {
		return projectVersion;
	}

	@Override
	public String getProjectUser() {
		return projectUser;
	}

	@Override
	public String getProjectUID() {
		return getProjectUID(getExecutorId());
	}

	@Override
	public String getProjectUID(int executor) {
		return projectUid + "_" + executor;
	}

	@Override
	public void overrideRuntimeOption(RuntimeOption runtimeOption) {
		runtimeOptions.put(runtimeOption.getName(), runtimeOption);

		// hack
		if (runtimeOption.getName().equals("etlunit.executorCount"))
		{
			executorCount = runtimeOption.getIntegerValue();
		}
	}

	@Override
	public void overrideRuntimeOptions(List<RuntimeOption> runtimeOptions) {
		for (RuntimeOption option : runtimeOptions) {
			overrideRuntimeOption(option);
		}
	}

	@Override
	public File getFeatureConfigurationDirectory(String feature) {
		return new FileBuilder(configDir).subdir(feature).mkdirs().file();
	}

	@Override
	public File getSourceDirectory(ETLTestPackage package_) {
		if (package_.isDefaultPackage()) {
			return getSourceDirectory();
		}

		FileBuilder fb = new FileBuilder(getSourceDirectory());

		for (String path : package_.getPackagePath())
		{
			fb = fb.subdir(path);
		}

		return fb.mkdirs().file();
	}

	@Override
	public ProcessExecutor getProcessExecutor() {
		return processExecutor;
	}

	@Override
	public File createSourceFile(ETLTestPackage feature, String name) {
		return new File(getSourceDirectory(feature), name);
	}

	@Override
	public File getReferenceDirectory(String path_) {
		FileBuilder fb = new FileBuilder(referenceDir);
		if (path_ != null)
		{
			fb = fb.subdir(path_).mkdirs();
		}

		return fb.file();
	}

	@Override
	public File getReferenceFile(String path_, String name) {
		return resolveFile(new File(getReferenceDirectory(path_), name));
	}

	@Override
	public File getReferenceFile(String path_) {
		int lastSeparator = path_.lastIndexOf("/");

		if (lastSeparator == -1)
		{
			return getReferenceFile(null, path_);
		}

		return getReferenceFile(processReference(path_).substring(0, lastSeparator), path_.substring(lastSeparator + 1));
	}

	@Override
	public URL getReferenceResource(String path_, String name) {
		File ref = getReferenceFile(path_, name);

		if (ref.exists())
		{
			try {
				return ref.toURL();
			} catch (MalformedURLException e) {
			}
		}

		return ResourceUtils.getResource(getClass(), "reference/" + path_ + "/" + name);
	}

	@Override
	public URL getReferenceResource(String path_) {
		// check the local file first
		File f = getReferenceFile(path_);

		if (f.exists())
		{
			try {
				return f.toURL();
			} catch (MalformedURLException e) {
			}
		}

		return ResourceUtils.getResource(getClass(), "reference/" + path_);
	}

	@Incomplete
	@NeedsTest
	@Override
	public void installProcessExecutor(ProcessExecutor od) {
		if (applicationLog != null)
		{
			if (od != null)
			{
				applicationLog.debug("Using process executor type: " + od.getClass());
			}
			else
			{
				applicationLog.debug("Using no process executor");
			}
		}

		processExecutor = od;
	}

	@Incomplete
	@NeedsTest
	public ProcessFacade execute(final ProcessDescription pd) throws IOException {
		applicationLog.info("Executing process: " + pd.debugString());

		// assign a private working folder and output
		File processFrames = createTempFile("processFrames", null);
		processFrames.mkdirs();

		pd.privateWorkingDir(processFrames);

		File processOutput = new File(processFrames, "log");

		pd.privateOutput(processOutput);

		if (processExecutor != null) {
			return processExecutor.execute(pd);
		}

		ProcessBuilder pb = new ProcessBuilder(pd.getCommand());
		if (pd.hasArguments()) {
			pb = pb.command(pd.getArguments());
		}

		pb.environment().putAll(pd.getEnvironment());


		final Process proc = pb.start();

		return new ProcessFacade() {
			@Override
			public ProcessDescription getDescriptor() {
				return pd;
			}

			public Process getProcess() {
				return proc;
			}

			@Override
			public void waitForCompletion() {
				try {
					proc.waitFor();
				} catch (InterruptedException e) {
					throw new IllegalStateException("Process interrupted");
				}
			}

			@Override
			public int getCompletionCode() {
				return proc.exitValue();
			}

			@Override
			public void kill() {
				proc.destroy();
			}

			@Override
			public void waitForStreams() {
				//these are always available
			}

			@Override
			public void waitForOutputStreamsToComplete() {
			}

			@Override
			public Writer getWriter() {
				return new BufferedWriter(new OutputStreamWriter(getOutputStream()));
			}

			@Override
			public BufferedReader getReader() {
				return new BufferedReader(new InputStreamReader(getInputStream()));
			}

			@Override
			public BufferedReader getErrorReader() {
				return new BufferedReader(new InputStreamReader(getErrorStream()));
			}

			@Override
			public StringBuffer getInputBuffered() throws IOException {
				return buffer(getReader());
			}

			@Override
			public StringBuffer getErrorBuffered() throws IOException {
				return buffer(getErrorReader());
			}

			private StringBuffer buffer(BufferedReader errorReader) throws IOException {
				StringBuffer stb = new StringBuffer();

				String line = null;

				int lineC = 0;

				while ((line = errorReader.readLine()) != null) {
					if (lineC != 0) {
						stb.append('\n');
					}

					lineC++;

					stb.append(line);
				}

				return stb;
			}

			@Override
			public OutputStream getOutputStream() {
				return proc.getOutputStream();
			}

			@Override
			public InputStream getInputStream() {
				return proc.getInputStream();
			}

			@Override
			public InputStream getErrorStream() {
				return proc.getErrorStream();
			}
		};
	}

	@Override
	public RuntimeOption getRuntimeOption(String qualifiedName) {
		return runtimeOptions.get(qualifiedName);
	}

	@Override
	public List<RuntimeOption> getRuntimeOptions(String featureName) {
		List<RuntimeOption> options = new ArrayList<RuntimeOption>();

		for (Map.Entry<String, RuntimeOption> entry : runtimeOptions.entrySet()) {
			String key = entry.getKey();
			RuntimeOption value = entry.getValue();

			if (featureName == null || key.startsWith(featureName + ".")) {
				options.add(value);
			}
		}

		return options;
	}

	@Override
	public File getVendorBinaryDir(String path) {
		return new FileBuilder(vendorBinaryDir).subdir(path).file();
	}

	@Override
	public File getVendorBinaryDir() {
		return vendorBinaryDir;
	}

	@Override
	public List<RuntimeOption> getRuntimeOptions() {
		return getRuntimeOptions(null);
	}

	private void getPackages(File root, final ETLTestPackage package_, final List<ETLTestPackage> packages) {
		root.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				if (pathname.isDirectory()) {
					PackNameExpression pne = new PackNameExpression(pathname.getName());

					if (pne.matches()) {
						ETLTestPackage pack = package_.getSubPackage(pne.getPackName());
						packages.add(pack);

						getPackages(pathname, pack, packages);
					}
				}

				return false;
			}
		});
	}

	@Override
	public List<ETLTestPackage> getPackages(File root) {
		final List<ETLTestPackage> packages = new ArrayList<ETLTestPackage>();

		// add a reference to the default package
		ETLTestPackage defaultPackage = ETLTestPackageImpl.getDefaultPackage();
		packages.add(defaultPackage);

		getPackages(root, defaultPackage, packages);

		return packages;
	}

	@Override
	public List<ETLTestPackage> getReferencePackages(String path) {
		File referenceDirectory = getReferenceDirectory(path);
		return getPackages(referenceDirectory);
	}

	@Override
	public List<ETLTestPackage> getTestPackages() {
		return getPackages(getTestSourceDirectory());
	}

	@Override
	public boolean isTestActive() {
		return getCurrentlyProcessingTestClass() != null;
	}

	public void setMapLocal(MapLocal mapLocal) {
		this.mapLocal = mapLocal;
	}

	public void setExecutorCount(int count)
	{
		executorCount = count;
	}
}
