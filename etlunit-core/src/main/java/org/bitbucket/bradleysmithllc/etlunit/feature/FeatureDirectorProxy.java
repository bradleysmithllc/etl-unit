package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

import java.util.List;

public class FeatureDirectorProxy implements ClassDirector
{
	private final FeatureProxyBase<ClassDirector> base;

	public FeatureDirectorProxy(List<Feature> features)
	{
		base = new FeatureProxyBase<ClassDirector>(features, new FeatureProxyBase.Getter<ClassDirector>()
		{
			public ClassDirector get(Feature f)
			{
				return f.getDirector();
			}
		});
	}

	private static interface FeatureDirector
	{
		response_code broadcast(ClassDirector director);
	}

	private response_code broadcast(FeatureDirector dire)
	{
		response_code ret = response_code.defer;

		for (ClassDirector direct : base.getList())
		{
			ret = dire.broadcast(direct);

			// reject and handle break the chain
			if (ret == response_code.reject || ret == response_code.accept)
			{
				break;
			}
		}

		return ret;
	}

	@Override
	public void beginBroadcast()
	{
		broadcast(new FeatureDirector()
		{
			@Override
			public response_code broadcast(ClassDirector director)
			{
				director.beginBroadcast();

				return response_code.defer;
			}
		});
	}

	@Override
	public response_code accept(final ETLTestClass cl)
	{
		return broadcast(new FeatureDirector()
		{
			@Override
			public response_code broadcast(ClassDirector director)
			{
				return director.accept(cl);
			}
		});
	}

	@Override
	public response_code accept(final ETLTestMethod mt)
	{
		return broadcast(new FeatureDirector()
		{
			@Override
			public response_code broadcast(ClassDirector director)
			{
				return director.accept(mt);
			}
		});
	}

	@Override
	public response_code accept(final ETLTestOperation op)
	{
		return broadcast(new FeatureDirector()
		{
			@Override
			public response_code broadcast(ClassDirector director)
			{
				return director.accept(op);
			}
		});
	}

	@Override
	public void endBroadcast()
	{
		broadcast(new FeatureDirector()
		{
			@Override
			public response_code broadcast(ClassDirector director)
			{
				director.endBroadcast();

				return response_code.defer;
			}
		});
	}
}
