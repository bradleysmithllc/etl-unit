package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileDataImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;

import java.io.FileNotFoundException;
import java.io.Reader;
import java.util.List;

public class ReaderFileDataImpl extends DataFileDataImpl {
	private final DataSetReader reader;

	public ReaderFileDataImpl(DataFileSchema schema, DataSetReader source, List<String> columns)
	{
		super(columns, schema);
		reader = source;
	}

	public ReaderFileDataImpl(DataFileSchema schema, DataSetReader source)
	{
		super(null, schema);
		reader = source;
	}

	@Override
	protected Reader getRowData() throws FileNotFoundException {
		return reader;
	}

	public boolean atEOF()
	{
		return reader.atEOF();
	}
}
