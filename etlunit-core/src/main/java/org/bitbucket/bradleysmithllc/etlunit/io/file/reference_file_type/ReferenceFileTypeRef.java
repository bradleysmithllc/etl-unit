package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;

import java.util.List;
import java.util.Objects;

public final class ReferenceFileTypeRef
{
	private static final String NULL_STR = "NULL~ATTR";

	private final String id;
	private final String classifier;
	private final String version;
	private ref_type refType;

	public enum ref_type
	{
		local,
		remote
	}

	public enum column_list_mode
	{
		include,
		exclude,
		none
	}

	private column_list_mode columnListMode = column_list_mode.none;
	private List<String> columns;

	private ReferenceFileTypeRef(String id, String classifier, String version, ref_type type) {
		this.id = id;
		this.classifier = classifier;
		this.version = version;
		this.refType = type;

		if (this.id == null)
		{
			throw new IllegalArgumentException("Cannot create ref with null id");
		}

		if (this.classifier == null)
		{
			throw new IllegalArgumentException("Cannot create ref with null classifier");
		}

		if (this.version == null)
		{
			throw new IllegalArgumentException("Cannot create ref with null version");
		}
	}

	public static ReferenceFileTypeRef refWithId(String id) {
		return new ReferenceFileTypeRef(ObjectUtils.firstNotNull(id, NULL_STR), NULL_STR, NULL_STR, ref_type.local);
	}

	public static ReferenceFileTypeRef refWithIdAndClassifier(String id, String classifier) {
		return new ReferenceFileTypeRef(ObjectUtils.firstNotNull(id, NULL_STR), classifier, NULL_STR, ref_type.local);
	}

	public static ReferenceFileTypeRef refWithIdAndVersion(String id, String version) {
		return new ReferenceFileTypeRef(ObjectUtils.firstNotNull(id, NULL_STR), NULL_STR, version, ref_type.local);
	}

	public static ReferenceFileTypeRef refWithIdClassifierAndVersion(String id, String classifier, String version) {
		return new ReferenceFileTypeRef(ObjectUtils.firstNotNull(id, NULL_STR), classifier, version, ref_type.local);
	}

	public static ReferenceFileTypeRef remoteRefWithId(String id) {
		return new ReferenceFileTypeRef(ObjectUtils.firstNotNull(id, NULL_STR), NULL_STR, NULL_STR, ref_type.remote);
	}

	public static ReferenceFileTypeRef remoteRefWithIdAndClassifier(String id, String classifier) {
		return new ReferenceFileTypeRef(ObjectUtils.firstNotNull(id, NULL_STR), classifier, NULL_STR, ref_type.remote);
	}

	public static ReferenceFileTypeRef remoteRefWithIdAndVersion(String id, String version) {
		return new ReferenceFileTypeRef(ObjectUtils.firstNotNull(id, NULL_STR), NULL_STR, version, ref_type.remote);
	}

	public static ReferenceFileTypeRef nullableRefWithIdClassifierAndVersion(String id, String classifier, String version) {
		return new ReferenceFileTypeRef(
			ObjectUtils.firstNotNull(id, NULL_STR),
			ObjectUtils.firstNotNull(classifier, NULL_STR),
			ObjectUtils.firstNotNull(version, NULL_STR),
			ref_type.local);
	}

	public static ReferenceFileTypeRef remoteRefWithIdClassifierAndVersion(String id, String classifier, String version) {
		return new ReferenceFileTypeRef(ObjectUtils.firstNotNull(id, NULL_STR), classifier, version, ref_type.remote);
	}

	public ReferenceFileTypeRef withId(String id)
	{
		return refWithIdClassifierAndVersion(id, classifier, version).copy(this);
	}

	private ReferenceFileTypeRef copy(ReferenceFileTypeRef referenceFileTypeRef) {
		columnListMode = referenceFileTypeRef.columnListMode;
		columns = referenceFileTypeRef.columns;
		refType = referenceFileTypeRef.refType;

		return this;
	}

	public ReferenceFileTypeRef withClassifier(String cls)
	{
		return refWithIdClassifierAndVersion(id, cls, version).copy(this);
	}

	public String getId() {
		return id;
	}

	public boolean hasId()
	{
		return id != NULL_STR;
	}

	public boolean hasClassifier()
	{
		return classifier != NULL_STR;
	}

	public String getClassifier() {
		return classifier;
	}

	public boolean hasVersion()
	{
		return version != NULL_STR;
	}

	public String getVersion() {
		return version;
	}

	public static ReferenceFileTypeRef refFromRequest(ETLTestValueObject query, ref_type type) {
		if (query.getValueType() != ETLTestValueObject.value_type.object)
		{
			return type == ref_type.local ? refWithId(query.getValueAsString()) : remoteRefWithId(query.getValueAsString());
		}

		// object - get the three parameters
		ETLTestValueObject query1 = query.query("id");
		String id = query1 != null ? query1.getValueAsString() : NULL_STR;

		query1 = query.query("classifier");
		String classifier = query1 != null ? query1.getValueAsString() : NULL_STR;

		query1 = query.query("version");
		String version = query1 != null ? query1.getValueAsString() : NULL_STR;

		ReferenceFileTypeRef referenceFileTypeRef =  type == ref_type.local ? refWithIdClassifierAndVersion(id, classifier, version) : remoteRefWithIdClassifierAndVersion(id, classifier, version);

		ETLTestValueObject query2 = query.query("column-list-mode");
		if (query2 != null && !query2.isNull())
		{
			referenceFileTypeRef.columnListMode = column_list_mode.valueOf(query2.getValueAsString());
		}

		query2 = query.query("column-list");
		if (query2 != null && !query2.isNull())
		{
			referenceFileTypeRef.columns = query2.getValueAsListOfStrings();

				// default mode is exclude
			if (referenceFileTypeRef.columnListMode == column_list_mode.none)
			{
				referenceFileTypeRef.columnListMode = column_list_mode.exclude;
			}
		}

		// sanity check - column list mode, but there is no column list
		if (referenceFileTypeRef.columnListMode != column_list_mode.none)
		{
			if (referenceFileTypeRef.columns == null)
			{
				throw new IllegalArgumentException("Do not provide a column list without columns");
			}
			else if (referenceFileTypeRef.getColumns().size() == 0)
			{
				throw new IllegalArgumentException("Empty column list");
			}
		}

		return referenceFileTypeRef;
	}

	public column_list_mode getColumnListMode() {
		return columnListMode;
	}

	public List<String> getColumns() {
		return columns;
	}

	@Override
	public String toString() {
		return "ReferenceFileTypeRef{" +
				"id='" + id + '\'' +
				", classifier='" + classifier + '\'' +
				", refType='" + refType + '\'' +
				", version='" + version + '\'' +
				", columnListMode=" + columnListMode +
				", columns=" + columns +
				'}';
	}

	public String toBriefString() {
		return "ref: {"
				+ (id == NULL_STR ? "" : ("id: '" + id + "'"))
				+ (classifier == NULL_STR ? "" : ("classifier: '" + classifier + "'"))
				+ (version == NULL_STR ? "" : ("version: '" + version + "'"))
				+ (columnListMode == column_list_mode.none ? "" : ("columnListMode: '" + columnListMode + "'"))
				+ (columns == null ? "" : ("columns: '" + columns + "'"))
		+ "}";
	}

	public String toRefString()
	{
		return getId() + (hasClassifier() ? ("~" + getClassifier()) : "") + (hasVersion() ? ("+" + getVersion()) : "");
	}

	public ref_type getRefType() {
		return refType;
	}

	public ReferenceFileTypeRef withVersion(String s) {
		return ReferenceFileTypeRef.refWithIdClassifierAndVersion(id, classifier, s);
	}

	public ReferenceFileTypeRef withoutVersion() {
		return ReferenceFileTypeRef.refWithIdClassifierAndVersion(id, classifier, NULL_STR);
	}

	public ReferenceFileTypeRef withoutClassifier() {
		return ReferenceFileTypeRef.refWithIdClassifierAndVersion(id, NULL_STR, version);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ReferenceFileTypeRef that = (ReferenceFileTypeRef) o;
		return Objects.equals(id, that.id) &&
				Objects.equals(classifier, that.classifier) &&
				Objects.equals(version, that.version) &&
				refType == that.refType &&
				columnListMode == that.columnListMode &&
				Objects.equals(columns, that.columns);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, classifier, version, refType, columnListMode, columns);
	}
}
