package org.bitbucket.bradleysmithllc.etlunit.test_support;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.input.NullReader;
import org.apache.commons.io.output.NullOutputStream;
import org.bitbucket.bradleysmithllc.etlunit.io.NullInputStream;
import org.bitbucket.bradleysmithllc.etlunit.io.NullWriter;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class ScriptRunner implements MockProcessExecutor.MockProcessFacade
{
	private final int exitCode;
	private final long minimumWait;
	private final long streamsMinimumDelay;

	private final Semaphore threadsStarted = new Semaphore(4);

	private final Semaphore processComplete = new Semaphore(1);
	private final Semaphore streamsComplete = new Semaphore(1);

	private final Semaphore aComplete = new Semaphore(1);

	private final Semaphore scriptsComplete = new Semaphore(2);

	private static final class Command
	{
		enum type
		{
			wait,
			print,
			println
		}

		private final type commandType;
		private final int waitTime;
		private final String message;

		Command(String message, boolean flush)
		{
			this.message = message;
			commandType = flush ? type.println : type.print;
			waitTime = -1;
		}

		Command(int time)
		{
			this.message = null;
			commandType = type.wait;
			waitTime = time;
		}
	}

	private final class ScriptThread implements Runnable
	{
		final PipedOutputStream pipout;
		final PrintStream prout;

		final InputStream stdout;
		final BufferedReader stdoutreader;

		final StringWriter swriter = new StringWriter();
		final PrintWriter srout = new PrintWriter(swriter);

		private final List<Command> commands = new ArrayList<Command>();

		ScriptThread()
		{
			stdout = new NullInputStream();
			stdoutreader = new BufferedReader(new NullReader(100));
			pipout = null;
			prout = null;
		}

		public ScriptThread(ETLTestValueObject query)
		{
			// in this case the reader and input must be pipes
			pipout = new PipedOutputStream();
			prout = new PrintStream(pipout);

			try
			{
				stdout = new PipedInputStream(pipout);
				stdoutreader = new BufferedReader(new InputStreamReader(stdout));
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}

			// this is an array of 0 or more objects.  Each one is a command, either wait or send
			for (ETLTestValueObject object : query.getValueAsList())
			{
				ETLTestValueObject print = object.query("print");
				if (print != null)
				{
					// grab me some print
					commands.add(new Command(print.getValueAsString(), false));
				}
				else
				{
					ETLTestValueObject println = object.query("println");
					if (println != null)
					{
						// grab me some println
						commands.add(new Command(println.getValueAsString(), true));
					}
					else
					{
						ETLTestValueObject wait = object.query("wait");
						if (wait != null)
						{
							// grab me some send
							commands.add(new Command(Integer.parseInt(wait.getValueAsString())));
						}
					}
				}
			}
		}

		public void run()
		{
			threadsStarted.release();

			for (Command command : commands)
			{
				switch (command.commandType)
				{
					case wait:
						try
						{
							Thread.sleep(command.waitTime);
						}
						catch (InterruptedException e)
						{
							throw new RuntimeException(e);
						}
						break;
					case println:
						// save a copy in the buffer
						srout.println(command.message);
						srout.flush();

						prout.println(command.message);
						prout.flush();

						break;
					case print:
						// save a copy in the buffer
						srout.print(command.message);
						srout.flush();

						prout.print(command.message);
						prout.flush();

						break;
				}
			}

			try
			{
				if (pipout != null)
				{
					pipout.close();
				}
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}

			scriptsComplete.release();
		}
	}

	private ScriptThread inputStreamThread = new ScriptThread();
	private ScriptThread errorStreamThread = new ScriptThread();

	public ScriptRunner(ETLTestValueObject commandImpl)
	{
		threadsStarted.acquireUninterruptibly(4);
		scriptsComplete.acquireUninterruptibly(2);
		aComplete.acquireUninterruptibly();

		ETLTestValueObject query = commandImpl.query("exit-code");

		if (query != null)
		{
			exitCode = Integer.valueOf(query.getValueAsString());
		}
		else
		{
			exitCode = 0;
		}

		query = commandImpl.query("minimum-run-time");

		if (query != null)
		{
			minimumWait = Long.valueOf(query.getValueAsString());
		}
		else
		{
			minimumWait = 0L;
		}

		query = commandImpl.query("streams-available-delay");

		if (query != null)
		{
			streamsMinimumDelay = Long.valueOf(query.getValueAsString());
		}
		else
		{
			streamsMinimumDelay = 0L;
		}

		if (minimumWait != 0L)
		{
			processComplete.acquireUninterruptibly();

			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					threadsStarted.release();

					try
					{
						Thread.sleep(minimumWait);
						processComplete.release();
					}
					catch (InterruptedException e)
					{
						throw new RuntimeException(e);
					}
				}
			}).start();
		}
		else
		{
			threadsStarted.release();
		}

		if (streamsMinimumDelay != 0L)
		{
			streamsComplete.acquireUninterruptibly();

			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					threadsStarted.release();

					try
					{
						Thread.sleep(streamsMinimumDelay);
						streamsComplete.release();
					}
					catch (InterruptedException e)
					{
						throw new RuntimeException(e);
					}
				}
			}).start();
		}
		else
		{
			threadsStarted.release();
		}

		query = commandImpl.query("input-script");

		if (query != null)
		{
			inputStreamThread = new ScriptThread(query);
		}
		else
		{
			inputStreamThread = new ScriptThread();
		}

		query = commandImpl.query("error-script");

		if (query != null)
		{
			errorStreamThread = new ScriptThread(query);
		}
		else
		{
			errorStreamThread = new ScriptThread();
		}

		new Thread(inputStreamThread).start();
		new Thread(errorStreamThread).start();

		threadsStarted.acquireUninterruptibly(4);
	}

	@Override
	public void mockWaitForProcess()
	{
		if (aComplete.availablePermits() == 0)
		{
			// streams must be available before the process can complete
			mockWaitForStreams();

			// drain the streams to free any blocking script threads
			try
			{
				byte[] buff = new byte[1024];

				while (inputStreamThread.stdout.read(buff) != -1)
				{
					;
				}
				while (errorStreamThread.stdout.read(buff) != -1)
				{
					;
				}

				char[] cbuff = new char[1024];

				while (inputStreamThread.stdoutreader.read(cbuff) != -1)
				{
					;
				}
				while (errorStreamThread.stdoutreader.read(cbuff) != -1)
				{
					;
				}
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}

			// scripts must be complete
			if (scriptsComplete.availablePermits() != 2)
			{
				scriptsComplete.acquireUninterruptibly(2);
			}

			// acquire in order to block, then release so the exit code knows we have finished
			processComplete.acquireUninterruptibly();
			processComplete.release();
			aComplete.release();
		}
	}

	@Override
	public int mockCompletionCode()
	{
		// this is what java lang Process does
		if (processComplete.availablePermits() != 1)
		{
			throw new IllegalThreadStateException();
		}

		return exitCode;
	}

	@Override
	public void mockKill()
	{
		processComplete.release();
	}

	@Override
	public void mockWaitForStreams()
	{
		streamsComplete.acquireUninterruptibly();
		streamsComplete.release();
	}

	@Override
	public Writer getWriter()
	{
		return new NullWriter();
	}

	@Override
	public BufferedReader getReader()
	{
		return inputStreamThread.stdoutreader;
	}

	@Override
	public BufferedReader getErrorReader()
	{
		return errorStreamThread.stdoutreader;
	}

	@Override
	public StringBuffer getInputBuffer()
	{
		mockWaitForProcess();

		return new StringBuffer(inputStreamThread.swriter.toString());
	}

	@Override
	public StringBuffer getErrorBuffer()
	{
		mockWaitForProcess();

		return new StringBuffer(errorStreamThread.swriter.toString());
	}

	@Override
	public OutputStream getOutputStream()
	{
		return new NullOutputStream();
	}

	@Override
	public InputStream getInputStream()
	{
		return inputStreamThread.stdout;
	}

	@Override
	public InputStream getErrorStream()
	{
		return errorStreamThread.stdout;
	}
}
