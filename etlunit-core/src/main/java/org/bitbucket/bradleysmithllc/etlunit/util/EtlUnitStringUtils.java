package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang.StringEscapeUtils;

import java.io.*;
import java.text.Collator;
import java.util.*;

public class EtlUnitStringUtils
{
	/**
	 * These are java keywords as specified at the following URL (sorted alphabetically).
	 * http://java.sun.com/docs/books/jls/second_edition/html/lexical.doc.html#229308
	 * Note that false, true, and null are not strictly keywords; they are literal values,
	 * but for the purposes of this array, they can be treated as literals.
	 * ****** PLEASE KEEP THIS LIST SORTED IN ASCENDING ORDER ******
	 */
	static final String keywords[] =
			{
					"abstract", "assert", "boolean", "break", "byte", "case",
					"catch", "char", "class", "const", "continue",
					"default", "do", "double", "else", "extends",
					"false", "final", "finally", "float", "for",
					"goto", "if", "implements", "import", "instanceof",
					"int", "interface", "long", "native", "new",
					"null", "package", "private", "protected", "public",
					"return", "short", "static", "strictfp", "super",
					"switch", "synchronized", "this", "throw", "throws",
					"transient", "true", "try", "void", "volatile",
					"while"
			};

	/**
	 * Collator for comparing the strings
	 */
	static final Collator englishCollator = Collator.getInstance(Locale.ENGLISH);

	public interface Justifier
	{
		String justify(String str, int length);
	}

	public static boolean isAsciiPrintable(char ch)
	{
		return ch >= 32 && ch < 127;
	}

	public static String encodeNonPrintable(String source)
	{
		StringBuilder strb = new StringBuilder();

		for (char ch : source.toCharArray())
		{
			if (
					isAsciiPrintable(ch)
					)
			{
				strb.append((char) ch);
			}
			else
			{
				switch (ch)
				{
					case '\\':
						strb.append("\\\\");
						break;
					case '\r':
						strb.append("\\r");
						break;
					case '\n':
						strb.append("\\n");
						break;
					case '\t':
						strb.append("\\t");
						break;
					case '\0':
						strb.append("\\0");
						break;
					case '\f':
						strb.append("\\f");
						break;
					case '\b':
						strb.append("\\b");
						break;
					default:
					{
						strb.append("0x");
						strb.append(Integer.toHexString(ch));
					}
				}
			}
		}

		return strb.toString();
	}

	public static String decodeNonPrintable(String source)
	{
		StringBuilder strb = new StringBuilder();

		for (char ch : source.toCharArray())
		{
			if (
					isAsciiPrintable(ch)
					)
			{
				strb.append((char) ch);
			}
			else
			{
				switch (ch)
				{
					case '\\':
						strb.append("\\\\");
						break;
					case '\r':
						strb.append("\\r");
						break;
					case '\n':
						strb.append("\\n");
						break;
					case '\t':
						strb.append("\\t");
						break;
					case '\0':
						strb.append("\\0");
						break;
					case '\f':
						strb.append("\\f");
						break;
					case '\b':
						strb.append("\\b");
						break;
					default:
					{
						strb.append("0x");
						strb.append(Integer.toHexString(ch));
					}
				}
			}
		}

		return strb.toString();
	}

	public static String stripSpaces(String source)
	{
		StringBuilder strb = new StringBuilder();

		for (int ch : source.toCharArray())
		{
			if (ch == ' ')
			{
				continue;
			}

			strb.append((char) ch);
		}

		return strb.toString();
	}

	private static class RightJustifier implements Justifier
	{
		@Override
		public String justify(String str, int length)
		{
			if (str.length() < length)
			{
				StringBuilder sb = new StringBuilder();

				int spacesRequired = length - str.length();
				for (int i = 0; i < spacesRequired; i++)
				{
					sb.append(' ');
				}

				sb.append(str);

				return sb.toString();
			}

			return str;
		}
	}

	private static class LeftJustifier implements Justifier
	{
		@Override
		public String justify(String str, int length)
		{
			if (str.length() < length)
			{
				StringBuilder sb = new StringBuilder(str);

				int spacesRequired = length - str.length();
				for (int i = 0; i < spacesRequired; i++)
				{
					sb.append(' ');
				}

				return sb.toString();
			}

			return str;
		}
	}

	public static Justifier RIGHT_JUSTIFIED = new RightJustifier();
	public static Justifier LEFT_JUSTIFIED = new LeftJustifier();

	public static String wrapToLength(String text, int width, String lineToken, String lineDelimiter)
	{
		return wrapToLength(text, width, lineToken, lineDelimiter, LEFT_JUSTIFIED);
	}

	public static String wrapToLength(String text, int width, String beginningOfLineToken, String endOfLinerDelimiter, Justifier justifier)
	{
		StringBuilder sb = new StringBuilder();
		StringBuilder lineBuilder = new StringBuilder();

		StringTokenizer st = new StringTokenizer(text, " ");

		while (st.hasMoreTokens())
		{
			String line = st.nextToken();

			if (lineBuilder.length() > 0 && (lineBuilder.length() + line.length() + 1) >= width)
			{
				if (beginningOfLineToken != null)
				{
					sb.append(beginningOfLineToken);
				}

				sb.append(justifier.justify(lineBuilder.toString(), width));
				sb.append(endOfLinerDelimiter);
				lineBuilder.setLength(0);
			}

			if (lineBuilder.length() != 0)
			{
				lineBuilder.append(' ');
			}

			lineBuilder.append(line);
		}


		if (lineBuilder.length() != 0)
		{
			if (beginningOfLineToken != null)
			{
				sb.append(beginningOfLineToken);
			}

			sb.append(justifier.justify(lineBuilder.toString(), width));
		}

		return sb.toString();
	}

	public static String sanitize(String input, char replacement)
	{
		return sanitize(input, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_", replacement);
	}

	public static String sanitize(String input, String allowedChars, char replacement)
	{
		StringBuffer sb = new StringBuffer(input.length());

		for (int offset = 0; offset < input.length(); offset++)
		{
			char c = input.charAt(offset);

			if (allowedChars.indexOf(c) == -1)
			{
				sb.append(replacement);
			}
			else
			{
				sb.append(c);
			}
		}

		return sb.toString();
	}

	public static String prepareForLog(String text, String header)
	{
		return header + " " + text.replaceAll("\r|\n|\r\n", "$0" + header + " ");
	}

	public static String makeProperPropertyName(String property)
	{
		String base = makePropertyName(property);

		base = Character.toUpperCase(base.charAt(0)) + base.substring(1);

		return base;
	}

	public static String preparePropertyForPackageName(String prop)
	{
		return checkForJavaKeywords(makeUnderscoreName(prop));
	}

	public static String makeUnderscoreName(String property)
	{
		if (isJavaKeyword(property))
		{
			return "_" + property;
		}

		StringBuilder stb = new StringBuilder();

		int count = 0;

		for (char ch : property.toCharArray())
		{
			if (!Character.isLetterOrDigit(ch))
			{
				stb.append('_');
			}
			else
			{
				if (Character.isUpperCase(ch) && count != 0)
				{
					stb.append('_');
				}

				stb.append(Character.toLowerCase(ch));
			}

			count++;
		}

		return stb.toString();
	}

	public static String makePropertyName(String property)
	{
		StringBuffer buffer = new StringBuffer();

		char[] array = property.toCharArray();

		boolean capNext = false;

		for (char ch : array)
		{
			if (ch == '-' || ch == '_' || ch == '.')
			{
				capNext = true;
			}
			else
			{
				if (capNext)
				{
					buffer.append(Character.toUpperCase(ch));
					capNext = false;
				}
				else
				{
					buffer.append(ch);
				}
			}
		}

		return buffer.toString();
	}

	public static String createRelativePath(File from1, File to)
	{
		return createRelativePath(from1, to, File.separatorChar);
	}

	public static String createRelativePath(File from1, File to, char separator)
	{
		List<String> path = convertToRelativePath(from1, to);

		// no relative possible
		if (path == null)
		{
			return null;
		}

		// build up by path
		StringBuilder stb = new StringBuilder();

		boolean first = true;

		for (String pathElement : path)
		{
			if (first)
			{
				first = false;
			}
			else
			{
				stb.append(separator);
			}

			stb.append(pathElement);
		}

		// append the destination file name if to is a file
		if (!to.isDirectory())
		{
			stb.append(separator).append(to.getName());
		}

		return stb.toString();
	}

	public static List<String> convertToRelativePath(File from, File to)
	{
		// files are not part of relative paths
		if (!from.isDirectory())
		{
			from = from.getParentFile();
		}

		if (!to.isDirectory())
		{
			to = to.getParentFile();
		}

		return convertToRelativePathImpl(from, to);
	}

	private static List<String> convertToRelativePathImpl(File from, File to)
	{
		List<String> resultPath = new ArrayList<String>();

		List<String> rootPath = new ArrayList<String>();
		List<String> relativePath = new ArrayList<String>();

		// grab both paths as a list of strings
		walkFilePath(rootPath, from);
		walkFilePath(relativePath, to);

		comparePaths(rootPath, relativePath, resultPath);

		// convert an empty result to null
		if (resultPath.size() == 0)
		{
			return null;
		}

		return resultPath;
	}

	public static void comparePaths(List<String> from, List<String> to, List<String> relativePath)
	{
		// find the nearest common ancestor, then for every subsequent path element in the root use '..', then use the names in relative to the target
		int lastCommonAncestor = -1;

		for (int i = 0; i < Math.min(from.size(), to.size()); i++)
		{
			String fromPath = from.get(i);
			String toPath = to.get(i);

			if (!fromPath.equals(toPath))
			{
				// this is what we wanted to know
				break;
			}

			lastCommonAncestor = i;
		}

		// special case - nothing in common.  Return empty.
		if (lastCommonAncestor == -1)
		{
			return;
		}

		// other special case - identical paths.  Return '.'
		if ((to.size() == from.size()) && lastCommonAncestor == (to.size() - 1))
		{
			relativePath.add(".");
			return;
		}

		// append '..' for every remaining path element in from
		for (int i = lastCommonAncestor + 1; i < from.size(); i++)
		{
			relativePath.add("..");
		}

		// append every remaining path element in to
		for (int i = lastCommonAncestor + 1; i < to.size(); i++)
		{
			relativePath.add(to.get(i));
		}

		return;
	}

	public static void walkFilePath(List<String> rootPath, File base) {
		while (base != null)
		{
			rootPath.add(base.getName());

			// next up
			base = base.getParentFile();
		}

		// reverse it so it goes in forward order
		Collections.reverse(rootPath);
	}

	public static String getSafeNullableLowercase(String val)
	{
		return val == null ? null : val.toLowerCase();
	}

	public static String getSafeNullableUppercase(String val)
	{
		return val == null ? null : val.toUpperCase();
	}

	public static String checkForJavaKeywords(String text)
	{
		if (isJavaKeyword(text))
		{
			return "_" + text;
		}

		return text;
	}

	/**
	 * checks if the input string is a valid java keyword.
	 *
	 * @return boolean true/false
	 */
	public static boolean isJavaKeyword(String keyword)
	{
		return (Arrays.binarySearch(keywords, keyword, englishCollator) >= 0);
	}

	enum eol_style
	{
		windows("\r\n"), unix("\n"), mac_legacy("\r");
		private final String eol;

		eol_style(String chars)
		{
			eol = chars;
		}
	}

	public static String convertEol(String source)
	{
		return convertEol(source, eol_style.unix);
	}

	public static String convertEol(String source, eol_style style)
	{
		return source.replaceAll("\\r\\n|\\r|\\n", style.eol);
	}

	public static String convertFilePathToUnix(File path)
	{
		try
		{
			return path.getCanonicalPath().replace(File.separator, "/");
		}
		catch (IOException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Encode a string using the following process:
	 * 1	- Escape the escape character, where found		- 3
	 * 2	- Encode non-printable characters							- 2
	 * 3 	- Escape each escape text bracketed with esc	- 1
	 * @param input
	 * @param escapeChar
	 * @param escapeTexts
	 * @return
	 */
	public static String escape(String input, char escapeChar, String... escapeTexts)
	{
		return StringEscapeUtils.escapeJava(input);
		// (1)
		// replace all occurrences of the escapeStr with escapeStrescapeStr
		//String escapeStr = String.valueOf(escapeChar);
		//input = input.replace(escapeStr, escapeStr + escapeStr);

		// (2)
		// encode non-printable
		//input = StringEscapeUtils.escapeJava(input);

		// (3)
		// now replace all occurrences of every escapeText with escapeStrescapeTextescapeStr
		//for (String escapeText : escapeTexts)
		//{
		//	input = input.replace(escapeText, escapeStr + escapeText + escapeStr);
		//}

		//return input;
	}

	/**
	 * Reverse the process of escaping string
	 * @param data
	 * @param escapeChar
	 * @param escapeTexts
	 * @return
	 */
	public static String unescape(String data, char escapeChar, String... escapeTexts) {
		return StringEscapeUtils.unescapeJava(data);
		// reverse the process of escaping

		// (3) strip out any dangling escapes.  These will
		// all be escaping an escapeText
		//List<EscapeTextReference> texts = getEscapeTexts(data);

		//data = removeEscapes(data);

		// (2) decode special chars
		//data = StringEscapeUtils.unescapeJava(data);

		// (3) - remove doubled-up escapes
		//String escapeStr = String.valueOf(escapeChar);
		//String escapeStrDbl = escapeStr + escapeStr;

		//data = data.replace(escapeStrDbl, escapeStr);

		//return data;
	}

	public static List<EscapeTextReference> getEscapeTexts(String input)
	{
		List<EscapeTextReference> refs = new ArrayList<EscapeTextReference>();

		int thisBegin = -1;
		int thisEnd = -1;

		int index = -1;

		for (char c: input.toCharArray())
		{
			index++;

			// check first for group 1 - this can happen exactly once
			if (c == '\\')
			{
				if (thisBegin == -1)
				{
					thisBegin = index;
				}
				else if(thisEnd == -1)
				{
					thisEnd = index;
				}
				else
				{
					throw new Error();
				}
			}

			if (thisEnd != -1 && thisBegin == -1)
			{
				throw new IllegalArgumentException("End tag with no matching begin: " + thisEnd);
			}
			else if (thisEnd != -1)
			{
				// record a pair
				String subtext = input.substring(thisBegin + 1, thisEnd);
				refs.add(new EscapeTextReference(subtext, thisBegin, thisEnd));
				thisEnd = -1;
				thisBegin = -1;
			}

			if (thisEnd != -1 && thisEnd == thisBegin)
			{
				throw new IllegalArgumentException("Bad deal");
			}
		}

		if (thisBegin == -1 && thisEnd != -1)
		{
			throw new IllegalArgumentException("End tag with no matching begin: " + thisEnd + " - " + input);
		}
		else if (thisBegin != -1 && thisEnd == -1)
		{
			throw new IllegalArgumentException("Begin tag with no matching end: " + thisBegin + " - " + input);
		}

		return refs;
	}

	public static class EscapeTextReference {
		final String text;
		final int beginOffset;
		final int endOffset;

		public String getText() {
			return text;
		}

		public int getBeginOffset() {
			return beginOffset;
		}

		public int getEndOffset() {
			return endOffset;
		}

		public EscapeTextReference(String text, int beginOffset, int endOffset) {
			this.text = text;
			this.beginOffset = beginOffset;
			this.endOffset = endOffset;
		}
	}

	public static String removeEscapes(String input)
	{
		return input.replace("\\", "");
	}

	public static String parseToToken(Reader source, char [] token) throws IOException {
		int delimOffset = 0;
		StringWriter sw = new StringWriter();

		int ch = 0;
		boolean inQuotes = false;

		while ((ch = source.read()) != -1)
		{
			if (ch == '"')
			{
				// switch state
				inQuotes = !inQuotes;

				// pass quote char on
				sw.write(ch);
			}
			else if (!inQuotes && ch == token[delimOffset])
			{
				// matched part of the token.  Check if the token has been matched
				if (delimOffset == (token.length - 1))
				{
					//done.  Return text
					return sw.toString();
				}
				else
				{
					delimOffset++;
				}
			}
			else if (delimOffset != 0)
			{
				// put back what we have already grabbed
				sw.write(token, 0, delimOffset);
				// put back what we just read
				sw.write(ch);

				// reset the offset
				delimOffset = 0;
			}
			else
			{
				sw.write(ch);
			}
		}

		// this is always a failure
		throw new EOFException("Stream ended before a token was reached: " + sw.toString());
	}
}
