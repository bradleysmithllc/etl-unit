package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.io.IOUtils;

import java.io.*;

public class ReaderDataSetUtils
{
	public interface CopyVisitor
	{
		JsonNode getProperties(DataSet sourceDataSet, int dsIndex);

		Reader read(DataSet set, int dsIndex) throws IOException;

		void close(Reader reader, DataSet set, int dsIndex) throws IOException;

		boolean includeDataSet(DataSet dataSet, int dsIndex);
	}

	public static void extract(DataSet set, File target) throws IOException
	{
		Reader read = set.read();

		try
		{
			FileWriter fileWriter = new FileWriter(target);

			try
			{
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				IOUtils.copy(read, bufferedWriter);
				bufferedWriter.flush();
			}
			finally
			{
				fileWriter.close();
			}
		}
		finally
		{
			read.close();
		}
	}

	public static void copy(DataSetContainer container, Writer target, CopyVisitor visitor) throws IOException
	{
		int dsIndex = 0;

		while (container.hasNext())
		{
			DataSet nextSet = container.next();

			if (visitor.includeDataSet(nextSet, dsIndex))
			{
				JsonNode jsonNode = visitor.getProperties(nextSet, dsIndex);
				if (jsonNode == null)
				{
					jsonNode = nextSet.getProperties();
				}

				Reader read = visitor.read(nextSet, dsIndex);

				if (read == null)
				{
					read = nextSet.read();
				}

				try
				{
					write(jsonNode, read, target);
				}
				finally
				{
					if (read != nextSet.read())
					{
						visitor.close(read, nextSet, dsIndex);
					}
				}
			}

			dsIndex++;
		}

		target.flush();
	}

	public static String toFormattedJsonString(JsonNode properties) throws IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
		mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);

		String valueAsString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(properties);
		String replace = valueAsString.replace("\r\n", "\n").replace("\r", "\n");

		// before going on, try to convert to etlunit style
		// unquote the property names
		replace = replace.replaceAll("\"(.+)\".*:", "$1:");

		// change double to single-quote for property values
		replace = replace.replace('"', '\'');

		return replace;
	}

	public static void write(JsonNode properties, Reader body, Writer target) throws IOException
	{
		target.write(toFormattedJsonString(properties));
		target.write(ReaderDataSet.token);

		IOUtils.copy(body, target);

		target.write(ReaderDataSet.token);
	}
}
