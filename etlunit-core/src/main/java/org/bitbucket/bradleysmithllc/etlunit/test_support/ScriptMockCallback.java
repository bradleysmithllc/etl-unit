package org.bitbucket.bradleysmithllc.etlunit.test_support;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonProcessingUtil;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonSchemaLoader;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.regex.Pattern;

public class ScriptMockCallback implements MockProcessExecutor.ProcessCallback
{
	private final String id;

	private static final JsonSchema jsonValidator;
	private static final JsonSchema jsonCommandValidator;

	static
	{
		try
		{
			jsonValidator = getProcessSchema();
			jsonCommandValidator = getCommandSchema();
		}
		catch (Exception e)
		{
			throw new RuntimeException("Error loading reference schemas", e);
		}
	}

	public ScriptMockCallback(String id)
	{
		this.id = id;
	}

	@Override
	public MockProcessExecutor.MockProcessFacade mock(ProcessDescription pd) throws IOException
	{
		// locate the json script
		URL resource = getClass().getClassLoader().getResource(id + "/" + pd.getCommandName() + ".process.json");

		if (resource == null)
		{
			throw new IOException("Command resource file not found: " + pd.getCommandName());
		}

		// load and validate the json
		String jsonNode = IOUtils.readURLToString(resource);

		try
		{
			ETLTestValueObject impl = ETLTestParser.loadObject(jsonNode);
			JsonNode node = impl.getJsonNode();

			// validate
			jsonValidator.validate(node);

			// all is well
			// validate the path
			ETLTestValueObject pathExpr = impl.query("command-expression");

			if (pathExpr != null)
			{
				// we have to check the path
				Pattern pattern = Pattern.compile(pathExpr.getValueAsString());

				if (!pattern.matcher(pd.getCommand()).matches())
				{
					throw new IOException("Command does not match pattern: command = '"
							+ pd.getCommand()
							+ "', pattern = '"
							+ pathExpr.getValueAsString()
							+ "'");
				}
			}

			List<String> pdArguments = pd.getArguments();

			// iterate through all listed commands and try each one, taking the first that matches
			ETLTestValueObject commands = impl.query("commands");

			for (String command : commands.getValueAsListOfStrings())
			{
				// locate
				URL commandResource = getClass().getClassLoader().getResource(id + "/" + command);

				if (commandResource == null)
				{
					throw new IOException("Missing command resource: " + command);
				}

				jsonNode = IOUtils.readURLToString(commandResource);

				// validate against command schema
				ETLTestValueObject commandImpl = ETLTestParser.loadObject(jsonNode);
				JsonNode commandNode = commandImpl.getJsonNode();

				ProcessingReport pr = jsonCommandValidator.validate(commandNode);

				if (!pr.isSuccess())
				{
					throw new IllegalArgumentException(JsonProcessingUtil.getProcessingReport(pr));
				}

				// check the arguments - if we have a match we are done
				ETLTestValueObject args = commandImpl.query("argument-expressions");

				boolean good = true;

				if (args != null)
				{
					List<String> argStringList = args.getValueAsListOfStrings();
					if (pdArguments.size() >= argStringList.size())
					{
						for (int i = 0; i < argStringList.size(); i++)
						{
							String argPattern = argStringList.get(i);

							if (argPattern == null)
							{
								// null means I don't care about this arg
								continue;
							}

							String pdArg = pdArguments.get(i);

							// validate the match
							if (!Pattern.compile(argPattern).matcher(pdArg).matches())
							{
								// fail
								good = false;
								break;
							}
						}
					}
					else
					{
						good = false;
					}
				}

				if (good)
				{
					return new ScriptRunner(commandImpl);
				}
			}

			throw new IOException("No command found to handle the arguments provided");
		}
		catch (ParseException e)
		{
			throw new IOException("Bad process json - not json", e);
		} catch (ProcessingException e) {
			throw new IOException("Bad process json - validation failure", e);
		}
	}

	private static JsonSchema getProcessSchema()
	{
		return	JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/test_support/mockProcess.jsonSchema");
	}

	private static JsonSchema getCommandSchema()
	{
		return	JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/test_support/mockProcessScript.jsonSchema");
	}
}
