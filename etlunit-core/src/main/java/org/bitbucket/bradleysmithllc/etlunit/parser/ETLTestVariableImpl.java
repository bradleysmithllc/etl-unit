package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class ETLTestVariableImpl extends ETLTestAnnotatedImpl implements ETLTestVariable
{
	private final String name;
	private final ETLTestValueObject value;
	private final value_type valueType;

	public ETLTestVariableImpl(String n, ETLTestValueObject v)
	{
		name = n;
		value = v;
		switch (v.getValueType())
		{
			case literal:
				valueType = value_type.assignment;
				break;
			case list:
			case object:
			case quoted_string:
			default:
				valueType = value_type.literal;
				break;
		}

	}

	public ETLTestVariableImpl(String n, ETLTestValueObject v, Token t)
	{
		super(t);
		name = n;
		value = v;
		switch (v.getValueType())
		{
			case literal:
				valueType = value_type.assignment;
				break;
			case list:
			case object:
			case quoted_string:
			default:
				valueType = value_type.literal;
				break;
		}

	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public ETLTestValueObject getValue()
	{
		return value;
	}

	@Override
	public value_type getValueType()
	{
		return valueType;
	}
}
