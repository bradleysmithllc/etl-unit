package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.OutputStream;

public class BifurcatingOutputStream extends OutputStream
{
	private final OutputStream streama;
	private final OutputStream streamb;

	public BifurcatingOutputStream(OutputStream streamb, OutputStream streama)
	{
		this.streamb = streamb;
		this.streama = streama;
	}

	@Override
	public synchronized void write(int b) throws IOException
	{
		streama.write(b);
		streamb.write(b);
	}

	@Override
	public synchronized void write(byte[] b) throws IOException
	{
		streama.write(b);
		streamb.write(b);
	}

	@Override
	public synchronized void write(byte[] b, int off, int len) throws IOException
	{
		streama.write(b, off, len);
		streamb.write(b, off, len);
	}

	@Override
	public synchronized void flush() throws IOException
	{
		streama.flush();
		streamb.flush();
	}

	@Override
	public synchronized void close() throws IOException
	{
		streama.close();
		streamb.close();
	}
}
