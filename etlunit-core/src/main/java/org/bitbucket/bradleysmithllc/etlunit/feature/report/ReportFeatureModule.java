package org.bitbucket.bradleysmithllc.etlunit.feature.report;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import com.google.inject.name.Named;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.NullStatusReporterImpl;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

//@FeatureModule
public class ReportFeatureModule extends AbstractFeature
{
	private LogFileManager logFileManager;

	private Log applicationLog;

	private RuntimeSupport runtimeSupport;

	private static final List<String> prerequisites = new ArrayList<String>(Collections.EMPTY_LIST);

	private final StatusReporter reportStatusReporter = new HtmlStatusReporter();

	public class TestResult
	{
		private ETLTestMethod method;
		private StatusReporter.CompletionStatus status;

		public ETLTestMethod getMethod()
		{
			return method;
		}

		public String logURI(LogFileManager.LogFile logFileg) throws IOException {
			File uriBase = runtimeSupport.getReportDirectory("html").getCanonicalFile();
			File uriDest = logFileg.getFile().getCanonicalFile();
			String path = EtlUnitStringUtils.createRelativePath(uriBase, uriDest);
			return path;
		}

		public StatusReporter.CompletionStatus getStatus()
		{
			return status;
		}

		public List<LogFileManager.LogFile> getLogFiles()
		{
			return logFileManager.getLogFilesByETLTestMethod(method);
		}

		public String getResultClass()
		{
			switch (status.getTestResult())
			{
				case success:
					return "success";
				case failure:
					return "failure";
				case warning:
					return "warning";
				case error:
					return "error";
				default:
					throw new IllegalStateException();
			}
		}
	}

	public class TestClassResult
	{
		private final List<TestResult> tests = new ArrayList<TestResult>();

		private ETLTestClass cls;

		public ETLTestClass getCls()
		{
			return cls;
		}

		public List<TestResult> getTests()
		{
			return tests;
		}
	}

	private class HtmlStatusReporter extends NullStatusReporterImpl
	{
		private final List<TestClassResult> tests = new ArrayList<TestClassResult>();

		private TestClassResult currentClass;

		@Override
		public void testCompleted(ETLTestMethod method, CompletionStatus status)
		{
			if (currentClass == null || method.getTestClass() != currentClass.cls)
			{
				currentClass = new TestClassResult();
				currentClass.cls = method.getTestClass();

				tests.add(currentClass);
			}

			TestResult tr = new TestResult();
			tr.method = method;
			tr.status = status;

			currentClass.tests.add(tr);
		}

		@Override
		public void testsCompleted()
		{
			File dir = runtimeSupport.getReportDirectory("html");

			File index = new File(dir, "index.html");

			Map<String, Object> mobj = new HashMap<String, Object>();
			mobj.put("tests", tests);

			try
			{
				// load the velocity template
				URL url = getClass().getClassLoader().getResource("htmlReportIndex.vm");

				String template = IOUtils.readURLToString(url);

				String text = VelocityUtil.writeTemplate(template, mobj);

				IOUtils.writeBufferToFile(index, new StringBuffer(text));
			}
			catch (Exception e)
			{
				applicationLog.severe("Error writing html report index file", e);
			}
		}
	}

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log)
	{
		this.applicationLog = log;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport rs)
	{
		runtimeSupport = rs;
	}

	@Override
	public List<String> getPrerequisites()
	{
		return prerequisites;
	}

	@Override
	public void initialize(Injector inj)
	{
		postCreate(reportStatusReporter);
	}

	@Override
	public StatusReporter getStatusReporter()
	{
		return reportStatusReporter;
	}

	@Override
	public String getFeatureName()
	{
		return "report";
	}

	@Inject
	public void receiveLogFileManager(LogFileManager logFileManager)
	{
		this.logFileManager = logFileManager;
	}

	@Override
	public boolean requiredForSimulation() {
		return true;
	}
}
