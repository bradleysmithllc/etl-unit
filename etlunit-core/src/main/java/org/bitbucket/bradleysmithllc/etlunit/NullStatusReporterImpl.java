package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

public class NullStatusReporterImpl implements StatusReporter
{
	@Override
	public void scanStarted()
	{
	}

	@Override
	public void scanCompleted()
	{
	}

	@Override
	public void testsStarted(int numTestsSelected)
	{
	}

	@Override
	public void testClassAccepted(ETLTestClass method)
	{
	}

	@Override
	public void testMethodAccepted(ETLTestMethod method)
	{
	}

	@Override
	public void testBeginning(ETLTestMethod method)
	{
	}

	@Override
	public void testCompleted(ETLTestMethod method, CompletionStatus status)
	{
	}

	@Override
	public void testsCompleted()
	{
	}
}
