package org.bitbucket.bradleysmithllc.etlunit.listener;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface BroadcasterCoordinator
{
	/**
	 * Called by every broadcaster executor when ready to participate
	 * in execution.  The executor lends it's thread to the coordinator
	 * and all work is completed through callbacks in the executor interface.
	 * @param executor
	 * @return true when there are more tests to process.  False otherwise.  Convenience for testing.
	 */
	boolean ready(BroadcasterExecutor executor);

	void waitForCompletion();

	/**
	 * Release the start latch and let them go.
	 */
	void start();

	/**
	 * How many executors are currently waiting?  This includes excutors that are waiting for the start signal.
	 * @return
	 */
	int getActiveExecutorCount();

	/**
	 * Wait for a minimum number of executors to be active.  This is primarily useful for waiting for the executors
	 * to check in before opening the queue.
	 * @param count
	 */
	void waitForExecutorsToActivate(int count);
}
