package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;

public class FrameInputStream extends InputStream {
	byte[] currentBuffer;
	int currentOffset = 0;
	boolean eos = false;

	private final DataInput din;

	public FrameInputStream(DataInput din) {
		this.din = din;
	}

	@Override
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (currentBuffer == null) {
			// refresh
			StreamFrame frame = StreamFrame.nextFrame(din);

			currentBuffer = frame.getData();
			currentOffset = 0;

			if (currentBuffer.length == 0)
			{
				eos = true;
			}
		}

		if (eos) {
			return -1;
		}

		// how much to send?
		int available = Math.min(len, currentBuffer.length - currentOffset);

		// return the next block
		System.arraycopy(currentBuffer, currentOffset, b, off, available);

		// advance the pointer
		currentOffset += available;

		if (currentOffset >= currentBuffer.length) {
			currentBuffer = null;
		}

		return available;
	}

	@Override
	public int available() throws IOException {
		if (currentBuffer == null) {
			return 0;
		}

		return currentBuffer.length - currentOffset;
	}

	@Override
	public int read() throws IOException {
		byte [] buff = new byte[1];

		int i = read(buff);

		if (i == -1)
		{
			return i;
		}

		return buff[i] & 0xFF;
	}
}
