package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.h2.util.IOUtils;

import java.io.*;
import java.util.concurrent.CountDownLatch;

public class StreamProcessExecutorProcessFacade implements ProcessFacade {
	private final ProcessDescription pd;
	private final Process process;
	private final CountDownLatch clatch;
	private final CountDownLatch internalLatch;
	private final Writer fow;
	private OutputStream fos;

	public StreamProcessExecutorProcessFacade(ProcessDescription pd, Process process, CountDownLatch cclatch, OutputStream cfos, final OutputStream logOutputStream) throws FileNotFoundException {
		this.pd = pd;
		this.process = process;
		this.clatch = cclatch;
		this.fos = cfos;

		NfurcatingOutputStream nfos = new NfurcatingOutputStream(fos);

		fos = nfos.bifurcate(logOutputStream);

		fow = new OutputStreamWriter(fos);

		internalLatch = new CountDownLatch(1);

		new Thread(new Runnable(){
			@Override
			public void run() {
				try
				{
					// wait for two output streams to finish
					clatch.await();

					// close the input stream
					try
					{
						fos.close();
					}
					finally
					{
						logOutputStream.close();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally
				{
					internalLatch.countDown();
				}
			}
		}).start();
	}

	@Override
	public ProcessDescription getDescriptor() {
		return pd;
	}

	@Override
	public void waitForCompletion() {
		try {
			process.waitFor();
			waitForOutputStreamsToComplete();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int getCompletionCode() {
		return process.exitValue();
	}

	@Override
	public void kill() {
		process.destroy();
	}

	@Override
	public void waitForStreams() {
	}

	@Override
	public void waitForOutputStreamsToComplete() throws IOException {
		try {
			internalLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Writer getWriter() {
		return fow;
	}

	@Override
	public BufferedReader getReader() throws IOException {
		return new BufferedReader(new InputStreamReader(getInputStream()));
	}

	@Override
	public BufferedReader getErrorReader() throws IOException {
		if (!pd.isRedirectStandardError())
		{
			return new BufferedReader(new InputStreamReader(getErrorStream()));
		}
		else
		{
			return new BufferedReader(new NullReader());
		}
	}

	@Override
	public StringBuffer getInputBuffered() throws IOException {
		return getInputBuffered(getInputStream());
	}

	private StringBuffer getInputBuffered(InputStream is) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		try
		{
			IOUtils.copy(is, byteArrayOutputStream);
		}
		finally
		{
			is.close();
		}

		return new StringBuffer(byteArrayOutputStream.toString());
	}

	@Override
	public StringBuffer getErrorBuffered() throws IOException {
		if (!pd.isRedirectStandardError())
		{
			return getInputBuffered(getErrorStream());
		}
		else
		{
			return new StringBuffer();
		}
	}

	@Override
	public OutputStream getOutputStream() {
		return fos;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return FrameInputStreamWrapper(getStreamOut(pd));
	}

	private InputStream FrameInputStreamWrapper(File streamOut) throws IOException {
		return new FrameRandomAccessFileInputStream(streamOut);
	}

	@Override
	public InputStream getErrorStream() throws IOException {
		if (!pd.isRedirectStandardError())
		{
			return FrameInputStreamWrapper(getStreamErr(pd));
		}
		else
		{
			return new NullInputStream();
		}
	}

	public File getStreamOut()
	{
		return getStreamOut(pd);
	}

	public File getStreamErr()
	{
		return getStreamErr(pd);
	}

	public File getStreamIn()
	{
		return getStreamIn(pd);
	}

	public static File getStreamOut(ProcessDescription pd)
	{
		return new File(pd.getPrivateWorkingDir(), "stdout.frm");
		//return new File(new File("/tmp"), "stdout.frm");
	}

	public static File getStreamErr(ProcessDescription pd)
	{
		return new File(pd.getPrivateWorkingDir(), "stderr.frm");
		//return new File(new File("/tmp"), "stderr.frm");
	}

	public static File getStreamIn(ProcessDescription pd)
	{
		return new File(pd.getPrivateWorkingDir(), "stdin.frm");
		//return new File(new File("/tmp"), "stdin.frm");
	}
}
