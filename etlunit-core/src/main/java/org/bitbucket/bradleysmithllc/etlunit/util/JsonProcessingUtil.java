package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;

public class JsonProcessingUtil {
	public static String getProcessingReport(ProcessingReport report) {
		try {
			StringWriter stw = new StringWriter();

			JsonFactory jfact = new JsonFactory();

			JsonGenerator generator = jfact.createGenerator(stw);
			generator.setPrettyPrinter(new DefaultPrettyPrinter());
			generator.setCodec(new ObjectMapper());

			Iterator<ProcessingMessage> it = report.iterator();

			generator.writeStartObject();
			generator.writeStringField("document-type", "processing-report");

			generator.writeFieldName("processing-messages");

			generator.writeStartArray();

			while (it.hasNext()) {
				ProcessingMessage pm = it.next();

				generator.writeTree(pm.asJson());
			}

			generator.writeEndArray();
			generator.writeEndObject();
			generator.close();

			return stw.toString();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static String getProcessingReport(ProcessingException report) {
		try {
			StringWriter stw = new StringWriter();

			JsonFactory jfact = new JsonFactory();

			JsonGenerator generator = jfact.createGenerator(stw);
			generator.setPrettyPrinter(new DefaultPrettyPrinter());
			generator.setCodec(new ObjectMapper());

			generator.writeStartObject();
			generator.writeStringField("document-type", "processing-exception");

			generator.writeStringField("message", report.getMessage());
			generator.writeFieldName("processing-messages");

			generator.writeStartArray();

			generator.writeTree(report.getProcessingMessage().asJson());

			generator.writeEndArray();
			generator.writeEndObject();
			generator.close();

			return stw.toString();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
