package org.bitbucket.bradleysmithllc.etlunit.feature.report;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import com.google.inject.name.Named;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogManagerRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifactContentReference;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.util.*;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.time.format.DateTimeFormatter;
import java.util.*;

@FeatureModule
public class BetterReportFeatureModule extends AbstractFeature {
	private LogFileManager logFileManager;
	private LogManagerRuntimeSupport logManagerRuntimeSupport;
	private MetaDataManager metaDataManager;
	private DiffManager diffManager;

	private Log applicationLog;

	private Locals locals = new Locals(this);

	private RuntimeSupport runtimeSupport;

	private static final List<String> prerequisites = new ArrayList<String>(Collections.EMPTY_LIST);

	private final HtmlStatusReporter reportStatusReporter = new HtmlStatusReporter();
	private final Map<String, TestStatistics> statsByTestType = new HashMap<String, TestStatistics>();
	private final TestStatistics testStatistics = getStats("[root]");

	private File html;

	@Override
	public ClassListener getListener() {
		return locals;
	}

	public class TestResult implements Comparable<TestResult> {
		private ETLTestMethod method;
		private int executorId = 0;
		private StatusReporter.CompletionStatus status;

		public int getExecutorId() {
			return executorId;
		}

		public boolean isLog() {
			return logManagerRuntimeSupport.getAggregatedLogFile(method).exists();
		}

		public List<String> getFailureIds()
		{
			return status.getAssertionFailureIds();
		}

		public List<TestExecutionError> getErrorIds()
		{
			return status.getErrors();
		}

		public List<String> getWarningIds()
		{
			return status.getTestWarningIds();
		}

		public String getLogPath() throws IOException {
			File logFile = logManagerRuntimeSupport.getAggregatedLogFile(method);

			return runtimeSupport.getProjectRelativePath(logFile);
		}

		public List<MetaDataArtifactContentReference> getArtifacts() {
			return metaDataManager.getMetaData().getTestArtifacts(method);
		}

		public String makeUrl(MetaDataArtifactContentReference ref) throws IOException {
			File log = new File(ref.getMetaDataArtifactContent().getMetaDataArtifact().getPath(), ref.getMetaDataArtifactContent().getName());

			return runtimeSupport.getProjectRelativePath(log);
		}

		public String evenOrOdd(int countr) {
			return countr % 2 == 0 ? "even" : "odd";
		}

		public String getDiffReport() throws IOException {
			File diffFile = diffManager.findDiffReport(method);

			return runtimeSupport.getProjectRelativePath(diffFile);
		}

		public String getMethodSummaryLog() throws IOException {
			File diffFile = new FileBuilder(runtimeSupport.getReportDirectory("html")).name(getIdentifierQualifiedName() + ".html").file();

			return runtimeSupport.getProjectRelativePath(diffFile);
		}

		public boolean getHasDiff() {
			return diffManager.findDiffReport(method) != null;
		}

		public ETLTestMethod getMethod() {
			return method;
		}

		public String getIdentifierQualifiedName() {
			return EtlUnitStringUtils.sanitize(method.getQualifiedName(), '_');
		}

		public boolean getLogged() {
			return getLogFiles() != null;
		}

		public String logURI(LogFileManager.LogFile logFileg) throws IOException {
			File uriDest = logFileg.getFile().getCanonicalFile();
			return runtimeSupport.getProjectRelativePath(uriDest);
		}

		public StatusReporter.CompletionStatus getStatus() {
			return status;
		}

		public List<LogFileManager.LogFile> getLogFiles() {
			return logFileManager.getLogFilesByETLTestMethod(method);
		}

		public String abbreviatedName(String fullName) {
			return
					org.apache.commons.lang3.StringUtils.abbreviateMiddle(fullName, "...", 50);
		}

		public String getResultClass() {
			switch (status.getTestResult()) {
				case success:
					return "success";
				case failure:
					return "failure";
				case warning:
					return "warning";
				case ignored:
					return "ignored";
				case error:
					return "error";
				default:
					throw new IllegalStateException();
			}
		}

		public TestStatistics getStatistics() {
			return statsByTestType.get(method.getQualifiedName());
		}

		@Override
		public int compareTo(TestResult o) {
			return method.getQualifiedName().compareTo(o.method.getQualifiedName());
		}

		public boolean hasStatus(StatusReporter.test_completion_code code) {
			return status.getTestResult() == code;
		}
	}

	public class TestClassResult implements Comparable<TestClassResult> {
		private String resultClass;
		private final List<TestResult> tests = new ArrayList<TestResult>();

		private ETLTestClass cls;

		public boolean isLog() {
			return logManagerRuntimeSupport.getAggregatedLogFile(cls).exists();
		}

		public String getLogPath() throws IOException {
			File logFile = logManagerRuntimeSupport.getAggregatedLogFile(cls);

			return runtimeSupport.getProjectRelativePath(logFile);
		}

		public ETLTestClass getCls() {
			return cls;
		}

		public List<TestResult> getTests() {
			// these need to be returned in the order declared in the test class
			// make a map of all names in this set
			Map<String, TestResult> resultMap = new HashMap<String, TestResult>();

			for (TestResult result : tests)
			{
				resultMap.put(result.getMethod().getQualifiedName(), result);
			}

			List<TestResult> orderedResults = new ArrayList<TestResult>();

			for (ETLTestMethod method : cls.getTestMethods())
			{
				String qn = method.getQualifiedName();

				// not all methods may have run.
				if (resultMap.containsKey(qn))
				{
					orderedResults.add(resultMap.get(qn));
				}
			}

			return orderedResults;
		}

		public TestStatistics getStatistics() {
			return statsByTestType.get(cls.getQualifiedName());
		}

		@Override
		public int compareTo(TestClassResult o) {
			return cls.getQualifiedName().compareTo(o.cls.getQualifiedName());
		}

		public String getResultClass() {
			if (resultClass == null) {
				for (TestResult method : tests) {
					switch (method.getStatus().getTestResult()) {
						case success:
							break;
						case failure:
							if (resultClass == null) {
								resultClass = "failure";
							}
							break;
						case ignored:
							if (resultClass == null) {
								resultClass = "ignored";
							}
							break;
						case warning:
							if (resultClass == null) {
								resultClass = "warning";
							}
							break;
						case error:
							resultClass = "error";
							break;
						default:
							throw new IllegalStateException();
					}
				}

				if (resultClass == null) {
					resultClass = "success";
				}
			}

			return resultClass;
		}

		public boolean hasStatus(StatusReporter.test_completion_code code) {
			for (TestResult i : tests)
			{
				if (i.hasStatus(code))
				{
					return true;
				}
			}

			return false;
		}
	}

	public class TestPackageResult implements Comparable<TestPackageResult> {
		private String resultClass;
		private final List<TestClassResult> classResults = new ArrayList<TestClassResult>();

		private ETLTestPackage packageName;

		public TestPackageResult(ETLTestPackage aPackage) {
			packageName = aPackage;
		}

		public String getName() {
			return packageName.getPackageName();
		}

		public List<TestClassResult> getClassResults() {
			Collections.sort(classResults);

			return classResults;
		}

		public boolean hasStatus(StatusReporter.test_completion_code code)
		{
				for (TestClassResult i : classResults)
				{
					if (i.hasStatus(code))
					{
						return true;
					}
				}

				return false;
		}

		public TestStatistics getStatistics() {
			return statsByTestType.get(packageName.getPackageName());
		}

		@Override
		public int compareTo(TestPackageResult o) {
			return packageName.getPackageName().compareTo(o.packageName.getPackageName());
		}

		public String getResultClass() {
			if (resultClass == null) {
				for (TestClassResult method : classResults) {
					String mresultClass = method.getResultClass();
					if (mresultClass.equals("failure") && resultClass == null) {
						resultClass = mresultClass;
					} else if (mresultClass.equals("warning") && resultClass == null) {
						resultClass = mresultClass;
					} else if (mresultClass.equals("ignored") && resultClass == null) {
						resultClass = mresultClass;
					} else if (mresultClass.equals("error")) {
						resultClass = mresultClass;
					}
				}

				if (resultClass == null) {
					resultClass = "success";
				}
			}

			return resultClass;
		}
	}

	private class HtmlStatusReporter extends NullStatusReporterImpl {
		private final List<TestPackageResult> packageResults = new ArrayList<TestPackageResult>();
		private final Map<ETLTestPackage, TestPackageResult> testMap = new HashMap<ETLTestPackage, TestPackageResult>();

		private final MapList<String, TestResult> failedTests = new HashMapArrayList<String, TestResult>();
		private final MapList<String, TestResult> erroredTests = new HashMapArrayList<String, TestResult>();
		private final MapList<String, TestResult> warnedTests = new HashMapArrayList<String, TestResult>();

		Map<String, TestClassResult> classResults = new HashMap<String, TestClassResult>();
		ThreadLocal<Long> startTime = new ThreadLocal<Long>();

		public boolean hasErrors()
		{
				for (TestPackageResult i : packageResults)
				{
					if (i.hasStatus(test_completion_code.error))
					{
						return true;
					}
				}

			return false;
		}

		public boolean hasFailures()
		{
			for (TestPackageResult i : packageResults)
			{
				if (i.hasStatus(test_completion_code.failure))
				{
					return true;
				}
			}

			return false;
		}

		public boolean hasIgnores()
		{
			for (TestPackageResult i : packageResults)
			{
				if (i.hasStatus(test_completion_code.ignored))
				{
					return true;
				}
			}

			return false;
		}

		public boolean hasPasses()
		{
			for (TestPackageResult i : packageResults)
			{
				if (i.hasStatus(test_completion_code.success))
				{
					return true;
				}
			}

			return false;
		}

		public boolean hasWarnings()
		{
			for (TestPackageResult pkg : packageResults)
			{
				for (TestClassResult cls : pkg.classResults)
				{
					for (TestResult mth : cls.getTests())
					{
						if (mth.getWarningIds().size() != 0)
						{
							return true;
						}
					}
				}
			}

			return false;
		}

		@Override
		public void testBeginning(ETLTestMethod method) {
			startTime.set(new Long(System.currentTimeMillis()));
		}

		@Override
		public synchronized void testCompleted(ETLTestMethod method, CompletionStatus status) {
			long endTime = System.currentTimeMillis();

			// generate stats for this test
			TestStatistics tStats = getStats(method.getQualifiedName());

			tStats.addExecutorDuration(endTime - startTime.get().longValue());
			tStats.addDuration(endTime - startTime.get().longValue());
			tStats.addRun();

			switch (status.getTestResult()) {
				case success:
					tStats.addPassed();

					if (status.getWarnings().size() != 0) {
						tStats.addWarned();
						tStats.addWarnings(status.getWarnings().size());
					}
					break;
				case failure:
					tStats.addFailed();
					tStats.addFailures(status.getAssertionFailures().size());
					break;
				case warning:
					tStats.addWarned();
					tStats.addWarnings(status.getTestWarningIds().size());
					break;
				case error:
					tStats.addErrored();
					tStats.addErrors(status.getErrors().size());
					break;
				case ignored:
					tStats.addIgnored();
					break;
			}

			TestClassResult ccurrClass = classResults.get(method.getTestClass().getQualifiedName());

			if (ccurrClass == null || method.getTestClass() != ccurrClass.cls) {
				ccurrClass = new TestClassResult();
				ccurrClass.cls = method.getTestClass();

				TestPackageResult tpr = testMap.get(ccurrClass.cls.getPackage());

				if (tpr == null) {
					tpr = new TestPackageResult(method.getTestClass().getPackage());
					packageResults.add(tpr);
					testMap.put(method.getTestClass().getPackage(), tpr);
				}

				tpr.classResults.add(ccurrClass);

				classResults.put(method.getTestClass().getQualifiedName(), ccurrClass);
			}

			// propogate to the class and package
			getStats(method.getTestClass().getQualifiedName()).merge(tStats);
			getStats(method.getTestClass().getPackage().getPackageName()).merge(tStats);

			// add to summary
			testStatistics.merge(tStats);

			TestResult tr = new TestResult();
			tr.method = method;
			tr.status = status;
			tr.executorId = runtimeSupport.getExecutorId();

			ccurrClass.tests.add(tr);

			switch (status.getTestResult()) {
				case failure:
					failedTests.add(method.getTestClass().getQualifiedName(), tr);
					break;
				case warning:
					warnedTests.add(method.getTestClass().getQualifiedName(), tr);
					break;
				case error:
					erroredTests.add(method.getTestClass().getQualifiedName(), tr);
					break;
			}
		}
	}

	protected TestStatistics getStats(String qualifiedName) {
		if (statsByTestType.containsKey(qualifiedName)) {
			return statsByTestType.get(qualifiedName);
		}

		TestStatistics ts = new TestStatistics();
		statsByTestType.put(qualifiedName, ts);

		return ts;
	}

	public File getReportIndexFile()
	{
		return new File(html, "index.html");
	}

	@Override
	public void dispose() {
		File index = getReportIndexFile();

		Map<String, Object> mobj = new HashMap<String, Object>();
		Collections.sort(reportStatusReporter.packageResults);
		mobj.put("hasErrors", reportStatusReporter.hasErrors());
		mobj.put("hasFailures", reportStatusReporter.hasFailures());
		mobj.put("hasIgnores", reportStatusReporter.hasIgnores());
		mobj.put("hasPasses", reportStatusReporter.hasPasses());
		mobj.put("hasWarnings", reportStatusReporter.hasWarnings());
		mobj.put("packageResults", reportStatusReporter.packageResults);
		mobj.put("failedTests", reportStatusReporter.failedTests);
		mobj.put("erroredTests", reportStatusReporter.erroredTests);
		mobj.put("warnedTests", reportStatusReporter.warnedTests);
		mobj.put("testStatistics", testStatistics);
		mobj.put("statsByTestType", statsByTestType);
		mobj.put("runIdentifier", runtimeSupport.getRunIdentifier());
		mobj.put("startTime", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS").format(runtimeSupport.getTestStartTime()));

		File logFile = logManagerRuntimeSupport.getApplicationLogFile();
		// generate the index page.
		try {
			// load the velocity template
			String path = runtimeSupport.getProjectRelativePath(logFile);

			mobj.put("applicationLogPath", path);

			mobj.put("baseRef", runtimeSupport.getProjectRoot().toURI());
			URL url = getClass().getClassLoader().getResource("betterHtmlReportIndex.vm");

			String template = IOUtils.readURLToString(url);

			String text = VelocityUtil.writeTemplate(template, mobj);

			IOUtils.writeBufferToFile(index, new StringBuffer(text));

			url = getClass().getClassLoader().getResource("betterHtmlReportMethod.vm");

			template = IOUtils.readURLToString(url);

			// generate the page for every method
			for (TestPackageResult pack : reportStatusReporter.packageResults)
			{
				for (TestClassResult cls : pack.classResults)
				{
					for (TestResult method : cls.getTests())
					{
						File methIndex = new File(html, method.getIdentifierQualifiedName() + ".html");

						mobj.put("test", method);

						text = VelocityUtil.writeTemplate(template, mobj);

						FileUtils.write(methIndex, text);
					}
				}
			}

			// always keep the latest report under the report index
			File rootTarget = new File(runtimeSupport.getGlobalReportDirectory("html"), "index.html");
			rootTarget.delete();

			Files.createLink(rootTarget.toPath(), index.toPath());

			url = getClass().getClassLoader().getResource("reportMetaRow.vm");
			String colTemplate = IOUtils.readURLToString(url);

			colTemplate = VelocityUtil.writeTemplate(colTemplate, mobj);

			mobj.put("thisReport", colTemplate);

			// update the meta report with the current run
			File metaIndex = new File(runtimeSupport.getGlobalReportDirectory("html"), "reports.html");

			if (!metaIndex.exists())
			{
				url = getClass().getClassLoader().getResource("reportMetaIndex.vm");
				template = IOUtils.readURLToString(url);
			}
			else
			{
				template = FileUtils.readFileToString(metaIndex);
			}

			text = template.replace("<!-- report -->", colTemplate);

			IOUtils.writeBufferToFile(metaIndex, new StringBuffer(text));
		} catch (Exception e) {
			applicationLog.severe("Error writing html report index file", e);
		}
	}

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log) {
		this.applicationLog = log;
	}

	@Inject
	public void receiveDiffManager(DiffManager manager) {
		diffManager = manager;
	}

	@Inject
	public void setMetaDataManager(MetaDataManager manager) {
		metaDataManager = manager;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport rs) throws IOException {
		runtimeSupport = rs;
		html = new FileBuilder(runtimeSupport.getReportDirectory("html")).mkdirs().file().getCanonicalFile();
	}

	@Inject
	public void receiveLogManagerRuntimeSupport(LogManagerRuntimeSupport rs) {
		logManagerRuntimeSupport = rs;
	}

	@Override
	public List<String> getPrerequisites() {
		return prerequisites;
	}

	@Override
	public void initialize(Injector inj) {
		postCreate(reportStatusReporter);
	}

	@Override
	public StatusReporter getStatusReporter() {
		return reportStatusReporter;
	}

	@Override
	public String getFeatureName() {
		return "better-report";
	}

	@Inject
	public void receiveLogFileManager(LogFileManager logFileManager) {
		this.logFileManager = logFileManager;
	}

	@Override
	public boolean requiredForSimulation() {
		return true;
	}
}

class Locals extends NullClassListener
{
	private final BetterReportFeatureModule betterReportFeatureModule;

	private final Map<String, Long> locals = new HashMap<String, Long>();

	Locals(BetterReportFeatureModule betterReportFeatureModule) {
		this.betterReportFeatureModule = betterReportFeatureModule;
	}

	@Override
	public void beginTests(VariableContext context) {
		start("[root]");
	}

	@Override
	public void endTests(VariableContext context) {
		end("[root]");

		// record all time
		betterReportFeatureModule.getStats("[root]").setDuration(difference("[root]"));
	}

	@Override
	public void beginPackage(ETLTestPackage name, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		start(name.getPackageName());
	}

	@Override
	public void begin(ETLTestClass cl, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		start(cl.getQualifiedName());
	}

	@Override
	public void begin(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		start(mt.getQualifiedName());
	}

	@Override
	public void end(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// record the end time for this method
		end(mt.getQualifiedName());
	}

	@Override
	public void end(ETLTestClass cl, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// record the end time for this class
		end(cl.getQualifiedName());

		// now that we have finished a class, let's record the actual runtime
		betterReportFeatureModule.getStats(cl.getQualifiedName()).setDuration(getClassDuration(cl));
	}

	@Override
	public void endPackage(ETLTestPackage name, VariableContext context, int executorId) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// record the end time for this package
		end(name.getPackageName());

		// now that we have finished a class, let's record the actual runtime
		long packageDuration = getPackageDuration(name);
		//System.out.println(name.getPackageName() + ":" + packageDuration);
		betterReportFeatureModule.getStats(name.getPackageName()).setDuration(packageDuration);
	}

	private synchronized void start(String qualifiedName) {
		Long currStart = locals.get(qualifiedName + ".start");

		if (currStart == null)
		{
			locals.put(qualifiedName + ".start", System.currentTimeMillis());
		}
	}

	long getMethodDuration(ETLTestMethod method)
	{
		// grab the start and end times and return the difference
		return difference(method.getQualifiedName());
	}

	private synchronized long difference(String qualifiedName) {
		Long startTime = locals.get(qualifiedName + ".start");
		Long endTime = locals.get(qualifiedName + ".end");

		if (startTime == null || endTime == null)
		{
			return 0L;
		}

		return endTime.longValue() - startTime.longValue();
	}

	long getClassDuration(ETLTestClass cls)
	{
		return difference(cls.getQualifiedName());
	}

	private synchronized void end(String packageName) {
		locals.put(packageName + ".end", System.currentTimeMillis());
	}

	long getPackageDuration(ETLTestPackage pack)
	{
		return difference(pack.getPackageName());
	}
}
