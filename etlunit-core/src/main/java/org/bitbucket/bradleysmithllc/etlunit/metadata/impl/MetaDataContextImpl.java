package org.bitbucket.bradleysmithllc.etlunit.metadata.impl;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaData;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

public class MetaDataContextImpl implements MetaDataContext
{
	private RuntimeSupport runtimeSupport;

	private String name;
	private MetaData metaData;
	private final List<MetaDataPackageContext> contexts	= new ArrayList<MetaDataPackageContext>();
	private final Map<String, MetaDataPackageContext> packageContextMap = new HashMap<String, MetaDataPackageContext>();

	public MetaDataContextImpl(String name) {
		this.name = name;
	}

	public MetaDataContextImpl() {
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
	}

	public void setMetaData(MetaData data)
	{
		metaData = data;
	}

	@Override
	public MetaData getMetaData() {
		return metaData;
	}

	@Override
	public String getContextName() {
		return name;
	}

	@Override
	public Map<String, MetaDataPackageContext> getPackageContextMap() {
		return Collections.unmodifiableMap(packageContextMap);
	}

	@Override
	public synchronized MetaDataPackageContext createPackageContext(ETLTestPackage pack, MetaDataPackageContext.path_type pathType) {
		String key = key(pack, pathType);

		if (packageContextMap.containsKey(key))
		{
			return packageContextMap.get(key);
		}

		MetaDataPackageContextImpl mpci = new MetaDataPackageContextImpl(pack, pathType);
		mpci.setMetaDataContext(this);
		mpci.receiveRuntimeSupport(runtimeSupport);

		packageContextMap.put(key, mpci);
		contexts.add(mpci);

		return mpci;
	}

	@Override
	public MetaDataPackageContext createPackageContextForCurrentTest(MetaDataPackageContext.path_type pathType) {
		if (runtimeSupport == null)
		{
			throw new IllegalStateException("Runtime support not available");
		}

		return createPackageContext(runtimeSupport.getCurrentlyProcessingTestPackage(), pathType);
	}

	@Override
	public List<MetaDataPackageContext> getPackageContexts() {
		return Collections.unmodifiableList(contexts);
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("name").value(name);

		writer.name("package-contexts");
		writer.beginArray();

		for (MetaDataPackageContext mpci : contexts)
		{
			writer.beginObject();
			mpci.toJson(writer);
			writer.endObject();
		}

		writer.endArray();
	}

	@Override
	public synchronized void fromJson(JsonNode node) {
		contexts.clear();
		packageContextMap.clear();

		name = node.get("name").asText();

		ArrayNode anode = (ArrayNode) node.get("package-contexts");

		Iterator<JsonNode> it = anode.elements();

		while (it.hasNext())
		{
			MetaDataPackageContextImpl mpci = new MetaDataPackageContextImpl();
			mpci.receiveRuntimeSupport(runtimeSupport);

			mpci.fromJson(it.next());
			String key = key(mpci.getPackageName(), mpci.getPathType());

			contexts.add(mpci);
			packageContextMap.put(key, mpci);
		}
	}

	private String key(ETLTestPackage packageName, MetaDataPackageContext.path_type pathType) {
		return packageName.getPackageName() + "[" + pathType + "]";
	}

	public boolean equals(Object other)
	{
		if (getClass() != other.getClass())
		{
			return false;
		}

		MetaDataContextImpl mpci = (MetaDataContextImpl) other;

		return ObjectUtils.compare(name, mpci.name) == 0 && contexts.equals(mpci.contexts);
	}
}
