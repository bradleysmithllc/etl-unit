package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class ThreadedStreamHandler
{
	private static final class StreamHandle
	{
		private final String identifier;
		private final InputStream in;
		NfurcatingOutputStream nfurcatingOutputStream;
		private boolean ended = false;
		private boolean enabled = false;

		private final byte [] buff = new byte[2048];

		private StreamHandle(String identifier, InputStream in) {
			this.identifier = identifier;
			this.in = in;
		}

		private void run() throws IOException {
			if (nfurcatingOutputStream != null && enabled)
			{
				while (in.available() > 0)
				{
					int read = in.read(buff, 0, Math.min(in.available(), buff.length));

					if (read == -1)
					{
						nfurcatingOutputStream.flush();
						ended = true;
					}
					else if (read != 0)
					{
						nfurcatingOutputStream.write(buff, 0, read);
					}
					else
					{
						// 0 bytes handled - let's use that as a wait
						return;
					}

					Thread.yield();
				}
			}
		}
	}

	private final class StreamHandler implements Runnable
	{
		public void run()
		{
			List<StreamHandle> endedStreams = new ArrayList<StreamHandle>();

			while (runnerSignal.availablePermits() == 0)
			{
				endedStreams.clear();

				// iterate over handles
				for (int i = 0; i < streamHandles.size(); i++)
				{
					try {
						StreamHandle streamHandle = streamHandles.get(i);
						streamHandle.run();

						if (streamHandle.ended)
						{
							endedStreams.add(streamHandle);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				for (StreamHandle handle : endedStreams)
				{
					closeStreamHandle(handle.identifier);
				}

				try {
					Thread.sleep(500L);
				} catch (InterruptedException e) {
					e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
				}
			}
		}
	}

	private final Semaphore runnerSignal = new Semaphore(1);

	private final Map<String, StreamHandle> streamHandleMap = new HashMap<String, StreamHandle>();
	private final List<StreamHandle> streamHandles = new ArrayList<StreamHandle>();

	public ThreadedStreamHandler() {
		runnerSignal.acquireUninterruptibly(1);

		new Thread(new StreamHandler()).start();
	}

	public void createStreamHandle(String identifier, InputStream in)
	{
		StreamHandle handle = new StreamHandle(identifier, in);

		streamHandles.add(handle);
		streamHandleMap.put(identifier, handle);
	}

	public void addOutputStreamToHandle(String identifier, OutputStream out)
	{
		StreamHandle handle = null;

		handle = streamHandleMap.get(identifier);

		if (handle == null)
		{
			throw new IllegalArgumentException("Stream handle not found for identifier " + identifier);
		}

		synchronized(handle)
		{
			if (handle.nfurcatingOutputStream == null)
			{
				handle.nfurcatingOutputStream = new NfurcatingOutputStream(out);
			}
			else
			{
				handle.nfurcatingOutputStream.bifurcate(out);
			}
		}
	}

	public void enableStreamHandle(String identifier)
	{
		StreamHandle streamHandle = streamHandleMap.get(identifier);

		if (streamHandle == null)
		{
			throw new IllegalArgumentException("Missing stream handle");
		}

		streamHandle.enabled = true;
	}

	public void closeStreamHandle(String identifier) {
		StreamHandle handle = streamHandleMap.remove(identifier);

		streamHandles.remove(handle);
	}

	public void disable()
	{
		runnerSignal.release();
	}
}
