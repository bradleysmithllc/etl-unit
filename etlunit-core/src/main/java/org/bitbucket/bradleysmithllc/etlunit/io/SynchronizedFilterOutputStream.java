package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.Semaphore;

public class SynchronizedFilterOutputStream extends FilterOutputStream
{
	private final Semaphore lock;

	public SynchronizedFilterOutputStream(OutputStream out, Semaphore lock)
	{
		super(out);

		this.lock = lock;
	}

	public SynchronizedFilterOutputStream(OutputStream out)
	{
		this(out, new Semaphore(1));
	}

	@Override
	public final void write(int b) throws IOException
	{
		lock.acquireUninterruptibly();

		try
		{
			out.write(b);
		}
		finally
		{
			lock.release();
		}
	}

	@Override
	public final void write(byte[] b) throws IOException
	{
		lock.acquireUninterruptibly();

		try
		{
			out.write(b);
		}
		finally
		{
			lock.release();
		}
	}

	@Override
	public final void write(byte[] b, int off, int len) throws IOException
	{
		lock.acquireUninterruptibly();

		try
		{
			out.write(b, off, len);
		}
		finally
		{
			lock.release();
		}
	}

	@Override
	public final void flush() throws IOException
	{
		lock.acquireUninterruptibly();

		try
		{
			out.flush();
		}
		finally
		{
			lock.release();
		}
	}

	@Override
	public final void close() throws IOException
	{
		lock.acquireUninterruptibly();

		try
		{
			out.close();
		}
		finally
		{
			lock.release();
		}
	}
}
