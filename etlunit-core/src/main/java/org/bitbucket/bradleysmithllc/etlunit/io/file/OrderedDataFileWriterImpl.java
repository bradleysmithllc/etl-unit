package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class OrderedDataFileWriterImpl implements DataFileWriter
{
	private final RelationalDataFileWriterImpl writerImpl;
	private final TemporaryTable temporaryTable;
	private final DataFileWriter temporaryTableWriter;
	private final DataFileManager.CopyOptions options;

	List<Map<String, Object>> setData = new ArrayList<Map<String, Object>>();

	public OrderedDataFileWriterImpl(DataFile flatFile, DataFileManager.CopyOptions options) throws IOException
	{
		this.options = options;
		writerImpl = new RelationalDataFileWriterImpl(flatFile, options);

		// create a table in the working database for storing and sorting this data
		try {
			temporaryTable = flatFile.createTemporaryDbTable();
		} catch (SQLException e) {
			throw new IOException(e);
		}
		temporaryTableWriter = temporaryTable.getWriter(options);
	}

	@Override
	public void close() throws IOException
	{
		temporaryTableWriter.close();

		DataFileReader tData = temporaryTable.getData();

		Iterator<DataFileReader.FileRow> it = tData.iterator();

		while (it.hasNext())
		{
			DataFileReader.FileRow nextObject = it.next();
			Map<String, Object> objectMap = nextObject.getData();
			writerImpl.addRow(objectMap);
		}

		writerImpl.close();
	}

	@Override
	public void addRow(Map<String, Object> rowData) throws IOException
	{
		temporaryTableWriter.addRow(rowData);
	}
}
