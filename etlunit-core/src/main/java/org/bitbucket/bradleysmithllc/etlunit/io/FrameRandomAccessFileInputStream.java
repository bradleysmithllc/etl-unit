package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

/**
 * Adapts a changing random access file to an input stream.
 */
public class FrameRandomAccessFileInputStream extends InputStream {
	byte[] currentBuffer;
	int currentOffset = 0;
	boolean eos = false;
	StreamFrame frame = null;

	private final RandomAccessFile randomAccessFile;

	public FrameRandomAccessFileInputStream(RandomAccessFile raf) {
		randomAccessFile = raf;
	}

	public FrameRandomAccessFileInputStream(File file) throws IOException {
		randomAccessFile = new RandomAccessFile(file, "r");
	}

	@Override
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (eos) {
			return -1;
		}

		if (currentBuffer == null) {
			// refresh - poll if necessary
			while (available() <= 0)
			{
				// a small enough interval for something to happen, but not so small that we busy the cpu
				try {
					Thread.sleep(100L);
				} catch (InterruptedException e) {
					throw new IOException(e);
				}
			}
		}

		if (eos) {
			return -1;
		}

		// how much to send?
		int available = Math.min(len, currentBuffer.length - currentOffset);

		// return the next block
		System.arraycopy(currentBuffer, currentOffset, b, off, available);

		// advance the pointer
		currentOffset += available;

		if (currentOffset >= currentBuffer.length) {
			currentBuffer = null;
		}

		return available;
	}

	@Override
	public int available() throws IOException {
		if (currentBuffer == null)
		{
			if (StreamFrame.frameAvailable(randomAccessFile))
			{
				frame = StreamFrame.nextFrame(randomAccessFile);
				currentBuffer = frame.getData();
				currentOffset = 0;

				if (currentBuffer.length == 0)
				{
					// close up
					randomAccessFile.close();
					eos = true;
				}
			}
			else
			{
				return 0;
			}
		}

		return eos ? 1 : (currentBuffer.length - currentOffset);
	}

	@Override
	public int read() throws IOException {
		byte [] buff = new byte[1];

		int i = read(buff);

		if (i == -1)
		{
			return i;
		}

		return buff[i] & 0xFF;
	}

	@Override
	public void close() throws IOException {
		randomAccessFile.close();
	}
}
