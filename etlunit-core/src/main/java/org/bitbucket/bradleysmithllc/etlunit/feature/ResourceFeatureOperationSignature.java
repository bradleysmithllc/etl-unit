package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.main.JsonSchema;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.OperationProcessor;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import javax.annotation.Nullable;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;

public class ResourceFeatureOperationSignature implements FeatureOperationSignature
{
	private final String id;
	private final FeatureOperation parent;
	private final boolean sub;
	private final ResourceFeatureMetaInfo resourceFeatureMetaInfo;

	private final JsonSchema validator;
	private final JsonNode validatorNode;

	public enum methodType
	{
		/**
		 * Method is not an operation processor.
		 */
		invalid,
		/**
		 * Method is an operation processor without executor id.
		 */
		lacksExecutor,
		/**
		 * Method is an operation processor with executor id.
		 */
		requiresExecutor
	}

	public ResourceFeatureOperationSignature(ResourceFeatureOperation resourceFeatureOperation, ResourceFeatureMetaInfo resourceFeatureMetaInfo, String sig)
	{
		id = sig;
		parent = resourceFeatureOperation;
		this.resourceFeatureMetaInfo = resourceFeatureMetaInfo;
		sub = !parent.getName().equals(id);

		validatorNode = resourceFeatureMetaInfo.loadValidatorNode((isSubOperation() ? (parent.getName() + "." + id) : id));
		validator = resourceFeatureMetaInfo.loadValidator(validatorNode, id);
	}

	@Override
	public String getId()
	{
		return id;
	}

	@Override
	public JsonSchema getValidator()
	{
		return validator;
	}

	@Override
	public JsonNode getValidatorNode()
	{
		return validatorNode;
	}

	@Override
	public boolean isSubOperation()
	{
		return sub;
	}

	@Override
	public action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		// TODO - get application log
		Feature describing = resourceFeatureMetaInfo.getDescribing();
		List<ClassListener> llist = describing.getListenerList();

		for (ClassListener listener : llist)
		{
			action_code act_code = processIdealMethod(listener, describing, mt, op, parameters, vcontext, econtext, resourceFeatureMetaInfo.getApplicationLog(), executor);

			if (act_code == action_code.reject || act_code == action_code.handled)
			{
				return act_code;
			}
		}

		return action_code.defer;
	}

	private action_code processIdealMethod(OperationProcessor listener, Feature feature, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj_new, VariableContext context, ExecutionContext econtext, Log applicationLog, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		Package p = feature.getClass().getPackage();
		String name = p.getName() + ".json." + EtlUnitStringUtils.makeUnderscoreName(feature.getFeatureName());

		applicationLog.info("Searching for handler for operation: '" + op.getOperationName() + "' with signature '" + id + "': " + (obj_new == null
				? ""
				: obj_new.getJsonNode()));

		String typeBase = name + "." + EtlUnitStringUtils.preparePropertyForPackageName(parent.getName()) + ".";

		if (isSubOperation())
		{
			typeBase += EtlUnitStringUtils.preparePropertyForPackageName(id) + ".";
		}

		typeBase += EtlUnitStringUtils.makeProperPropertyName(id);

		String typeName = typeBase + "Request";
		String interfaceName = typeBase + "Handler";

		// check if such types exist
		try
		{
			applicationLog.info("Searching for operation type : " + typeName);

			Class typeCl = feature.getClass().getClassLoader().loadClass(typeName);

			applicationLog.info("Resolved operation type : " + typeCl);

			try
			{
				applicationLog.info("Searching for interface type : " + interfaceName);
				Class interfaceCl = feature.getClass().getClassLoader().loadClass(interfaceName);

				applicationLog.info("Resolved operation processor type : " + interfaceCl);

				if (interfaceCl.isAssignableFrom(listener.getClass()))
				{
					applicationLog.info("Using operation processor interface");

					// iterate through the methods, and call the one which matches this signature
					for (Method method : interfaceCl.getMethods())
					{
						methodType methodT = inspectOperationProcessorMethod(method, typeCl);

						Type [] types = method.getGenericParameterTypes();

						if (methodT != methodType.invalid)
						{
							// load the gson object
							// give gson a shot at filling it out

							ObjectMapper om = new ObjectMapper();

							String json = obj_new == null ? "{}" : obj_new.getJsonNode().toString();

							try {
								Object confObj = om.readValue(json, typeCl);

								Object o =
									methodT == methodType.lacksExecutor ?
										method.invoke(listener, confObj, mt, op, obj_new, context, econtext)
										:
										method.invoke(listener, confObj, mt, op, obj_new, context, econtext, executor);

								return (action_code) o;
							} catch (IOException e) {
								throw new TestExecutionError(TestConstants.ERR_BAD_OPERANDS, "", e);
							}
						}
					}

					applicationLog.info("Feature exposes an interface type, but does not have the required process method");
				}
				else
				{
					applicationLog.info("Feature exposes an interface type, but does not implement it");
					return listener.process(mt, op, obj_new, context, econtext, executor);
				}
			}
			catch (ClassNotFoundException e)
			{
				applicationLog.info("Feature exposes an operation type, but not an interface");
			}
			catch (InvocationTargetException e)
			{
				if (e.getCause() instanceof TestAssertionFailure)
				{
					throw (TestAssertionFailure) e.getCause();
				}
				else if (e.getCause() instanceof TestWarning)
				{
					throw (TestWarning) e.getCause();
				}
				else if (e.getCause() instanceof TestExecutionError)
				{
					throw (TestExecutionError) e.getCause();
				}

				throw new TestExecutionError("", e);
			}
			catch (IllegalAccessException e)
			{
				applicationLog.info("Feature exposes an interface type, but does not expose the required process method");
			}
		}
		catch (ClassNotFoundException e)
		{
		}

		return listener.process(mt, op, obj_new, context, econtext, executor);
	}

	public static methodType inspectOperationProcessorMethod(Method method, Class typeCl) {
		Type [] types = method.getGenericParameterTypes();

		if (types != null)
		{
			// check for 6 or 7 parameters
			if (types.length == 6 || types.length == 7)
			{
				// check first six
				if (
					types[0].equals(typeCl)
					&&
					types[1].equals(ETLTestMethod.class)
					&&
					types[2].equals(ETLTestOperation.class)
					&&
					types[3].equals(ETLTestValueObject.class)
					&&
					types[4].equals(VariableContext.class)
					&&
					types[5].equals(ExecutionContext.class)
				)
				{
					// if six, good without executor
					if (types.length == 6)
					{
						return methodType.lacksExecutor;
					}
					else if (types[6].equals(int.class))
					{
						// executor here
						return methodType.requiresExecutor;
					}
					else
					{
						// nothing - fall through to invalid
					}
				}
			}
		}

		return methodType.invalid;
	}
}
