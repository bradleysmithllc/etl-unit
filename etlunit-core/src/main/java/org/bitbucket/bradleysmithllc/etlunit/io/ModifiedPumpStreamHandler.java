package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.exec.ExecuteStreamHandler;

import java.io.*;
import java.util.concurrent.CountDownLatch;

public class ModifiedPumpStreamHandler implements ExecuteStreamHandler
{
	private final CustomPumpStreamHandler pumpStreamHandler;

	private final CountDownLatch streamAcquiredLatch = new CountDownLatch(3);

	/**
	 * These two pipes are for process clients to write output into the input stream of the
	 * external process.
	 */
	//private final PipedOutputStream processStandardInputWriter = new PipedOutputStream();
	//private final PipedInputStream processStandardInputReader;

	/**
	 * These two streams are for process clients to read the standard out / err streams
	 * from the external process.
	 */
	private final PipedOutputStream processStandardOutputWriter = new PipedOutputStream();
	private final PipedInputStream processStandardOutputReader;

	private OutputStream processInputStream;
	private Writer writer;
	private final Reader reader;

	private final NfurcatingOutputStream outputAggregator;

	public ModifiedPumpStreamHandler() throws IOException
	{
		this(null);
	}

	public ModifiedPumpStreamHandler(File output) throws IOException
	{
		processStandardOutputReader = new PipedInputStream(processStandardOutputWriter, 65536);
		//processStandardInputReader = new PipedInputStream(processStandardInputWriter, 65536);

		OutputStream os = processStandardOutputWriter;

		if (output != null)
		{
			if (output.exists() && !output.canWrite())
			{
				throw new IOException("Target file not accessible: " + output.getAbsolutePath());
			}

			BufferedOutputStream osBuff = new BufferedOutputStream(new FileOutputStream(output));
			outputAggregator = new NfurcatingOutputStream(osBuff);
			os = outputAggregator.bifurcate(processStandardOutputWriter);
		}
		else
		{
			outputAggregator = null;
		}

		pumpStreamHandler = new CustomPumpStreamHandler(
				os//,
				//processStandardInputReader
		);

		//writer = new OutputStreamWriter(new BifurcatingOutputStream(processStandardInputWriter, System.out));
		reader = new InputStreamReader(processStandardOutputReader);
	}

	public InputStream getProcessOutputStream()
	{
		return processStandardOutputReader;
	}

	public OutputStream getProcessInputStream()
	{
		return processInputStream;
	}

	public Writer getProcessInput()
	{
		return writer;
	}

	public Reader getProcessOutput()
	{
		return reader;
	}

	public void setProcessOutputStream(InputStream is)
	{
		pumpStreamHandler.setProcessOutputStream(is);
		streamAcquiredLatch.countDown();
	}

	public void stop()
	{
		pumpStreamHandler.stop();

		if (outputAggregator != null)
		{
			try
			{
				outputAggregator.close();
			}
			catch (IOException e)
			{
				throw new IllegalArgumentException(e);
			}
		}
	}

	public void start()
	{
		pumpStreamHandler.start();
	}

	public void setProcessInputStream(OutputStream os) throws IOException
	{
		if (outputAggregator != null)
		{
			processInputStream = outputAggregator.bifurcate(os);
		}
		else
		{
			processInputStream = os;
		}

		writer = new OutputStreamWriter(processInputStream);
		//pumpStreamHandler.setProcessInputStream(os);
		streamAcquiredLatch.countDown();
	}

	public void setProcessErrorStream(InputStream is)
	{
		pumpStreamHandler.setProcessErrorStream(is);
		streamAcquiredLatch.countDown();
	}

	public void waitForStreams()
	{
		try {
			streamAcquiredLatch.await();
		} catch (InterruptedException e) {
			throw new RuntimeException("Why did this happen?", e);
		}
	}

	public void waitForOutputStreamToComplete() throws IOException {
		pumpStreamHandler.waitForOutput();

		pumpStreamHandler.getOut().flush();
		pumpStreamHandler.getErr().flush();
	}
}
