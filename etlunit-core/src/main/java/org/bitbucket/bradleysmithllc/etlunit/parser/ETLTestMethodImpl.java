package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ETLTestMethodImpl extends ETLTestAnnotatedImpl implements ETLTestMethod
{
	private final String className;
	private final method_type type;

	private final ETLTestClass ETLTestClass;

	private final List<ETLTestOperation> operations = new ArrayList<ETLTestOperation>();
	private final List<ETLTestOperation> operationsPub = Collections.unmodifiableList(operations);

	public ETLTestMethodImpl(String name, method_type mt, ETLTestClass cls)
	{
		className = name;
		type = mt;
		ETLTestClass = cls;
	}

	public ETLTestMethodImpl(String name, method_type mt, ETLTestClass cls, Token t)
	{
		super(t);
		className = name;
		type = mt;
		ETLTestClass = cls;
	}

	public String getName()
	{
		return className;
	}

	@Override
	public String getQualifiedName()
	{
		return ETLTestClass.getQualifiedName() + "." + getName();
	}

	public method_type getMethodType()
	{
		return type;
	}

	public void addOperation(ETLTestOperation op)
	{
		operations.add(op);
	}

	public List<ETLTestOperation> getOperations()
	{
		return operationsPub;
	}

	public boolean requiresPurge()
	{
		return !hasAnnotation("@DoNotPurge");
	}

	public ETLTestClass getTestClass()
	{
		return ETLTestClass;
	}
}
