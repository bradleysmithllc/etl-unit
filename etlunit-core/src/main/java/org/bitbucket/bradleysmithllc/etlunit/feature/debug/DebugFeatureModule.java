package org.bitbucket.bradleysmithllc.etlunit.feature.debug;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestAnnotation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.util.List;

@FeatureModule
public class DebugFeatureModule extends AbstractFeature
{
	@Override
	public String getFeatureName()
	{
		return "debug";
	}

	@Override
	public ClassListener getListener()
	{
		return new NullClassListener()
		{
			@Override
			public action_code process(ETLTestOperation op, ETLTestValueObject obj, final VariableContext context, ExecutionContext econtext, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				String name = op.getOperationName();

				if (name.equals("error"))
				{
					ETLTestValueObject errorId = obj.query("error-id");

					if (errorId != null)
					{
						throw new TestExecutionError(obj.getValueAsMap().get("message").getValueAsString(),
								errorId.getValueAsString());
					}
					else
					{
						throw new TestExecutionError(obj.getValueAsMap().get("message").getValueAsString());
					}
				}
				else if (name.equals("die"))
				{
					throw new RuntimeException("Blech");
				}
				else if (name.equals("fail"))
				{
					ETLTestValueObject errorId = obj.query("failure-id");

					if (errorId != null)
					{
						throw new TestAssertionFailure(op.getOperands().getValueAsMap().get("message").getValueAsString(),
								errorId.getValueAsString());
					}
					else
					{
						throw new TestAssertionFailure(op.getOperands().getValueAsMap().get("message").getValueAsString());
					}
				}
				else if (name.equals("warn"))
				{
					ETLTestValueObject errorId = obj.query("warning-id");

					if (errorId != null)
					{
						throw new TestWarning(op.getOperands().getValueAsMap().get("message").getValueAsString(),
								errorId.getValueAsString());
					}
					else
					{
						throw new TestWarning(op.getOperands().getValueAsMap().get("message").getValueAsString());
					}
				}
				else if (name.equals("wait"))
				{
					ETLTestValueObject sleepTime = obj.query("sleep-time");

					if (sleepTime != null)
					{
						try {
							Thread.sleep(Long.parseLong(sleepTime.getValueAsString()));
						} catch (InterruptedException e) {
							throw new TestExecutionError("Cannot sleep", e);
						}
					}

					return action_code.handled;
				}
				else if (name.equals("assert"))
				{
					ETLTestValueObject var = obj.query("variable");
					ETLTestValueObject value = obj.query("value");

					if (var != null && value != null)
					{
						String varName = var.getValueAsString();

						ETLTestValueObject cvalue = context.getValue(varName);

						if (!cvalue.getJsonNode().equals(value.getJsonNode()))
						{
							throw new TestAssertionFailure("Variable " + varName + " does not have the correct value");
						}

						return action_code.handled;
					}
				}

				return action_code.defer;
			}

			@Override
			public void end(ETLTestMethod mt, VariableContext context, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
				// look for an annotation to throw an error
				List<ETLTestAnnotation> err = mt.getAnnotations("@Error");

				if (err.size() != 0)
				{
					String errorId = TestConstants.ERR_UNSPECIFIED;

					ETLTestAnnotation errAnno = err.get(0);

					ETLTestValueObject value = errAnno.getValue();

					if (value != null)
					{
						value = value.query("error-id");

						if (value != null)
						{
							errorId = value.getValueAsString();
						}
					}

					throw new TestExecutionError("", errorId);
				}

				// look for an annotation to throw an error
				err = mt.getAnnotations("@Failure");

				if (err.size() != 0)
				{
					String errorId = TestConstants.FAIL_UNSPECIFIED;

					ETLTestAnnotation errAnno = err.get(0);

					ETLTestValueObject value = errAnno.getValue();

					if (value != null)
					{
						value = value.query("failure-id");

						if (value != null)
						{
							errorId = value.getValueAsString();
						}
					}

					throw new TestExecutionError("", errorId);
				}
			}
		};
	}

	@Override
	public boolean requiredForSimulation() {
		return true;
	}
}
