package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class ETLTestOperandImpl extends ETLTestDebugTraceableImpl implements ETLTestOperand
{
	private final ETLTestValueObject value;

	private final ETLTestOperation operation;

	public ETLTestOperandImpl(ETLTestOperation pOperation, ETLTestValueObject op)
	{
		operation = pOperation;
		value = op;
	}

	public ETLTestOperandImpl(ETLTestOperation pOperation, ETLTestValueObject op, Token t)
	{
		super(t);
		operation = pOperation;
		value = op;
	}

	@Override
	public ETLTestValueObject getValue()
	{
		return value;
	}

	@Override
	public String getOperandAsString()
	{
		return value.getValueAsString();
	}

	@Override
	public ETLTestOperation getOperation()
	{
		return operation;
	}
}
