package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ETLTestCases
{
	public class ExecutorTestCases
	{
		private final ETLTestClass etlTestClass;
		private final List<ETLTestMethod> etlTestMethods = new ArrayList<ETLTestMethod>();

		public ExecutorTestCases(ETLTestClass etlTestClass) {
			this.etlTestClass = etlTestClass;
		}

		public void addTestMethod(ETLTestMethod method)
		{
			etlTestMethods.add(method);
		}

		public ETLTestClass getEtlTestClass() {
			return etlTestClass;
		}

		public List<ETLTestMethod> getEtlTestMethods() {
			return etlTestMethods;
		}
	}

	private int testCount;

	private final List<ExecutorTestCases> etlTestMethods = new ArrayList<ExecutorTestCases>();
	private final Map<String, ExecutorTestCases> etlTestCases = new HashMap<String, ExecutorTestCases>();

	public void addTestMethod(ETLTestMethod method)
	{
		ETLTestClass testClass = method.getTestClass();

		ExecutorTestCases list = etlTestCases.get(testClass.getQualifiedName());

		// if non existent, create
		if (list == null)
		{
			list = new ExecutorTestCases(testClass);
			etlTestCases.put(list.etlTestClass.getQualifiedName(), list);
			etlTestMethods.add(list);
		}

		// add this method to the list
		list.addTestMethod(method);

		// move the pointer forward
		testCount++;
	}

	public void addTestClasses(List<ETLTestClass> classes)
	{
		for (ETLTestClass cls : classes)
		{
			for (ETLTestMethod metho : cls.getTestMethods())
			{
				addTestMethod(metho);
			}
		}
	}

	public ExecutorTestCases getTestCases(ETLTestClass etlC)
	{
		return etlTestCases.get(etlC.getQualifiedName());
	}

	public List<ExecutorTestCases> getEtlTestMethods() {
		return etlTestMethods;
	}

	public int getTestCount() {
		return testCount;
	}
}
