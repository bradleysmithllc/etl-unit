package org.bitbucket.bradleysmithllc.etlunit.listener;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ExecutionContext;
import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.TestWarning;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import javax.annotation.Nullable;

public interface OperationProcessor extends ClassResponder
{
	/**
	 * @param mt - The method being tested.  May differ from the method the operation belongs
	 *           to in the case this is a before or after test method.  Null if processing
	 *           a before or after class method.
	 * @param op
	 * @return handled if the operation was handled, reject to skip, and defer to allow a downstream
	 *         listener to evaluate.
	 * @throws org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure
	 *
	 * @throws org.bitbucket.bradleysmithllc.etlunit.TestExecutionError
	 * @throws org.bitbucket.bradleysmithllc.etlunit.TestWarning
	 */
	action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executorId)
			throws TestAssertionFailure, TestExecutionError, TestWarning;
}
