package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.listener.OperationProcessor;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import javax.annotation.Nullable;
import java.util.List;

public class OperationProcessorSpammer implements OperationProcessor
{
	private final List<OperationProcessor> processors;

	public OperationProcessorSpammer(List<OperationProcessor> processors)
	{
		this.processors = processors;
	}

	@Override
	public action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		for (OperationProcessor processor : processors)
		{
			action_code actionCode = processor.process(mt, op, parameters, vcontext, econtext, executor);
			if (actionCode != action_code.defer)
			{
				return actionCode;
			}
		}

		return action_code.defer;
	}
}
