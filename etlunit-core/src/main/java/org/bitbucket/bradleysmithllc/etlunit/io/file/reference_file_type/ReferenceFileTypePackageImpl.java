package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.JsonSerializable;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ReferenceFileTypePackageImpl implements ReferenceFileTypePackage, JsonSerializable
{
	private String description;
	private final String name;
	private final Map<String, ReferenceFileType> typesById = new HashMap<String, ReferenceFileType>();
	private final Map<String, ReferenceFileType> typesByName = new HashMap<String, ReferenceFileType>();
	private final Map<String, ReferenceFileType> typesByLowerId = new HashMap<String, ReferenceFileType>();
	private final Map<String, ReferenceFileType> typesByLowerName = new HashMap<String, ReferenceFileType>();

	private RuntimeSupport runtimeSupport;

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport supp)
	{
		runtimeSupport = supp;
	}

	public ReferenceFileTypePackageImpl(String name) {
		this.name = name;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("description").value(description);

		writer.name("types");
		writer.beginObject();

		for (Map.Entry<String, ReferenceFileType> type : typesByName.entrySet())
		{
			writer.name(type.getKey());
			writer.beginObject();

			ReferenceFileTypeImpl impl = (ReferenceFileTypeImpl) type.getValue();

			impl.toJson(writer);
			writer.endObject();
		}

		writer.endObject();
	}

	@Override
	public void fromJson(JsonNode node) {
		description = node.get("description").asText();

		JsonNode typesNode = node.get("types");

		Iterator<Map.Entry<String, JsonNode>> fields = typesNode.fields();

		while (fields.hasNext())
		{
			Map.Entry<String, JsonNode> nextField = fields.next();

			String name = nextField.getKey();
			JsonNode nextNode = nextField.getValue();

			String id = getName() + "." + name;
			ReferenceFileTypeImpl impl = new ReferenceFileTypeImpl(name, id);
			impl.receiveRuntimeSupport(runtimeSupport);
			impl.fromJson(nextNode);

			typesByLowerId.put(id.toLowerCase(), impl);
			typesById.put(id, impl);
			typesByLowerName.put(name.toLowerCase(), impl);
			typesByName.put(name, impl);
		}
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Map<String, ReferenceFileType> getReferenceFileTypesById() {
		return Collections.unmodifiableMap(typesByLowerId);
	}

	@Override
	public Map<String, ReferenceFileType> getReferenceFileTypesByName() {
		return Collections.unmodifiableMap(typesByLowerName);
	}

	@Override
	public int compareTo(ReferenceFileTypePackage o) {
		return getName().compareTo(o.getName());
	}
}
