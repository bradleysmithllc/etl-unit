package org.bitbucket.bradleysmithllc.etlunit.feature.logging;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.util.HashMapArrayList;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class LogFileManagerImpl implements LogFileManager
{
	private File logFileBase;
	private RuntimeSupport runtimeSupport;
	private final List<LogFile> files = new ArrayList<LogFile>();
	private final MapList<String, LogFile> logFileClasifiers = new HashMapArrayList<String, LogFile>();

	private final Map<ETLTestClass, List<LogFile>> classLogFileMap = Collections.synchronizedMap(new HashMap<ETLTestClass, List<LogFile>>());
	private final Map<ETLTestMethod, List<LogFile>> methodLogFileMap = Collections.synchronizedMap(new HashMap<ETLTestMethod, List<LogFile>>());
	private final	Map<ETLTestOperation, List<LogFile>> operationLogFileMap = Collections.synchronizedMap(new HashMap<ETLTestOperation, List<LogFile>>());

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport rs)
	{
		runtimeSupport = rs;
		logFileBase = rs.getGeneratedSourceDirectory("log");

		IOUtils.purge(logFileBase, true);
	}

	@Override
	public synchronized List<LogFile> getLogFilesByETLTestOperation(ETLTestOperation operation)
	{
		return operationLogFileMap.get(operation);
	}

	@Override
	public synchronized List<LogFile> getLogFilesByETLTestMethod(ETLTestMethod operation)
	{
		return methodLogFileMap.get(operation);
	}

	@Override
	public synchronized List<LogFile> getLogFilesByETLTestClass(ETLTestClass operation)
	{
		return classLogFileMap.get(operation);
	}

	@Override
	public synchronized void addLogFile(ETLTestMethod mth, ETLTestOperation operation, File log, String classifier) throws IOException
	{
		LogFileImpl
				logFile =
				new LogFileImpl(null,
						classifier,
						cacheLog(log),
						log,
						mth.getTestClass(),
						mth,
						operation);
		files.add(logFile);

		synchronized(logFileClasifiers)
		{
			logFileClasifiers.getOrCreate(classifier).add(logFile);
		}

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(ETLTestOperation operation, File log, String classifier) throws IOException
	{
		LogFileImpl
				logFile =
				new LogFileImpl(null,
						classifier,
						cacheLog(log),
						log,
						operation.getTestClass(),
						operation.getTestMethod(),
						operation);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(ETLTestMethod operation, File log, String classifier) throws IOException
	{
		LogFileImpl
				logFile =
				new LogFileImpl(null, classifier,
						cacheLog(log),
						log,
						operation.getTestClass(),
						operation,
						null);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(ETLTestClass operation, File log, String classifier) throws IOException
	{
		LogFileImpl
				logFile =
				new LogFileImpl(null, classifier, cacheLog(log), log, operation, null, null);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(File log, String classifier) throws IOException
	{
		LogFileImpl logFile = new LogFileImpl(null, classifier, cacheLog(log), log, null, null, null);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(ETLTestMethod mt, ETLTestOperation operation, File log, String classifier, String message) throws IOException
	{
		LogFileImpl
				logFile =
				new LogFileImpl(message, classifier,
						cacheLog(log),
						log,
						mt.getTestClass(),
						mt,
						operation);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(ETLTestOperation operation, File log, String classifier, String message) throws IOException
	{
		LogFileImpl
				logFile =
				new LogFileImpl(message, classifier,
						cacheLog(log),
						log,
						operation.getTestClass(),
						operation.getTestMethod(),
						operation);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(ETLTestMethod operation, File log, String classifier, String message) throws IOException
	{
		LogFileImpl
				logFile =
				new LogFileImpl(message, classifier,
						cacheLog(log),
						log,
						operation.getTestClass(),
						operation,
						null);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(ETLTestClass operation, File log, String classifier, String message) throws IOException
	{
		LogFileImpl
				logFile =
				new LogFileImpl(message, classifier, cacheLog(log), log, operation, null, null);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public synchronized void addLogFile(File log, String classifier, String message) throws IOException
	{
		LogFileImpl logFile = new LogFileImpl(message, classifier, cacheLog(log), log, null, null, null);
		files.add(logFile);

		logFileClasifiers.getOrCreate(classifier).add(logFile);

		catalog(logFile);
	}

	@Override
	public File getLogRoot()
	{
		return logFileBase;
	}

	private void catalog(LogFileImpl logFile)
	{
		if (logFile.getTestClass() != null)
		{
			getList(classLogFileMap, logFile.getTestClass()).add(logFile);
		}
		if (logFile.getTestMethod() != null)
		{
			getList(methodLogFileMap, logFile.getTestMethod()).add(logFile);
		}
		if (logFile.getTestOperation() != null)
		{
			getList(operationLogFileMap, logFile.getTestOperation()).add(logFile);
		}
	}

	private <K> List<LogFile> getList(Map<K, List<LogFile>> operationLogFileMap, K testClass)
	{
		if (!operationLogFileMap.containsKey(testClass))
		{
			operationLogFileMap.put(testClass, new ArrayList<LogFile>());
		}

		return operationLogFileMap.get(testClass);
	}

	private File cacheLog(File log) throws IOException
	{
		File dir = runtimeSupport.getGeneratedSourceDirectory("log");

		FileBuilder ffBuilder = new FileBuilder(dir);

		ETLTestClass _class = runtimeSupport.getCurrentlyProcessingTestClass();

		if (_class != null)
		{
			ffBuilder = ffBuilder.subdir(_class.getName());
		}

		ETLTestMethod method = runtimeSupport.getCurrentlyProcessingTestMethod();

		if (method != null)
		{
			ffBuilder = ffBuilder.subdir(method.getName());
		}

		ETLTestOperation operation = runtimeSupport.getCurrentlyProcessingTestOperation();

		if (operation != null)
		{
			ffBuilder = ffBuilder.subdir(operation.getOrdinal() + "_" + operation.getOperationName());
		}

		File ffCache = ffBuilder.mkdirs().name(log.getName()).file();

		// ensure parent dir exists for target
		FileUtils.forceMkdir(ffCache.getParentFile());

		IOUtils.copyFiles(log, ffCache);

		return ffCache;
	}
}
