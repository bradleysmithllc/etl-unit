package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.sf.json.util.JSONUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.NeedsTest;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

public class ETLTestValueObjectImpl extends ETLTestDebugTraceableImpl implements ETLTestValueObject {
	public static final Object NULL_VALUE = new Object[0];

	private final String strValue;
	private final value_type vt;
	private final List<ETLTestValueObject> listValue;
	private final Map<String, ETLTestValueObject> mapValue;
	private final Object pojoValue;

	public ETLTestValueObjectImpl(Object value) {
		this(value, null);
	}

	public ETLTestValueObjectImpl(Object value, Token t) {
		// lame special case
		if (value == NULL_VALUE) {
			vt = value_type.t_null;

			pojoValue = null;
			listValue = null;
			mapValue = null;
			strValue = null;
		} else {
			pojoValue = value;
			vt = value_type.pojo;
			listValue = null;
			mapValue = null;
			strValue = null;
		}
	}

	public ETLTestValueObjectImpl(String value) {
		this(value, false);
	}

	public ETLTestValueObjectImpl(String value, Token t) {
		this(value, false, t);
	}

	public ETLTestValueObjectImpl(String value, boolean literal) {
		strValue = value;
		listValue = null;
		mapValue = null;
		pojoValue = null;
		vt = literal ? value_type.literal : value_type.quoted_string;
	}

	public ETLTestValueObjectImpl(boolean value, Token t) {
		super(t);
		strValue = Boolean.toString(value);
		listValue = null;
		mapValue = null;
		pojoValue = null;
		vt = value_type.literal;
	}

	public ETLTestValueObjectImpl(String value, boolean literal, Token t) {
		super(t);
		strValue = value;
		listValue = null;
		mapValue = null;
		pojoValue = null;
		vt = literal ? value_type.literal : value_type.quoted_string;
	}

	public ETLTestValueObjectImpl(List<ETLTestValueObject> value) {
		strValue = null;
		listValue = value;
		mapValue = null;
		pojoValue = null;
		vt = value_type.list;
	}

	public ETLTestValueObjectImpl(Map<String, ETLTestValueObject> value) {
		strValue = null;
		listValue = null;
		mapValue = value;
		pojoValue = null;
		vt = value_type.object;
	}

	@Override
	public boolean isNull() {
		return vt == value_type.t_null;
	}

	@NeedsTest
	@Override
	public String getValueAsString() {
		throwNull(strValue);

		return strValue;
	}

	@Override
	public boolean getValueAsBoolean() {
		String val = getValueAsString();
		String valLower = val.toLowerCase();

		if (valLower.equals("true")) {
			return true;
		} else if (valLower.equals("false")) {
			return false;
		}

		throw new IllegalArgumentException("Value cannot be represented as boolean true or false: " + val);
	}

	@NeedsTest
	@Override
	public List<ETLTestValueObject> getValueAsList() {
		throwNull(listValue);

		return listValue;
	}

	@NeedsTest
	@Override
	public List<String> getValueAsListOfStrings() {
		List<String> slist = new ArrayList<String>();

		List<ETLTestValueObject> list = getValueAsList();

		Iterator<ETLTestValueObject> it = list.iterator();

		while (it.hasNext()) {
			ETLTestValueObject next = it.next();
			slist.add(next != null ? next.getValueAsString() : null);
		}

		return slist;
	}

	@NeedsTest
	@Override
	public Map<String, ETLTestValueObject> getValueAsMap() {
		throwNull(mapValue);

		return mapValue;
	}

	@Override
	public Object getValueAsPojo() {
		return pojoValue;
	}

	@Override
	public <T> T getValueAsType() {
		return (T) getValueAsPojo();
	}

	private void throwNull(Object v) {
		if (v == null) {
			throw new UnsupportedOperationException("Value Object cannot be coerced to this type");
		}
	}

	@Override
	public value_type getValueType() {
		return vt;
	}

	@Override
	public ETLTestValueObject queryRequired(String path) {
		ETLTestValueObject res = query(path);

		if (res == null) {
			throw new IllegalArgumentException("Path '" + path + "' not found");
		}

		return res;
	}

	@Override
	public ETLTestValueObject query(String path) {
		return queryImpl(path);
	}

	@NeedsTest
	private ETLTestValueObject queryImpl(String path) {
		ETLTestValueObject val = this;

		StringTokenizer st = new StringTokenizer(path, ".");

		while (st.hasMoreTokens()) {
			String token = st.nextToken();

			switch (val.getValueType()) {
				case literal:
				case list:
				case quoted_string:
					throw new UnsupportedOperationException("Path queries are only valid on object types");
				case object:
					Map<String, ETLTestValueObject> map = val.getValueAsMap();

					if (!map.containsKey(token)) {
						return null;
					}

					val = map.get(token);
					break;
				case pojo:
					try {
						BeanInfo info = Introspector.getBeanInfo(pojoValue.getClass());
						for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
							if (!pd.getName().equals(token)) {
								continue;
							}

							Method m = pd.getReadMethod();
							if (m != null) {
								Object invoke = m.invoke(pojoValue);

								if (invoke instanceof String) {
									val = new ETLTestValueObjectImpl((String) invoke);
								} else {
									val = new ETLTestValueObjectImpl(invoke);
								}
							}
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
					break;
			}
		}

		return val;
	}

	@Override
	public ETLTestValueObject merge(ETLTestValueObject obj) {
		return merge(obj, merge_type.left_merge, merge_policy.recursive);
	}

	private static ETLTestValueObject applyMergePolicy(ETLTestValueObject lobj, ETLTestValueObject robj, merge_type type, merge_policy policy) {
		// if either side is null, handle that right up front
		// if objects are different types, let the merge direction determine what to do
		if (
				lobj.getValueType() == value_type.t_null ||
						robj.getValueType() == value_type.t_null ||
						policy == merge_policy.nonrecursive ||
						(lobj.getValueType() != value_type.object && robj.getValueType() != value_type.object)
				) {
			switch (type) {
				case left_merge:
					return lobj;
				case right_merge:
					return robj;
			}
		}

		// recursive merge.  Defer
		return lobj.merge(robj, type, policy);
	}

	@Override
	public ETLTestValueObject merge(ETLTestValueObject obj, merge_type type, merge_policy policy) {
		// the rest is just for objects
		if (vt != value_type.object || obj.getValueType() != value_type.object) {
			throw new UnsupportedOperationException("Merge operations only supported on object types");
		}

		ETLTestValueObjectImpl impl = new ETLTestValueObjectImpl(new HashMap<String, ETLTestValueObject>(mapValue));
		Map<String, ETLTestValueObject> valueOther = obj.getValueAsMap();

		// create a unified key set
		Set<String> setCombined = new TreeSet<String>(getValueAsMap().keySet());
		setCombined.addAll(obj.getValueAsMap().keySet());

		// iterate through the keys, and handle l/r insert / merge as required
		for (String s : setCombined) {
			// check the lvalue (me)
			boolean lvalueExists = mapValue.containsKey(s);
			boolean rvalueExists = valueOther.containsKey(s);

			ETLTestValueObject lvalue = mapValue.get(s);
			ETLTestValueObject rvalue = valueOther.get(s);

			if (!lvalueExists && !rvalueExists) {
				throw new IllegalStateException("What??!?");
			} else if (!lvalueExists) {
				// insert into impl
				impl.mapValue.put(s, rvalue);
			} else if (!rvalueExists) {
				// impl already has
			} else {
				// do a recursive merge (Ryan loves this!)
				// revisit this as it may cause unnecessary object copies
				impl.mapValue.put(s, applyMergePolicy(lvalue, rvalue, type, policy));
			}
		}

		return impl;
	}

	@Override
	public ETLTestValueObject copy() {
		switch (vt) {
			case quoted_string:
				return new ETLTestValueObjectImpl(strValue, false);
			case object:
				return new ETLTestValueObjectImpl(deepCopy(mapValue));
			case list:
				return new ETLTestValueObjectImpl(deepCopy(listValue));
			case literal:
				return new ETLTestValueObjectImpl(strValue, true);
			case pojo:
				return new ETLTestValueObjectImpl(pojoValue);
			case t_null:
				return new ETLTestValueObjectImpl(NULL_VALUE);
			default:
				throw new Error("JVM fault");
		}
	}

	private Map<String, ETLTestValueObject> deepCopy(Map<String, ETLTestValueObject> mapValue) {
		Map<String, ETLTestValueObject> map = new HashMap<String, ETLTestValueObject>();

		for (Map.Entry<String, ETLTestValueObject> entry : mapValue.entrySet()) {
			map.put(entry.getKey(), entry.getValue().copy());
		}

		return map;
	}

	private List<ETLTestValueObject> deepCopy(List<ETLTestValueObject> listValue) {
		List<ETLTestValueObject> list = new ArrayList<ETLTestValueObject>();

		for (ETLTestValueObject value : listValue) {
			list.add(value.copy());
		}

		return list;
	}

	@Override
	public JsonNode getJsonNode() {
		ObjectMapper mapper = new ObjectMapper();

		try {
			switch (getValueType()) {
				case t_null:
					return mapper.readValue(JSONUtils.valueToString("null"), JsonNode.class);
				case quoted_string:
					return mapper.readValue(JSONUtils.valueToString(strValue), JsonNode.class);
				case object: {
					ObjectNode node = mapper.createObjectNode();

					Map<String, ETLTestValueObject> map = getValueAsMap();

					for (Map.Entry<String, ETLTestValueObject> enode : map.entrySet()) {
						String key = enode.getKey();
						ETLTestValueObject value = enode.getValue();

						if (value == null) {
							node.putNull(key);
						} else {
							node.put(key, value.getJsonNode());
						}
					}

					return node;
				}
				case list: {
					ArrayNode node = mapper.createArrayNode();

					List<ETLTestValueObject> list = getValueAsList();

					for (ETLTestValueObject enode : list) {
						node.add(enode != null ? enode.getJsonNode() : null);
					}

					return node;
				}
				case literal:
					return mapper.readValue(strValue, JsonNode.class);
				case pojo:
					//Gson gson = new GsonBuilder().create();
					//String js = gson.toJson(pojoValue);

					// Jackson has a pretty stupid API
					return mapper.readValue("\"" + pojoValue.toString() + "\"", JsonNode.class);
				default:
					throw new IllegalArgumentException("JVM Fault");
			}
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public String toFormattedString()
	{
		return toFormattedStringImpl("");
	}

	private String toFormattedStringImpl(String padding)
	{
		switch (getValueType())
		{
			case quoted_string:
				return "'" + getValueAsString() + "'";
			case object:
				return "object";
			case list:
				StringBuilder stb = new StringBuilder("[\n");

				int ordinal = 0;
				for (ETLTestValueObject value : getValueAsList())
				{
					if (ordinal != 0)
					{
						stb.append(",\n");
					}

					ordinal++;
					stb.append(padding);
					stb.append(((ETLTestValueObjectImpl) value).toFormattedStringImpl(padding));
				}

				// final block
				stb.append(padding);
				stb.append("]");
			case literal:
				return getValueAsString();
			case pojo:
				return getValueAsPojo().toString();
			case t_null:
				return "null";
			default:
				throw new IllegalArgumentException("JVM Fault");
		}
	}
}
