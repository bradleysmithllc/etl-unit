package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import utsupport.create_test_cases;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ConvertV1TestToV2
{

	public static void main(String[] argv) throws Exception
	{
		File rootDir = new File("D:\\trunk\\UnitTest\\");

		File descDir = new File(rootDir, "test_descriptors");
		File testDir = new File(rootDir, "src\\test");

		File[] files = descDir.listFiles();

		if (files != null)
		{
			for (int i = 0; i < files.length; i++)
			{
				String file = files[i].getName();

				if (file.endsWith("TRUNCATE_test.txt"))
				{
					String testName = file.substring(0, file.length() - 9);
					convert(files[i], new File(testDir, testName.toLowerCase() + ".infaunit"), testName.toLowerCase());
				}
			}
		}
	}

	private static void convert(File file, File file0, String testName) throws IOException
	{
		// create new test with the same name
		Map<String, List<String[]>> m = create_test_cases.readSections(file);

		String description = "Description lacking";

		List<String[]> desc = m.get("Description");

		if (desc != null)
		{
			description = rehydrate(desc);
		}

		StringBuffer builder = new StringBuffer("[\r\n" + description + "\r\n]\r\n");

		List<String[]> s = m.get("Suite");

		List<String> slist = flattenToList(s);

		for (int i = 0; i < slist.size(); i++)
		{
			builder.append("@JoinSuite(" + slist.get(i) + ")\r\n");
		}

		builder.append("test " + testName + " {\r\n");

		builder.append("\t@Test\r\n\tvoid run_test()\r\n\t{\r\n");

		s = m.get("SrcFiles");

		slist = flattenToList(s);

		builder.append("\t\t//Load src files, if any\r\n");
		for (int i = 0; i < slist.size(); i++)
		{
			builder.append("\t\tloadSrcFile(\"" + slist.get(i) + "\");\r\n");
		}

		builder.append("\r\n\t\t//Load src data, if any\r\n");

		s = m.get("Data");

		if (s != null)
		{
			for (int i = 0; i < s.size(); i++)
			{
				String[] sarr = s.get(i);

				String dest = sarr[0];
				String targetOperation = "Src";

				if (sarr.length == 3 && sarr[2].equals("TST_TGT"))
				{
					targetOperation = "Tgt";
				}

				builder.append("\t\tcopy" + targetOperation + "Data(\"" + sarr[1] + "\", \"" + dest + "\");\r\n");

				if (targetOperation.equals("Tgt"))
				{
					builder.append("\t\tassertTableNotEmpty(\"" + dest + "\");\r\n");
				}
			}
		}

		s = m.get("PreSql");

		if (s != null)
		{
			builder.append("\t\texecuteSql(\r\n");

			builder.append("\t\t\t\"");

			for (int i = 0; i < s.size(); i++)
			{
				String[] sarr = s.get(i);

				if (i != 0)
				{
					builder.append(' ');
				}

				for (int j = 0; j < sarr.length; j++)
				{
					if (j != 0)
					{
						builder.append(' ');
					}

					builder.append(sarr[j].replace("\t", ""));
				}
			}

			builder.append("\"");

			builder.append("\r\n\t\t);\r\n");
		}

		builder.append("\r\n\t\t//Run workflow\r\n");
		s = m.get("Workflow");

		if (s != null)
		{
			for (int i = 0; i < s.size(); i++)
			{
				String[] sarr = s.get(i);

				String folder = sarr[1];

				if (folder.equals("Subset"))
				{
					folder = "EDW_SUBSET";
				}

				builder.append("\t\texecuteWorkflow(\"" + sarr[0] + "\", \"" + folder + "\");\r\n");
			}
		}

		builder.append("\r\n\t\t//asssertions\r\n");

		s = m.get("Assert");

		if (s != null)
		{
			for (int i = 0; i < s.size(); i++)
			{
				String[] sarr = s.get(i);

				if (sarr[0].equals("TBL"))
				{
					builder.append("\t\tassertTableNotEmpty(\"" + sarr[1] + "\");\r\n");
					builder.append("\t\tassertTableEquals(\"" + sarr[2] + "\", \"" + sarr[1] + "\");\r\n");
				}
				else if (sarr[0].equals("FILE"))
				{
					builder.append("\t\tassertFileEquals(\"" + sarr[2] + "\", \"" + sarr[1] + "\");\r\n");
				}
				else
				{
					builder.append("\t\tfailFast(\"Missing assertion\");\r\n");
				}
			}
		}

		builder.append("\t}\r\n");

		builder.append("}");

		IOUtils.writeBufferToFile(file0, builder);
	}

	private static String rehydrate(List<String[]> desc)
	{
		StringBuilder builder = new StringBuilder();

		for (int j = 0; j < desc.size(); j++)
		{
			String[] sarr = desc.get(j);

			if (j != 0)
			{
				builder.append("\r\n\t");
			}
			else
			{
				builder.append("\t");
			}

			for (int i = 0; i < sarr.length; i++)
			{
				if (i != 0)
				{
					builder.append(' ');
				}

				builder.append(sarr[i]);
			}
		}

		return builder.toString();
	}

	private static List<String> flattenToList(List<String[]> s)
	{
		List<String> l = new ArrayList<String>();

		if (s != null)
		{
			for (int i = 0; i < s.size(); i++)
			{
				l.addAll(Arrays.asList(s.get(i)));
			}
		}

		return l;
	}
}
