package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import javax.inject.Inject;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HtmlDiffManagerImpl implements DiffManagerImpl
{
	File reportsDir;
	private LogFileManager logFileManager;

	@Inject
	public void receiveLogFileManager(LogFileManager logFileManager)
	{
		this.logFileManager = logFileManager;
	}

	public class MethodDiff
	{
		private final ETLTestMethod method;
		private final List<OperationDiffGrid> diffList = new ArrayList<OperationDiffGrid>();

		public MethodDiff(ETLTestMethod method)
		{
			this.method = method;
		}

		public ETLTestMethod getMethod()
		{
			return method;
		}

		public List<OperationDiffGrid> getDiffList()
		{
			return diffList;
		}
	}

	public class OperationDiffGrid
	{
		private final ETLTestOperation operation;
		private final HtmlDiffGridImpl diffGrid;
		private final String failureId;
		private final String expectedId;
		private final String actualId;

		public OperationDiffGrid(ETLTestOperation method, HtmlDiffGridImpl list, String failureId, String expectedId, String actualId)
		{
			this.operation = method;
			diffGrid = list;
			this.failureId = failureId;
			this.expectedId = expectedId;
			this.actualId = actualId;
		}

		public ETLTestOperation getOperation()
		{
			return operation;
		}

		public String getFailureId()
		{
			return failureId;
		}

		public HtmlDiffGridImpl getDiffGrid()
		{
			return diffGrid;
		}

		public String getExpectedId() {
			return expectedId;
		}

		public String getActualId() {
			return actualId;
		}
	}

	private final Map<ETLTestMethod, MethodDiff> diffMap = new HashMap<ETLTestMethod, MethodDiff>();

	@Override
	public void setOutputDirectory(File file)
	{
		reportsDir = file;
	}

	@Override
	public void dispose()
	{
		// purge reports dir before proceeding
		IOUtils.purge(reportsDir);

		try
		{
			// grab the velocity template for the report
			URL url = getClass().getClassLoader().getResource("htmlDiffReport.vm");

			String template = IOUtils.readURLToString(url);

			// for each method, prepare a diff html file and save in the reports folder
			for (Map.Entry<ETLTestMethod, MethodDiff> entry : diffMap.entrySet())
			{
				ETLTestMethod method = entry.getKey();

				// open the report file
				File target = getFile(method);

				// process the template with the grid list
				String reportText = VelocityUtil.writeTemplate(template, entry.getValue());

				// persist the report
				IOUtils.writeBufferToFile(target, new StringBuffer(reportText));
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("Error writing diff report files", e);
		}
	}

	private File getFile(ETLTestMethod method) {
		return new File(reportsDir, method.getQualifiedName() + "_diff_report.html");
	}

	@Override
	public DiffGrid reportDiff(ETLTestMethod context, ETLTestOperation operation, String failureId, String expectedId, String actualId)
	{
		HtmlDiffGridImpl htmlDiffGrid = new HtmlDiffGridImpl(context);

		MethodDiff methodDiff = diffMap.get(context);

		if (methodDiff == null)
		{
			methodDiff = new MethodDiff(context);
			diffMap.put(context, methodDiff);
		}

		OperationDiffGrid odg = new OperationDiffGrid(operation, htmlDiffGrid, failureId, expectedId, actualId);

		methodDiff.diffList.add(odg);

		return htmlDiffGrid;
	}

	@Override
	public DataSetGrid reportDataSet(ETLTestOperation method, List<String> columns, String failureId)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public File findDiffReport(ETLTestMethod method) {
		File file = getFile(method);

		if (file.exists())
		{
			return file;
		}

		return null;
	}

	public final class HtmlDiffGridImpl implements DiffGrid
	{
		private final ETLTestMethod method;
		private final List<HtmlDiffGridRowImpl> rows = new ArrayList<HtmlDiffGridRowImpl>();

		public HtmlDiffGridImpl(ETLTestMethod method)
		{
			this.method = method;
		}

		@Override
		public DiffGridRow addRow(int sourceLine, int targetLine, line_type type)
		{
			HtmlDiffGridRowImpl gridRow = new HtmlDiffGridRowImpl(sourceLine, targetLine, type);
			rows.add(gridRow);

			return gridRow;
		}

		public List<HtmlDiffGridRowImpl> getRows()
		{
			return rows;
		}

		@Override
		public void done()
		{
		}
	}

	public final class HtmlDiffGridRowImpl implements DiffGridRow
	{
		private final int sourceRecordNum;
		private final int targetRecordNum;

		private final DiffGrid.line_type lineType;

		private String columnName;
		private String sourceValue;
		private String targetValue;
		private String orderKey;

		public HtmlDiffGridRowImpl(int sourceRecordNum, int targetRecordNum, DiffGrid.line_type lineType)
		{
			this.sourceRecordNum = sourceRecordNum;
			this.targetRecordNum = targetRecordNum;
			this.lineType = lineType;
		}

		@Override
		public void setColumnName(String col)
		{
			columnName = col;
		}

		@Override
		public void setOrderKey(String key)
		{
			orderKey = key;
		}

		@Override
		public void setSourceValue(String value)
		{
			sourceValue = value;
		}

		@Override
		public void setTargetValue(String value)
		{
			targetValue = value;
		}

		public int getSourceRecordNum()
		{
			return sourceRecordNum;
		}

		public int getTargetRecordNum()
		{
			return targetRecordNum;
		}

		public DiffGrid.line_type getLineType()
		{
			return lineType;
		}

		public String getOrderKey()
		{
			return orderKey;
		}

		public String getOrderKeyEncoded()
		{
			return EtlUnitStringUtils.encodeNonPrintable(getOrderKey());
		}

		public String getColumnName()
		{
			return columnName;
		}

		public String getSourceValue()
		{
			return sourceValue;
		}

		public String getTargetValue()
		{
			return targetValue;
		}

		public String getSourceValueEncoded()
		{
			return EtlUnitStringUtils.encodeNonPrintable(getSourceValue());
		}

		public String getTargetValueEncoded()
		{
			return EtlUnitStringUtils.encodeNonPrintable(getTargetValue());
		}

		@Override
		public void done()
		{
		}
	}
}
