package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.google.inject.Module;
import com.google.inject.*;
import com.google.inject.binder.LinkedBindingBuilder;
import com.google.inject.name.Names;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.debug.DebugFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LoggingFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.report.BetterReportFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.results.ResultsFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.tags.TagsFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.test_locator.DirectoryTestLocatorFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.workspace.WorkspaceFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassBroadcasterImpl;
import org.bitbucket.bradleysmithllc.etlunit.logging.LoggingSetup;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;
import org.bitbucket.bradleysmithllc.etlunit.metadata.impl.MetaDataManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.HashMapArrayList;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonProcessingUtil;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ETLTestVM
{
	private final FeatureLocator featureLocator;
	private final Configuration configuration;
	private final RuntimeSupport runtimeSupport;
	private final MapList<Feature, RuntimeOption> featureOptionMap = new HashMapArrayList<Feature, RuntimeOption>();

	private Log applicationLog;
	private PrintWriterLog userLog = new PrintWriterLog();

	private final Map<String, Feature> discoveredFeaturesMap = new HashMap<String, Feature>();
	private final Map<String, Feature> installedFeatureMap = new HashMap<String, Feature>();
	private final Map<Class, List<Feature>> installedFeatureTypeMap = new HashMap<Class, List<Feature>>();
	private final List<Feature> features = new ArrayList<Feature>();
	private final ResultsFeatureModule resultsFeatureModule;
	private final DiffManagerImpl diffReporter = new HtmlDiffManagerImpl();
	private final VariableContext variableContext = new VariableContextImpl();
	private final DataFileManager dataFileManager;
	private final ReferenceFileTypeManager referenceFileTypeManager;
	private final TagsFeatureModule tagsFeatureModule;
	private final LogFileManager logFileManager = new LogFileManagerImpl();

	private final MetaDataManager metaDataManager = new MetaDataManagerImpl();

	private final MapLocal mapLocal = new MapLocal();

	private class ETLModule implements Module
	{
		@Override
		public void configure(Binder binder)
		{
			binder.bind(LogFileManager.class).toInstance(logFileManager);
			binder.bind(Configuration.class).toInstance(configuration);
			binder.bind(new TypeLiteral<MapList<Feature, RuntimeOption>>()
			{
			}).toInstance(featureOptionMap);
			binder.bind(RuntimeSupport.class).toInstance(runtimeSupport);
			binder.bind(Log.class).annotatedWith(Names.named("applicationLog")).toInstance(applicationLog);
			binder.bind(Log.class).annotatedWith(Names.named("userLog")).toInstance(userLog);
			binder.bind(DiffManager.class).toInstance(diffReporter);
			binder.bind(VariableContext.class).toInstance(variableContext);
			binder.bind(DataFileManager.class).toInstance(dataFileManager);
			binder.bind(MetaDataManager.class).toInstance(metaDataManager);
			binder.bind(ReferenceFileTypeManager.class).toInstance(referenceFileTypeManager);

			binder.bind(List.class).toInstance(Collections.unmodifiableList(features));

			List<RuntimeOption> options = runtimeSupport.getRuntimeOptions();

			for (RuntimeOption option : options)
			{
				String val = "";

				switch (option.getType()) {
					case bool:
						val = String.valueOf(option.isEnabled());
						break;
					case integer:
						val = String.valueOf(option.getIntegerValue());
						break;
					case string:
						val = option.getStringValue();
						break;
				}

				applicationLog.info("Binding option [" + option.getName() + "] to value [" + val + "]");
				binder.bind(RuntimeOption.class).annotatedWith(Names.named(option.getName())).toInstance(option);
			}
		}
	}

	private ETLModule guiceModule;// = new ETLModule();
	private Injector injector;

	public ETLTestVM(String con)
	{
		this(
				new ServiceLocatorFeatureLocator(),
				new Configuration(con)
		);
	}

	public ETLTestVM(Configuration con)
	{
		this(
				new ServiceLocatorFeatureLocator(),
				con
		);
	}

	public ETLTestVM(FeatureLocator locator, Configuration configuration)
	{
		List<Feature> lfi = new ArrayList<Feature>();

		this.configuration = configuration;

		BasicRuntimeSupport basicRuntimeSupport = new BasicRuntimeSupport(configuration, this);
		basicRuntimeSupport.setMapLocal(mapLocal);

		ETLTestValueObject exeCount = configuration.query("options.etlunit.executorsPerCore.integer-value");

		if (exeCount != null && !exeCount.isNull())
		{
			basicRuntimeSupport.setExecutorCount(Integer.valueOf(exeCount.getValueAsString()) * Runtime.getRuntime().availableProcessors());
		}
		else
		{
			exeCount = configuration.query("options.etlunit.executorCount.integer-value");

			if (exeCount != null && !exeCount.isNull())
			{
				basicRuntimeSupport.setExecutorCount(Integer.valueOf(exeCount.getValueAsString()));
			}
		}

		runtimeSupport = basicRuntimeSupport;
		dataFileManager = new DataFileManagerImpl(
			new FileBuilder(runtimeSupport.getTempDirectory()).subdir("h2").mkdirs().clearDir().file()
		);

		runtimeSupport.setTestStartTime();

		referenceFileTypeManager = new ReferenceFileTypeManagerImpl();

		File outputDirectory = new FileBuilder(runtimeSupport.getReportDirectory("diff")).mkdirs().file();
		diffReporter.setOutputDirectory(outputDirectory);

		LoggingFeatureModule loggingFeatureModule = new LoggingFeatureModule(runtimeSupport, features);

		applicationLog = loggingFeatureModule.getLog();

		applicationLog.info("Project ["
				+ runtimeSupport.getProjectName()
				+ "] version ["
				+ runtimeSupport.getProjectVersion()
				+ "] running for user ["
				+ runtimeSupport.getProjectUser()
				+ "] using project identifier ["
				+ runtimeSupport.getProjectUID()
				+ "]");

		// install a feature which will validate all annotations
		addFeature(new AnnotationValidationFeature());

		addFeature(loggingFeatureModule);
		addFeature(new DebugFeatureModule());
		addFeature(new BetterReportFeatureModule());
		addFeature(new TestFeatureModule());
		addFeature(new DirectoryTestLocatorFeatureModule());

		// add a feature module managing a local workspace
		addFeature(new WorkspaceFeatureModule());

		// add a feature to handle contexts
		addFeature(new ContextFeatureModule());

		resultsFeatureModule = new ResultsFeatureModule();
		addFeature(resultsFeatureModule);

		tagsFeatureModule = new TagsFeatureModule();
		addFeature(tagsFeatureModule);

		this.featureLocator = locator;

		for (Feature feat : featureLocator.getFeatures())
		{
			discoveredFeaturesMap.put(feat.getFeatureName(), feat);
		}

		ETLTestValueObject res = configuration.query("install-features");

		if (res != null)
		{
			if (res.getValueType() != ETLTestValueObject.value_type.list)
			{
				throw new IllegalArgumentException(
						"install-features must be a list of feature class names (not a map or single string)");
			}

			List<ETLTestValueObject> flist = res.getValueAsList();

			Iterator<ETLTestValueObject> fit = flist.iterator();

			while (fit.hasNext())
			{
				ETLTestValueObject fclass = fit.next();

				if (fclass.getValueType() != ETLTestValueObject.value_type.quoted_string)
				{
					throw new IllegalArgumentException(
							"install-features must be a list of feature class names (not a map or single string).  List element is not a string");
				}

				String name = fclass.getValueAsString();

				addFeatureById(name);
			}
		}
	}

	public void installFeatures()
	{
		workOutPrerequisites();

		sortByDependency(features);

		guiceModule = new ETLModule();

		injector = Guice.createInjector(guiceModule);

		injector.injectMembers(this);

		injector.injectMembers(runtimeSupport);
		injector.injectMembers(configuration);
		injector.injectMembers(featureLocator);

		// validate runtime options in the configuration
		List<RuntimeOption> options = runtimeSupport.getRuntimeOptions();

		for (RuntimeOption option : options)
		{
			String name = option.getName();

			// grab the feature.optionName parts
			int index = name.indexOf(".");

			String feature = name.substring(0, index);
			String foption = name.substring(index + 1);

			Feature f = installedFeatureMap.get(feature);

			if (f == null)
			{
				throw new IllegalArgumentException("Feature not installed for option: " + name);
			}

			List<RuntimeOptionDescriptor> optList = f.getMetaInfo().getOptions();

			boolean found = false;

			for (RuntimeOptionDescriptor ro : optList)
			{
				if (ro.getName().equals(foption))
				{
					found = true;
				}
			}

			if (!found)
			{
				throw new IllegalArgumentException("Feature does not export option: " + name);
			}
		}

		// inject all runtime options that each feature exposes and the system options
		injector = injector.createChildInjector(new Module()
		{
			@Override
			public void configure(Binder binder)
			{
				Map<String, RuntimeOption> runtimeOptionMap = new HashMap<String, RuntimeOption>();

				for (RuntimeOption option : runtimeSupport.getRuntimeOptions())
				{
					runtimeOptionMap.put(option.getName(), option);
				}

				for (Feature feature : features)
				{
					FeatureMetaInfo metaInfo = feature.getMetaInfo();
					if (metaInfo != null)
					{
						List<RuntimeOptionDescriptor> options1 = metaInfo.getOptions();

						if (options1 != null)
						{
							for (RuntimeOptionDescriptor featureOption : options1)
							{
								String name = feature.getFeatureName() + "." + featureOption.getName();
								if (!runtimeOptionMap.containsKey(name))
								{
									RuntimeOption ro = new RuntimeOption(name);
									ro.setFeature(feature);
									ro.setDescriptor(featureOption);
									switch (featureOption.getOptionType())
									{
										case bool:
											ro.setEnabled(featureOption.getDefaultBooleanValue());
											break;
										case integer:
											ro.setIntegerValue(featureOption.getDefaultIntegerValue());
											break;
										case string:
											ro.setStringValue(featureOption.getDefaultStringValue());
											break;
									}

									// store reference in the map
									featureOptionMap.getOrCreate(feature).add(ro);

									binder.bind(RuntimeOption.class).annotatedWith(Names.named(ro.getName())).toInstance(ro);
								}
								else
								{
									RuntimeOption runtimeOption = runtimeOptionMap.get(name);

									runtimeOption.setFeature(feature);
									runtimeOption.setDescriptor(featureOption);
									featureOptionMap.getOrCreate(feature).add(runtimeOption);
								}
							}
						}
					}
				}
			}
		});

		Iterator<Feature> i = features.iterator();

		while (i.hasNext())
		{
			final Feature f = i.next();

			applicationLog.info("Initializing feature: " + f.getFeatureName());

			injector = injector.createChildInjector(new Module()
			{
				@Override
				public void configure(Binder binder)
				{
					// cast the builder to remove the type requirement.  Otherwise all types have to be
					// bound by name
					// check to see if more than one of the same class has been installed.
					if (installedFeatureTypeMap.get(f.getClass()).size() != 1)
					{
						binder.bind(Feature.class).annotatedWith(Names.named(f.getFeatureName())).toInstance(f);
					}
					else
					{
						LinkedBindingBuilder linker = binder.bind(f.getClass());
						linker.toInstance(f);
					}

					// reflectively look for a configuration class and inject
					String configurationClassname = f.getClass().getPackage().getName() + ".json." + f.getClass().getSimpleName() + "Configuration";

					ETLTestValueObject query = configuration.query("features." + f.getFeatureName());

					if (query != null)
					{
						// optionally validate
						FeatureMetaInfo meta = f.getMetaInfo();

						JsonSchema featureConfigurationValidator = meta.getFeatureConfigurationValidator();

						if (featureConfigurationValidator != null)
						{
							try
							{
								ProcessingReport report = featureConfigurationValidator.validate(query.getJsonNode());

								if (!report.isSuccess())
								{
									throw new IllegalArgumentException("Invalid configuration for feature ["
											+ f.getFeatureName()
											+ "]: "
											+ JsonProcessingUtil.getProcessingReport(report));
								}
							}
							catch (ProcessingException e)
							{
								throw new IllegalArgumentException("Invalid configuration for feature ["
										+ f.getFeatureName()
										+ "]: "
										+ e.getProcessingMessage().asJson(), e);
							}
						}

						try
						{
							Class conClass = Thread.currentThread().getContextClassLoader().loadClass(configurationClassname);

							String json = query.getJsonNode().toString();
							ObjectMapper om = new ObjectMapper();
							Object confObj = om.readValue(json, conClass);

							// check to see if more than one of the same class has been installed.
							LinkedBindingBuilder linker = binder.bind(conClass);
							linker.toInstance(confObj);

							applicationLog.info("Configuration class " + conClass + " bound to the session");
						}
						catch (ClassNotFoundException e)
						{
							applicationLog.info("Feature " + f.getFeatureName() + " does not expose a configuration class - should be '" + configurationClassname + "'");
						} catch (IOException e) {
							throw new RuntimeException("Feature " + f.getFeatureName() + " could not deserialize configuration class", e);
						}

						f.setFeatureConfiguration(query);
					}
				}
			});

			//injector.injectMembers(f);
			Injector injectorSub = f.preCreate(injector);

			if (injectorSub != null)
			{
				injector = injectorSub;
			}

			f.initialize(injector);
		}

		//check for a process executor implementation
		ETLTestValueObject procex = configuration.query("runtimeSupport.processExecutor");

		if (procex != null)
		{
			applicationLog.info("User specified process executor: " + procex.getValueAsString());

			String name = procex.getValueAsString();

			try
			{
				Class impl = Class.forName(name);

				if (!ProcessExecutor.class.isAssignableFrom(impl))
				{
					throw new IllegalArgumentException("Class named '"
							+ impl
							+ "' does not implement the ProcessExecutor interface");
				}

				runtimeSupport.installProcessExecutor((ProcessExecutor) postCreate(impl.newInstance()));

				applicationLog.info("User specified process executor: '" + procex.getValueAsString() + "' installed");
			}
			catch (ClassNotFoundException e)
			{
				throw new IllegalArgumentException("Class named '" + name + "' not found", e);
			}
			catch (InstantiationException | IllegalAccessException e)
			{
				throw new IllegalArgumentException("Class named '" + name + "' could not be created", e);
			}
		}
	}

	public List<Feature> getFeatures()
	{
		return features;
	}

	private void workOutPrerequisites()
	{
		// install any missing features
		boolean installed = false;

		for (Feature feat : new ArrayList<Feature>(features))
		{
			for (String prereq : feat.getPrerequisites())
			{
				// check if it is already installed
				if (!installedFeatureMap.containsKey(prereq))
				{
					// it hasn't been installed yet, determine if it is a known feature
					Feature feature = discoveredFeaturesMap.get(prereq);

					if (feature == null)
					{
						throw new IllegalArgumentException("Feature named '" + prereq + "' not found");
					}

					addFeature(feature);
					installed = true;
				}
			}
		}

		// recurse if any features were installed
		if (installed)
		{
			workOutPrerequisites();
		}
	}

	public void addFeatureById(String featureId)
	{
		applicationLog.info("Installing feature id " + featureId);
		Feature feature = discoveredFeaturesMap.get(featureId);

		if (feature == null)
		{
			throw new IllegalArgumentException("Feature id '" + featureId + "' not discovered");
		}

		addFeature(feature);
	}

	public void addFeature(Feature feat)
	{
		if (guiceModule != null)
		{
			throw new IllegalStateException("Features may not be added after installFeatures is called");
		}

		applicationLog.info("Installing feature " + feat.getFeatureName());

		if (feat.getPrerequisites().contains(feat.getFeatureName()))
		{
			throw new IllegalArgumentException("A feature may not depend on itself: " + feat.getFeatureName());
		}

		features.add(feat);
		installedFeatureMap.put(feat.getFeatureName(), feat);

		Class<? extends Feature> aClass = feat.getClass();
		if (!installedFeatureTypeMap.containsKey(aClass))
		{
			installedFeatureTypeMap.put(aClass, new ArrayList<Feature>());
		}

		installedFeatureTypeMap.get(aClass).add(feat);
	}

	public void addFeature(Class cl) throws InstantiationException, IllegalAccessException
	{
		if (!Feature.class.isAssignableFrom(cl))
		{
			applicationLog.severe("Bad feature added.  Does not implement the FeatureModule interface: " + cl);
		}
		else
		{
			addFeature((Feature) cl.newInstance());
		}
	}

	public RuntimeSupport getRuntimeSupport()
	{
		return runtimeSupport;
	}

	public TestResults runTests()
	{
		LoggingSetup.setupLoggingToFile(new File(runtimeSupport.getGeneratedSourceDirectory("vm"), "log4j." + DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-SSS").format(LocalDateTime.now()) + ".log"));
		applicationLog.info("Beginning run: " + runtimeSupport.getRunIdentifier());

		ClassBroadcasterImpl cbi = new ClassBroadcasterImpl(
				postCreate(new FeatureClassLocatorProxy(features)),
				postCreate(new FeatureDirectorProxy(features)),
				postCreate(new FeatureListenerProxy(features)),
				mapLocal,
				applicationLog,
				variableContext,
				runtimeSupport.getExecutorCount(),
				runtimeSupport.getTestStartTime()
		);

		// add the implicit variables to the context
		variableContext.declareAndSetStringValue("projectName", runtimeSupport.getProjectName());
		variableContext.declareAndSetStringValue("projectUser", runtimeSupport.getProjectUser());
		variableContext.declareAndSetStringValue("projectUID", runtimeSupport.getProjectUID());

		ETLTestCoordinator coordinator = new ETLTestCoordinator(
				cbi,
				postCreate(new FeatureStatusReporterProxy(features))
		);

		/* This is the test cycle - it all happens on this call
		*/
		coordinator.beginTesting();

		/*Begin cleanup and disposal
		 *
		 */
		// dispose the diff manager first so reports are available during feature disposal
		diffReporter.dispose();

		for (Feature feature : features)
		{
			try
			{
				applicationLog.info("Disposing feature: " + feature.getFeatureName());
				feature.dispose();
			}
			catch (Exception exc)
			{
				applicationLog.severe("Error disposing feature", exc);
			}
		}

		// dispose metadata and data file managers
		metaDataManager.dispose();
		dataFileManager.dispose();

		return resultsFeatureModule.getTestClassResults();
	}

	public static void sortByDependency(List<Feature> features)
	{
		// create a map of features
		Map<String, Feature> fmap = new HashMap<String, Feature>();

		Iterator<Feature> fit = features.iterator();

		while (fit.hasNext())
		{
			Feature f = fit.next();

			if (fmap.containsKey(f.getFeatureName()))
			{
				System.out
						.println("Feature " + f.getFeatureName() + " already installed as " + fmap.get(f.getFeatureName())
								.getClass() + ", ignoring duplicate " + f.getClass());
			}
			else
			{
				fmap.put(f.getFeatureName(), f);
			}
		}

		// iterate through list and check for prerequisites and circular dependencies
		fit = features.iterator();

		while (fit.hasNext())
		{
			Feature f = fit.next();

			List<String> preq = f.getPrerequisites();

			Iterator<String> prit = preq.iterator();

			while (prit.hasNext())
			{
				String pr = prit.next();

				Feature prf = fmap.get(pr);

				if (prf == null)
				{
					throw new IllegalArgumentException("Missing prerequisite feature: " + pr);
				}

				if (prf.getPrerequisites().contains(f.getFeatureName()))
				{
					throw new IllegalStateException("Circular dependencies: " + pr + " <<>> " + f.getFeatureName());
				}
			}
		}

		final Map<String, Long> priorities = new HashMap<String, Long>();

		// on the first pass, assign each feature its' 'suggested' priority
		for (Feature feature : features)
		{
			priorities.put(feature.getFeatureName(), new Long(feature.getPriorityLevel()));
		}

		// go through the list, and for each dependency, assign this feature 1 higher than it's highest priority
		boolean adjusted = true;
		while (adjusted)
		{
			adjusted = false;

			for (Feature feature : features)
			{
				for (String prereq : feature.getPrerequisites())
				{
					Long prePri = priorities.get(prereq);
					Long pri = priorities.get(feature.getFeatureName());

					Long preNew = new Long(Math.max(prePri.longValue() + 1L, pri.longValue()));

					priorities.put(feature.getFeatureName(), preNew);

					adjusted = adjusted | !preNew.equals(pri);
				}
			}
		}


		// use a sorter to put them into correct order
		Collections.sort(features, new Comparator<Feature>()
		{
			@Override
			public int compare(Feature feature, Feature feature1)
			{
				Long fp1 = priorities.get(feature.getFeatureName());
				Long fp2 = priorities.get(feature1.getFeatureName());

				return fp1.compareTo(fp2);
			}
		});
	}

	public <T> T postCreate(T object)
	{
		injector.injectMembers(object);

		return object;
	}
}
