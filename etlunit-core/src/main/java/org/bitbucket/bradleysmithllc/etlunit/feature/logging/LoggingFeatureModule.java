package org.bitbucket.bradleysmithllc.etlunit.feature.logging;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Module;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureLogListenerProxy;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.module_signer_mojo.IMavenProject;
import org.bitbucket.bradleysmithllc.module_signer_mojo.ModuleVersions;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@FeatureModule
public class LoggingFeatureModule extends AbstractFeature
{
	private RuntimeSupport runtimeSupport;
	private LogManagerRuntimeSupport logManagerRuntimeSupport;

	private Log applicationLog;
	private final LogListenerLogProxy logListenerLogProxy;
	private LogFileManager logFileManager;

	private final DateTimeFormatter df = DateTimeFormatter.ofPattern("HH:mm:ss");

	private final boolean echo;

	public LoggingFeatureModule(RuntimeSupport runtimeSupport, List<Feature> features)
	{
		this.runtimeSupport = runtimeSupport;

		logManagerRuntimeSupport = new LogManagerRuntimeSupportImpl(runtimeSupport);

		// before logging anything, clear the log folder
		IOUtils.purge(logManagerRuntimeSupport.getLogDirectory());

		logListenerLogProxy = new LogListenerLogProxy(new FeatureLogListenerProxy(features), runtimeSupport);

		echo = System.getProperty("org.bitbucket.bradleysmithllc.etlunit.feature.logging.echo") != null;
	}

	public LogListenerLogProxy getLog()
	{
		return logListenerLogProxy;
	}

	@Override
	protected Injector preCreateSub(Injector inj) {
		logManagerRuntimeSupport = postCreate(logManagerRuntimeSupport);

		Injector childInjector = inj.createChildInjector(new Module() {
			public void configure(Binder binder) {
				binder.bind(LogManagerRuntimeSupport.class).toInstance(logManagerRuntimeSupport);
			}
		});

		return childInjector;
	}

	@Inject
	public void receiveLogFileManager(LogFileManager manager)
	{
		logFileManager = manager;
	}

	@Inject
	public void receiveApplicationLog(@Named(value = "applicationLog") Log log)
	{
		applicationLog = log;
	}

	@Override
	public ClassListener getListener()
	{
		return new NullClassListener()
		{
			@Override
			public void begin(ETLTestClass cl, VariableContext context, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				logListenerLogProxy.begin(cl, context, executor);
			}

			@Override
			public void begin(ETLTestMethod mt, VariableContext context, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				logListenerLogProxy.begin(mt, context, executor);
			}

			@Override
			public void begin(ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				logListenerLogProxy.begin(op, parameters, vcontext, econtext, executor);
			}

			@Override
			public void end(ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				logListenerLogProxy.end(op, parameters, vcontext, econtext, executor);
			}

			@Override
			public void end(ETLTestMethod mt, VariableContext context, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				logListenerLogProxy.end(mt, context, executor);
			}

			@Override
			public void end(ETLTestClass cl, VariableContext context, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				logListenerLogProxy.end(cl, context, executor);
			}

			@Override
			public action_code process(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, final VariableContext context, ExecutionContext econtext, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				// add the option to log to a file here
				if (op.getOperationName().equals("log"))
				{
					String message = context.contextualize(obj.getValueAsMap().get("message").getValueAsString());

					applicationLog.info("Logging message: " + message);

					// check for a log file name
					ETLTestValueObject lfnQuery = obj.query("log-file-name");
					ETLTestValueObject lcQuery = obj.query("log-classifier");

					if (lfnQuery == null && lcQuery != null)
					{
						throw new TestExecutionError("Cannot log to a file without log-file-name", TestConstants.ERR_BAD_OPERANDS);
					}
					else if (lfnQuery != null && lcQuery == null)
					{
						throw new TestExecutionError("Cannot log to a file without log-classifier", TestConstants.ERR_BAD_OPERANDS);
					}
					else if (lfnQuery != null && lcQuery != null)
					{
						String logFileName = lfnQuery.getValueAsString();
						String logClassifier = lcQuery.getValueAsString();

						try {
							// create the file - use an anonymous folder to protect against concurrency
							File file = runtimeSupport.createAnonymousTempFolder();
							File logFile = new File(file, logFileName);

							FileUtils.write(logFile, message);

							applicationLog.info("Logging message '" + message + "' to file '" + logFile.getAbsolutePath() + "'");

							if (mt != null)
							{
								logFileManager.addLogFile(mt, op, logFile, logClassifier, message);
							}
							else
							{
								logFileManager.addLogFile(op, logFile, logClassifier, message);
							}
						} catch (IOException e) {
							throw new TestExecutionError("Error writing log file", TestConstants.ERR_IO_ERROR, e);
						}
					}

					return action_code.handled;
				}

				return action_code.defer;
			}
		};
	}

	@Override
	public ClassDirector getDirector() {
		return new NullClassDirector()
		{
			@Override
			public void beginBroadcast() {
				LogListener ll = getLogListener();

				ll.log(LogListener.message_type.debug, "Installed modules", null, null, null);
				ll.log(LogListener.message_type.debug, "-------------------------------", null, null, null);

				List<IMavenProject> mvlist = new ModuleVersions(Thread.currentThread().getContextClassLoader()).getAvailableVersions();

				for (IMavenProject mv : mvlist)
				{
					String message = mv.getMavenGroupId()
									+ "."
									+ mv.getMavenArtifactId() + "\n" + mv.getMavenVersionNumber() + "\n" + mv.getGitBranch() + "\n" + mv.isGitBranchClean() + "\n" + mv.getGitBranchChecksum() + "\n";
					ll.log(LogListener.message_type.debug, message, null, null, null);
				}

				ll.log(LogListener.message_type.debug, "-------------------------------", null, null, null);
			}
		};
	}

	@Override
	public LogListener getLogListener()
	{
		return new LogListener()
		{
			@Override
			public void log(message_type type, String text, ETLTestClass etlClass, ETLTestMethod method, ETLTestOperation operation)
			{
				log(type, null, text, etlClass, method, operation);
			}

			@Override
			public void log(message_type type, Throwable error, String text, ETLTestClass etlClass, ETLTestMethod method, ETLTestOperation operation)
			{
				File log = null;

				if (method != null)
				{
					log = logManagerRuntimeSupport.getLogFile(method);
				}
				else if (etlClass != null)
				{
					log = logManagerRuntimeSupport.getLogFile(etlClass);
				}
				else
				{
					log = logManagerRuntimeSupport.getApplicationLogFile();
				}

				StringBuilder header = new StringBuilder("[");

				header.append(df.format(LocalDateTime.now()));
				header.append('.');

				switch (type)
				{
					case debug:
						header.append("DBUG");
						break;
					case info:
						header.append("INFO");
						break;
					case severe:
						header.append("SEVR");
						break;
				}

				if (etlClass != null)
				{
					header.append(".");
					header.append(etlClass.getQualifiedName());

					if (method != null)
					{
						header.append(".");
						header.append(method.getName());

						if (operation != null)
						{
							header.append(".");
							header.append(operation.getOperationName());
						}
					}
				}

				header.append("]");

				StringBuilder tbuilder = new StringBuilder(text);

				if (error != null)
				{
					StringWriter swriter = new StringWriter();
					PrintWriter pwriter = new PrintWriter(swriter);

					error.printStackTrace(pwriter);
					pwriter.flush();
					pwriter.close();

					tbuilder.append("\n").append(swriter.toString());
				}

				String
						logEntry =
						EtlUnitStringUtils.prepareForLog(tbuilder.toString().replace("\r\n", "\n").replace("\r", "\n"), header.toString());

				if (echo)
				{
					System.out.println(logEntry);
				}

				try
				{
					PrintWriter writer = new PrintWriter(new FileWriter(log, true), true);

					try
					{
						writer.println(logEntry);
					}
					finally
					{
						writer.close();
					}
				}
				catch (IOException exc)
				{
					exc.printStackTrace();
				}
			}
		};
	}

	@Override
	public String getFeatureName()
	{
		return "logging";
	}

	@Override
	public boolean requiredForSimulation() {
		return true;
	}
}
