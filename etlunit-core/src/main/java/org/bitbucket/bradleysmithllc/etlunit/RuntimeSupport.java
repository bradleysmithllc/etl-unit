package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;

public interface RuntimeSupport
{
	MapLocal mapLocal();

	/* Process runtime variables in the reference */
	String processReference(String pathName);

	/* Processes a possible reference to another package */
	Pair<ETLTestPackage, String> processPackageReference(ETLTestPackage etlTestPackage, String name);

	interface TempFileNamingPolicy
	{
		String makeAnonymousFileName();
		String makeAnonymousFileNameWithExtension(String ext);
		String makeAnonymousFileNameWithPrefix(String pref);
		String makeAnonymousFolderName();
		String nameTempFolder(String name);
		String nameTempFile(String name);
		File mkTempDirectoryRoot(File tempRoot, int executor);
	}

	String digestIdentifier(String id);

	void activateTempFileNamingPolicy(TempFileNamingPolicy policy);

	/**
	 * Given the file reference, check if it exists, if it does not,
	 * search for a file with the same name but different case.  If the
	 * case does not match, a canonical file will be returned which matches the
	 * file case correctly.
	 * @param file
	 * @return
	 */
	File resolveFile(File file);

	File getProjectRoot() throws IOException;
	String getProjectRelativePath(File fke) throws IOException;

	/**
	 * Root of the temp file storage area.
	 * @return
	 */
	File getTempRoot();

	/**
	 * The temp file storage that this thread should use.
	 * @return
	 */
	File getTempDirectory();

	/**
	 * The temp storage area used by the named executor.
	 * @param executor
	 * @return
	 */
	File getTempDirectory(int executor);

	File createTempFolder(String dirname);
	File createAnonymousTempFolder();

	File createTempFile(String name);
	File createTempFile(String subdir, String name);

	File createAnonymousTempFileWithPrefix(String name);
	File createAnonymousTempFile();
	File createAnonymousTempFileWithExtension(String ext);
	File createAnonymousTempFileInSubdir(String subdir);
	File createAnonymousTempFile(String subdir, String ext);

	File getGeneratedSourceDirectory();
	File getGeneratedSourceDirectory(String feature);
	File getGeneratedSourceDirectoryForCurrentTest(String feature);

	File createGeneratedSourceFile(String feature, String name);

	File getFeatureSourceDirectory(String feature);

	File getSourceDirectory();

	File getTestSourceDirectory();

	File getGeneratedSourceDirectory(ETLTestPackage _package);

	File getTestSourceDirectory(ETLTestPackage _package);

	/**
	 * Gets the source directory for the currently executing test.
	 */
	File getCurrentTestSourceDirectory();

	File getTestResourceDirectory(String subdir);
	File getTestResourceDirectory(ETLTestPackage _package, String subdir);

	/**
	 * Gets the source directory for the currently executing test.
	 */
	File getCurrentTestResourceDirectory(String subdir);

	File getGlobalReportDirectory(String type);
	File getReportDirectory(String type);

	File getFeatureConfigurationDirectory(String feature);

	File getSourceDirectory(ETLTestPackage feature);

	File createSourceFile(ETLTestPackage feature, String name);

	File getReferenceDirectory(String path_);

	File getReferenceFile(String path_, String name);
	File getReferenceFile(String path_);

	URL getReferenceResource(String path_, String name);
	URL getReferenceResource(String path_);

	ProcessExecutor getProcessExecutor();

	void installProcessExecutor(ProcessExecutor od);

	ProcessFacade execute(ProcessDescription pd) throws IOException;

	String getProjectName();

	String getProjectVersion();

	String getProjectUser();

	/** Returns a unique identifier for this project based on the absolute path to the project and the MAC address of the computer.
	 * The executor ID is appended to the end, so this may return different values depending on where in the lifecycle the system is currently.
	 * @return
	 */
	String getProjectUID();

	/** Returns the identifier a specific executor would use.
	 * @return
	 */
	String getProjectUID(int executor);

	void overrideRuntimeOption(RuntimeOption runtimeOption);
	void overrideRuntimeOptions(List<RuntimeOption> runtimeOptions);

	RuntimeOption getRuntimeOption(String qualifiedName);

	List<RuntimeOption> getRuntimeOptions(String featureName);

	List<RuntimeOption> getRuntimeOptions();

	List<ETLTestPackage> getPackages(File root);

	List<ETLTestPackage> getReferencePackages(String path);

	List<ETLTestPackage> getTestPackages();

	boolean isTestActive();
	ETLTestPackage getCurrentlyProcessingTestPackage();
	ETLTestClass getCurrentlyProcessingTestClass();
	ETLTestMethod getCurrentlyProcessingTestMethod();
	ETLTestOperation getCurrentlyProcessingTestOperation();
	VariableContext getCurrentlyProcessingVariableContext();

	File getVendorBinaryDir(String path);

	File getVendorBinaryDir();

	Log getApplicationLog();

	Log getUserLog();

	String getLocalAddress();

	/**
	 * Gets the number of parallel executors this build will use.
	 * @return
	 */
	int getExecutorCount();

	/**
	 * If a build is in progress, retrieves the id of the current executor.  If there are no active executors, will return id 0.
	 * @return
	 */
	int getExecutorId();

	/*Store something in executor thread-local storage*/
	<T> void storeInExecutor(String id, T t);
	/*Retrieve something from executor thread-local storage*/
	<T> T retrieveFromExecutor(String id, Class<T> cls);

	/*As above, except that it scopes with the feature to avoid global namespace collisions*/
	<T> void storeInExecutor(Feature module, String id, T t);
	<T> T retrieveFromExecutor(Feature module, String id, Class<T> cls);

	void removeStoredFromExecutor(String id);
	void removeStoredFromExecutor(Feature owner, String id);


	/**
	 * Returns a unique identifier guaranteed to distinguish this test run from every other one.
	 * @return
	 */
	String getRunIdentifier();

	void setTestStartTime(LocalDateTime time);

	void setTestStartTime();

	//void setTestStartTime(long testStartTimeL);

	LocalDateTime getTestStartTime();
}
