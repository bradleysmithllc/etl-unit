package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.bitbucket.bradleysmithllc.etlunit.util.jdbc.JdbcObjectRead;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.*;
import java.text.ParseException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class DataFileSchemaImpl implements DataFileSchema {
	private static AtomicInteger tableInstanceCounter = new AtomicInteger();

	private final String id;

	private final DataFileSchemaDef dataFileSchemaDef;

	private final DataFileManager dataFileManager;

	private final ArrayList<DataFileSchema> dataFileSchemas;

	DataFileSchemaImpl(
			String resourceId,
			format_type format,
			String rowDelimiter,
			String columnDelimiter,
			String nullDelimiter,
			DataFileManager dataFileManager
	) {
		dataFileSchemaDef = new DataFileSchemaDef();
		dataFileSchemaDef.processDefaults();

		this.dataFileManager = dataFileManager;

		dataFileSchemaDef.formatType = format;
		id = resourceId;
		dataFileSchemaDef.rowDelimiter = rowDelimiter;
		dataFileSchemaDef.columnDelimiter = columnDelimiter;
		setNullToken(nullDelimiter);

		dataFileSchemas = new ArrayList<>(Arrays.asList(this));
		validateInternal();
	}

	DataFileSchemaImpl(
		DataFileSchemaDef def,
		String resourceId,
		DataFileManager dataFileManager
	) {
		dataFileSchemas = new ArrayList<>(Arrays.asList(this));
		this.dataFileManager = dataFileManager;
		id = resourceId;

		dataFileSchemaDef = def;
		validateInternal();
	}

	public boolean hasInheritance() {
		return dataFileSchemaDef.inheritSchema != null;
	}

	public String inherit() {
		return dataFileSchemaDef.inheritSchema;
	}

	public DataFileSchemaImpl withInherit(String _extends) {
		this.dataFileSchemaDef.inheritSchema = _extends;
		return this;
	}

	private void validateInternal() {
		if (dataFileSchemaDef.columnDelimiter != null && dataFileSchemaDef.formatType == format_type.fixed) {
			throw new IllegalArgumentException("Fixed-width files do not have column delimiters");
		}

		if (dataFileSchemaDef.columnDelimiter == null && dataFileSchemaDef.formatType == format_type.delimited) {
			throw new IllegalArgumentException("Delimited files must have column delimiters");
		}

		if (dataFileSchemaDef.nullToken == null) {
			throw new IllegalArgumentException("Null tokens may not have the value null");
		}
	}

	public String getId() {
		return id;
	}

	@Override
	public DataFileSchemaView createSubViewIncludingColumns(List<String> columns, String id, format_type format) {
		List<String> properColumns = getLogicalColumnNames();

		if (columns != null) {
			properColumns = intersectInclusiveLists(getLogicalColumnNames(), columns);
		}

		return new DataFileSchemaViewImpl(properColumns, this, id, format);
	}

	@Override
	public List<Column> getOrderColumns() {
		return Collections.unmodifiableList(dataFileSchemaDef.orderColumns());
	}

	public String getRowDelimiter() {
		return dataFileSchemaDef.rowDelimiter;
	}

	public format_type getFormatType() {
		return dataFileSchemaDef.formatType;
	}

	public Column createColumn(String id) {
		return new SchemaColumn(id, "VARCHAR", dataFileManager);
	}

	@Override
	public void addColumns(List<Column> columns) {
		for (Column column : columns) {
			addColumn(column);
		}
	}

	@Override
	public void addColumn(Column column) {
		dataFileSchemaDef.addColumn(column);
	}

	@Override
	public void addKeyColumn(String name) {
		dataFileSchemaDef.addKeyColumn(name);
	}

	@Override
	public void addOrderColumns(List<Column> name) {
		for (Column column : name) {
			addOrderColumn(column.getId());
		}
	}

	@Override
	public void addKeyColumns(List<Column> keyColumns) {
		for (Column column : keyColumns) {
			addKeyColumn(column.getId());
		}
	}

	private static synchronized int getNextInstance() {
		return tableInstanceCounter.incrementAndGet();
	}

	@Override
	public TemporaryTable createTemporaryDbTable(Connection embeddedDatabase) throws SQLException {
		return createTemporaryDbTableImpl(this, embeddedDatabase);
	}

	static TemporaryTable createTemporaryDbTableImpl(final DataFileSchema schema, final Connection embeddedDatabase) throws SQLException {
		Statement statement = embeddedDatabase.createStatement();

		final String name = schema.createTemporaryDbTableName();

		try {
			String temporaryDbTableSql = schema.createTemporaryDbTableSql(name);
			statement.execute(temporaryDbTableSql);
		} finally {
			statement.close();
		}

		return new TemporaryTable() {
			final PreparedStatement insertSt = embeddedDatabase.prepareStatement(schema.createInsertSql(name));

			@Override
			public DataFileReader getData() {
				return getData(null);
			}

			@Override
			public DataFileReader getData(final String query) {
				return new DataFileReader() {
					@Override
					public Iterator<FileRow> iterator() throws IOException {
						final ResultSet selectSet;
						final List<String> columnLabels = new ArrayList<String>();
						final Map<String, Integer> columnTypes = new HashMap<String, Integer>();

						try {
							String selectSql = query;

							if (selectSql == null) {
								selectSql = schema.createSelectSql(name);
							} else {
								// process with velocity, providing the runtime table name
								Map<String, String> contextMap = new HashMap<String, String>();

								contextMap.put("file-table-name", "\"" + name + "\"");
								contextMap.put(schema.getId().replace('.', '_'), "\"" + name + "\"");

								selectSql = VelocityUtil.writeTemplate(selectSql, contextMap);
							}

							selectSet = embeddedDatabase.createStatement().executeQuery(selectSql);

							ResultSetMetaData rsmd = selectSet.getMetaData();

							int colCount = rsmd.getColumnCount();
							for (int i = 1; i <= colCount; i++) {
								String colName = rsmd.getColumnLabel(i);
								columnLabels.add(colName);
								columnTypes.put(colName, rsmd.getColumnType(i));
							}
						} catch (SQLException e) {
							throw new IOException(e);
						} catch (Exception e) {
							throw new IOException(e);
						}

						return new Iterator<FileRow>() {
							FileRow nextRow = null;
							Map<String, Object> rowMap = new HashMap<String, Object>();

							@Override
							public boolean hasNext() {
								nextRow = null;
								rowMap.clear();

								try {
									if (selectSet.next()) {
										int n = 0;
										for (String col : columnLabels) {
											n++;

											// timestamp!!
											Object o = JdbcObjectRead.readObject(selectSet, n, columnTypes.get(col).intValue());

											if (o == null) {
												rowMap.put(col.toLowerCase(), null);
											} else if (columnTypes.get(col).intValue() != Types.CLOB) {
												rowMap.put(col.toLowerCase(), o);
											} else {
												Clob clob = (Clob) o;
												StringWriter stw = new StringWriter();

												IOUtils.copy(clob.getCharacterStream(), stw);

												rowMap.put(col.toLowerCase(), stw.toString());
											}
										}

										nextRow = new FileRow() {
											@Override
											public Map<String, Object> getData() {
												return rowMap;
											}

											@Override
											public OrderKey getOrderKey() {
												return null;
											}

											@Override
											public Column getColumn(String id) {
												return schema.getColumn(id);
											}
										};
									} else {
										selectSet.close();
									}
								} catch (SQLException e) {
									throw new RuntimeException(e);
								} catch (IOException e) {
									throw new RuntimeException(e);
								}

								return nextRow != null;
							}

							@Override
							public FileRow next() {
								return nextRow;
							}

							@Override
							public void remove() {
							}
						};
					}

					@Override
					public void dispose() throws IOException {
					}
				};
			}

			@Override
			public DataFileWriter getWriter() {
				return getWriter(new CopyOptionsBuilder().options());
			}

			@Override
			public DataFileWriter getWriter(final DataFileManager.CopyOptions options) {
				return new DataFileWriter() {
					int sequence = -1;
					int batchSize = 0;

					// keep a list of resolved values for passing to the default value deal
					private final Map<String, Object> beanData = new HashMap<String, Object>();
					private final Map<String, Object> thisColData = new HashMap<String, Object>();

					@Override
					public void close() throws IOException {
						try {
							if (batchSize != 0) {
								try {
									insertSt.executeBatch();
								} catch (SQLException e) {
									throw new IOException(e);
								}
							}
						} finally {
							try {
								insertSt.close();
							} catch (SQLException e) {
								throw new IOException(e);
							}
						}
					}

					@Override
					public void addRow(Map<String, Object> rowData) throws IOException {
						sequence++;

						beanData.clear();
						thisColData.clear();

						thisColData.putAll(rowData);

						beanData.put("sequence", sequence);
						beanData.put("rowData", thisColData);

						try {
							int colNo = 0;
							for (Column col : schema.getPhysicalColumns()) {
								colNo++;
								boolean containsCol = rowData.containsKey(col.getId());
								Object colVal = rowData.get(col.getId());

								if (!containsCol) {
									// check missing policy
									if (options.getMissingColumnPolicy() == DataFileManager.CopyOptions.missing_column_policy.use_default) {
										// check for a default value
										Object defaultValue = col.generateColumnValue(beanData, sequence);

										if ("null".equals(defaultValue)) {
											defaultValue = null;
										}

										if (defaultValue != null) {
											String stringDefaultValue = String.valueOf(defaultValue);

											DataConverter conv = col.getConverter();

											if (conv != null) {
												try {
													defaultValue = conv.parse(stringDefaultValue, col);
												} catch (ParseException e) {
													throw new SQLException(stringDefaultValue, e);
												}
											}

											thisColData.put(col.getId(), defaultValue);

											//if (defaultValue instanceof ZonedDateTime) {
											//	insertSt.setTimestamp(colNo, Timestamp.valueOf(((ZonedDateTime) defaultValue).toLocalDateTime()));
											//} else {
												insertSt.setObject(colNo, defaultValue);
											//}
										} else {
											thisColData.put(col.getId(), schema.getNullToken());
											insertSt.setNull(colNo, col.hasType() ? col.getConverter().getJdbcType() : Types.VARCHAR);
										}
									} else {
										thisColData.put(col.getId(), schema.getNullToken());
										insertSt.setNull(colNo, col.hasType() ? col.getConverter().getJdbcType() : Types.VARCHAR);
									}
								} else {
									if (colVal != null) {
										// check for a clob - it's value must be passed on specially.
										if (col.hasType() && col.getConverter().getJdbcType() == Types.CLOB) {
											Clob clob = (Clob) colVal;
											String clobStr = IOUtils.toString(clob.getCharacterStream());
											thisColData.put(col.getId(), clobStr);

											insertSt.setClob(colNo, new StringReader(clobStr));
										} else {
											thisColData.put(col.getId(), colVal);

											if (colVal instanceof ZonedDateTime) {
												insertSt.setTimestamp(colNo, Timestamp.valueOf(((ZonedDateTime) colVal).toLocalDateTime()));
											} else {
												// check for a diff type and corresponding time types
												if (options.honorDateDiff() == DataFileManager.CopyOptions.honor_date_diff.yes && col.jdbcType() == Types.VARCHAR) {
													// try to be clever here.  If the date/time are within a small amount of now, reference the symbolic
													// time (as in ts()), otherwise match as a literal
													switch (col.diffType()) {
														case date:
															// for date - it must match exactly
															if (colVal instanceof LocalDate) {
																LocalDate localDateVal = (LocalDate) colVal;

																if (localDateVal.equals(LocalDate.now())) {
																	insertSt.setObject(colNo, "<!--dt-->");
																} else {
																	insertSt.setObject(colNo, "<!--dt(baseValue='" + DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDateVal) + "')-->");
																}
															} else {
																insertSt.setObject(colNo, colVal);
															}
															break;
														case timestamp:
															// as for timestamp, give a 60-second window from the past.  Future times always fail.
															if (colVal instanceof LocalDateTime) {
																LocalDateTime localDateTimeVal = (LocalDateTime) colVal;

																if (
																	localDateTimeVal.isAfter(LocalDateTime.now().minus(1, ChronoUnit.MINUTES))
																	&&
																	localDateTimeVal.isBefore(LocalDateTime.now())
																) {
																	insertSt.setObject(colNo, "<!--ts-->");
																} else {
																	insertSt.setObject(colNo, "<!--ts(baseValue='" + DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS").format(localDateTimeVal) + "')-->");
																}
															} else {
																insertSt.setObject(colNo, colVal);
															}
															break;
														case time:
															// as for timestamp, give a 60-second window from the past.  Future times always fail.
															if (colVal instanceof LocalTime) {
																LocalTime localDateTimeVal = (LocalTime) colVal;

																LocalTime oldestTime = LocalTime.now().minus(1, ChronoUnit.MINUTES);
																LocalTime now = LocalTime.now().plus(1, ChronoUnit.MINUTES);
																if (
																	localDateTimeVal.isAfter(oldestTime)
																		&&
																		localDateTimeVal.isBefore(now)
																) {
																	insertSt.setObject(colNo, "<!--tm-->");
																} else {
																	insertSt.setObject(colNo, "<!--tm(baseValue='" + DateTimeFormatter.ofPattern("HH:mm:ss.SSS").format(localDateTimeVal) + "')-->");
																}
															} else {
																insertSt.setObject(colNo, colVal);
															}
															break;
														case none:
															insertSt.setObject(colNo, colVal);
															break;
													}
												} else {
													insertSt.setObject(colNo, colVal);
												}
											}
										}
									} else {
										thisColData.put(col.getId(), schema.getNullToken());
										insertSt.setNull(colNo, col.hasType() ? col.getConverter().getJdbcType() : Types.VARCHAR);
									}
								}
							}

							batchSize++;
							insertSt.addBatch();

							if (batchSize >= 1000) {
								insertSt.executeBatch();
								batchSize = 0;
							}
						} catch (SQLException e) {
							throw new IOException(e);
						}
					}
				};
			}

			@Override
			public String getName() {
				return name;
			}
		};
	}

	@Override
	public String createSelectSql(String name) {
		return createSelectSqlImpl(name, this);
	}

	@Override
	public DataFileManager getDataFileManager() {
		return dataFileManager;
	}

	@Override
	public List<String> getReadOnlyColumnNames() {
		List<String> readOnlyColNames = new ArrayList<String>();
		List<Column> cols = getPhysicalColumns();

		Iterator<Column> it = cols.iterator();

		while (it.hasNext()) {
			Column col = it.next();

			if (col.isReadOnly()) {
				readOnlyColNames.add(col.getId());
			}
		}

		return readOnlyColNames;
	}

	@Override
	public void clearOrderColumns() {
		dataFileSchemaDef.clearOrderColumns();
	}

	static String createSelectSqlImpl(String name, DataFileSchema schema) {
		StringBuilder stb = new StringBuilder("SELECT\n");

		int count = 0;
		for (Column col : schema.getLogicalColumns()) {
			count++;

			if (count != 1) {
				stb.append(",\n");
			}

			if (col.getId().contains(" ")) {
				stb.append("\t\"" + col.getId() + "\"");
			} else {
				stb.append("\t").append(col.getId());
			}
		}

		stb.append("\nFROM\n").append("\t\"" + name + "\"\n");

		if (schema.getOrderColumns().size() != 0) {
			stb.append("ORDER BY\n");

			count = 0;
			for (Column col : schema.getOrderColumns()) {
				count++;

				if (count != 1) {
					stb.append(",\n");
				}

				if (col.getId().contains(" ")) {
					stb.append("\t\"" + col.getId() + "\"");
				} else {
					stb.append("\t").append(col.getId());
				}
			}
		}

		return stb.toString();
	}

	@Override
	public String createTemporaryDbTableName() {
		return createTemporaryDbTableNameImpl(this);
	}

	static String createTemporaryDbTableNameImpl(DataFileSchema id) {
		return createTemporaryDbTableNameImpl(id, System.currentTimeMillis(), getNextInstance());
	}

	@Override
	public String createInsertSql(String name) {
		return createInsertSqlImpl(name, this);
	}

	static String createInsertSqlImpl(String name, DataFileSchema schema) {
		StringBuilder stb = new StringBuilder("INSERT INTO \"" + name + "\"");

		stb.append("\n(\n");

		int count = 0;
		for (Column col : schema.getPhysicalColumns()) {
			count++;

			if (count != 1) {
				stb.append(",\n");
			}

			if (col.getId().contains(" ")) {
				stb.append("\t\"" + col.getId() + "\"");
			} else {
				stb.append("\t").append(col.getId());
			}
		}

		stb.append("\n)\n");
		stb.append("VALUES\n");
		stb.append("(");
		count = 0;
		for (Column col : schema.getPhysicalColumns()) {
			count++;

			if (count != 1) {
				stb.append(", ");
			}

			stb.append("?");
		}

		stb.append(");");

		return stb.toString();
	}

	@Override
	public String createTemporaryDbTableName(long timesTamp, int instanceCount) {
		return createTemporaryDbTableNameImpl(this, timesTamp, instanceCount);
	}

	static String createTemporaryDbTableNameImpl(DataFileSchema id, long timesTamp, int instanceCount) {
		// create a table matching the columns in this schema
		String dateGlom = DateFormatUtils.format(new Date(timesTamp), "yyyyMMdd-HH-mm-ss-SSS", TimeZone.getTimeZone(ZoneId.of("UTC")));

		String tableName = id.getId() + "-virtualtable-" + dateGlom + "-" + instanceCount;

		return tableName;
	}

	@Override
	public String createTemporaryDbTableSql(String name) {
		return createTemporaryDbTableSqlImpl(name, this);
	}

	static String createTemporaryDbTableSqlImpl(String name, DataFileSchema schema) {
		StringBuilder stb = new StringBuilder("CREATE TABLE \"" + name + "\"\n");
		stb.append("(\n");

		// add a string column for every column we know about
		int colNum = 0;
		for (Column col : schema.getPhysicalColumns()) {
			colNum++;
			if (colNum != 1) {
				stb.append(",\n");
			}

			int type = Types.VARCHAR;

			// if the type is null, varchar is the default
			if (col.hasType()) {
				// get the type from the converter
				type = col.getConverter().getJdbcType();
			}

			if (col.getId().contains(" ")) {
				stb.append("\t\"").append(col.getId()).append("\"");
			} else {
				stb.append("\t").append(col.getId());
			}

			stb.append(" ");

			switch (type) {
				case Types.TIMESTAMP:
					stb.append("TIMESTAMP");
					break;
				case Types.TIMESTAMP_WITH_TIMEZONE:
					stb.append("TIMESTAMP WITH TIME ZONE");
					break;
				case Types.INTEGER:
					stb.append("INTEGER");
					break;
				case Types.SMALLINT:
					stb.append("SMALLINT");
					break;
				case Types.BIGINT:
					stb.append("BIGINT");
					break;
				case Types.TINYINT:
					stb.append("TINYINT");
					break;
				case Types.CHAR:
					stb.append("CHAR");
					if (col.getLength() != -1) {
						stb.append("(").append(col.getLength()).append(")");
					}
					break;
				case Types.LONGVARCHAR:
					stb.append("LONGVARCHAR");
					break;
				case Types.VARCHAR:
					stb.append("VARCHAR");
					if (col.getLength() != -1) {
						stb.append("(").append(col.getLength()).append(")");
					} else {
						stb.append("(1024)");
					}
					break;
				case Types.DATE:
					stb.append("DATE");
					break;
				case Types.TIME:
					stb.append("TIME");
					break;
				case Types.TIME_WITH_TIMEZONE:
					stb.append("TIME WITH TIME ZONE");
					break;
				case Types.BIT:
					stb.append("BIT");
					break;
				case Types.FLOAT:
					stb.append("FLOAT");
					break;
				case Types.DECIMAL:
				case Types.NUMERIC:
					switch(type){
						case Types.DECIMAL:
							stb.append("DECIMAL");
							break;
						case Types.NUMERIC:
							stb.append("NUMERIC");
							break;
					}

					if (col.getLength() != -1) {
						stb.append("(").append(col.getLength()).append(", ");

						if (col.getScale() != -1) {
							stb.append(col.getScale());
						} else {
							stb.append("0");
						}

						stb.append(")");
					}


					break;
				case Types.DOUBLE:
					stb.append("DOUBLE");
					break;
				case Types.REAL:
					stb.append("REAL");
					break;
				case Types.BOOLEAN:
					stb.append("BOOLEAN");
					break;
				case Types.CLOB:
					stb.append("CLOB");
					break;
				case Types.BLOB:
					stb.append("BLOB");
					break;
			}

			stb.append(" NULL");
		}

		StringBuilder stindex = new StringBuilder();

		if (schema.getKeyColumns().size() != 0) {
			stindex.append(",\n\tPRIMARY KEY").append("\n");
			stindex.append("\t(\n");

			colNum = 0;
			for (Column col : schema.getKeyColumns()) {
				colNum++;
				if (colNum != 1) {
					stindex.append(",\n");
				}

				if (col.getId().contains(" ")) {
					stindex.append("\t\t\"").append(col.getId()).append("\"");
				} else {
					stindex.append("\t\t").append(col.getId());
				}
			}

			stindex.append("\n\t)\n");

			if (colNum != 0) {
				stb.append(stindex.toString());
			}
		} else {
			stb.append("\n");
		}

		stb.append(");\n");

		// this index only works if there is at least one non-[c|b]lob column
		if (schema.getOrderColumns().size() != 0) {
			stindex.setLength(0);
			stindex.append("\n").append("CREATE INDEX \"" + name + "_IDX\" ON \"" + name + "\"").append("\n");
			stindex.append("(\n");

			colNum = 0;
			for (Column col : schema.getOrderColumns()) {
				if (!col.hasType() || (col.getConverter().getJdbcType() != Types.CLOB && col.getConverter().getJdbcType() != Types.BLOB)) {
					colNum++;
					if (colNum != 1) {
						stindex.append(",\n");
					}

					if (col.getId().contains(" ")) {
						stindex.append("\t\"").append(col.getId()).append("\"");
					} else {
						stindex.append("\t").append(col.getId());
					}
				}
			}

			stindex.append("\n);");

			if (colNum != 0) {
				stb.append(stindex.toString());
			}
		}

		return stb.toString();
	}

	@Override
	public void addOrderColumn(String name) {
		dataFileSchemaDef.addOrderColumn(name);
	}

	@Override
	public void setKeyColumns(List<String> names) {
		dataFileSchemaDef.keyColumnNames.clear();
		dataFileSchemaDef.keyColumns.clear();

		for (String col : names) {
			addKeyColumn(col);
		}
	}

	@Override
	public void setOrderColumns(List<String> names) {
		dataFileSchemaDef.orderColumnNames.clear();
		dataFileSchemaDef.orderColumns.clear();

		for (String col : names) {
			addOrderColumn(col);
		}
	}

	public String getColumnDelimiter() {
		return dataFileSchemaDef.columnDelimiter;
	}

	public List<Column> getLogicalColumns() {
		return Collections.unmodifiableList(dataFileSchemaDef.columns);
	}

	public List<Column> getPhysicalColumns() {
		return getLogicalColumns();
	}

	@Override
	public String getPKId() {
		return null;
	}

	@Override
	public List<DataFileSchema> lineage() {
		return dataFileSchemas;
	}

	@Override
	public Column getColumn(String name) {
		return dataFileSchemaDef.getColumn(name);
	}

	@Override
	public boolean hasColumn(String name) {
		return dataFileSchemaDef.columnNames.indexOf(name) != -1;
	}

	public Map<String, Object> validateAndSplitLine(String line) throws DataFileMismatchException {
		Map<String, Object> map = new HashMap<String, Object>();

		List<String> colData = null;

		switch (dataFileSchemaDef.formatType) {
			case delimited:
				// split on the delimiter
				String search = Pattern.quote(dataFileSchemaDef.columnDelimiter);

				colData = Arrays.asList(line.split(search, -1));

				String columnDelimiter1 = getColumnDelimiter();

				// unescape every data point
				for (int i = 0; i < colData.size(); i++) {
					String data = colData.get(i);

					if (dataFileSchemaDef.escapeNonPrintable) {
						if (columnDelimiter1 != null) {
							String unescape = EtlUnitStringUtils.unescape(data, '\\', columnDelimiter1, getRowDelimiter());
							colData.set(i, unescape);
						} else {
							String unescape = EtlUnitStringUtils.unescape(data, '\\', getRowDelimiter());
							colData.set(i, unescape);
						}
					}
				}

				break;

			case fixed:
				// do this before the length check since the length might balloon if there is escaping
				if (dataFileSchemaDef.escapeNonPrintable) {
					line = EtlUnitStringUtils.unescape(line, '\\', getRowDelimiter());
				}

				if (line.length() != dataFileSchemaDef.lineLength) {
					throw new DataFileMismatchException("Illegal line - incorrect length.  Found["
							+ line.length()
							+ "] required ["
							+ dataFileSchemaDef.lineLength
							+ "]: "
							+ line);
				}

				colData = new ArrayList<String>();

				for (int i = 0; i < dataFileSchemaDef.columns.size(); i++) {
					Column schemaCol = dataFileSchemaDef.columns.get(i);

					int offset = schemaCol.getOffset();
					int endIndex = offset + schemaCol.getLength();

					if (line.length() < endIndex) {
						throw new IllegalArgumentException("Illegal line - length too short: " + line);
					}

					String colText = line.substring(offset, endIndex);

					colData.add(colText);
				}

				break;
		}


		if (colData.size() != dataFileSchemaDef.columns.size()) {
			throw new DataFileMismatchException("Line does not have the correct number of columns: expected["
					+ dataFileSchemaDef.columns.size()
					+ "], actual["
					+ colData.size()
					+ "] "
					+ line);
		}

		for (int colNo = 0; colNo < colData.size(); colNo++) {
			String token = colData.get(colNo);

			Column schemaCol = getLogicalColumns().get(colNo);

			if (dataFileSchemaDef.nullToken != null && dataFileSchemaDef.nullToken.equals(token)) {
				map.put(schemaCol.getId(), null);
			} else {
				schemaCol.validateText(token);

				try {
					map.put(schemaCol.getId(), schemaCol.getConverter().parse(token, schemaCol));
				} catch (ParseException e) {
					throw new DataFileMismatchException(e);
				}
			}
		}

		return map;
	}

	@Override
	public DataFileSchemaView createSubViewExcludingColumns(List<String> columns, String id, format_type format) {
		List<String> properColumns = getLogicalColumnNames();

		if (columns != null) {
			properColumns = intersectExclusiveLists(getLogicalColumnNames(), columns);
		}

		return createSubViewIncludingColumns(properColumns, id, format);
	}

	@Override
	public DataFileSchemaView createSubViewExcludingColumns(List<String> columns, String id) {
		return createSubViewExcludingColumns(columns, id, null);
	}

	@Override
	public DataFileSchemaView intersect(DataFileSchema that, String id, format_type format) {
		DataFileSchemaView subViewIncludingColumns = createSubViewIncludingColumns(that.getLogicalColumnNames(), id, format);
		return subViewIncludingColumns;
	}

	@Override
	public DataFileSchemaView intersect(DataFileSchema that, String id) {
		return intersect(that, id, null);
	}

	public DataFileSchemaView createSubViewIncludingColumns(
			List<String> columns,
			String id) {
		return createSubViewIncludingColumns(columns, id, null);
	}

	static DataFileSchema createSubViewIncludingColumnsImpl
			(
					List<String> columns,
					String id,
					String thisId,
					format_type format,
					boolean escapeNonPrintable,
					String rowDelimiter, String columnDelimiter, String nullToken, List<Column> columnList, List<Column> keyColumns, List<Column> orderColumns, DataFileManager dataFileManager) {
		String delim = columnDelimiter;

		switch (format) {
			case fixed:
				delim = null;
				break;
			case delimited:
				if (delim == null) {
					delim = dataFileManager.getDefaultColumnDelimiter();
				}
				break;
		}

		DataFileSchemaImpl newSchema = new DataFileSchemaImpl(
				id != null ? id : thisId,
				format,
				rowDelimiter,
				delim,
				nullToken,
				dataFileManager
		);

		newSchema.setEscapeNonPrintable(escapeNonPrintable);

		// When materializing a view, be sure to preserve column order for
		// primary key and order columns
		// clone all of the columns into the new object to make sure the schema
		// is truly independent of this one.
		if (columns != null) {
			for (Column col : columnList) {
				if (columns.contains(col.getId())) {
					newSchema.addColumn(col.clone());
				}
			}

			for (Column col : orderColumns) {
				if (newSchema.hasColumn(col.getId())) {
					Column ncol = newSchema.getColumn(col.getId());
					newSchema.dataFileSchemaDef.orderColumns.add(ncol);
					newSchema.dataFileSchemaDef.orderColumnNames.add(ncol.getId());
				}
			}

			for (Column col : keyColumns) {
				if (newSchema.hasColumn(col.getId())) {
					Column ncol = newSchema.getColumn(col.getId());
					newSchema.dataFileSchemaDef.keyColumns.add(ncol);
					newSchema.dataFileSchemaDef.keyColumnNames.add(ncol.getId());
				}
			}

			if (newSchema.dataFileSchemaDef.columns.size() != columns.size()) {
				throw new IllegalArgumentException("Unmatched columns in view");
			}
		} else {
			newSchema.addColumns(columnList);
			newSchema.addOrderColumns(orderColumns);
			newSchema.addKeyColumns(keyColumns);
		}

		return newSchema;
	}

	@Override
	public List<String> getLogicalColumnNames() {
		return Collections.unmodifiableList(dataFileSchemaDef.columnNames);
	}

	@Override
	public List<String> getOrderColumnNames() {
		return Collections.unmodifiableList(dataFileSchemaDef.orderColumnNames());
	}

	@Override
	public List<Column> getKeyColumns() {
		return Collections.unmodifiableList(dataFileSchemaDef.keyColumns);
	}

	@Override
	public List<String> getKeyColumnNames() {
		return Collections.unmodifiableList(dataFileSchemaDef.keyColumnNames);
	}

	@Override
	public boolean isView() {
		return false;
	}

	public String toJsonString() {
		return dataFileSchemaDef.toJsonString();
	}

	public String getNullToken() {
		return dataFileSchemaDef.nullToken;
	}

	public String getPrintableNullToken() {
		return dataFileSchemaDef.nullToken != null ? "\"" + dataFileSchemaDef.nullToken + "\"" : null;
	}

	@Override
	public boolean escapeNonPrintable() {
		return dataFileSchemaDef.escapeNonPrintable;
	}

	@Override
	public void setEscapeNonPrintable(boolean esc) {
		dataFileSchemaDef.escapeNonPrintable = esc;
	}

	public void setColumnDelimiter(String columnDelimiter) {
		if (dataFileSchemaDef.formatType == format_type.fixed) {
			throw new IllegalArgumentException("Fixed-width files do not have column delimiters");
		}

		this.dataFileSchemaDef.columnDelimiter = columnDelimiter;
	}

	public void setRowDelimiter(String rowDelimiter) {
		this.dataFileSchemaDef.rowDelimiter = rowDelimiter;
	}

	public void setNullToken(String nullToken) {
		this.dataFileSchemaDef.withNullToken(nullToken);
	}

	@Override
	public void setFormatType(format_type type) {
		if (dataFileSchemaDef.formatType != type) {
			if (dataFileSchemaDef.formatType == format_type.delimited) {
				dataFileSchemaDef.columnDelimiter = null;
			} else {
				// set the column delimiter to the default
				dataFileSchemaDef.columnDelimiter = dataFileManager.getDefaultColumnDelimiter();
			}

			dataFileSchemaDef.formatType = type;
		}
	}

	@Override
	public String toString() {
		return "DataFileSchema{" +
				"id='" + id + '\'' +
				", escapeNonPrintable=" + dataFileSchemaDef.escapeNonPrintable +
				", rowDelimiter='" + dataFileSchemaDef.rowDelimiter + '\'' +
				", columnDelimiter='" + dataFileSchemaDef.columnDelimiter + '\'' +
				", nullToken='" + dataFileSchemaDef.nullToken + '\'' +
				", columns=" + dataFileSchemaDef.columns +
				", columnNames=" + dataFileSchemaDef.columnNames +
				", orderColumns=" + dataFileSchemaDef.orderColumns +
				", orderColumnNames=" + dataFileSchemaDef.orderColumnNames +
				", keyColumns=" + dataFileSchemaDef.keyColumns +
				", keyColumnNames=" + dataFileSchemaDef.keyColumnNames +
				", formatType=" + dataFileSchemaDef.formatType +
				", dataFileManager=" + dataFileManager +
				", lineLength=" + dataFileSchemaDef.lineLength +
				", dataFileSchemas[size]=" + dataFileSchemas.size() +
				'}';
	}

	public static List<String> intersectLists(Collection<String> reference, Collection<String> request, boolean inclusive) {
		if (inclusive) {
			return intersectInclusiveLists(reference, request);
		} else {
			return intersectExclusiveLists(reference, request);
		}
	}

	public static List<String> intersectInclusiveLists(Collection<String> reference, Collection<String> request) {
		List<String> intersectedColumnNames = new ArrayList<>();

		for (String col : request) {
			Iterator<String> colsI = reference.iterator();
			boolean matched = false;

			while (colsI.hasNext()) {
				String colName = colsI.next();

				if (colName.equalsIgnoreCase(col)) {
					intersectedColumnNames.add(colName);
					matched = true;
					break;
				}
			}

			if (!matched) {
				throw new IllegalArgumentException("Column not found: " + col);
			}
		}

		if (request.size() != intersectedColumnNames.size()) {
			throw new IllegalArgumentException("Invalid columns list");
		}

		return intersectedColumnNames;
	}

	public static List<String> intersectExclusiveLists(Collection<String> reference, Collection<String> request) {
		Map<String, String> lowerNames = new HashMap<>();

		for (String req : request) {
			lowerNames.put(req.toLowerCase(), req.toLowerCase());
		}

		List<String> intersectedColumnNames = new ArrayList<>();

		for (String ref : reference) {
			if (lowerNames.containsKey(ref.toLowerCase())) {
				lowerNames.remove(ref.toLowerCase());
			} else {
				intersectedColumnNames.add(ref);
			}
		}

		if (lowerNames.size() != 0) {
			throw new IllegalArgumentException("Invalid columns list: " + lowerNames.keySet());
		}

		return intersectedColumnNames;
	}
}
