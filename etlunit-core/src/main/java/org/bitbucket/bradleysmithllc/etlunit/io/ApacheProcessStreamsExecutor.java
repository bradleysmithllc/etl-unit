package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.exec.*;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.util.Incomplete;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ApacheProcessStreamsExecutor implements ProcessExecutor
{
	@Incomplete
	@Override
	public ProcessFacade execute(final ProcessDescription pd)
	{
		// apache thinks they are too good for process builders
		final CommandLine cmdLine = new CommandLine(pd.getCommand());

		Iterator<String> it = pd.getArguments().iterator();

		// drop the first entry - that is the command
		while (it.hasNext())
		{
			cmdLine.addArgument(it.next());
		}

		final DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

		final ExecuteWatchdog watchdog = new ExecuteWatchdog(15 * 60 * 1000);

		Executor executor = new DefaultExecutor();

		try
		{
			final File outputFile = pd.getOutputFile();
			final ModifiedPumpStreamHandler modifiedPumpStreamHandler =
					new ModifiedPumpStreamHandler(outputFile);

			executor.setStreamHandler(modifiedPumpStreamHandler);

			executor.setWatchdog(watchdog);

			if (pd.getWorkingDirectory() != null)
			{
				executor.setWorkingDirectory(pd.getWorkingDirectory());
			}

			Map<String, String> getenv = new HashMap<String, String>(System.getenv());

			getenv.putAll(pd.getEnvironment());
			executor.execute(cmdLine, getenv, resultHandler);

			return new ProcessFacade()
			{
				private StringBuffer bufferedInput;

				@Override
				public ProcessDescription getDescriptor()
				{
					return pd;
				}

				@Override
				public void waitForCompletion()
				{
					waitForStreams();

					try
					{
						resultHandler.waitFor();

						waitForOutputStreamsToComplete();
					}
					catch (InterruptedException e)
					{
						throw new RuntimeException("Error while waiting for process", e);
					}
					catch (IOException e)
					{
						throw new RuntimeException("Error while waiting for process", e);
					}
				}

				@Override
				public int getCompletionCode()
				{
					return resultHandler.getExitValue();
				}

				@Override
				public void kill()
				{
					watchdog.destroyProcess();
				}

				@Incomplete
				@Override
				public Writer getWriter()
				{
					return modifiedPumpStreamHandler.getProcessInput();
				}

				@Override
				public void waitForStreams()
				{
					//wait until the streams have been injected
					modifiedPumpStreamHandler.waitForStreams();
				}

				@Override
				public void waitForOutputStreamsToComplete() throws IOException {
					modifiedPumpStreamHandler.waitForOutputStreamToComplete();
				}

				@Incomplete
				@Override
				public BufferedReader getReader()
				{
					return new BufferedReader(modifiedPumpStreamHandler.getProcessOutput());
				}

				@Incomplete
				@Override
				public BufferedReader getErrorReader()
				{
					return new BufferedReader(modifiedPumpStreamHandler.getProcessOutput());
				}

				@Incomplete
				@Override
				public StringBuffer getInputBuffered() throws IOException
				{
					return new StringBuffer(FileUtils.readFileToString(outputFile));
					/*
					Reader processOutput = modifiedPumpStreamHandler.getProcessOutput();

					if (bufferedInput == null)
					{
						bufferedInput = new StringBuffer();
					}

					waitForCompletion();

					BufferedReader bread = new BufferedReader(processOutput);

					char[] buff = new char[4096];

					int read = 0;

					while ((read = bread.read(buff)) != -1)
					{
						bufferedInput.append(buff, 0, read);
					}

					return bufferedInput;
					 */
				}

				@Incomplete
				@Override
				public StringBuffer getErrorBuffered() throws IOException
				{
					return getInputBuffered();
				}

				@Incomplete
				@Override
				public OutputStream getOutputStream()
				{
					return modifiedPumpStreamHandler.getProcessInputStream();
				}

				@Incomplete
				@Override
				public InputStream getInputStream()
				{
					return modifiedPumpStreamHandler.getProcessOutputStream();
				}

				@Incomplete
				@Override
				public InputStream getErrorStream()
				{
					return modifiedPumpStreamHandler.getProcessOutputStream();
				}
			};
		}
		catch (IOException e)
		{
			throw new IllegalArgumentException("Error invoking system process", e);
		}
	}
}
