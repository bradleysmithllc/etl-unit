package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.exec.ExecuteStreamHandler;

import java.io.*;
import java.util.concurrent.CountDownLatch;

public class ModifiedPumpRedirectHandler implements ExecuteStreamHandler
{
	private final CustomPumpStreamHandler pumpStreamHandler;

	private final CountDownLatch streamAcquiredLatch = new CountDownLatch(3);

	public ModifiedPumpRedirectHandler(OutputStream output) throws IOException
	{
		pumpStreamHandler = new CustomPumpStreamHandler(output);
	}

	public InputStream getProcessOutputStream()
	{
		throw new UnsupportedOperationException();
	}

	public OutputStream getProcessInputStream()
	{
		throw new UnsupportedOperationException();
	}

	public Writer getProcessInput()
	{
		throw new UnsupportedOperationException();
	}

	public Reader getProcessOutput()
	{
		throw new UnsupportedOperationException();
	}

	public void setProcessOutputStream(InputStream is)
	{
		pumpStreamHandler.setProcessOutputStream(is);
		streamAcquiredLatch.countDown();
	}

	public void stop()
	{
		pumpStreamHandler.stop();
	}

	public void start()
	{
		pumpStreamHandler.start();
	}

	public void setProcessInputStream(OutputStream os) throws IOException
	{
		pumpStreamHandler.setProcessInputStream(os);
		streamAcquiredLatch.countDown();
	}

	public void setProcessErrorStream(InputStream is)
	{
		pumpStreamHandler.setProcessErrorStream(is);
		streamAcquiredLatch.countDown();
	}

	public void waitForStreams()
	{
		try {
			streamAcquiredLatch.await();
		} catch (InterruptedException e) {
			throw new RuntimeException("Why did this happen?", e);
		}
	}

	public void waitForOutputStreamToComplete() throws IOException {
		pumpStreamHandler.waitForOutput();
	}
}
