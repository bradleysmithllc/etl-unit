package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DataFile
{
	TemporaryTable createTemporaryDbTable() throws SQLException;

	File getFile();

	File getSource();

	DataFileWriter writer() throws IOException;

	/**
	 *
	 * @param options Use the given copy options during copy
	 * @return
	 * @throws IOException
	 */
	DataFileWriter writer(DataFileManager.CopyOptions options) throws IOException;

	Map<String, Map<String, String>> getIndex();

	DataFileReader reader() throws IOException;

	DataFileReader reader(List<String> columns) throws IOException;

	DataFileSchema getDataFileSchema();
	DataFileManager getDataFileManager();
}
