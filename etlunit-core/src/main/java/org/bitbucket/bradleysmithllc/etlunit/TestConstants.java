package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface TestConstants
{
	String ERR_UNSPECIFIED = "ERR_UNSPECIFIED";
	String ERR_INVALID_TEST_ANNOTATION = "ERR_INVALID_TEST_ANNOTATION";
	String ERR_UNCAUGHT_EXCEPTION = "ERR_UNCAUGHT_EXCEPTION";
	String ERR_IO_ERROR = "ERR_IO_ERROR";

	String ERR_BAD_OPERANDS = "ERR_BAD_OPERANDS";

	String FAIL_UNSPECIFIED = "FAIL_UNSPECIFIED";
	String FAIL_EXPECTED_ASSERTION = "FAIL_EXPECTED_ASSERTION";
	String FAIL_EXPECTED_WARNING = "FAIL_EXPECTED_WARNING";
	String FAIL_EXPECTED_EXCEPTION = "FAIL_EXPECTED_EXCEPTION";

	String WARN_UNSPECIFIED = "WARN_UNSPECIFIED";
	String ERR_INVALID_ANNOTATION = "ERR_INVALID_ANNOTATION";
	String ERR_INVALID_ANNOTATION_PROPERTIES = "ERR_INVALID_ANNOTATION_PROPERTIES";
	String ERR_INVALID_OPERATION = "ERR_INVALID_OPERATION";
	String ERR_IGNORE_EXPIRED = "ERR_IGNORE_EXPIRED";
}
