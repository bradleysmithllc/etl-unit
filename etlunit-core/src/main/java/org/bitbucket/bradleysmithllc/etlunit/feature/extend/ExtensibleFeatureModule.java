package org.bitbucket.bradleysmithllc.etlunit.feature.extend;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.OperationProcessor;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;

import java.util.*;

@FeatureModule
public abstract class ExtensibleFeatureModule extends AbstractFeature
{
	private final List<Extender>
			extenders =
			new ArrayList<Extender>();

	@Override
	public final ClassListener getListener()
	{
		return new ClassListener()
		{
			private final ClassListener classListener = getDelegateListener();

			@Override
			public void beginTests(VariableContext context, int executorId) throws TestExecutionError {
				if (classListener != null)
				{
					classListener.beginTests(context, executorId);
				}
			}

			@Override
			public void beginTests(VariableContext context) throws TestExecutionError {
				if (classListener != null)
				{
					classListener.beginTests(context);
				}
			}

			@Override
			public void beginPackage(ETLTestPackage name, VariableContext context, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
				if (classListener != null)
				{
					classListener.beginPackage(name, context, executor);
				}
			}

			@Override
			public void endPackage(ETLTestPackage name, VariableContext context, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
				if (classListener != null)
				{
					classListener.endPackage(name, context, executor);
				}
			}

			@Override
			public void endTests(VariableContext context, int executorId) {
				if (classListener != null)
				{
					classListener.endTests(context, executorId);
				}
			}

			@Override
			public void endTests(VariableContext context) {
				if (classListener != null)
				{
					classListener.endTests(context);
				}
			}

			@Override
			public void begin(ETLTestClass cl, final VariableContext context, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				if (classListener != null)
				{
					classListener.begin(cl, context, executor);
				}
			}

			@Override
			public void declare(ETLTestVariable var, final VariableContext context, final int executor)
			{
				if (classListener != null)
				{
					classListener.declare(var, context, executor);
				}
			}

			@Override
			public void begin(ETLTestMethod mt, final VariableContext context, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				if (classListener != null)
				{
					classListener.begin(mt, context, executor);
				}
			}

			@Override
			public void begin(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				if (classListener != null)
				{
					classListener.begin(mt, op, parameters, vcontext, econtext, executor);
				}
			}

			@Override
			public action_code process(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				if (classListener != null)
				{
					return classListener.process(mt, op, obj, context, econtext, executor);
				}

				// check for meta info - in case we have none we should defer to the extenders here.
				if (getMetaInfo().getExportedOperations() == null)
				{
					return new OperationProcessorSpammer((List) extenders).process(mt, op, obj, context, econtext, executor);
				}

				return action_code.defer;
			}

			@Override
			public void end(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				if (classListener != null)
				{
					classListener.end(mt, op, parameters, vcontext, econtext, executor);
				}
			}

			@Override
			public void end(ETLTestMethod mt, final VariableContext context, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				if (classListener != null)
				{
					classListener.end(mt, context, executor);
				}
			}

			@Override
			public void end(ETLTestClass cl, final VariableContext context, final int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				if (classListener != null)
				{
					classListener.end(cl, context, executor);
				}
			}
		};
	}

	@Override
	public FeatureMetaInfo getMetaInfo()
	{
		final FeatureMetaInfo meta = super.getMetaInfo();

		return new FeatureMetaInfo()
		{
			@Override
			public String getFeatureName()
			{
				return meta.getFeatureName();
			}

			@Override
			public String getFeatureConfiguration()
			{
				return meta.getFeatureConfiguration();
			}

			@Override
			public JsonSchema getFeatureConfigurationValidator()
			{
				return meta.getFeatureConfigurationValidator();
			}

			@Override
			public JsonNode getFeatureConfigurationValidatorNode() {
				return meta.getFeatureConfigurationValidatorNode();
			}

			@Override
			public Map<String, FeatureOperation> getExportedOperations()
			{
				Map<String, FeatureOperation> expOp = meta.getExportedOperations();

				if (expOp != null)
				{
					Map<String, FeatureOperation> newExpOp = new HashMap<String, FeatureOperation>();

					// wrap this to capture the signatures of our exported operations and expose the extender
					// operations

					for (Map.Entry<String, FeatureOperation> entry : expOp.entrySet())
					{
						String opName = entry.getKey();

						FeatureOperation featOp = entry.getValue();

						newExpOp.put(opName, new FeatureOperationWrapper(featOp, getExtenderOperations(opName)));
					}

					return newExpOp;
				}

				return null;
			}

			@Override
			public Map<String, FeatureAnnotation> getExportedAnnotations()
			{
				return meta.getExportedAnnotations();
			}

			@Override
			public boolean isInternalFeature()
			{
				return meta.isInternalFeature();
			}

			@Override
			public String getFeatureUsage()
			{
				return meta.getFeatureUsage();
			}

			@Override
			public List<RuntimeOptionDescriptor> getOptions()
			{
				return Collections.emptyList();
			}

			@Override
			public String getDescribingClassName()
			{
				return getDescribing().getClass().getName();
			}

			@Override
			public Feature getDescribing()
			{
				return ExtensibleFeatureModule.this;
			}
		};
	}

	/**
	 * Get a list of all operations which can extend this operation.  This relies on the meta info, but
	 * the fallback is always the extender itself.
	 *
	 * @param opName
	 * @return
	 */
	private List<OperationProcessor> getExtenderOperations(String opName)
	{
		List<OperationProcessor> flist = new ArrayList<OperationProcessor>();

		for (Extender ext : extenders)
		{
			FeatureMetaInfo metaInfo = ext.getFeature().getMetaInfo();

			if (metaInfo != null)
			{
				Map<String, FeatureOperation> exportedOperations = metaInfo.getExportedOperations();

				if (exportedOperations != null)
				{
					FeatureOperation featop = exportedOperations.get(opName);

					if (featop != null)
					{
						flist.add(featop);
					}
					else
					{
						flist.add(ext);
					}
				}
				else
				{
					flist.add(ext);
				}
			}
			else
			{
				flist.add(ext);
			}
		}

		return flist;
	}

	public final void extend(Extender exe)
	{
		extenders.add(exe);
	}

	protected ClassListener getDelegateListener()
	{
		return null;
	}

	private class FeatureOperationWrapper implements FeatureOperation
	{
		private final FeatureOperation featureOperation;
		private final OperationProcessorSpammer operationProcessorSpammer;

		public FeatureOperationWrapper(FeatureOperation featOp, List<OperationProcessor> extenderOperations)
		{
			featureOperation = featOp;
			operationProcessorSpammer = new OperationProcessorSpammer(extenderOperations);
		}

		@Override
		public String getName()
		{
			return featureOperation.getName();
		}

		@Override
		public String getDescription()
		{
			return featureOperation.getDescription();
		}

		@Override
		public String getUsage()
		{
			return featureOperation.getUsage();
		}

		@Override
		public String getPrototype()
		{
			return featureOperation.getPrototype();
		}

		@Override
		public List<FeatureOperationSignature> getSignatures()
		{
			return featureOperation.getSignatures();
		}

		@Override
		public action_code process(ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, final int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			return operationProcessorSpammer.process(mt, op, parameters, vcontext, econtext, executor);
		}
	}
}
