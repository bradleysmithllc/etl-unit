package org.bitbucket.bradleysmithllc.etlunit.metadata.impl;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class MetaDataPackageContextImpl implements MetaDataPackageContext {
	private RuntimeSupport runtimeSupport;

	private MetaDataContext metaDataContext;

	private ETLTestPackage packageName;
	private path_type pathType;
	private List<MetaDataArtifact> artifacts = new ArrayList<MetaDataArtifact>();
	private Map<String, MetaDataArtifact> artifactMap = new HashMap<String, MetaDataArtifact>();

	public MetaDataPackageContextImpl(ETLTestPackage packageName, path_type pathType) {
		this.packageName = packageName;
		this.pathType = pathType;
	}

	public MetaDataPackageContextImpl() {
	}

	public void setMetaDataContext(MetaDataContext metaDataContext) {
		this.metaDataContext = metaDataContext;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support) {
		runtimeSupport = support;
	}

	@Override
	public ETLTestPackage getPackageName() {
		return packageName;
	}

	@Override
	public path_type getPathType() {
		return pathType;
	}

	@Override
	public List<MetaDataArtifact> getArtifacts() {
		return Collections.unmodifiableList(artifacts);
	}

	@Override
	public Map<String, MetaDataArtifact> getArtifactMap() {
		return artifactMap;
	}

	@Override
	public MetaDataContext getMetaDataContext() {
		return metaDataContext;
	}

	@Override
	public MetaDataArtifact createArtifact(String relativePath, File path) throws IOException {
		if (runtimeSupport == null) {
			throw new IllegalStateException("RuntimeSupport not available");
		}

		return createArtifact(relativePath, runtimeSupport.getProjectRelativePath(path), path);
	}

	@Override
	public synchronized MetaDataArtifact createArtifact(String relativePath, String projectRelativePath, File path) {
		if (artifactMap.containsKey(relativePath))
		{
			return artifactMap.get(relativePath);
		}

		MetaDataArtifactImpl art = new MetaDataArtifactImpl(relativePath, projectRelativePath, path);
		art.receiveRuntimeSupport(runtimeSupport);
		art.setMetaDataPackageContext(this);
		artifacts.add(art);
		artifactMap.put(relativePath, art);
		return art;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("path-type").value(pathType.name());
		writer.name("package");

		if (packageName.isDefaultPackage()) {
			writer.nullValue();
		} else {
			writer.value(packageName.getPackageId());
		}

		writer.name("artifacts");
		writer.beginArray();

		for (MetaDataArtifact art : artifacts) {
			writer.beginObject();
			art.toJson(writer);
			writer.endObject();
		}

		writer.endArray();
	}

	@Override
	public synchronized void fromJson(JsonNode node) {
		artifacts.clear();
		artifactMap.clear();

		pathType = path_type.valueOf(node.get("path-type").asText());
		JsonNode packNode = node.get("package");

		if (packNode != null && !packNode.isNull()) {
			packageName = ETLTestPackageImpl.getDefaultPackage().getSubPackage(packNode.asText());
		}
		else
		{
			packageName = ETLTestPackageImpl.getDefaultPackage();
		}

		ArrayNode arts = (ArrayNode) node.get("artifacts");

		Iterator<JsonNode> it = arts.elements();

		while (it.hasNext()) {
			MetaDataArtifactImpl impl = new MetaDataArtifactImpl();

			impl.receiveRuntimeSupport(runtimeSupport);
			impl.fromJson(it.next());

			artifacts.add(impl);
			artifactMap.put(impl.getRelativePath(), impl);
		}
	}

	public boolean equals(Object other) {
		if (getClass() != other.getClass()) {
			return false;
		}

		MetaDataPackageContextImpl oImpl = (MetaDataPackageContextImpl) other;

		return
				pathType == oImpl.pathType &&
						ObjectUtils.compare(packageName, oImpl.packageName) == 0 &&
						artifacts.equals(oImpl.artifacts);
	}
}
