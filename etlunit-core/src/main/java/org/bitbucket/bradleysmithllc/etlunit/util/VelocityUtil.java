package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.parser.node.MathUtils;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.util.cli.Enumerations;

import java.io.File;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

public class VelocityUtil
{
	private static final File templateRoot = new File(".");

	public static VelocityEngine getVelocityEngine()
	{
		return getVelocityEngine(templateRoot);
	}

	private static MapList<String, VelocityEngine> engines = new HashMapArrayList<String, VelocityEngine>();

	public static VelocityEngine getVelocityEngine(File templateRoot)
	{
		VelocityEngine velocityEngine = newVelocityEngine(templateRoot);

		return velocityEngine;
	}

	private static VelocityEngine newVelocityEngine(File templateRoot) {
		synchronized (engines)
		{
			List<VelocityEngine> orCreate = engines.getOrCreate(templateRoot.getAbsolutePath());

			if (orCreate.size() != 0)
			{
				return orCreate.remove(orCreate.size() - 1);
			}

			VelocityEngine velocityEngine = new VelocityEngine();
			velocityEngine.addProperty("file.resource.loader.path", templateRoot.getAbsolutePath());
			velocityEngine.init();

			return velocityEngine;
		}
	}

	public static void writeTestReport(File testReport, TestSuite suite) throws Exception
	{
		VelocityContext vcontext = new VelocityContext();

		vcontext.put("testSuite", suite);

		writeTemplate(new File("..\\conf\\TEST-report.vm"), testReport, suite);
	}

	public static void writeTemplate(File template, File output, Object bean) throws Exception
	{
		VelocityContext vcontext = new VelocityContext();

		vcontext.put("bean", bean);

		IOUtils.writeBufferToFile(output, new StringBuffer(writeTemplate(template, bean)));
	}

	public static String writeTemplate(File template, Object bean) throws Exception
	{
		VelocityContext vcontext = new VelocityContext();

		vcontext.put("bean", bean);

		/* lets render a template */
		String st = IOUtils.readFileToString(template);

		return writeTemplate(st, bean);
	}

	public static String writeTemplate(String template, Object bean) throws Exception
	{
		VelocityContext vcontext = new VelocityContext();

		vcontext.put("bean", bean);
		vcontext.put("Math", Math.class);
		vcontext.put("StringUtils", org.apache.commons.lang3.StringUtils.class);
		vcontext.put("numbers", Numbers.class);
		vcontext.put("strings", Strings.class);

		StringWriter w = new StringWriter();

		VelocityEngine velocityEngine = getVelocityEngine();

		try
		{
			velocityEngine.evaluate(vcontext, w, "log", template);
		}
		finally
		{
			putVelocityEngine(velocityEngine, templateRoot);
		}

		return w.toString();
	}

	public static String writeTemplateWithStaticSupport(String template, Map bean) throws Exception
	{
		return writeTemplateWithStaticSupport(template, bean, new File("."));
	}

	public static String writeTemplateWithStaticSupport(String template, Map bean, File templateRoot) throws Exception
	{
		bean.put("Math", Math.class);
		bean.put("math", MathUtils.class);
		bean.put("StringUtils", org.apache.commons.lang3.StringUtils.class);
		bean.put("numbers", Numbers.class);
		bean.put("strings", Strings.class);
		bean.put("enumerations", Enumerations.class);

		VelocityContext context = new VelocityContext(bean);

		StringWriter w = new StringWriter();

		VelocityEngine velocityEngine = getVelocityEngine(templateRoot);

		try
		{
			velocityEngine.evaluate(context, w, "log", template);
		}
		finally
		{
			putVelocityEngine(velocityEngine, templateRoot);
		}

		return w.toString();
	}

	public static String writeTemplate(String template, Map bean) throws Exception
	{
		return writeTemplate(template, bean, new File("."));
	}

	public static String writeTemplate(String template, Map bean, File templateRoot) throws Exception
	{
		VelocityContext context = new VelocityContext(bean);

		StringWriter w = new StringWriter();

		VelocityEngine velocityEngine = getVelocityEngine(templateRoot);

		try
		{
			velocityEngine.evaluate(context, w, "log", template);
		}
		finally
		{
			putVelocityEngine(velocityEngine, templateRoot);
		}

		return w.toString();
	}

	public static String writeTemplate(String template, VariableContext bean) throws Exception
	{
		return writeTemplate(template, bean, new File("."));
	}

	public static String writeTemplate(String template, VariableContext bean, File templateRoot) throws Exception
	{
		VelocityContext context = new VelocityContext(bean.getVelocityWrapper());

		StringWriter w = new StringWriter();

		VelocityEngine velocityEngine = getVelocityEngine(templateRoot);

		try
		{
			velocityEngine.evaluate(context, w, "log", template);
		}
		finally
		{
			putVelocityEngine(velocityEngine, templateRoot);
		}

		return w.toString();
	}

	private static void putVelocityEngine(VelocityEngine velocityEngine, File root) {
		synchronized (engines)
		{
			engines.getOrCreate(root.getAbsolutePath()).add(velocityEngine);
		}
	}
}
