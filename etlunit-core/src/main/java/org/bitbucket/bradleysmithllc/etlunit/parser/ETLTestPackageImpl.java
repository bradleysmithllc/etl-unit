package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

public class ETLTestPackageImpl implements ETLTestPackage {
	private final String packageId;
	private final String packageName;
	private final String [] packagePath;

	private final Map<String, ETLTestPackage> children = new HashMap<String, ETLTestPackage>();

	private final ETLTestPackage parent;

	private static final ETLTestPackage defaultPackage = new ETLTestPackageImpl();

	private ETLTestPackageImpl()
	{
		packageId = null;
		packageName = NAME_OF_DEFAULT_PACKAGE;
		packagePath = null;
		parent = null;
	}

	private ETLTestPackageImpl(ETLTestPackage parent, String packageId) {
		this.packageId = packageId;

		if (packageId == null)
		{
			throw new IllegalArgumentException("Cannot instantiate default package");
		}

		packageName = packageId;
		packagePath = packageName.split("\\.");

		this.parent = parent;
	}

	@Override
	public String getPackageName() {
		return packageName;
	}

	@Override
	public String getPackageId() {
		return packageId;
	}

	@Override
	public synchronized ETLTestPackage getSubPackage(String name) {
		String [] path = name.split("\\.");

		// check for children
		ETLTestPackage destPackage = children.get(path[0]);

		if (destPackage == null)
		{
			String packageIID = isDefaultPackage() ? path[0] : (packageId + "." + path[0]);
			destPackage = new ETLTestPackageImpl(this, packageIID);
			children.put(path[0], destPackage);
		}

		for (int i = 1; i < path.length; i++)
		{
			destPackage = destPackage.getSubPackage(path[i]);
		}

		return destPackage;
	}

	@Override
	public ETLTestPackage getParentPackage() {
		return parent;
	}

	@Override
	public String [] getPackagePath() {
		return packagePath;
	}

	@Override
	public boolean isDefaultPackage() {
		return packageId == null;
	}

	public static ETLTestPackage getDefaultPackage()
	{
		return defaultPackage;
	}

	@Override
	public int compareTo(ETLTestPackage o) {
		if (o.isDefaultPackage() && isDefaultPackage())
		{
			// both default - equal
			return 0;
		}
		else if (isDefaultPackage())
		{
			// I am default, other not - I am less
			return -1;
		}
		else if (o.isDefaultPackage())
		{
			// other default, I am not - I am greater
			return 1;
		}

		// let them work it out
		return getPackageName().compareTo(o.getPackageName());
	}
}
