package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.NullInputStream;
import org.bitbucket.bradleysmithllc.etlunit.io.NullReader;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;

public class LoggingProcessExecutor implements ProcessExecutor
{
	private Log applicationLog;

	private final StringBuilder processLog = new StringBuilder();

	public StringBuilder getProcessLog()
	{
		return processLog;
	}

	@Inject
	public void setLogger(@Named("applicationLog") Log log)
	{
		this.applicationLog = log;
	}

	@Override
	public ProcessFacade execute(final ProcessDescription pd)
	{
		processLog.append(pd.debugString() + "\n");

		return new ProcessFacade()
		{
			@Override
			public ProcessDescription getDescriptor()
			{
				return pd;
			}

			@Override
			public void waitForCompletion()
			{
			}

			@Override
			public int getCompletionCode()
			{
				return 0;
			}

			@Override
			public void kill()
			{
			}

			@Override
			public Writer getWriter()
			{
				return null;
			}

			@Override
			public BufferedReader getReader()
			{
				return new BufferedReader(new NullReader());
			}

			@Override
			public BufferedReader getErrorReader()
			{
				return new BufferedReader(new NullReader());
			}

			@Override
			public StringBuffer getInputBuffered() throws IOException
			{
				return new StringBuffer();
			}

			@Override
			public StringBuffer getErrorBuffered() throws IOException
			{
				return new StringBuffer();
			}

			@Override
			public void waitForStreams()
			{
			}

			@Override
			public void waitForOutputStreamsToComplete() {
			}

			@Override
			public OutputStream getOutputStream()
			{
				return null;
			}

			@Override
			public InputStream getInputStream()
			{
				return new NullInputStream();
			}

			@Override
			public InputStream getErrorStream()
			{
				return new NullInputStream();
			}
		};
	}
}
