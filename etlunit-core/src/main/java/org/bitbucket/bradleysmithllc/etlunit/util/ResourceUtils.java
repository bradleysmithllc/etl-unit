package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;

public class ResourceUtils
{
	public static String loadResourceAsString(ClassLoader cl, String path) throws IOException {
		URL resource = ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), cl).getResource(path);

		if (resource == null) {
			throw new IOException("Resource not found at path " + path);
		}

		return IOUtils.readURLToString(resource);
	}

	public static String loadResourceAsString(Class cl, String path) throws IOException {
		URL resource = getResource(cl, path);

		if (resource == null) {
			throw new IOException("Resource not found at path " + path);
		}

		return IOUtils.readURLToString(resource);
	}

	public static URL getResource(Class cl, String path)
	{
		return ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), cl.getClassLoader()).getResource(path);
	}

	public static URL getResource(File root, Class cl, String path)
	{
		FileBuilder fb = new FileBuilder(root);

		// check the file path first
		String[] pathComp = path.split("/");

		for (int i = 0; i < pathComp.length - 1; i++)
		{
			fb = fb.subdir(pathComp[i]);
		}

		fb = fb.name(pathComp[pathComp.length - 1]);
		File file = fb.file();

		if (file.exists())
		{
			try {
				return file.toURL();
			} catch (MalformedURLException e) {
			}
		}

		return ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), cl.getClassLoader()).getResource(path);
	}

	public static Enumeration<URL> getResources(Class cl, String catalogName) throws IOException {
		return ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), cl.getClassLoader()).getResources(catalogName);
	}
}
