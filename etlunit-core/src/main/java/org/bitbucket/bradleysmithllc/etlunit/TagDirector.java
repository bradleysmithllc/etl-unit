package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.tags.TagRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import javax.inject.Inject;

public class TagDirector extends NullClassDirector
{
	private TagRuntimeSupport tagRuntimeSupport;
	private final String tagPattern;
	private final String tagType;
	private final boolean regexp;

	private TestClasses tag;

	public TagDirector(String pattern, boolean regexp, String type) {
		tagPattern = pattern;
		tagType = type;
		this.regexp = regexp;
	}

	@Inject
	public void receiveTagRuntimeSupport(TagRuntimeSupport supp)
	{
		tagRuntimeSupport = supp;
		tag = regexp ? supp.loadTagByNamePattern(tagPattern, tagType) : supp.loadTagByName(tagPattern, tagType);
	}

	@Override
	public response_code accept(ETLTestMethod mt) {
		if (tag.acceptTestMethod(mt))
		{
			return response_code.accept;
		}
		else
		{
			return response_code.reject;
		}
	}

	@Override
	public response_code accept(ETLTestClass cl) {
		if (tag.acceptTestClass(cl))
		{
			return response_code.accept;
		}
		else
		{
			return response_code.reject;
		}
	}
}
