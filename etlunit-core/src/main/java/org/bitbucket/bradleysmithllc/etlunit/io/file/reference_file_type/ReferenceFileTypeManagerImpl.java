package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonSchemaLoader;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class ReferenceFileTypeManagerImpl implements ReferenceFileTypeManager
{
	private static final String DEFAULT_CATALOG_PATH = "reference/file/fml";
	static final String DEFAULT_CATALOG_NAME = "reference-file-type-catalog.json";

	private static final JsonSchema referenceTypeCatalogSchema = JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/io/file/reference_file_type/reference-file-type-catalog.jsonSchema");
	static final JsonSchema referenceTypeSchema = JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/io/file/reference_file_type/reference-file-type.jsonSchema");

	private final String catalogName;
	private List<ReferenceFileTypeCatalog> catalogs;

	private RuntimeSupport runtimeSupport;

	public ReferenceFileTypeManagerImpl(String catalogName) {
		this.catalogName = catalogName;
	}

	public ReferenceFileTypeManagerImpl() {
		this(DEFAULT_CATALOG_PATH + "/" + DEFAULT_CATALOG_NAME);
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport supp)
	{
		runtimeSupport = supp;
	}

	@Override
	public synchronized List<ReferenceFileTypeCatalog> locateReferenceTypeCatalogs() {
		if (catalogs != null)
		{
			return catalogs;
		}

		catalogs = new ArrayList<ReferenceFileTypeCatalog>();

		List<URL> urlList = new ArrayList<URL>();

		// look in the local folder first
		if (runtimeSupport != null)
		{
			File src = runtimeSupport.getReferenceDirectory("file/fml");

			File catalog = new File(src, DEFAULT_CATALOG_NAME);
			if (catalog.exists())
			{
				try {
					urlList.add(catalog.toURL());
				} catch (MalformedURLException e) {
					runtimeSupport.getApplicationLog().severe("What??", e);
				}
			}
		}

		try {
			Enumeration<URL> urls = ResourceUtils.getResources(getClass(), catalogName);

			while (urls.hasMoreElements())
			{
				urlList.add(urls.nextElement());
			}

			for (URL url : urlList)
			{
				try
				{
					JsonNode jsonNode = JsonLoader.fromURL(url);

					ProcessingReport result = referenceTypeCatalogSchema.validate(jsonNode);

					if (!result.isSuccess())
					{
						System.out.println("Invalid reference catalog on classpath: " + result.toString());
					}
					else
					{
						ReferenceFileTypeCatalogImpl impl = new ReferenceFileTypeCatalogImpl();
						impl.receiveRuntimeSupport(runtimeSupport);
						impl.fromJson(jsonNode);
						catalogs.add(impl);
					}
				} catch (ProcessingException e) {
					System.out.println("Invalid reference catalog on classpath: " + url);
					e.printStackTrace(System.out);
				}
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

		return catalogs;
	}

	@Override
	public ReferenceFileType locateReferenceFileType(String id) throws RequestedFileTypeNotFoundException {
		locateReferenceTypeCatalogs();

		String lowerId = id.toLowerCase();

		for (ReferenceFileTypeCatalog catalog : catalogs)
		{
			ReferenceFileType referenceFileType = catalog.getReferenceFileType(lowerId);
			if (referenceFileType != null)
			{
				return referenceFileType;
			}
		}

		throw new RequestedFileTypeNotFoundException();
	}

	@Override
	public String generateResourcePathForRef(ReferenceFileTypeRef ref) throws RequestedFileTypeNotFoundException {
		ReferenceFileType rft = locateReferenceFileType(ref.getId());

		String relPath = rft.generateResourcePathForRef(ref);

		return relPath;
	}

	@Override
	public String toExternalResourcePath(String path) {
		return "resource/file/fml/" + path + ".fml";
	}
}
