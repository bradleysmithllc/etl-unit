package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import org.bitbucket.bradleysmithllc.etlunit.io.NullReader;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileReader;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReaderDataSet implements DataSet
{
	public static final String token = "\n<><><><><><>\n";
	public static final char [] tokenArray = token.toCharArray();

	private JsonNode jsonNode;

	private ReaderFileDataImpl dataFileReader;
	private final DataSetReader dataSetReader;

	private DataFileManager dataFileManager;

	public ReaderDataSet(DataFileManager manager, PushbackReader source) throws IOException {
		String node = EtlUnitStringUtils.parseToToken(source, tokenArray);

		// load using our parser so we can use relaxed format
		try {
			jsonNode = ETLTestParser.loadObject(node).getJsonNode();
		} catch (ParseException e) {
			throw new IOException(e);
		}

		dataFileManager = manager;

		dataSetReader = new DataSetReader(source);
	}

	@Override
	public JsonNode getProperties() {
		return jsonNode;
	}

	@Override
	public Reader read() throws IOException {
		if (dataFileReader != null)
		{
			if (dataFileReader.atEOF())
			{
				return new NullReader();
			}
			else
			{
				throw new IllegalStateException("There is an open data file reader.");
			}
		}

		return dataSetReader;
	}

	@Override
	public DataFileReader open() throws IOException {
		return open( (List<String>) null);
	}

	@Override
	public DataFileReader open(List<String> columns) throws IOException {
		JsonNode node = jsonNode.get("data-file-schema");

		if (node == null)
		{
			throw new IllegalStateException("data-file-schema property not present and no schema provided");
		}
		else if (node.getNodeType() == JsonNodeType.NULL)
		{
			throw new IllegalArgumentException("data-file-schema property is null");
		}
		else if (node.getNodeType() != JsonNodeType.STRING)
		{
			throw new IllegalArgumentException("data-file-schema property is not a string type");
		}

		String url = node.asText();

		try
		{
			DataFileSchema dfs = dataFileManager.loadDataFileSchemaFromResource(url, url);
			return open(dfs, columns);
		}
		catch(IllegalArgumentException exc)
		{
			throw exc;
		}
		catch(Exception exc)
		{
			throw new IOException(exc);
		}
	}

	/**
	 * Skips the data portion of this data set.  Invalid if there is an open data reader.
	 */
	void skipBody() throws IOException {
		// if we have finished the stream do nothing
		if (dataFileReader != null)
		{
			if (dataFileReader.atEOF())
			{
				return;
			}
			else
			{
				throw new IllegalStateException("There is an open data file reader.");
			}
		}

		StringWriter sw = null;

		if (ReaderDataSetContainer.debug)
		{
			sw = new StringWriter();
		}

		// read through the data set
		char [] arr = new char[1024];
		int read = 0;

		while ((read = dataSetReader.read(arr)) != -1)
		{
			if (ReaderDataSetContainer.debug)
			{
				sw.write(arr, 0, read);
			}
		}

		if (ReaderDataSetContainer.debug)
		{
			System.out.println("Skipped: " + sw.toString());
		}

		// ready for the next set
	}

	@Override
	public DataFileReader open(DataFileSchema schema, List<String> columns) {
		// check for columns defined in properties if not specified as a parameter
		if (columns == null)
		{
			JsonNode cols = getProperties().get("data-file-columns");

			if (cols != null && cols.getNodeType() != JsonNodeType.NULL)
			{
				if (cols.getNodeType() != JsonNodeType.ARRAY)
				{
					throw new IllegalArgumentException("data-file-columns is not an array type");
				}

				columns = new ArrayList<String>();

				Iterator<JsonNode> it = cols.iterator();

				while (it.hasNext())
				{
					JsonNode ne = it.next();

					if (ne.getNodeType() != JsonNodeType.STRING)
					{
						throw new IllegalArgumentException("data-file-columns array element[" + columns.size() + "] is not a text type: '" + ne.toString() + "'");
					}

					columns.add(ne.asText());
				}
			}
		}

		if (dataFileReader != null)
		{
			throw new IllegalStateException("There is already an open data file reader.");
		}

		return dataFileReader = new ReaderFileDataImpl(schema, dataSetReader, columns);
	}

	@Override
	public DataFileReader open(DataFileSchema schema) {
		return open(schema, null);
	}
}
