package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.google.inject.name.Named;
import org.bitbucket.bradleysmithllc.etlunit.ClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.NullClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.TestConstants;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestAnnotation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonProcessingUtil;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class AnnotationValidationFeature extends AbstractFeature
{
	private Log applicationLog;
	private Log userLog;
	private List<Feature> featureList;

	@Override
	public long getPriorityLevel()
	{
		return Long.MIN_VALUE;
	}

	@Inject
	public void setFeatureList(List features)
	{
		featureList = features;
	}

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log)
	{
		this.applicationLog = log;
	}

	@Inject
	public void setUserLog(@Named("userLog") Log log)
	{
		this.userLog = log;
	}

	@Override
	public String getFeatureName()
	{
		return "annotation-validator";
	}

	// this director will decline any tests or classes which have invalid annotations
	@Override
	public ClassDirector getDirector()
	{
		return new NullClassDirector()
		{
			@Override
			public response_code accept(ETLTestClass cl)
			{
				for (ETLTestAnnotation annotation : cl.getAnnotations())
				{
					validateAnnotation(cl.getQualifiedName(), annotation);
				}

				return response_code.defer;
			}

			@Override
			public response_code accept(ETLTestMethod mt)
			{
				for (ETLTestAnnotation annotation : mt.getAnnotations())
				{
					validateAnnotation(mt.getQualifiedName(), annotation);
				}

				return response_code.defer;
			}
		};
	}

	private void validateAnnotation(String qualifiedName, ETLTestAnnotation annotation)
	{
		for (Feature feature : featureList)
		{
			FeatureMetaInfo fmi = feature.getMetaInfo();

			if (fmi != null)
			{
				Map<String, FeatureAnnotation> exports = fmi.getExportedAnnotations();

				if (exports != null)
				{
					FeatureAnnotation fmiannot = exports.get(annotation.getName());

					if (fmiannot != null)
					{
						if (annotation.hasValue())
						{
							if (fmiannot.getPropertyType() == FeatureAnnotation.propertyType.none)
							{
								throw new IllegalArgumentException(TestConstants.ERR_INVALID_ANNOTATION_PROPERTIES
										+ " not allowed: "
										+ annotation.toString());
							}

							List<JsonSchema> validator = fmiannot.getValidator();

							if (validator != null)
							{
								for (JsonSchema jschema : validator)
								{
									try {
										ProcessingReport res = jschema.validate(annotation.getValue().getJsonNode());

										if (res.isSuccess())
										{
											// the one and only good case
											return;
										}
									} catch (ProcessingException e)
									{
										System.out.println(JsonProcessingUtil.getProcessingReport(e));
									}
								}
							}
						}
						else
						{
							if (fmiannot.getPropertyType() == FeatureAnnotation.propertyType.required)
							{
								throw new IllegalArgumentException(TestConstants.ERR_INVALID_ANNOTATION_PROPERTIES
										+ ": "
										+ annotation.toString());
							}
							else
							{
								return;
							}
						}
					}
				}
			}
		}

		userLog.severe("Annotation " + annotation.getName() + " is invalid on object " + qualifiedName);
		applicationLog.severe("Annotation " + annotation.getName() + " is invalid on object " + qualifiedName);

		throw new IllegalArgumentException(TestConstants.ERR_INVALID_ANNOTATION + ": " + annotation.toString());
	}
}
