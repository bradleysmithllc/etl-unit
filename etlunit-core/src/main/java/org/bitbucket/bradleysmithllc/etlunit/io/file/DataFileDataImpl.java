package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.concurrent.Semaphore;

/**
 * Created by bsmith on 10/22/14.
 */
public abstract class DataFileDataImpl implements DataFileReader {
	protected final DataFileSchema dataFileSchema;
	protected final List<String> columns;Semaphore state = new Semaphore(1);
	private BufferedReader bread;

	public DataFileDataImpl(List<String> columns, DataFileSchema schema) {
		dataFileSchema = schema;
		if (columns == null)
		{
			this.columns = dataFileSchema.getLogicalColumnNames();
		}
		else
		{
			this.columns = columns;
		}

		// verify at least one column is selected
		if (this.columns.size() == 0)
		{
			throw new IllegalArgumentException("At least one column must be specified in the select list");
		}
	}

	protected abstract Reader getRowData() throws FileNotFoundException;

	@Override
	public Iterator<FileRow> iterator() throws IOException
	{
		if (bread != null)
		{
			throw new IOException("One per costumer, please (I.E., don't call iterator() twice)");
		}

		bread = new BufferedReader(getRowData(), 16384);

		return new DataIterator();
	}

	@Override
	public void dispose() throws IOException
	{
		if (state.availablePermits() == 1)
		{
			bread.close();
		}
	}

	private class DataIterator implements Iterator<FileRow>
	{
		String nullToken;
		String columnDelimiter;

		String nextLine;
		private final Map<String, Object> lineData;
		private final Map<String, Object> publicLineData;

		public DataIterator()
		{
			nullToken = dataFileSchema.getNullToken();
			columnDelimiter = dataFileSchema.getColumnDelimiter();
			nextLine = null;
			lineData = new HashMap<String, Object>();
			publicLineData = Collections.unmodifiableMap(lineData);
		}

		public boolean hasNext()
		{
			if (state.availablePermits() == 0)
			{
				return false;
			}

			if (nextLine != null)
			{
				return true;
			}

			while (state.availablePermits() == 1)
			{
				try
				{
					nextLine = FlatFile.readLine(bread, dataFileSchema.getRowDelimiter());

					if (nextLine == null)
					{
						bread.close();
						state.acquireUninterruptibly();
					}
					else
					{
						String trim = nextLine.trim();

						if (!trim.equals("") && !(trim.startsWith("/*") && trim.endsWith("*/")) && !trim.startsWith("#"))
						{
							break;
						}
					}
				}
				catch (IOException e)
				{
					throw new RuntimeException(e);
				}
			}

			return nextLine != null;
		}

		public FileRow next()
		{
			if (nextLine == null)
			{
				throw new IllegalStateException("Next line does not exist");
			}

			// convert the line into columns
			lineData.clear();

			final DataFileSchema dataFileSchema = DataFileDataImpl.this.dataFileSchema;

			if (dataFileSchema != null)
			{
				// defer mapping columns to the schema
				lineData.putAll(dataFileSchema.validateAndSplitLine(nextLine));
			}
			else
			{
				String[] columns = nextLine.split(columnDelimiter);

				List<String> logicalColumnNames = dataFileSchema.getLogicalColumnNames();

				for (int i = 0; i < columns.length; i++)
				{
					if (nullToken.equals(columns[i]))
					{
						lineData.put(logicalColumnNames.get(i), null);
					}
					else
					{
						lineData.put(logicalColumnNames.get(i), columns[i]);
					}
				}
			}

			if (lineData.size() != dataFileSchema.getLogicalColumnNames().size())
			{
				throw new IllegalStateException("Line has the wrong number of columns.  Required["
						+ dataFileSchema.getLogicalColumnNames().size()
						+ "], actual["
						+ lineData.size()
						+ "] - source '"
						+ nextLine
						+ "'");
			}

			// remove unwanted columns - this is ugly but better than doing it twice above in two different handlers
			Set<Map.Entry<String, Object>> eSet = lineData.entrySet();

			Iterator<Map.Entry<String, Object>> eSi = eSet.iterator();

			while (eSi.hasNext())
			{
				if (!columns.contains(eSi.next().getKey()))
				{
					// get rid of it
					eSi.remove();
				}
			}

			// this method also needs to know about columns
			final OrderKey orderKey = FlatFile.addOrderKey(dataFileSchema, lineData, columns);

			nextLine = null;

			return new FileRow()
			{
				@Override
				public Map<String, Object> getData()
				{
					return publicLineData;
				}

				@Override
				public OrderKey getOrderKey()
				{
					return orderKey;
				}

				@Override
				public DataFileSchema.Column getColumn(String id) {
					return dataFileSchema.getColumn(id);
				}
			};
		}

		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}
}
