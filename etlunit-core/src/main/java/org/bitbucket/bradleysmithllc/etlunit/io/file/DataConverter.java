package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.ParseException;
import java.util.regex.Pattern;

/**
 * The data converter has the responsibility of moving data between
 * string representation and java objects.  The default converter
 * uses the JDBC type and converts the data in a way that is compatible
 * with JDBC.  If no jdbc type is specified, VARCHAR is used.
 *
 * Default JDBC type converters are used (java.sql.Data, java.sql.Timestamp, etc)
 * using the string constructors unless a format is specified on the column.
 * In the latter case, the format string will be used to construct a java object
 * using a native type (E.G.),
 * column type=TIMESTAMP gets converted using java.sql.Timestamp.valueOf(text)
 * column type=TIMESTAMP format="YYYYMMDD" gets converted using SimpleDateFormat and new Timestamp(sdf.parse(text).getTime())
 */
public interface DataConverter
{
	String format(Object data, DataFileSchema.Column column);
	Object parse(String data, DataFileSchema.Column column) throws ParseException;

	String getId();

	Pattern getPattern(DataFileSchema.Column column);

	int getJdbcType();
}
