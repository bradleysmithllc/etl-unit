package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LogListenerLogProxy extends NullClassListener implements Log
{
	private final LogListener deferred;
	private final RuntimeSupport runtimeSupport;

	private final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public LogListenerLogProxy(LogListener deferred, RuntimeSupport runtimeSupport)
	{
		this.deferred = deferred;
		this.runtimeSupport = runtimeSupport;
	}

	@Override
	public void info(String message)
	{
		deferred.log(
			LogListener.message_type.info,
			message,
			runtimeSupport.getCurrentlyProcessingTestClass(),
			runtimeSupport.getCurrentlyProcessingTestMethod(),
			runtimeSupport.getCurrentlyProcessingTestOperation()
		);
	}

	@Override
	public void debug(String message)
	{
		deferred.log(
				LogListener.message_type.debug,
				message,
				runtimeSupport.getCurrentlyProcessingTestClass(),
				runtimeSupport.getCurrentlyProcessingTestMethod(),
				runtimeSupport.getCurrentlyProcessingTestOperation()
		);
	}

	@Override
	public void severe(String message)
	{
		deferred.log(
				LogListener.message_type.severe,
				message,
				runtimeSupport.getCurrentlyProcessingTestClass(),
				runtimeSupport.getCurrentlyProcessingTestMethod(),
				runtimeSupport.getCurrentlyProcessingTestOperation()
		);
	}

	@Override
	public void severe(String message, Throwable thr)
	{
		deferred.log(
				LogListener.message_type.severe,
				thr,
				message,
				runtimeSupport.getCurrentlyProcessingTestClass(),
				runtimeSupport.getCurrentlyProcessingTestMethod(),
				runtimeSupport.getCurrentlyProcessingTestOperation()
		);
	}

	@Override
	public void suspend(boolean state)
	{
	}

	@Override
	public void begin(ETLTestClass cl, VariableContext context, int executor)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		info("Begin processing test class " + df.format(LocalDateTime.now()));
	}
}
