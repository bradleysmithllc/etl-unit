package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class RuntimeOption
{
	private String name;

	private Boolean enabled;
	private String stringValue;
	private Integer integerValue;

	private RuntimeOptionDescriptor descriptor;
	private Feature feature;

	private RuntimeOptionDescriptor.option_type type;

	public RuntimeOption()
	{
	}

	public RuntimeOption(String name)
	{
		this.name = name;
	}

	public RuntimeOption(String name, Boolean enabled)
	{
		this.name = name;
		this.enabled = enabled;

		validate();
	}

	public RuntimeOption(String name, String stringValue)
	{
		this.name = name;
		this.stringValue = stringValue;

		validate();
	}

	public RuntimeOption(String name, Integer integerValue)
	{
		this.name = name;
		this.integerValue = integerValue;

		validate();
	}

	public RuntimeOptionDescriptor getDescriptor()
	{
		return descriptor;
	}

	public RuntimeOptionDescriptor.option_type getType() {
		return type;
	}

	public void setDescriptor(RuntimeOptionDescriptor descriptor)
	{
		this.descriptor = descriptor;

		if (type == null)
		{
			type = descriptor.getOptionType();
		}
		else if (type != descriptor.getOptionType())
		{
			throw new IllegalArgumentException("Option type mismatch - " + name + " - " + descriptor.getName());
		}
	}

	public Feature getFeature()
	{
		return feature;
	}

	public void setFeature(Feature feature)
	{
		this.feature = feature;
	}

	public void setEnabled(Boolean enabled)
	{
		if (type != null && type != RuntimeOptionDescriptor.option_type.bool)
		{
			throw new IllegalArgumentException("Cannot assign boolean value to non-boolean option - " + name + " - " + type);
		}
		else
		{
			type = RuntimeOptionDescriptor.option_type.bool;
		}

		this.enabled = enabled;
	}

	@JsonProperty(value = "string-value")
	public void setStringValue(String stringValue)
	{
		if (type != null && type != RuntimeOptionDescriptor.option_type.string)
		{
			throw new IllegalArgumentException("Cannot assign string value to non-string option - " + name + " - " + type);
		}
		else
		{
			type = RuntimeOptionDescriptor.option_type.string;
		}

		this.stringValue = stringValue;
	}

	@JsonProperty(value = "integer-value")
	public void setIntegerValue(Integer integerValue)
	{
		if (type != null && type != RuntimeOptionDescriptor.option_type.integer)
		{
			throw new IllegalArgumentException("Cannot assign integer value to non-integer option - " + name + " - " + type);
		}
		else
		{
			type = RuntimeOptionDescriptor.option_type.integer;
		}

		this.integerValue = integerValue;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public boolean isEnabled()
	{
		if (type != RuntimeOptionDescriptor.option_type.bool)
		{
			throw new UnsupportedOperationException();
		}

		if (enabled == null)
		{
			throw new IllegalStateException("Uninitialized option: " + descriptor.getName());
		}

		return enabled.booleanValue();
	}

	public int getIntegerValue()
	{
		if (type != RuntimeOptionDescriptor.option_type.integer)
		{
			throw new UnsupportedOperationException();
		}

		return integerValue.intValue();
	}

	public String getStringValue()
	{
		if (type != RuntimeOptionDescriptor.option_type.string)
		{
			throw new UnsupportedOperationException();
		}

		return stringValue;
	}

	public void validate()
	{
		if (enabled != null)
		{
			type = RuntimeOptionDescriptor.option_type.bool;
		}
		else if (integerValue != null)
		{
			type = RuntimeOptionDescriptor.option_type.integer;
		}
		else if (stringValue != null)
		{
			type = RuntimeOptionDescriptor.option_type.string;
		}
	}

	public String printableValue()
	{
		RuntimeOptionDescriptor.option_type type1 = getType();

		if (type1 == null)
		{
			return null;
		}

		switch (type1)
		{
			case bool:
				return String.valueOf(enabled);
			case integer:
				return String.valueOf(integerValue);
			case string:
				return String.valueOf(stringValue);
			default:
				throw new IllegalStateException();
		}
	}
}
