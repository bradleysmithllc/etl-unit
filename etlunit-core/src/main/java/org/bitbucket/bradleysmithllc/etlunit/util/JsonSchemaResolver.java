package org.bitbucket.bradleysmithllc.etlunit.util;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;

import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class JsonSchemaResolver
{
	private static final Logger logger = LogManager.getLogger(JsonSchemaResolver.class);
	private static boolean loggerSet = false;

	public static JsonNode resolveSchemaReference(String resourcePath) throws Exception
	{
		return resolveSchemaReference(resourcePath, Thread.currentThread().getContextClassLoader());
	}

	public static JsonNode resolveSchemaReference(String resourcePath, ClassLoader loader) throws Exception
	{
		ClassLoader classLoader = ObjectUtils.firstNotNull(loader, Thread.currentThread().getContextClassLoader());

		URL url = classLoader.getResource(resourcePath);

		if (url == null)
		{
			throw new IllegalArgumentException("Could not find resource " + resourcePath);
		}

		logger.debug("Resource {" + resourcePath + "} loaded through classloader {" + loader + "} located at URL {" + url.toExternalForm() + "}");

		JsonNode node = JsonLoader.fromURL(url);

		return resolveRefs(node, classLoader);
	}

	public static JsonNode resolveSchemaReference(URL resourcePath) throws Exception
	{
		return resolveSchemaReference(resourcePath, Thread.currentThread().getContextClassLoader());
	}

	public static JsonNode resolveSchemaReference(URL resourcePath, ClassLoader loader) throws Exception
	{
		JsonNode node = JsonLoader.fromURL(resourcePath);

		return resolveRefs(node, loader);
	}

	public static JsonNode resolveSchemaReference(JsonNode jNode) throws Exception
	{
		return resolveRefs(jNode, Thread.currentThread().getContextClassLoader());
	}

	public static JsonNode resolveSchemaReference(JsonNode jNode, ClassLoader loader) throws Exception
	{
		return resolveRefs(jNode, loader);
	}

	/**
	 * Read through this object and replace all $ref properties with the external document
	 *
	 * @param schema
	 * @param loader
	 * @return
	 */
	private static JsonNode resolveRefs(JsonNode schema, ClassLoader loader) throws Exception
	{
		ObjectMapper om = new ObjectMapper();

		schema = resolveObject((ObjectNode) schema, om, loader);

		return schema;
	}

	private static ObjectNode resolveObject(ObjectNode schema, ObjectMapper mapper, ClassLoader loader) throws Exception
	{
		JsonNode $ref = schema.get("$ref");
		JsonNode $patch = schema.get("$patch");
		JsonNode $with = schema.get("$with");

		// check for conflicting patch and ref
		if ($ref != null && $patch != null)
		{
			throw new IllegalStateException("Cannot specify $ref and $patch");
		}
		// check for conflicting $ref and $with
		else if ($ref != null && $with != null)
		{
			throw new IllegalStateException("Cannot specify $ref and $with");
		}
		// check for conflicting $with without $patch
		else if ($with != null && $patch == null)
		{
			throw new IllegalStateException("Cannot specify $with without $patch");
		}
		// check for conflicting $patch without $with
		else if ($with == null && $patch != null)
		{
			throw new IllegalStateException("Cannot specify $patch without $with");
		}
		// check for a patch
		else if ($patch != null)
		{
			// $patch must be textual
			if (!$patch.isTextual())
			{
				throw new IllegalArgumentException("$patch nodes must be textual");
			}

			// check that $with must be an object
			if (!$with.isObject())
			{
				throw new IllegalArgumentException("$with must be an object type: " + $with);
			}

			// resolve $patch reference
			JsonNode extPatchNode = resolveObject(getExternalNode(mapper, loader, $patch), mapper, loader);

			if (extPatchNode == null)
			{
				throw new IllegalArgumentException("Node to patch not found: " + $patch);
			}

			JsonNode resolvedWithNode = resolveObject((ObjectNode) $with, mapper, loader);

			// before passing it on, do a left-merge with the $with node as the left value
			return (ObjectNode) JsonUtils.merge(resolvedWithNode, extPatchNode, JsonUtils.merge_type.left_merge);
		}
		// check for a $ref member in the object.
		// if so, substitute with the reference
		else if ($ref != null)
		{
			// $ref must be textual
			if (!$ref.isTextual())
			{
				throw new IllegalArgumentException("$ref nodes must be textual");
			}
			return getExternalNode(mapper, loader, $ref);
		}
		else
		{
			Iterator<Map.Entry<String, JsonNode>> it = schema.fields();

			while (it.hasNext())
			{
				Map.Entry<String, JsonNode> node = it.next();

				JsonNode nodeValue = node.getValue();
				if (nodeValue.isObject())
				{
					ObjectNode onode = (ObjectNode) nodeValue;

					nodeValue = resolveObject(onode, mapper, loader);
				}

				schema.put(node.getKey(), nodeValue);
			}
		}

		return schema;
	}

	private static ObjectNode getExternalNode(ObjectMapper mapper, ClassLoader loader, JsonNode $ref) throws Exception
	{
		URI url = new URI($ref.asText());

		if (url.getScheme().equals("resource"))
		{
			// insist that the 'host' on this URI must be 'classpath'
			if (!url.getHost().equals("classpath"))
			{
				throw new IllegalArgumentException("Resource URI $ref's must use 'classpath' as the host name: " + url);
			}

			String path = url.getPath();

			// make sure to strip of any leading '/'
			if (path.startsWith("/"))
			{
				path = path.substring(1);
			}

			JsonNode node = resolveSchemaReference(path, loader);

			if (!node.isObject())
			{
				throw new IllegalArgumentException("Ref points to non-object node");
			}

			// place this object into the result
			return resolveObject((ObjectNode) node, mapper, loader);
		}
		else
		{
			JsonNode node = resolveSchemaReference(url.toURL(), loader);

			if (!node.isObject())
			{
				throw new IllegalArgumentException("Ref points to non-object node");
			}

			// place this object into the result
			return resolveObject((ObjectNode) node, mapper, loader);
		}
	}
}
