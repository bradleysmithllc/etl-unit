package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.util.HashMapArrayList;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;

import java.util.List;

public class ClassAndTestNamesDirectorImpl extends NullClassDirector
{
	private final MapList<String, String> suiteNames = new HashMapArrayList<String, String>();

	private static final String ALL_METHODS = new String();

	public List<String> addClassName(String name)
	{
		return suiteNames.getOrCreate(name);
	}

	public void addClassNameWildcard(String name)
	{
		suiteNames.getOrCreate(name).add(ALL_METHODS);
	}

	@Override
	public response_code accept(ETLTestClass cl) {
		return suiteNames.containsKey(cl.getQualifiedName()) ? response_code.accept : response_code.reject;
	}

	@Override
	public response_code accept(ETLTestMethod mt) {
		List<String> methodNames = suiteNames.get(mt.getTestClass().getQualifiedName());

		if (methodNames != null && methodNames.size() != 0)
		{
			if (methodNames.get(0) == ALL_METHODS)
			{
				return response_code.accept;
			}
			else
			{
				return methodNames.contains(mt.getName()) ? response_code.accept : response_code.reject;
			}
		}

		return response_code.reject;
	}

	@Override
	public response_code accept(ETLTestOperation op) {
		return response_code.accept;
	}
}
