package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DataFileSchema
{
	String getPKId();

	/**
	 * The list of schemas which get to the root schema.  lineage()[0] will never be
	 * a view, and lineage()[n - 1] will always be this object.  In the case of a
	 * schema or materialized view, lineage().length will always be 1 and contain
	 * only this object.
	 * @return
	 */
	List<DataFileSchema> lineage();

	Column getColumn(String name);
	boolean hasColumn(String name);

	void addColumns(List<Column> columns);

	void addOrderColumns(List<Column> name);

	void addKeyColumns(List<Column> keyColumns);

	TemporaryTable createTemporaryDbTable(Connection embeddedDatabase) throws SQLException;

	String createTemporaryDbTableName();
	String createInsertSql(String name);
	String createTemporaryDbTableName(long timeStamp, int instance);

	String createTemporaryDbTableSql(String name);

	String createSelectSql(String name);

	DataFileManager getDataFileManager();

	List<String> getReadOnlyColumnNames();

	void clearOrderColumns();

	interface Column extends Cloneable
	{
		Column clone();

		/**
		 * The scale, or -1 if there is none.
		 * @return
		 */
		int getScale();

		void setScale(int scale);

		SchemaColumn.DefaultSequence getDefaultSequence();

		SchemaColumn.DefaultSequence populateDefaultSequence();

		void setLenientFormatter(boolean lenient);

		enum diff_type
		{
			date,
			timestamp,
			time,
			none
		}

		diff_type diffType();

		enum basic_type
		{
			string,
			numeric,
			integer
		}

		basic_type getBasicType();

		void setTypeAnnotation(String typeAnnotation);

		void setBasicType(basic_type type);

		boolean validateText(String text);

		String getId();

		void setOffset(int offset);

		int getOffset();

		boolean hasTypeAnnotation();

		String getTypeAnnotation();

		boolean hasType();

		/**
		 *
		 * @return
		 */
		String getFormat();

		void setFormat(String format);
		boolean isFormatterLenient();

		String getType();
		DataConverter getConverter();

		int jdbcType();

		void setType(String type);

		/**
		 * The column length, use -1 for none.
		 * @return
		 */
		void setLength(int length);

		/**
		 * The column length, or -1 for none.
		 * @return
		 */
		int getLength();

		Object getDefaultValue();

		Object generateColumnValue(Map<String, Object> columnValues, int rowSequence);

		/**
		 * Denotes that this value should not be written to the output file.
		 * @return
		 */
		boolean isReadOnly();

		int ordinal();
		int inferredOrdinal();
		void setInferredOrdinal(int ord);
		int actualOrdinal();
	}

	enum format_type
	{
		delimited,
		fixed
	}

	String getId();

	DataFileSchemaView createSubViewIncludingColumns(List<String> columns, String id, format_type format);

	DataFileSchemaView createSubViewIncludingColumns(List<String> columns, String id);

	DataFileSchemaView createSubViewExcludingColumns(List<String> columns, String id, format_type format);

	DataFileSchemaView createSubViewExcludingColumns(List<String> columns, String id);

	DataFileSchemaView intersect(DataFileSchema that, String id, format_type format);

	DataFileSchemaView intersect(DataFileSchema that, String id);

	String toJsonString();

	void setColumnDelimiter(String delimiter);

	void setRowDelimiter(String delimiter);

	void setNullToken(String token);

	void setFormatType(DataFileSchema.format_type type);

	String getColumnDelimiter();

	String getRowDelimiter();

	String getNullToken();
	boolean escapeNonPrintable();
	void setEscapeNonPrintable(boolean esc);

	DataFileSchema.format_type getFormatType();

	Map<String, Object> validateAndSplitLine(String line) throws DataFileMismatchException;

	Column createColumn(String id);

	void addColumn(Column column);

	void addKeyColumn(String name);

	void addOrderColumn(String name);

	void setKeyColumns(List<String> names);

	void setOrderColumns(List<String> names);

	List<Column> getLogicalColumns();
	List<String> getLogicalColumnNames();

	List<Column> getPhysicalColumns();

	List<Column> getOrderColumns();

	List<String> getOrderColumnNames();

	List<Column> getKeyColumns();

	List<String> getKeyColumnNames();

	boolean isView();
}
