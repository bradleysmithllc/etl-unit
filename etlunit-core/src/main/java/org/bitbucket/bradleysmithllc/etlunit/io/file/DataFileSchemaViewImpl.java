package org.bitbucket.bradleysmithllc.etlunit.io.file;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.ObjectUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class DataFileSchemaViewImpl implements DataFileSchemaView {
	private final DataFileSchema dataFileSchema;
	private final List<String> columnNames = new ArrayList<String>();
	private final List<Column> columns = new ArrayList<Column>();
	private final Map<String, Column> columnMap = new HashMap<String, Column>();
	List<DataFileSchema> lineage;
	private final String thisId;

	private format_type thisFormatType;
	private String thisNullToken;
	private String thisRowDelimiter;
	private String thisColumnDelimiter;

	public DataFileSchemaViewImpl(List<String> columnNs, DataFileSchema dataFileSchema, String id, format_type format) {
		this.dataFileSchema = dataFileSchema;
		lineage = new ArrayList<>(dataFileSchema.lineage());
		lineage.add(this);
		thisId = id;
		thisFormatType = format;

		// check for switch from fixed to delimited
		if (format == format_type.delimited && dataFileSchema.getFormatType() == format_type.fixed) {
			thisColumnDelimiter = dataFileSchema.getDataFileManager().getDefaultColumnDelimiter();
		}

		if (columnNs != null) {
			List<String> alist = new ArrayList<String>(columnNs);

			for (Column col : dataFileSchema.getLogicalColumns()) {
				String id1 = col.getId();
				if (alist.contains(id1)) {
					columnNames.add(id1);
					columns.add(col);
					columnMap.put(id1.toLowerCase(), col);

					alist.remove(id1);
				}
			}

			if (alist.size() != 0)
			{
				throw new IllegalArgumentException("Data File Schema " + dataFileSchema.getId() + " does not have columns specified in view: " + alist);
			}
		} else {
			columnNames.addAll(dataFileSchema.getLogicalColumnNames());
			columns.addAll(dataFileSchema.getLogicalColumns());

			for (Column col : columns)
			{
				columnMap.put(col.getId().toLowerCase(), col);
			}
		}

		if (columns.size() == 0) {
			throw new IllegalArgumentException("View contains no columns");
		}
	}

	@Override
	public String getPKId() {
		return dataFileSchema.getPKId();
	}

	@Override
	public List<DataFileSchema> lineage() {
		return lineage;
	}

	@Override
	public Column getColumn(String name) {
		return columnMap.get(name.toLowerCase());
	}

	@Override
	public boolean hasColumn(String name) {
		return columnMap.get(name.toLowerCase()) != null;
	}

	@Override
	public void addColumns(List<Column> columns) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addOrderColumns(List<Column> name) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addKeyColumns(List<Column> keyColumns) {
		throw new UnsupportedOperationException();
	}

	@Override
	public TemporaryTable createTemporaryDbTable(Connection embeddedDatabase) throws SQLException {
		return DataFileSchemaImpl.createTemporaryDbTableImpl(this, embeddedDatabase);
	}

	@Override
	public String createTemporaryDbTableName() {
		return DataFileSchemaImpl.createTemporaryDbTableNameImpl(this);
	}

	@Override
	public String createInsertSql(String name) {
		return DataFileSchemaImpl.createInsertSqlImpl(name, this);
	}

	@Override
	public String createTemporaryDbTableName(long timeStamp, int instance) {
		return DataFileSchemaImpl.createTemporaryDbTableNameImpl(this, timeStamp, instance);
	}

	private List<Column> getCombinedColumns()
	{
		// this must include all pk columns in the select columns list or else the pk will be invalid
		List<Column> columnList = new ArrayList<Column>(dataFileSchema.getLogicalColumns());

		Iterator<Column> colIt = columnList.iterator();

		List<String> keyColumns = getKeyColumnNames();
		List<String> columnList1 = getLogicalColumnNames();

		while (colIt.hasNext())
		{
			// preserve my cols plus any pk cols
			Column next = colIt.next();
			String id = next.getId();
			if (columnList1.contains(id) || keyColumns.contains(id))
			{
			}
			else
			{
				colIt.remove();
			}
		}

		return columnList;
	}

	@Override
	public String createTemporaryDbTableSql(String name) {
		return DataFileSchemaImpl.createTemporaryDbTableSqlImpl(name, this);
	}

	@Override
	public String createSelectSql(String name) {
		return DataFileSchemaImpl.createSelectSqlImpl(name, this);
	}

	@Override
	public String getId() {
		return thisId;
	}

	@Override
	public DataFileSchemaView createSubViewIncludingColumns(List<String> columns, String id, format_type format) {
		return new DataFileSchemaViewImpl(columns, this, id, format);
	}

	@Override
	public DataFileSchemaView createSubViewIncludingColumns(List<String> columns, String id) {
		return createSubViewIncludingColumns(columns, id, null);
	}

	@Override
	public DataFileSchemaView createSubViewExcludingColumns(List<String> columns, String id, format_type format) {
		List<String> outerColumns = new ArrayList<String>();

		for (String innerCol : columnNames) {
			for (String outerCol : columns) {
				if (!innerCol.equals(outerCol)) {
					outerColumns.add(innerCol);
				}
			}
		}

		return createSubViewIncludingColumns(outerColumns, id, format);
	}

	@Override
	public DataFileSchemaView createSubViewExcludingColumns(List<String> columns, String id) {
		return createSubViewExcludingColumns(columns, id, null);
	}

	@Override
	public DataFileSchemaView intersect(DataFileSchema that, String id, format_type format) {
		return createSubViewIncludingColumns(that.getLogicalColumnNames(), id, format);
	}

	@Override
	public DataFileSchemaView intersect(DataFileSchema that, String id) {
		return intersect(that, id, null);
	}

	@Override
	public String toJsonString() {
		return dataFileSchema.toJsonString();
	}

	@Override
	public void setColumnDelimiter(String delimiter) {
		if (thisFormatType == format_type.fixed) {
			throw new IllegalArgumentException("Fixed-width files do not have column delimiters");
		}

		thisColumnDelimiter = delimiter;
	}

	@Override
	public void setRowDelimiter(String delimiter) {
		thisRowDelimiter = delimiter;
	}

	@Override
	public void setNullToken(String token) {
		thisNullToken = token;
	}

	@Override
	public void setFormatType(format_type type) {
		thisFormatType = type;
	}

	@Override
	public String getColumnDelimiter() {
		return ObjectUtils.firstNonNull(thisColumnDelimiter, dataFileSchema.getColumnDelimiter());
	}

	@Override
	public String getRowDelimiter() {
		return ObjectUtils.firstNonNull(thisRowDelimiter, dataFileSchema.getRowDelimiter());
	}

	@Override
	public String getNullToken() {
		return ObjectUtils.firstNonNull(thisNullToken, dataFileSchema.getNullToken());
	}

	@Override
	public boolean escapeNonPrintable() {
		return dataFileSchema.escapeNonPrintable();
	}

	@Override
	public void setEscapeNonPrintable(boolean esc) {
		throw new UnsupportedOperationException();
	}

	@Override
	public format_type getFormatType() {
		return ObjectUtils.firstNonNull(thisFormatType, dataFileSchema.getFormatType());
	}

	@Override
	public Map<String, Object> validateAndSplitLine(String line) throws DataFileMismatchException {
		return dataFileSchema.validateAndSplitLine(line);
	}

	@Override
	public Column createColumn(String id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addColumn(Column column) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addKeyColumn(String name) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addOrderColumn(String name) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setKeyColumns(List<String> names) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setOrderColumns(List<String> names) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Column> getLogicalColumns() {
		return columns;
	}

	@Override
	public List<String> getLogicalColumnNames() {
		return columnNames;
	}

	@Override
	public List<Column> getPhysicalColumns() {
		return getCombinedColumns();
	}

	@Override
	public List<Column> getOrderColumns() {
		List<Column> cols = new ArrayList<Column>(dataFileSchema.getOrderColumns());

		Iterator<Column> it = cols.iterator();

		while (it.hasNext())
		{
			if (!columnMap.containsKey(it.next().getId().toLowerCase()))
			{
				it.remove();
			}
		}

		return cols;
	}

	@Override
	public List<String> getOrderColumnNames() {
		List<String> cols = new ArrayList<String>();

		for (Column col : getOrderColumns())
		{
			cols.add(col.getId());
		}

		return cols;
	}

	@Override
	public List<Column> getKeyColumns() {
		return dataFileSchema.getKeyColumns();
	}

	@Override
	public List<String> getKeyColumnNames() {
		return dataFileSchema.getKeyColumnNames();
	}

	@Override
	public boolean isView() {
		return true;
	}

	@Override
	public DataFileSchema materialize() {
		return ((DataFileSchemaImpl) lineage.get(0)).createSubViewIncludingColumnsImpl(
			columnNames,
			getId(),
			dataFileSchema.getId(),
			getFormatType(),
			escapeNonPrintable(),
			getRowDelimiter(),
			getColumnDelimiter(),
			getNullToken(),
			getLogicalColumns(),
			getKeyColumns(),
			getOrderColumns(),
			dataFileSchema.getDataFileManager()
		);
	}

	@Override
	public DataFileManager getDataFileManager() {
		return dataFileSchema.getDataFileManager();
	}

	@Override
	public List<String> getReadOnlyColumnNames() {
		List<String> readOnlyColNames = new ArrayList<String>();
		List<Column> cols = getPhysicalColumns();

		Iterator<Column> it = cols.iterator();

		while (it.hasNext())
		{
			Column col = it.next();

			if (col.isReadOnly())
			{
				readOnlyColNames.add(col.getId());
			}
		}

		return readOnlyColNames;
	}

	@Override
	public void clearOrderColumns() {
		dataFileSchema.clearOrderColumns();
	}
}
