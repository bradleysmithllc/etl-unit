package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestVariable;

public class ContextFeatureModule extends AbstractFeature
{
	private final class ContextListener extends NullClassListener
	{
		@Override
		public void declare(ETLTestVariable var, VariableContext context, int executor)
		{
			// just pass on the declare to the context
			context.declareVariable(var.getName());

			// set the value
			context.setValue(var.getName(), var.getValue());
		}

		// Provide a set variable method
		@Override
		public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor)
				throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			if (op.getOperationName().equals("set"))
			{
				// variable value
				ETLTestValueObject value = obj.query("value");
				ETLTestValueObject variable = obj.query("variable");

				// in case somebody else wants it . . .
				if (value == null || variable == null)
				{
					return action_code.defer;
				}

				// contextualize the message
				String resultingValue = context.contextualize(value.getValueAsString());

				context.declareAndSetStringValue(variable.getValueAsString(), resultingValue);

				return action_code.handled;
			}

			return action_code.defer;
		}
	}

	private final ContextListener contextListener = new ContextListener();

	@Override
	public String getFeatureName()
	{
		return "context";
	}

	@Override
	public ClassListener getListener()
	{
		return contextListener;
	}

	// this should be pretty high on the priority chain
	@Override
	public long getPriorityLevel() {
		return -100L;
	}
}
