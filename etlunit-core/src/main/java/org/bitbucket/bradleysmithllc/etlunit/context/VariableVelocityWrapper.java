package org.bitbucket.bradleysmithllc.etlunit.context;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class VariableVelocityWrapper implements Map<String, Object>
{
	private final ETLTestValueObject qcontext;
	private final VariableContext vcontext;

	public VariableVelocityWrapper(VariableContext variableContext)
	{
		qcontext = null;
		vcontext = variableContext;
	}

	public VariableVelocityWrapper(ETLTestValueObject variableContext)
	{
		qcontext = variableContext;
		vcontext = null;
	}

	@Override
	public int size()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isEmpty()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsKey(Object o)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsValue(Object o)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object get(Object o)
	{
		String name = String.valueOf(o);

		ETLTestValueObject value = null;

		if (qcontext != null)
		{
			value = qcontext.query(name);
		}
		else
		{
			try
			{
				value = vcontext.getValue(name);
			}
			catch (IllegalArgumentException arg)
			{
				// trap this to allow velocity to test variables
			}
		}

		if (value != null)
		{
			switch (value.getValueType())
			{
				case literal:
				case quoted_string:
					return value.getValueAsString();
				case object:
					return new VariableVelocityWrapper(value);
				case list:
					// TODO: What to do for lists??
					throw new UnsupportedOperationException("I can't handle lists yet.  Please fix that");
				case pojo:
					// don't wrap pojos
					return value.getValueAsPojo();
			}
		}

		return null;
	}

	@Override
	public Object put(String s, Object o)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object remove(Object o)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> map)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<String> keySet()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Object> values()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<Entry<String, Object>> entrySet()
	{
		throw new UnsupportedOperationException();
	}

	public String toString()
	{
		StringBuilder stb = new StringBuilder();

		stb.append("{");
		for (Entry<String, Object> entry : entrySet())
		{
			stb.append(entry.getKey());
			stb.append(": '");
			stb.append(entry.getValue().toString());
			stb.append("'");
		}
		stb.append("}");

		return stb.toString();
	}
}
