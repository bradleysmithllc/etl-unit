package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import org.bitbucket.bradleysmithllc.etlunit.ExecutionContext;
import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.TestWarning;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ResourceFeatureOperation implements FeatureOperation
{
	private final String name;
	private final String description;
	private final ResourceFeatureMetaInfo resourceFeatureMetaInfo;
	private final List<String> signatures;
	private final List<FeatureOperationSignature> operationSignatures = new ArrayList<FeatureOperationSignature>();

	public static final JsonNode NULL_NODE;

	static
	{
		try
		{
			NULL_NODE = JsonLoader.fromString("{}");
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	public ResourceFeatureOperation(ResourceFeatureMetaInfo resourceFeatureMetaInfo, String key, String description, List<String> signatur)
	{
		name = key;
		this.description = description;
		this.resourceFeatureMetaInfo = resourceFeatureMetaInfo;
		this.signatures = signatur;

		for (String sig : signatures)
		{
			operationSignatures.add(new ResourceFeatureOperationSignature(this, resourceFeatureMetaInfo, sig));
		}
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public String getDescription()
	{
		return description;
	}

	@Override
	public String getUsage()
	{
		String val = resourceFeatureMetaInfo.getResource(name + ".usage");

		return val;
	}

	@Override
	public String getPrototype()
	{
		String val = resourceFeatureMetaInfo.getResource(name + ".prototype");

		return val;
	}

	@Override
	public List<FeatureOperationSignature> getSignatures()
	{
		return operationSignatures;
	}

	@Override
	public action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		if (name.equals(op.getOperationName()))
		{
			for (FeatureOperationSignature sig : operationSignatures)
			{
				JsonSchema schema = sig.getValidator();

				if (schema != null)
				{
					resourceFeatureMetaInfo.getApplicationLog().info("Verifying operation request '" + op.getOperationName() + "' against the schema signature '" + resourceFeatureMetaInfo.getDescribing().getFeatureName() + "." + sig.getId() + "'");

					try
					{
						ProcessingReport report = schema.validate(parameters != null ? parameters.getJsonNode() : NULL_NODE);

						if (report.isSuccess())
						{
							return sig.process(mt, op, parameters, vcontext, econtext, executor);
						}
						else
						{
							//System.out.println(report.toString());
						}
					}
					catch (ProcessingException e)
					{
						// didn't validate - pass on to the next one
					}
				}
				else
				{
					// no schema, this one wins
					return sig.process(mt, op, parameters, vcontext, econtext, executor);
				}
			}
		}

		return action_code.defer;
	}
}
