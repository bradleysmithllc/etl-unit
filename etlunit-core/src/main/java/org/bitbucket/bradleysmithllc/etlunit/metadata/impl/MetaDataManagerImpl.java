package org.bitbucket.bradleysmithllc.etlunit.metadata.impl;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaData;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

public class MetaDataManagerImpl implements MetaDataManager
{
	private RuntimeSupport runtimeSupport;
	private MetaDataImpl metaData = new MetaDataImpl();

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
		metaData.receiveRuntimeSupport(runtimeSupport);
	}

	@Override
	public JsonNode toJson(MetaData data) {
		StringWriter writer = new StringWriter();
		JsonWriter jwriter = new JsonWriter(writer);

		try {
			jwriter.beginObject();
			data.toJson(jwriter);
			jwriter.endObject();

			jwriter.close();
			return JsonLoader.fromString(writer.toString());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized MetaData fromJson(JsonNode node) {
		MetaDataImpl mdimpl = new MetaDataImpl();
		mdimpl.receiveRuntimeSupport(runtimeSupport);
		mdimpl.fromJson(node);

		return mdimpl;
	}

	@Override
	public MetaData getMetaData() {
		return metaData;
	}

	@Override
	public synchronized void dispose() {
		// write out meta data json catalog
		File tgt = runtimeSupport.createGeneratedSourceFile("metadata", "meta-data.json");

		JsonNode jnode = toJson(metaData);

		try {
			FileUtils.write(tgt, jnode.toString());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
