package org.bitbucket.bradleysmithllc.etlunit.feature.logging;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

import java.io.File;

public class LogFileImpl implements LogFileManager.LogFile
{
	private final String message;
	private final File cachedLog;
	private final File sourceLog;
	private final String classifier;

	private final ETLTestClass testClass;
	private final ETLTestMethod testMethod;
	private final ETLTestOperation testOperation;

	public LogFileImpl(
			String message,
			String classifier,
			File cachedLog,
			File sourceLog,
			ETLTestClass testClass,
			ETLTestMethod testMethod,
			ETLTestOperation testOperation
										)
	{
		this.message = message;
		this.classifier = classifier;
		this.cachedLog = cachedLog;
		this.sourceLog = sourceLog;
		this.testClass = testClass;
		this.testMethod = testMethod;
		this.testOperation = testOperation;
	}

	@Override
	public String getMessage()
	{
		return message;
	}

	@Override
	public File getFile()
	{
		return cachedLog;
	}

	@Override
	public File getOriginalPath()
	{
		return sourceLog;
	}

	@Override
	public ETLTestOperation getTestOperation()
	{
		return testOperation;
	}

	@Override
	public ETLTestMethod getTestMethod()
	{
		return testMethod;
	}

	@Override
	public ETLTestClass getTestClass()
	{
		return testClass;
	}

	public String getClassifier() {
		return classifier;
	}
}
