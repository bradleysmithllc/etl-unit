package org.bitbucket.bradleysmithllc.etlunit.metadata.impl;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.metadata.TestReference;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;

import java.io.IOException;

public class TestReferenceImpl implements TestReference
{
	private String classification;
	private ETLTestPackage referencingPackage;
	private String referencingTestClass;
	private String referencingTestName;
	private String qualifiedReferencingTestName;

	public TestReferenceImpl() {
	}

	public TestReferenceImpl(String classification, ETLTestPackage referencingPackage, String referencingTestClass, String referencingTestName, String qName) {
		this.classification = classification;

		if (referencingPackage != null && !referencingPackage.equals(""))
		{
			this.referencingPackage = referencingPackage;
		}

		this.referencingTestClass = referencingTestClass;
		this.referencingTestName = referencingTestName;
		qualifiedReferencingTestName = qName;
	}

	@Override
	public String getClassification() {
		return classification;
	}

	@Override
	public ETLTestPackage getReferencingTestPackage() {
		return referencingPackage;
	}

	@Override
	public String getReferencingTestClass() {
		return referencingTestClass;
	}

	@Override
	public String getReferencingTestName() {
		return referencingTestName;
	}

	@Override
	public String getQualifiedTestName() {
		return qualifiedReferencingTestName;
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("classification").value(classification);

		writer.name("package");

		if (referencingPackage.isDefaultPackage())
		{
			writer.nullValue();
		}
		else
		{
			writer.value(referencingPackage.getPackageId());
		}
		writer.name("class").value(referencingTestClass);
		writer.name("method").value(referencingTestName);

		writer.name("qualified-test-name").value(qualifiedReferencingTestName);
	}

	@Override
	public synchronized void fromJson(JsonNode node) {
		classification = node.get("classification").asText();
		referencingTestName = node.get("method").asText();
		referencingTestClass = node.get("class").asText();
		qualifiedReferencingTestName = node.get("qualified-test-name").asText();

		JsonNode aPackage = node.get("package");
		if (aPackage != null && !aPackage.isNull())
		{
			referencingPackage = ETLTestPackageImpl.getDefaultPackage().getSubPackage(aPackage.asText());
		}
		else
		{
			referencingPackage = ETLTestPackageImpl.getDefaultPackage();
		}
	}

	public boolean equals(Object reference)
	{
		if (reference.getClass() != getClass())
		{
			return false;
		}

		TestReferenceImpl oth = (TestReferenceImpl) reference;

		return
			ObjectUtils.compare(classification, oth.classification) == 0 &&
			ObjectUtils.compare(qualifiedReferencingTestName, oth.qualifiedReferencingTestName) == 0 &&
			ObjectUtils.compare(referencingPackage, oth.referencingPackage) == 0 &&
			ObjectUtils.compare(referencingTestClass, oth.referencingTestClass) == 0 &&
			ObjectUtils.compare(referencingTestName, oth.referencingTestName) == 0;
	}

	@Override
	public int compareTo(TestReference o) {
		// return true if the fully-qualified test reference matches.  We don't consider the classification here

		return qualifiedReferencingTestName.compareTo(o.getQualifiedTestName());
	}
}
