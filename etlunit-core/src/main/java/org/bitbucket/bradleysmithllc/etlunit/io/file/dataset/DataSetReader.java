package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.EOFException;
import java.io.FilterReader;
import java.io.IOException;
import java.io.PushbackReader;

/**
 * Takes a source stream and reads until the next token is encountered
 */
public class DataSetReader extends FilterReader
{
	private final PushbackReader pushbackReader;
	private boolean closed;

	public DataSetReader(PushbackReader reader) {
		super(reader);
		this.pushbackReader = reader;
	}

	@Override
	public int read() throws IOException {
		if (closed)
		{
			return -1;
		}

		// check for the token reached
		for (int i = 0; i < ReaderDataSet.tokenArray.length; i++)
		{
			int ch = readRequired();

			if (ch != ReaderDataSet.tokenArray[i])
			{
				if (i != 0)
				{
					// put it back along with the matched part of the token
					pushbackReader.unread(ch);
					pushbackReader.unread(ReaderDataSet.tokenArray, 0, i);

					// return the first char 'unread'
					return readRequired();
				}
				else
				{
					return ch;
				}
			}
		}

		// at EOF
		closed = true;

		return -1;
	}

	private int readRequired() throws IOException {
		int ch = pushbackReader.read();

		if (ch == -1)
		{
			throw new EOFException("Stream ended before token was reached");
		}

		return ch;
	}

	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		if (len == 0)
		{
			throw new IllegalArgumentException("Length may not be 0");
		}
		else if (cbuf == null)
		{
			throw new IllegalArgumentException("Buffer cannot be null");
		}
		else if (off >= cbuf.length)
		{
			throw new IllegalArgumentException("Offset cannot be greater than length of array");
		}
		else if (len > cbuf.length)
		{
			throw new IllegalArgumentException("Length cannot be greater than length of array");
		}
		else if(off + len > cbuf.length)
		{
			throw new IllegalArgumentException("Offset + length cannot be greater than length of array");
		}

		int read = 0;

		for (int i = off; i < len; i++)
		{
			int r = read();

			if (r != -1)
			{
				read++;

				cbuf[i] = (char) r;
			}
			else
			{
				break;
			}
		}

		return read == 0 ? -1 : read;
	}

	@Override
	public long skip(long n) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean ready() throws IOException {
		return pushbackReader.ready();
	}

	@Override
	public boolean markSupported() {
		return false;
	}

	@Override
	public void mark(int readAheadLimit) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void reset() throws IOException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Close on this class means to skip to the end.
	 * @throws IOException
	 */
	@Override
	public void close() throws IOException {
		while (read() != -1)
		{}
	}

	public boolean atEOF() {
		return closed;
	}
}
