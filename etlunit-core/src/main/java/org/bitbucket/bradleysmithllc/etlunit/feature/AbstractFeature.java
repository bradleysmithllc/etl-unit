package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class AbstractFeature implements Feature
{
	protected Configuration configuration;
	protected Log applicationLog;
	protected Log userLog;

	private Injector injector;
	protected ETLTestValueObject featureConfiguration;
	private ResourceFeatureMetaInfo metaInfo = new ResourceFeatureMetaInfo(this);

	private List<String> supportedFolderNames;

	@Override
	public void dispose()
	{
		// by default features don't need disposing.  If they do they must override
		// this method.
	}

	@Override
	public final List<String> getTestSupportFolderNames()
	{
		if (supportedFolderNames == null)
		{
			supportedFolderNames = getSupportedFolderNamesSub();
		}

		return supportedFolderNames;
	}

	protected List<String> getSupportedFolderNamesSub()
	{
		return Collections.EMPTY_LIST;
	}

	@Inject
	@Override
	public void setApplicationLog(@Named("applicationLog") Log log)
	{
		applicationLog = log;
	}

	@Inject
	@Override
	public void setUserLog(@Named("userLog") Log log)
	{
		userLog = log;
	}

	@Override
	public void setFeatureConfiguration(ETLTestValueObject obj)
	{
		featureConfiguration = obj;
	}

	@Override
	public ETLTestValueObject getFeatureConfiguration()
	{
		return featureConfiguration;
	}

	@Inject
	@Override
	public void setConfiguration(Configuration conf)
	{
		configuration = conf;
	}

	@Override
	public StatusReporter getStatusReporter()
	{
		return null;
	}

	public boolean isInjected()
	{
		return configuration != null;
	}

	protected <T> T postCreate(T object)
	{
		if (injector == null)
		{
			throw new IllegalStateException("Feature does not have a valid injector");
		}

		injector.injectMembers(object);

		return object;
	}

	protected void useInjector(Injector inj)
	{
	}

	@Override
	public ClassDirector getDirector()
	{
		return null;
	}

	@Override
	public ClassLocator getLocator()
	{
		return null;
	}

	public ClassListener getListener()
	{
		return null;
	}

	@Override
	public List<ClassListener> getListenerList()
	{
		ClassListener listener = getListener();

		if (listener != null)
		{
			return Arrays.asList(new ClassListener[]{listener});
		}

		return Collections.EMPTY_LIST;
	}

	@Override
	public List<String> getPrerequisites()
	{
		return Collections.emptyList();
	}

	protected Injector preCreateSub(Injector inj)
	{
		return null;
	}

	@Override
	public final Injector preCreate(Injector inj)
	{
		useInjector(inj);

		injector = inj;

		Injector injSub = preCreateSub(inj);

		if (injSub != null)
		{
			inj = injSub;
		}

		injector = inj;

		postCreate(metaInfo);

		return inj;
	}

	@Override
	public void initialize(Injector inj)
	{
	}

	@Override
	public String toString()
	{
		return "AbstractFeature{" +
				"name='" + getFeatureName() +
				"'}";
	}

	@Override
	public FeatureMetaInfo getMetaInfo()
	{
		return metaInfo;
	}

	@Override
	public LogListener getLogListener()
	{
		return null;
	}

	@Override
	public long getPriorityLevel()
	{
		return 0L;
	}

	@Override
	public String getFeatureName()
	{
		return getMetaInfo().getFeatureName();
	}

	@Override
	public boolean requiredForSimulation() {
		return false;
	}
}
