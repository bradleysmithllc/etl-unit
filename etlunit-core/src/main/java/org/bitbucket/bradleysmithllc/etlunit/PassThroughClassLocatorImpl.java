package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PassThroughClassLocatorImpl implements ClassLocator
{
	private final List<ETLTestClass> classes;
	private Iterator<ETLTestClass> it;

	public PassThroughClassLocatorImpl(List<ETLTestClass> classes)
	{
		this.classes = classes;

		// sort to guarantee ordering
		Collections.sort(classes);

		reset();
	}

	@Override
	public boolean hasNext()
	{
		return it.hasNext();
	}

	@Override
	public ETLTestClass next()
	{
		return it.next();
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void reset()
	{
		it = classes.iterator();
	}
}
