package org.bitbucket.bradleysmithllc.etlunit.feature.report;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.DurationFormat;

public class TestStatistics
{
	private int testsRun = 0;
	private int testsIgnored = 0;
	private int testsPassed = 0;
	private int testFailures = 0;
	private int testsFailed = 0;
	private int testsErrored = 0;
	private int testErrors = 0;
	private int testsWarned = 0;
	private int testWarnings = 0;
	private long duration = 0L;
	private long executorDuration = 0L;

	public void setDuration(long dur)
	{
		duration = dur;
	}

	public void addDuration(long dur)
	{
		duration += dur;
	}

	public void addExecutorDuration(long dur)
	{
		executorDuration += dur;
	}

	public void addRun()
	{
		testsRun++;
	}

	public void addPassed()
	{
		testsPassed++;
	}

	public void addIgnored()
	{
		testsIgnored++;
	}

	public void addFailed()
	{
		testsFailed++;
	}

	public void addFailures(int num)
	{
		testFailures += num;
	}

	public void addErrored()
	{
		testsErrored++;
	}

	public void addErrors(int num)
	{
		testErrors += num;
	}

	public void addWarned()
	{
		testsWarned++;
	}

	public void addWarnings(int num)
	{
		testWarnings += num;
	}

	public int getTestsWarned() {
		return testsWarned;
	}

	public int getTestWarnings() {
		return testWarnings;
	}

	public int getTestsRun() {
		return testsRun;
	}

	public int getTestsPassed() {
		return testsPassed;
	}

	public int getTestsIgnored() {
		return testsIgnored;
	}

	public int getTestFailures() {
		return testFailures;
	}

	public int getTestsFailed() {
		return testsFailed;
	}

	public int getTestsErrored() {
		return testsErrored;
	}

	public int getTestErrors() {
		return testErrors;
	}

	public long getDuration() {
		return duration;
	}

	public long getExecutorDuration() {
		return executorDuration;
	}

	public String getExecutorDurationFormatted() {
		return getDurationFormatted(getExecutorDuration());
	}

	public String getDurationFormatted()
	{
		return getDurationFormatted(getDuration());
	}

	private String getDurationFormatted(long time)
	{
		return DurationFormat.formatDuration(time);
	}

	public void merge(TestStatistics other)
	{
		testsRun += other.testsRun;
		testsPassed += other.testsPassed;
		testsIgnored += other.testsIgnored;
		testFailures += other.testFailures;
		testsFailed += other.testsFailed;
		testsErrored += other.testsErrored;
		testErrors += other.testErrors;
		testsWarned += other.testsWarned;
		testWarnings += other.testWarnings;
		executorDuration += other.executorDuration;
	}
}
