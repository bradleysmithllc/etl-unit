package org.bitbucket.bradleysmithllc.etlunit.feature.logging;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface LogFileManager
{
	interface LogFile
	{
		String getMessage();

		String getClassifier();

		File getFile();

		File getOriginalPath();

		ETLTestOperation getTestOperation();

		ETLTestMethod getTestMethod();

		ETLTestClass getTestClass();
	}

	List<LogFile> getLogFilesByETLTestOperation(ETLTestOperation operation);

	List<LogFile> getLogFilesByETLTestMethod(ETLTestMethod operation);

	List<LogFile> getLogFilesByETLTestClass(ETLTestClass operation);

	void addLogFile(ETLTestMethod method, ETLTestOperation operation, File log, String classifier) throws IOException;
	void addLogFile(ETLTestOperation operation, File log, String classifier) throws IOException;

	void addLogFile(ETLTestMethod operation, File log, String classifier) throws IOException;

	void addLogFile(ETLTestClass operation, File log, String classifier) throws IOException;

	void addLogFile(File log, String classifier) throws IOException;

	void addLogFile(ETLTestMethod method, ETLTestOperation operation, File log, String classifier, String message) throws IOException;
	void addLogFile(ETLTestOperation operation, File log, String classifier, String message) throws IOException;

	void addLogFile(ETLTestMethod operation, File log, String classifier, String message) throws IOException;

	void addLogFile(ETLTestClass operation, File log, String classifier, String message) throws IOException;

	void addLogFile(File log, String classifier, String message) throws IOException;

	File getLogRoot();
}
