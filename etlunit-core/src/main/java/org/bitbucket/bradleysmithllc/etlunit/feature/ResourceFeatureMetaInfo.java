package org.bitbucket.bradleysmithllc.etlunit.feature;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.util.*;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class ResourceFeatureMetaInfo implements FeatureMetaInfo {
	private final Feature describing;
	private final ClassLoader loader;

	private final String featureClassName;

	private final String resourceNameBase;
	private Map<String, FeatureOperation> exportedOperations;
	private Log applicationLog;

	private JsonNode configurationNode;

	private static final JsonSchema featureOperationsJsonSchema = JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/featureOperations.jsonSchema");
	private static final JsonSchema featureAnnotationsJsonSchema = JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/featureAnnotations.jsonSchema");
	private static final JsonSchema featureOptionsJsonSchema = JsonSchemaLoader.fromResource("org/bitbucket/bradleysmithllc/etlunit/featureOptions.jsonSchema");

	public ResourceFeatureMetaInfo(String describingClName, ClassLoader lo) {
		featureClassName = describingClName;
		resourceNameBase = describingClName.replace('.', '/');
		describing = null;
		loader = lo;
	}

	public ResourceFeatureMetaInfo(Feature describing) {
		this.describing = describing;
		loader = ObjectUtils.firstNotNull(
				Thread.currentThread().getContextClassLoader(),
				describing.getClass().getClassLoader()
		);
		featureClassName = describing.getClass().getName();
		resourceNameBase = describing.getClass().getName().replace('.', '/');
	}

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log) {
		applicationLog = log;
	}

	@Override
	public String getFeatureName() {
		ObjectNode obj = getResourceObject("meta");

		if (obj == null) {
			throw new IllegalArgumentException("Meta information not defined");
		}

		// all we care about here is the one entry feature-type
		JsonNode type = JsonUtils.query(obj, "feature-name");

		if (type == null) {
			throw new IllegalArgumentException("Meta information missing feature-name");
		}

		String typeString = type.asText();

		return typeString;
	}

	@Override
	public String getFeatureConfiguration() {
		return getResource("configuration");
	}

	@Override
	public JsonNode getFeatureConfigurationValidatorNode() {
		if (configurationNode == null) {
			String schema = getResource("configuration.validator.jsonSchema");

			if (schema != null) {
				try {
					configurationNode = JsonSchemaResolver.resolveSchemaReference(JsonLoader.fromString(schema), loader);
				} catch (Exception e) {
					throw new RuntimeException("Could not load schema for feature configuration ["
							+ describing.getFeatureName()
							+ "]: "
							+ e.toString());
				}
			}
		}

		return configurationNode;
	}

	JsonNode loadValidatorNode(String id)
	{
		String schema = getResource(id + ".validator.jsonSchema");

		if (schema != null)
		{
			try
			{
				return JsonSchemaResolver.resolveSchemaReference(JsonLoader.fromString(schema), loader);
			}
			catch (Exception e)
			{
				throw new RuntimeException("Invalid schema for [" + getReportableFeatureName()
						+ "].[" + id + "]", e);
			}
		}

		return null;
	}

	JsonSchema loadValidator(JsonNode validatorNode, String id)
	{
		if (validatorNode != null)
		{
			try
			{
				JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

				return factory.getJsonSchema(validatorNode);
			}
			catch (ProcessingException e)
			{
				throw new RuntimeException("Invalid schema for [" + getReportableFeatureName()
						+ "].[" + id + "]: " + JsonProcessingUtil.getProcessingReport(e));
			}
			catch (Exception e)
			{
				throw new RuntimeException("Invalid schema for [" + getReportableFeatureName()
						+ "].[" + id + "]", e);
			}
		}

		return null;
	}

	@Override
	public JsonSchema getFeatureConfigurationValidator() {
		JsonNode node = getFeatureConfigurationValidatorNode();

		if (node != null) {
			try {
				JsonSchemaFactory fact = JsonSchemaFactory.byDefault();

				return fact.getJsonSchema(node);
			} catch (ProcessingException e) {
				throw new RuntimeException("Invalid schema for feature configuration ["
						+ describing.getFeatureName()
						+ "]: "
						+ JsonProcessingUtil.getProcessingReport(e));
			}
		}

		return null;
	}

	protected String getResource(String id) {
		String name = resourceNameBase + "." + id;
		URL res = loader.getResource(name);

		if (res == null) {
			return null;
		}

		try {
			String desc = IOUtils.readURLToString(res);

			return desc;
		} catch (IOException e) {
			throw new UnsupportedOperationException();
		}
	}

	public Log getApplicationLog() {
		return applicationLog;
	}

	@Override
	public Map<String, FeatureOperation> getExportedOperations() {
		if (exportedOperations == null) {
			exportedOperations = loadExportedOperations();
		}

		return exportedOperations;
	}

	private Map<String, FeatureOperation> loadExportedOperations() {
		ObjectNode obj = getResourceObject("operations");

		if (obj == null) {
			return null;
		}

		// validate against the schema
		try {
			ProcessingReport report = featureOperationsJsonSchema.validate(obj);

			if (!report.isSuccess()) {
				throw new IllegalArgumentException("Bad operations descriptor: " + JsonProcessingUtil.getProcessingReport(report));
			}
		} catch (ProcessingException e) {
			throw new IllegalArgumentException("Bad operations descriptor: " + JsonProcessingUtil.getProcessingReport(e), e);
		}

		Map<String, FeatureOperation> map = new HashMap<String, FeatureOperation>();

		Iterator<Map.Entry<String, JsonNode>> fields = obj.fields();

		while (fields.hasNext()) {
			List<String> signatures = new ArrayList<String>();

			Map.Entry<String, JsonNode> field = fields.next();

			String key = field.getKey();

			JsonNode jsonNode = field.getValue();
			String description = jsonNode.get("description").asText();

			if (jsonNode.has("signatures")) {
				ArrayNode node = (ArrayNode) jsonNode.get("signatures");

				Iterator<JsonNode> iterator = node.iterator();
				while (iterator.hasNext()) {
					JsonNode n = iterator.next();

					signatures.add(n.asText());
				}
			} else {
				signatures.add(key);
			}

			map.put(key, new ResourceFeatureOperation(this, key, description, signatures));
		}

		return map;
	}

	@Override
	public String getDescribingClassName() {
		return featureClassName;
	}

	public String getReportableFeatureName() {
		if (describing == null) {
			return featureClassName;
		}

		return getDescribing().getFeatureName();
	}

	@Override
	public Feature getDescribing() {
		if (describing != null) {
			return describing;
		}

		throw new IllegalStateException("Describing feature not available");
	}

	@Override
	public Map<String, FeatureAnnotation> getExportedAnnotations() {
		ObjectNode obj = getResourceObject("annotations");

		if (obj == null) {
			return null;
		}

		// validate against the schema
		try {
			ProcessingReport report = featureAnnotationsJsonSchema.validate(obj);

			if (!report.isSuccess()) {
				throw new IllegalArgumentException("Bad annotations descriptor: " + JsonProcessingUtil.getProcessingReport(report));
			}
		} catch (ProcessingException e) {
			throw new IllegalArgumentException("Bad annotations descriptor: " + JsonProcessingUtil.getProcessingReport(e), e);
		}

		Map<String, FeatureAnnotation> map = new HashMap<String, FeatureAnnotation>();

		Iterator<Map.Entry<String, JsonNode>> fields = obj.fields();

		while (fields.hasNext()) {
			Map.Entry<String, JsonNode> field = fields.next();

			String key = field.getKey();

			String description = field.getValue().get("description").asText();

			String sType = field.getValue().get("propertyType").asText();

			FeatureAnnotation.propertyType type = FeatureAnnotation.propertyType.none;

			if (sType.equals("none")) {
			} else if (sType.equals("required")) {
				type = FeatureAnnotation.propertyType.required;
			} else if (sType.equals("optional")) {
				type = FeatureAnnotation.propertyType.optional;
			}

			map.put(key, new ResourceFeatureAnnotation(this, key, description, type));
		}

		return map;
	}

	@Override
	public boolean isInternalFeature() {
		ObjectNode obj = getResourceObject("meta");

		if (obj == null) {
			return true;
		}

		// all we care about here is the one entry feature-type
		JsonNode type = JsonUtils.query(obj, "feature-type");

		if (type == null) {
			return true;
		}

		String typeString = type.asText();
		if (typeString.equals("external")) {
			return false;
		} else if (typeString.equals("internal")) {
			return true;
		} else {
			throw new IllegalArgumentException("Unknown feature type: " + typeString);
		}
	}

	@Override
	public String getFeatureUsage() {
		String res = getResource("usage");

		return res;
	}

	@Override
	public List<RuntimeOptionDescriptor> getOptions() {
		String res = getResource("options");

		if (res == null) {
			return Collections.emptyList();
		}

		List<RuntimeOptionDescriptor> list = new ArrayList<RuntimeOptionDescriptor>();

		// we have json, now load the schema for options and verify
		try {
			JsonNode node = JsonLoader.fromString(res);

			ProcessingReport report = featureOptionsJsonSchema.validate(node);

			if (!report.isSuccess()) {
				throw new IllegalArgumentException("Bad operations descriptor: " + JsonProcessingUtil.getProcessingReport(report));
			}

			Iterator<String> names = node.fieldNames();

			while (names.hasNext()) {
				String optionName = names.next();

				JsonNode descNode = node.get(optionName);

				// get the description
				String description = descNode.get("description").asText();

				// check first for a bool property
				JsonNode bn = descNode.get("enabled");

				if (bn != null) {
					list.add(new RuntimeOptionDescriptorImpl(
							optionName,
							description,
							bn.asBoolean()
					));
				} else {
					// check next for integer
					bn = descNode.get("integer-value");

					if (bn != null) {
						list.add(new RuntimeOptionDescriptorImpl(
								optionName,
								description,
								bn.asInt()
						));
					} else {
						// this must be a string
						bn = descNode.get("string-value");

						list.add(new RuntimeOptionDescriptorImpl(
								optionName,
								description,
								bn.asText()
						));
					}
				}
			}

			return list;
		} catch (ProcessingException e) {
			throw new IllegalArgumentException("Bad options descriptor: " + JsonProcessingUtil.getProcessingReport(e), e);
		} catch (IOException e) {
			throw new IllegalArgumentException("Bad options descriptor", e);
		}
	}

	public ObjectNode getResourceObject(String id) {
		String src = getResource(id);

		if (src == null) {
			return null;
		}

		try {
			JsonNode node = JsonLoader.fromString(src);

			if (!node.isObject()) {
				throw new IllegalArgumentException("Bad json. Root not an object");
			}

			return (ObjectNode) JsonSchemaResolver.resolveSchemaReference(node);
		} catch (Exception e) {
			throw new UnsupportedOperationException(resourceNameBase + "." + id, e);
		}
	}
}
