package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.*;

public class NoOpProcessExecutor implements ProcessExecutor
{
	@Override
	public ProcessFacade execute(final ProcessDescription pd)
	{
		return new ProcessFacade()
		{
			@Override
			public ProcessDescription getDescriptor()
			{
				return pd;
			}

			@Override
			public void waitForStreams()
			{
			}

			@Override
			public void waitForOutputStreamsToComplete() {
			}

			@Override
			public void waitForCompletion()
			{
			}

			@Override
			public int getCompletionCode()
			{
				return 0;
			}

			@Override
			public void kill()
			{
			}

			@Override
			public Writer getWriter()
			{
				return null;
			}

			@Override
			public BufferedReader getReader()
			{
				return null;
			}

			@Override
			public BufferedReader getErrorReader()
			{
				return null;
			}

			@Override
			public StringBuffer getInputBuffered() throws IOException
			{
				return new StringBuffer();
			}

			@Override
			public StringBuffer getErrorBuffered() throws IOException
			{
				return new StringBuffer();
			}

			@Override
			public OutputStream getOutputStream()
			{
				return null;
			}

			@Override
			public InputStream getInputStream()
			{
				return null;
			}

			@Override
			public InputStream getErrorStream()
			{
				return null;
			}
		};
	}
}
