package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.*;

/*Replacement for Process Builder.  This one is to allow for better
 * testing.
 */
public final class ProcessDescription
{
	private final String command;
	private boolean redirectStandardError;
	private final List<String> arguments = new ArrayList<String>();
	private File privateWorkingDir;
	private File workingDirectory;
	private File privateOutput;
	private File userDirectedOutput;

	// This hashmap was changed to a treemap to make the output
	// stable.  This was done for debugging and testing, but shouldn't
	// have an impact on performance.
	private final Map<String, String> environment = new TreeMap<String, String>(System.getenv());

	public ProcessDescription(String command)
	{
		this.command = command;
	}

	public ProcessDescription redirectStdError(boolean redirectStandardError)
	{
		this.redirectStandardError = redirectStandardError;

		return this;
	}

	public ProcessDescription redirectStdError()
	{
		return redirectStdError(true);
	}

	public ProcessDescription argument(String arg)
	{
		arguments.add(arg);

		return this;
	}

	public boolean isRedirectStandardError() {
		return redirectStandardError;
	}


	public ProcessDescription privateWorkingDir(File arg)
	{
		privateWorkingDir = arg;

		return this;
	}

	public File getPrivateWorkingDir() {
		return privateWorkingDir;
	}

	public ProcessDescription output(File arg)
	{
		userDirectedOutput = arg;

		return this;
	}

	public File getOutputFile()
	{
		return userDirectedOutput;
	}

	public ProcessDescription privateOutput(File arg)
	{
		privateOutput = arg;

		return this;
	}

	public File getPrivateOutput()
	{
		return privateOutput;
	}

	public ProcessDescription arguments(List<String> args)
	{
		arguments.addAll(args);

		return this;
	}

	public String getCommandName()
	{
		File f = new File(command);

		return f.getName();
	}

	public ProcessDescription workingDirectory(File file)
	{
		this.workingDirectory = file;

		return this;
	}

	public ProcessDescription environment(String var, String value)
	{
		environment.put(var, value);
		return this;
	}

	public String getCommand()
	{
		return command;
	}

	public List<String> getArguments()
	{
		return arguments;
	}

	public boolean hasArguments()
	{
		return arguments.size() != 0;
	}

	public File getWorkingDirectory()
	{
		return workingDirectory;
	}

	public Map<String, String> getEnvironment()
	{
		return environment;
	}

	public String debugString()
	{
		return
				new StringBuilder("Command[")
						.append(command)
						.append("]Arguments[")
						.append(arguments.toString())
						.append("]Env[")
						.append(deltaEnvironment())
						.append("]Cwd[")
						.append(workingDirectory)
						.append("]")
						.append("]Output[")
						.append(userDirectedOutput)
						.append("]")
						.toString();
	}

	public Map<String, String> deltaEnvironment()
	{
		Map<String, String> deltaMap = new TreeMap<String, String>(getEnvironment());

		Map<String, String> sysMap = System.getenv();

		Set<Map.Entry<String, String>> set = sysMap.entrySet();
		Iterator<Map.Entry<String, String>> it = set.iterator();

		while (it.hasNext())
		{
			Map.Entry<String, String> entry = it.next();

			String key = entry.getKey();
			String value = entry.getValue();

			String envValue = deltaMap.get(key);
			if (envValue != null && (envValue == value || envValue.equals(value)))
			{
				deltaMap.remove(key);
			}
		}

		return deltaMap;
	}
}
