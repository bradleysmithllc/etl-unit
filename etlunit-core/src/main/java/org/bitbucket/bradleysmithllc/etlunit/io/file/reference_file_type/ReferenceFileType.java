package org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;
import java.util.Map;

public interface ReferenceFileType extends Comparable<ReferenceFileType>
{
	String getDescription();

	boolean hasVersions();

	List<String> getVersions();

	String getDefaultVersion();

	String getDefaultClassifier();

	Map<String, ReferenceFileTypeClassifier> getClassifiers();

	/**
	 * The qualified name
	 * @return
	 */
	String getId();

	/**
	 * Simple name
	 * @return
	 */
	String getName();

	void loadFromResource();

	String generateResourcePathForRef(ReferenceFileTypeRef ref) throws RequestedFileTypeNotFoundException;
}
