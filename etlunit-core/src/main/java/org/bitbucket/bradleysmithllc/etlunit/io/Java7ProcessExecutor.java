package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Java7ProcessExecutor implements ProcessExecutor {
	@Override
	public ProcessFacade execute(final ProcessDescription pd) throws IOException {
		ProcessBuilder pb = new ProcessBuilder(pd.getCommand());

		List<String> args = new ArrayList<String>(pd.getArguments());
		args.add(0, pd.getCommand());

		pb.command(args);

		pb.redirectErrorStream(true);

		final File outputFile = pd.getOutputFile();
		pb.redirectOutput(outputFile).directory(pd.getWorkingDirectory());

		pb.environment().putAll(pd.getEnvironment());

		final Process process = pb.start();
		final OutputStream outputStream = process.getOutputStream();
		final Writer writer = new OutputStreamWriter(outputStream);

		// very primitive for now.  Create a thread to read the file until it reaches end of file, pumping
		// the data into a pipe.  Once end of file is reached, if the process is dead, complete.  Otherwise, poll.

		final PipedInputStream pipedInputStream = new PipedInputStream();
		final PipedOutputStream pipedOutputStream = new PipedOutputStream(pipedInputStream);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					byte[] buff = new byte[4096];

					int offset = 0;
					int read = 0;

					while (true) {
						RandomAccessFile raf = new RandomAccessFile(outputFile, "r");
						raf.seek(offset);

						while ((read = raf.read(buff)) != -1) {
							pipedOutputStream.write(buff, 0, read);
							offset += read;
						}

						raf.close();

						boolean more = false;

						// reached end of file, possibly before the process finished.  Check the process, and if it isn't complete, continue.
						try {
							process.exitValue();

							// really done - exit.
						} catch (IllegalThreadStateException exc) {
							// still more to do
							more = true;
							Thread.currentThread().sleep(100L);
						}

						if (!more)
						{
							break;
						}
					}
				} catch (Exception e) {
					throw new RuntimeException(e);
				} finally {
					// close the pipe so readers don't block forever.
					try {
						pipedOutputStream.close();
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
			}
		}).start();

		// expose the reader end of the pipe to clients
		final InputStream innie = new BufferedInputStream(pipedInputStream);

		final BufferedReader reader = new BufferedReader(new InputStreamReader(innie));

		return new ProcessFacade() {
			@Override
			public ProcessDescription getDescriptor() {
				return pd;
			}

			@Override
			public void waitForCompletion() {
				try {
					process.waitFor();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}

			@Override
			public int getCompletionCode() {
				return process.exitValue();
			}

			@Override
			public void kill() {
				process.destroy();
			}

			@Override
			public void waitForStreams() {
			}

			@Override
			public void waitForOutputStreamsToComplete() throws IOException {
			}

			@Override
			public Writer getWriter() {
				return writer;
			}

			@Override
			public BufferedReader getReader() {
				return reader;
			}

			@Override
			public BufferedReader getErrorReader() {
				return getReader();
			}

			@Override
			public StringBuffer getInputBuffered() throws IOException {
				return new StringBuffer(FileUtils.readFileToString(outputFile));
			}

			@Override
			public StringBuffer getErrorBuffered() throws IOException {
				return getInputBuffered();
			}

			@Override
			public OutputStream getOutputStream() {
				return outputStream;
			}

			@Override
			public InputStream getInputStream() {
				return innie;
			}

			@Override
			public InputStream getErrorStream() {
				return getInputStream();
			}
		};
	}
}
