package org.bitbucket.bradleysmithllc.etlunit.feature.workspace;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Workspace
{
	private final File root;
	private File checkpointRoot;

	private final Map<String, File> rootMap = new HashMap<>();

	private final List<File> checkPoints = new ArrayList<>();

	public Workspace(File root) throws IOException {
		this.root = root;

		mkdir(root);
	}

	private void mkdir(File root) throws IOException {
		FileUtils.forceMkdir(root);
	}

	public String rootPath()
	{
		return checkpointRoot.getAbsolutePath();
	}

	public File workspaceRoot()
	{
		return root;
	}

	public File root()
	{
		return checkpointRoot;
	}

	/*Completes the current workspace and creates a new one*/
	public void checkPoint(String identifier) throws IOException {
		if (checkpointRoot != null) {
			checkPoints.add(checkpointRoot);
		}

		rootMap.clear();

		checkpointRoot = new File(root, identifier);

		// this should be a clean workspace - purge in case there is anything already there
		FileUtils.deleteDirectory(checkpointRoot);

		mkdir(checkpointRoot);
	}

	public void mkPartition(String id) throws IOException {
		File part = new File(root(), id);

		rootMap.put(id, part);
		mkdir(part);
	}

	public File openPartition(String id) {
		if (!rootMap.containsKey(id))
		{
			throw new IllegalArgumentException("Partition '" + id + "' does not exist in this workspace");
		}

		return rootMap.get(id);
	}

	public Set<String> partitionKeys() {
		return rootMap.keySet();
	}
}
