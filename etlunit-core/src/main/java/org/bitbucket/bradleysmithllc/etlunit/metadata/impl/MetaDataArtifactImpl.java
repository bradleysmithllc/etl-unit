package org.bitbucket.bradleysmithllc.etlunit.metadata.impl;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifactContent;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;

import javax.inject.Inject;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.*;

public class MetaDataArtifactImpl implements MetaDataArtifact
{
	private RuntimeSupport runtimeSupport;

	private MetaDataPackageContext metaDataPackageContext;

	private String relativePath;
	private String projectRelativePath;
	private File root;
	private final List<MetaDataArtifactContent> artifactContents = new ArrayList<MetaDataArtifactContent>();
	private final Map<String, MetaDataArtifactContent> contentMap = new HashMap<String, MetaDataArtifactContent>();

	public MetaDataArtifactImpl(String relativePath, String projectRelativePath, File root) {
		this.relativePath = relativePath;
		this.projectRelativePath = projectRelativePath;
		this.root = root;
	}

	public MetaDataArtifactImpl() {
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
	}

	@Override
	public MetaDataPackageContext getMetaDataPackageContext() {
		return metaDataPackageContext;
	}

	public void setMetaDataPackageContext(MetaDataPackageContext metaDataPackageContext) {
		this.metaDataPackageContext = metaDataPackageContext;
	}

	@Override
	public String getProjectRelativePath() {
		return projectRelativePath;
	}

	@Override
	public String getRelativePath() {
		return relativePath;
	}

	@Override
	public File getPath() {
		return root;
	}

	@Override
	public Map<String, MetaDataArtifactContent> getContentMap() {
		return Collections.unmodifiableMap(contentMap);
	}

	@Override
	public List<MetaDataArtifactContent> getContents() {
		return Collections.unmodifiableList(artifactContents);
	}

	@Override
	public synchronized MetaDataArtifactContent createContent(String name) {
		if (contentMap.containsKey(name))
		{
			return contentMap.get(name);
		}

		MetaDataArtifactContentImpl content = new MetaDataArtifactContentImpl(name);
		content.setMetaDataArtifact(this);
		content.receiveRuntimeSupport(runtimeSupport);

		artifactContents.add(content);
		contentMap.put(name, content);

		return content;
	}

	@Override
	public void populateAllFromDir() {
		if (root == null)
		{
			throw new IllegalStateException("Root dir not available");
		}

		root.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				// only handle files, not directories
				if (pathname.isFile())
				{
					createContent(pathname.getName());
				}

				return false;
			}
		});
	}

	@Override
	public void toJson(JsonWriter writer) throws IOException {
		writer.name("relative-path").value(relativePath);
		writer.name("project-relative-path").value(projectRelativePath);
		writer.name("absolute-path").value(root.getCanonicalPath());

		writer.name("contents").beginArray();

		for (MetaDataArtifactContent content : artifactContents)
		{
			writer.beginObject();
			content.toJson(writer);
			writer.endObject();
		}

		writer.endArray();
	}

	@Override
	public synchronized void fromJson(JsonNode node) {
		artifactContents.clear();
		contentMap.clear();

		relativePath = node.get("relative-path").asText();
		projectRelativePath = node.get("project-relative-path").asText();
		root = new File(node.get("absolute-path").asText());

		ArrayNode anode = (ArrayNode) node.get("contents");

		Iterator<JsonNode> it = anode.elements();

		while (it.hasNext())
		{
			JsonNode cnode = it.next();

			MetaDataArtifactContentImpl content = new MetaDataArtifactContentImpl();
			content.receiveRuntimeSupport(runtimeSupport);
			content.fromJson(cnode);

			artifactContents.add(content);
			contentMap.put(content.getName(), content);
		}
	}

	public boolean equals(Object other)
	{
		if (getClass() != other.getClass())
		{
			return false;
		}

		MetaDataArtifactImpl oImpl = (MetaDataArtifactImpl) other;

		return
			ObjectUtils.compare(relativePath, oImpl.relativePath) == 0 &&
			ObjectUtils.compare(projectRelativePath, oImpl.projectRelativePath) == 0 &&
			artifactContents.equals(oImpl.artifactContents);
	}
}
