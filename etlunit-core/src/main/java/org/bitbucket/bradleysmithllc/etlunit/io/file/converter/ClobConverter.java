package org.bitbucket.bradleysmithllc.etlunit.io.file.converter;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CharSequenceInputStream;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;

import java.io.*;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * CLOB expects a java.sql.Clob object
 */
public class ClobConverter extends JdbcConverter
{
	@Override
	public String format(Object data, DataFileSchema.Column column) {
		if (data instanceof Clob)
		{
			Clob clob = (Clob) data;

			try
			{
				Reader reader = clob.getCharacterStream();

				try
				{
					StringWriter stw = new StringWriter();
					IOUtils.copy(reader, stw);

					return stw.toString();
				}
				finally
				{
					reader.close();
				}
			}
			catch (Exception e)
			{
				throw new RuntimeException("Error processing CLOB", e);
			}
		}
		else
		{
			return String.valueOf(data);
		}
	}

	@Override
	public Object parse(String data, DataFileSchema.Column column) {
		Clob clob = new StringClob(data);
		return clob;
	}

	@Override
	public String getId() {
		return "CLOB";
	}

	@Override
	public Pattern getPattern(DataFileSchema.Column column) {
		return null;
	}
}

class StringClob implements Clob
{
	private final String text;

	StringClob(String text)
	{
		this.text = text;
	}

	@Override
	public long length() throws SQLException {
		return text.length();
	}

	@Override
	public String getSubString(long pos, int length) throws SQLException {
		return text.substring((int) pos, (int) (pos + length));
	}

	@Override
	public Reader getCharacterStream() throws SQLException {
		return new StringReader(text);
	}

	@Override
	public InputStream getAsciiStream() throws SQLException {
		return new CharSequenceInputStream(text, "");
	}

	@Override
	public long position(String searchstr, long start) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public long position(Clob searchstr, long start) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public int setString(long pos, String str) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public int setString(long pos, String str, int offset, int len) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public OutputStream setAsciiStream(long pos) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Writer setCharacterStream(long pos) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void truncate(long len) throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void free() throws SQLException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Reader getCharacterStream(long pos, long length) throws SQLException {
		return new StringReader(getSubString(pos, (int) length));
	}
}
