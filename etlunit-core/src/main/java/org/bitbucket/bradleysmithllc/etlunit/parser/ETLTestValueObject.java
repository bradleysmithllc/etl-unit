package org.bitbucket.bradleysmithllc.etlunit.parser;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;
import java.util.Map;

public interface ETLTestValueObject extends ETLTestDebugTraceable
{
	boolean isNull();

	enum value_type
	{
		quoted_string,
		object,
		list,
		literal,
		pojo,
		t_null
	}

	String getValueAsString();

	boolean getValueAsBoolean();

	List<ETLTestValueObject> getValueAsList();

	List<String> getValueAsListOfStrings();

	Map<String, ETLTestValueObject> getValueAsMap();

	Object getValueAsPojo();
	<T> T getValueAsType();

	value_type getValueType();

	ETLTestValueObject query(String path);

	ETLTestValueObject queryRequired(String path);

	enum merge_type
	{
		/**
		 * If there is a conflict, the left value (this) wins
		 */
		left_merge,

		/**
		 * If there is a conflict, the right value (other) wins
		 */
		right_merge,
	}

	enum merge_policy
	{
		recursive,
		nonrecursive
	}

	/**
	 * Perform a recursive left merge.
	 * @param obj
	 * @return
	 */
	ETLTestValueObject merge(ETLTestValueObject obj);

	ETLTestValueObject merge(ETLTestValueObject object, merge_type type, merge_policy policy);

	ETLTestValueObject copy();

	JsonNode getJsonNode();

	String toFormattedString();
}
