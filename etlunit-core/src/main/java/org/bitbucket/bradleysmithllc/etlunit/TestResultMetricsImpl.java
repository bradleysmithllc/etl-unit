package org.bitbucket.bradleysmithllc.etlunit;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;

import java.util.HashMap;
import java.util.Map;

public class TestResultMetricsImpl implements TestResultMetrics
{
	private int numTests;
	private int numPasses;
	private int numFailures;
	private int numWarnings;
	private int numErrors;
	private int numIgnored;

	private final
	Map<String, StatusReporter.CompletionStatus>
			statsByTest =
			new HashMap<String, StatusReporter.CompletionStatus>();

	@Override
	public int getNumberOfTestsRun()
	{
		return numTests;
	}

	@Override
	public int getNumberOfTestsPassed()
	{
		return numPasses;
	}

	@Override
	public int getNumberOfAssertionFailures()
	{
		return numFailures;
	}

	@Override
	public int getNumberOfWarnings()
	{
		return numWarnings;
	}

	@Override
	public int getNumberIgnored() {
		return numIgnored;
	}

	@Override
	public int getNumberOfErrors()
	{
		return numErrors;
	}

	@Override
	public void addStatus(ETLTestMethod method, StatusReporter.CompletionStatus status)
	{
		numTests++;
		statsByTest.put(method.getQualifiedName(), status);
	}

	@Override
	public void addTestsPassed(int num)
	{
		numPasses += num;
	}

	@Override
	public void addTestsIgnored(int num) {
		numIgnored += num;
	}

	@Override
	public void addAssertionFailures(int num)
	{
		numFailures += num;
	}

	@Override
	public void addTestWarnings(int num)
	{
		numWarnings += num;
	}

	@Override
	public void addErrors(int num)
	{
		numErrors += num;
	}

	@Override
	public void reset()
	{
		numTests = 0;
		numPasses = 0;
		numFailures = 0;
		numWarnings = 0;
		numErrors = 0;
		numIgnored = 0;
	}

	@Override
	public Map<String, StatusReporter.CompletionStatus> getResultsMapByTest()
	{
		return statsByTest;
	}
}
