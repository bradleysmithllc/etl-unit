package org.bitbucket.bradleysmithllc.etlunit.listener;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContextImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.HashMapArrayList;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;
import org.bitbucket.bradleysmithllc.etlunit.util.NeedsTest;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@NeedsTest
public class ClassBroadcasterImpl implements ClassBroadcaster {
	private final ClassLocator locator;
	private final ClassListener listener;
	private final ClassDirector director;
	private final VariableContext context;
	private final Log log;
	private final MapLocal mapLocal;
	private final int executorCount;
	private final LocalDateTime testStartTime;

	public ClassBroadcasterImpl(ClassLocator plocator, ClassDirector pdirector, ClassListener plistener, MapLocal mapLocal, Log log) {
		this(plocator, pdirector, plistener, mapLocal, log, new VariableContextImpl(), 1, LocalDateTime.now());
	}

	public ClassBroadcasterImpl(ClassLocator plocator, ClassDirector pdirector, ClassListener plistener, MapLocal mapLocal, Log log, VariableContext variableContext, int exe, LocalDateTime ts) {
		locator = plocator;
		listener = plistener;
		director = pdirector;
		this.log = log;
		context = variableContext;
		this.mapLocal = mapLocal;
		executorCount = exe;
		testStartTime = ts;
	}

	@Override
	public void broadcast(StatusReporter statusReporter, ETLTestCases cases) {
		broadcast1(statusReporter == null ? new NullStatusReporterImpl() : statusReporter, cases);
	}

	@Override
	public ETLTestCases preScan(StatusReporter statusReporter) {
		statusReporter = (statusReporter == null ? new NullStatusReporterImpl() : statusReporter);

		statusReporter.scanStarted();

		ETLTestCases cases = new ETLTestCases();

		director.beginBroadcast();

		while (locator.hasNext()) {
			ETLTestClass cl = locator.next();

			// we don't care about rejected or deferred results
			if (director.accept(cl) == ClassResponder.response_code.accept) {
				statusReporter.testClassAccepted(cl);

				broadcastAccepts(director, cl.getBeforeClassMethods());

				// get a list of all methods before doing anything else
				for (ETLTestMethod method : cl.getTestMethods()) {
					if (director.accept(method) == ClassResponder.response_code.accept) {
						statusReporter.testMethodAccepted(method);

						// add to our list
						cases.addTestMethod(method);

						// broadcast every operation just for fun along with before class / before / after and after class operations
						broadcastAccepts(director, method.getTestClass().getBeforeTestMethods());
						broadcastAccepts(director, method);
						broadcastAccepts(director, method.getTestClass().getAfterTestMethods());
					} else {
						log.debug("Test method not accepted:  " + method.getQualifiedName());
					}
				}
				broadcastAccepts(director, cl.getAfterClassMethods());
			} else {
				log.info("Test class not accepted: " + cl.getQualifiedName());
			}
		}

		director.endBroadcast();

		statusReporter.scanCompleted();

		return cases;
	}

	private void broadcastAccepts(ClassDirector director, List<ETLTestMethod> beforeClassMethods) {
		for (ETLTestMethod op : beforeClassMethods) {
			broadcastAccepts(director, op);
		}
	}

	private void broadcastAccepts(ClassDirector director, ETLTestMethod method) {
		for (ETLTestOperation op : method.getOperations()) {
			director.accept(op);
		}
	}

/*
	private int broadcast0(StatusReporter statusReporter, ETLTestCases cases) {
		if (statusReporter == null) throw new IllegalStateException();

		if (statusReporter != null) {
			statusReporter.scanStarted();
		}

		int testCount = 0;

		StatusReporter.CompletionStatus classStatuses = new StatusReporter.CompletionStatus();

		// seed with an impossible value so the null package can be properly detected
		ETLTestPackage thisPackage = null;

		director.beginBroadcast();

		while (locator.hasNext()) {
			ETLTestClass cl = locator.next();

			mapLocal.setCurrentlyProcessingTestClass(cl);

			// we don't care about rejected or deferred results
			if (director.accept(cl) == ClassResponder.response_code.accept) {
				// create a subcontext for the class
				/**  \/\/ DONE \/ \/ ** /
				VariableContext classScope = context.createNestedScope();
				classScope.declareAndSetValue("etlunit_test_class", new ETLTestValueObjectImpl(cl));

				if (statusReporter != null) {
					statusReporter.testClassAccepted(cl);
				}

				List<ETLTestMethod> methodsToExecute = new ArrayList<ETLTestMethod>();

				List<ETLTestMethod> methods = cl.getTestMethods();

				Iterator<ETLTestMethod> mit = methods.iterator();

				// get a list of all methods before doing anything else
				while (mit.hasNext()) {
					ETLTestMethod method = mit.next();

					if (director.accept(method) == ClassResponder.response_code.accept) {
						if (statusReporter != null) {
							statusReporter.testMethodAccepted(method);
						}

						testCount++;

						methodsToExecute.add(method);
					} else {
						log.debug("Test method not accepted:  " + method.getQualifiedName());
					}
				}
				/**  /\/\ DONE /\/\ ** /

				if (methodsToExecute.size() > 0) {
					/**  \/\/ DONE \/ \/ ** /
					if (!scan_only) {
						try {
							ETLTestPackage newPackage = cl.getPackage();

							if (ObjectUtils.compare(thisPackage, newPackage) != 0) {
								listener.beginPackage(newPackage, classScope, 1);
								thisPackage = newPackage;
							}

							listener.begin(cl, classScope, 1);
						} catch (TestExecutionError err) {
							classStatuses.addError(err);
						} catch (TestAssertionFailure fail) {
							classStatuses.addFailure(fail);
						} catch (TestWarning warn) {
							classStatuses.addWarning(warn);
						}

						if (!classStatuses.hasFailures()) {
							// broadcast the variables
							List<ETLTestVariable> varlist = cl.getClassVariables();
							Iterator<ETLTestVariable> varit = varlist.iterator();

							while (varit.hasNext()) {
								ETLTestVariable var = varit.next();
								listener.declare(var, classScope, 1);
							}
						}
					}
					/**  /\/\ DONE /\/\ ** /

					/**  \/\/ DONE \/ \/ ** /
					try {
						// now broadcast all the before class methods.  Any failures here will break
						// the entire chain.
						List<ETLTestMethod> beforeClassMethods = cl.getBeforeClassMethods();
						Iterator<ETLTestMethod> bcit = beforeClassMethods.iterator();

						while (bcit.hasNext() && !classStatuses.hasFailures()) {
							ETLTestMethod bcmethod = bcit.next();

							broadcast(null, bcmethod, classStatuses, scan_only, classScope);
						}
					} catch (TestExecutionError err) {
						classStatuses.addError(err);
					}
					/**  /\/\ DONE /\/\ ** /

					mit = methodsToExecute.iterator();
					while (mit.hasNext()) {
						ETLTestMethod method = mit.next();

						mapLocal.setCurrentlyProcessingTestMethod(method);

						/**  \/\/ DONE \/ \/ ** /
						List<ETLTestAnnotation> testAnnotList = method.getAnnotations("@Test");

						// we only care about the first one - all others are bogus.
						// There must be at least one or this method would not be a test method
						ETLTestAnnotation testAnnot = testAnnotList.get(0);

						StatusReporter.CompletionStatus methodStatus = new StatusReporter.CompletionStatus();

						if (statusReporter != null) {
							statusReporter.testBeginning(method);
						}

						if (classStatuses.hasFailures()) {
							if (statusReporter != null) {
								statusReporter.testCompleted(method, classStatuses);
							}
						} else {
							try {
								ETLTestValueObject value = testAnnot.getValue();

								if (value != null) {
									ETLTestValueObject expect = value.query("expected-error-id");

									if (expect != null) {
										if (expect.getValueType() != ETLTestValueObject.value_type.quoted_string) {
											throw new TestExecutionError("Invalid expected-error-id - must be a string type",
													TestConstants.ERR_INVALID_TEST_ANNOTATION);
										}

										methodStatus.setExpectedErrorId(expect.getValueAsString());
									}

									expect = value.query("expected-failure-ids");

									if (expect != null) {
										if (expect.getValueType() != ETLTestValueObject.value_type.list) {
											throw new TestExecutionError("Invalid expected-failure-ids - must be a list type",
													TestConstants.ERR_INVALID_TEST_ANNOTATION);
										}

										methodStatus.addExpectedFailureIds(expect.getValueAsListOfStrings());
									}

									expect = value.query("expected-failure-id");

									if (expect != null) {
										if (expect.getValueType() != ETLTestValueObject.value_type.quoted_string) {
											throw new TestExecutionError("Invalid expected-failure-id - must be a string type",
													TestConstants.ERR_INVALID_TEST_ANNOTATION);
										}

										methodStatus.addExpectedFailureId(expect.getValueAsString());
									}

									expect = value.query("expected-warning-ids");

									if (expect != null) {
										if (expect.getValueType() != ETLTestValueObject.value_type.list) {
											throw new TestExecutionError("Invalid expected-warning-ids - must be a list type",
													TestConstants.ERR_INVALID_TEST_ANNOTATION);
										}

										methodStatus.addExpectedWarningIds(expect.getValueAsListOfStrings());
									}
								}

								runTest(method,
										cl.getBeforeTestMethods(),
										cl.getAfterTestMethods(),
										statusReporter,
										methodStatus,
										scan_only,
										classScope);
							} catch (TestExecutionError err) {
								methodStatus.addError(err);
							} catch (Throwable err) {
								log.severe("", err);
								methodStatus.addError(new TestExecutionError(err.toString(),
										TestConstants.ERR_UNCAUGHT_EXCEPTION,
										err));
							}

							if (statusReporter != null) {
								statusReporter.testCompleted(method, methodStatus);
							}
						}
						/**  /\/\ DONE /\/\ ** /
					}
				}

				/**  \/\/ DONE \/ \/ ** /
				try {
					// now broadcast all the after class methods.  Any failures here will break
					// the entire chain.
					List<ETLTestMethod> afterClassMethods = cl.getAfterClassMethods();
					Iterator<ETLTestMethod> acit = afterClassMethods.iterator();

					while (acit.hasNext() && !classStatuses.hasFailures()) {
						ETLTestMethod bcmethod = acit.next();

						broadcast(null, bcmethod, classStatuses, scan_only, classScope);
					}
				} catch (TestExecutionError err) {
					classStatuses.addError(err);
				}

				if (!scan_only) {
					try {
						listener.end(cl, classScope, 1);
					} catch (TestExecutionError err) {
						classStatuses.addError(err);
					} catch (TestAssertionFailure err) {
						classStatuses.addFailure(err);
					} catch (TestWarning err) {
						classStatuses.addWarning(err);
					}
				}
				/**  /\/\ DONE /\/\ ** /
			} else {
				log.info("Test class not accepted: " + cl.getQualifiedName());
			}
		}

		director.endBroadcast();

		statusReporter.scanCompleted();

		return testCount;
	}
		*/

	private void broadcast1(StatusReporter statusReporter, ETLTestCases cases) {
		if (statusReporter == null) throw new IllegalStateException();

		statusReporter.scanStarted();

		director.beginBroadcast();

		// extra listener method for begin before any executors start
		try {
			listener.beginTests(context);
		} catch (TestExecutionError testExecutionError) {
			throw new IllegalStateException(testExecutionError);
		}

		// now we have obtained our list of tests - we need to spawn enough broadcasters to handle the tests
		BroadcasterCoordinator brc = new BroadcasterCoordinatorImpl(cases, mapLocal, statusReporter, log);

		// spawn off executors
		for (int i = 0; i < executorCount; i++) {
			new Thread(new BroadcasterExecutorImpl(brc, i, mapLocal, context, director, listener, statusReporter, log, testStartTime)).start();
		}

		// wait for them to get in line
		brc.waitForExecutorsToActivate(executorCount);

		// kick it off
		brc.start();

		// pause until done
		brc.waitForCompletion();

		listener.endTests(context);

		director.endBroadcast();

		statusReporter.scanCompleted();
	}

	private void runTest(ETLTestMethod method, List<ETLTestMethod> beforeTestMethods, List<ETLTestMethod> afterTestMethods, StatusReporter statusReporter, StatusReporter.CompletionStatus methodStatus, boolean scan_only, VariableContext classScope)
			throws TestExecutionError {
		// create a nested scope
		VariableContext methodScope = classScope.createNestedScope();
		// store the test method object
		methodScope.declareAndSetValue("etlunit_test_method", new ETLTestValueObjectImpl(method));

		if (!scan_only) {
			try {
				listener.begin(method, methodScope, 1);
			} catch (TestExecutionError err) {
				methodStatus.addError(err);
			} catch (TestAssertionFailure err) {
				methodStatus.addFailure(err);
			} catch (TestWarning err) {
				methodStatus.addWarning(err);
			}
		}

		try {
			try {
				runTestBody(method, beforeTestMethods, afterTestMethods, methodStatus, scan_only, methodScope);
			} catch (TestExecutionError err) {
				log.severe("", err);
				throw err;
			}
		} finally {
			if (!scan_only) {
				try {
					listener.end(method, methodScope, 1);
				} catch (TestExecutionError err) {
					methodStatus.addError(err);
				} catch (TestAssertionFailure err) {
					methodStatus.addFailure(err);
				} catch (TestWarning err) {
					methodStatus.addWarning(err);
				}
			}
		}
	}

	private void runTestBody(ETLTestMethod method, List<ETLTestMethod> beforeTestMethods, List<ETLTestMethod> afterTestMethods, StatusReporter.CompletionStatus methodStatus, boolean scan_only, VariableContext methodScope) throws TestExecutionError {
		// hit the before test methods
		Iterator<ETLTestMethod> bit = beforeTestMethods.iterator();

		while (bit.hasNext()) {
			ETLTestMethod bmethod = bit.next();

			mapLocal.setCurrentlyProcessingMethod(bmethod);

			broadcast(method, bmethod, methodStatus, scan_only, methodScope);
		}

		// test method
		if (!methodStatus.hasFailures()) {
			mapLocal.setCurrentlyProcessingMethod(method);

			broadcast(method, method, methodStatus, scan_only, methodScope);
		}

		// hit the after test methods
		Iterator<ETLTestMethod> ait = afterTestMethods.iterator();

		while (ait.hasNext()) {
			ETLTestMethod amethod = ait.next();

			mapLocal.setCurrentlyProcessingMethod(amethod);

			broadcast(method, amethod, methodStatus, scan_only, methodScope);
		}
	}

	private void broadcast(final ETLTestMethod concerning, ETLTestMethod next, StatusReporter.CompletionStatus status, boolean scan_only, final VariableContext methodContext)
			throws TestExecutionError {
		List<ETLTestAnnotation> methodDefaults = next.getAnnotations("@OperationDefault");
		List<ETLTestAnnotation> classDefaults = next.getTestClass().getAnnotations("@OperationDefault");

		final MapList<String, ETLTestAnnotation> defaultMap = new HashMapArrayList<String, ETLTestAnnotation>();

		// grab the method defaults.  Overlay if a class default is present
		for (ETLTestAnnotation mdefault : methodDefaults) {
			Map<String, ETLTestValueObject> value = mdefault.getValue().getValueAsMap();

			// get the operation name
			ETLTestValueObject opName = value.get("operation");

			String opNameValueAsString = opName.getValueAsString();

			defaultMap.getOrCreate(opNameValueAsString).add(mdefault);
		}

		// grab the class defaults first.  Just drop these in
		for (ETLTestAnnotation cdefault : classDefaults) {
			Map<String, ETLTestValueObject> value = cdefault.getValue().getValueAsMap();

			// get the operation name
			ETLTestValueObject opName = value.get("operation");

			defaultMap.getOrCreate(opName.getValueAsString()).add(cdefault);
		}

		List<ETLTestOperation> ops = next.getOperations();

		Iterator<ETLTestOperation> it = ops.iterator();

		while (it.hasNext()) {
			ETLTestOperation op = it.next();

			mapLocal.setCurrentlyProcessingTestOperation(op);

			if (director.accept(op) == ClassResponder.response_code.accept) {
				if (!scan_only) {
					try {
						ETLTestValueObject operands = resolveMergedOperationValue(defaultMap, op);

						final ExecutionContext econtext = new ExecutionContext() {
							@Override
							public void process(ETLTestOperation op, VariableContext vcontext)
									throws TestAssertionFailure, TestExecutionError, TestWarning {
								ETLTestValueObject operands = resolveMergedOperationValue(defaultMap, op);

								// handled or reject are both acceptable
								if (listener.process(concerning, op, operands, methodContext, this, 1) == ClassResponder.action_code.defer) {
									throw new TestExecutionError("Listener could not handle operation: "
											+ op.getOperationName()
											+ ": "
											+ (operands != null ? operands.getJsonNode() : "{}"), TestConstants.ERR_INVALID_OPERATION);
								}
							}
						};

						methodContext.declareAndSetValue("etlunit_test_operation", new ETLTestValueObjectImpl(op));
						listener.begin(concerning, op, operands, methodContext, econtext, 1);

						try {
							if (listener.process(concerning, op, operands, methodContext, econtext, 1) == ClassResponder.action_code.defer) {
								throw new TestExecutionError("Listener could not handle operation: " + op.getOperationName() + ": " + (
										operands != null
												? operands.getJsonNode()
												: "{}"), TestConstants.ERR_INVALID_OPERATION);
							}
						} finally {
							listener.end(concerning, op, operands, methodContext, econtext, 1);
						}
					} catch (TestAssertionFailure asrt) {
						status.addFailure(asrt);
					} catch (TestWarning warn) {
						status.addWarning(warn);
					} catch (TestExecutionError thr) {
						throw thr;
					} catch (Throwable thr) {
						throw new TestExecutionError("Untrapped error", TestConstants.ERR_UNCAUGHT_EXCEPTION, thr);
					}
				}
			} else {
				log.info("Test operation not accepted:  " + op.getQualifiedName());
			}
		}
	}

	private ETLTestValueObject resolveMergedOperationValue(MapList<String, ETLTestAnnotation> defaultMap, ETLTestOperation op) {
		// TODO - lookup this operation against the map and merge defaults if found
		List<ETLTestAnnotation> opDefaults = defaultMap.get(op.getOperationName());

		ETLTestValueObject toperands = op.getOperands();

		if (opDefaults != null) {
			for (ETLTestAnnotation opdef : opDefaults) {
				Map<String, ETLTestValueObject> annotValueAsMap = opdef.getValue().getValueAsMap();

				ETLTestValueObject defaultValue = annotValueAsMap.get("defaultValue");

				if (toperands != null) {
					Map<String, ETLTestValueObject> toperandsValueAsMap = toperands.getValueAsMap();

					// verify the matchWhen and doNotMatchWhen conditions

					boolean shouldMerge = true;

					// run through matchWhen first
					if (annotValueAsMap.containsKey("matchWhen")) {
						for (String matchProperty : annotValueAsMap.get("matchWhen").getValueAsListOfStrings()) {
							if (!toperandsValueAsMap.containsKey(matchProperty)) {
								shouldMerge = false;
								break;
							}
						}
					}

					if (annotValueAsMap.containsKey("doNotMatchWhen")) {
						for (String matchProperty : annotValueAsMap.get("doNotMatchWhen").getValueAsListOfStrings()) {
							if (toperandsValueAsMap.containsKey(matchProperty)) {
								shouldMerge = false;
								break;
							}
						}
					}

					if (shouldMerge) {
						// this is a left merge.  Defaults can't override actual arguments
						toperands = toperands.merge(defaultValue, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
					}
				} else {
					// if there is a matchwhen clause, then this always fails.  doNotMatchWhen is irrelevant
					if (!annotValueAsMap.containsKey("matchWhen")) {
						toperands = defaultValue;
					}
				}
			}
		}

		return toperands;
	}

	@Override
	public void reset() {
		locator.reset();
	}

	public VariableContext getVariableContext() {
		return context;
	}
}
