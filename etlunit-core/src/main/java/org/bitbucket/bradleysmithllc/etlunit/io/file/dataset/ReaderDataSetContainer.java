package org.bitbucket.bradleysmithllc.etlunit.io.file.dataset;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;

import java.io.*;
import java.net.URL;

public class ReaderDataSetContainer implements DataSetContainer
{
	public static boolean debug = false;

	private final PushbackReader source;
	private ReaderDataSet readerDataSet;
	private boolean hnext = false;
	private final DataFileManager dataFileManager;

	public ReaderDataSetContainer(DataFileManager manager, String resource) throws IOException {
		URL url = getClass().getResource("/" + resource);

		if (url == null)
		{
			throw new IllegalArgumentException("Resource not found " + resource);
		}

		source = new PushbackReader(new BufferedReader(new InputStreamReader(url.openStream())), 5);
		dataFileManager = manager;
	}

	public ReaderDataSetContainer(DataFileManager manager, Reader source) {
		this.source = new PushbackReader(new BufferedReader(source), 5);
		dataFileManager = manager;
	}

	@Override
	public boolean hasNext() throws IOException {
		// make sure successive calls don't eat characters
		if (hnext)
		{
			return true;
		}

		// skip any content waiting to be read
		if (readerDataSet != null)
		{
			readerDataSet.skipBody();
			readerDataSet = null;
		}

		// spin until reaching non-whitespace
		// eof means no more data
		int ch;

		while (true)
		{
			ch = source.read();

			if (ch == -1)
			{
				return false;
			}

			switch(ch)
			{
				case '\r':
				case ' ':
					continue;
				default:
					source.unread(ch);
					return hnext = true;
			}
		}
	}

	@Override
	public DataSet next() throws IOException {
		hnext = false;

		return readerDataSet = new ReaderDataSet(dataFileManager, source);
	}

	@Override
	public void close() throws IOException {
		source.close();
	}

	@Override
	public DataSet locate(String id) throws IOException {
		int c = 0;
		while (hasNext())
		{
			DataSet ds = next();

			JsonNode idN = ds.getProperties().get("id");

			if (idN != null && idN.getNodeType() != JsonNodeType.NULL)
			{
				JsonNodeType nodeType = idN.getNodeType();
				if (nodeType != JsonNodeType.STRING)
				{
					throw new IllegalArgumentException("Bad node type for id: " + nodeType + " on data set [" + c + "]");
				}

				String tid = idN.asText();

				if (tid.equals(id))
				{
					return ds;
				}
			}

			c++;
		}

		throw new IllegalArgumentException("Data Set with id '" + id + "' not found.");
	}
}
