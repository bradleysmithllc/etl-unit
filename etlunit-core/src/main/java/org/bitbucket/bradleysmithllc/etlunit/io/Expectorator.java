package org.bitbucket.bradleysmithllc.etlunit.io;

/*
 * #%L
 * etlunit-core
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.Incomplete;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Expectorator
{
	public static final long DEFAULT_TIMEOUT = 60000L;
	public static final long NO_TIMEOUT = 0L;

	private final Reader reader;
	private boolean readerClosed = false;
	private final Writer writer;

	private final char[] byteBuff = new char[4096];

	private final StringBuilder stringBuffer = new StringBuilder();

	public Expectorator(Reader reader, Writer writer)
	{
		this.reader = reader;
		this.writer = writer;
	}

	public String sayAndExpect(String command, String expression) throws IOException
	{
		try
		{
			return sayAndExpect(command, expression, DEFAULT_TIMEOUT);
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}

	public String sayAndExpect(String command, String expression, long timeout) throws IOException, InterruptedException
	{
		synchronized (byteBuff)
		{
			say(command);
			return expect(expression, timeout);
		}
	}

	public void say(String command) throws IOException
	{
		synchronized (byteBuff)
		{
			writer.write(command);
			writer.flush();
		}
	}

	public String expect(String expression) throws IOException
	{
		try
		{
			return expect(expression, DEFAULT_TIMEOUT);
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Incomplete(explanation = "Make this method not throw away input after the expectation")
	public String expect(String expression, long timeout) throws IOException, InterruptedException
	{
		long completionTime = timeout == NO_TIMEOUT ? Long.MAX_VALUE : (System.currentTimeMillis() + timeout);

		synchronized (byteBuff)
		{
			//stringBuffer.setLength(0);

			Pattern p = Pattern.compile(expression);

			while (System.currentTimeMillis() < completionTime)
			{
				if (!readerClosed && reader.ready())
				{
					int bytesRead = reader.read(byteBuff);

					if (bytesRead == -1)
					{
						readerClosed = true;
						if (stringBuffer.length() == 0)
						{
							throw new InterruptedException("Stream closed before expectation was met");
						}
					}
					else
					{
						stringBuffer.append(byteBuff, 0, bytesRead);
					}
				}

				// For now, check every time around in case chars were left over from previous output (E.G., script processing).
				Matcher m = p.matcher(stringBuffer);

				if (m.find())
				{
					// pass the entire output back, up to and including the expectation
					String prestring = stringBuffer.substring(0, m.end());
					String poststring = stringBuffer.substring(m.end());

					stringBuffer.setLength(0);
					stringBuffer.append(poststring);
					return prestring;
				}
				else if (readerClosed)
				{
					// match not found and reader closed - fail now since no further input is possible
					throw new InterruptedException("Stream did not produce the desired output before closing");
				}

				// poll and wait for 100ms - considering the generic nature of the stream I don't think anything
				// more specific can be done other than polling

				try
				{
					long timeout1 = Math.min(
							100L,
							Math.max(0, completionTime - System.currentTimeMillis())
																	);
					Thread.sleep(timeout1);
					/*
					byteBuff.wait(
						timeout1
					);
					 */
				}
				catch (InterruptedException e)
				{
					throw new RuntimeException("Who is interrupting me?", e);
				}
			}
		}

		throw new InterruptedException("Stream did not produce the desired output in the time allotted");
	}
}
