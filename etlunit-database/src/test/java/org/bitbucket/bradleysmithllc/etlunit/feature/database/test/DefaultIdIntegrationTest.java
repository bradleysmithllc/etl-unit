package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;

public class DefaultIdIntegrationTest extends BaseDatabaseIntegrationTest {
	private H2DBImplementation oracle;

	public void prepareDatabaseFeatureModule() throws IOException {
		oracle = new H2DBImplementation("oracle", temporaryFolder.newFolder());
		databaseFeatureModule.addDatabaseImplementation(oracle);
	}

	@Test
	public void testDifferentDatabaseImplementations() {
		startTest();
	}

	/**
	 * This one can't be verified when running multi pass
	 *
	 * @return
	 */
	@Override
	protected void assertions(TestResults res, int pass) {
		if (pass == 1) {
			Assert.assertEquals(1, h2impl.createCount);
			Assert.assertEquals(2, h2impl.preCleanTablesCount);

			// check these sorted to avoid directory-order differences
			Assert.assertEquals("[d_sqlserver_oracle, e_sqlserver_oracle]", h2impl.prepareOrder.toString());

			Assert.assertEquals(1, oracle.createCount);
			Assert.assertEquals(2, oracle.preCleanTablesCount);

			Assert.assertEquals("[d_sqlserver_oracle, e_sqlserver_oracle]", oracle.prepareOrder.toString());
		}
	}
}
