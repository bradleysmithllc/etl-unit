package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractDatabaseFeatureModuleTest extends BaseFeatureModuleTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	protected H2DBImplementation sqlserver;

	protected FileRuntimeSupport fileRuntimeSupport;
	protected DatabaseRuntimeSupport databaseRuntimeSupport;
	protected DatabaseFeatureModule databaseFeatureModule;

	@Inject
	public void receive(DatabaseRuntimeSupport support)
	{
		databaseRuntimeSupport = support;
	}

	@Inject
	public void receive(FileRuntimeSupport frs)
	{
		fileRuntimeSupport = frs;
	}

	@Inject
	public void receive(DatabaseFeatureModule frs) throws Exception {
		databaseFeatureModule = frs;
		sqlserver = new H2DBImplementation("sqlserver", temporaryFolder.newFolder());
		databaseFeatureModule.addDatabaseImplementation(sqlserver);

		prepareDatabaseFeatureModule();
	}

	@Override
	protected List<Feature> getTestFeatures() {
		return Arrays.asList(
				new DatabaseFeatureModule()
		);
	}

	protected void prepareDatabaseFeatureModule() throws Exception {}
}
