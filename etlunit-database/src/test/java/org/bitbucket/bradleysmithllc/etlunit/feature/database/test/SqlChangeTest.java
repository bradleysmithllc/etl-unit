package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.CatalogImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.DatabaseImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Table;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

public class SqlChangeTest extends BaseDatabaseIntegrationTest
{
	private FileRuntimeSupport fileRuntimeSupport;
	private boolean second = false;

	@Override
	protected boolean cleanWorkspace() {
		return false;
	}

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport manager)
	{
		fileRuntimeSupport = manager;
	}

	@Override
	public void prepareImplementation(DatabaseImplementation sqlserver) {
		h2impl.setRefresh(false);
	}

	@Override
	protected void prepareTestContext() {
		if (!second)
		{
			velocityContext.put("sql", "sql");
		}
		else
		{
			velocityContext.put("sql", "sql2");
		}
	}

	@Test
	public void secondChance() throws IOException {
		startTest();
		DatabaseConnection databaseConnection = databaseFeatureModule.getDatabaseConfiguration().getDatabaseConnection("edw");

		File databaseMetadataTarget = databaseRuntimeSupport.getCachedMetaData(databaseConnection);

		Assert.assertTrue(databaseMetadataTarget.exists());

		JsonNode nodeBefore = JsonLoader.fromFile(databaseMetadataTarget);

		Database databaseBefore = new DatabaseImpl(null);
		databaseBefore.fromJson(nodeBefore);

		second = true;
		startTest();

		Assert.assertTrue(databaseMetadataTarget.exists());

		JsonNode nodeAfter = JsonLoader.fromFile(databaseMetadataTarget);
		Database databaseAfter = new DatabaseImpl(null);
		databaseAfter.fromJson(nodeAfter);

		Table tblBefore = databaseBefore.getCatalog("__sql__").getSchema(null).getTable("sql");
		Table tblAfter = databaseAfter.getCatalog("__sql__").getSchema(null).getTable("sql");
		// compare the before and after - the third column should be the diff
		//JsonNode beforeThree = nodeBefore.get("columns").get(2);
		//JsonNode afterThree = nodeAfter.get("columns").get(2);

		Assert.assertEquals("raw", tblBefore.getTableColumns().get(2).getLogicalName());
		Assert.assertEquals("raw_upd", tblAfter.getTableColumns().get(2).getLogicalName());
	}

	/**
	 * Tests a lifecycle, so can't be run multiple times.
	 * @return
	 */
	@Override
	protected boolean multiPassSafe() {
		return false;
	}

	@Override
	protected void prepareTest() {
		// swap the sql script the second time around so it changes

		if (second)
		{
			File ds1 = databaseRuntimeSupport.getSourceScript(null, "SQL", "");
			File ds2 = databaseRuntimeSupport.getSourceScript(null, "SQL2", "");

			try {
				FileUtils.forceDelete(ds1);
				FileUtils.moveFile(ds2, ds1);
			} catch (IOException e) {
				e.printStackTrace();
				Assert.fail();
			}

			ds1 = fileRuntimeSupport.getReferenceFileSchema("sql");
			ds2 = fileRuntimeSupport.getReferenceFileSchema("sql2");

			try {
				FileUtils.forceDelete(ds1);
				FileUtils.moveFile(ds2, ds1);
			} catch (IOException e) {
				e.printStackTrace();
				Assert.fail();
			}
		}
	}
}
