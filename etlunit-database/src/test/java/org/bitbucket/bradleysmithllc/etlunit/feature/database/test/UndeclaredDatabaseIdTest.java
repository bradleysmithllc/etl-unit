package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class UndeclaredDatabaseIdTest extends AbstractDatabaseFeatureModuleTest {
	private boolean class_fail = true;
	private boolean method_fail = true;

	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
				.object()
				.key("features")
				.object()
				.key("database")
				.object()
				.key("database-definitions")
				.object()
				.key("edw")
				.object()
				.key("implementation-id")
				.value("sqlserver")
				.key("user-name")
				.value("user")
				.key("password")
				.value("pass")
				.endObject()
				.endObject()
				.endObject()
				.endObject()
				.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSource("database1.etlunit", "@Database(id: 'edw1') class test_1 {@Test(expected-error-id: 'ERR_INVALID_DATABASE_CONNECTION') class_fail(){}}");
		createSource("database2.etlunit", "class test_2 {@Database(id: 'edw1') @Test(expected-error-id: 'ERR_INVALID_DATABASE_CONNECTION') method_fail(){}}");
	}

	@Test
	public void testDifferentDatabaseImplementations() {
		TestResults results = startTest();

		Assert.assertEquals(2, results.getNumTestsSelected());
		Assert.assertEquals(2, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());

		Assert.assertFalse(class_fail);
		Assert.assertFalse(method_fail);
	}

	@Override
	protected void assertMethodCompletionStatus(ETLTestMethod method, StatusReporter.CompletionStatus status, int testNumber, int operationNumber) {
		if (status.hasError() && status.getErrors().toString().contains("Undeclared connection id")) {
			if (method.getName().equals("class_fail")) {
				class_fail = false;
			} else {
				method_fail = false;
			}
		}
	}
}
