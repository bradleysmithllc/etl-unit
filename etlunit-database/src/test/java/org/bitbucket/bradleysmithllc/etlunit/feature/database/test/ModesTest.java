package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ModesTest extends AbstractDatabaseFeatureModuleTest {
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("raw.ddl"))
											.endObject()
										.key("edw-II")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("raw.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSourceFromClasspath(null, null, "database_modes.etlunit");

		createResourceFromClasspath("sqlserver", "edw", "raw.ddl");
		createResourceFromClasspath("sqlserver", "edw-II", "raw.ddl");
		createSource(null, "data", "MASTER_BLAH.delimited", "1\t2\n3\t4");
	}

	@Test
	public void testDifferentDatabaseImplementations()
	{
		TestResults result = startTest();

		Assert.assertEquals(0, result.getMetrics().getNumberOfErrors());
		Assert.assertEquals(0, result.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, result.getMetrics().getNumberOfWarnings());

		//Assert.assertEquals(2, sqlserver.createCount);
		//Assert.assertEquals(2, sqlserver.prepareCount);
		//Assert.assertEquals("[src, tgt]", sqlserver.modes.toString());

		//Assert.assertEquals("[a_sqlserver_oracle]", sqlserver.prepareOrder.toString());
	}
}
