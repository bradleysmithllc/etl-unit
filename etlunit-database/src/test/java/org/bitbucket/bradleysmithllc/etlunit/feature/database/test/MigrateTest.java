package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class MigrateTest extends AbstractDatabaseFeatureModuleTest {
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("default-connection-id")
								.value("edw")
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("schema_truncate.ddl"))
											.endObject()
										.key("edw-2")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("schema_truncate.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSourceFromClasspath(null, "database_migrate.etlunit");
		createSourceFromClasspath(null, "data", "CUSTOM_TEST_TRUNCATE_STUFF.delimited");

		createResourceFromClasspath("sqlserver", "edw", "schema_truncate.ddl");
		createResourceFromClasspath("sqlserver", "edw-2", "schema_truncate.ddl");
	}

	@Test
	public void migrateTables()
	{
		TestResults results = startTest();

		Assert.assertEquals(9, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());

		//Assert.fail("This needs to be completed");
	}
}
