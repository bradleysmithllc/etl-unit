package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

public class DatabaseConfigurationTest {
	@Test(expected = IllegalArgumentException.class)
	public void testBadDefault() throws ParseException {
		DatabaseConfiguration dc = new DatabaseConfiguration(ETLTestParser.loadObject("{default-connection-id: 'edw', database-definitions: {}}"), "", "", "user", "uid");
	}

	@Test
	public void testConfigs() throws ParseException {
		DatabaseConfiguration dc = new DatabaseConfiguration(
			ETLTestParser.loadObject("{default-connection-id: 'edw'," +
				"default-implementation-id: 'sql', " +
				"database-definitions: {" +
					"edw: {" +
						"user-name: 'adminUser'," +
						"password: 'adminPass'" +
					"}," +
					"edw_ods: {" +
					"}" +
				"}}"), "", "", "", ""
		);

		Assert.assertEquals("edw", dc.getDefaultConnection().getId());
		Assert.assertEquals("sql", dc.getDefaultImplementationId());
		Assert.assertEquals("sql", dc.getDatabaseConnection("edw").getImplementationId());
		Assert.assertEquals(dc.getDatabaseConnection("edw"), dc.getDatabaseConnection("edw"));
		Assert.assertEquals("adminUser", dc.getDatabaseConnection("edw").getAdminUserName());
		Assert.assertEquals("adminPass", dc.getDatabaseConnection("edw").getAdminPassword());

		Assert.assertEquals(dc.getDatabaseConnection("edw_ods"), dc.getDatabaseConnection("edw_ods"));
	}

	@Test
	public void testDelimiters() throws ParseException {
		DatabaseConfiguration dc = new DatabaseConfiguration(
			ETLTestParser.loadObject(
				"{" +
					"default-implementation-id: 'sql', " +
					"database-definitions: {" +
						"edw: {" +
						"}," +
						"edw_ods: {" +
						"}" +
					"}" +
				"}"
			), "", "", "", ""
		);

		Assert.assertEquals("\t", dc.getDataFileColumnDelimiter());
		Assert.assertEquals("\n", dc.getDataFileRowDelimiter());

		dc = new DatabaseConfiguration(
			ETLTestParser.loadObject(
				"{" +
					"data-file-column-delimiter: ','," +
					"data-file-row-delimiter: '|'," +
					"default-implementation-id: 'sql', " +
					"database-definitions: {" +
						"edw: {" +
						"}," +
						"edw_ods: {" +
							"}" +
						"}" +
					"}"
			), "", "", "", ""
		);

		Assert.assertEquals(",", dc.getDataFileColumnDelimiter());
		Assert.assertEquals("|", dc.getDataFileRowDelimiter());
	}
}
