package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class AssertFailureIdsTest extends AbstractDatabaseFeatureModuleTest {
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("default-connection-id")
								.value("edw")
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
													.key("user-name")
													.value("user")
													.key("password")
													.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("test_schema_assert.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSource("database1.etlunit", "@Database(id: 'edw') class test1 " +
			"{ " +
				"@Test(expected-failure-ids: ['FAIL_CUSTOM']) " +
				"assertNotEmpty()" +
				"{" +
					"assert(source-table: 'TABLEPASS', assertion-mode: 'notEmpty', failure-id: 'FAIL_CUSTOM');" +
				"}" +
				"@Test(expected-failure-ids: ['FAIL_CUSTOM_2']) " +
				"assertNotEmptyWithSchema()" +
				"{" +
					"assert(source-table: 'TABLEFAIL', assertion-mode: 'notEmpty', source-schema: 'EMPTY', failure-id: 'FAIL_CUSTOM_2');" +
				"}" +
			"}"
		);

		createSource("", "data", "FILE.delimited", "ID\nINTEGER");

		createResourceFromClasspath("sqlserver", "edw", "test_schema_assert.ddl");
	}

	@Test
	public void databaseAssertionsPassThroughToImplementation()
	{
		TestResults results = startTest();

		Assert.assertEquals(2, results.getNumTestsSelected());
		Assert.assertEquals(2, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
	}
}
