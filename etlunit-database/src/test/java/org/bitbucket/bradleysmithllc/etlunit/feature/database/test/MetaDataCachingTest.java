package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

public class MetaDataCachingTest extends BaseDatabaseIntegrationTest
{
	private boolean prepareDatabaseOption = true;
	private boolean forceDatabaseOption = false;

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides() {
		return Arrays.asList(
			new RuntimeOption("database.prepareDatabase", prepareDatabaseOption),
			new RuntimeOption("database.forceInitializeDatabase", forceDatabaseOption)
		);
	}

	@Test
	public void prepareDatabaseOn()
	{
		prepareDatabaseOption = true;
		forceDatabaseOption = false;
		TestResults result = startTest();
		result = startTest();
	}

	//@Test
	public void prepareDatabaseOff()
	{
		prepareDatabaseOption = true;
		forceDatabaseOption = false;
		TestResults result = startTest();
		prepareDatabaseOption = false;
		result = startTest();
	}

	@Test
	public void forceDatabaseOn()
	{
		prepareDatabaseOption = true;
		forceDatabaseOption = true;
		TestResults result = startTest();
		result = startTest();
	}

	//@Test
	public void forceDatabaseOff()
	{
		prepareDatabaseOption = true;
		forceDatabaseOption = false;
		TestResults result = startTest();
		prepareDatabaseOption = false;
		result = startTest();
	}

	@Override
	protected boolean cleanWorkspace() {
		return false;
	}
}
