package org.bitbucket.bradleysmithllc.etlunit.feature.database.regexp.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DDLDirectiveNameExpression;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DDLInstrumentationExpression;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DDLReferenceNameExpression;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.SQLCallExpression;
import org.junit.Assert;
import org.junit.Test;

public class RegexpTest
{
	@Test
	public void directives()
	{
		DDLDirectiveNameExpression ddl = new DDLDirectiveNameExpression("");
		Assert.assertFalse(ddl.hasNext());

		ddl = DDLDirectiveNameExpression.match("#include<hello>");
		Assert.assertTrue(ddl.matches());
		Assert.assertEquals("include", ddl.getDirective());

		ddl = DDLDirectiveNameExpression.match("#	include	<	hello/goodbye	>");
		Assert.assertTrue(ddl.matches());
		Assert.assertEquals("include", ddl.getDirective());

		ddl = DDLDirectiveNameExpression.match(" \t# \tinclude \t< \thello \t> \t");
		Assert.assertTrue(ddl.matches());
		Assert.assertEquals("include", ddl.getDirective());

		ddl = DDLDirectiveNameExpression.match("#	require	<	hello	>");
		Assert.assertTrue(ddl.matches());
		Assert.assertEquals("require", ddl.getDirective());
		Assert.assertEquals("hello", ddl.getDdlReference());

		ddl = DDLDirectiveNameExpression.match("#	provide	<	hello.txt	>");
		Assert.assertTrue(ddl.matches());
		Assert.assertEquals("provide", ddl.getDirective());
		Assert.assertEquals("hello.txt", ddl.getDdlReference());
	}

	@Test
	public void ddlInstrumentation()
	{
		DDLInstrumentationExpression ddlie = new DDLInstrumentationExpression("/**\tDDL Source [test_script.sql:11]\t**/");

		Assert.assertTrue(ddlie.matches());
		Assert.assertEquals("test_script.sql", ddlie.getRef());
		Assert.assertEquals(11, ddlie.getLineNo());

		ddlie = new DDLInstrumentationExpression("/**\tDDL Source [edw/test_script.sql:0]\t**/");

		Assert.assertTrue(ddlie.matches());
		Assert.assertEquals("edw/test_script.sql", ddlie.getRef());
		Assert.assertEquals(0, ddlie.getLineNo());

		ddlie = new DDLInstrumentationExpression("/**\tDDL Source [edw\\edw/test_script.sql:1000]\t**/");

		Assert.assertTrue(ddlie.matches());
		Assert.assertEquals("edw\\edw/test_script.sql", ddlie.getRef());
		Assert.assertEquals(1000, ddlie.getLineNo());

		ddlie = new DDLInstrumentationExpression("\n\r\n\n/**\tDDL Source [test_script.sql:11]\t**/\n\n\n");

		Assert.assertTrue(ddlie.hasNext());
		Assert.assertEquals("\n", ddlie.getNewLine());
		Assert.assertFalse(ddlie.hasRef());
		Assert.assertFalse(ddlie.hasLineNo());
		Assert.assertTrue(ddlie.hasNext());
		Assert.assertEquals("\r\n", ddlie.getNewLine());
		Assert.assertFalse(ddlie.hasRef());
		Assert.assertFalse(ddlie.hasLineNo());
		Assert.assertTrue(ddlie.hasNext());
		Assert.assertEquals("\n", ddlie.getNewLine());
		Assert.assertFalse(ddlie.hasRef());
		Assert.assertFalse(ddlie.hasLineNo());

		Assert.assertTrue(ddlie.hasNext());
		Assert.assertFalse(ddlie.hasNewLine());
		Assert.assertTrue(ddlie.hasRef());
		Assert.assertTrue(ddlie.hasLineNo());
		Assert.assertEquals("test_script.sql", ddlie.getRef());
		Assert.assertEquals(11, ddlie.getLineNo());
		Assert.assertTrue(ddlie.hasNext());
		Assert.assertEquals("\n", ddlie.getNewLine());
		Assert.assertFalse(ddlie.hasRef());
		Assert.assertFalse(ddlie.hasLineNo());
		Assert.assertTrue(ddlie.hasNext());
		Assert.assertEquals("\n", ddlie.getNewLine());
		Assert.assertFalse(ddlie.hasRef());
		Assert.assertFalse(ddlie.hasLineNo());
		Assert.assertTrue(ddlie.hasNext());
		Assert.assertEquals("\n", ddlie.getNewLine());
		Assert.assertFalse(ddlie.hasRef());
		Assert.assertFalse(ddlie.hasLineNo());
		Assert.assertFalse(ddlie.hasNext());


		ddlie = new DDLInstrumentationExpression("/**\tDDL Source [sqlserver/edw/test_script.sql:1]\t**/");
		Assert.assertTrue(ddlie.matches());
		Assert.assertEquals("sqlserver/edw/test_script.sql", ddlie.getRef());
	}

	@Test
	public void splitTest()
	{
		String [] input = {
				"",
				"Hello",
				"Hello;;",
				"Hello;;Hello",
				";;H;;H;;;;;;l",
				";;",
				"Hello;;",
				"Hello;;;;",
				"Hello;;Hello;;",
				";;H;;H;;;;;;l;;"
		};

		for (String in : input)
		{
			String [] splits = in.split(";;", -1);

			//System.out.println(Arrays.asList(splits));
		}
	}

	@Test
	public void testSql()
	{
		SQLCallExpression sqce = new SQLCallExpression("run");
		Assert.assertFalse(sqce.hasNext());

		sqce = new SQLCallExpression("     run      ");
		Assert.assertFalse(sqce.hasNext());

		sqce = new SQLCallExpression("run()");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("run(?,?)");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("run(?)");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("run(5)");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("run(5,5)");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("run(?,5)");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("run(5,?)");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("   run   ( )     ");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression(" run  (    ? ,      ?     )");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("     run (    ?     ) ");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("     run     ( 5      ) ");
		Assert.assertTrue(sqce.hasNext());

		sqce = new SQLCallExpression("     run       ( 5        ,     5      )     ");
		Assert.assertTrue(sqce.hasNext());
	}

	@Test
	public void testImplIdDbIdScope()
	{
		DDLReferenceNameExpression ddlrne = new DDLReferenceNameExpression("/sqlserver/edw/test_script.sql");
		Assert.assertTrue(ddlrne.matches());

		Assert.assertEquals("sqlserver", ddlrne.getDbImplementationId());
		Assert.assertEquals("edw", ddlrne.getDbId());
		Assert.assertFalse(ddlrne.hasAbsolute());
		Assert.assertEquals("test_script.sql", ddlrne.getDdlReference());
	}

	@Test
	public void testImplIdScope()
	{
		DDLReferenceNameExpression ddlrne = new DDLReferenceNameExpression("/sqlserver/test_script.sql");
		Assert.assertTrue(ddlrne.matches());

		Assert.assertEquals("sqlserver", ddlrne.getDbImplementationId());
		Assert.assertFalse(ddlrne.hasDbId());
		Assert.assertFalse(ddlrne.hasAbsolute());
		Assert.assertEquals("test_script.sql", ddlrne.getDdlReference());
	}

	@Test
	public void testDbIdScope()
	{
		DDLReferenceNameExpression ddlrne = new DDLReferenceNameExpression("edw/test_script.sql");
		Assert.assertTrue(ddlrne.matches());

		Assert.assertFalse(ddlrne.hasDbImplementationId());
		Assert.assertEquals("edw", ddlrne.getDbId());
		Assert.assertFalse(ddlrne.hasAbsolute());
		Assert.assertEquals("test_script.sql", ddlrne.getDdlReference());
	}

	@Test
	public void testRefScope()
	{
		DDLReferenceNameExpression ddlrne = new DDLReferenceNameExpression("test_script.sql");
		Assert.assertTrue(ddlrne.matches());

		Assert.assertFalse(ddlrne.hasDbImplementationId());
		Assert.assertFalse(ddlrne.hasDbId());
		Assert.assertFalse(ddlrne.hasAbsolute());
		Assert.assertEquals("test_script.sql", ddlrne.getDdlReference());
	}

	@Test
	public void testAbsoluteRefScope()
	{
		DDLReferenceNameExpression ddlrne = new DDLReferenceNameExpression("/test_script.sql");
		Assert.assertTrue(ddlrne.matches());

		Assert.assertFalse(ddlrne.hasDbImplementationId());
		Assert.assertFalse(ddlrne.hasDbId());
		Assert.assertTrue(ddlrne.hasAbsolute());
		Assert.assertEquals("test_script.sql", ddlrne.getDdlReference());
	}
}
