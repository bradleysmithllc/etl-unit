package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.BaseDatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.hsqldb.jdbc.JDBCDriver;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class HSQLDBImplementation extends BaseDatabaseImplementation
{
	private static int nextCode = 0;

	private int code = nextCode++;

	private final String id;
	int createCount = 0;
	int completeCreateCount = 0;
	int preCreateSchemaCount = 0;
	int postCreateSchemaCount = 0;
	int dropConstraintsCount = 0;
	int materializeViewsCount = 0;
	int preCleanTablesCount = 0;
	int postCleanTablesCount = 0;

	final Set<String> prepareOrder = Collections.synchronizedSet(new TreeSet<>());
	final Set<String> modes = Collections.synchronizedSet(new TreeSet<>());

	private boolean refresh = true;
	private final File tableSpace;

	// default for HSQLDB
	private String defaultSchema = "PUBLIC";

	public HSQLDBImplementation(String id) {
		this.id = id;
		tableSpace = null;
	}

	public HSQLDBImplementation(String id, File tableSpace) {
		this.id = id;
		this.tableSpace = tableSpace;
	}

	public String getImplementationId() {
		return id;
	}

	@Override
	protected boolean useInformationSchemaConstraints() {
		return false;
	}

	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException {
		switch (op) {
			case createDatabase:
				// if using a file database, delete the file when this happens
				if (tableSpace != null)
				{
					File file = getFile(request.getInitializeRequest().getConnection(), request.getInitializeRequest().getMode());
					File script = new File(file.getAbsolutePath() + ".script");

					if (script.exists())
					{
						try {
							FileUtils.forceDelete(script);
						} catch (IOException e) {
							throw new RuntimeException("Failed to delete database script file", e);
						}
					}

					File properties = new File(file.getAbsolutePath() + ".properties");

					if (properties.exists())
					{
						try {
							FileUtils.forceDelete(properties);
						} catch (IOException e) {
							throw new RuntimeException("Failed to delete database properties file", e);
						}
					}
				}

				createCount++;
				return null;
			case completeDatabaseInitialization:
				completeCreateCount++;
				return null;
			default:
				throw new UnsupportedOperationException(op.name() + " is unsupported");
			case preCreateSchema:
				preCreateSchemaCount++;
				return null;
			case postCreateSchema:
				postCreateSchemaCount++;
				return null;
			case dropConstraints:
				dropConstraintsCount++;
				return null;
			case materializeViews:
				materializeViewsCount++;
				return null;
			case preCleanTables:
				DatabaseRequest prepare = request.getDatabaseRequest();

				preCleanTablesCount++;

				String mode = null;

				if (prepare.getMode() == null) {
					mode = "NULL";
				} else {
					mode = prepare.getMode();
				}

				checkPrepare(prepare.getTestMethod().getName());

				modes.add(mode);
				return null;
			case resetIdentities:
				return null;
			case postCleanTables:
				prepare = request.getDatabaseRequest();

				postCleanTablesCount++;

				mode = null;

				if (prepare.getMode() == null) {
					mode = "NULL";
				} else {
					mode = prepare.getMode();
				}

				checkPrepare(prepare.getTestMethod().getName());

				modes.add(mode);
				return null;
		}
	}

	private synchronized void checkPrepare(String name) {
		if (!prepareOrder.contains(name)) {
			prepareOrder.add(name);
		}
	}

	@Override
	public String getJdbcUrl(DatabaseConnection dc, String mode, database_role id)
	{
		if (tableSpace == null)
		{
			return "jdbc:hsqldb:mem:/" + getDatabaseName(dc, mode) + "_" + code + ";shutdown=true";
		}

		return "jdbc:hsqldb:file:" + getFile(dc, mode).getAbsolutePath() + ";shutdown=true";
	}

	protected File getFile(DatabaseConnection dc, String mode) {
		return new File(tableSpace, getDatabaseName(dc, mode) + ".dat");
	}

	@Override
	public Class getJdbcDriverClass()
	{
		return JDBCDriver.class;
	}

	public String getDefaultSchema(DatabaseConnection dc, String mode)
	{
		return defaultSchema;
	}

	@Override
	public database_state getDatabaseState(DatabaseConnection databaseConnection, String s, Database database) {
		return refresh ? database_state.fail : database_state.pass;
	}

	public void setDefaultSchema(String defaultSchema) {
		this.defaultSchema = defaultSchema;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	@Override
	protected String createScriptToMaterializeViewToTable(String sourceSchema, String sourceView, String targetSchema, String targetName) {
		return "CREATE TABLE " + targetSchema + "." + targetName + " AS (SELECT * FROM " + sourceSchema + "." + sourceView + " WHERE 1 = 0) WITH DATA;";
	}
}
