package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.*;
import org.junit.Assert;
import org.junit.Test;

public class BaseDatabaseImplementationTest {
	private DatabaseImplementation uni = new HSQLDBImplementation(""){
		@Override
		protected String escapeChar() {
			return "~";
		}
	};

	private DatabaseImplementation dui = new HSQLDBImplementation(""){
		@Override
		protected String openEscapeChar() {
			return ">";
		}

		@Override
		protected String closeEscapeChar() {
			return "<";
		}
	};

	@Test
	public void beforeAndAfter() {

		Assert.assertEquals(">l l<", dui.escapeIdentifierIfNeeded("l l"));
	}

	@Test
	public void mixedCase() {
		Assert.assertEquals("~HelloKitty~", uni.escapeIdentifierIfNeeded("HelloKitty"));
		Assert.assertEquals("hellokitty", uni.escapeIdentifierIfNeeded("hellokitty"));
		Assert.assertEquals("HELLOKITTY", uni.escapeIdentifierIfNeeded("HELLOKITTY"));
	}

	@Test
	public void same() {
		Assert.assertEquals("~l l~", uni.escapeIdentifierIfNeeded("l l"));
	}

	@Test
	public void justUnderscore() {
		Assert.assertEquals("_", uni.escapeIdentifierIfNeeded("_"));
	}

	@Test
	public void identifierWithUnderscore() {
		Assert.assertEquals("bradley_smith", uni.escapeIdentifierIfNeeded("bradley_smith"));
	}

	@Test
	public void identifierWithUnderscoreFirst() {
		Assert.assertEquals("_BRADLEY_SMITH", uni.escapeIdentifierIfNeeded("_BRADLEY_SMITH"));
	}

	@Test
	public void qualifiedWithUnderscore() {
		Catalog c = new CatalogImpl(null, "BBB_C");
		Schema s = new SchemaImpl(c, "BBB_S");
		Table t = new TableImpl(c, s, "BBB_T", Table.type.table);

		Assert.assertEquals("BBB_C.BBB_S.BBB_T", uni.escapeQualifiedIdentifier(t));
	}

	@Test
	public void qualifiedWithUnderscoreFirst() {
		Catalog c = new CatalogImpl(null, "_BBB_C_");
		Schema s = new SchemaImpl(c, "_BBB_S_");
		Table t = new TableImpl(c, s, "_BBB_T_", Table.type.table);

		Assert.assertEquals("_BBB_C_._BBB_S_._BBB_T_", uni.escapeQualifiedIdentifier(t));
	}

	@Test
	public void firstCharIsNumeric() {
		Assert.assertEquals("~1~", uni.escapeIdentifierIfNeeded("1"));
		Assert.assertEquals("~1A~", uni.escapeIdentifierIfNeeded("1A"));
		Assert.assertEquals("A1", uni.escapeIdentifierIfNeeded("A1"));
	}

	@Test
	public void chars() {
		Assert.assertEquals("~ ~", uni.escapeIdentifierIfNeeded(" "));
		Assert.assertEquals("~,~", uni.escapeIdentifierIfNeeded(","));
		Assert.assertEquals("~.~", uni.escapeIdentifierIfNeeded("."));
		Assert.assertEquals("~?~", uni.escapeIdentifierIfNeeded("?"));
		Assert.assertEquals("~'~", uni.escapeIdentifierIfNeeded("'"));
		Assert.assertEquals("~-~", uni.escapeIdentifierIfNeeded("-"));
		Assert.assertEquals("~\\~", uni.escapeIdentifierIfNeeded("\\"));
		Assert.assertEquals("~/~", uni.escapeIdentifierIfNeeded("/"));
	}
}
