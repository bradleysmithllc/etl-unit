package org.bitbucket.bradleysmithllc.etlunit.feature.database.test.db.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Table;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.TableColumn;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.TableColumnImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.TableImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TableTest
{

	private Table table;

	@Before
	public void createTable()
	{
		table = new TableImpl(null, null, "name", Table.type.table);
	}

	@Test
	public void hasColumn()
	{
		Assert.assertFalse(table.hasColumn("ID"));

		TableColumn tc = new TableColumnImpl(table, "ID", 0, false, 0, 0, false);
		table.addColumn(tc);

		Assert.assertTrue(table.hasColumn("ID"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void getColumnFails()
	{
		table.getColumn("");
	}

	@Test(expected = NullPointerException.class)
	public void getColumnFailsNull()
	{
		table.getColumn(null);
	}

	@Test(expected = NullPointerException.class)
	public void addPKFailsNull()
	{
		table.addPrimaryKeyColumn(null);
	}

	@Test(expected = NullPointerException.class)
	public void addOKFailsNull()
	{
		table.addOrderKeyColumn(null);
	}
}
