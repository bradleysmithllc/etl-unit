package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class MissingImplementationTest extends AbstractDatabaseFeatureModuleTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("database-definitions")
									.object()
										.key("pos_alx")
											.object()
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSource("database.etlunit", "class test {@Database(id: 'pos_alx') @Test defaultImpl(){}}");
	}

	@Test
	public void testDifferentDatabaseImplementations()
	{
		expectedException.expect(RuntimeException.class);
		expectedException.expectMessage("implementation-id is not specified and no default is provided");
		startTest();
	}
}
