package org.bitbucket.bradleysmithllc.etlunit.feature.database.test.util;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.util.DimensionalHashMap;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.util.DimensionalMap;
import org.junit.Assert;
import org.junit.Test;

public class DimensionalHashMapTest {
	@Test
	public void testMap()
	{
		DimensionalMap<String, Integer, Long> map = new DimensionalHashMap<String, Integer, Long>();

		map.put("1", new Integer(1), new Long(7L));

		Assert.assertEquals(new Long(7L), map.get("1", new Integer(1)));
	}

	@Test
	public void nullIsAValidKey()
	{
		DimensionalMap<String, Integer, Long> map = new DimensionalHashMap<String, Integer, Long>();

		map.put("1", new Integer(1), new Long(7L));
		map.put("2", null, new Long(8L));
		map.put(null, new Integer(7), new Long(9L));

		Assert.assertEquals(new Long(7L), map.get("1", new Integer(1)));
		Assert.assertEquals(new Long(8L), map.get("2", null));
		Assert.assertEquals(new Long(9L), map.get(null, new Integer(7)));
	}
}
