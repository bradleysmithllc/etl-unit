package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.RelationalColumn;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.RelationalDataSet;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.extract.ExtractRequest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * ToDO Migrate to Table
 * */
public class RelationalDatSetTest
{
	@Test
	public void testOrdersAndKeysSQLTable()
	{
		TableImpl rds = new TableImpl(null, null, "src", Table.type.synthetic);
		rds.setSqlScriptSource("SELECT * FROM TABLE");

		Assert.assertEquals("SELECT * FROM TABLE", rds.createSQLSelect());
		Assert.assertEquals("SELECT * FROM TABLE", rds.createSQLSelect(true));
		Assert.assertEquals("SELECT * FROM TABLE", rds.createSQLSelect(false));

		TableColumn rc = new TableColumnImpl(rds, "NAME", 0, false, 0, 0, false);
		rds.addColumn(rc);

		rds.addPrimaryKeyColumn(rc);

		Assert.assertEquals("SELECT * FROM TABLE", rds.createSQLSelect());
		Assert.assertEquals("SELECT * FROM TABLE", rds.createSQLSelect(true));
		Assert.assertEquals("SELECT * FROM TABLE", rds.createSQLSelect(false));
	}

	@Test
	public void testOrdersAndKeysRelationalDataSet()
	{
		Table rds = new TableImpl(null, new SchemaImpl(null, "PUBLIC"), "TABLE", Table.type.table);

		TableColumn rc = new TableColumnImpl(rds, "NAME", 0, false, 0, 0, false);
		rds.addColumn(rc);

		Assert.assertEquals("SELECT\n" +
			"\tNAME\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE\n" +
			"ORDER BY\n" +
			"\tNAME\n", rds.createSQLSelect());
		Assert.assertEquals("SELECT\n" +
			"\tNAME\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE\n" +
			"ORDER BY\n" +
			"\tNAME\n", rds.createSQLSelect(true));
		Assert.assertEquals("SELECT\n" +
			"\tNAME\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE", rds.createSQLSelect(false));

		rds.addPrimaryKeyColumn(rc);

		Assert.assertEquals("SELECT\n" +
			"\tNAME\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE\n" +
			"ORDER BY\n" +
			"\tNAME\n", rds.createSQLSelect());
		Assert.assertEquals("SELECT\n" +
			"\tNAME\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE\n" +
			"ORDER BY\n" +
			"\tNAME\n", rds.createSQLSelect(true));
		Assert.assertEquals("SELECT\n" +
			"\tNAME\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE", rds.createSQLSelect(false));

		rc = new TableColumnImpl(rds, "NAME_II", 0, false, 0, 0, false);
		rds.addColumn(rc);

		Assert.assertEquals("SELECT\n" +
			"\tNAME,\n" +
			"\tNAME_II\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE\n" +
			"ORDER BY\n" +
			"\tNAME\n", rds.createSQLSelect());
		Assert.assertEquals("SELECT\n" +
			"\tNAME,\n" +
			"\tNAME_II\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE\n" +
			"ORDER BY\n" +
			"\tNAME\n", rds.createSQLSelect(true));
		Assert.assertEquals("SELECT\n" +
			"\tNAME,\n" +
			"\tNAME_II\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE", rds.createSQLSelect(false));

		rds.addOrderKeyColumn(rc);

		Assert.assertEquals("SELECT\n" +
			"\tNAME,\n" +
			"\tNAME_II\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE\n" +
			"ORDER BY\n" +
			"\tNAME_II\n", rds.createSQLSelect());
		Assert.assertEquals("SELECT\n" +
			"\tNAME,\n" +
			"\tNAME_II\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE\n" +
			"ORDER BY\n" +
			"\tNAME_II\n", rds.createSQLSelect(true));
		Assert.assertEquals("SELECT\n" +
			"\tNAME,\n" +
			"\tNAME_II\n" +
			"FROM\n" +
			"\tPUBLIC.TABLE", rds.createSQLSelect(false));
	}

	class DBIMPL extends HSQLDBImplementation {
		public DBIMPL() {
			super("id");
		}

		@Override
		public String escapeIdentifierIfNeeded(String name) {
			return "_" + name + "_";
		}
	}

	@Test
	public void overrideSqlNullSchema()
	{
		Table rds = new TableImpl(null, new SchemaImpl(null, null), "TABLE", Table.type.table);

		TableColumn rc = new TableColumnImpl(rds, "NAME", 0, false, 0, 0, false);
		rds.addColumn(rc);

		Assert.assertEquals("SELECT\n" +
				"\tNAME\n" +
				"FROM\n" +
				"\tTABLE\n" +
				"ORDER BY\n" +
				"\tNAME\n", rds.createSQLSelect());

		Assert.assertEquals("SELECT\n" +
				"\tNAME\n" +
				"FROM\n" +
				"\tsch.tbl\n" +
				"ORDER BY\n" +
				"\tNAME\n", rds.createSQLSelect((name) -> {return name;}, true, ImmutablePair.of("sch", "tbl")));

		Assert.assertEquals("SELECT\n" +
				"\tNAME\n" +
				"FROM\n" +
				"\tTABLE", rds.createSQLSelect(false));

		Assert.assertEquals("SELECT\n" +
				"\tNAME\n" +
				"FROM\n" +
				"\tsch.tbl", rds.createSQLSelect((name) -> {return name;}, false, ImmutablePair.of("sch", "tbl")));

		Assert.assertEquals("SELECT\n" +
				"\t_NAME_\n" +
				"FROM\n" +
				"\t_TABLE_\n" +
				"ORDER BY\n" +
				"\t_NAME_\n", rds.createSQLSelect(new DBIMPL()));

		Assert.assertEquals("SELECT\n" +
				"\t_NAME_\n" +
				"FROM\n" +
				"\t_sch_._tbl_\n" +
				"ORDER BY\n" +
				"\t_NAME_\n", rds.createSQLSelectUsingAlternateTableName(new DBIMPL(), ImmutablePair.of("sch", "tbl")));

		Assert.assertEquals("SELECT\n" +
				"\t_NAME_\n" +
				"FROM\n" +
				"\t_TABLE_", rds.createSQLSelect(new DBIMPL(), false, null));

		Assert.assertEquals("SELECT\n" +
				"\t_NAME_\n" +
				"FROM\n" +
				"\t_sch_._tbl_", rds.createSQLSelect(new DBIMPL(), false, ImmutablePair.of("sch", "tbl")));
	}

	@Test
	public void overrideSqlSchema()
	{
		Table rds = new TableImpl(null, new SchemaImpl(null, "SCHEMA"), "TABLE", Table.type.table);

		TableColumn rc = new TableColumnImpl(rds, "NAME", 0, false, 0, 0, false);
		rds.addColumn(rc);

		Assert.assertEquals("SELECT\n" +
				"\tNAME\n" +
				"FROM\n" +
				"\tSCHEMA.TABLE\n" +
				"ORDER BY\n" +
				"\tNAME\n", rds.createSQLSelect());

		Assert.assertEquals("SELECT\n" +
				"\tNAME\n" +
				"FROM\n" +
				"\tsch.tbl\n" +
				"ORDER BY\n" +
				"\tNAME\n", rds.createSQLSelect((name) -> {return name;}, true, ImmutablePair.of("sch", "tbl")));

		Assert.assertEquals("SELECT\n" +
				"\tNAME\n" +
				"FROM\n" +
				"\tSCHEMA.TABLE", rds.createSQLSelect(false));

		Assert.assertEquals("SELECT\n" +
				"\tNAME\n" +
				"FROM\n" +
				"\tsch.tbl", rds.createSQLSelect((name) -> {return name;}, false, ImmutablePair.of("sch", "tbl")));

		Assert.assertEquals("SELECT\n" +
				"\t_NAME_\n" +
				"FROM\n" +
				"\t_SCHEMA_._TABLE_\n" +
				"ORDER BY\n" +
				"\t_NAME_\n", rds.createSQLSelect(new DBIMPL()));

		Assert.assertEquals("SELECT\n" +
				"\t_NAME_\n" +
				"FROM\n" +
				"\t_sch_._tbl_\n" +
				"ORDER BY\n" +
				"\t_NAME_\n", rds.createSQLSelectUsingAlternateTableName(new DBIMPL(), ImmutablePair.of("sch", "tbl")));

		Assert.assertEquals("SELECT\n" +
				"\t_NAME_\n" +
				"FROM\n" +
				"\t_SCHEMA_._TABLE_", rds.createSQLSelect(new DBIMPL(), false, null));

		Assert.assertEquals("SELECT\n" +
				"\t_NAME_\n" +
				"FROM\n" +
				"\t_sch_._tbl_", rds.createSQLSelect(new DBIMPL(), false, ImmutablePair.of("sch", "tbl")));
	}

	@Test
	public void escapes()
	{
		Table rds = new TableImpl(null, new SchemaImpl(null, "SCHE mA"), "Ta BLE", Table.type.table);

		TableColumn rc = new TableColumnImpl(rds, " NaME ", 0, false, 0, 0, false);
		rds.addColumn(rc);

		Assert.assertEquals("SELECT\n" +
				"\t| NaME |\n" +
				"FROM\n" +
				"\t|SCHE mA|.|Ta BLE|\n" +
				"ORDER BY\n" +
				"\t| NaME |\n", rds.createSQLSelect((name) -> {return "|" + name + "|";}));

		Assert.assertEquals("INSERT INTO\n" +
				"\t|SCHE mA|.|Ta BLE|\n" +
				"(\n" +
				"\t| NaME |\n" +
				")\n" +
				"VALUES\n" +
				"(\n" +
				"\t?\n" +
				")", rds.createSQLInsert((name) -> {return "|" + name + "|";}));
	}

	@Test(expected = IllegalArgumentException.class)
	public void addPKDoesntExist()
	{
		RelationalDataSet rds = new RelationalDataSet("SELECT * FROM TABLE", null, "");

		RelationalColumn rc = new RelationalColumn("NAME", 0, false, false);

		rds.addPrimaryKeyColumn(rc);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addOrderByDoesntExist()
	{
		RelationalDataSet rds = new RelationalDataSet("SELECT * FROM TABLE", null, "");

		RelationalColumn rc = new RelationalColumn("NAME", 0, false, false);

		rds.addOrderByColumn(rc);
	}

	@Test
	public void orderInSubViews()
	{
		RelationalDataSet rds = new RelationalDataSet("PUBLIC", "TABLE", null);

		RelationalColumn rc = new RelationalColumn("NAME", 0, false, false);
		rds.addColumn(rc);
		rds.addPrimaryKeyColumn(rc);
		rds.addOrderByColumn(rc);

		RelationalDataSet rds1 = rds.createSubView(Arrays.asList("NAME"), ExtractRequest.ColumnListMode.INCLUDE);
		Assert.assertEquals(rc, rds1.getColumn("NAME"));

		//Assert.assertEquals(rds.getLogicalColumns(), rds1.getPrimaryKeyColumns());
		//Assert.assertEquals(rds.getPrimaryKeyColumns(), rds1.getPrimaryKeyColumns());
		//Assert.assertEquals(rds.getOrderByColumns(), rds1.getOrderByColumns());
	}

	@Test(expected = IllegalArgumentException.class)
	public void duplicateColumns()
	{
		RelationalDataSet rds = new RelationalDataSet("PUBLIC", "TABLE", null);

		RelationalColumn rc = new RelationalColumn("NAME", 0, false, false);
		rds.addColumn(rc);
		rds.addColumn(rc);
	}

	@Test(expected = IllegalArgumentException.class)
	public void duplicateKeyColumns()
	{
		RelationalDataSet rds = new RelationalDataSet("PUBLIC", "TABLE", null);

		RelationalColumn rc = new RelationalColumn("NAME", 0, false, false);
		rds.addColumn(rc);
		rds.addPrimaryKeyColumn(rc);
		rds.addPrimaryKeyColumn(rc);
	}

	@Test(expected = IllegalArgumentException.class)
	public void duplicateOrderByColumns()
	{
		RelationalDataSet rds = new RelationalDataSet("PUBLIC", "TABLE", null);

		RelationalColumn rc = new RelationalColumn("NAME", 0, false, false);
		rds.addColumn(rc);
		rds.addOrderByColumn(rc);
		rds.addOrderByColumn(rc);
	}

	@Test
	public void orderInSubViews1()
	{
		RelationalDataSet rds = new RelationalDataSet("PUBLIC", "TABLE", null);

		RelationalColumn rc = new RelationalColumn("NAME", 0, false, false);
		rds.addColumn(rc);

		rc = new RelationalColumn("NAME_II", 0, false, false);
		rds.addColumn(rc);
		rds.addPrimaryKeyColumn(rc);
		rds.addOrderByColumn(rc);

		RelationalDataSet rds1 = rds.createSubView(Arrays.asList("NAME"), ExtractRequest.ColumnListMode.EXCLUDE);
		Assert.assertEquals(rc, rds1.getColumn("NAME_II"));

		//Assert.assertEquals(Arrays.asList(rc), rds1.getPrimaryKeyColumns());
		//Assert.assertEquals(Arrays.asList(rc), rds1.getPrimaryKeyColumns());
		//Assert.assertEquals(Arrays.asList(rc), rds1.getOrderByColumns());

		rds1 = rds.createSubView(Arrays.asList("NAME_II"), ExtractRequest.ColumnListMode.INCLUDE);
		Assert.assertEquals(rc, rds1.getColumn("NAME_II"));

		//Assert.assertEquals(Arrays.asList(rc), rds1.getPrimaryKeyColumns());
		//Assert.assertEquals(Arrays.asList(rc), rds1.getPrimaryKeyColumns());
		//Assert.assertEquals(Arrays.asList(rc), rds1.getOrderByColumns());
	}
}
