package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseClassListener;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

public class MaxDigitsTest {
	@Test(expected = IllegalArgumentException.class)
	public void testMinus1()
	{
		Assert.assertEquals(new BigInteger("0"), DatabaseClassListener.getMaxDigits(-1));
	}

	@Test
	public void testZero()
	{
		Assert.assertEquals(new BigInteger("0"), DatabaseClassListener.getMaxDigits(0));
	}

	@Test
	public void test5()
	{
		Assert.assertEquals(new BigInteger("99999"), DatabaseClassListener.getMaxDigits(5));
	}

	@Test
	public void test20()
	{
		Assert.assertEquals(new BigInteger("99999999999999999999"), DatabaseClassListener.getMaxDigits(20));
	}
}
