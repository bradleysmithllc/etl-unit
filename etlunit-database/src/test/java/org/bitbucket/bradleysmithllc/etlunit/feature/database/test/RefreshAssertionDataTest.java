package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

public class RefreshAssertionDataTest extends BaseDatabaseIntegrationTest {
	Object [] [] compareFiles = {
			{"FILE", "FILE_TAB", DataFileSchema.format_type.delimited},
			{"ASSERTION_FILE_TAB_II", "FILE_TAB_II", DataFileSchema.format_type.fixed},
			{"ASSERTION_FILE_TAB_SQL", "FILE_TAB_SQL", DataFileSchema.format_type.delimited},
			{"ASSERTION_FILE_TAB_SCRIPT", "FILE_TAB_SCRIPT", DataFileSchema.format_type.delimited},
			{"ASSERTION_FILE_TAB_COL_EX", "FILE_TAB_COL_EX", DataFileSchema.format_type.delimited},
			{"ASSERTION_FILE_TAB_II_COL_EX", "FILE_TAB_II_COL_EX", DataFileSchema.format_type.fixed},
			{"ASSERTION_FILE_TAB_SQL_COL_EX", "FILE_TAB_SQL_COL_EX", DataFileSchema.format_type.delimited},
			{"ASSERTION_FILE_TAB_SCRIPT_COL_EX", "FILE_TAB_SCRIPT_COL_EX", DataFileSchema.format_type.delimited}
	};

	Object [] [] compareDataSets = {
		{"ref_assrt_here", "assrt_here"},
		{"ref_assrt", "assrt"},
		{"ref_assrt_missing", "assrt_missing"},
		{"ref_assrt_missing_id", "assrt_missing_id"}
	};

	@Test
	public void databaseAssertionsPassThroughToImplementation() throws IOException {
		startTest();

		for (Object[] names : compareFiles)
		{
			// after running, there must be a data file which matches a comparison file
			File file = databaseRuntimeSupport.getSourceData(null, (String) names[0], "assertion-verification", (DataFileSchema.format_type) names[2]);
			File ftab = databaseRuntimeSupport.getSourceData(null, (String) names[1], "assertion-verification", (DataFileSchema.format_type) names[2]);

			Assert.assertTrue((String) names[0], file.exists());
			Assert.assertTrue((String) names[1], ftab.exists());

			Assert.assertEquals((String) names[0], FileUtils.readFileToString(file), FileUtils.readFileToString(ftab));
		}

		for (Object[] names : compareDataSets)
		{
			// after running, there must be a data file which matches a comparison file
			File file = databaseRuntimeSupport.getSourceDataSet(null, (String) names[0], "assertion-verification");
			File ftab = databaseRuntimeSupport.getSourceDataSet(null, (String) names[1], "assertion-verification");

			Assert.assertTrue((String) names[0], file.exists());
			Assert.assertTrue((String) names[1], ftab.exists());

			Assert.assertEquals((String) names[0], FileUtils.readFileToString(file), FileUtils.readFileToString(ftab));
		}
	}
}
