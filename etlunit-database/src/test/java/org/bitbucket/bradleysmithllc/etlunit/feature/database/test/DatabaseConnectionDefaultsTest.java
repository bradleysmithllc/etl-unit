package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.*;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * tests that implementation defaults are loaded correctly.
 * impl-server-name: exposes a default server name
 * impl-server-name-local: exposes a default server name of [local]
 * impl-server-port: exposes a default server port
 * impl-user-name: exposes a default user name
 * impl-password: exposes a default password
 * impl-properties: exposes default properties
 */
public class DatabaseConnectionDefaultsTest {
	@Test
	public void defaultImplServerNameOverrides() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();

		DatabaseConnection dc = loadWithJson("id", configuration, "impl-server-name", "project", "2_1", "user", "uid");

		Assert.assertEquals("server_host", dc.getServerName());
	}

	@Test
	public void defaultImplServerNameLocalOverrides() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();

		DatabaseConnection dc = loadWithJson("id", configuration, "impl-server-name-local", "project", "2_1", "user", "uid");

		Assert.assertEquals(InetAddress.getLocalHost().getHostName(), dc.getServerName());
	}

	@Test
	public void implServerNameOverridden() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-server-name");
		configuration.setServerName("override");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals("override", dc.getServerName());
	}

	@Test
	public void implServerNameOverrides() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-server-name");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals("server_host", dc.getServerName());
	}

	@Test
	public void implServerNameOverridesToLocal() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setServerName("[local]");
		configuration.setImplementationId("impl-server-name");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals(InetAddress.getLocalHost().getHostName(), dc.getServerName());
	}

	@Test
	public void implServerNameLocalOverridden() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-server-name-local");
		configuration.setServerName("override2");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals("override2", dc.getServerName());
	}

	@Test
	public void implServerNameLocalOverrides() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-server-name-local");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals(InetAddress.getLocalHost().getHostName(), dc.getServerName());
	}

	@Test
	public void implServerPortOverrides() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-server-port");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals(50, dc.getServerPort());
	}

	@Test
	public void implServerPortOverridden() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-server-port");
		configuration.setServerPort(new Long(75L));

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals(75, dc.getServerPort());
	}

	@Test
	public void implUserNameOverrides() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-user-name");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals("user_name", dc.getAdminUserName());
	}

	@Test
	public void implUserNameOverridden() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-user-name");
		configuration.setUserName("bluser");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals("bluser", dc.getAdminUserName());
	}

	@Test
	public void implPasswordOverrides() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-password");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals("password", dc.getAdminPassword());
	}

	@Test
	public void implPasswordOverridden() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-password");
		configuration.setPassword("blpass");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Assert.assertEquals("blpass", dc.getAdminPassword());
	}

	@Test
	public void implPropertiesOverride() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-properties");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Map<String, String> databaseProperties = dc.getDatabaseProperties();
		Assert.assertNotNull(databaseProperties);

		Assert.assertEquals("1laiceps", databaseProperties.get("special1"));
	}

	@Test
	public void implPropertiesOverridden() throws ParseException, UnknownHostException, JsonProcessingException {
		DatabaseDefinitionsProperty configuration = new DatabaseDefinitionsProperty();
		configuration.setImplementationId("impl-properties");
		configuration.setImplementationProperties(new ImplementationProperties__1());
		configuration.getImplementationProperties().getAdditionalProperties().put("special1", "cieapls1");

		DatabaseConnection dc = loadWithJson("id", configuration, null, "project", "2_1", "user", "uid");

		Map<String, String> databaseProperties = dc.getDatabaseProperties();
		Assert.assertNotNull(databaseProperties);

		Assert.assertEquals("cieapls1", databaseProperties.get("special1"));
	}

	private DatabaseConnection loadWithJson(String connectionIdToLoad, DatabaseDefinitionsProperty configurationJson, String defaultImplId, String project, String version, String user, String uid) throws ParseException, JsonProcessingException {
		DatabaseFeatureModuleConfiguration dfmc = new DatabaseFeatureModuleConfiguration();

		if (defaultImplId != null)
		{
			dfmc.setDefaultImplementationId(defaultImplId);
		}

		dfmc.setImplementationDefaults(new ImplementationDefaults());

		ImplementationDefaultsProperty implServerNameDefault = new ImplementationDefaultsProperty();
		implServerNameDefault.setServerPort(new Long(50L));
		Map<String,ImplementationDefaultsProperty> additionalProperties = dfmc.getImplementationDefaults().getAdditionalProperties();
		additionalProperties.put("impl-server-port", implServerNameDefault);

		implServerNameDefault = new ImplementationDefaultsProperty();
		implServerNameDefault.setUserName("user_name");
		additionalProperties.put("impl-user-name", implServerNameDefault);

		implServerNameDefault = new ImplementationDefaultsProperty();
		implServerNameDefault.setPassword("password");
		additionalProperties.put("impl-password", implServerNameDefault);

		implServerNameDefault = new ImplementationDefaultsProperty();
		implServerNameDefault.setServerName("server_host");
		additionalProperties.put("impl-server-name", implServerNameDefault);

		implServerNameDefault = new ImplementationDefaultsProperty();
		implServerNameDefault.setImplementationProperties(new ImplementationProperties());
		implServerNameDefault.getImplementationProperties().getAdditionalProperties().put("special1", "1laiceps");

		additionalProperties.put("impl-properties", implServerNameDefault);

		ImplementationDefaultsProperty implServerNameLocalDefault = new ImplementationDefaultsProperty();
		implServerNameLocalDefault.setServerName("[local]");
		additionalProperties.put("impl-server-name-local", implServerNameLocalDefault);

		dfmc.setDatabaseDefinitions(new DatabaseDefinitions());
		dfmc.getDatabaseDefinitions().getAdditionalProperties().put(connectionIdToLoad, configurationJson);

		String json = new ObjectMapper().writeValueAsString(dfmc);

		DatabaseConfiguration dbc = new DatabaseConfiguration(ETLTestParser.loadObject(json), project, version, user, uid);

		DatabaseConnection databaseConnection = dbc.getDatabaseConnection(connectionIdToLoad);
		Assert.assertNotNull(databaseConnection);

		return databaseConnection;
	}
}
