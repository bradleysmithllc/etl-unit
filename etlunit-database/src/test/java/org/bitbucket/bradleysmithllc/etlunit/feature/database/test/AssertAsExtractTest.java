package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class AssertAsExtractTest extends AbstractDatabaseFeatureModuleTest {
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("default-connection-id")
								.value("edw")
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("test_schema_assert.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides()
	{
		return Arrays.asList(new RuntimeOption("database.refreshAssertionData", true));
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSourceFromClasspath(null, "database_assert_extract.etlunit");

		createSource("", "data", "FILE.delimited", "/*-- id --*/\n/*-- INTEGER --*/");

		createResourceFromClasspath("sqlserver", "edw", "test_schema_assert.ddl");
	}

	@Test
	public void databaseAssertionsPassThroughToImplementation() throws IOException
	{
		TestResults results = startTest();

		Assert.assertEquals(3, results.getNumTestsSelected());
		Assert.assertEquals(3, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());

		File file = databaseRuntimeSupport.getSourceData(null, "FILE", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertTrue(file.exists());
		Assert.assertEquals("/*-- id  --*/\n/*-- INTEGER  --*/", IOUtils.readFileToString(file));

		File file_2 = databaseRuntimeSupport.getSourceData(null, "FILE_2", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertTrue(file_2.exists());
		Assert.assertEquals("/*-- id  --*/\n/*-- INTEGER  --*/\n1", IOUtils.readFileToString(file_2));
	}
}
