package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;

import javax.inject.Inject;
import java.io.IOException;

public class BaseDatabaseIntegrationTest extends BaseIntegrationTest {
	protected H2DBImplementation h2impl = null;
	protected HSQLDBImplementation hsqlimpl = null;
	protected RestrictedDBImplementation restrictedimpl = null;

	protected DatabaseRuntimeSupport databaseRuntimeSupport;
	protected DatabaseFeatureModule databaseFeatureModule;

	enum db {
		h2,
		hsql,
		restricted
	}

	@Inject
	public void setDatabaseRuntimeSupport(DatabaseRuntimeSupport support)
	{
		databaseRuntimeSupport = support;
	}

	@Inject
	public void setDatabaseFeature(DatabaseFeatureModule feature) throws IOException {
		databaseFeatureModule = feature;
		DatabaseImplementation impl = null;

		switch (databaseType()) {
			case h2:
				if (h2impl == null) {
					h2impl = new H2DBImplementation("sqlserver", runtimeSupport.createAnonymousTempFolder());
				}
				impl = h2impl;
				break;
			case hsql:
				if (hsqlimpl == null) {
					hsqlimpl = new HSQLDBImplementation("sqlserver", runtimeSupport.createAnonymousTempFolder());
				}
				impl = hsqlimpl;
				break;
			case restricted:
				if (restrictedimpl == null) {
					restrictedimpl = new RestrictedDBImplementation("sqlserver", runtimeSupport.createAnonymousTempFolder());
				}
				impl = restrictedimpl;
				break;
		}

		prepareImplementation(impl);

		databaseFeatureModule.addDatabaseImplementation(impl);
		prepareDatabaseFeatureModule();
	}

	protected void prepareDatabaseFeatureModule() throws IOException {
	}

	protected db databaseType() {
		return db.h2;
	}

	public void prepareImplementation(DatabaseImplementation sqlserver) {
	}
}
