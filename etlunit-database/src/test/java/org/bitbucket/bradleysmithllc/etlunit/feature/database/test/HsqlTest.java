package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.h2.Driver;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.sql.*;

public class HsqlTest
{
	@Test
	public void createHsqlDB() throws SQLException, IOException
	{
		Class cl = Driver.class;

		Connection con = DriverManager.getConnection("jdbc:h2:mem:/database" + System.currentTimeMillis());

		DatabaseMetaData md = con.getMetaData();

		Statement st = con.createStatement();

		URL ddl = getClass().getResource("/resource/test.ddl");

		st.execute(IOUtils.readURLToString(ddl));

		ResultSet rs = md.getTables(null, null, null, new String[]{"TABLE"});

		while (rs.next())
		{
			System.out.println(rs.getString(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getString(3));
			System.out.println(rs.getString(4));
			System.out.println(rs.getString(5));
			System.out.println(rs.getString(6));
			System.out.println(rs.getString(7));
			System.out.println(rs.getString(8));
			System.out.println(rs.getString(9));
			System.out.println(rs.getString(10));
		}

		rs.close();

		rs = st.executeQuery("SELECT * FROM CUSTOM");

		ResultSetMetaData rsmd = rs.getMetaData();

		System.out.println(rsmd.getColumnName(1));
		System.out.println(rsmd.getColumnType(1));
		System.out.println(rsmd.getColumnClassName(1));

		rs.close();

		rs = md.getPrimaryKeys(null, null, "CUSTOM");

		while (rs.next())
		{
			System.out.println(rs.getString(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getString(3));
			System.out.println(rs.getString(4));
			System.out.println(rs.getString(5));
			System.out.println(rs.getString(6));
		}

		rs.close();

		rs = st.executeQuery("SELECT ID, DI FROM CUSTOM ORDER BY ID, DI");

		rsmd = rs.getMetaData();

		int cols = rsmd.getColumnCount();

		for (int i = 1; i <= cols; i++)
		{
			System.out.print(rsmd.getColumnName(i));
			System.out.print('\t');
		}

		rs.close();

		st.close();
	}
}
