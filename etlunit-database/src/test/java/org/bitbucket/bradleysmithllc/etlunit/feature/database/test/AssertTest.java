package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class AssertTest extends AbstractDatabaseFeatureModuleTest {
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("default-connection-id")
								.value("edw")
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("test_schema_assert.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSourceFromClasspath(null, "database_assert_fail.etlunit");

		File f = databaseRuntimeSupport.getSourceData(null, "FILE", "test-setup", DataFileSchema.format_type.delimited);
		createFromClasspath(f, "FILE.delimited");

		f = databaseRuntimeSupport.getSourceData(null, "FILE1", "test-setup", DataFileSchema.format_type.delimited);
		createFromClasspath(f, "FILE1.delimited");

		createResourceFromClasspath("sqlserver", "edw", "test_schema_assert.ddl");
	}

	@Test
	public void databaseAssertionsPassThroughToImplementation()
	{
		TestResults results = startTest();

		Assert.assertEquals(1, results.getNumTestsSelected());
		Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
	}
}
