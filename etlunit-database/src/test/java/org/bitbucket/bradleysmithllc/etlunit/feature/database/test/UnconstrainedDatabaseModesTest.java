package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class UnconstrainedDatabaseModesTest extends AbstractDatabaseFeatureModuleTest {
	private H2DBImplementation mysql;

	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
											.endObject()
										.key("edw_ods")
											.object()
												.key("implementation-id")
												.value("mysql")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSource("database.etlunit", "@Database(id: 'edw', modes: ['src1']) class test {@Test a_sqlserver_oracle(){}}");
		createSource("database1.etlunit", "@Database(id: 'edw', modes: ['src', 'tgt']) class test {@Database(id: 'edw_ods', modes: ['lkp']) @Test a_sqlserver_oracle(){}}");
	}

	public void prepareDatabaseFeatureModule() throws Exception {
		mysql = new H2DBImplementation("mysql", temporaryFolder.newFolder());
		databaseFeatureModule.addDatabaseImplementation(mysql);
	}

	@Test
	public void testDifferentDatabaseImplementations()
	{
		startTest();
	}

	@Override
	protected void assertions(TestResults results, int pass)
	{
		if (pass == 1)
		{
			Assert.assertEquals(3, sqlserver.createCount);
			Assert.assertEquals(3, sqlserver.preCleanTablesCount);
			Assert.assertEquals("[src, src1, tgt]", sqlserver.modes.toString());

			Assert.assertEquals(1, mysql.createCount);
			Assert.assertEquals(1, mysql.preCleanTablesCount);
			Assert.assertEquals("[lkp]", mysql.modes.toString());
		}
	}
}
