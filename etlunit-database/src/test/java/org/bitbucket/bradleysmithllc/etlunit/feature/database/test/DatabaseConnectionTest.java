package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnectionImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class DatabaseConnectionTest {
	@Test
	public void test() throws ParseException, UnknownHostException {
		DatabaseConnectionImpl dc = loadWithJson("id", "{}", "def-imp", "project", "2_1", "user", "uid");

		Assert.assertEquals("id", dc.getId());
		Assert.assertEquals("def-imp", dc.getImplementationId());
		Assert.assertEquals("user_project_2_1_id_uid", dc.getDatabaseName(null));
		Assert.assertEquals(dc.getDatabaseName(null), dc.getPassword(null));
		Assert.assertEquals(dc.getDatabaseName(null), dc.getLoginName(null));
		Assert.assertEquals(InetAddress.getLocalHost().getHostName(), dc.getServerName());

		dc =loadWithJson("id", "{implementation-id: 'iid', server-name: 'goobully'}", null, "project", "", "user", "uid");
		Assert.assertEquals("id", dc.getId());
		Assert.assertEquals("iid", dc.getImplementationId());
		Assert.assertEquals("goobully", dc.getServerName());

		dc = loadWithJson("id", "{implementation-id: 'iid'}", null, "project", "3_2_snappy", "user", "uid");

		Assert.assertEquals("user_project_3_2_snappy_id_uid", dc.getDatabaseName(null));
		Assert.assertEquals("user_project_3_2_snappy_id_a_uid", dc.getDatabaseName("a"));
		Assert.assertEquals("user_project_3_2_snappy_id_b_uid", dc.getDatabaseName("b"));

		Assert.assertEquals(dc.getDatabaseName(null), dc.getPassword(null));
		Assert.assertEquals(dc.getDatabaseName("a"), dc.getLoginName("a"));
		Assert.assertEquals(dc.getDatabaseName("b"), dc.getLoginName("b"));

		Assert.assertEquals("id", dc.getId());
		Assert.assertEquals("iid", dc.getImplementationId());
	}

	private DatabaseConnectionImpl loadWithJson(String connectionIdToLoad, String configurationJson, String defaulImplementationId, String project, String version, String user, String uid) throws ParseException {
		DatabaseConfiguration dbc = new DatabaseConfiguration(ETLTestParser.loadObject("{" + (defaulImplementationId != null ? ("default-implementation-id: \"" + defaulImplementationId + "\", ") : "") + "database-definitions: {" + connectionIdToLoad + ":" + configurationJson + "}}"), project, version, user, uid);

		DatabaseConnectionImpl databaseConnection = (DatabaseConnectionImpl) dbc.getDatabaseConnection(connectionIdToLoad);
		Assert.assertNotNull(databaseConnection);

		return databaseConnection;
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFailsWithMissingDefaultImplementation() throws ParseException {
		DatabaseConnection dc = loadWithJson("id", "{}", null, "project", "", "user", "uid");
	}

	@Test
	public void ddlScripts() throws ParseException {
		DatabaseConnection dc = loadWithJson("id", "{schema-scripts: ['a.ddl', 'b.ddl']}", "def-imp", "project", "2.1", "user", "uid");

		Assert.assertEquals("a.ddl", dc.getSchemaScripts().get(0));
		Assert.assertEquals("b.ddl", dc.getSchemaScripts().get(1));
	}
}
