package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class StageAsExtractTest extends AbstractDatabaseFeatureModuleTest {
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("default-connection-id")
								.value("edw")
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("schema_truncate.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides()
	{
		return Arrays.asList(new RuntimeOption("database.refreshStageData", true));
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSourceFromClasspath(null, "database_stage_extract.etlunit");

		createResourceFromClasspath("sqlserver", "edw", "schema_truncate.ddl");
	}

	@Test
	public void stagedDataTest() throws IOException
	{
		TestResults results = startTest();

		Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());

		Assert.assertNotNull(databaseRuntimeSupport);

		// verify the 6 master tables written out
		File master1 = databaseRuntimeSupport.getSourceData(null, "MASTER_1", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master1);
		Assert.assertTrue(master1.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n" +
			"/*-- INTEGER\tINTEGER  --*/\n" +
			"1\t2", IOUtils.readFileToString(master1));

		File master2 = databaseRuntimeSupport.getSourceData(null, "MASTER_2", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master2);
		Assert.assertTrue(master2.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n/*-- INTEGER\tINTEGER  --*/\n2\t3", IOUtils.readFileToString(master2));

		File master3 = databaseRuntimeSupport.getSourceData(null, "MASTER_3", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master3);
		Assert.assertTrue(master3.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n/*-- INTEGER\tINTEGER  --*/\n1\t2\n3\t4", IOUtils.readFileToString(master3));

		File master4 = databaseRuntimeSupport.getSourceData(null, "MASTER_4", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master4);
		Assert.assertTrue(master4.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n/*-- INTEGER\tINTEGER  --*/\n4\t5", IOUtils.readFileToString(master4));

		File master5 = databaseRuntimeSupport.getSourceData(null, "MASTER_5", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master5);
		Assert.assertTrue(master5.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n/*-- INTEGER\tINTEGER  --*/\n5\t6", IOUtils.readFileToString(master5));

		File master6 = databaseRuntimeSupport.getSourceData(null, "MASTER_6", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master6);
		Assert.assertTrue(master6.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n/*-- INTEGER\tINTEGER  --*/\n4\t5\n6\t7", IOUtils.readFileToString(master6));

		File master7 = databaseRuntimeSupport.getSourceData(null, "MASTER_7", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master7);
		Assert.assertTrue(master7.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n/*-- INTEGER\tINTEGER  --*/\n4\t5\n6\t7\n7\t8", IOUtils.readFileToString(master7));

		File master8 = databaseRuntimeSupport.getSourceData(null, "MASTER_8", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master8);
		Assert.assertTrue(master8.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n/*-- INTEGER\tINTEGER  --*/\n5\t6", IOUtils.readFileToString(master8));

		File master9 = databaseRuntimeSupport.getSourceData(null, "MASTER_9", "test-setup", DataFileSchema.format_type.delimited);
		Assert.assertNotNull(master9);
		Assert.assertTrue(master9.exists());

		Assert.assertEquals("/*-- id\tdi  --*/\n/*-- INTEGER\tINTEGER  --*/\n4\t5\n6\t7\n7\t8\n8\t9\n9\t10", IOUtils.readFileToString(master9));
	}
}
