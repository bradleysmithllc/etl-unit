package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;

import java.io.File;

public class RestrictedDBImplementation extends HSQLDBImplementation
{
	public RestrictedDBImplementation(String id) {
		super(id);
	}

	public RestrictedDBImplementation(String id, File tableSpace) {
		super(id, tableSpace);
	}

	/**
	 * Coalesce all modes into one
	 * @param dc
	 * @param mode
	 * @return
	 */
	@Override
	protected File getFile(DatabaseConnection dc, String mode) {
		return super.getFile(dc, null);
	}

	@Override
	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException {
		switch (op) {
			case preCreateSchema:
				// create a literal schema which matches our login name
				InitializeRequest databaseRequest = request.getInitializeRequest();
				DatabaseConnection databaseConnection = databaseRequest.getConnection();
				String mode = databaseRequest.getMode();

				// with the user and password in hand, create a new user and schema and set these as default for this login
				try {
					executeScript(
							IOUtils.toString(Thread.currentThread().getContextClassLoader().getResource("hsql.user.sql.vm")),
							databaseConnection,
							mode,
							database_role.sysadmin
				);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}

				break;
			default:
				return super.processOperationSub(op, request);
		}

		return null;
	}

	@Override
	public String getLoginNameImpl(DatabaseConnection dc, String mode) {
		return mode == null ? "default" : mode;
	}

	@Override
	protected String getPasswordImpl(DatabaseConnection dc, String mode) {
		return mode == null ? "default" : mode;
	}

	@Override
	public boolean restrictToOwnedSchema(DatabaseConnection dc) {
		return true;
	}
}
