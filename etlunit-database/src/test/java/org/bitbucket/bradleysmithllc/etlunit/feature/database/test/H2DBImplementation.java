package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.BaseDatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.bitbucket.bradleysmithllc.etlunit.util.jdbc.JdbcVisitors;
import org.h2.Driver;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class H2DBImplementation extends BaseDatabaseImplementation {
	private static int nextCode = 0;

	private int code = nextCode++;

	private final String id;
	int createCount = 0;
	int completeCreateCount = 0;
	int preCreateSchemaCount = 0;
	int postCreateSchemaCount = 0;
	int dropConstraintsCount = 0;
	int materializeViewsCount = 0;
	int preCleanTablesCount = 0;
	int postCleanTablesCount = 0;

	final Set<String> prepareOrder = Collections.synchronizedSet(new TreeSet<>());
	final Set<String> modes = Collections.synchronizedSet(new TreeSet<>());

	private boolean refresh = true;
	private final File tableSpace;

	// default for H2
	private String defaultSchema = "PUBLIC";

	public H2DBImplementation(String id) {
		this.id = id;
		tableSpace = null;
	}

	public H2DBImplementation(String id, File tableSpace) {
		this.id = id;
		this.tableSpace = tableSpace;
	}

	public String getImplementationId() {
		return id;
	}

	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException {
		switch (op) {
			case createDatabase:
				if (tableSpace != null) {
					// close all active connections to this database / mode and issue drop all objects
					InitializeRequest databaseRequest = request.getInitializeRequest();

					try {
						DatabaseConnection databaseConnection = databaseRequest.getConnection();
						String mode = databaseRequest.getMode();

						applicationLog.info("Disposing connection pool " + databaseConnection.getId() + "_" + mode);
						disposePool(databaseConnection, mode, database_role.database);

						File dbRoot = getDBRoot(databaseRequest.getDatabase().getLogicalName(), mode, database_role.database);

						applicationLog.info("Using database root " + dbRoot.getAbsolutePath());

						// open a new connection and issue drop all objects
						JdbcVisitors.withConnection(getConnection(databaseConnection, mode), (connection) -> {returnConnection(connection, databaseConnection, mode, database_role.database, true);})
								.withStatement((statement) -> {
									statement.execute("DROP ALL OBJECTS");
								})
								.dispose();
					} catch (Exception e) {
						throw new RuntimeException("Failed to delete database directory", e);
					}
				}

				createCount++;
				return null;
			case completeDatabaseInitialization:
				completeCreateCount++;
				return null;
			default:
				throw new UnsupportedOperationException(op.name() + " is unsupported");
			case preCreateSchema:
				preCreateSchemaCount++;
				return null;
			case postCreateSchema:
				postCreateSchemaCount++;
				return null;
			case dropConstraints:
				dropConstraintsCount++;
				return null;
			case materializeViews:
				materializeViewsCount++;
				return null;
			case preCleanTables:
				DatabaseRequest prepare = request.getDatabaseRequest();

				preCleanTablesCount++;

				String mode = null;

				if (prepare.getMode() == null) {
					mode = "NULL";
				} else {
					mode = prepare.getMode();
				}

				checkPrepare(prepare.getTestMethod().getName());

				modes.add(mode);
				return null;
			case resetIdentities:
				return null;
			case postCleanTables:
				prepare = request.getDatabaseRequest();

				postCleanTablesCount++;

				mode = null;

				if (prepare.getMode() == null) {
					mode = "NULL";
				} else {
					mode = prepare.getMode();
				}

				checkPrepare(prepare.getTestMethod().getName());

				modes.add(mode);
				return null;
		}
	}

	private synchronized void checkPrepare(String name) {
		if (!prepareOrder.contains(name)) {
			prepareOrder.add(name);
		}
	}

	@Override
	public String getJdbcUrl(DatabaseConnection dc, String mode, database_role id) {
		if (tableSpace == null) {
			return "jdbc:h2:mem:/" + getDatabaseName(dc, mode) + "_" + code + ";DB_CLOSE_DELAY=-1";
		}

		String url = "jdbc:h2:file:" + getFile(dc, mode, id).getAbsolutePath() + "";
		return url;
	}

	@Override
	protected String getPasswordImpl(DatabaseConnection dc, String mode) {
		return "h2";
	}

	@Override
	protected String getLoginNameImpl(DatabaseConnection dc, String mode) {
		return "h2";
	}

	protected File getFile(DatabaseConnection dc, String mode, database_role id) {
		return new File(getDBRoot(dc.getId(), mode, id), getDatabaseName(dc, mode).toLowerCase() + ".dat");
	}

	private File getDBRoot(String dcid, String mode, database_role id) {
		return new File(tableSpace, (dcid + "_" + mode + "_" + getImplementationId() + "_" + String.valueOf(id) + "_" + runtimeSupport.getExecutorId()).toLowerCase());
	}

	@Override
	public Class getJdbcDriverClass() {
		return Driver.class;
	}

	public String getDefaultSchema(DatabaseConnection dc, String mode) {
		return defaultSchema;
	}

	@Override
	public database_state getDatabaseState(DatabaseConnection databaseConnection, String s, Database database) {
		return refresh ? database_state.fail : database_state.pass;
	}

	public void setDefaultSchema(String defaultSchema) {
		this.defaultSchema = defaultSchema;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	@Override
	protected String createScriptToMaterializeViewToTable(String sourceSchema, String sourceView, String targetSchema, String targetName) {
		return "CREATE TABLE " + targetSchema + "." + targetName + " AS (SELECT * FROM " + sourceSchema + "." + sourceView + " WHERE 1 = 0) WITH DATA;";
	}
}
