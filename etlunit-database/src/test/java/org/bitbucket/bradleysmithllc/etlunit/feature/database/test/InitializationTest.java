package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class InitializationTest extends BaseFeatureModuleTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private H2DBImplementation sqlserver = null;
	private H2DBImplementation sqlserver2 = null;

	private boolean prepareDatabase = true;
	private boolean forceInitialize = false;

	@Before
	public void resetOptions() throws IOException {
		sqlserver = new H2DBImplementation("sqlserver", temporaryFolder.newFolder());
		sqlserver2 = new H2DBImplementation("sqlserver2", temporaryFolder.newFolder());

		prepareDatabase = true;
		forceInitialize = false;
	}

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides()
	{
		return Arrays.asList(new RuntimeOption("database.prepareDatabase", prepareDatabase), new RuntimeOption("database.forceInitializeDatabase", forceInitialize));
	}

	@Inject
	public void setDatabaseFeatureModule(DatabaseFeatureModule databaseFeatureModule) throws IOException {
		databaseFeatureModule.addDatabaseImplementation(sqlserver);
		databaseFeatureModule.addDatabaseImplementation(sqlserver2);
	}

	@Override
	protected List<Feature> getTestFeatures() {
		DatabaseFeatureModule databaseFeatureModule = new DatabaseFeatureModule();
		return Arrays.asList((Feature) databaseFeatureModule);
	}

	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("default-connection-id")
								.value("edw")
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
													.array()
														.value("script1.ddl")
														.value("script2.ddl")
														.value("script3.ddl")
													.endArray()
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSource("db.etlunit", "@Database (id: 'edw', modes: ['" + mode + "']) class dbtest {@Test tes(){ log(message: \"hello\"); }}");
		createResource("sqlserver", "edw", "script1.ddl", ddlText1);
		createResource("sqlserver", "edw", "script2.ddl", ddlText2);
		createResource("sqlserver", "edw", "script3.ddl", ddlText3);

		createResource("sqlserver2", "edw", "script1.ddl", ddlText1);
		createResource("sqlserver2", "edw", "script2.ddl", ddlText2);
		createResource("sqlserver2", "edw", "script3.ddl", ddlText3);
	}

	private boolean clean;
	private String mode = "src";
	private String ddlText1 = "CREATE TABLE DDL1(ID INT)";
	private String ddlText2 = "CREATE TABLE DDL2(ID INT)";
	private String ddlText3 = "CREATE TABLE DDL3(ID INT)";

	@Test
	public void databaseInitializesOnlyWhenNeeded()
	{
		clean = false;
		sqlserver.setRefresh(false);

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		clean = true;
		sqlserver.setRefresh(true);

		startTest();

		Assert.assertEquals(2, sqlserver.createCount);
		Assert.assertEquals(2, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);
	}

	@Test
	public void databaseInitializesEveryTimeWhenOptionSet()
	{
		clean = false;
		forceInitialize = true;

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(2, sqlserver.createCount);
		Assert.assertEquals(2, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		clean = true;

		startTest();

		Assert.assertEquals(3, sqlserver.createCount);
		Assert.assertEquals(3, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);
	}

	@Test
	public void databaseNeverInitializesOrPreparesWithOption()
	{
		prepareDatabase = false;
		startTest();

		Assert.assertEquals(0, sqlserver.createCount);
		Assert.assertEquals(0, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(0, sqlserver.createCount);
		Assert.assertEquals(0, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(0, sqlserver.createCount);
		Assert.assertEquals(0, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);
	}

	@Test
	public void prepareOptionWinsAgainstForceInitialize()
	{
		prepareDatabase = false;
		forceInitialize = true;
		startTest();

		Assert.assertEquals(0, sqlserver.createCount);
		Assert.assertEquals(0, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(0, sqlserver.createCount);
		Assert.assertEquals(0, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(0, sqlserver.createCount);
		Assert.assertEquals(0, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);
	}

	@Test
	public void databaseInitializesWhenConfigChanges()
	{
		clean = false;
		sqlserver.setRefresh(false);

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		mode = "src1";

		startTest();

		Assert.assertEquals(2, sqlserver.createCount);
		Assert.assertEquals(2, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);
	}

	@Test
	public void databaseInitializesWhenDDLChanges()
	{
		clean = false;
		sqlserver.setRefresh(false);

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		ddlText1 = "CREATE TABLE DDL4(ID INT NOT NULL)";

		startTest();

		Assert.assertEquals(2, sqlserver.createCount);
		Assert.assertEquals(2, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(2, sqlserver.createCount);
		Assert.assertEquals(2, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		ddlText2 = "CREATE TABLE DDL5(ID INT NOT NULL)";

		startTest();

		Assert.assertEquals(3, sqlserver.createCount);
		Assert.assertEquals(3, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(3, sqlserver.createCount);
		Assert.assertEquals(3, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		ddlText3 = "CREATE TABLE DDL6(ID INT NOT NULL)";

		startTest();

		Assert.assertEquals(4, sqlserver.createCount);
		Assert.assertEquals(4, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(4, sqlserver.createCount);
		Assert.assertEquals(4, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);
	}

	@Test
	public void databaseFailsWhenDDLChangesAndPrepareIsOff()
	{
		clean = false;
		sqlserver.setRefresh(false);

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		startTest();

		Assert.assertEquals(1, sqlserver.createCount);
		Assert.assertEquals(1, sqlserver.completeCreateCount);
		Assert.assertEquals(0, sqlserver2.createCount);
		Assert.assertEquals(0, sqlserver2.completeCreateCount);

		prepareDatabase = false;
		ddlText1 = "CREATE TABLE DDL4(ID INT NOT NULL)";

		TestResults met = startTest();

		Assert.assertEquals(1, met.getNumTestsSelected());
		Assert.assertEquals(1, met.getMetrics().getNumberOfErrors());

		Assert.assertEquals(1, met.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getNumberOfErrors());
		Assert.assertNotNull(met.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getResultsMapByTest().get("[default].dbtest.tes"));
		Assert.assertEquals(1, met.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getResultsMapByTest().get("[default].dbtest.tes").getErrors().size());
		Assert.assertEquals("ERR_INVALID_SCHEMA_STATE", met.getResultsByClass().get(0).getMethodResults().get(0).getMetrics().getResultsMapByTest().get("[default].dbtest.tes").getErrors().get(0).getErrorId());
	}

	@Override
	protected boolean cleanWorkspace() {
		return clean;
	}

	/**
	 * This test asserts a multi-run behavior
	 * @return
	 */
	@Override
	protected boolean multiPassSafe() {
		return false;
	}
}
