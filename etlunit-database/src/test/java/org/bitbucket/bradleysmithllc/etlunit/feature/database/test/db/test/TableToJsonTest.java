package org.bitbucket.bradleysmithllc.etlunit.feature.database.test.db.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;

public class TableToJsonTest {
	enum testType
	{
		table_column,
		table,
		schema,
		catalog,
		database
	}

	@Test
	public void runAll() throws IOException {
		JsonNode json = JsonLoader.fromURL(getClass().getResource("databaseSerializeTests.json"));

		ObjectNode on = (ObjectNode) json.get("tests");

		Iterator<String> it = on.fieldNames();

		while (it.hasNext())
		{
			String jnodeName = it.next();
			JsonNode jnode = on.get(jnodeName);

			testType type = testType.valueOf(jnode.get("type").asText());
			JsonNode nodebefore = jnode.get("json");

			JsonRepresentation jr = null;
			JsonNode nodeafter =null;

			switch (type) {
				case table_column:
				{
					TableColumnImpl timpl = new TableColumnImpl(null);
					timpl.fromJson(nodebefore);
					nodeafter = JsonLoader.fromString(toString(timpl));
					break;
				}
				case table:
				{
					TableImpl timpl = new TableImpl(null, null);
					timpl.fromJson(nodebefore);
					nodeafter = JsonLoader.fromString(toString(timpl));
					break;
				}
				case schema:
				{
					SchemaImpl timpl = new SchemaImpl(null, null);
					timpl.fromJson(nodebefore);
					nodeafter = JsonLoader.fromString(toString(timpl));
					break;
				}
				case catalog:
				{
					CatalogImpl timpl = new CatalogImpl(null);
					timpl.fromJson(nodebefore);
					nodeafter = JsonLoader.fromString(toString(timpl));
					break;
				}
				case database:
				{
					DatabaseImpl timpl = new DatabaseImpl(null);
					timpl.fromJson(nodebefore);
					nodeafter = JsonLoader.fromString(toString(timpl));
					break;
				}
			}

			Assert.assertEquals(jnodeName, nodebefore, nodeafter);
		}
	}

	private String toString(JsonRepresentation timpl1) throws IOException {
		StringWriter stw = new StringWriter();
		JsonWriter jsw = new JsonWriter(stw);

		jsw.beginObject();
		timpl1.toJson(jsw);
		jsw.endObject();

		jsw.close();

		return stw.toString();
	}
}
