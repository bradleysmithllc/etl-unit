package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileConstants;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

public class DatabaseFileTypesIntegrationTest extends BaseDatabaseIntegrationTest {
	@Override
	protected final void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int methodNumber, int operationsProcessed)
	{
		// grab the description and parse it as json.  Then assert on the src and tgt reference file types
		try {
			ETLTestValueObject descobj = ETLTestParser.loadBareObject(mt.getDescription());

			ETLTestValueObject srcObj = descobj.query("expected-fml");

			if (srcObj != null)
			{
				String valueAsString = srcObj.getValueAsString();
				String valueAsString1 = context.getValue(FileConstants.ASSERT_FILE_TARGET_SCHEMA).getValueAsString();
				Assert.assertEquals(valueAsString.toLowerCase(), valueAsString1.toLowerCase());
			}

			ETLTestValueObject srcRule = descobj.query("expected-rule");

			if (srcRule != null)
			{
				String valueAsString = srcRule.getValueAsString();
				String valueAsString1 = context.getValue(FileConstants.ASSERT_FILE_TARGET_SCHEMA_MATCHING_RULE).getValueAsString();
				Assert.assertEquals(valueAsString, valueAsString1);
			}

			ETLTestValueObject tgtObj = descobj.query("actual-fml");

			if (tgtObj != null)
			{
				String valueAsString = tgtObj.getValueAsString();
				String valueAsString1 = context.getValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA).getValueAsString();
				Assert.assertEquals(valueAsString.toLowerCase(), valueAsString1.toLowerCase());
			}

			ETLTestValueObject tgtRule = descobj.query("actual-rule");

			if (tgtRule != null)
			{
				String valueAsString = tgtRule.getValueAsString();
				String valueAsString1 = context.getValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA_MATCHING_RULE).getValueAsString();
				Assert.assertEquals(valueAsString, valueAsString1);
			}
		} catch (ParseException e) {
			Assert.fail(e.toString());
		}
	}
}
