package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public class ImplicitContextTest extends AbstractDatabaseFeatureModuleTest {
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
											.endObject()
										.key("edw_2")
											.object()
												.key("server-name")
												.value("localhost")
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSource("database.etlunit",
			"@Database(id: 'edw', modes: ['src', 'tgt'])" +
			"class test {" +
				"@Test " +
					"a_sqlserver(){" +
				"}" +
				"@Database(id: 'edw_2', modes: ['src1'])" +
				"@Test " +
				"b_sqlserver(){" +
				"}" +
			"}");
	}

	@Test
	public void testDifferentDatabaseImplementations()
	{
		TestResults results = startTest();
		Assert.assertEquals(2, results.getMetrics().getNumberOfTestsPassed());
	}

	@Override
	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context, int testNumber) {
		ETLTestValueObject database = context.getValue("database");

		Assert.assertNotNull(database);

		ETLTestValueObject connections = database.query("connections");

		Assert.assertNotNull(connections);

		switch(testNumber)
		{
			case 1:
				ETLTestValueObject edw2_src1 = connections.query("edw_2.src1");
				Assert.assertNotNull(edw2_src1);

				String databaseName = edw2_src1.query("database-name").getValueAsString();
				Assert.assertEquals("__builduser_feature_test_1_0_test_edw_2_src1_" + runtimeSupport.getProjectUID(), databaseName);
				Assert.assertEquals("h2", edw2_src1.query("login-name").getValueAsString());
				Assert.assertEquals("h2", edw2_src1.query("password").getValueAsString());
				Assert.assertEquals("localhost", edw2_src1.query("server-name").getValueAsString());

			case 0:
				// edw src and tgt every time
				ETLTestValueObject edw_src = connections.query("edw.src");
				Assert.assertNotNull(edw_src);

				String valueAsString = edw_src.query("database-name").getValueAsString();
				Assert.assertEquals("__builduser_feature_test_1_0_test_edw_src_" + runtimeSupport.getProjectUID(), valueAsString);
				Assert.assertEquals("h2", edw_src.query("login-name").getValueAsString());
				Assert.assertEquals("h2", edw_src.query("password").getValueAsString());
				try {
					Assert.assertEquals(InetAddress.getLocalHost().getHostName(), edw_src.query("server-name").getValueAsString());
				} catch (UnknownHostException e) {
				throw new IllegalArgumentException(e);
				}

				ETLTestValueObject edw_tgt = connections.query("edw.tgt");
				Assert.assertNotNull(edw_tgt);

				String valueAsString1 = edw_tgt.query("database-name").getValueAsString();
				Assert.assertEquals("__builduser_feature_test_1_0_test_edw_tgt_" + runtimeSupport.getProjectUID(), valueAsString1);
				Assert.assertEquals("h2", edw_tgt.query("login-name").getValueAsString());
				Assert.assertEquals("h2", edw_tgt.query("password").getValueAsString());
				try {
					Assert.assertEquals(InetAddress.getLocalHost().getHostName(), edw_tgt.query("server-name").getValueAsString());
				} catch (UnknownHostException e) {
					throw new IllegalArgumentException(e);
				}
				break;
		}
	}
}
