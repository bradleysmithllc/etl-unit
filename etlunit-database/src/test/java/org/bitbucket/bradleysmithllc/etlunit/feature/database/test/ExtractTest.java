package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ExtractTest extends AbstractDatabaseFeatureModuleTest {
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("default-connection-id")
								.value("edw")
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("test_schema.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSourceFromClasspath(null, "database_extract_1.etlunit");
		createSourceFromClasspath(null, "database_extract_2.etlunit");
		createSourceFromClasspath(null, "database_extract_3.etlunit");
		createSourceFromClasspath(null, "database_extract_4.etlunit");
		createSourceFromClasspath(null, "database_extract_5.etlunit");
		createSourceFromClasspath(null, "database_extract_6.etlunit");

		createResourceFromClasspath("sqlserver", "edw", "test_schema.ddl");

		DataFileSchema meta = databaseRuntimeSupport.createRelationalMetaInfo("edw", "TST", "CUSTOM_TEST_2");

		File out = fileRuntimeSupport.getReferenceFileSchema(null, meta.getId());

		createFromClasspath(out, "tst-custom_test_2-meta.json");

		File scpt = databaseRuntimeSupport.getSourceScript(null, "sql_script.sql", "");
		FileUtils.write(scpt, "SELECT * FROM CUSTOM_TEST");
	}

	@Test
	public void dataExtractsProperly()
	{
		TestResults results = startTest();

		Assert.assertEquals(6, results.getMetrics().getNumberOfTestsPassed());
	}
}
