package org.bitbucket.bradleysmithllc.etlunit.feature.database.test;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Inject;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SchemaProcessingTest extends AbstractDatabaseFeatureModuleTest
{
	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("database-definitions")
									.object()
										.key("pos_alx")
											.object()
												.key("implementation-id")
												.value("sqlserver")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("test_schema_complex.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException
	{
		createSource("database.etlunit", "class test {@Database(id: 'pos_alx') @Test defaultImpl(){execute( sql: 'SELECT ID, DI, POS_ALX FROM CUSTOM_TEST');execute( sql: 'SELECT * FROM CUSTOM_TEST2');execute( sql: 'SELECT * FROM CUSTOM_TEST21');}}");

		createResourceFromClasspath("sqlserver", "pos_alx", "test_schema_complex.ddl");
		createResourceFromClasspath("sqlserver", "pos_alx", "test_schema_complex_2.ddl");
	}

	@Test
	public void schemaStitching()
	{
		TestResults tr = startTest();

		Assert.assertTrue(tr.getNumTestsSelected() != 0);
		Assert.assertEquals(0, tr.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, tr.getMetrics().getNumberOfErrors());
	}
}
