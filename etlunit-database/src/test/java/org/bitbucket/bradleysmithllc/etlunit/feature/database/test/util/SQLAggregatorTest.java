package org.bitbucket.bradleysmithllc.etlunit.feature.database.test.util;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.SQLAggregator;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.SQLAggregatorImpl;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class SQLAggregatorTest
{
	private static final class AssertRow
	{
		final String bref;
		final int bline;
		final String eref;
		final int eline;
		final int beg_totalLine;
		final int end_totalLine;
		final int beg_cleanLine;
		final int end_cleanLine;
		final String text;

		private AssertRow(String br, int bl, String er, int el, int btLine, int etLine, int bcLine, int ecLine, String text)
		{
			this.text = text;
			this.bline = bl;
			this.bref = br;
			this.eline = el;
			this.eref = er;
			beg_totalLine = btLine;
			end_totalLine = etLine;
			beg_cleanLine = bcLine;
			end_cleanLine = ecLine;
		}
	}

	private static final AssertRow [] ASSERT_LINES = new AssertRow[]
	{
		new AssertRow("test_schema_complex_2.ddl", 1, "test_schema_complex_2.ddl", 1, 1, 1, 1, 1, "CREATE TABLE CUSTOM_TEST2#if(${suffix})${suffix}#end"),
		new AssertRow("test_schema_complex_2.ddl", 2, "test_schema_complex_2.ddl", 2, 2, 2, 2, 2,""),
		new AssertRow("test_schema_complex_2.ddl", 3, "test_schema_complex_2.ddl", 3, 3, 3, 3, 3, "("),
		new AssertRow("test_schema_complex_2.ddl", 4, "test_schema_complex_2.ddl", 4, 4, 4, 4, 4, "\tID INT NOT NULL,"),
		new AssertRow("test_schema_complex_2.ddl", 5, "test_schema_complex_2.ddl", 5, 5, 5, 5, 5, "\tDI INT NOT NULL,"),
		new AssertRow("test_schema_complex_2.ddl", 6, "test_schema_complex_2.ddl", 6, 6, 6, 6, 6, "\tPRIMARY KEY (ID, DI)"),
		new AssertRow("test_schema_complex_2.ddl", 7, "test_schema_complex_2.ddl", 7, 7, 7, 7, 7, ");;"),
		new AssertRow("test_schema_complex_2.ddl", 7, "test_schema_complex_2.ddl", 7, 8, 8, 8, 8,""),
		new AssertRow("test_schema_complex.ddl", 2, "test_schema_complex.ddl", 2, 9, 9, 9, 9,""),
		new AssertRow("test_schema_complex.ddl", 3, "test_schema_complex.ddl", 3, 10, 10, 10, 10,"#set($suffix = '1')"),
		new AssertRow("test_schema_complex.ddl", 4, "test_schema_complex.ddl", 4, 11, 11, 11, 11,""),
		new AssertRow("test_schema_complex.ddl", 5, "test_schema_complex.ddl", 5, 12, 12, 12, 12,"-- ${databaseConnection}"),
		new AssertRow("test_schema_complex.ddl", 6, "test_schema_complex.ddl", 6, 13, 13, 13, 13,"-- ${databaseConnection.id}"),
		new AssertRow("test_schema_complex.ddl", 7, "test_schema_complex.ddl", 7, 14, 14, 14, 14,""),
		new AssertRow("test_schema_complex_2.ddl", 1, "test_schema_complex_2.ddl", 1, 15, 15, 15, 15, "CREATE TABLE CUSTOM_TEST2#if(${suffix})${suffix}#end"),
		new AssertRow("test_schema_complex_2.ddl", 2, "test_schema_complex_2.ddl", 2, 16, 16, 16, 16,""),
		new AssertRow("test_schema_complex_2.ddl", 3, "test_schema_complex_2.ddl", 3, 17, 17, 17, 17, "("),
		new AssertRow("test_schema_complex_2.ddl", 4, "test_schema_complex_2.ddl", 4, 18, 18, 18, 18, "\tID INT NOT NULL,"),
		new AssertRow("test_schema_complex_2.ddl", 5, "test_schema_complex_2.ddl", 5, 19, 19, 19, 19, "\tDI INT NOT NULL,"),
		new AssertRow("test_schema_complex_2.ddl", 6, "test_schema_complex_2.ddl", 6, 20, 20, 20, 20, "\tPRIMARY KEY (ID, DI)"),
		new AssertRow("test_schema_complex_2.ddl", 7, "test_schema_complex_2.ddl", 7, 21, 21, 21, 21, ");;"),
		new AssertRow("test_schema_complex_2.ddl", 7, "test_schema_complex_2.ddl", 7, 22, 22, 22, 22,""),
		new AssertRow("test_schema_complex.ddl", 9, "test_schema_complex.ddl", 9, 23, 23, 23, 23, ""),
		new AssertRow("test_schema_complex.ddl", 10, "test_schema_complex.ddl", 10, 24, 24, 24, 24, "CREATE TABLE CUSTOM_TEST"),
		new AssertRow("test_schema_complex.ddl", 11, "test_schema_complex.ddl", 11, 25, 25, 25, 25, "("),
		new AssertRow("test_schema_complex.ddl", 12, "test_schema_complex.ddl", 12, 26, 26, 26, 26, "\tID INT NOT NULL,"),
		new AssertRow("test_schema_complex.ddl", 13, "test_schema_complex.ddl", 13, 27, 27, 27, 27, "\tDI INT NOT NULL,"),
		new AssertRow("test_schema_complex.ddl", 14, "test_schema_complex.ddl", 14, 28, 28, 28, 28, "\t${databaseConnection.id} INT NULL,"),
		new AssertRow("test_schema_complex.ddl", 15, "test_schema_complex.ddl", 15, 29, 29, 29, 29, "\tPRIMARY KEY (ID, DI)"),
		new AssertRow("test_schema_complex.ddl", 16, "test_schema_complex.ddl", 16, 30, 30, 30, 30, ");;")
	};

	private static final AssertRow [] ASSERT_STS = new AssertRow[]
	{
		new AssertRow("test_schema_complex_2.ddl", 1, "test_schema_complex_2.ddl", 7, 1, 7, 1, 7, "CREATE TABLE CUSTOM_TEST2#if(${suffix})${suffix}#end\n" +
				"\n" +
				"(\n" +
				"\tID INT NOT NULL,\n" +
				"\tDI INT NOT NULL,\n" +
				"\tPRIMARY KEY (ID, DI)\n" +
				")"),
		new AssertRow("test_schema_complex_2.ddl", 7, "test_schema_complex_2.ddl", 7, 8, 21, 8, 21, "\n" +
				"\n" +
				"\n" +
				"#set($suffix = '1')\n" +
				"\n" +
				"-- ${databaseConnection}\n" +
				"-- ${databaseConnection.id}\n" +
				"\n" +
				"CREATE TABLE CUSTOM_TEST2#if(${suffix})${suffix}#end\n" +
				"\n" +
				"(\n" +
				"\tID INT NOT NULL,\n" +
				"\tDI INT NOT NULL,\n" +
				"\tPRIMARY KEY (ID, DI)\n" +
				")"),
		new AssertRow("test_schema_complex_2.ddl", 7, "test_schema_complex.ddl", 16, 22, 30, 22, 30, "\n" +
				"\n" +
				"\n" +
				"CREATE TABLE CUSTOM_TEST\n" +
				"(\n" +
				"\tID INT NOT NULL,\n" +
				"\tDI INT NOT NULL,\n" +
				"\t${databaseConnection.id} INT NULL,\n" +
				"\tPRIMARY KEY (ID, DI)\n" +
				")")
	};

	@Test
	public void lines() throws IOException, TestExecutionError
	{
		SQLAggregatorImpl sqlai = new SQLAggregatorImpl(IOUtils.toString(getClass().getClassLoader().getResourceAsStream("resource/sqlaggregator_line_test_source.txt")));

		checkLines(sqlai.getLineAggregator(), ASSERT_LINES);
		checkLines(sqlai.getStatementAggregator(), ASSERT_STS);
	}

	private void checkLines(SQLAggregator.Aggregator sa, AssertRow [] rows)
	{
		int offset = 0;

		for (AssertRow row : rows)
		{
			Assert.assertTrue("EOD", sa.hasNext());

			SQLAggregator.FileRef fileRef = sa.next();
			Assert.assertEquals(offset + ":brr " + row.text, row.bref, fileRef.getBeginRef().getRefName());
			Assert.assertEquals(offset + ":brl " + row.text, row.bline, fileRef.getBeginRef().getLineNumber());
			Assert.assertEquals(offset + ":err " + row.text, row.eref, fileRef.getEndRef().getRefName());
			Assert.assertEquals(offset + ":erl " + row.text, row.eline, fileRef.getEndRef().getLineNumber());
			Assert.assertEquals(offset + ":te " + row.text, row.text, fileRef.getLine().replace("\r\n", "\n").replace("\r", "\n"));
			Assert.assertEquals(offset + ":btl " + row.text, row.beg_totalLine, fileRef.getBeginAggregatedLineNumber());
			Assert.assertEquals(offset + ":etl " + row.text, row.end_totalLine, fileRef.getEndAggregatedLineNumber());
			Assert.assertEquals(offset + ":bcl " + row.text, row.beg_cleanLine, fileRef.getBeginCleanLineNumber());
			Assert.assertEquals(offset + ":ecl " + row.text, row.end_cleanLine, fileRef.getEndCleanLineNumber());

			offset++;
		}

		Assert.assertFalse("Left-over data", sa.hasNext());
	}
}
