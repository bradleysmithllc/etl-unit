class test_no_class_annotation
{
	@Test(expected-error-id: 'ERR_NO_CONNECTION_SPECIFIED')
	noMethodAnnotation()
	{
		execute(sql: 'SELECT * FROM EDW1');
	}

	@Database(id: 'edw1')
	@Test
	edw1MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW1');
	}

	@Database(id: 'edw2')
	@Test
	edw2MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW2');
	}

	@Database(id: 'edw3')
	@Test
	edw3MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW3');
	}
}

@Database(id: 'edw1')
class test_edw1_class_annotation
{
	@Test
	noMethodAnnotation()
	{
		execute(sql: 'SELECT * FROM EDW1');
	}

	@Database(id: 'edw2')
	@Test
	edw2MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW2');
		execute(
			sql: 'SELECT * FROM EDW1',
			connection-id: 'edw1'
		);
	}

	@Database(id: 'edw3')
	@Test
	edw3MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW3');
		execute(
			sql: 'SELECT * FROM EDW1',
			connection-id: 'edw1'
		);
	}
}

@Database(id: 'edw2')
class test_edw2_class_annotation
{
	@Test
	noMethodAnnotation()
	{
		execute(sql: 'SELECT * FROM EDW2');
	}

	@Database(id: 'edw1')
	@Test
	edw2MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW1');
		execute(
			sql: 'SELECT * FROM EDW2',
			connection-id: 'edw2'
		);
	}

	@Database(id: 'edw3')
	@Test
	edw3MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW3');
		execute(
			sql: 'SELECT * FROM EDW2',
			connection-id: 'edw2'
		);
	}
}

@Database(id: 'edw3')
class test_edw3_class_annotation
{
	@Test
	noMethodAnnotation()
	{
		execute(sql: 'SELECT * FROM EDW3');
	}

	@Database(id: 'edw1')
	@Test
	edw2MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW1');
		execute(
			sql: 'SELECT * FROM EDW3',
			connection-id: 'edw3'
		);
	}

	@Database(id: 'edw2')
	@Test
	edw3MethodAnnotationNoModes()
	{
		execute(sql: 'SELECT * FROM EDW2');
		execute(
			sql: 'SELECT * FROM EDW3',
			connection-id: 'edw3'
		);
	}
}

@Database(id: 'edw1', modes: ['src1', 'tgt1'])
class test_edw1_class_annotation_with_src_mode
{
	@Test
	noMethodAnnotation()
	{
		assert(
			source-table: 'EDW1',
			mode: 'src1',
			assertion-mode: 'empty'
		);
		execute(sql: 'INSERT INTO EDW1(ID, DI) VALUES(1,2)');
		assert(
			source-table: 'EDW1',
			mode: 'tgt1',
			assertion-mode: 'empty'
		);
		assert(
			source-table: 'EDW1',
			mode: 'src1',
			assertion-mode: 'notEmpty'
		);
		execute(sql: 'INSERT INTO EDW1(ID, DI) VALUES(1,2)', mode: 'tgt1');
		assert(
			source-table: 'EDW1',
			mode: 'tgt1',
			assertion-mode: 'notEmpty'
		);
	}
}

@Database(id: 'edw1', modes: ['tgt1', 'src1', 'tgt2'])
class test_edw1_class_annotation_with_tgt_mode
{
	@Test
	noMethodAnnotation()
	{
		execute(sql: 'INSERT INTO EDW1(ID, DI) VALUES(1,2)');
		assert(
			source-table: 'EDW1',
			mode: 'src1',
			assertion-mode: 'empty'
		);
		assert(
			source-table: 'EDW1',
			mode: 'tgt2',
			assertion-mode: 'empty'
		);
		assert(
			source-table: 'EDW1',
			mode: 'tgt1',
			assertion-mode: 'notEmpty'
		);
		execute(sql: 'INSERT INTO EDW1(ID, DI) VALUES(1,2)', mode: 'src1');
		assert(
			source-table: 'EDW1',
			mode: 'src1',
			assertion-mode: 'notEmpty'
		);
	}

	@Database(id: 'edw1', modes: ['tgt2'])
	@Test
	methodAnnotation()
	{
		assert(
			source-table: 'EDW1',
			mode: 'tgt2',
			assertion-mode: 'empty'
		);
		execute(sql: 'INSERT INTO EDW1(ID, DI) VALUES(1,2)');
		assert(
			source-table: 'EDW1',
			mode: 'tgt2',
			assertion-mode: 'notEmpty'
		);
	}
}

@Database(id: 'edw1_1', modes: ['tgt1', 'src1', 'tgt2'])
@Database(id: 'edw1_2', modes: ['src1', 'tgt1'])
@Database(id: 'edw1_3')
class testMultipleConnections
{
	@Test
	noMethodAnnotation()
	{
		stage(
			source: 'CUSTOM_TEST_TRUNCATE_STUFF',
			target-table: 'EDW1',
			connections:
			{
				edw1_1: { modes: ['tgt1', 'src1'] },
				edw1_2: { mode: 'tgt1' },
				edw1_3: {}
			}
		);
		// EDW1_1
		assert(
			source-table: 'EDW1',
			connection-id: 'edw1_1',
			mode: 'tgt1',
			assertion-mode: 'notEmpty'
		);
		assert(
			source-table: 'EDW1',
			connection-id: 'edw1_1',
			mode: 'src1',
			assertion-mode: 'notEmpty'
		);
		assert(
			source-table: 'EDW1',
			connection-id: 'edw1_1',
			mode: 'tgt2',
			assertion-mode: 'empty'
		);

		// EDW1_2
		assert(
			source-table: 'EDW1',
			connection-id: 'edw1_2',
			mode: 'tgt1',
			assertion-mode: 'notEmpty'
		);
		assert(
			source-table: 'EDW1',
			connection-id: 'edw1_2',
			mode: 'src1',
			assertion-mode: 'empty'
		);

		assert(
			source-table: 'EDW1',
			connection-id: 'edw1_3',
			assertion-mode: 'notEmpty'
		);
	}
}