#fetch<test_schema_complex_2.ddl>

#set($suffix = '1')

-- ${databaseConnection}
-- ${databaseConnection.id}

#fetch<test_schema_complex_2.ddl>

CREATE TABLE CUSTOM_TEST
(
	ID INT NOT NULL,
	DI INT NOT NULL,
	${databaseConnection.id} INT NULL,
	PRIMARY KEY (ID, DI)
);;