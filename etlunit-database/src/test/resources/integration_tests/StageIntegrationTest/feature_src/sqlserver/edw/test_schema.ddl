\#include<test_schema20.ddl>
\#include<test_schema20.ddl>
\#include<test_schema20.ddl>
\#include<test_schema20.ddl>
\#include<test_schema20.ddl>
\#include<test_schema20.ddl>
\#include<test_schema20.ddl>
\#include<test_schema20.ddl>

#fetch<test_schema30.ddl>
#fetch<test_schema30.ddl>

#require<DIM_STORE>

CREATE TABLE CUSTOM_TEST
(
	ID INT NOT NULL,
	DI INT NOT NULL,
	PRIMARY KEY (ID, DI)
);

CREATE TABLE DDL_TEST
(
	COl1 VARCHAR(1),
	COl2 VARCHAR(2),
	COl3 VARCHAR(3),
	COl4 VARCHAR(4),
	COl5 VARCHAR(5),
	COl6 VARCHAR(6),
	COl7 VARCHAR(7),
	COl8 VARCHAR(8),
	COl9 VARCHAR(9),
	COl10 VARCHAR(10),
	COl11 VARCHAR(11),
	COl12 VARCHAR(12),
	COl13 VARCHAR(13),
	COl14 VARCHAR(14),
	COl15 VARCHAR(15),
	COl16 VARCHAR(16),
	COl17 VARCHAR(17),
	COl18 VARCHAR(18),
	COl19 VARCHAR(19),
	COl20 VARCHAR(20)
);