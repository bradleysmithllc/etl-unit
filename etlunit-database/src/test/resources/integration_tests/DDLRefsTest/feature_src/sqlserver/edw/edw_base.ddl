\#include<edw_extended.ddl>

\#include<edw-II/edw_ii.ddl>

\#include</sqlserver/edw-III/edw_iii.ddl>

\#include</oracle/db.ddl>

\#include</oracle/edw-ocl/edw_ocl.ddl>

CREATE TABLE RAW
(
#fetch<raw_columns.ddl>
);;

CREATE TABLE RAW2
(
#fetch<raw_columns.ddl>,
  RAW_ID INT NULL
);;

CREATE TABLE RESOLVED (
#fetch<id.ddl>,
#fetch<db.ddl>,
#fetch<db2.ddl>,
#fetch<impl.ddl>
);;