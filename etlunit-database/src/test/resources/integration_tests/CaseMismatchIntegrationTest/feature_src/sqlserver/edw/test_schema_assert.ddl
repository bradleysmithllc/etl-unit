CREATE TABLE TABLEPASS
(
	ID INT NOT NULL
);;

CREATE TABLE TABLEDATA
(
	ID INT NOT NULL,
	DI INT NOT NULL,
	raw_id int not null
);;

CREATE TABLE TABLEFAIL
(
	ID INT NOT NULL
);;

CREATE SCHEMA EMPTY;;

CREATE TABLE EMPTY.TABLEPASS
(
	ID INT NOT NULL
);;

CREATE TABLE EMPTY.TABLEFAIL
(
	ID INT NOT NULL
);;