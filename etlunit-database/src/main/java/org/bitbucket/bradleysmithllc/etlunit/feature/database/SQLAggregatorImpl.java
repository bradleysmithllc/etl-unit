package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQLAggregatorImpl implements SQLAggregator
{
	private class DDLRefImpl implements DDLRef
	{
		private final String refName;
		private final int lineNumber;

		private DDLRefImpl(String refName, int lineNumber)
		{
			this.refName = refName;
			this.lineNumber = lineNumber;
		}

		@Override
		public String getRefName()
		{
			return refName;
		}

		@Override
		public int getLineNumber()
		{
			return lineNumber;
		}

		@Override
		public String describe()
		{
			return " ddlref: { refName: '" + refName + "', lineNumber: " + lineNumber + "}";
		}
	}

	private final List<FileRef> lineList = new ArrayList<>();
	private final List<FileRef> statementList = new ArrayList<>();
	private final String text;

	public SQLAggregatorImpl(DDLSourceRef ddlRef, SQLLocator locator, DatabaseConnection databaseConnection) throws IOException, TestExecutionError
	{
		this(makeText(ddlRef, locator, databaseConnection));
	}

	public SQLAggregatorImpl(String text) throws IOException, TestExecutionError
	{
		this.text = text;

		// Use the output of the line processor and break it into statements since JDBC doesn't support multiple in one statement
		splitLines();
	}

	private void splitLines()
	{
		// pull tokens out
		DDLInstrumentationExpression ddlie = new DDLInstrumentationExpression(text);

		try
		{
			int lastOffset = 0;
			StringBuffer stb = new StringBuffer();

			String thisRef = "synthetic";
			int thisLine = -1;
			int totalLines = 0;
			int cleanLine = 0;

			while (ddlie.hasNext())
			{
				if (ddlie.hasNewLine())
				{
					// this is a new line.  Advance the line number markers
					totalLines++;
					cleanLine++;
					stb.append(text.substring(lastOffset, ddlie.start()));
					lastOffset = ddlie.end();

					lineList.add(new FileRefImpl(new DDLRefImpl(thisRef, thisLine), new DDLRefImpl(thisRef, thisLine), totalLines, totalLines, cleanLine, cleanLine, stb.toString()));
					stb.setLength(0);
				}
				else
				{
					thisRef = ddlie.getRef();
					thisLine = ddlie.getLineNo();

					stb.append(text.substring(lastOffset, ddlie.start()));
					lastOffset = ddlie.end();
				}
			}

			if (lastOffset < text.length())
			{
				stb.append(text.substring(lastOffset));
				lineList.add(new FileRefImpl(new DDLRefImpl(thisRef, thisLine), new DDLRefImpl(thisRef, thisLine), totalLines, totalLines, cleanLine, cleanLine, stb.toString()));
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("?", e);
		}

		StringWriter stw = new StringWriter();
		PrintWriter prw = new PrintWriter(stw);

		StringBuffer buffer = stw.getBuffer();
		FileRef beginRec = null;

		for (FileRef lineRec : lineList)
		{
			if (beginRec == null)
			{
				beginRec = lineRec;
			}

			String [] sts = lineRec.getLine().split(";;", -1);

			// append the first result.
			prw.print(sts[0]);

			for (int i = 1; i < sts.length; i++)
			{
				// each subsequent match is a complete statement, until the last one which is either a "" or should be a new line
				prw.flush();
				addStatement(statementList,
						beginRec.getBeginRef(),
						lineRec.getEndRef(),
						beginRec.getBeginAggregatedLineNumber(),
						lineRec.getEndAggregatedLineNumber(),
						beginRec.getBeginCleanLineNumber(),
						lineRec.getEndCleanLineNumber(),
						stw.toString());
				buffer.setLength(0);

				beginRec = lineRec;

				// in the case of the last entry, just append to the string
				if (i == (sts.length - 1))
				{
					if (!sts[i].trim().equals(""))
					{
						prw.println(sts[i]);
					}
					else
					{
						beginRec = null;
					}
				}
				else
				{
					// otherwise, append a full statement
					addStatement(statementList,
							lineRec.getBeginRef(),
							lineRec.getEndRef(),
							lineRec.getBeginAggregatedLineNumber(),
							lineRec.getEndAggregatedLineNumber(),
							lineRec.getBeginCleanLineNumber(),
							lineRec.getEndCleanLineNumber(),
							sts[i]);
				}
			}

			// append a newline
			prw.println();
		}

		// if there is anything left over, push it on to the string writer stack
		prw.flush();

		if (buffer.length() != 0)
		{
			// attribute to the last line processed
			FileRef lastRec = lineList.get(lineList.size() - 1);
			addStatement(statementList,
					beginRec == null ? lastRec.getBeginRef() : beginRec.getBeginRef(),
					lastRec.getEndRef(),
					beginRec == null ? lastRec.getBeginAggregatedLineNumber() : beginRec.getBeginAggregatedLineNumber(),
					lastRec.getEndAggregatedLineNumber(),
					beginRec == null ? lastRec.getBeginAggregatedLineNumber() : beginRec.getBeginCleanLineNumber(),
					lastRec.getEndCleanLineNumber(),
					stw.toString());
		}
	}

	private void addStatement(List<FileRef> statementList, DDLRef bref, DDLRef eref, int baggregatedLineNumber, int eaggregatedLineNumber, int bcleanLine, int ecleanLine, String st)
	{
		if (!st.trim().equals(""))
		{
			statementList.add(new FileRefImpl(bref, eref, baggregatedLineNumber, eaggregatedLineNumber, bcleanLine, ecleanLine, st));
		}
	}

	public static String makeText(DDLSourceRef ddlRef, SQLLocator locator, DatabaseConnection databaseConnection) throws TestExecutionError, IOException
	{
		String ptext = locator.locate(ddlRef, databaseConnection);

		DDLDirectiveNameExpression ddldne = new DDLDirectiveNameExpression(ptext);

		Map<String, String> includedReferences = new HashMap<String, String>();
		Map<String, String> provideMap = new HashMap<String, String>();
		Map<String, String> requireMap = new HashMap<String, String>();

		while (ddldne.hasNext())
		{
			String subText = "";

			int thisStart = ddldne.start();

			String directive = ddldne.getDirective();
			String ddlReference = ddldne.getDdlReference();
			DDLSourceRef currentRefContext = getCurrentRefLine(ptext, ddldne.start());

			// include is guaranteed once per ddl - fetch is varbatim
			if (directive.equals("include") || directive.equals("fetch"))
			{
				// pull out the full ddl source ref
				DDLReferenceNameExpression ddlrne = new DDLReferenceNameExpression(ddlReference);

				// replace with current values and pass on
				if (!ddlrne.matches())
				{
					throw new IllegalStateException("Bad source reference: " + ddlReference);
				}

				DDLSourceRef thisSourceRef = !ddlrne.hasAbsolute() ? new DDLSourceRef(
					ddlrne.hasDbImplementationId() ? ddlrne.getDbImplementationId() : currentRefContext.getImplementationId(),
					ddlrne.hasDbId() ? ddlrne.getDbId() : currentRefContext.getDatabaseId(),
					ddlrne.getDdlReference()
				) : new DDLSourceRef(ddlrne.getDdlReference());

				if (directive.equals("fetch") || !includedReferences.containsKey(thisSourceRef.urlPath()))
				{
					subText = locator.locate(thisSourceRef, databaseConnection);

					includedReferences.put(thisSourceRef.urlPath(), "");
				}
			}
			else if (directive.equals("require"))
			{
				requireMap.put(ddlReference, ddlReference);
			}
			else if (directive.equals("provide"))
			{
				provideMap.put(ddlReference, ddlReference);
			}
			else
			{
				throw new TestExecutionError("Directive '" + directive + "' not understood", DatabaseConstants.ERR_BAD_DIRECTIVE);
			}

			int startIndex = ddldne.start();
			int endIndex = ddldne.end();

			// insert
			ptext = (startIndex == 0 ? "" : ptext.substring(0, startIndex)) + subText + ptext.substring(endIndex);

			ddldne = new DDLDirectiveNameExpression(ptext);
		}

		// validate that all 'require's have been met
		for (String require : requireMap.keySet())
		{
			if (!provideMap.containsKey(require))
			{
				throw new TestExecutionError("Script requires '" + require + "' but it was not provided: " + ptext, DatabaseConstants.ERR_UNMET_REQUIRE);
			}
		}

		return ptext;
	}

	private static DDLSourceRef getCurrentRefLine(String ptext, int start) {
		DDLInstrumentationExpression ddlie = new DDLInstrumentationExpression(ptext);

		// match until the specified offset is passed, then use the last one observed
		DDLSourceRef lastDDLRef = null;

		while (ddlie.hasNext())
		{
			if (ddlie.hasNewLine())
			{
				continue;
			}

			DDLReferenceNameExpression ddlrne = new DDLReferenceNameExpression(ddlie.getRef());

			if (!ddlrne.matches())
			{
				throw new IllegalStateException("Illegal DDL reference: " + ddlie.getRef());
			}

			if (ddlie.end() > start)
			{
				return lastDDLRef;
			}

			lastDDLRef = new DDLSourceRef(
				ddlrne.hasDbImplementationId() ? ddlrne.getDbImplementationId() : null,
				ddlrne.hasDbId() ? ddlrne.getDbId() : null,
				ddlrne.getDdlReference()
			);
		}

		if (lastDDLRef == null)
		{
			throw new IllegalStateException("No current reference");
		}

		return lastDDLRef;
	}

	public String getText()
	{
		return text;
	}

	@Override
	public String getCleanText()
	{
		StringWriter stringWriter = new StringWriter();
		PrintWriter prw = new PrintWriter(stringWriter);

		for (FileRef line : lineList)
		{
			prw.println(line.getLine());
		}

		return stringWriter.toString();
	}

	public Aggregator getLineAggregator()
	{
		return new Aggregator()
		{
			int index = 0;

			public boolean hasNext()
			{
				return index < lineList.size();
			}

			public FileRef next()
			{
				return lineList.get(index++);
			}
		};
	}

	public Aggregator getStatementAggregator()
	{
		return new Aggregator()
		{
			int index = 0;

			public boolean hasNext()
			{
				return index < statementList.size();
			}

			public FileRef next()
			{
				return statementList.get(index++);
			}
		};
	}
}
