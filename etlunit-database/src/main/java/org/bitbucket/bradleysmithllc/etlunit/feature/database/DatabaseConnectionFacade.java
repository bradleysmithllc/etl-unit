package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.DatabaseDefinitionsProperty;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

/**
 * This object wraps a database connection, only permitting one mode to be used.
 */
public class DatabaseConnectionFacade implements DatabaseConnection {
	private final DatabaseConnection delegate;
	private final String targetMode;

	public DatabaseConnectionFacade(DatabaseConnection delegate, String mode) {
		targetMode = mode;
		this.delegate = delegate;
	}

	public String getId() {
		return delegate.getId();
	}

	//@Override
	//public Database getDatabase() {
		//return delegate.getDatabase();
	//}

	//@Override
	//public boolean hasDatabase() {
	//	return delegate.hasDatabase();
	//}

	public String getAdminUserName()
	{
		return delegate.getAdminUserName();
	}

	public String getAdminPassword()
	{
		return delegate.getAdminPassword();
	}

	public String getImplementationId() {
		return delegate.getImplementationId();
	}

	@Override
	public String defaultSchema() {
		return delegate.defaultSchema();
	}

	public String getServerName() {
		return delegate.getServerName();
	}

	@Override
	public String getServerAddress() throws UnknownHostException {
		return delegate.getServerAddress();
	}

	public List<String> getSchemaScripts() {
		return delegate.getSchemaScripts();
	}

	public DatabaseDefinitionsProperty getDatabaseConfiguration() {
		return delegate.getDatabaseConfiguration();
	}

	@Override
	public JsonNode getRawDatabaseConfiguration() {
		return delegate.getRawDatabaseConfiguration();
	}

	@Override
	public Map<String, String> getDatabaseProperties() {
		return delegate.getDatabaseProperties();
	}

	//@Override
	//public void setDatabase(Database database) {
	//	delegate.setDatabase(database);
	//}

	@Override
	public int getServerPort() {
		return delegate.getServerPort();
	}
}
