package org.bitbucket.bradleysmithllc.etlunit.feature.database.db;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.extract.ExtractRequest;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;

import java.util.List;
import java.util.function.Function;

public interface Table extends JsonRepresentation {
	Schema getSchema();

	Catalog getCatalog();

	String getPhysicalName();
	String getLogicalName();

	void addColumn(TableColumn timpl);

	boolean hasColumn(String columnName);

	TableColumn getColumn(String columnName);

	List<TableColumn> getTableColumns();

	List<String> getTableColumnNames();

	void resetPrimaryKeyColumns();

	void addPrimaryKeyColumn(TableColumn column);

	List<TableColumn> getPrimaryKeyColumns();

	void resetOrderColumns();

	void addOrderKeyColumn(TableColumn column);

	List<TableColumn> getOrderKeyColumns();

	DataFileSchema getDataFileSchema();

	void setDataFileSchema(DataFileSchema dataFileSchema);

	String getQualifiedName();

	boolean hasIdentityColumns();

	default String createSQLInsert() {
		return createSQLInsert((name) -> { return name;});
	}

	default String createSQLInsert(DatabaseImplementation dbi) {
		return createSQLInsert((name) -> { return dbi.escapeIdentifierIfNeeded(name);});
	}

	/**
	 * Create sql for the given destination implementation.
	 * @return
	 */
	String createSQLInsert(Function<String, String> escapeFunction);

	default String createSQLSelect(Function<String, String> escapeFunction) {
		return createSQLSelect(escapeFunction, true, null);
	}

	default String createSQLSelect(DatabaseImplementation dbi) {
		return createSQLSelect(dbi, true, null);
	}

	default String createSQLSelect() {
		return createSQLSelect((name) -> {return name;}, true, null);
	}

	default String createSQLSelect(DatabaseImplementation dbi, boolean ordered, Pair<String, String> alternateTableName) {
		return createSQLSelect((name) -> {return dbi.escapeIdentifierIfNeeded(name);}, ordered, alternateTableName);
	}
	String createSQLSelect(Function<String, String> escapeFunction, boolean ordered, Pair<String, String> alternateTableName);
	String createSQLSelect(boolean ordered);

	Table createSubView(List<String> list, ExtractRequest.ColumnListMode listMode);

	void setSqlScriptSource(String sqlScriptSource);

	default String createSQLSelectUsingAlternateTableName(DatabaseImplementation impl, Pair<String, String> alternateTableName) {
		return createSQLSelect(impl, true, alternateTableName);
	}

	enum type {
		table,
		view,
		system_table,
		temp_table,
		synthetic,
		sql
	}

	type getType();

	String getDBLocalName();
}
