package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;

import java.io.IOException;

public interface SQLAggregator
{
	interface DDLRef
	{
		String getRefName();
		int getLineNumber();
		String describe();
	}

	interface FileRef
	{
		DDLRef getBeginRef();
		DDLRef getEndRef();

		int getBeginAggregatedLineNumber();
		int getEndAggregatedLineNumber();

		int getBeginCleanLineNumber();
		int getEndCleanLineNumber();

		String getLine();

		String describe();
	}

	interface Aggregator
	{
		boolean hasNext();
		FileRef next();
	}

	interface SQLLocator
	{
		String locate(DDLSourceRef ref, DatabaseConnection connection) throws IOException, TestExecutionError;
	}

	/**
	 * Gets the clean text, without instrumentation.
	 * @return
	 */
	String getCleanText();

	String getText();
	Aggregator getLineAggregator();
	Aggregator getStatementAggregator();
}
