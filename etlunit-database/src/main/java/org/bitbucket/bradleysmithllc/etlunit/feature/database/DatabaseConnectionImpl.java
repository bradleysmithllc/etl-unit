package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.DatabaseDefinitionsProperty;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseConnectionImpl implements DatabaseConnection {
	private final String implementationId;

	private final String id;
	private final String projectVersion;
	private final String projectName;
	private final String projectUid;
	private final String userName;
	private final String defaultSchema;
	private final String serverName;
	private String serverAddress;
	private final int serverPort;

	private final String adminUserName;
	private final String adminPassword;

	private final List<String> ddlScripts = new ArrayList<String>();

	private final DatabaseDefinitionsProperty databaseConfiguration;
	private final Map<String, String> implementationProperties = new HashMap<String, String>();

	private Database database;

	public DatabaseConnectionImpl(
			String id,
			DatabaseDefinitionsProperty databaseConfiguration,
			DatabaseConfiguration configuration,
			String projectName,
			String projectVersion,
			String userName,
			String projectUid
	) {
		this.projectName = projectName;
		this.projectVersion = projectVersion;
		this.userName = userName;
		this.projectUid = projectUid;
		this.id = id;

		this.databaseConfiguration = databaseConfiguration;

		String eImplementationId = databaseConfiguration.getImplementationId();

		if (eImplementationId == null) {
			implementationId = configuration.getDefaultImplementationId();

			if (implementationId == null) {
				throw new IllegalArgumentException("implementation-id is not specified and no default is provided");
			}
		} else {
			implementationId = eImplementationId;
		}

		Map<String, String> effectiveImplementationProperties = configuration.getEffectiveImplementationProperties(databaseConfiguration);

		if (effectiveImplementationProperties != null)
		{
			implementationProperties.putAll(effectiveImplementationProperties);
		}

		serverName = configuration.getEffectiveServerName(databaseConfiguration);
		serverPort = configuration.getEffectiveServerPort(databaseConfiguration);

		List<String> ddl = databaseConfiguration.getSchemaScripts();

		if (ddl != null) {
			for (String script : ddl) {
				ddlScripts.add(script);
			}
		}

		adminUserName = configuration.getEffectiveUserName(databaseConfiguration);
		adminPassword = configuration.getEffectivePassword(databaseConfiguration);

		defaultSchema = databaseConfiguration.getDefaultSchema();
	}

	@Override
	public String defaultSchema() {
		return defaultSchema;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public String getId() {
		return id;
	}

	public void setDatabase(Database database) {
		if (this.database != null) {
			throw new IllegalStateException("Database already set");
		}

		this.database = database;
	}

	public Database getDatabase() {
		if (database == null) {
			throw new IllegalStateException("Database not yet available");
		}

		return database;
	}

	public boolean hasDatabase() {
		return database != null;
	}

	public String getImplementationId() {
		return implementationId;
	}

	public String getDatabaseName(String mode) {
		return
				getUserName() +
						"_" + projectName +
						"_" + projectVersion +
						"_" + EtlUnitStringUtils.sanitize(getId(), '_') +
						(mode == null ? "" : ("_" + EtlUnitStringUtils.sanitize(mode, '_'))) +
						"_" + projectUid;
	}

	private String getUserName() {
		return userName;
	}

	public String getLoginName(String mode) {
		return getDatabaseName(mode);
	}

	public String getPassword(String mode) {
		return getDatabaseName(mode);
	}

	public String getServerName() {
		return serverName;
	}

	@Override
	public String getServerAddress() throws UnknownHostException {
		if (serverAddress == null)
		{
			InetAddress serverA = InetAddress.getByName(getServerName());
			serverAddress = serverA.getHostAddress();
		}

		return serverAddress;
	}

	public int getServerPort() {
		return serverPort;
	}

	public List<String> getSchemaScripts() {
		return ddlScripts;
	}

	public DatabaseDefinitionsProperty getDatabaseConfiguration() {
		return databaseConfiguration;
	}

	@Override
	public JsonNode getRawDatabaseConfiguration() {
		// write the value back to a string and reload
		ObjectMapper objectMapper = new ObjectMapper();

		String rawJson = null;
		try {
			rawJson = objectMapper.writeValueAsString(databaseConfiguration);

			return JsonLoader.fromString(rawJson);
		} catch (Exception e) {
			throw new RuntimeException("Error roundtripping json configuration", e);
		}
	}

	@Override
	public Map<String, String> getDatabaseProperties() {
		return implementationProperties;
	}
}
