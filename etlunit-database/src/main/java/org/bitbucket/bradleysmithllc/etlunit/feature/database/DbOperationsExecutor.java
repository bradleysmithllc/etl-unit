package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql.ExecuteSqlHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql.ExecuteSqlRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql_procedure.ExecuteSqlProcedureHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql_procedure.ExecuteSqlProcedureRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql_script.ExecuteSqlScriptHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql_script.ExecuteSqlScriptRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.extend.Extender;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

public class DbOperationsExecutor implements Extender, ExecuteSqlHandler, ExecuteSqlScriptHandler, ExecuteSqlProcedureHandler {
	private Log applicationLog;
	private final DatabaseFeatureModule databaseFeature;
	private DatabaseRuntimeSupport databaseRuntimeSupport;
	private RuntimeSupport runtimeSupport;
	private JDBCClient jdbcClient;

	public DbOperationsExecutor(DatabaseFeatureModule databaseFeature) {
		this.databaseFeature = databaseFeature;
	}

	@Inject
	public void setJdbcClient(JDBCClient client) {
		jdbcClient = client;
	}

	@Inject
	public void setDatabaseRuntimeSupport(DatabaseRuntimeSupport databaseRuntimeSupport) {
		this.databaseRuntimeSupport = databaseRuntimeSupport;
	}

	@Inject
	public void setRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	@Inject
	public void setLogger(@Named("applicationLog") Log log) {
		applicationLog = log;
	}

	@Override
	public ClassResponder.action_code process(ETLTestMethod mt, ETLTestOperation etlTestOperation, ETLTestValueObject etlTestValueObject, VariableContext context, ExecutionContext executionContext, int executor) {
		return ClassResponder.action_code.handled;
	}

	public Feature getFeature() {
		return databaseFeature;
	}

	@Override
	public action_code executeSql(ExecuteSqlRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		String sqlText = request.getSql();

		applicationLog.info("executing statement " + sqlText);

		// process with the context
		final String sql;
		try {
			sql = VelocityUtil.writeTemplate(sqlText, variableContext);
		} catch (Exception e) {
			throw new TestExecutionError("", TestConstants.ERR_IO_ERROR, e);
		}

		DatabaseClassListener.ConnectionMode connMode = DatabaseClassListener.getConnection(databaseFeature, valueObject, variableContext, testOperation.getTestMethod()).get(0);

		final DatabaseConnection conn = connMode.conn;

		final String mode = connMode.getMode();

		jdbcClient.useStatement(conn, mode, new JDBCClient.StatementClient() {
			public void connection(Connection conn, Statement st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception {
				SQLAggregator sqlagg = databaseRuntimeSupport.resolveSQLRef(sql);

				SQLAggregator.Aggregator statementAggregator = sqlagg.getStatementAggregator();
				while (statementAggregator.hasNext()) {
					st.execute(statementAggregator.next().getLine());
				}
			}
		});

		return ClassResponder.action_code.handled;
	}

	@Override
	public action_code executeSqlProcedure(final ExecuteSqlProcedureRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		String script = request.getSqlProcedure().trim();

		final String argList;

		// dynamically create an argument list which exactly matches the supplied parameters
		StringBuilder argBuilder = new StringBuilder();

		List<String> orderedParameters = request.getParameters();

		if (orderedParameters != null && orderedParameters.size() != 0) {
			List pmap = (List) orderedParameters;

			for (int i = 0; i < pmap.size(); i++) {
				if (i != 0) {
					argBuilder.append(',');
				}

				argBuilder.append('?');
			}

			argList = " (" + argBuilder.toString() + ")";
		} else {
			argList = "()";
		}

		final String fscript = script;
		final String fscriptFull = fscript + argList;

		applicationLog.info("executing procedure " + fscript + " as " + fscriptFull);

		DatabaseClassListener.ConnectionMode connMode = DatabaseClassListener.getConnection(databaseFeature, valueObject, variableContext, testOperation.getTestMethod()).get(0);

		try {
			jdbcClient.useCallableStatement(connMode.conn, connMode.getMode(), new JDBCClient.CallableStatementClient() {
				@Override
				public String callText() {
					return "{call " + fscriptFull + "}";
				}

				@Override
				public void connection(Connection conn, CallableStatement st, DatabaseConnection connection, String mode,  DatabaseImplementation.database_role id) throws Exception {
					List<String> orderedParameters = request.getParameters();

					if (orderedParameters != null && orderedParameters.size() != 0) {
						List pmap = (List) orderedParameters;

						for (int i = 0; i < pmap.size(); i++) {
							st.setString(i + 1, String.valueOf(pmap.get(i)));
						}
					}

					st.execute();
				}
			});
		} catch (TestExecutionError exe) {
			throw new TestExecutionError("", DatabaseConstants.ERR_SQL_SP_FAILURE, exe);
		}

		return action_code.handled;
	}

	@Override
	public action_code executeSqlScript(ExecuteSqlScriptRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		String script = request.getSqlScript();

		applicationLog.info("executing script " + script);

		File scriptFile = new FileBuilder(runtimeSupport.getCurrentTestResourceDirectory("sql")).mkdirs().name(script + ".sql").file();

		if (!scriptFile.exists())
		{
			throw new TestExecutionError("Can not find sql script: " + script, DatabaseConstants.ERR_MISSING_SQL_SCRIPT);
		}

		// process with the context
		final String sqlText;
		try {
			sqlText = VelocityUtil.writeTemplate(IOUtils.readFileToString(scriptFile), variableContext);
		} catch (Exception e) {
			throw new TestExecutionError("", TestConstants.ERR_IO_ERROR, e);
		}

		DatabaseClassListener.ConnectionMode connMode = DatabaseClassListener.getConnection(databaseFeature, valueObject, variableContext, testOperation.getTestMethod()).get(0);

		final DatabaseConnection conn = connMode.conn;

		final String mode = connMode.getMode();

		jdbcClient.useStatement(conn, mode, new JDBCClient.StatementClient() {
			public void connection(Connection conn, Statement st, DatabaseConnection connection, String mode,  DatabaseImplementation.database_role id) throws Exception {
				SQLAggregator sqlagg = databaseRuntimeSupport.resolveSQLRef(sqlText);

				SQLAggregator.Aggregator statementAggregator = sqlagg.getStatementAggregator();
				while (statementAggregator.hasNext()) {
					st.execute(statementAggregator.next().getLine());
				}
			}
		});

		return ClassResponder.action_code.handled;
	}
}
