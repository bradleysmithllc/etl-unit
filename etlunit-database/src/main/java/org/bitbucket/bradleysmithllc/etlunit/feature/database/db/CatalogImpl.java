package org.bitbucket.bradleysmithllc.etlunit.feature.database.db;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import java.io.IOException;
import java.util.*;

public class CatalogImpl extends AbstractPhysicalLogicalNamed implements Catalog
{
	private final Database database;

	private final List<Schema> schemaList = new ArrayList<Schema>();
	private final Map<String, Schema> schemaMap = new HashMap<String, Schema>();

	public CatalogImpl(Database database, String name) {
		super(name);
		this.database = database;
	}

	public CatalogImpl(Database database) {
		super(null);
		this.database = database;
	}

	@Override
	public Database getDatabase() {
		return database;
	}

	@Override
	public List<Schema> getSchemas() {
		return Collections.unmodifiableList(schemaList);
	}

	@Override
	public Map<String, Schema> getSchemaMap() {
		return Collections.unmodifiableMap(schemaMap);
	}

	@Override
	public Schema getSchema(String name) {
		String safeNullableUppercase = EtlUnitStringUtils.getSafeNullableLowercase(name);
		Schema s = schemaMap.get(safeNullableUppercase);

		if (s == null)
		{
			return addSchema(new SchemaImpl(this, name));
		}

		return s;
	}

	public Schema addSchema(Schema schema)
	{
		if (schemaMap.containsKey(schema.getLogicalName()))
		{
			throw new IllegalArgumentException("Schema name [" + schema.getLogicalName() + "] already added to catalog");
		}

		schemaList.add(schema);
		schemaMap.put(schema.getLogicalName(), schema);

		touch();

		return schema;
	}

	@Override
	public void toMoreJson(JsonWriter writer) throws IOException {
		writer.name("schemas").beginArray();

		for (Schema table : schemaList)
		{
			writer.beginObject();
			table.toJson(writer);
			writer.endObject();
		}

		writer.endArray();
	}

	@Override
	public void fromMoreJson(JsonNode node) {
		JsonNode tables = node.get("schemas");

		if (tables != null && !tables.isNull())
		{
			ArrayNode nodeList = (ArrayNode) tables;

			Iterator<JsonNode> it = nodeList.iterator();

			while (it.hasNext())
			{
				JsonNode tnode = it.next();

				SchemaImpl timpl = new SchemaImpl(this);

				timpl.fromJson(tnode);

				addSchema(timpl);
			}
		}
	}

	public void touch()
	{
		if (database != null)
		{
			database.touch();
		}
	}
}
