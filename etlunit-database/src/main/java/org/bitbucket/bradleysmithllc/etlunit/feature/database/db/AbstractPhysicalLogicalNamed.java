package org.bitbucket.bradleysmithllc.etlunit.feature.database.db;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public abstract class AbstractPhysicalLogicalNamed implements JsonRepresentation {
	private String physicalName;
	private String logicalName;

	public AbstractPhysicalLogicalNamed(String physicalName) {
		this.physicalName = physicalName;
		this.logicalName = physicalName != null ? physicalName.toLowerCase() : null;
	}

	public String getPhysicalName() {
		return physicalName;
	}

	public String getLogicalName() {
		return logicalName;
	}

	@Override
	public final void toJson(JsonWriter writer) throws IOException {
		writer.name("physical-name");

		if (physicalName == null)
		{
			writer.nullValue();
		}
		else
		{
			writer.value(physicalName);
		}

		writer.name("logical-name");

		if (logicalName == null)
		{
			writer.nullValue();
		}
		else
		{
			writer.value(logicalName);
		}

		toMoreJson(writer);
	}

	protected abstract void toMoreJson(JsonWriter writer) throws IOException;

	@Override
	public final void fromJson(JsonNode node) {
		JsonNode jsonNode = node.get("physical-name");

		if (!jsonNode.isNull())
		{
			physicalName = jsonNode.asText();
		}

		jsonNode = node.get("logical-name");

		if (!jsonNode.isNull())
		{
			logicalName = jsonNode.asText();
		}

		fromMoreJson(node);
	}

	protected abstract void fromMoreJson(JsonNode node);
}
