package org.bitbucket.bradleysmithllc.etlunit.feature.database.util;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data.Connections;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data.DatabaseDefinitions;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data.ImplementationDefaults;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data.StageDataRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data_set.StageDataSetRequest;

import java.util.List;
import java.util.Set;

public class StageRequestContainer
{
	private final StageDataRequest stageDataRequest;
	private final StageDataSetRequest stageDataSetRequest;

	public StageRequestContainer(StageDataRequest request)
	{
		stageDataRequest = request;
		stageDataSetRequest = null;
	}

	public StageRequestContainer(StageDataSetRequest request)
	{
		stageDataSetRequest = request;
		stageDataRequest = null;
	}

	public String getTargetTable()
	{
		return stageDataRequest != null ? stageDataRequest.getTargetTable() : stageDataSetRequest.getTargetTable();
	}

	public String getTargetSchema()
	{
		return stageDataRequest != null ? stageDataRequest.getTargetSchema() : stageDataSetRequest.getTargetSchema();
	}

	public boolean hasConnections()
	{
		return stageDataRequest != null ? (stageDataRequest.getConnections() != null) : (stageDataSetRequest.getConnections() != null);
	}

	public String getSource()
	{
		return stageDataRequest != null ? stageDataRequest.getSource() : stageDataSetRequest.getSource();
	}

	public String getSourceFile()
	{
		return stageDataRequest != null ? stageDataRequest.getSourceFile() : stageDataSetRequest.getSourceFile();
	}

	public String getSourceFilePath()
	{
		return stageDataSetRequest != null ? stageDataSetRequest.getSourceFilePath() : null;
	}

	public boolean hasMode()
	{
		return stageDataRequest != null ? (stageDataRequest.getMode() != null) : (stageDataSetRequest.getMode() != null);
	}

	public boolean hasModes()
	{
		if (stageDataRequest != null)
		{
			List<String> modes = stageDataRequest.getModes();
			return modes != null && modes.size() != 0;
		}
		else
		{
			List<String> modes = stageDataSetRequest.getModes();
			return modes != null && modes.size() != 0;
		}
	}

	public boolean hasConnectionId()
	{
		return stageDataRequest != null ? (stageDataRequest.getConnectionId() != null) : (stageDataSetRequest.getConnectionId() != null);
	}

	@JsonProperty("column-list")
	public Set<String> getColumnList()
	{
		return stageDataRequest != null ? stageDataRequest.getColumnList() : stageDataSetRequest.getColumnList();
	}

	public StageDataRequest.ColumnListMode getColumnListMode()
	{
		if (stageDataRequest != null)
		{
			return stageDataRequest.getColumnListMode();
		}
		else
		{
			StageDataSetRequest.ColumnListMode columnListMode = stageDataSetRequest.getColumnListMode();
			return columnListMode != null ? StageDataRequest.ColumnListMode.valueOf(columnListMode.name()) : null;
		}
	}
}
