package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Table;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.RequestedFmlNotFoundException;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeRef;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface DatabaseRuntimeSupport
{
	File getSourceDataSetDirectoryForCurrentTest();
	File getSourceDataDirectoryForCurrentTest();
	File getSourceScriptsDirectoryForCurrentTest();

	File getSourceDataSetDirectory(ETLTestPackage package_);
	File getSourceDataDirectory(ETLTestPackage package_);
	File getSourceScriptsDirectory(ETLTestPackage package_);

	File getSourceDataSet(ETLTestPackage package_, String dataSetName, String use);
	File getSourceDataSetForCurrentTest(String dataSetName, String use);

	File getSourceData(ETLTestPackage package_, String dataSetName, String use, DataFileSchema.format_type type);
	File getSourceDataForCurrentTest(String dataSetName, String use, DataFileSchema.format_type type);

	File getSourceScript(ETLTestPackage package_, String scriptName, String use);
	File getSourceScriptForCurrentTest(String scriptName, String use);

	File getCachedMetaData(DatabaseConnection conn);
	File getGeneratedSourceDirectory(DatabaseConnection conn, String mode);
	File getGeneratedData(DatabaseConnection conn, String mode, String dataSetName);

	DataFileSchema createRelationalMetaInfo(DatabaseConnection dbConn, String schema, String name);
	DataFileSchema createRelationalMetaInfo(String connectionId, String schema, String name);

	FileRuntimeSupport.DataFileSchemaQueryResult getRelationalMetaInfo(DatabaseConnection dbConn, String schema, String name, List<String> masterName, ETLTestValueObject etlObj, ReferenceFileTypeRef.ref_type type) throws RequestedFmlNotFoundException, TestExecutionError;
	FileRuntimeSupport.DataFileSchemaQueryResult getRelationalMetaInfo(String connId, String schema, String name, List<String> masterName, ETLTestValueObject etlObj, ReferenceFileTypeRef.ref_type type) throws RequestedFmlNotFoundException, TestExecutionError;

	void persistDataFileSchema(ETLTestOperation op, DataFileSchema schema) throws IOException;

	String createSchemaKey(Table table);

	SQLAggregator resolveSQLRef(String text) throws IOException, TestExecutionError;

	SQLAggregator resolveDDLRef(DDLSourceRef ddlRef, DatabaseConnection databaseConnection) throws IOException, TestExecutionError;

	/**
	 * Tell whether the configuration for this database has changed since the last run
	 * @param databaseConnection
	 * @return
	 */
	int getDatabaseConfigurationState(DatabaseConnection databaseConnection);

	/**
	 * Tells whether the DDL for the database has changed
	 * @param databaseConnection
	 * @return
	 */
	int getDatabaseDDLState(DatabaseConnection databaseConnection) throws TestExecutionError;

	DataFileSchema generateReferenceFileForTable(Table table);

	boolean hasDatabase(DatabaseConnection connection, VariableContext context);

	Database getDatabase(DatabaseConnection connection, VariableContext context);

	void refreshDatabaseMetadata(
			DatabaseConnection conn,
			String mode,
			DatabaseImplementation impl,
			VariableContext context,
			MetaDataContext databaseMetaContext,
			JDBCClient jdbcClient
	) throws Exception;

	void loadTable(
			Table table,
			String mode,
			DatabaseImplementation impl
	) throws Exception;

	void loadTable(
			Table table,
			String mode,
			DatabaseImplementation impl,
			ETLTestOperation op
	) throws Exception;

	void loadTable(
			Table table,
			String mode,
			DatabaseImplementation impl,
			ETLTestOperation op,
			boolean loadReferenceType
	) throws Exception;
}
