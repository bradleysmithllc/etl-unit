package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;

import javax.inject.Inject;
import java.io.IOException;
import java.sql.*;

public interface JDBCClient
{
	void worksFor(DatabaseImplementation dbi);

	interface ConnectionClient
	{
		void connection(Connection conn, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception;
	}

	void useConnection(DatabaseConnection connection, String mode, ConnectionClient client) throws TestExecutionError;
	void useConnection(DatabaseConnection connection, String mode, ConnectionClient client, DatabaseImplementation.database_role id) throws TestExecutionError;

	interface StatementClient
	{
		void connection(Connection conn, Statement st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception;
	}

	void useStatement(DatabaseConnection connection, String mode, StatementClient client) throws TestExecutionError;
	void useStatement(DatabaseConnection connection, String mode, StatementClient client, DatabaseImplementation.database_role id) throws TestExecutionError;

	interface PreparedStatementClient
	{
		String prepareText();
		void connection(Connection conn, PreparedStatement st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception;
	}

	void usePreparedStatement(DatabaseConnection connection, String mode, PreparedStatementClient client) throws TestExecutionError;
	void usePreparedStatement(DatabaseConnection connection, String mode, PreparedStatementClient client, DatabaseImplementation.database_role id) throws TestExecutionError;

	interface CallableStatementClient
	{
		String callText();
		void connection(Connection conn, CallableStatement st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception;
	}

	void useCallableStatement(DatabaseConnection connection, String mode, CallableStatementClient client) throws TestExecutionError;
	void useCallableStatement(DatabaseConnection connection, String mode, CallableStatementClient client, DatabaseImplementation.database_role id) throws TestExecutionError;

	interface ResultSetClient
	{
		void beginSet(Connection conn, ResultSet st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception;
		void next(Connection conn, ResultSet st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception;
		void endSet(Connection conn, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception;
	}

	void useResultSet(DatabaseConnection connection, String mode, ResultSetClient client, String query) throws TestExecutionError;
	void useResultSet(DatabaseConnection connection, String mode, ResultSetClient client, String query, DatabaseImplementation.database_role id) throws TestExecutionError;

	void useResultSetScript(DatabaseConnection connection, String mode, ResultSetClient client, String script) throws TestExecutionError, IOException;
	void useResultSetScript(DatabaseConnection connection, String mode, ResultSetClient client, String script, DatabaseImplementation.database_role id) throws TestExecutionError, IOException;

	void useResultSetScriptResource(DatabaseConnection connection, String mode, ResultSetClient client, String script) throws TestExecutionError, IOException;
	void useResultSetScriptResource(DatabaseConnection connection, String mode, ResultSetClient client, String script, DatabaseImplementation.database_role id) throws TestExecutionError, IOException;
}
