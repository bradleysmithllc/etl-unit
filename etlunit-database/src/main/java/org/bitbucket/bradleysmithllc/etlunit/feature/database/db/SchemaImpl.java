package org.bitbucket.bradleysmithllc.etlunit.feature.database.db;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import java.io.IOException;
import java.util.*;

public class SchemaImpl extends AbstractPhysicalLogicalNamed implements Schema
{
	private final Catalog catalog;
	private boolean virtual = false;

	private final List<Table> tableList = new ArrayList<Table>();
	private final Map<String, Table> tableMap = new HashMap<String, Table>();

	public SchemaImpl(Catalog catalog, String name) {
		super(name);
		this.catalog = catalog;
	}

	public SchemaImpl(Catalog catalog) {
		super(null);
		this.catalog = catalog;
	}

	@Override
	public Catalog getCatalog() {
		return catalog;
	}

	@Override
	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}

	@Override
	public boolean isVirtual() {
		return virtual;
	}

	@Override
	public List<Table> getTables() {
		return Collections.unmodifiableList(tableList);
	}

	@Override
	public Map<String, Table> getTableMap() {
		return Collections.unmodifiableMap(tableMap);
	}

	@Override
	public Table getTable(String name) {
		return tableMap.get(EtlUnitStringUtils.getSafeNullableLowercase(name));
	}

	public void addTable(Table table)
	{
		tableList.add(table);
		tableMap.put(table.getLogicalName(), table);

		touch();
	}

	private void touch() {
		if (catalog != null)
		{
			catalog.touch();
		}
	}

	public String toString()
	{
		return (catalog == null ? "" : (catalog.toString() + ".")) + (getLogicalName() == null ? "[DEFAULT]" : getLogicalName());
	}

	@Override
	public void toMoreJson(JsonWriter writer) throws IOException
	{
		writer.name("virtual").value(virtual);
		writer.name("tables").beginArray();

		for (Table table : tableList)
		{
			writer.beginObject();
			table.toJson(writer);
			writer.endObject();
		}

		writer.endArray();
	}

	@Override
	public void fromMoreJson(JsonNode node)
	{
		virtual = node.get("virtual").asBoolean();
		JsonNode tables = node.get("tables");

		if (tables != null && !tables.isNull())
		{
			ArrayNode nodeList = (ArrayNode) tables;

			Iterator<JsonNode> it = nodeList.iterator();

			while (it.hasNext())
			{
				JsonNode tnode = it.next();

				TableImpl timpl = new TableImpl(catalog, this);

				timpl.fromJson(tnode);

				addTable(timpl);
			}
		}
	}
}
