package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.stream.JsonWriter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database._assert.assert_data.AssertDataHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database._assert.assert_data.AssertDataRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database._assert.assert_data_set.AssertDataSetHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database._assert.assert_data_set.AssertDataSetRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql.ExecuteSqlHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql.ExecuteSqlRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql_procedure.ExecuteSqlProcedureHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql_procedure.ExecuteSqlProcedureRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql_script.ExecuteSqlScriptHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.execute.execute_sql_script.ExecuteSqlScriptRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.extract.ExtractHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.extract.ExtractRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.migrate.MigrateHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.migrate.MigrateRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data.StageDataHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data.StageDataRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data_set.StageDataSetHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.stage.stage_data_set.StageDataSetRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.truncate.TruncateHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.truncate.TruncateRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.util.DimensionalHashMap;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.util.DimensionalMap;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.util.StageRequestContainer;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.RequestedFmlNotFoundException;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeRef;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.bitbucket.bradleysmithllc.etlunit.util.jdbc.JdbcObjectRead;
import org.bitbucket.bradleysmithllc.json.validator.ResourceNotFoundException;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.math.BigInteger;
import java.sql.*;
import java.util.*;

public class DatabaseClassListener extends NullClassListener implements ExtractHandler, StageDataHandler, StageDataSetHandler, TruncateHandler, MigrateHandler, AssertDataHandler, AssertDataSetHandler, ExecuteSqlHandler, ExecuteSqlProcedureHandler, ExecuteSqlScriptHandler {
	public static final String DEFAULT_MODE = null;
	public static final String LAST_EXTRACT_FILE_SCHEMA = "lastExtractFileSchema";
	public static final String LAST_EXTRACT_TABLE = "LAST_EXTRACT_TABLE";

	private final DatabaseFeatureModule parent;
	private final DatabaseConfiguration databaseConfiguration;
	private DatabaseRuntimeSupport databaseRuntimeSupport;
	private RuntimeSupport runtimeSupport;
	private FileRuntimeSupport fileRuntimeSupport;
	protected Log applicationLog;
	private JDBCClient jdbcClient;

	@Inject
	@Named("database.prepareDatabase")
	private RuntimeOption prepareDatabase = null;

	@Inject
	@Named("database.refreshStageData")
	private RuntimeOption refreshStageData = null;

	@Inject
	@Named("database.forceInitializeDatabase")
	private RuntimeOption forceInitializeDatabase = null;

	private DataFileManager dataFileManager;

	private final DbAssertExtender assertHandler;
	private final DbOperationsExecutor executeHandler;

	public DatabaseClassListener(DatabaseFeatureModule parent, DatabaseConfiguration databaseConfiguration, DbAssertExtender object1, DbOperationsExecutor object) {
		assertHandler = object1;
		executeHandler = object;
		this.parent = parent;
		this.databaseConfiguration = databaseConfiguration;
	}

	@Inject
	public void receiveDataFileManager(DataFileManager manager) {
		dataFileManager = manager;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport manager) {
		runtimeSupport = manager;
	}

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport manager) {
		fileRuntimeSupport = manager;
	}

	@Inject
	public void setJdbcClient(JDBCClient client) {
		jdbcClient = client;
	}

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log) {
		applicationLog = log;
	}

	@Inject
	public void setDatabaseRuntimeSupport(DatabaseRuntimeSupport databaseRuntimeSupport) {
		this.databaseRuntimeSupport = databaseRuntimeSupport;
	}

	@Override
	public action_code process(ETLTestOperation etlTestOperation, ETLTestValueObject etlTestValueObject, VariableContext context, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return action_code.defer;
	}

	@Override
	public void begin(final ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// if this method, or class is annotated with @NoDatabase, then no further work is done
		if (mt.getAnnotations("@NoDatabase").size() > 0 || mt.getTestClass().getAnnotations("@NoDatabase").size() > 0) {
			// do nothing
			return;
		}

		// create a view of the annotations which has them in priority order descending
		List<ETLTestAnnotation> annoList = getDatabaseAnnotationsInPriorityOrder(mt);

		// before starting a method, initialize any database support necessary.
		// get a list of databases to prepare
		DimensionalMap<DatabaseConnection, String, Object> dbMap = new DimensionalHashMap<DatabaseConnection, String, Object>();

		if (databaseConfiguration == null) {
			throw new TestExecutionError("Database feature lacks configuration", DatabaseConstants.ERR_MISSING_DATABASE_CONFIGURATION);
		}

		Iterator<ETLTestAnnotation> it = annoList.iterator();

		while (it.hasNext()) {
			ETLTestAnnotation anno = it.next();

			ETLTestValueObject annoet = anno.getValue();
			String id = annoet.query("id").getValueAsString();

			List<String> modeList = readModes(annoet);

			DatabaseConnection dc = databaseConfiguration.getDatabaseConnection(id);

			if (dc == null) {
				throw new TestExecutionError("Undeclared connection id " + id, DatabaseConstants.ERR_INVALID_DATABASE_CONNECTION);
			}

			// clear previous modes for this connection
			dbMap.remove(dc);

			if (modeList.size() > 0) {
				for (String mode : modeList) {
					dbMap.put(dc, mode, "");
				}
			} else {
				dbMap.put(dc, DEFAULT_MODE, "");
			}
		}

		if (dbMap.size() == 0) {
			// nothing to do at all
			return;
		}

		// prepare the databases
		Iterator<DatabaseConnection> set = dbMap.keySet().iterator();

		while (set.hasNext()) {
			final DatabaseConnection databaseConnection = set.next();

			// look up the implementation and request a prepare
			DatabaseImplementation impl = parent.getImplementation(databaseConnection.getId());

			if (impl == null) {
				throw new TestExecutionError("Connection id " + databaseConnection.getId() + " does not have a valid implementation: " + databaseConnection.getImplementationId(), DatabaseConstants.ERR_MISSING_IMPLEMENTATION);
			}

			Map<String, Object> lmap = dbMap.get(databaseConnection);

			for (final String mode : lmap.keySet()) {
				// validate the modes
				// check that the configuration allows for any mode (indicated by no mode specified on the connection)
				File databaseMetadataTarget = databaseRuntimeSupport.getCachedMetaData(databaseConnection);

				// check this every time.  It will only create the database the first time
				databaseRuntimeSupport.getDatabase(databaseConnection, context);

				if (prepareDatabase.isEnabled()) {
					if (!context.getTopLevelScope().hasVariableBeenDeclared(databaseConnection.getId() + "/" + mode)) {
						// initialize - this happens at most once in every run
						initializeDatabase(mt, databaseConnection, impl, mode, databaseMetadataTarget, context);
					}

					// prepare the database for test - this could happen for every test run
					purgeDatabaseForTest(mt, databaseConnection, impl, mode, context);
				} else if (parent.databaseOutOfDate(mode, databaseConnection)) {
					throw new TestExecutionError("Prepare database option is false, but the database schema has changed.", DatabaseConstants.ERR_INVALID_SCHEMA_STATE);
				}
			}
		}

		/* store database information in the variable context for use by other features
		 * These should look like this to the clients:
		 * database: {
		 * 	connections: {
		 * 		connectionName: {
		 * 			target: databaseConnection
		 * 		}
		 *	 }
		 *	}
		 *	Where the database connection is a facade which only responds to the configured
		 *	mode (target in the example).  The 'modeless' connection uses the
		 *	reserved name 'default'
		 */

		// create the database container
		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder();
		builder = builder.object().key("connections").object();

		// iterate through all required connections and modes
		for (DatabaseConnection key : dbMap.keySet()) {
			Map<String, Object> lmap = dbMap.get(key);

			// create the base connection object
			// this will contain modes
			builder = builder.key(key.getId()).object();
			// run through all modes, checking for the special null mode which gets converted
			// to 'default'

			for (String mode : lmap.keySet()) {
				if (mode == null) {
					builder.key("default").object();
				} else {
					builder.key(mode).object();
				}

				// add properties
				parent.propogateProperties(builder, key, mode);
				builder.endObject();
			}

			builder.endObject();
		}

		builder.endObject().endObject();

		context.declareAndSetValue("database", builder.toObject());
	}

	private List<ETLTestAnnotation> getDatabaseAnnotationsInPriorityOrder(ETLTestMethod mt) {
		List<ETLTestAnnotation> annoList = new ArrayList<ETLTestAnnotation>(mt.getTestClass().getAnnotations("@Database"));
		annoList.addAll(mt.getAnnotations("@Database"));

		return annoList;
	}

	private void initializeDatabase(
			final ETLTestMethod mt,
			final DatabaseConnection databaseConnection,
			DatabaseImplementation impl,
			final String mode,
			File databaseMetadataTarget,
			final VariableContext variableContext
	) throws TestExecutionError {
		// conditions which affect 'dirtiness' of the database.
		// (0) Force initialize database is enabled.
		// (1) All database instances (for this connection id) are dirty if the configuration for the db has changed.
		// (2) All database instances (..) are dirty if the ref DDL has changed.
		// (3) This database is dirty if the database implementation says it is.
		// Rules (0), (1) and (2) mean all databases of this id are dirty for this run, until initialized.
		// Rule (3) only applies to this database until initialized.

		// (0)
		boolean forceEnabled = forceInitializeDatabase.isEnabled();

		// (1)
		int configState = databaseRuntimeSupport.getDatabaseConfigurationState(databaseConnection);

		// (2)
		int ddlState = databaseRuntimeSupport.getDatabaseDDLState(databaseConnection);

		// check for a flag file matching these states
		File stateFlag = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory("database")).subdir(impl.getImplementationId()).subdir(mode == null ? "[default]" : mode).mkdirs().name(
				Integer.toHexString(configState) + "-" + Integer.toHexString(ddlState) + runtimeSupport.getExecutorId() + ".tag"
		).file();

		boolean tagExists = stateFlag.exists();

		applicationLog.info("Database tag file " + tagExists);

		if (
				forceEnabled
						||
						!tagExists
						||
						// (3)
						impl.getDatabaseState(databaseConnection, mode, databaseRuntimeSupport.getDatabase(databaseConnection, variableContext)) == DatabaseImplementation.database_state.fail
			//!databaseMetadataTarget.exists() ||
			//parent.databaseOutOfDate(mode, databaseConnection) ||
		) {
			applicationLog.info("Initializing database: " + databaseConnection.getId() + (mode == null ? "" : ("." + mode)));

			DenyAllOperationsOperationRequest request = new DenyAllOperationsOperationRequest() {
				@Override
				public DatabaseImplementation.InitializeRequest getInitializeRequest() throws UnsupportedOperationException {
					return new DatabaseImplementation.InitializeRequest() {
						public String getMode() {
							return mode;
						}

						public DatabaseConnection getConnection() {
							return databaseConnection;
						}

						@Override
						public Database getDatabase() {
							return databaseRuntimeSupport.getDatabase(getConnection(), variableContext);
						}

						public ETLTestMethod getTestMethod() {
							return mt;
						}

						public File resolveSchemaReference(String ddlRef) {
							throw new UnsupportedOperationException();
						}
					};
				}
			};

			// we have to issue createDatabase, createSchema, complete, dropConstraints and materializeViews
			impl.processOperation(DatabaseImplementation.operation.createDatabase, request);
			impl.processOperation(DatabaseImplementation.operation.preCreateSchema, request);

			// create the schema
			jdbcClient.useStatement(databaseConnection, mode, new JDBCClient.StatementClient() {
				public void connection(Connection conn, Statement st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception {
					for (String ref : databaseConnection.getSchemaScripts()) {
						applicationLog.info("Processing schema source: " + ref);

						DDLSourceRef sourceRef = new DDLSourceRef(databaseConnection.getImplementationId(), databaseConnection.getId(), ref);

						SQLAggregator SQLAggregator = databaseRuntimeSupport.resolveDDLRef(sourceRef, databaseConnection);

						int count = 0;

						SQLAggregator.Aggregator agg = SQLAggregator.getStatementAggregator();

						while (agg.hasNext()) {
							count++;

							SQLAggregator.FileRef fRef = agg.next();

							String sql = fRef.getLine();

							try {
								st.execute(sql);
							} catch (Throwable thr) {
								throw new TestExecutionError("Error processing DDL reference: " + fRef.describe(), DatabaseConstants.ERR_DDL_APPLICATION_FAILURE, thr);
							}
						}

						applicationLog.info("Processed [" + count + "] statements");
					}
				}
			});

			impl.processOperation(DatabaseImplementation.operation.postCreateSchema, request);
			impl.processOperation(DatabaseImplementation.operation.completeDatabaseInitialization, request);

			impl.processOperation(DatabaseImplementation.operation.materializeViews, request);

			try {
				// this is here to force the database meta data to be refreshed after schema creation
				// otherwise the process operation calls after this one will not have a database loaded
				// in the connection.  This call may not be cached since the schema was just created
				databaseRuntimeSupport.refreshDatabaseMetadata(databaseConnection, mode, impl, variableContext, parent.getDatabaseMetaContext(), jdbcClient);
			} catch (Exception e) {
				throw new TestExecutionError("Failure reading database meta data [" + databaseConnection.getId() + "]", DatabaseConstants.ERR_PROCESS_DATABASE_META_DATA, e);
			}

			impl.processOperation(DatabaseImplementation.operation.dropConstraints, request);
			parent.updateDatabaseFlag(mode, databaseConnection);

			try {
				FileUtils.touch(stateFlag);
			} catch (IOException e) {
				throw new TestExecutionError("Error updating database state", TestConstants.ERR_IO_ERROR, e);
			}
		}

		variableContext.getTopLevelScope().declareAndSetStringValue(databaseConnection.getId() + "/" + mode, "");
	}

	private void purgeDatabaseForTest(
			final ETLTestMethod mt,
			final DatabaseConnection databaseConnection,
			DatabaseImplementation impl,
			final String mode,
			VariableContext context
	) throws TestExecutionError {
		applicationLog.info("Purging database for test: " + databaseConnection.getId() + (mode == null ? "" : ("." + mode)));

		final Database database = databaseRuntimeSupport.getDatabase(databaseConnection, context);

		DenyAllOperationsOperationRequest denyAllOperationsOperationRequest = new DenyAllOperationsOperationRequest() {
			@Override
			public DatabaseImplementation.DatabaseRequest getDatabaseRequest() throws UnsupportedOperationException {
				return new DatabaseImplementation.DatabaseRequest() {
					public ETLTestMethod getTestMethod() {
						return mt;
					}

					public String getMode() {
						return mode;
					}

					public DatabaseConnection getConnection() {
						return databaseConnection;
					}

					@Override
					public Database getDatabase() {
						return database;
					}
				};
			}
		};

		impl.processOperation(DatabaseImplementation.operation.preCleanTables, denyAllOperationsOperationRequest);

		int tblCounter = 0;

		for (Catalog catalog : database.getCatalogs()) {
			for (Schema schema : catalog.getSchemas()) {
				for (Table table : schema.getTables()) {
					try {
						if (table.getType() != Table.type.sql) {
							tblCounter++;
							impl.purgeTableForTest(databaseConnection, mode, table);
						}
					} catch (Exception e) {
						throw new TestExecutionError("Failure preparing table [" + table.toString() + "] for test", DatabaseConstants.ERR_PREPARE_TABLES, e);
					}
				}
			}
		}

		applicationLog.info("Purged [" + tblCounter + "] tables");
		impl.processOperation(DatabaseImplementation.operation.resetIdentities, denyAllOperationsOperationRequest);

		impl.processOperation(DatabaseImplementation.operation.postCleanTables, denyAllOperationsOperationRequest);
	}

	@Override
	public void begin(ETLTestClass cl, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		applicationLog.info(refreshStageData.isEnabled() ? "Refreshing stage data" : "Not refreshing stage data");
		applicationLog.info(prepareDatabase.isEnabled() ? "Preparing database" : "Not preparing database");
		applicationLog.info(forceInitializeDatabase.isEnabled() ? "Forcing database" : "Not forcing database");
	}

	@Override
	public action_code assertDataSet(AssertDataSetRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return assertHandler.assertDataSet(
				request, testMethod, testOperation, valueObject, variableContext, executionContext
		);
	}

	enum source_mode {table, sql}

	public action_code extract(ExtractRequest operation, final ETLTestMethod mt, final ETLTestOperation op, final ETLTestValueObject obj, final VariableContext context, ExecutionContext econtext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		final String source;
		source_mode smode = source_mode.table;

		String sqlScript = operation.getSqlScript();
		String extractOperationSql = operation.getSql();

		final List<String> searchList = new ArrayList<String>();

		if (sqlScript != null) {
			searchList.add(sqlScript);
			// load the sql into the source variable
			File sqlFile = databaseRuntimeSupport.getSourceScriptForCurrentTest(sqlScript, "sql-extract-script");

			if (!sqlFile.exists()) {
				throw new TestExecutionError("Sql script not found: " + sqlFile, DatabaseConstants.ERR_MISSING_SQL_SCRIPT);
			}

			// process using the context
			try {
				source = VelocityUtil.writeTemplate(IOUtils.readFileToString(sqlFile), context);
			} catch (Exception e) {
				throw new TestExecutionError("", e);
			}
			smode = source_mode.sql;
		} else if (extractOperationSql != null) {
			smode = source_mode.sql;
			try {
				source = VelocityUtil.writeTemplate(extractOperationSql, context);
			} catch (Exception e) {
				throw new TestExecutionError("", e);
			}
		} else {
			source = operation.getSourceTable();
			searchList.add(source);
		}

		//final String sourceTable = source;
		final String target = operation.getTarget();
		if (target != null) {
			searchList.add(target);
		}

		final source_mode sourcemode = smode;

		ExtractRequest.ColumnListMode lm = operation.getColumnListMode();

		final List<String> list = new ArrayList<String>(operation.getColumnList());

		if (list.size() != 0 && lm == null) {
			// exclude is the default
			lm = ExtractRequest.ColumnListMode.EXCLUDE;
		} else if (lm != null && list.size() == 0) {
			throw new TestExecutionError("Extract operation does not allow column-list-mode without column-list");
		}

		final ExtractRequest.ColumnListMode listMode = lm;

		// look for optional data
		final String targetFile = operation.getTargetFile();

		ConnectionMode connMode = getConnection(parent, obj, context, op.getTestMethod()).get(0);

		final DatabaseConnection conn = connMode.conn;

		final String mode = connMode.modes.get(0);

		// now send it off to the implementation
		final DatabaseImplementation impl = getDatabaseImplementation(parent, conn);

		String aschema = operation.getSourceSchema();

		boolean restrictedSchema = impl.restrictToOwnedSchema(conn);
		if (aschema == null) {
			aschema = restrictedSchema ? null : getDefaultSchemaForTable(conn, impl, op, mode);
		}

		final File targetBase;

		if (targetFile != null) {
			File f = new File(targetFile);

			if (f.isAbsolute()) {
				targetBase = f;
			} else {
				targetBase = databaseRuntimeSupport.getGeneratedData(conn, connMode.getPrettyMode(), targetFile);
			}
		} else {
			targetBase = null;
		}

		// get the relational table definition
		try {
			Database meta = databaseRuntimeSupport.getDatabase(conn, context);

			Catalog catalog = meta.getCatalog(null);

			final String key;

			Table ltable;

			String sqlId = null;

			if (sourcemode == source_mode.sql) {
				sqlId = sqlScript;

				if (extractOperationSql != null) {
					// in this case, use the identity of the operation to uniquely identify the meta format
					sqlId = op.getQualifiedName();
				}

				sqlId = sqlId.replace(".", "-");

				key = (conn.getId() + "__SQL__" + sqlId).toUpperCase();
				Catalog sqlCatalog = catalog.getDatabase().getCatalog("__SQL__");
				Schema catalogSchema = sqlCatalog.getSchema(null);
				ltable = catalogSchema.getTable(sqlId);

				if (ltable == null) {
					TableImpl tbl = new TableImpl(sqlCatalog, catalogSchema, sqlId, Table.type.sql);
					tbl.setSqlScriptSource(source);
					catalogSchema.addTable(tbl);
					ltable = tbl;
				} else {
					// always update the sql for the source query so if the script is updated in the project that will be recorded
					ltable.setSqlScriptSource(source);
				}
			} else {
				// check for a view or a plain table.
				Pair<String, String> tbl = ImmutablePair.of(aschema, source);

				key = (conn.getId() + "." + impl.strings(tbl)).toLowerCase();

				Table table1 = catalog.getSchema(restrictedSchema ? "user" : tbl.getLeft()).getTable(tbl.getRight());

				if (!restrictedSchema && aschema == null && table1 == null) {
					// try to find the table without a schema
					table1 = getSchemaLessTableData(conn, mode, impl, meta, tbl.getRight());
				}

				ltable = table1;
			}

			// validate meta data exists
			if (ltable == null) {
				throw new TestExecutionError("Database does not export table " + key + " in it's meta data", DatabaseConstants.ERR_MISSING_SCHEMA_TABLE);
			}

			// make sure the meta-data information is available
			databaseRuntimeSupport.loadTable(ltable, mode, impl, op);

			final Table table = ltable;
			final String useSchema = aschema;

			final File dataTarget;

			if (targetFile == null) {
				dataTarget = databaseRuntimeSupport.getSourceDataForCurrentTest(target, "extract-target", DataFileSchema.format_type.delimited);
			} else {
				dataTarget = targetBase;
			}

			// write to temporary file then burst it over in a synchronized block
			final File tempFile = runtimeSupport.createAnonymousTempFile();

			applicationLog.info("Extracting source " + (sourcemode == source_mode.table ? "table" : "query") + "[" + key + "]" + " to local dataset [" + dataTarget.getAbsolutePath() + "]");

			// export the data
			jdbcClient.useStatement(conn, mode, new JDBCClient.StatementClient() {
				public void connection(Connection conn, Statement st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception {
					DataFileSchema fileSchema = loadSchema(table, searchList, obj, ReferenceFileTypeRef.ref_type.remote);

					applicationLog.info("Resolved data file schema: " + fileSchema.getId());

					applicationLog.info("Setting context variable for schema name");
					context.declareAndSetValue("extract.schema." + target, new ETLTestValueObjectImpl(fileSchema.getId()));
					context.declareAndSetValue("extract.schema.last", new ETLTestValueObjectImpl(fileSchema.getId()));

					Table useTable = table;

					/**
					 * The subview for sql queries must happen after extraction - not before.  I.E., we can't modify the source query only the results.
					 */
					if (listMode != null && table.getType() != Table.type.sql) {
						useTable = table.createSubView(list, listMode);
						switch (listMode) {
							case INCLUDE:
								fileSchema = fileSchema.createSubViewIncludingColumns(list, fileSchema.getId() + "-subIncluding-" + op.getQualifiedName());
								break;
							case EXCLUDE:
								fileSchema = fileSchema.createSubViewExcludingColumns(list, fileSchema.getId() + "-subExcluding-" + op.getQualifiedName());
								break;
						}

						// store the generated file schema for reference
						fileRuntimeSupport.persistGeneratedDataFileSchemaForCurrentTest(op, fileSchema);
					}

					// assert that the extract schema has at least one column remaining
					if (fileSchema.getLogicalColumns().size() == 0) {
						throw new TestExecutionError("Schema must have at least one column for extraction", DatabaseConstants.ERR_INVALID_SCHEMA_STATE);
					}

					// store the schema regardless of whether we had a sql assertion or not
					context.declareAndSetValue(LAST_EXTRACT_FILE_SCHEMA, new ETLTestValueObjectImpl(fileSchema));
					context.declareAndSetValue(LAST_EXTRACT_TABLE, new ETLTestValueObjectImpl(table));

					applicationLog.info("Using temp data file [" + tempFile + "]");
					DataFile ff = dataFileManager.loadDataFile(tempFile, fileSchema);

					String variableName = "extract.columns." + target;
					applicationLog.info("Setting context variable for " + variableName);
					context.declareAndSetValue(variableName, new ETLTestValueObjectImpl(useTable.getTableColumnNames()));

					final String sqlSelect;

					if (ObjectUtils.firstNotNull(operation.getUseLiveView(), Boolean.FALSE).booleanValue()) {
						// check for view source
						sqlSelect = useTable.createSQLSelectUsingAlternateTableName(impl, impl.mapToRealViewName(useTable.getSchema().getLogicalName(), useTable.getLogicalName()));
					}
					else {
						sqlSelect = useTable.createSQLSelect(impl);
					}

					if (sqlSelect == null) {
						throw new TestExecutionError("Null sql select created by table: " + useTable.getQualifiedName());
					}

					applicationLog.info("Using sql select for object(" + key + " in [" + connection.getId() + "." + mode + "]):\n" + sqlSelect);

					long startTime = System.currentTimeMillis();
					int rowsRead = 0;

					DataFileWriter ffwriter = ff.writer(new CopyOptionsBuilder().missingColumnPolicy(DataFileManager.CopyOptions.missing_column_policy.use_null).options());

					try {
						ResultSet rs = st.executeQuery(sqlSelect);

						ResultSetMetaData rsmd = rs.getMetaData();

						int cols = rsmd.getColumnCount();

						try {
							Map<String, Object> rowData = new HashMap<String, Object>();

							while (rs.next()) {
								rowsRead++;

								for (int i = 1; i <= cols; i++) {
									String colName = rsmd.getColumnLabel(i);

									Object data = JdbcObjectRead.readObject(rs, i, rsmd.getColumnType(i));

									rowData.put(colName.toLowerCase(), data);
								}

								ffwriter.addRow(rowData);
							}
						} finally {
							rs.close();
						}
					} finally {
						ffwriter.close();
					}

					// in the case of a sql query, we must create a view of the file to pass on
					if (table.getType() == Table.type.sql && listMode != null) {
						applicationLog.info("Copying sql extract to match column list");

						DataFileSchema targetFileSchema = fileSchema;

						switch (listMode) {
							case INCLUDE:
								targetFileSchema = fileSchema.createSubViewIncludingColumns(list, fileSchema.getId() + "-sql-subIncluding-" + op.getQualifiedName());
								break;
							case EXCLUDE:
								targetFileSchema = fileSchema.createSubViewExcludingColumns(list, fileSchema.getId() + "-sql-subExcluding-" + op.getQualifiedName());
								break;
						}

						// assert that the extract schema has at least one column remaining
						if (targetFileSchema.getLogicalColumns().size() == 0) {
							throw new TestExecutionError("Schema must have at least one column for extraction", DatabaseConstants.ERR_INVALID_SCHEMA_STATE);
						}

						fileRuntimeSupport.persistGeneratedDataFileSchemaForCurrentTest(op, fileSchema);

						context.setValue(LAST_EXTRACT_FILE_SCHEMA, new ETLTestValueObjectImpl(targetFileSchema));

						// create a temporary data file
						File ddsql = databaseRuntimeSupport.getGeneratedData(connection, mode, target + ".sqldat");

						// copy the output to the generated temp
						FileUtils.copyFile(tempFile, ddsql);

						// create a data file off the temp sql output with the full schema
						DataFile ffSql = dataFileManager.loadDataFile(ddsql, fileSchema);

						// create a data file pointing to the target with the final column list
						DataFile ffExt = dataFileManager.loadDataFile(tempFile, targetFileSchema);

						dataFileManager.copyDataFile(ffSql, ffExt);
					}

					long endTime = System.currentTimeMillis();
					applicationLog.debug("Selected [" + rowsRead + "] rows in [" + (endTime - startTime) + "] ms.");
				}
			});

			// try to make this as small as possible
			synchronized (this) {
				FileUtils.copyFile(tempFile, dataTarget);
			}
		} catch (TestExecutionError e) {
			throw e;
		} catch (Exception e) {
			throw new TestExecutionError("", e);
		}

		return action_code.handled;
	}

	private DataFileSchema loadSchema(Table table, List<String> names, ETLTestValueObject obj, ReferenceFileTypeRef.ref_type type) throws TestExecutionError {
		// look for the meta info in the project
		try {
			// look it up in the project
			FileRuntimeSupport.DataFileSchemaQueryResult dresult = databaseRuntimeSupport.getRelationalMetaInfo(table.getCatalog().getDatabase().getDatabaseConnection(), table.getSchema().getLogicalName(), table.getLogicalName(), names, obj, type);

			DataFileSchema dataFileSchema;

			if (dresult.getFmlSearchType() == FileRuntimeSupport.fmlSearchType.notFound) {
				dataFileSchema = table.getDataFileSchema();
			} else {
				dataFileSchema = dresult.getDataFileSchema();
				return dataFileSchema;
			}

			// before returning, narrow if needed
			ETLTestValueObject query = obj.query("reference-file-type");
			if (query != null) {
				ReferenceFileTypeRef ref = ReferenceFileTypeRef.refFromRequest(query, ReferenceFileTypeRef.ref_type.local);

				// but only if version, id and classifier are NOT specified . . .
				if (!ref.hasVersion() && !ref.hasClassifier() && !ref.hasId()) {
					return fileRuntimeSupport.narrowIfRequired(dataFileSchema, ref);
				}
			}

			return dataFileSchema;
		} catch (RequestedFmlNotFoundException exc) {
			throw new TestExecutionError("Error resolving Flat File Schema", DatabaseConstants.ERR_REFERENCE_FILE_NOT_FOUND, exc);
		} catch (IllegalArgumentException exc) {
			throw new TestExecutionError(exc.toString(), DatabaseConstants.ERR_REFERENCE_FILE_NOT_FOUND);
		}
	}

	private void loadTable1(final Table table, String mode, final DatabaseImplementation impl, final ETLTestOperation op) throws Exception {
		if (table.getTableColumns().size() == 0) {
			applicationLog.info("Updating database meta data for target [" + table.getCatalog().getDatabase().getDatabaseConnection().getId() + "." + mode + "." + table.getSchema().getLogicalName() + "." + table.getLogicalName() + "]");

			jdbcClient.useStatement(table.getCatalog().getDatabase().getDatabaseConnection(), mode, new JDBCClient.StatementClient() {
				public void connection(Connection conn, Statement st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception {
					String sql;

					if (table.getType() != Table.type.sql) {
						sql = "SELECT * FROM " + impl.escapeQualifiedIdentifier(table) + " WHERE 1 = 2";
					} else {
						sql = table.createSQLSelect(impl);
					}

					DatabaseMetaData meta = conn.getMetaData();

					// issue a select for the table and check the meta data for column info
					applicationLog.info("Using meta query string: " + sql);
					ResultSet rs = st.executeQuery(sql);

					try {
						ResultSetMetaData rsmd = rs.getMetaData();

						for (int col = 1; col <= rsmd.getColumnCount(); col++) {
							String columnName = rsmd.getColumnLabel(col);

							int columnType = rsmd.getColumnType(col);

							int nullable = rsmd.isNullable(col);
							boolean autoIncrement = rsmd.isAutoIncrement(col);
							int precision = rsmd.getPrecision(col);
							int scale = rsmd.getScale(col);

							// special case for length == 0 which some RDBMSs mean unconstrained
							if (precision == 0) {
								precision = -1;
							}

							// another special case, this time for scale.  Some RDBMSs give weird negative numbers (-127 for Oracle)
							if (scale < 0) {
								scale = -1;
							}

							TableColumn tc = new TableColumnImpl(
									table,
									columnName,
									columnType,
									nullable == ResultSetMetaData.columnNullable,
									precision,
									scale,
									autoIncrement
							);

							table.addColumn(tc);
						}
					} finally {
						rs.close();
					}

					// add all primary key columns (only the first is of interest) - if this is not a sql query
					if (table.getType() != Table.type.sql) {
						rs = meta.getPrimaryKeys(null, table.getSchema().getPhysicalName(), table.getPhysicalName());

						try {
							String firstPk = null;

							while (rs.next()) {
								String columnName = rs.getString(4);
								String pkName = rs.getString(6);

								if (firstPk == null) {
									firstPk = pkName;
								} else if (!firstPk.equals(pkName)) {
									// we don't care about the rest
									break;
								}

								table.addPrimaryKeyColumn(table.getColumn(columnName));
							}
						} finally {
							rs.close();
						}
					}
				}
			});
		}

		// look for the meta info in the project
		try {
			FileRuntimeSupport.DataFileSchemaQueryResult dresult = databaseRuntimeSupport.getRelationalMetaInfo(table.getCatalog().getDatabase().getDatabaseConnection(), table.getSchema().getLogicalName(), table.getLogicalName(), null, null, ReferenceFileTypeRef.ref_type.remote);

			if (dresult.getFmlSearchType() == FileRuntimeSupport.fmlSearchType.notFound) {
				// lazy, but I don't want to refactor try / catch now
				throw new ResourceNotFoundException();
			}

			DataFileSchema ffs = dresult.getDataFileSchema();

			applicationLog.info("Reading meta data from reference file: " + ffs.getId());

			// load the file and override the order by columns
			table.resetOrderColumns();
			for (String name : ffs.getOrderColumnNames()) {
				table.addOrderKeyColumn(table.getColumn(name));
			}

			table.resetPrimaryKeyColumns();
			for (String name : ffs.getKeyColumnNames()) {
				table.addPrimaryKeyColumn(table.getColumn(name));
			}

			table.setDataFileSchema(ffs);
		} catch (ResourceNotFoundException exc) {
			applicationLog.info("Could not find reference file - creating boilerplate: " + table.getQualifiedName());

			DataFileSchema ffs = databaseRuntimeSupport.generateReferenceFileForTable(table);

			table.setDataFileSchema(ffs);

			// load into Jackson and pretty-print
			databaseRuntimeSupport.persistDataFileSchema(op, ffs);
		}
	}

	public static BigInteger getMaxDigits(int lod) {
		if (lod < 0) {
			throw new IllegalArgumentException("Digits may not be negative");
		}

		BigInteger bi = new BigInteger("10");

		BigInteger res = bi.pow(lod);

		return res.subtract(new BigInteger("1"));
	}

	private Table getSchemaLessTableData(DatabaseConnection conn, String mode, final DatabaseImplementation impl, final Database relationalSet, final String tableName) throws Exception {
		Table table = relationalSet.getCatalog(null).getSchema(null).getTable(tableName);

		if (table != null) {
			return table;
		}

		jdbcClient.useConnection(conn, mode, new JDBCClient.ConnectionClient() {
			public void connection(Connection conn, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception {
				DatabaseMetaData meta = conn.getMetaData();

				ResultSet rs = meta.getTables(null, null, tableName, null);

				try {
					if (rs.next()) {
						//String tableCatalog = rs.getString(1); // Ditto
						String tableSchema = rs.getString(2);
						String tableName = rs.getString(3);
						String tableType = rs.getString(4);

						if (tableType != null) {
							String tableUpper = tableType.toUpperCase();

							Table.type type = impl.translateTableType(tableUpper);

							if (type != null) {
								Catalog databaseCatalog = relationalSet.getCatalog(null);
								Schema sch = databaseCatalog.getSchema(tableSchema);

								TableImpl tbl = (TableImpl) sch.getTable(tableName);

								if (tbl == null) {
									tbl = new TableImpl(databaseCatalog, sch, tableName, type);

									((SchemaImpl) sch).addTable(tbl);

									String key1 = (connection.getId() + "." + tableSchema + '.' + tableName).toUpperCase();

									applicationLog.debug(key1);
								}

								relationalSet.getCatalog(null).getSchema(null).addTable(tbl);
							}
						}
					} else {
						throw new TestExecutionError("Could not locate table without schema: " + tableName, DatabaseConstants.ERR_SCHEMALESS_TABLE_NOT_FOUND);
					}

					if (rs.next()) {
						throw new TestExecutionError("Table reference ambiguous without schema: " + tableName, DatabaseConstants.ERR_AMBIGUOUS_TABLE);
					}
				} finally {
					rs.close();
				}
			}
		});


		return relationalSet.getCatalog(null).getSchema(null).getTable(tableName);
	}

	@Override
	public action_code stageDataSet(
			final StageDataSetRequest request,
			ETLTestMethod testMethod,
			final ETLTestOperation testOperation,
			final ETLTestValueObject valueObject,
			final VariableContext variableContext,
			ExecutionContext executionContext
	) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return stage(new StageRequestContainer(request), testOperation, valueObject, variableContext, executionContext);
		// first task is to open the data set and look for the correct id
		/*
		File ds = databaseRuntimeSupport.getSourceDataSetForCurrentTest(request.getDataSetName(), "stage-data-set");

		try {
			FileReader fr = new FileReader(ds);

			try {
				ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager, fr);

				// locate the data set to load
				String dataSetId = request.getDataSetId();
					DataSet dataSetToLoadLkp;

					try
					{
						dataSetToLoadLkp = rdsc.locate(dataSetId);
					}
					catch(IllegalArgumentException iae)
					{
						// missing data set id
						throw new TestExecutionError("Data set does not contain id[" + dataSetId + "]", DatabaseConstants.ERR_MISSING_DATA_SET_ID, iae);
					}

					final DataSet dataSetToLoad = dataSetToLoadLkp;

					// do a left-merge (request object >> data set properties) of properties
					ETLTestValueObject copy = valueObject.copy();

					Boolean ignoreDataSetProperties = request.getIgnoreDataSetProperties();
					if (ignoreDataSetProperties == null || !ignoreDataSetProperties.booleanValue())
					{
						// do not ignore properties
						copy = copy.merge(
								ETLTestParser.loadObject(dataSetToLoad.getProperties().toString()),
								ETLTestValueObject.merge_type.left_merge,
								ETLTestValueObject.merge_policy.recursive
						);
					}

					final ETLTestValueObject newRequestObject = copy;

					stageDataImpl(testOperation, newRequestObject, variableContext, executionContext, new StageDataHelper() {
						private File dataFile;

						@Override
						public File getDataFile(DataFileSchema.format_type format) throws TestExecutionError {
							if (dataFile != null) {
								return dataFile;
							}

							// extract the body of the data set to a temporary file and pass it on
							dataFile = runtimeSupport.createAnonymousTempFile();

							try {
								Writer output = new BufferedWriter(new FileWriter(dataFile));

								try {
									org.apache.commons.io.IOUtils.copy(dataSetToLoad.read(), output);
								} finally {
									output.close();
								}
							} catch (IOException e) {
								throw new TestExecutionError("Error copying source data from data set", DatabaseConstants.ERR_IO_ERR, e);
							}

							return dataFile;
						}

						@Override
						public List<String> getSchemaSearchNames() {
							List<String> names = new ArrayList<String>();

							String setName = request.getDataSetName();

							String setId = request.getDataSetId();

							if (setId != null) {
								names.add(setName + "_" + setId);
							} else {
								names.add(setName);
							}

							return names;
						}

						@Override
						public void refreshStageData(File targetDataFile) throws TestExecutionError {
							if (true) throw new UnsupportedOperationException();

							try {
								if (request.getSourceFile() != null) {
									// copy back to the source file
									FileUtils.copyFile(
											targetDataFile,
											new File(request.getSourceFile())
									);
								} else {
									// utilize the schema used by the extract operation
									DataFileSchema schema = (DataFileSchema) variableContext.getValue(LAST_EXTRACT_FILE_SCHEMA).getValueAsPojo();

									File sourceData = databaseRuntimeSupport.getSourceData(testOperation.getTestClass().getPackage(), request.getSource(), "stage-source", schema.getFormatType());

									FileUtils.copyFile(
											targetDataFile,
											sourceData
									);
								}
							} catch (IOException exc) {
								throw new TestExecutionError("Could not refresh stage data: " + exc);
							}
						}

						@Override
						public String getTargetTable() {
							return newRequestObject.query("target-table").getValueAsString();
						}

						@Override
						public boolean hasMode() {
							return newRequestObject.query("mode") != null;
						}

						@Override
						public boolean hasConnectionId() {
							return newRequestObject.query("connection-id") != null;
						}

						@Override
						public boolean hasModes() {
							return newRequestObject.query("modes") != null;
						}

						@Override
						public boolean hasConnections() {
							return request.getConnections() != null;
						}

						@Override
						public String getTargetSchema() {
							return request.getTargetSchema();
						}

						@Override
						public List<String> getColumnList() {
							return new ArrayList(request.getColumnList());
						}

						@Override
						public StageDataRequest.ColumnListMode getColumnListMode() {
							StageDataSetRequest.ColumnListMode columnListMode = request.getColumnListMode();

							if (columnListMode != null) {
								return StageDataRequest.ColumnListMode.valueOf(columnListMode.name());
							}

							return null;
						}
					});
			} finally {
				fr.close();
			}
		} catch (ParseException e) {
			throw new TestExecutionError("Could not process data set properties: " + ds.getName(), DatabaseConstants.ERR_IO_ERR, e);
		} catch (FileNotFoundException exc) {
			throw new TestExecutionError("Could not open data set: " + ds.getName(), DatabaseConstants.ERR_MISSING_DATA_SET, exc);
		} catch (IOException exc) {
			throw new TestExecutionError("Could not process data set: " + ds.getName(), DatabaseConstants.ERR_IO_ERR, exc);
		}
		 */
	}

	@Override
	public action_code stageData(final StageDataRequest request, ETLTestMethod testMethod, final ETLTestOperation testOperation, ETLTestValueObject valueObject, final VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return stage(new StageRequestContainer(request), testOperation, valueObject, variableContext, executionContext);
	}

	private action_code stage(final StageRequestContainer request, final ETLTestOperation testOperation, ETLTestValueObject valueObject, final VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return stageDataImpl(testOperation, valueObject, variableContext, executionContext, new StageDataHelper() {
			@Override
			public File getDataFile(DataFileSchema.format_type format) throws TestExecutionError {
				File dataFile = null;

				if (request.getSourceFile() != null) {
					dataFile = new File(request.getSourceFile());
				} else if (request.getSourceFilePath() != null) {
					dataFile = new File(request.getSourceFilePath());
				} else {
					dataFile = databaseRuntimeSupport.getSourceData(testOperation.getTestClass().getPackage(), request.getSource(), "stage-source", format);
				}

				// get the actual data file
				if (!dataFile.exists()) {
					throw new TestExecutionError(dataFile.getAbsolutePath(), DatabaseConstants.ERR_MISSING_DATA_FILE);
				}

				return dataFile;
			}

			@Override
			public List<String> getSchemaSearchNames() {
				List<String> names = new ArrayList<String>();

				String source = request.getSource();
				if (source != null) {
					names.add(source);
				}

				source = request.getSourceFile();
				if (source != null) {
					names.add(source);
				}

				return names;
			}

			@Override
			public void refreshStageData(File targetDataFile) throws TestExecutionError {
				try {
					if (request.getSourceFile() != null) {
						// copy back to the source file
						FileUtils.copyFile(
								targetDataFile,
								new File(request.getSourceFile())
						);
					} else {
						// utilize the schema used by the extract operation
						DataFileSchema schema = (DataFileSchema) variableContext.getValue(LAST_EXTRACT_FILE_SCHEMA).getValueAsPojo();

						File sourceData = databaseRuntimeSupport.getSourceData(testOperation.getTestClass().getPackage(), request.getSource(), "stage-source", schema.getFormatType());

						FileUtils.copyFile(
								targetDataFile,
								sourceData
						);
					}
				} catch (IOException exc) {
					throw new TestExecutionError("Could not refresh stage data: " + exc);
				}
			}

			@Override
			public String getTargetTable() {
				return request.getTargetTable();
			}

			@Override
			public boolean hasMode() {
				return request.hasMode();
			}

			@Override
			public boolean hasConnectionId() {
				return request.hasConnectionId();
			}

			@Override
			public boolean hasModes() {
				return request.hasModes();
			}

			@Override
			public boolean hasConnections() {
				return request.hasConnections();
			}

			@Override
			public String getTargetSchema() {
				return request.getTargetSchema();
			}

			@Override
			public List<String> getColumnList() {
				return new ArrayList(request.getColumnList());
			}

			@Override
			public StageDataRequest.ColumnListMode getColumnListMode() {
				return request.getColumnListMode();
			}
		});
	}

	private interface StageDataHelper {
		File getDataFile(DataFileSchema.format_type format) throws TestExecutionError;

		List<String> getSchemaSearchNames();

		void refreshStageData(File targetDataFile) throws TestExecutionError;

		String getTargetTable();

		boolean hasMode();

		boolean hasConnectionId();

		boolean hasModes();

		boolean hasConnections();

		String getTargetSchema();

		List<String> getColumnList();

		StageDataRequest.ColumnListMode getColumnListMode();
	}

	public action_code stageDataImpl(
			ETLTestOperation testOperation,
			ETLTestValueObject valueObject,
			VariableContext variableContext,
			ExecutionContext executionContext,
			StageDataHelper helper
	) throws TestAssertionFailure, TestExecutionError, TestWarning {
		String targetTable = helper.getTargetTable();

		// check the runtime option for refresh data.  If so, convert this call into an extract operation
		if (refreshStageData.isEnabled()) {
			ETLTestValueObject copy = valueObject.copy();

			ETLTestValueObjectBuilder cbuilder = new ETLTestValueObjectBuilder(copy);

			//!!
			cbuilder = cbuilder.key("source-table").value(valueObject.query("target-table"));
			//!!
			cbuilder.removeKey("target-table");

			// here this needs to utilize defaults for schema
			if (valueObject.query("target-schema") != null) {
				cbuilder = cbuilder.key("source-schema").value(valueObject.query("target-schema"));
				cbuilder.removeKey("target-schema");
			}

			//!!
			// extract to a temporary data file and let the helper decide what to do with it
			File targetDataFile = runtimeSupport.createAnonymousTempFile("data", "txt");
			cbuilder = cbuilder.key("target-file").value(targetDataFile.getAbsolutePath());
			//!!

			cbuilder.removeKey("source");

			// check for multiple modes specified.  If present, replace with a single mode reference
			if (valueObject.query("modes") != null) {
				cbuilder.removeKey("modes").key("mode").value(valueObject.query("modes").getValueAsList().get(0));
			}

			// check for connections declaration
			ETLTestValueObject connections = valueObject.query("connections");
			if (connections != null) {
				// get the connection id and mode
				Map<String, ETLTestValueObject> connMap = connections.getValueAsMap();
				// grab the first id
				Map.Entry<String, ETLTestValueObject> entry = connMap.entrySet().iterator().next();

				String mode = null;

				List<String> modeList = readModes(entry.getValue());

				if (modeList.size() > 0) {
					mode = modeList.get(0);
				}

				cbuilder.key("connection-id").value(entry.getKey());

				if (mode != null) {
					cbuilder.key("mode").value(mode);
				}

				cbuilder.removeKey("connections");
			}

			copy = cbuilder.toObject();

			ETLTestOperation extract_op = testOperation.createSibling("extract", copy);

			executionContext.process(extract_op, variableContext);

			// send the file back to the helper
			helper.refreshStageData(targetDataFile);
			return action_code.handled;
		}

		// look for connections and connection-id or {mode, modes}
		boolean hasConnections = helper.hasConnections();
		boolean hasMode = helper.hasMode();
		boolean hasModes = helper.hasModes();
		boolean hasConnectionId = helper.hasConnectionId();
		if (hasConnections &&
				(hasMode || hasModes || hasConnectionId)
		) {
			throw new TestExecutionError("Attribute 'connections' may not be used in conjunction with connection-id, mode or modes");
		}

		// look for optional data
		List<ConnectionMode> connectionModes = getConnection(parent, valueObject, variableContext, testOperation.getTestMethod());

		for (final ConnectionMode connectionMode : connectionModes) {
			// now send it off to the implementation
			final DatabaseImplementation impl = getDatabaseImplementation(parent, connectionMode.conn);

			// get the relational table definition
			List<String> modeList = connectionMode.modes;

			if (modeList == null) {
				modeList = new ArrayList<String>();
				modeList.add(null);
			}

			for (final String thisMode : modeList) {
				final String schema;

				String targetSchema = helper.getTargetSchema();
				if (targetSchema != null) {
					schema = targetSchema;
				} else {
					schema = impl.restrictToOwnedSchema(connectionMode.conn) ? null : getDefaultSchemaForTable(connectionMode.conn, impl, testOperation, thisMode);
				}

				final String key = (connectionMode.connectionId + "." + (schema != null ? (schema.toLowerCase() + ".") : "") + targetTable).toLowerCase();

				try {
					Database meta = databaseRuntimeSupport.getDatabase(connectionMode.conn, variableContext);

					Table t1 = meta.getCatalog(null).getSchema(impl.restrictToOwnedSchema(connectionMode.conn) ? "user" : schema).getTable(targetTable);

					if (schema == null && t1 == null) {
						// try to find the table without a schema
						t1 = getSchemaLessTableData(connectionMode.conn, thisMode, impl, meta, targetTable);
					}

					// load column and key data if not loaded already
					if (t1 == null) {
						throw new TestExecutionError("Database does not export table " + key + " in it's meta data", DatabaseConstants.ERR_MISSING_SCHEMA_TABLE);
					}

					// make sure it is populated
					databaseRuntimeSupport.loadTable(t1, thisMode, impl, testOperation);

					final Table table = t1;

					// make a copy of the data file from the resolved schema to the relational one - even if they are the same - so that
					// default values may be applied BEFORE the upload to the relational table
					final DataFileSchema relSchema = table.getDataFileSchema();

					DataFileSchema fileSchema = loadSchema(table, helper.getSchemaSearchNames(), valueObject, ReferenceFileTypeRef.ref_type.local);

					final File dataFile = helper.getDataFile(fileSchema.getFormatType());
					applicationLog.info("Loading (" + key + ") data set (" + dataFile.getName() + ")");

					DataFile ff = dataFileManager.loadDataFile(dataFile, fileSchema);

					// create copy
					final DataFile ffCopy = dataFileManager.loadDataFile(runtimeSupport.createAnonymousTempFile(), relSchema);

					dataFileManager.copyDataFile(ff, ffCopy, new CopyOptionsBuilder().missingColumnPolicy(DataFileManager.CopyOptions.missing_column_policy.use_default).options());

					StageDataRequest.ColumnListMode lm = helper.getColumnListMode();

					final List<String> list = new ArrayList(helper.getColumnList());

					if (list.size() != 0 && lm == null) {
						// exclude is the default
					} else if (lm != null && list.size() == 0) {
						throw new TestExecutionError("Stage operation does not allow column-list-mode without column-list", DatabaseConstants.ERR_BAD_COLUMN_LIST);
					}

					if (lm == null) {
						lm = StageDataRequest.ColumnListMode.EXCLUDE;
					}

					// start with all columns, then narrow down, then remove all read-only columns
					List<String> columnsNames = table.getTableColumnNames();

					try {
						switch (lm) {
							case INCLUDE:
								// remove all columns that don't match
								// case-insensitive
								columnsNames = DataFileSchemaImpl.intersectLists(columnsNames, list, true);
								break;

							case EXCLUDE:
								// exclude every non-matching name - check for bogus names
								// Update - this is case-insensitive.  Who'd've thought?
								columnsNames = DataFileSchemaImpl.intersectLists(columnsNames, list, false);
								break;
						}
					} catch (IllegalArgumentException exc) {
						throw new TestExecutionError(exc.getMessage(), DatabaseConstants.ERR_BAD_COLUMN_LIST);
					}

					// remove read-only columns
					List<String> readOnlyColumnNames = relSchema.getReadOnlyColumnNames();
					columnsNames.removeAll(readOnlyColumnNames);

					if (columnsNames.size() == 0) {
						throw new TestExecutionError("Schema must have at least one column for staging", DatabaseConstants.ERR_BAD_COLUMN_LIST);
					}

					final Table table2 = columnsNames.size() == 0 ? table : table.createSubView(columnsNames, ExtractRequest.ColumnListMode.INCLUDE);

					// open a jdbc connection and run away with it
					jdbcClient.usePreparedStatement(connectionMode.conn, thisMode, new JDBCClient.PreparedStatementClient() {
						public String prepareText() {
							String sqlInsert = table2.createSQLInsert(impl);
							applicationLog.info("Using sql insert for object(" + key + ") in  [" + connectionMode.connectionId + "." + thisMode + "] - data set (" + dataFile.getName() + "):\n" + sqlInsert);

							return sqlInsert;
						}

						public void connection(Connection conn, PreparedStatement pst, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception {
							impl.prepareConnectionForInsert(conn, table, connection, mode);

							List<TableColumn> columns = table2.getTableColumns();

							DataFileReader fdata = ffCopy.reader();

							try {
								Iterator<DataFileReader.FileRow> dataIt = fdata.iterator();

								int rowNum = 0;
								int rowsInserted = 0;
								long startTime = System.currentTimeMillis();

								while (dataIt.hasNext()) {
									rowNum++;
									rowsInserted++;

									Map<String, Object> rowData = dataIt.next().getData();

									for (int i = 0; i < columns.size(); i++) {
										TableColumn col = columns.get(i);
										// ignore read only columns
										if (relSchema.getColumn(col.getLogicalName()).isReadOnly()) {
											continue;
										}

										int columnType = col.getType();

										Object val = rowData.get(col.getLogicalName());

										// TMI!!
										//applicationLog.info("Processing value: " + val);

										int parameterIndex = i + 1;
										if (val == null) {
											// set this to null in the insert
											pst.setNull(parameterIndex, columnType);
										} else {
											try {
												// Let the converters handle this one.  In the case of a CLOB or BLOB, however, the
												// Values must be handled appropriately
												if (columnType == Types.CLOB) {
													Clob myClob = (Clob) val;

													StringWriter stw = new StringWriter();

													Reader characterStream = myClob.getCharacterStream();
													try {
														org.apache.commons.io.IOUtils.copy(characterStream, stw);
													} finally {
														characterStream.close();
													}

													pst.setObject(parameterIndex, stw.toString());

												} else if (columnType == Types.BLOB) {

												} else {
													pst.setObject(parameterIndex, val);
												}
											} catch (Throwable thr) {
												throw new TestExecutionError("Error loading row[" + rowNum + "], columnIndex=[" + i + "], column=[" + col + "], colType[" + columnType + "], value='" + val + "'", DatabaseConstants.ERR_DATA_TYPE_CONVERSION, thr);
											}
										}
									}

									// add as a batch
									pst.addBatch();

									if (rowNum >= 5000) {
										pst.executeBatch();
										rowNum = 0;
									}
								}

								if (rowNum != 0) {
									pst.executeBatch();
								}

								long endTime = System.currentTimeMillis();
								applicationLog.info("Inserted [" + rowsInserted + "] rows in [" + (endTime - startTime) + "] ms.");
							} finally {
								fdata.dispose();
							}
						}
					});
				} catch (DataFileMismatchException e) {
					throw new TestExecutionError("", DatabaseConstants.ERR_DATA_FILE_SCHEMA_MISMATCH, e);
				} catch (TestExecutionError e) {
					throw e;
				} catch (Exception e) {
					throw new TestExecutionError("", e);
				}
			}
		}

		return action_code.handled;
	}

	/**
	 * Called when there is no schema attribute on the operation (target or source).
	 * @param databaseConnection
	 * @param impl
	 * @param testOperation
	 * @param mode
	 * @return
	 */
	private String getDefaultSchemaForTable(DatabaseConnection databaseConnection, DatabaseImplementation impl, ETLTestOperation testOperation, String mode) {
		// Schema search order:
		//   a - Annotation declared on the method
		//   b - Annotation declared on the class
		//   c - database connection configuration
		//   d - implementation default
		//   z - none.

		List<ETLTestAnnotation> annotationList = getDatabaseAnnotationsInPriorityOrder(testOperation.getTestMethod());

		for (ETLTestAnnotation annotation : annotationList) {
			ETLTestValueObject value = annotation.getValue();

			// verify that the connection id is correct
			String connectionId = value.query("id").getValueAsString();

			if (connectionId.equals(databaseConnection.getId())) {
				// this might be the one
				ETLTestValueObject declaredModes = value.query("modes");
				if ((declaredModes == null && mode == null) || (declaredModes != null && declaredModes.getValueAsListOfStrings().contains(mode))) {
					// this is the annotation that declared this connection.  Look for a default schema
					ETLTestValueObject query = value.query("default-schema");
					if (query != null) {
						return query.getValueAsString();
					}
				}
			}
		}

		// look into the database connection for a default before using the driver default
		String implementationDefault = impl.getDefaultSchema(databaseConnection, mode);
		String databaseConnectionDefault = databaseConnection.defaultSchema();
		return ObjectUtils.firstNotNull(databaseConnectionDefault, implementationDefault);
	}

	static DatabaseImplementation getDatabaseImplementation(DatabaseFeatureModule parent, DatabaseConnection conn) throws TestExecutionError {
		DatabaseImplementation impl = parent.getImplementation(conn.getId());

		if (impl == null) {
			throw new TestExecutionError("Database implementation id " + conn.getId() + " doesn't exist", DatabaseConstants.ERR_MISSING_DATABASE_IMPLEMENTATION);
		}
		return impl;
	}

	static void validateMode(DatabaseConnection conn, String mode, VariableContext context) throws TestExecutionError {
		// validate that this connection was prepared for this test
		if (!context.hasVariableBeenDeclared("database")) {
			throw new TestExecutionError("Database support not available for this test", DatabaseConstants.ERR_MISSING_DATABASE_SUPPORT);
		}

		ETLTestValueObject database = context.getValue("database");

		ETLTestValueObject connQuery = database.query("connections." + conn.getId() + "." + (mode == null ? "default" : mode));

		if (connQuery == null) {
			throw new TestExecutionError("Database not prepared for connection id " + conn.getId() + ", mode " + mode, DatabaseConstants.ERR_DATABASE_MODE_NOT_PREPARED);
		}
	}

	static String getDefaultMode(DatabaseConnection conn, ETLTestMethod method) throws TestExecutionError {
		String mode = null;

		// validate the mode
		// the search order for modes is like this:
		// 1 - Look at method annotations.  If this connection is present, use the first mode declared
		// 2 - Look at class annotations.  If this connection is present, use the first mode declared
		List<ETLTestAnnotation> annot = new ArrayList<ETLTestAnnotation>();

		annot.addAll(method.getAnnotations("@Database"));
		annot.addAll(method.getTestClass().getAnnotations("@Database"));

		for (ETLTestAnnotation ann : annot) {
			ETLTestValueObject annValue = ann.getValue();
			if (annValue.query("id").getValueAsString().equals(conn.getId())) {
				List<String> modeList = readModes(annValue);

				if (modeList.size() > 0) {
					mode = modeList.get(0);

					break;
				}
			}
		}

		return mode;
	}

	@Override
	public action_code assertData(AssertDataRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return assertHandler.assertData(request, mt, testOperation, valueObject, variableContext, executionContext);
	}

	@Override
	public action_code executeSql(ExecuteSqlRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return executeHandler.executeSql(request, mt, testOperation, valueObject, variableContext, executionContext);
	}

	@Override
	public action_code executeSqlProcedure(ExecuteSqlProcedureRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return executeHandler.executeSqlProcedure(request, mt, testOperation, valueObject, variableContext, executionContext);
	}

	@Override
	public action_code executeSqlScript(ExecuteSqlScriptRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return executeHandler.executeSqlScript(request, mt, testOperation, valueObject, variableContext, executionContext);
	}

	static class ConnectionMode {
		String connectionId;
		DatabaseConnection conn;
		List<String> modes = new ArrayList<String>();

		String getMode() {
			if (modes != null) {
				return modes.get(0);
			}

			return null;
		}

		String getPrettyMode() {
			String mode = getMode();

			return mode != null ? mode : "default";
		}
	}

	static List<ConnectionMode> getConnection(DatabaseFeatureModule parent, ETLTestValueObject etlTestValueObject, VariableContext context, ETLTestMethod method) throws TestExecutionError {
		List<ConnectionMode> connModes = new ArrayList<ConnectionMode>();

		DatabaseConfiguration databaseConfiguration = parent.getDatabaseConfiguration();

		// query for 'connections'
		ETLTestValueObject connectionsObj = etlTestValueObject.query("connections");

		if (connectionsObj != null) {
			Map<String, ETLTestValueObject> valueAsMap = connectionsObj.getValueAsMap();
			for (Map.Entry<String, ETLTestValueObject> connectionObj : valueAsMap.entrySet()) {
				ConnectionMode connectionMode = new ConnectionMode();

				connectionMode.connectionId = connectionObj.getKey();

				// we either have a 'mode' here, or 'modes'
				readModes(connectionMode, connectionObj.getValue());

				connModes.add(connectionMode);
			}
		} else {
			ConnectionMode connMode = new ConnectionMode();
			connModes.add(connMode);

			// determine the connection id and mode
			ETLTestValueObject connectionIdObj = etlTestValueObject.query("connection-id");

			final DatabaseConnection conn;

			if (connectionIdObj != null) {
				connMode.connectionId = connectionIdObj.getValueAsString();
			} else {
				// go through the search order:
				// 1 - Annotation on the method
				// 2 - Annotation on the class
				// 3 - The default connection
				List<ETLTestAnnotation> annot = method.getAnnotations("@Database");

				if (annot.size() != 0) {
					// this is the one - use the first
					connMode.connectionId = annot.get(0).getValue().query("id").getValueAsString();
				} else {
					// move on to the class annotations
					annot = method.getTestClass().getAnnotations("@Database");

					if (annot.size() != 0) {
						// this is the one - use the first
						connMode.connectionId = annot.get(0).getValue().query("id").getValueAsString();
					} else {
						// move on to the default
						// bail
						if (!databaseConfiguration.hasDefaultConnection()) {
							throw new TestExecutionError("Connection not specified and no default has been set", DatabaseConstants.ERR_NO_CONNECTION_SPECIFIED);
						}

						connMode.connectionId = databaseConfiguration.getDefaultConnection().getId();
					}
				}
			}

			// now check for the mode(s)
			readModes(connMode, etlTestValueObject);
		}

		// now that that's done, look up the info in the configuration for validation and set the default mode if not specified
		Iterator<ConnectionMode> it = connModes.iterator();

		while (it.hasNext()) {
			ConnectionMode cMode = it.next();

			cMode.conn = databaseConfiguration.getDatabaseConnection(cMode.connectionId);

			if (cMode.conn == null) {
				throw new TestExecutionError("Connection " + cMode.conn + " is not valid", DatabaseConstants.ERR_INVALID_DATABASE_CONNECTION);
			}

			if (cMode.modes.size() == 0) {
				// resolve the modes for the connection
				cMode.modes.add(getDefaultMode(cMode.conn, method));
			}

			for (String mode : cMode.modes) {
				validateMode(cMode.conn, mode, context);
			}
		}

		return connModes;
	}

	private static void readModes(ConnectionMode connMode, ETLTestValueObject etlTestValueObject) throws TestExecutionError {
		List<String> modeList = readModes(etlTestValueObject);

		for (String mode : modeList) {
			connMode.modes.add(mode);
		}
	}

	public Feature getFeature() {
		return parent;
	}

	public action_code truncate(TruncateRequest operation, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		final String target = operation.getTarget();

		final ConnectionMode connMode = getConnection(parent, obj, context, op.getTestMethod()).get(0);

		DatabaseConnection conn = connMode.conn;

		String mode = connMode.getMode();

		DatabaseImplementation impl = getDatabaseImplementation(parent, conn);

		final String schema = operation.getSchema() == null ? impl.getDefaultSchema(conn, mode) : operation.getSchema();

		// get the connection and blast the sucker
		jdbcClient.useStatement(conn, mode, new JDBCClient.StatementClient() {
			public void connection(Connection conn, Statement st, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception {
				String sql = "DELETE FROM " + (schema != null ? (schema + ".") : "") + target;
				applicationLog.info("Using truncate sql [" + connection.getId() + "." + connMode.getPrettyMode() + "]:\n" + sql);
				st.executeUpdate(sql);
			}
		});

		return action_code.handled;
	}

	class MigrateInfo {
		String sourceValue;
		String targetValue;
	}

	class MigrateInfos {
		MigrateInfo tableInfo;
		MigrateInfo connectionInfo;
		MigrateInfo schemaInfo;
		MigrateInfo modeInfo;
	}

	public action_code migrate(MigrateRequest operation, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		MigrateInfos mi = readMigrateExclusiveOptions(operation);

		// this process is three steps:
		// 1 - run truncate on the target
		// 2 - run extract on the source
		// 3 - run stage on the target
		ETLTestValueObject truncateValueObj = obj.copy();

		/* truncate has to see:
		-- target    << target-table
		-- optionally:
		--  connection-id << passes through
		--  schema
		--  mode
		-- for this purpose, we will need to convert the target-* properties to
		-- the unqualified properties truncate wants to see.
		*/

		ETLTestValueObjectBuilder builder = new ETLTestValueObjectBuilder(truncateValueObj);

		// remove all table attributes and set
		builder = builder.removeAllKeys("source-table", "target-table", "table");
		builder = builder.key("target").value(mi.tableInfo.targetValue);

		// remove all connection attributes and set
		builder = builder.removeAllKeys("source-connection-id", "target-connection-id", "connection-id");
		if (mi.connectionInfo.targetValue != null) {
			builder = builder.key("connection-id").value(mi.connectionInfo.targetValue);
		}

		// remove all schema attributes and set
		builder = builder.removeAllKeys("source-schema", "target-schema", "schema");
		if (mi.schemaInfo.targetValue != null) {
			builder = builder.key("schema").value(mi.schemaInfo.targetValue);
		}

		// remove all mode attributes and set
		builder = builder.removeAllKeys("source-mode", "target-mode", "mode");
		if (mi.modeInfo.targetValue != null) {
			builder = builder.key("mode").value(mi.modeInfo.targetValue);
		}

		// create the operation and dispatch
		ETLTestOperation truncateOp = op.createSibling("truncate", truncateValueObj);

		econtext.process(truncateOp, context);

		/* extract needs:
		-- source-table *
		-- connection-id *
		-- mode *
		-- target-file *
		-- source-schema *
		-- column-list *
		-- column-list-mode *
		 */

		ETLTestValueObject extractValueObj = obj.copy();
		builder = new ETLTestValueObjectBuilder(extractValueObj);

		// remove all table attributes and set
		builder = builder.removeAllKeys("source-table", "target-table", "table");
		builder = builder.key("source-table").value(mi.tableInfo.sourceValue);

		// remove all connection attributes and set
		builder = builder.removeAllKeys("source-connection-id", "target-connection-id", "connection-id");
		if (mi.connectionInfo.sourceValue != null) {
			builder = builder.key("connection-id").value(mi.connectionInfo.sourceValue);
		}

		// remove all schema attributes and set
		builder = builder.removeAllKeys("source-schema", "target-schema", "schema");
		if (mi.schemaInfo.sourceValue != null) {
			builder = builder.key("source-schema").value(mi.schemaInfo.sourceValue);
		}

		// remove all mode attributes and set
		builder = builder.removeAllKeys("source-mode", "target-mode", "mode");
		if (mi.modeInfo.sourceValue != null) {
			builder = builder.key("mode").value(mi.modeInfo.sourceValue);
		}

		ConnectionMode connMode = getConnection(parent, extractValueObj, context, op.getTestMethod()).get(0);

		DatabaseConnection conn = connMode.conn;

		File targetFile = databaseRuntimeSupport.getGeneratedData(conn, connMode.getPrettyMode(), op.getQualifiedName() + "_extract");

		// remove all mode attributes and set
		builder = builder.key("target-file").value(targetFile.getAbsolutePath());

		// create the operation and dispatch
		ETLTestOperation extractOp = op.createSibling("extract", extractValueObj);

		econtext.process(extractOp, context);

		/* stage needs:
		-- source-file *
		-- target-table *
		-- connection-id *
		-- target-schema *
		-- mode *
		 */
		ETLTestValueObject stageValueObj = obj.copy();
		builder = new ETLTestValueObjectBuilder(stageValueObj);

		// remove all table attributes and set
		builder = builder.removeAllKeys("source-table", "target-table", "table");
		builder = builder.key("source-file").value(targetFile.getAbsolutePath());
		builder = builder.key("target-table").value(mi.tableInfo.targetValue);

		// remove all connection attributes and set
		builder = builder.removeAllKeys("source-connection-id", "target-connection-id", "connection-id");
		if (mi.connectionInfo.targetValue != null) {
			builder = builder.key("connection-id").value(mi.connectionInfo.targetValue);
		}

		// remove all schema attributes and set
		builder = builder.removeAllKeys("source-schema", "target-schema", "schema");
		if (mi.schemaInfo.targetValue != null) {
			builder = builder.key("target-schema").value(mi.schemaInfo.targetValue);
		}

		// remove all mode attributes and set
		builder = builder.removeAllKeys("source-mode", "target-mode", "mode");
		if (mi.modeInfo.targetValue != null) {
			builder = builder.key("mode").value(mi.modeInfo.targetValue);
		}

		// create the operation and dispatch
		ETLTestOperation stageOp = op.createSibling("stage", stageValueObj);

		econtext.process(stageOp, context);

		return action_code.handled;
	}

	private MigrateInfos readMigrateExclusiveOptions(MigrateRequest operation) throws TestExecutionError {
		// table
		String table = operation.getTable();
		String sourceTable = operation.getSourceTable();
		String targetTable = operation.getTargetTable();

		// connection ID
		String conId = operation.getConnectionId();
		String sourceConId = operation.getSourceConnectionId();
		String targetConId = operation.getTargetConnectionId();

		// schema
		String schema = operation.getSchema();
		String sourceSchema = operation.getSourceSchema();
		String targetSchema = operation.getTargetSchema();

		// mode
		String mode = operation.getMode();
		String sourceMode = operation.getSourceMode();
		String targetMode = operation.getTargetMode();

		MigrateInfos mi = new MigrateInfos();

		mi.tableInfo = checkExclusive(table, sourceTable, targetTable, "table", true);

		/* Connection, schema and mode can all use defaults, so there is no problem with exclusivity */
		mi.connectionInfo = checkExclusive(conId, sourceConId, targetConId, "connection-id", false);
		mi.schemaInfo = checkExclusive(schema, sourceSchema, targetSchema, "schema", false);
		mi.modeInfo = checkExclusive(mode, sourceMode, targetMode, "mode", false);

		/* there are a couple of few error conditions here:
		-- 1 - If the source and target tables are the same, meaning
		       that the table, connection, schema and mode are all the same
		-- 2 - If no table is specified, fail
		*/

		if (mi.tableInfo.sourceValue == null || mi.tableInfo.targetValue == null) {
			throw new TestExecutionError("Source and target tables are the same", DatabaseConstants.ERR_TABLE_NOT_SPECIFIED);
		}

		if (
				compare(mi.tableInfo.sourceValue, mi.tableInfo.targetValue)
						&&
						compare(mi.connectionInfo.sourceValue, mi.connectionInfo.targetValue)
						&&
						compare(mi.schemaInfo.sourceValue, mi.schemaInfo.targetValue)
						&&
						compare(mi.modeInfo.sourceValue, mi.modeInfo.targetValue)) {
			throw new TestExecutionError("Source and target tables are the same", DatabaseConstants.ERR_SOURCE_TARGET_IDENTICAL);
		}

		return mi;
	}

	private boolean compare(String s, String t) {
		if (s == null && t == null) {
			return true;
		} else if (s == null && t != null) {
			return false;
		} else if (s != null && t == null) {
			return false;
		} else if (s == t) {
			return true;
		} else if (s.equals(t)) {
			return true;
		}

		return false;
	}

	private MigrateInfo checkExclusive(String conId, String sourceConId, String targetConId, String baseAttribute, boolean checkExclusive) throws TestExecutionError {
		MigrateInfo mt = new MigrateInfo();

		// fail if all three are specified
		if (conId != null && sourceConId != null && targetConId != null) {
			throw new TestExecutionError("There is no need to specify " + baseAttribute + ", source-" + baseAttribute + " and target-" + baseAttribute, DatabaseConstants.ERR_GENERIC_AND_SPECIFIC_OPTIONS_FAIL);
		}

		// only check if at least one of them is provided
		if (checkExclusive && (conId != null || sourceConId != null || targetConId != null)) {
			// if the generic is NOT specified, then both of the specific operands are required
			if (conId == null && (sourceConId == null || targetConId == null)) {
				throw new TestExecutionError("When " + baseAttribute + " is not specified, both source-" + baseAttribute + " and target-" + baseAttribute + " must be specified", DatabaseConstants.ERR_GENERIC_AND_SPECIFIC_OPTIONS_FAIL);
			}
		}

		// if the generic is specified and either of the specific ones is specified, fail
		if (conId != null && (sourceConId != null || targetConId != null)) {
			throw new TestExecutionError("When " + baseAttribute + " is specified, neither of source-" + baseAttribute + " or target-" + baseAttribute + " may be specified", DatabaseConstants.ERR_GENERIC_AND_SPECIFIC_OPTIONS_FAIL);
		}

		mt.sourceValue = ObjectUtils.firstNotNull(sourceConId, conId);
		mt.targetValue = ObjectUtils.firstNotNull(targetConId, conId);

		return mt;
	}

	@Override
	public void beginPackage(ETLTestPackage name, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// record the meta data files that exist in this test
		MetaDataContext dataMetaContext = parent.getDataMetaContext();
		MetaDataContext databaseMetaContext = parent.getDatabaseMetaContext();

		MetaDataPackageContext dataPackageContext = dataMetaContext.createPackageContextForCurrentTest(MetaDataPackageContext.path_type.test_source);
		try {
			MetaDataArtifact dataPackageContextArtifact = dataPackageContext.createArtifact("data", databaseRuntimeSupport.getSourceDataDirectoryForCurrentTest());
			dataPackageContextArtifact.populateAllFromDir();
		} catch (IOException e) {
			throw new TestExecutionError("Error creating meta data context", e);
		}

		dataPackageContext = databaseMetaContext.createPackageContextForCurrentTest(MetaDataPackageContext.path_type.test_source);
		try {
			MetaDataArtifact sqlPackageContextArtifact = dataPackageContext.createArtifact("sql", databaseRuntimeSupport.getSourceScriptsDirectoryForCurrentTest());
			sqlPackageContextArtifact.populateAllFromDir();
		} catch (IOException e) {
			throw new TestExecutionError("Error creating meta data context", e);
		}
	}

	@Override
	public void endTests(VariableContext context, int executorId) {
		if (databaseConfiguration != null) {
			for (DatabaseConnection dc : databaseConfiguration.getConnectionMap().values()) {
				if (databaseRuntimeSupport.hasDatabase(dc, context) && !databaseRuntimeSupport.getDatabase(dc, context).isClean()) {
					Database db = databaseRuntimeSupport.getDatabase(dc, context);

					// any database meta data which has been loaded must be persisted
					File databaseMetadataTarget = new FileBuilder(databaseRuntimeSupport.getGeneratedSourceDirectory(dc, null)).name(dc.getId() + "_metadata.json").file();

					try {
						FileWriter fw = new FileWriter(databaseMetadataTarget);

						try {
							JsonWriter jw = new JsonWriter(new BufferedWriter(fw));
							jw.beginObject();
							db.toJson(jw);
							jw.endObject();
							jw.close();
						} finally {
							fw.close();
						}
					} catch (IOException e) {
						throw new RuntimeException("Error persisting database meta data", e);
					}
				}
			}
		}
	}

	public static List<String> readModes(ETLTestValueObject obj) throws TestExecutionError {
		if (obj.query("mode") != null && obj.query("modes") != null) {
			throw new TestExecutionError("Attributes mode and modes are exclusive");
		}

		ETLTestValueObject mode1 = obj.query("mode");
		if (mode1 != null) {
			return Arrays.asList(mode1.getValueAsString());
		}

		mode1 = obj.query("modes");
		if (mode1 != null) {
			return mode1.getValueAsListOfStrings();
		}

		return Collections.emptyList();
	}
}
