package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Database;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Table;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.TableColumn;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObjectBuilder;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface DatabaseImplementation {
	enum database_role {
		/** Connect to the database as sysadmin - the user name and password supplied in the database
		 * configuration.
		 */
		sysadmin,

		/** Connect to the database as a test-provided user.
		 */
		database,

		/* Just a ping.  Executes a test connection*/
		ping
	}

	// Update the truncate command if needed
	String adjustTruncateTable(String command);
	Table.type translateTableType(String JDBCMetaTableTypeName);

	String getPassword(DatabaseConnection dc, String mode, database_role id);
	String getLoginName(DatabaseConnection dc, String mode, database_role id);

	String getPassword(DatabaseConnection dc, String mode);
	String getLoginName(DatabaseConnection dc, String mode);

	String getDatabaseName(DatabaseConnection dc, String mode);

	void setJdbcClient(JDBCClient client);

	/**
	 * Pass on any information a consumer of this specific database type will need.  E.G., instance names
	 * for SqlServer, or a service name for Oracle.  Don't create a container around the content, one has already been
	 * provided by the caller.  E.G., for db implementation id 'db' the builder will look like this:
	 *
	 * db:
	 * {
	 *   // tree pointer is here.
	 * }
	 *
	 * @param databaseConnection
	 * @param mode
	 * @param builder
	 */
	void propagateImplementationProperties(DatabaseConnection databaseConnection, String mode, ETLTestValueObjectBuilder builder);

	// Map a request for view data to the real, unmaterialized view
	Pair<String, String> mapToRealViewName(String aschema, String source);

	enum operation {
		/**
		 * (static 1) Create database logins and containers.
		 */
		createDatabase,

		/**
		 * (static 2) Create database tables, etc.
		 */
		preCreateSchema,

		/**
		 * (static 3) Create database tables, etc.
		 */
		postCreateSchema,

		/**
		 * (static 4) Complete any tasks required for the database
		 */
		completeDatabaseInitialization,

		/**
		 * (static 5) Request type to materialize views
		 */
		materializeViews,

		/**
		 * (static 6) Request type to drop constraints after creating the database.  At this point the database meta data will be
		 * available for implementations to use.
		 */
		dropConstraints,

		/**
		 * (test 1) Request to clean all database tables prior to running tests.
		 */
		preCleanTables,

		/**
		 * Reset identity columns.  Performed after pre clean before every test
		 */
		resetIdentities,

		/**
		 * (test 2) Request to clean all database tables prior to running tests.
		 */
		postCleanTables
	}

	interface BaseRequest
	{
		String getMode();
		DatabaseConnection getConnection();
		Database getDatabase();
		ETLTestMethod getTestMethod();
	}

	interface InitializeRequest extends BaseRequest
	{
		File resolveSchemaReference(String ddlRef);
	}

	interface DatabaseRequest extends BaseRequest
	{
		ETLTestMethod getTestMethod();
	}

	interface OperationRequest
	{
		InitializeRequest getInitializeRequest() throws UnsupportedOperationException;
		DatabaseRequest getDatabaseRequest() throws UnsupportedOperationException;
	}

	enum database_state
	{
		pass,
		fail
	}

	String strings(Pair<String, String> tbl);

	database_state getDatabaseState(DatabaseConnection dc, String mode, Database database);

	String getImplementationId();

	Object processOperation(operation op, OperationRequest request) throws UnsupportedOperationException;

	Connection getConnection(DatabaseConnection dc, String mode) throws TestExecutionError;
	Connection getConnection(DatabaseConnection dc, String mode, database_role identifier) throws TestExecutionError;

	void prepareConnectionForInsert(Connection connection, Table target, DatabaseConnection dc, String mode) throws Exception;

	void returnConnection(Connection conn, DatabaseConnection dc, String mode, database_role id, boolean normalState) throws TestExecutionError;
	String getDefaultSchema(DatabaseConnection dc, String mode);

	void dispose();

	/* Hides certain schemas from processing.  Return false to prevent etlunit from processing the schema */
	boolean isUserSchema(DatabaseConnection dc, String schemaName);
	boolean isTableTestVisible(DatabaseConnection dc, String mode, Table table);

	void purgeTableForTest(DatabaseConnection dc, String mode, Table table) throws Exception;

	/**
	 * Use this database's conventions to escape the identifier.  E.G., in SQL Server
	 * this might return [name], and in another database it might return "name".
	 *
	 * @param table
	 * @return
	 * @throws Exception
	 */
	String escapeQualifiedIdentifier(Table table);

	/**
	 * Use this database's conventions to escape the identifier.  E.G., in SQL Server
	 * this might return [name], and in another database it might return "name".
	 *
	 * @param name
	 * @return
	 * @throws Exception
	 */
	String escapeIdentifierIfNeeded(String name);

	/**
	 * Gets a jdbdc url for connecting to the specified database connection and mode.
	 * @param dc - connection
	 * @param mode - mode name or null for the default
	 * @param id - an implementation-specific id.  The only system-defined value is 0 which means a normal connection.
	 * @return
	 */
	String getJdbcUrl(DatabaseConnection dc, String mode, database_role id);

	/**
	 * Gets a jdbdc url for connecting to the specified database connection and mode.  Uses implementation id 0 which
	 * is defined as the default.
	 * @param dc - connection
	 * @param mode - mode name or null for the default
	 * @return
	 */
	String getJdbcUrl(DatabaseConnection dc, String mode);

	Class getJdbcDriverClass();

	/**
	 * Restrict queryies pulling meta-data from the database to the user's schema only.  If false, all tables
	 * visible in the database which are returned from the meta data query will be sent to the isTableTestVisible
	 * method.
	 * @param dc
	 * @return
	 */
	boolean restrictToOwnedSchema(DatabaseConnection dc);
}
