package org.bitbucket.bradleysmithllc.etlunit.feature.database.util;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DimensionalHashMap<T, K, L> implements DimensionalMap<T, K, L> {
	private final Map<T, Map<K, L>> store = new HashMap<T, Map<K, L>>();

	public void put(T t, K k, L l) {
		Map<K, L> t2 = store.get(t);

		if (t2 == null)
		{
			t2 = new HashMap<K, L>();
			store.put(t, t2);
		}

		t2.put(k, l);
	}

	public L get(T t, K k) {
		Map<K, L> t2 = store.get(t);

		if (t2 == null)
		{
			return null;
		}

		return t2.get(k);
	}

	public boolean containsKey(T t, K k) {
		Map<K, L> t2 = store.get(t);

		if (t2 == null)
		{
			return false;
		}

		return t2.containsKey(k);
	}

	public boolean containsKey(T t) {
		return store.containsKey(t);
	}

	public Set<T> keySet() {
		return store.keySet();
	}

	public int size() {
		return store.size();
	}

	public int size(T t) {
		Map<K, L> klMap = store.get(t);

		if (klMap == null)
		{
			return 0;
		}

		return klMap.size();
	}

	public Map<K, L> get(T t) {
		return store.get(t);
	}

	public L remove(T t, K k) {
		Map<K, L> t2 = store.get(t);

		if (t2 == null)
		{
			return null;
		}

		return t2.remove(k);
	}

	public Map<K, L> remove(T t) {
		return store.remove(t);
	}

	public void clear() {
		store.clear();
	}
}
