package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.RequestedFmlNotFoundException;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.io.file.JdbcTypeConverter;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeRef;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.bitbucket.bradleysmithllc.etlunit.util.jdbc.JdbcVisitors;
import org.bitbucket.bradleysmithllc.etlunit.util.jdbc.MetadataPrimaryKey;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.bitbucket.bradleysmithllc.json.validator.ResourceNotFoundException;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.net.URL;
import java.sql.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class DatabaseRuntimeSupportImpl implements DatabaseRuntimeSupport {
	private MetaDataManager metaDataManager;
	private MetaDataContext dataMetaContext;
	private MetaDataContext scriptsMetaContext;

	private RuntimeSupport runtimeSupport;
	private FileRuntimeSupport fileRuntimeSupport;
	private DataFileManager dataFileManager;
	private DatabaseConfiguration databaseConfiguration;
	private Log applicationLog;

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log) {
		applicationLog = log;
	}

	@Inject
	public void receiveMetaDataManager(MetaDataManager manager) {
		metaDataManager = manager;
		dataMetaContext = metaDataManager.getMetaData().getOrCreateContext("data");
		scriptsMetaContext = metaDataManager.getMetaData().getOrCreateContext("database");
	}

	@Inject
	public void setRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport manager) {
		fileRuntimeSupport = manager;
	}

	@Inject
	public void receiveDataFileManager(DataFileManager manager) {
		dataFileManager = manager;
	}

	@Override
	public File getSourceDataSetDirectoryForCurrentTest() {
		return getSourceDataSetDirectory(runtimeSupport.getCurrentlyProcessingTestPackage());
	}

	@Override
	public File getSourceDataDirectoryForCurrentTest() {
		return getSourceDataDirectory(runtimeSupport.getCurrentlyProcessingTestPackage());
	}

	@Override
	public File getSourceScriptsDirectoryForCurrentTest() {
		return fileRuntimeSupport.getSourceScriptsDirectoryForCurrentTest();
	}

	@Override
	public File getSourceDataSetForCurrentTest(String dataSetName, String use) {
		return getSourceDataSet(runtimeSupport.getCurrentlyProcessingTestPackage(), dataSetName, use);
	}

	@Override
	public File getSourceDataForCurrentTest(String dataSetName, String use, DataFileSchema.format_type type) {
		return getSourceData(runtimeSupport.getCurrentlyProcessingTestPackage(), dataSetName, use, type);
	}

	@Override
	public File getSourceScriptForCurrentTest(String scriptName, String use) {
		return fileRuntimeSupport.getSourceScriptForCurrentTest(scriptName, use);
	}

	@Override
	public File getSourceDataSetDirectory(ETLTestPackage package_) {
		return runtimeSupport.getTestResourceDirectory(package_, "dataset");
	}

	@Override
	public File getSourceDataDirectory(ETLTestPackage package_) {
		return runtimeSupport.getTestResourceDirectory(package_, "data");
	}

	@Override
	public File getSourceScriptsDirectory(ETLTestPackage package_) {
		return fileRuntimeSupport.getSourceScriptsDirectory(package_);
	}

	@Override
	public File getSourceDataSet(ETLTestPackage _package, String _dataSetName, String use) {
		Pair<ETLTestPackage, String> results = runtimeSupport.processPackageReference(_package, _dataSetName);

		File delim = runtimeSupport.resolveFile(new FileBuilder(getSourceDataSetDirectory(results.getLeft())).name(runtimeSupport.processReference(results.getRight()) + ".dataset").file());

		if (delim != null && runtimeSupport.isTestActive()) {
			MetaDataPackageContext dataPackageContext = dataMetaContext.createPackageContext(results.getLeft(), MetaDataPackageContext.path_type.test_source);

			try {
				MetaDataArtifact art = dataPackageContext.createArtifact("dataset", getSourceDataDirectory(results.getLeft()));
				art.createContent(delim.getName()).referencedByCurrentTest(use);
			} catch (IOException e) {
				throw new IllegalStateException("Error getting data context", e);
			}
		}

		return delim;
	}

	@Override
	public File getSourceData(ETLTestPackage _package, String _dataSetName, String use, DataFileSchema.format_type type) {
		Pair<ETLTestPackage, String> results = runtimeSupport.processPackageReference(_package, _dataSetName);

		File delim = runtimeSupport.resolveFile(new FileBuilder(getSourceDataDirectory(results.getLeft())).name(runtimeSupport.processReference(results.getRight()) + "." + type.name()).file());

		if (delim != null && runtimeSupport.isTestActive()) {
			MetaDataPackageContext dataPackageContext = dataMetaContext.createPackageContext(results.getLeft(), MetaDataPackageContext.path_type.test_source);

			try {
				MetaDataArtifact art = dataPackageContext.createArtifact("data", getSourceDataDirectory(results.getLeft()));
				art.createContent(delim.getName()).referencedByCurrentTest(use);
			} catch (IOException e) {
				throw new IllegalStateException("Error getting data context", e);
			}
		}

		return delim;
	}

	@Override
	public File getSourceScript(ETLTestPackage package_, String scriptName, String use) {
		return fileRuntimeSupport.getSourceScript(package_, scriptName, use);
	}

	@Override
	public File getCachedMetaData(DatabaseConnection conn) {
		return new FileBuilder(getGeneratedSourceDirectory(conn, null)).name(conn.getId() + "_metadata.json").file();
	}

	@Override
	public File getGeneratedSourceDirectory(DatabaseConnection conn, String mode) {
		int exeId = runtimeSupport.getExecutorId();
		File database = runtimeSupport.getGeneratedSourceDirectory("database");

		File dir = new FileBuilder(database).subdir(conn.getId()).subdir(mode == null ? "[default]" : mode).subdir(String.valueOf(exeId)).mkdirs().file();

		return dir;
	}

	@Override
	public File getGeneratedData(DatabaseConnection conn, String mode, String dataSetName) {
		File dir = new FileBuilder(getGeneratedSourceDirectory(conn, mode)).name(runtimeSupport.processReference(dataSetName) + ".delimited").file();

		return dir;
	}

	@Override
	public DataFileSchema createRelationalMetaInfo(DatabaseConnection dbConn, String schema, String name) {
		return createRelationalMetaInfo(dbConn.getId(), schema, name);
	}

	@Override	public String createSchemaKey(Table table) {
		return createSchemaKey(table.getCatalog().getDatabase().getDatabaseConnection().getId(), table.getSchema().getLogicalName(), table.getLogicalName());
	}

	private String createSchemaKey(String connId, String schema, String name) {
		return connId + "." + (schema != null ? (schema + ".") : "") + name;
	}

	@Override
	public DataFileSchema createRelationalMetaInfo(String connId, String schema, String name) {
		return dataFileManager.createDataFileSchema(createSchemaKey(connId, schema, name));
	}

	@Override
	public FileRuntimeSupport.DataFileSchemaQueryResult getRelationalMetaInfo(DatabaseConnection dbConn, String schema, String name, List<String> masterName, ETLTestValueObject etlObj, ReferenceFileTypeRef.ref_type type) throws RequestedFmlNotFoundException, TestExecutionError {
		return getRelationalMetaInfo(dbConn.getId(), schema, name, masterName, etlObj, type);
	}

	@Override
	public FileRuntimeSupport.DataFileSchemaQueryResult getRelationalMetaInfo(String connId, String schema, String name, List<String> masterName, ETLTestValueObject etlObj, ReferenceFileTypeRef.ref_type type) throws RequestedFmlNotFoundException, TestExecutionError {
		// add to the search path the simple and qualified table names
		if (masterName == null) {
			masterName = new ArrayList<String>();
		}

		masterName.add(name);
		masterName.add(createSchemaKey(connId, schema, name));

		FileRuntimeSupport.DataFileSchemaQueryResult fsr = fileRuntimeSupport.locateReferenceFileSchemaForCurrentTest(etlObj, type, masterName.toArray(new String[masterName.size()]));

		return fsr;
	}

	@Override
	public void persistDataFileSchema(ETLTestOperation op, DataFileSchema schema) throws IOException {
		fileRuntimeSupport.persistGeneratedDataFileSchema(op, schema);
	}

	@Override
	public SQLAggregator resolveSQLRef(String text) throws IOException, TestExecutionError {
		SQLAggregatorImpl ddlAgg = new SQLAggregatorImpl(text);

		return ddlAgg;
	}

	@Override
	public SQLAggregator resolveDDLRef(final DDLSourceRef ddlRef, DatabaseConnection databaseConnection) throws IOException, TestExecutionError {
		String rawDDL = SQLAggregatorImpl.makeText(ddlRef, new SQLAggregator.SQLLocator() {
			public String locate(DDLSourceRef ref, DatabaseConnection connection) throws IOException, TestExecutionError {
				return getDDLText(ref).getText();
			}
		}, databaseConnection);

		File output = new FileBuilder(getGeneratedSourceDirectory(databaseConnection, null)).name("GENERATED_" + ddlRef.urlPath().replace('/', '_')).file();
		IOUtils.writeBufferToFile(output, new StringBuffer(rawDDL));

		try {
			Map bean = new HashMap();
			bean.put("databaseConnection", databaseConnection);
			bean.put("databaseRuntimeSupport", this);
			bean.put("databaseConfiguration", databaseConfiguration);
			bean.put("runtimeSupport", runtimeSupport);

			String newDDL = VelocityUtil.writeTemplate(rawDDL, bean);

			SQLAggregatorImpl impl = new SQLAggregatorImpl(newDDL);

			File outputProcessed = new FileBuilder(getGeneratedSourceDirectory(databaseConnection, null)).name("PROCESSED_" + ddlRef.urlPath().replace('/', '_')).file();
			IOUtils.writeBufferToFile(outputProcessed, new StringBuffer(newDDL));

			File outputClean = new FileBuilder(getGeneratedSourceDirectory(databaseConnection, null)).name("CLEAN_" + ddlRef.urlPath().replace('/', '_')).file();
			IOUtils.writeBufferToFile(outputClean, new StringBuffer(impl.getCleanText()));

			return impl;
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	/**
	 * Synchronized because this method works on a single schema
	 *
	 * @param databaseConnection
	 * @return
	 */
	@Override
	public synchronized int getDatabaseConfigurationState(DatabaseConnection databaseConnection) {
		// check for a marker which identifies the configuration changed before this run, but has already been updated
		File cleanDBFlag = new FileBuilder(runtimeSupport.createGeneratedSourceFile("database", databaseConnection.getImplementationId())).mkdirs().name(databaseConnection.getId() + runtimeSupport.getRunIdentifier() + "_configClean.tag").file();
		try {

			// if the flag exists - we have come through here, seen the config state
			if (cleanDBFlag.exists()) {
				runtimeSupport.getApplicationLog().info("Clean database has the state: " + databaseConnection.getId());
				return Integer.parseInt(FileUtils.readFileToString(cleanDBFlag));
			}

			File configCache = new FileBuilder(runtimeSupport.createGeneratedSourceFile("database", databaseConnection.getImplementationId())).mkdirs().name(databaseConnection.getId() + "_configCache.json").file();

			JsonNode configNode = databaseConnection.getRawDatabaseConfiguration();

			int check = configNode.toString().hashCode();

			if (configCache.exists()) {
				JsonNode flgNode = JsonUtils.loadJson(configCache);

				if (flgNode.equals(configNode)) {
					// touch the clean marker file
					FileUtils.write(cleanDBFlag, String.valueOf(check));
					return check;
				}
			}

			runtimeSupport.getApplicationLog().info("Caching database config and tagging it: " + databaseConnection.getId());
			FileUtils.write(configCache, configNode.toString());
			FileUtils.write(cleanDBFlag, String.valueOf(check));

			return check;
		} catch (Exception e) {
			throw new IllegalArgumentException("", e);
		}
	}

	@Override
	public synchronized int getDatabaseDDLState(DatabaseConnection databaseConnection) throws TestExecutionError {
		// check for a marker which identifies the configuration changed before this run, but has already been updated
		File cleanDBFlag = new FileBuilder(runtimeSupport.createGeneratedSourceFile("database", databaseConnection.getImplementationId())).mkdirs().name(databaseConnection.getId() + runtimeSupport.getRunIdentifier() + "_ddlClean.tag").file();

		try {
			// if the dirty flag exists - we have come through here, seen the config change, updated it, and left a marker for later callers
			if (cleanDBFlag.exists()) {
				runtimeSupport.getApplicationLog().info("Clean database flag says the ddl is clean: " + databaseConnection.getId());
				return Integer.parseInt(FileUtils.readFileToString(cleanDBFlag));
			}

			int state = 0;

			for (String script : databaseConnection.getSchemaScripts()) {
				File ddlCache = new FileBuilder(runtimeSupport.createGeneratedSourceFile("database", databaseConnection.getImplementationId())).mkdirs().name(databaseConnection.getId() + "_ddlCache_" + script).file();

				// load ddl ref and compare to the file cache
				DDLSourceRef ddlSourceRef = new DDLSourceRef(databaseConnection.getImplementationId(), databaseConnection.getId(), script);
				SQLAggregator sqlAggregator = resolveDDLRef(ddlSourceRef, databaseConnection);
				String text = sqlAggregator.getText();

				state += text.hashCode();

				if (!ddlCache.exists()) {
					runtimeSupport.getApplicationLog().info("DDL Cache does not exist: " + script);
					FileUtils.write(ddlCache, text);
				} else {
					String cacheText = FileUtils.readFileToString(ddlCache);

					if (!cacheText.equals(text)) {
						runtimeSupport.getApplicationLog().info("DDL Cache does not match: " + script);
						FileUtils.write(ddlCache, text);
					}
				}
			}

			FileUtils.write(cleanDBFlag, String.valueOf(state));

			return state;
		} catch (IOException exc) {
			throw new IllegalStateException("Error resolving database schema state", exc);
		}
	}

	@Override
	public DataFileSchema generateReferenceFileForTable(Table table) {
		DataFileSchema ffs = createRelationalMetaInfo(table.getCatalog().getDatabase().getDatabaseConnection(), table.getSchema().getLogicalName(), table.getLogicalName());

		// generate a source definition for this set

		// write out a json object which defines the connection id and table, and the columns
		ffs.setFormatType(DataFileSchema.format_type.delimited);

		int colIncrement = 0;
		for (TableColumn col : table.getTableColumns()) {
			colIncrement++;
			DataFileSchema.Column sc = ffs.createColumn(col.getLogicalName());

			int type = col.getType();

			sc.setType(JdbcTypeConverter.getTypeName(type));
			sc.setScale(col.getScale());
			sc.setLength(col.getPrecision());

			// let some differences in
			sc.populateDefaultSequence().setInitialValue(colIncrement);
			sc.populateDefaultSequence().setIncrement(1);

			StringBuilder expressionBuilder = new StringBuilder();

			if (col.isNullable()) {
				expressionBuilder.append("#{if}($math.modulo($defaultValue, 10) == 0)null#{else}");
			}

			switch (type) {
				case Types.BIGINT:
				case Types.INTEGER:
				case Types.SMALLINT:
				case Types.TINYINT:
					sc.setBasicType(DataFileSchema.Column.basic_type.integer);
					expressionBuilder.append("$defaultValue");

					if (type == Types.TINYINT) {
						sc.populateDefaultSequence().setModulus(128);
					} else if (type == Types.SMALLINT) {
						sc.populateDefaultSequence().setModulus(32768);
					}
					break;
				case Types.BIT:
					sc.setBasicType(DataFileSchema.Column.basic_type.integer);
					expressionBuilder.append("#{if}($defaultValue == 0)false#{else}true#{end}");
					sc.populateDefaultSequence().setModulus(2);
					break;
				case Types.DECIMAL:
				case Types.DOUBLE:
				case Types.FLOAT:
				case Types.NUMERIC:
					// check here for precision and scale
					sc.setBasicType(DataFileSchema.Column.basic_type.numeric);

					int prec = col.getPrecision();
					int scale = col.getScale();

					int rod = scale == -1 ? 0 : scale;
					int lod = prec - rod;

					if (prec != -1) {
						String expression;

						if (lod == 0) {
							expression = "0";
						} else {
							expression = "${numbers.makeIntegralDigits($defaultValue, " + lod + ")}";
						}

						if (rod > 0) {
							expression += ".${numbers.makeDecimalDigits($math.add($defaultValue, 1), " + rod + ")}";
						}

						expressionBuilder.append(expression);
					} else {
						expressionBuilder.append("$defaultValue");
					}
					break;
				case Types.DATE:
					expressionBuilder.append("$math.add(2000, $math.modulo($defaultValue, 100))-$numbers.leftPad($math.add(1, $math.modulo($defaultValue, 12)), 2)-$numbers.leftPad($math.add(1, $math.modulo($defaultValue, 28)), 2)");
					break;
				case Types.TIME:
				case Types.TIME_WITH_TIMEZONE:
					expressionBuilder.append("$numbers.leftPad($math.modulo($defaultValue, 24), 2):$numbers.leftPad($math.modulo($defaultValue, 60), 2):$numbers.leftPad($math.modulo($defaultValue, 60), 2)");
					break;
				case Types.TIMESTAMP_WITH_TIMEZONE:
					expressionBuilder.append("$math.add(2000, $math.modulo($defaultValue, 100))-$numbers.leftPad($math.add(1, $math.modulo($defaultValue, 12)), 2)-$numbers.leftPad($math.add(1, $math.modulo($defaultValue, 28)), 2)#set ($space = \" \")${space}$numbers.leftPad($math.modulo($defaultValue, 24), 2):$numbers.leftPad($math.modulo($defaultValue, 60), 2):$numbers.leftPad($math.modulo($defaultValue, 60), 2).000");
					break;
				case Types.TIMESTAMP:
					expressionBuilder.append("$math.add(2000, $math.modulo($defaultValue, 100))-$numbers.leftPad($math.add(1, $math.modulo($defaultValue, 12)), 2)-$numbers.leftPad($math.add(1, $math.modulo($defaultValue, 28)), 2)#set ($space = \" \")${space}$numbers.leftPad($math.modulo($defaultValue, 24), 2):$numbers.leftPad($math.modulo($defaultValue, 60), 2):$numbers.leftPad($math.modulo($defaultValue, 60), 2).000");
					break;
				case Types.BOOLEAN:
					// boolean is very restricted.  Any variability here makes it less useful.
					expressionBuilder.append("$math.modulo($defaultValue, 2)");
					break;
				case Types.CHAR:
				case Types.VARCHAR:
					// for a string column, if the length is less than the column name + 5, use the enumerations instead
					if (col.getPrecision() < (col.getLogicalName().length() + 5)) {
						expressionBuilder.append("$enumerations.generateEnumerated(${defaultValue}, " + col.getPrecision() + ")");
					} else {
						expressionBuilder.append("$strings.constrain(\"" + col.getLogicalName() + "[${defaultValue}]\", " + col.getPrecision() + ")");
					}
			}

			if (col.isNullable()) {
				expressionBuilder.append("#{end}");
			}

			sc.populateDefaultSequence().setExpression(expressionBuilder.toString());

			ffs.addColumn(sc);
		}

		for (TableColumn colName : table.getPrimaryKeyColumns()) {
			ffs.addKeyColumn(colName.getLogicalName());
		}

		for (TableColumn colName : table.getOrderKeyColumns()) {
			ffs.addOrderColumn(colName.getLogicalName());
		}

		return ffs;
	}

	/**
	 * Lookup the ddl text using a search path and return.
	 *
	 * @param ddlRef
	 * @return
	 * @throws IOException
	 * @throws TestExecutionError
	 */
	protected DDLSourceRef getDDLText(final DDLSourceRef ddlRef) throws IOException, TestExecutionError {
		// Enhance this to look at the classpath if the file isn't found
		DDLSourceRef actualRef = ddlRef;

		if (!loadDDLRef(actualRef)) {
			runtimeSupport.getApplicationLog().info("Unable to find ddl for reference: " + actualRef.urlPath());
			throw new TestExecutionError("Error resolving ddl ref: " + actualRef.urlPath(), DatabaseConstants.ERR_MISSING_DDL);
		}

		// instrument the ddl text with ref tags and line numbers
		StringWriter stringWriter = new StringWriter();
		PrintWriter str = new PrintWriter(stringWriter);

		BufferedReader br = new BufferedReader(new StringReader(actualRef.getText()));

		String line;

		int lineNo = 1;

		while ((line = br.readLine()) != null) {
			str.print("/**\tDDL Source [" + actualRef.refPath() + ":" + lineNo++ + "]\t**/");
			str.println(line);
		}

		actualRef.setText(stringWriter.toString());

		return actualRef;
	}

	interface DDLResolutionStrategy {
		boolean loadRef(DDLSourceRef ref) throws IOException;
	}

	List<DDLResolutionStrategy> ddlResolutionStrategies = Arrays.asList(
			this::loadDDLRefFromV2DatabaseAndImplementationFile,
			this::loadDDLRefFromV2DatabaseFile,
			this::loadDDLRefFromV2ImplementationFile,
			this::loadDDLRefFromV2File,
			this::loadDDLRefFromV2DatabaseAndImplementationClasspath,
			this::loadDDLRefFromV2DatabaseClasspath,
			this::loadDDLRefFromV2ImplementationClasspath,
			this::loadDDLRefFromV2Classpath,
			this::loadDDLRefFromV1AbsoluteClasspath,
			this::loadDDLRefFromImplementationAndDatabase,
			this::loadDDLRefV1
	);

	private boolean loadDDLRefFromV2ImplementationFile(DDLSourceRef ddl) throws IOException {
		String implementationId = ddl.getImplementationId();

		if (implementationId == null) {
			return false;
		}

		FileBuilder ddlFile = new FileBuilder(runtimeSupport.getFeatureSourceDirectory("database")).subdir(implementationId);

		File f = ddlFile.name(ddl.getSource()).file();

		if (f.exists()) {
			applicationLog.info("loadDDLRefFromV2ImplementationFile::DDL reference resolved to file " + f.getAbsolutePath());
			ddl.setText(FileUtils.readFileToString(f));
			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromV2ImplementationClasspath(DDLSourceRef ddl) throws IOException {
		String implementationId = ddl.getImplementationId();

		if (implementationId == null) {
			return false;
		}

		// check for a classpath reference
		URL ref = ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), getClass().getClassLoader()).getResource("database/" + implementationId + "/" + ddl.getSource());

		if (ref != null) {
			applicationLog.info("loadDDLRefFromV2ImplementationClasspath::DDL reference resolved to url " + ref.toExternalForm());
			ddl.setText(IOUtils.readURLToString(ref));

			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromV2File(DDLSourceRef ddl) throws IOException {
		FileBuilder ddlFile = new FileBuilder(runtimeSupport.getFeatureSourceDirectory("database"));

		File f = ddlFile.name(ddl.getSource()).file();

		if (f.exists()) {
			applicationLog.info("loadDDLRefFromV2DatabaseFile::DDL reference resolved to file " + f.getAbsolutePath());
			ddl.setText(FileUtils.readFileToString(f));
			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromV2DatabaseFile(DDLSourceRef ddl) throws IOException {
		String databaseId = ddl.getDatabaseId();

		if (databaseId == null) {
			return false;
		}

		FileBuilder ddlFile = new FileBuilder(runtimeSupport.getFeatureSourceDirectory("database")).subdir(databaseId);

		File f = ddlFile.name(ddl.getSource()).file();

		if (f.exists()) {
			applicationLog.info("loadDDLRefFromV2DatabaseFile::DDL reference resolved to file " + f.getAbsolutePath());
			ddl.setText(FileUtils.readFileToString(f));
			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromV1AbsoluteClasspath(DDLSourceRef ddl) throws IOException {
		if (!ddl.isAbsolute()) {
			return false;
		}

		// check for a classpath reference
		URL ref = ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), getClass().getClassLoader()).getResource(ddl.urlPath());

		if (ref != null) {
			applicationLog.info("loadDDLRefFromV2AbsoluteClasspath::DDL reference resolved to url " + ref.toExternalForm());
			ddl.setText(IOUtils.readURLToString(ref));

			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromV2Classpath(DDLSourceRef ddl) throws IOException {
		// check for a classpath reference
		String name = "database/" + ddl.getSource();
		URL ref = ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), getClass().getClassLoader()).getResource(name);

		if (ref != null) {
			applicationLog.info("loadDDLRefFromV2DatabaseClasspath::DDL reference resolved to url " + ref.toExternalForm());
			ddl.setText(IOUtils.readURLToString(ref));

			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromV2DatabaseClasspath(DDLSourceRef ddl) throws IOException {
		String databaseId = ddl.getDatabaseId();

		if (databaseId == null) {
			return false;
		}

		// check for a classpath reference
		String name = "database/" + databaseId + "/" + ddl.getSource();
		URL ref = ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), getClass().getClassLoader()).getResource(name);

		if (ref != null) {
			applicationLog.info("loadDDLRefFromV2DatabaseClasspath::DDL reference resolved to url " + ref.toExternalForm());
			ddl.setText(IOUtils.readURLToString(ref));

			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromV2DatabaseAndImplementationFile(DDLSourceRef ddl) throws IOException {
		String implementationId = ddl.getImplementationId();
		String databaseId = ddl.getDatabaseId();

		if (implementationId == null || databaseId == null) {
			return false;
		}

		FileBuilder ddlFile = new FileBuilder(runtimeSupport.getFeatureSourceDirectory("database")).subdir(databaseId).subdir(implementationId);

		File f = ddlFile.name(ddl.getSource()).file();

		if (f.exists()) {
			applicationLog.info("loadDDLRefFromV2DatabaseAndImplementationFile::DDL reference resolved to file " + f.getAbsolutePath());
			ddl.setText(FileUtils.readFileToString(f));
			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromV2DatabaseAndImplementationClasspath(DDLSourceRef ddl) throws IOException {
		String implementationId = ddl.getImplementationId();
		String databaseId = ddl.getDatabaseId();

		if (implementationId == null || databaseId == null) {
			return false;
		}

		// check for a classpath reference
		URL ref = ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), getClass().getClassLoader()).getResource("database/" + databaseId + "/" + implementationId + "/" + ddl.getSource());

		if (ref != null) {
			applicationLog.info("loadDDLRefFromV2DatabaseAndImplementationClasspath::DDL reference resolved to url " + ref.toExternalForm());
			ddl.setText(IOUtils.readURLToString(ref));

			return true;
		}

		return false;
	}

	private boolean loadDDLRefFromImplementationAndDatabase(DDLSourceRef ddl) throws IOException {
		String implementationId = ddl.getImplementationId();
		String databaseId = ddl.getDatabaseId();

		if (implementationId == null || databaseId == null) {
			return false;
		}

		FileBuilder ddlFile = new FileBuilder(runtimeSupport.getFeatureSourceDirectory(implementationId)).subdir(databaseId);

		File f = ddlFile.name(ddl.getSource()).file();

		if (f.exists()) {
			applicationLog.info("loadDDLRefFromImplementationAndDatabase::DDL reference resolved to file " + f.getAbsolutePath());
			ddl.setText(FileUtils.readFileToString(f));
			return true;
		}

		return false;
	}

	private boolean loadDDLRef(DDLSourceRef ddl) throws IOException {
		for (DDLResolutionStrategy strategy : ddlResolutionStrategies) {
			if (strategy.loadRef(ddl)) {
				return true;
			}
		}

		return false;
	}

	private boolean loadDDLRefV1(DDLSourceRef ddl) throws IOException {
		String implementationId = ddl.getImplementationId();
		String databaseId = ddl.getDatabaseId();

		if (implementationId != null && !ddl.isAbsolute()) {
			FileBuilder ddlFile = new FileBuilder(runtimeSupport.getFeatureSourceDirectory(implementationId));

			if (databaseId != null) {
				ddlFile.subdir(databaseId);
			}

			File f = ddlFile.name(ddl.getSource()).file();

			if (f.exists()) {
				applicationLog.info("loadDDLRefV1::DDL reference resolved to file " + f.getAbsolutePath());
				ddl.setText(FileUtils.readFileToString(f));
				return true;
			}
		}

		// check for a classpath reference
		URL ref = ObjectUtils.firstNotNull(Thread.currentThread().getContextClassLoader(), getClass().getClassLoader()).getResource(ddl.urlPath());

		if (ref != null) {
			applicationLog.info("loadDDLRefV1::DDL reference resolved to url " + ref.toExternalForm());
			ddl.setText(IOUtils.readURLToString(ref));

			return true;
		}

		return false;
	}

	public void setDatabaseConfiguration(DatabaseConfiguration databaseConfiguration) {
		this.databaseConfiguration = databaseConfiguration;
	}


	@Override
	public boolean hasDatabase(DatabaseConnection connection, VariableContext context) {
		VariableContext topLevelScope = context.getTopLevelScope();
		String variableName = "database-connection-" + connection.getId();
		return topLevelScope.hasVariableBeenDeclared(variableName);
	}

	@Override
	public Database getDatabase(DatabaseConnection connection, VariableContext context) {
		File databaseMetadataTarget = getCachedMetaData(connection);

		if (!hasDatabase(connection, context)) {
			Database database = new DatabaseImpl(connection, connection.getId());
			setDatabase(connection, context, database);

			if (databaseMetadataTarget.exists()) {
				applicationLog.info("Loading cached meta data for connection [" + connection.getId() + "]");

				try {
					database.fromJson(JsonLoader.fromFile(databaseMetadataTarget));
				} catch (IOException e) {
					// bad cache file, delete
					applicationLog.info("Discarding bad cached meta data for connection [" + connection.getId() + "]");
					databaseMetadataTarget.delete();
				}
			}
		}

		VariableContext topLevelScope = context.getTopLevelScope();
		String variableName = "database-connection-" + connection.getId();
		if (topLevelScope.hasVariableBeenDeclared(variableName)) {
			return (Database) topLevelScope.getValue(variableName).getValueAsPojo();
		}

		return null;
	}

	private void setDatabase(DatabaseConnection databaseConnection, VariableContext context, Database database) {
		VariableContext topLevelScope = context.getTopLevelScope();
		String variableName = "database-connection-" + databaseConnection.getId();

		if (!topLevelScope.hasVariableBeenDeclared(variableName)) {
			topLevelScope.declareVariable(variableName);
		}

		topLevelScope.setValue(variableName, new ETLTestValueObjectImpl(database));
	}

	@Override
	public void refreshDatabaseMetadata(
			DatabaseConnection conn,
			String mode,
			final DatabaseImplementation impl,
			VariableContext context,
			MetaDataContext databaseMetaContext,
			JDBCClient jdbcClient
	) throws Exception {
		// fake this out a bit.  Use the database ID and mode as a 'package'
		ETLTestPackage pack = ETLTestPackageImpl.getDefaultPackage().getSubPackage(conn.getId());
		if (mode != null) {
			pack.getSubPackage(mode);
		} else {
			pack.getSubPackage("[default]");
		}

		final MetaDataPackageContext packContext = databaseMetaContext.createPackageContext(pack, MetaDataPackageContext.path_type.external_source);

		File databaseMetadataTarget = getCachedMetaData(conn);
		final MetaDataArtifact art = packContext.createArtifact("schema", databaseMetadataTarget);

		final Database database = getDatabase(conn, context);
		database.clear();

		applicationLog.info("Querying meta data for connection [" + conn.getId() + "]");

		jdbcClient.useConnection(conn, mode, new JDBCClient.ConnectionClient() {
			public void connection(Connection conn, DatabaseConnection connection, String mode, DatabaseImplementation.database_role id) throws Exception {
				DatabaseMetaData meta = conn.getMetaData();

				boolean restrictToOwnedSchema = impl.restrictToOwnedSchema(connection);
				String schemaPattern = restrictToOwnedSchema ? EtlUnitStringUtils.getSafeNullableUppercase(impl.getLoginName(connection, mode)) : null;
				ResultSet rs = meta.getTables(null, schemaPattern, null, null);

				try {
					while (rs.next()) {
						//String catalog = rs.getString(1);  // I don't know what this affects yet.
						String dbTableSchema = rs.getString("TABLE_SCHEM");

						if (!impl.isUserSchema(connection, dbTableSchema)) {
							continue;
						}

						String tableName = rs.getString("TABLE_NAME");
						String tableType = rs.getString("TABLE_TYPE");

						String tableSchema = restrictToOwnedSchema ? "USER" : dbTableSchema;

						if (tableType != null) {
							String tableUpper = tableType.toUpperCase();

							Table.type type = impl.translateTableType(tableUpper);

							if (type != null) {
								Catalog databaseCatalog = database.getCatalog(null);
								Schema sch = databaseCatalog.getSchema(tableSchema);
								sch.setVirtual(restrictToOwnedSchema);

								TableImpl tbl = new TableImpl(databaseCatalog, sch, tableName, type);

								if (impl.isTableTestVisible(connection, mode, tbl)) {
									sch.addTable(tbl);

									applicationLog.debug("Driver classified table " + impl.escapeQualifiedIdentifier(tbl) + " as [" + type + "]");
									// add to meta data

									art.createContent(tbl.getQualifiedName());
								} else {
									applicationLog.debug("Driver excluded table: " + impl.escapeQualifiedIdentifier(tbl));
								}
							}
						}
					}
				} finally {
					rs.close();
				}
			}
		});
	}

	@Override
	public void loadTable(
			final Table table,
			String mode,
			final DatabaseImplementation impl
	) throws Exception {
		loadTable(table, mode, impl, null, false);
	}

	@Override
	public void loadTable(
			final Table table,
			String mode,
			final DatabaseImplementation impl,
			final ETLTestOperation op
	) throws Exception {
		loadTable(table, mode, impl, op, true);
	}

	@Override
	public void loadTable(
			final Table table,
			String mode,
			final DatabaseImplementation impl,
			final ETLTestOperation op,
			boolean loadReferenceType
	) throws Exception {
		if (table.getTableColumns().size() == 0) {
			applicationLog.info("Updating database meta data for target [" + table.getCatalog().getDatabase().getDatabaseConnection().getId() + "." + mode + "." + table.getSchema().getLogicalName() + "." + table.getLogicalName() + "]");

			String sql = null;

			if (table.getType() != Table.type.sql) {
				sql = "SELECT * FROM " + impl.escapeQualifiedIdentifier(table) + " WHERE 1 = 2";
			} else {
				sql = table.createSQLSelect(impl);
			}

			applicationLog.info("Using meta query string: " + sql);

			// issue a select for the table and check the meta data for column info
			JdbcVisitors.JdbcVisitorCommands jdvf = JdbcVisitors.withConnection(() -> {
				return impl.getConnection(table.getCatalog().getDatabase().getDatabaseConnection(), mode);
			}, (connection) -> {impl.returnConnection(connection, table.getCatalog().getDatabase().getDatabaseConnection(), mode, DatabaseImplementation.database_role.database, true);})
			.withQuery(sql, (resultSet) -> {
			}, (resultSetMetaData) -> {
				for (int col = 1; col <= resultSetMetaData.getColumnCount(); col++) {
					String columnName = resultSetMetaData.getColumnLabel(col);

					int columnType = resultSetMetaData.getColumnType(col);

					int nullable = resultSetMetaData.isNullable(col);
					boolean autoIncrement = resultSetMetaData.isAutoIncrement(col);
					int precision = resultSetMetaData.getPrecision(col);
					int scale = resultSetMetaData.getScale(col);

					// special case for length == 0 which some RDBMSs mean unconstrained
					if (precision == 0) {
						precision = -1;
					}

					// another special case, this time for scale.  Some RDBMSs give weird negative numbers (-127 for Oracle)
					if (scale < 0) {
						scale = -1;
					}

					TableColumn tc = new TableColumnImpl(
							table,
							columnName,
							columnType,
							nullable == ResultSetMetaData.columnNullable,
							precision,
							scale,
							autoIncrement
					);

					table.addColumn(tc);
				}
			});

			// add all primary key columns (only the first is of interest) - if this is not a sql query
			if (table.getType() != Table.type.sql) {
				// grab the key column names first, since the ordinals do not come in order (not always)
				List<MetadataPrimaryKey> keyColumns = new ArrayList<>();
				final AtomicReference<String> firstPk = new AtomicReference<>();

				jdvf.withPrimaryKeys(null, table.getSchema().getPhysicalName(), table.getPhysicalName(), (metadataPrimaryKey) -> {
						if (firstPk.get() == null) {
							firstPk.set(metadataPrimaryKey.pkName());
						} else if (!firstPk.get().equals(metadataPrimaryKey.pkName())) {
							// we don't care about the rest
							return;
						}

					keyColumns.add(metadataPrimaryKey);
				});

				Collections.sort(keyColumns, (keya, keyb) -> {
					return Integer.compare(keya.keySequence(), keyb.keySequence());
				});

				for (MetadataPrimaryKey metadataPrimaryKey : keyColumns) {
					table.addPrimaryKeyColumn(table.getColumn(metadataPrimaryKey.columnName()));
				}
			}

			jdvf.dispose();
		}

		if (!loadReferenceType) {
			return;
		}

		// look for the meta info in the project
		try {
			FileRuntimeSupport.DataFileSchemaQueryResult dresult = getRelationalMetaInfo(table.getCatalog().getDatabase().getDatabaseConnection(), table.getSchema().getLogicalName(), table.getLogicalName(), null, null, ReferenceFileTypeRef.ref_type.remote);

			if (dresult.getFmlSearchType() == FileRuntimeSupport.fmlSearchType.notFound) {
				// lazy, but I don't want to refactor try / catch now
				throw new ResourceNotFoundException();
			}

			DataFileSchema ffs = dresult.getDataFileSchema();

			applicationLog.info("Reading meta data from reference file: " + ffs.getId());

			// load the file and override the order by columns
			table.resetOrderColumns();
			for (String name : ffs.getOrderColumnNames()) {
				table.addOrderKeyColumn(table.getColumn(name));
			}

			table.resetPrimaryKeyColumns();
			for (String name : ffs.getKeyColumnNames()) {
				table.addPrimaryKeyColumn(table.getColumn(name));
			}

			table.setDataFileSchema(ffs);
		} catch (ResourceNotFoundException exc) {
			applicationLog.info("Could not find reference file - creating boilerplate: " + table.getQualifiedName());

			DataFileSchema ffs = generateReferenceFileForTable(table);

			table.setDataFileSchema(ffs);

			// load into Jackson and pretty-print
			persistDataFileSchema(op, ffs);
		}
	}
}
