package org.bitbucket.bradleysmithllc.etlunit.feature.database.util;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.DatabaseDefinitionsProperty;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class used for testing.
 */
public class PlainDatabaseConnection implements DatabaseConnection {
	private String id;

	private String adminUserName;
	private String adminUserPassword;

	private String defaultSchema;
	private String serverName;
	private String serverAddress;

	private int serverPort;

	private Map<String, String> properties = new HashMap<String, String>();

	public void setId(String d)
	{
		id = d;
	}

	public String defaultSchema() {
		return defaultSchema;
	}

	public PlainDatabaseConnection setDefaultSchema(String defaultSchema) {
		this.defaultSchema = defaultSchema;
		return this;
	}

	@Override
	public String getId() {
		return id;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public void setAdminUserPassword(String adminUserPassword) {
		this.adminUserPassword = adminUserPassword;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	@Override
	public String getAdminUserName() {
		return adminUserName;
	}

	@Override
	public String getAdminPassword() {
		return adminUserPassword;
	}

	@Override
	public String getImplementationId() {
		return null;
	}

	@Override
	public String getServerName() {
		return serverName;
	}

	@Override
	public String getServerAddress() {
		return serverAddress;
	}

	@Override
	public List<String> getSchemaScripts() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public DatabaseDefinitionsProperty getDatabaseConfiguration() {
		return null;
	}

	@Override
	public JsonNode getRawDatabaseConfiguration() {
		return null;
	}

	@Override
	public Map<String, String> getDatabaseProperties() {
		return properties;
	}

	@Override
	public int getServerPort() {
		return serverPort;
	}

	public void setProperty(String name, String value)
	{
		properties.put(name, value);
	}
}
