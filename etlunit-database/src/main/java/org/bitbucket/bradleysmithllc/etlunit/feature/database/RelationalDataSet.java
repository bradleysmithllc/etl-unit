package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.extract.ExtractRequest;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;

import java.util.*;

public class RelationalDataSet
{
	private final List<RelationalColumn> columns = new ArrayList<RelationalColumn>();

	private final List<RelationalColumn> keyColumns = new ArrayList<RelationalColumn>();

	private final List<RelationalColumn> orderColumns = new ArrayList<RelationalColumn>();

	private final Map<String, RelationalColumn> columnNames = new HashMap<String, RelationalColumn>();

	private final String schema;
	private final String sql;
	private final String name;

	private final boolean relationalSet;

	private final DatabaseConnection databaseConnection;

	public RelationalDataSet(String schema, String name, DatabaseConnection databaseConnection)
	{
		relationalSet = true;
		this.schema = schema;
		this.name = name;
		this.sql = null;
		this.databaseConnection = databaseConnection;
	}

	public RelationalDataSet(String sql, DatabaseConnection databaseConnection, String name)
	{
		relationalSet = false;
		this.schema = "none";
		this.name = name;
		this.sql = sql;
		this.databaseConnection = databaseConnection;
	}

	private boolean sqlDataSet()
	{
		return !relationalSet;
	}

	public RelationalColumn getColumn(String name)
	{
		RelationalColumn column =  columnNames.get(name);

		if (column == null)
		{
			throw new IllegalArgumentException("Column not found: " + name);
		}

		return column;
	}

	public void addColumn(RelationalColumn str)
	{
		for (RelationalColumn column : columns)
		{
			if (column.getName().equals(str.getName()))
			{
				throw new IllegalArgumentException("Duplicate column name: " + str.getName());
			}
		}

		columns.add(str);
		columnNames.put(str.getName(), str);
	}

	public void addPrimaryKeyColumn(RelationalColumn str)
	{
		for (RelationalColumn column : keyColumns)
		{
			if (column.getName().equals(str.getName()))
			{
				throw new IllegalArgumentException("Duplicate Key column name: " + str.getName());
			}
		}

		if (!columnNames.containsKey(str.getName()))
		{
			throw new IllegalArgumentException("Column " + str.getName() + " not found");
		}

		keyColumns.add(str);
	}

	public void addOrderByColumn(RelationalColumn str)
	{
		for (RelationalColumn column : orderColumns)
		{
			if (column.getName().equals(str.getName()))
			{
				throw new IllegalArgumentException("Duplicate Order by column name: " + str.getName());
			}
		}

		if (!columnNames.containsKey(str.getName()))
		{
			throw new IllegalArgumentException("Column " + str.getName() + " not found");
		}

		orderColumns.add(str);
	}

	public RelationalDataSet createSubView(List<String> list, ExtractRequest.ColumnListMode listMode)
	{
		if (sqlDataSet())
		{
			throw new UnsupportedOperationException("Can't create view of sql data set");
		}

		List<RelationalColumn> actualList = new ArrayList<RelationalColumn>(columns);
		List<RelationalColumn> actualKeys = new ArrayList<RelationalColumn>(keyColumns);
		List<RelationalColumn> actualOrders = new ArrayList<RelationalColumn>(orderColumns);
		Map<String, RelationalColumn> actualNames = new HashMap<String, RelationalColumn>(columnNames);

		if (actualKeys.size() == 0)
		{
			actualKeys.addAll(actualList);
		}

		if (list != null)
		{
			if (listMode == ExtractRequest.ColumnListMode.INCLUDE)
			{
				actualList.clear();
				actualKeys.clear();
				actualOrders.clear();
				actualNames.clear();
			}

			for (String col : list)
			{
				RelationalColumn column = columnNames.get(col);

				if (column == null)
				{
					throw new IllegalArgumentException("Column not exported: " + col);
				}

				switch (listMode)
				{
					case INCLUDE:
						// check the column
						actualList.add(column);
						actualNames.put(column.getName(), column);

						// check the primary key
						if (keyColumns.contains(column))
						{
							actualKeys.add(column);
						}

						// check the order columns
						if (orderColumns.contains(column))
						{
							actualOrders.add(column);
						}
						break;
					case EXCLUDE:
						// check the column
						actualList.remove(column);
						actualNames.remove(column.getName());

						// check the primary key
						if (actualKeys.contains(column))
						{
							actualKeys.remove(column);
						}

						// check the order columns
						if (orderColumns.contains(column))
						{
							actualOrders.remove(column);
						}
						break;
					default:
						throw new RuntimeException("Refactor error");
				}
			}
		}

		RelationalDataSet rds = new RelationalDataSet(schema, name, databaseConnection);

		rds.columns.addAll(actualList);
		rds.keyColumns.addAll(actualKeys);
		rds.orderColumns.addAll(actualOrders);
		rds.columnNames.putAll(actualNames);

		return rds;
	}
}
