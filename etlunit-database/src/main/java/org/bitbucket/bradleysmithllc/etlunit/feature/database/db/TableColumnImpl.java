package org.bitbucket.bradleysmithllc.etlunit.feature.database.db;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Locale;

public class TableColumnImpl extends AbstractPhysicalLogicalNamed implements TableColumn
{
	private int type;
	private boolean nullable;
	private boolean autoIncrement;
	private final Table table;
	private int precision;
	private int scale;

	public TableColumnImpl(Table table, String name, int type, boolean nullable, int prec, int sc, boolean autoIncrement)
	{
		super(name);
		this.type = type;
		this.nullable = nullable;
		this.autoIncrement = autoIncrement;
		this.table = table;
		precision = prec;
		scale = sc;
	}

	public TableColumnImpl(Table table)
	{
		this(table, null, -1, false, -1, -1, false);
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public int getType()
	{
		return type;
	}

	public boolean isNullable()
	{
		return nullable;
	}

	public boolean isAutoIncrement()
	{
		return autoIncrement;
	}

	@Override
	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	@Override
	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	@Override
	public Table getTable() {
		return table;
	}

	@Override
	public void toMoreJson(JsonWriter writer) throws IOException {
		writer.name("type").value(type);
		writer.name("nullable").value(nullable);
		writer.name("auto-increment").value(autoIncrement);
		writer.name("precision").value(precision);
		writer.name("scale").value(scale);
	}

	@Override
	public void fromMoreJson(JsonNode node) {
		type = node.get("type").asInt();
		nullable = node.get("nullable").asBoolean();
		autoIncrement = node.get("auto-increment").asBoolean();
		precision = node.get("precision").asInt();
		scale = node.get("scale").asInt();
	}
}
