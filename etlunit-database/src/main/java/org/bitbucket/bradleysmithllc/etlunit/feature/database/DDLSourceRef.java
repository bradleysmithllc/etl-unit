package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class DDLSourceRef
{
	private final String implementationId;
	private final String databaseId;
	private final String source;
	private final boolean absolute;

	private String text;

	public DDLSourceRef(String source) {
		this(null, null, source, true);
	}

	public DDLSourceRef(String implementationId, String databaseId, String source) {
		this(implementationId, databaseId, source, true);
	}

	private DDLSourceRef(String implementationId, String databaseId, String source, boolean absolute) {
		this.implementationId = implementationId;
		this.databaseId = databaseId;
		this.source = source;
		this.absolute = false;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImplementationId() {
		return implementationId;
	}

	public String getDatabaseId() {
		return databaseId;
	}

	public String getSource() {
		return source;
	}

	public String getText() {
		return text;
	}

	public boolean isAbsolute() {
		return absolute;
	}

	public String urlPath() {
		String path = getSource();

		if (getDatabaseId() != null)
		{
			path = getDatabaseId() + "/" + path;
		}

		if (getImplementationId() != null)
		{
			path = getImplementationId() + "/" + path;
		}

		return path;
	}

	public String toString()
	{
		return urlPath();
	}

	/**
	 * A ref path matches the encoded name in the script files.  The only difference is a leading
	 * '/' for absolute paths.
	 */
	public String refPath() {
		if (absolute || implementationId != null)
		{
			return "/" + urlPath();
		}
		else
		{
			return urlPath();
		}
	}
}
