package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class RelationalColumn
{
	private final String name;
	private int type;
	private final boolean nullable;
	private final boolean autoIncrement;

	public RelationalColumn(String name, int type, boolean nullable, boolean autoIncrement)
	{
		this.name = name;
		this.type = type;
		this.nullable = nullable;
		this.autoIncrement = autoIncrement;
	}

	public String getName()
	{
		return name;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public int getType()
	{
		return type;
	}

	public boolean isNullable()
	{
		return nullable;
	}

	public boolean isAutoIncrement()
	{
		return autoIncrement;
	}
}
