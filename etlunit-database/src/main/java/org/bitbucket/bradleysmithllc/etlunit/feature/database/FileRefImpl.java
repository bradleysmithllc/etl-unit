package org.bitbucket.bradleysmithllc.etlunit.feature.database;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class FileRefImpl implements SQLAggregator.FileRef
{
	private final SQLAggregator.DDLRef beginRef;
	private final SQLAggregator.DDLRef endRef;

	private final int endAggregatedLineNumber;
	private final int beginAggregatedLineNumber;

	private final int endCleanLineNumber;
	private final int beginCleanLineNumber;

	private final String text;

	public FileRefImpl(SQLAggregator.DDLRef begin, SQLAggregator.DDLRef end, int baggNo, int eaggNo, int bcln, int ecln, String text)
	{
		beginRef = begin;
		endRef = end;
		this.text = text;
		beginAggregatedLineNumber = baggNo;
		endAggregatedLineNumber = eaggNo;
		beginCleanLineNumber = bcln;
		endCleanLineNumber = ecln;
	}

	public int getEndAggregatedLineNumber()
	{
		return endAggregatedLineNumber;
	}

	public int getEndCleanLineNumber()
	{
		return endCleanLineNumber;
	}

	public int getBeginCleanLineNumber()
	{
		return beginCleanLineNumber;
	}

	@Override
	public SQLAggregator.DDLRef getBeginRef()
	{
		return beginRef;
	}

	@Override
	public SQLAggregator.DDLRef getEndRef()
	{
		return endRef;
	}

	public int getBeginAggregatedLineNumber()
	{
		return beginAggregatedLineNumber;
	}

	public String getLine()
	{
		return text;
	}

	@Override
	public String describe()
	{
		return
				" beginDDLRef: " + beginRef.describe() +
				"', endDDLRef: " + endRef.describe() +
				", beginAggregateLine: " + getBeginAggregatedLineNumber() +
				", endAggregateLine: " + getEndAggregatedLineNumber() +
				", beginCleanLine: " + getBeginCleanLineNumber() +
				", endCleanLine: " + getEndCleanLineNumber() +
				", sql: '" + getLine() +
				"'";
	}
}
