package org.bitbucket.bradleysmithllc.etlunit.feature.database.db;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.json.database.extract.ExtractRequest;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;

public class TableImpl extends AbstractPhysicalLogicalNamed implements Table
{
	private final Schema schema;
	private final Catalog catalog;
	private transient DataFileSchema dataFileSchema;

	private type type;

	private String sqlScriptSource;

	private final List<TableColumn> tableColumns = new ArrayList<TableColumn>();
	private final Map<String, TableColumn> columnLowerNames = new HashMap<String, TableColumn>();

	private final List<TableColumn> primaryKeyColumns = new ArrayList<TableColumn>();
	private final List<TableColumn> orderKeyColumns = new ArrayList<TableColumn>();

	public TableImpl(Catalog catalog, Schema schema, String name, Table.type type) {
		super(name);
		this.schema = schema;
		this.catalog = catalog;
		this.type = type;
	}

	public TableImpl(Catalog catalog, Schema schema) {
		this(catalog, schema, null, null);
	}

	@Override
	public Schema getSchema() {
		return schema;
	}

	@Override
	public Catalog getCatalog() {
		return catalog;
	}

	@Override
	public type getType() {
		return type;
	}

	@Override
	public String getQualifiedName()
	{
		return getSchema().getLogicalName() + "." + getLogicalName();
	}

	@Override
	public String getDBLocalName() {
		StringBuilder stb = new StringBuilder();

		if (catalog.getPhysicalName() != null)
		{
			stb.append(catalog.getPhysicalName());
			stb.append('.');
		}

		if (schema.getPhysicalName() != null)
		{
			stb.append(schema.getPhysicalName());
			stb.append('.');
		}

		stb.append(getPhysicalName());

		return stb.toString();
	}

	@Override
	public List<TableColumn> getTableColumns() {
		return Collections.unmodifiableList(tableColumns);
	}

	@Override
	public List<String> getTableColumnNames() {
		List<String> names = new ArrayList<String>();

		for (TableColumn tc : tableColumns)
		{
			names.add(tc.getLogicalName());
		}

		return names;
	}

	@Override
	public void resetPrimaryKeyColumns() {
		primaryKeyColumns.clear();
	}

	public String toString()
	{
		return (schema == null ? "" : (schema.toString() + ".")) + getLogicalName();
	}

	@Override
	public void toMoreJson(JsonWriter writer) throws IOException {
		writer.name("type").value(type.name());
		writer.name("sql-script").value(sqlScriptSource);

		writer.name("columns");
		writer.beginArray();

		for (TableColumn column : tableColumns)
		{
			writer.beginObject();
			column.toJson(writer);
			writer.endObject();
		}

		writer.endArray();

		writer.name("primary-key-columns");
		writer.beginArray();

		for (TableColumn column : primaryKeyColumns)
		{
			writer.value(column.getLogicalName());
		}

		writer.endArray();
	}

	@Override
	public void fromMoreJson(JsonNode node) {
		type = Table.type.valueOf(node.get("type").asText());

		JsonNode jsonNode = node.get("sql-script");

		sqlScriptSource = (jsonNode == null || jsonNode.isNull()) ? null : jsonNode.asText();

		ArrayNode anode = (ArrayNode) node.get("columns");

		Iterator<JsonNode> it = anode.iterator();

		while (it.hasNext())
		{
			JsonNode onode = it.next();

			TableColumnImpl timpl = new TableColumnImpl(this);

			timpl.fromJson(onode);

			addColumn(timpl);
		}

		anode = (ArrayNode) node.get("primary-key-columns");

		it = anode.iterator();

		while (it.hasNext())
		{
			JsonNode onode = it.next();

			addPrimaryKeyColumn(getColumn(onode.asText()));
		}
	}

	@Override
	public void addColumn(TableColumn timpl) {
		if (columnLowerNames.containsKey(timpl.getLogicalName()))
		{
			throw new IllegalArgumentException("Column named [" + timpl.getLogicalName() + "] already present");
		}

		tableColumns.add(timpl);
		columnLowerNames.put(timpl.getLogicalName(), timpl);
	}

	@Override
	public boolean hasColumn(String columnName) {
		return columnLowerNames.containsKey(columnName.toLowerCase());
	}

	@Override
	public TableColumn getColumn(String columnName) {
		if (columnName == null)
		{
			throw new NullPointerException("Column name is null");
		}

		if (!hasColumn(columnName))
		{
			throw new IllegalArgumentException("Column [" + columnName + "] not in column list");
		}

		return columnLowerNames.get(columnName.toLowerCase());
	}

	@Override
	public void addPrimaryKeyColumn(TableColumn column) {
		if (column == null)
		{
			throw new NullPointerException("Column is null");
		}

		if (primaryKeyColumns.contains(column))
		{
			throw new IllegalArgumentException("Column [" + column.getLogicalName() + "] already added to primary keys");
		}

		if (!tableColumns.contains(column))
		{
			throw new IllegalArgumentException("Column [" + column.getLogicalName() + "] not in column list");
		}

		primaryKeyColumns.add(column);
		touch();
	}

	@Override
	public List<TableColumn> getPrimaryKeyColumns() {
		return Collections.unmodifiableList(primaryKeyColumns);
	}

	@Override
	public void resetOrderColumns() {
		orderKeyColumns.clear();
		touch();
	}

	private void touch() {
		if (catalog != null)
		{
			catalog.getDatabase().touch();
		}
	}

	@Override
	public void addOrderKeyColumn(TableColumn column) {
		if (column == null)
		{
			throw new NullPointerException("Column is null");
		}

		if (orderKeyColumns.contains(column))
		{
			throw new IllegalArgumentException("Column [" + column.getLogicalName() + "] already added to order keys");
		}

		if (!tableColumns.contains(column))
		{
			throw new IllegalArgumentException("Column [" + column.getLogicalName() + "] not in column list");
		}

		orderKeyColumns.add(column);

		touch();
	}

	@Override
	public List<TableColumn> getOrderKeyColumns() {
		return Collections.unmodifiableList(orderKeyColumns);
	}

	@Override
	public void setSqlScriptSource(String sqlScriptSource) {
		if (this.sqlScriptSource == null || !this.sqlScriptSource.equals(sqlScriptSource))
		{
			if (type != Table.type.sql)
			{
				type = Table.type.sql;
			}

			this.sqlScriptSource = sqlScriptSource;

			columnLowerNames.clear();
			primaryKeyColumns.clear();
			tableColumns.clear();
			orderKeyColumns.clear();
			touch();
		}
	}

	public String getSqlScriptSource() {
		return sqlScriptSource;
	}

	@Override
	public DataFileSchema getDataFileSchema() {
		if (dataFileSchema == null)
		{
			throw new IllegalStateException("No schema");
		}

		return dataFileSchema;
	}

	@Override
	public void setDataFileSchema(DataFileSchema dataFileSchema) {
		this.dataFileSchema = dataFileSchema;

		if (this.dataFileSchema == null || !this.dataFileSchema.equals(dataFileSchema))
		{
			this.dataFileSchema = dataFileSchema;

			touch();
		}
	}

	@Override
	public boolean hasIdentityColumns()
	{
		for (TableColumn column : tableColumns)
		{
			if (column.isAutoIncrement())
			{
				return true;
			}
		}

		return false;
	}

	@Override
	public String createSQLInsert(Function<String, String> escapeFunction)
	{
		if (type == Table.type.sql)
		{
			throw new UnsupportedOperationException("Cannot insert into a synthetic data set");
		}

		StringBuffer buffer = new StringBuffer("INSERT INTO\n");
		buffer.append('\t');

		if (!schema.isVirtual() && schema.getPhysicalName() != null)
		{
			buffer.append(escapeFunction.apply(schema.getPhysicalName()));
			buffer.append('.');
		}

		buffer.append(escapeFunction.apply(getPhysicalName()));
		buffer.append("\n(\n");

		for (int i = 0; i < tableColumns.size(); i++)
		{
			TableColumn tableColumn = tableColumns.get(i);

			buffer.append('\t');
			buffer.append(escapeFunction.apply(tableColumn.getPhysicalName()));

			if (i != (tableColumns.size() - 1))
			{
				buffer.append(',');
			}

			buffer.append('\n');
		}

		buffer.append(")\n");

		buffer.append("VALUES\n(\n\t");

		for (int i = 0; i < tableColumns.size(); i++)
		{
			if (i != 0)
			{
				buffer.append(',');
			}

			buffer.append('?');
		}
		buffer.append("\n)");

		return buffer.toString();
	}

	@Override
	public String createSQLSelect(boolean ordered)
	{
		return createSQLSelect((name) -> {return name;}, ordered, null);
	}

	@Override
	public String createSQLSelect(Function<String, String> escapeFunction, boolean ordered, Pair<String, String> alternateTableName)
	{
		List<TableColumn> actualList = new ArrayList<TableColumn>(tableColumns);
		List<TableColumn> actualOrderBy = new ArrayList<TableColumn>(primaryKeyColumns);

		if (orderKeyColumns.size() != 0)
		{
			actualOrderBy.clear();
			actualOrderBy.addAll(orderKeyColumns);
		}
		else if (actualOrderBy.size() == 0)
		{
			actualOrderBy.addAll(actualList);
		}

		if (type != Table.type.sql)
		{
			StringBuffer buffer = new StringBuffer("SELECT\n");

			for (int i = 0; i < actualList.size(); i++)
			{
				buffer.append('\t');
				buffer.append(escapeFunction.apply(actualList.get(i).getPhysicalName()));

				if (i != (actualList.size() - 1))
				{
					buffer.append(',');
				}

				buffer.append('\n');
			}

			buffer.append("FROM\n");
			buffer.append('\t');

			String schemaName = null;
			String tableName = getPhysicalName();

			if (alternateTableName != null) {
				schemaName = alternateTableName.getLeft();
				tableName = alternateTableName.getRight();
			}
			else if (schema.getPhysicalName() != null && !schema.isVirtual())
			{
				schemaName = schema.getPhysicalName();
			}

			if (schemaName != null) {
				buffer.append(escapeFunction.apply(schemaName));
				buffer.append('.');
			}

			buffer.append(escapeFunction.apply(tableName));

			if (ordered)
			{
				buffer.append("\nORDER BY\n");

				for (int i = 0; i < actualOrderBy.size(); i++)
				{
					buffer.append('\t');
					buffer.append(escapeFunction.apply(actualOrderBy.get(i).getPhysicalName()));

					if (i != (actualOrderBy.size() - 1))
					{
						buffer.append(',');
					}

					buffer.append('\n');
				}
			}

			return buffer.toString();
		}

		return sqlScriptSource;
	}

	@Override
	public Table createSubView(List<String> list, ExtractRequest.ColumnListMode listMode)
	{
		if (type == Table.type.synthetic)
		{
			throw new UnsupportedOperationException("Can't create view of sql data set");
		}

		List<TableColumn> actualList = new ArrayList<TableColumn>(tableColumns);
		List<TableColumn> actualKeys = new ArrayList<TableColumn>(primaryKeyColumns);
		List<TableColumn> actualOrders = new ArrayList<TableColumn>(orderKeyColumns);
		Map<String, TableColumn> actualLowerNames = new HashMap<String, TableColumn>(columnLowerNames);

		if (actualKeys.size() == 0)
		{
			actualKeys.addAll(actualList);
		}

		if (list != null)
		{
			if (listMode == ExtractRequest.ColumnListMode.INCLUDE)
			{
				actualList.clear();
				actualKeys.clear();
				actualOrders.clear();
				actualLowerNames.clear();
			}

			for (String col : list)
			{
				TableColumn column = getColumn(col);

				if (column == null)
				{
					throw new IllegalArgumentException("Column not exported: " + col);
				}

				switch (listMode)
				{
					case INCLUDE:
						// check the column
						actualList.add(column);
						actualLowerNames.put(column.getLogicalName(), column);

						// check the primary key
						if (primaryKeyColumns.contains(column))
						{
							actualKeys.add(column);
						}

						// check the order columns
						if (orderKeyColumns.contains(column))
						{
							actualOrders.add(column);
						}
						break;
					case EXCLUDE:
						// check the column
						actualList.remove(column);
						actualLowerNames.remove(column.getLogicalName());

						// check the primary key
						if (actualKeys.contains(column))
						{
							actualKeys.remove(column);
						}

						// check the order columns
						if (orderKeyColumns.contains(column))
						{
							actualOrders.remove(column);
						}
						break;
					default:
						throw new RuntimeException("Refactor error");
				}
			}
		}

		TableImpl timpl = new TableImpl(catalog, schema, getPhysicalName(), Table.type.synthetic);

		timpl.tableColumns.addAll(actualList);
		timpl.primaryKeyColumns.addAll(actualKeys);
		timpl.orderKeyColumns.addAll(actualOrders);
		timpl.columnLowerNames.putAll(actualLowerNames);

		return timpl;
	}
}
