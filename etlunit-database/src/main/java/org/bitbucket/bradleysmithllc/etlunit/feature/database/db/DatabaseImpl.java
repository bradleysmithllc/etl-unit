package org.bitbucket.bradleysmithllc.etlunit.feature.database.db;

/*
 * #%L
 * etlunit-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import java.io.IOException;
import java.util.*;

public class DatabaseImpl extends AbstractPhysicalLogicalNamed implements Database
{
	private boolean dirty;

	private final List<Catalog> catalogList = new ArrayList<Catalog>();
	private final Map<String, Catalog> catalogMap = new HashMap<String, Catalog>();

	private final DatabaseConnection databaseConnection;

	public DatabaseImpl(DatabaseConnection dc, String name) {
		super(name);
		databaseConnection = dc;
	}

	public DatabaseImpl(DatabaseConnection dc) {
		this(dc, null);
	}

	@Override
	public void touch() {
		dirty = true;
	}

	@Override
	public boolean isClean() {
		return !dirty;
	}

	@Override
	public void clear() {
		touch();
		catalogList.clear();
		catalogMap.clear();
	}

	@Override
	public List<Catalog> getCatalogs() {
		return Collections.unmodifiableList(catalogList);
	}

	@Override
	public Map<String, Catalog> getCatalogMap() {
		return catalogMap;
	}

	@Override
	public Catalog getCatalog(String name) {
		String safeNullableUppercase = EtlUnitStringUtils.getSafeNullableUppercase(name);
		Catalog c = catalogMap.get(safeNullableUppercase);

		if (c == null)
		{
			return addCatalog(new CatalogImpl(this, safeNullableUppercase));
		}

		return c;
	}

	@Override
	public DatabaseConnection getDatabaseConnection() {
		return databaseConnection;
	}

	public Catalog addCatalog(Catalog ca)
	{
		String safeNullableUppercase = EtlUnitStringUtils.getSafeNullableUppercase(ca.getLogicalName());
		if (catalogMap.containsKey(safeNullableUppercase))
		{
			throw new IllegalArgumentException("Catalog with name [" + safeNullableUppercase + "] already added.");
		}

		catalogMap.put(safeNullableUppercase, ca);
		catalogList.add(ca);

		return ca;
	}

	@Override
	public String toString() {
		return getLogicalName();
	}

	@Override
	public void toMoreJson(JsonWriter writer) throws IOException {
		writer.name("catalogs").beginArray();

		for (Catalog table : catalogList)
		{
			writer.beginObject();
			table.toJson(writer);
			writer.endObject();
		}

		writer.endArray();

		dirty = false;
	}

	@Override
	public void fromMoreJson(JsonNode node) {
		JsonNode tables = node.get("catalogs");

		if (tables != null && !tables.isNull())
		{
			ArrayNode nodeList = (ArrayNode) tables;

			Iterator<JsonNode> it = nodeList.iterator();

			while (it.hasNext())
			{
				JsonNode tnode = it.next();

				CatalogImpl timpl = new CatalogImpl(this);

				timpl.fromJson(tnode);

				addCatalog(timpl);
			}
		}
	}
}
