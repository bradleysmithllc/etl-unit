package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.junit.Assert;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class ContextIntegrationTest extends BaseIntegrationTest
{
	private RuntimeSupport runtimeSupport;

	@Inject
	public void receiveRuntimeManager(RuntimeSupport rm)
	{
		runtimeSupport = rm;
	}

	@Test
	public void runWithInjections()
	{
		startTest();

		Assert.assertNotNull(runtimeSupport);
	}

	@Override
	protected void inspectTestRootPreTests()
	{
		File src = runtimeSupport.getTestSourceDirectory();

		File file = new File(src, "test.json");
		Assert.assertTrue(file.exists());

		try {
			JsonNode node = JsonLoader.fromFile(file);

			Assert.assertEquals(node.get("tmp_dir").asText(), JsonUtils.query(node, "runtimeSupport.tempDir").asText());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
