package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.UserDirectedClassDirectorFeature;
import org.bitbucket.bradleysmithllc.etlunit.parser.specification.ParseException;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class UserClassDirectorTest extends BaseIntegrationTest
{
	@Override
	protected List<Feature> getTestFeatures() {
		try {
			return Arrays.asList((Feature) new UserDirectedClassDirectorFeature("inst", ".*#run or .*#Run"));
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void runWithInjections()
	{
		startTest();
	}
}
