package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.io.File;
import java.util.Arrays;
import java.util.List;

public class ReportIntegrationTest extends BaseIntegrationTest
{
	private RuntimeSupport runtimeSupport;
	private int executorCount = 1;

	@Override
	protected List<Feature> getTestFeatures() {
		return Arrays.asList((Feature) new FailureFeature(temporaryFolder));
	}

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides() {
		return Arrays.asList(new RuntimeOption("etlunit.executorCount", executorCount));
	}

	@Inject
	public void receiveRuntimeManager(RuntimeSupport rm)
	{
		runtimeSupport = rm;
	}

	@Test
	public void runWithManyExecutors()
	{
		executorCount = 7;
		startTest();

		Assert.assertNotNull(runtimeSupport);
	}

	@Test
	public void runWithOneExecutor()
	{
		executorCount = 1;
		startTest();

		Assert.assertNotNull(runtimeSupport);
	}

	//@Override
	protected File useStaticTestRoot1() {
		return new File("/tmp/ut");
	}
}


class FailureFeature extends AbstractFeature
{
	private DiffManager diffManager;

	private LogFileManager logFileManager;
	private final TemporaryFolder temporaryFolder;

	FailureFeature(TemporaryFolder temporaryFolder)
	{
		this.temporaryFolder = temporaryFolder;
	}

	@Inject
	public void receiveLogFileManager(LogFileManager logFileManager)
	{
		this.logFileManager = logFileManager;
	}

	@Inject
	public void receiveDiffManager(DiffManager reporter)
	{
		diffManager = reporter;
	}

	@Override
	public String getFeatureName()
	{
		return "I-failure";
	}

	@Override
	public ClassListener getListener()
	{
		return new NullClassListener()
		{
			@Override
			public action_code process(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor)
					throws TestAssertionFailure, TestExecutionError, TestWarning
			{
				if (op.getOperationName().equals("failDiff"))
				{
					DiffGrid diffReport = diffManager.reportDiff(op.getTestMethod(), op, "TEST_FID", "exp", "act");

					DiffGridRow dRow = diffReport.addRow(0, 0, DiffGrid.line_type.changed);

					dRow.setColumnName("COLUMN_CHANGED");
					dRow.setOrderKey("KEY1");
					dRow.setSourceValue("SOURCE");
					dRow.setTargetValue("TARGET");

					dRow.done();

					dRow = diffReport.addRow(-1, 1, DiffGrid.line_type.added);

					dRow.setOrderKey("KEY2");
					dRow.setColumnName("NOSHOW");
					dRow.setSourceValue("NOSHOW");
					dRow.setTargetValue("TARGET_ADDED");

					dRow.done();

					dRow = diffReport.addRow(2, -1, DiffGrid.line_type.removed);

					dRow.setOrderKey("KEY3");
					dRow.setColumnName("NOSHOW");
					dRow.setSourceValue("SOURCE_REMOVED");
					dRow.setTargetValue("NOSHOW");

					dRow.done();

					dRow = diffReport.addRow(3, 7, DiffGrid.line_type.changed);

					dRow.setOrderKey("KEY3");
					dRow.setColumnName("DIFF");
					dRow.setSourceValue("\r\n\t\\\f\b\0");
					dRow.setTargetValue("Target");

					dRow.done();

					diffReport.done();
					return action_code.handled;
				}

				return action_code.defer;
			}
		};
	}
}
