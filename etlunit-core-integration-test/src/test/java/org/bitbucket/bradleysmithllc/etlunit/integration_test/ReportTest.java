package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.report.BetterReportFeatureModule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class ReportTest extends BaseIntegrationTest
{
	private BetterReportFeatureModule betterReportFeatureModule;

	@Inject
	public void setBetterReportFeatureModule(BetterReportFeatureModule module)
	{
		betterReportFeatureModule = module;
	}

	/* Cases:
	 * id:       	pass error ignore warn fail
	 * passed    	Y    N     N      N    N
	 * errored   	N    Y     N      N    N
	 * pyeyinwnfn	Y    Y     N      N    N
	 * ignored   	N    N     Y      N    N
	 * pyeniywnfn	Y    N     Y      N    N
	 * pneyiywnfn	N    Y     Y      N    N
	 * pyeyiywnfn	Y    Y     Y      N    N
	 * warned    	N    N     N      Y    N
	 * pyeninwyfn	Y    N     N      Y    N
	 * pneyinwyfn	N    Y     N      Y    N
	 * pyeyinwyfn	Y    Y     N      Y    N
	 * pneniywyfn	N    N     Y      Y    N
	 * pyeniywyfn	Y    N     Y      Y    N
	 * pneyiywyfn	N    Y     Y      Y    N
	 * pyeyiywyfn	Y    Y     Y      Y    N
	 * failed    	N    N     N      N    Y
	 * pyeninwnfy	Y    N     N      N    Y
	 * pneyinwnfy	N    Y     N      N    Y
	 * pyeyinwnfy	Y    Y     N      N    Y
	 * pneniywnfy	N    N     Y      N    Y
	 * pyeniywnfy	Y    N     Y      N    Y
	 * pneyiywnfy	N    Y     Y      N    Y
	 * pyeyiywnfy	Y    Y     Y      N    Y
	 * pneninwyfy	N    N     N      Y    Y
	 * pyeninwyfy	Y    N     N      Y    Y
	 * pneyinwyfy	N    Y     N      Y    Y
	 * pyeyinwyfy	Y    Y     N      Y    Y
	 * pneniywyfy	N    N     Y      Y    Y
	 * pyeniywyfy	Y    N     Y      Y    Y
	 * pneyiywyfy	N    Y     Y      Y    Y
	 * pyeyiywyfy	Y    Y     Y      Y    Y
	 */

	private boolean hasPass = false;
	private boolean hasError = false;
	private boolean hasIgnore = false;
	private boolean hasWarn = false;
	private boolean hasFail = false;

	@Before
	public void resetFlags()
	{
		hasPass = false;
		hasError = false;
		hasIgnore = false;
		hasWarn = false;
		hasFail = false;
	}

	@Override
	protected void assertions(TestResults results, int pass) throws Exception {
		File ind = betterReportFeatureModule.getReportIndexFile();

		String str = FileUtils.readFileToString(ind);

		if (hasPass)
		{
			Assert.assertTrue(str.contains("<th>Passed</th>"));
		}
		else
		{
			Assert.assertFalse(str.contains("<th>Passed</th>"));
		}

		if (hasError)
		{
			Assert.assertTrue(str.contains("<th>Error</th>"));
		}
		else
		{
			Assert.assertFalse(str.contains("<th>Error</th>"));
		}

		if (hasFail)
		{
			Assert.assertTrue(str.contains("<th>Failed</th>"));
		}
		else
		{
			Assert.assertFalse(str.contains("<th>Failed</th>"));
		}

		if (hasIgnore)
		{
			Assert.assertTrue(str.contains("<th>Ignored</th>"));
		}
		else
		{
			Assert.assertFalse(str.contains("<th>Ignored</th>"));
		}

		if (hasWarn)
		{
			Assert.assertTrue(str.contains("<th>Warning</th>"));
		}
		else
		{
			Assert.assertFalse(str.contains("<th>Warning</th>"));
		}
	}

	private ReportTest hasError()
	{
		hasError = true;
		return this;
	}

	private ReportTest hasPass()
	{
		hasPass = true;
		return this;
	}

	private ReportTest hasFail()
	{
		hasFail = true;
		return this;
	}

	private ReportTest hasIgnore()
	{
		hasIgnore = true;
		return this;
	}

	private ReportTest hasWarn()
	{
		hasWarn = true;
		return this;
	}

	@Test
	public void errored()
	{
		hasError();
		startTest("ReportTest_errored");
	}

	@Test
	public void ignored()
	{
		hasIgnore();
		startTest("ReportTest_ignored");
	}

	@Test
	public void failed()
	{
		hasFail();
		startTest("ReportTest_failed");
	}

	@Test
	public void passed()
	{
		hasPass();
		startTest("ReportTest_passed");
	}

	@Test
	public void warned()
	{
		hasPass().hasWarn();
		startTest("ReportTest_warned");
	}

	@Test
	public void pyeyinwnfn()
	{
		hasPass().hasError();
		startTest("ReportTest_pyeyinwnfn");
	}

	@Test
	public void pyeniywnfn()
	{
		hasPass().hasIgnore();
		startTest("ReportTest_pyeniywnfn");
	}

	@Test
	public void pneyiywnfn()
	{
		hasError().hasIgnore();
		startTest("ReportTest_pneyiywnfn");
	}

	@Test
	public void pyeninwnfy()
	{
		hasPass().hasFail();
		startTest("ReportTest_pyeninwnfy");
	}

	@Test
	public void pyeyinwnfy()
	{
		hasPass().hasError().hasFail();
		startTest("ReportTest_pyeyinwnfy");
	}

	@Test
	public void pneniywnfy()
	{
		hasIgnore().hasFail();
		startTest("ReportTest_pneniywnfy");
	}

	@Test
	public void pneyinwnfy()
	{
		hasError().hasFail();
		startTest("ReportTest_pneyinwnfy");
	}

	@Test
	public void pyeyiywnfn() {
		hasPass().hasError().hasIgnore();
		startTest("ReportTest_pyeyiywnfn");
	}

	@Test
	public void pyeniywnfy()
	{
		hasPass().hasIgnore().hasFail();
		startTest("ReportTest_pyeniywnfy");
	}

	@Test
	public void pneyiywnfy()
	{
		hasError().hasIgnore().hasFail();
		startTest("ReportTest_pneyiywnfy");
	}

	@Test
	public void pyeyiywnfy()
	{
		hasPass().hasError().hasIgnore().hasFail();
		startTest("ReportTest_pyeyiywnfy");
	}

	@Test
	public void pneninwyfy()
	{
		hasPass().hasWarn().hasFail();
		startTest("ReportTest_pneninwyfy");
	}

	@Test
	public void pyeninwyfy()
	{
		hasPass().hasWarn().hasFail();
		startTest("ReportTest_pyeninwyfy");
	}

	@Test
	public void pneyinwyfy()
	{
		hasPass().hasError().hasWarn().hasFail();
		startTest("ReportTest_pneyinwyfy");
	}

	@Test
	public void pyeyinwyfy()
	{
		hasPass().hasError().hasWarn().hasFail();
		startTest("ReportTest_pyeyinwyfy");
	}

	@Test
	public void pneniywyfy()
	{
		hasPass().hasIgnore().hasWarn().hasFail();
		startTest("ReportTest_pneniywyfy");
	}

	@Test
	public void pyeniywyfy()
	{
		hasPass().hasIgnore().hasWarn().hasFail();
		startTest("ReportTest_pyeniywyfy");
	}

	@Test
	public void pneyiywyfy()
	{
		hasPass().hasError().hasIgnore().hasWarn().hasFail();
		startTest("ReportTest_pneyiywyfy");
	}

	@Test
	public void pyeyiywyfy()
	{
		hasPass().hasError().hasIgnore().hasWarn().hasFail();
		startTest("ReportTest_pyeyiywyfy");
	}

	@Test
	public void pyeninwyfn()
	{
		hasPass().hasWarn();
		startTest("ReportTest_pyeninwyfn");
	}

	@Test
	public void pneyinwyfn()
	{
		hasPass().hasError().hasWarn();
		startTest("ReportTest_pneyinwyfn");
	}

	@Test
	public void pyeyinwyfn()
	{
		hasPass().hasError().hasWarn();
		startTest("ReportTest_pyeyinwyfn");
	}

	@Test
	public void pneniywyfn()
	{
		hasPass().hasIgnore().hasWarn();
		startTest("ReportTest_pneniywyfn");
	}

	@Test
	public void pyeniywyfn()
	{
		hasPass().hasIgnore().hasWarn();
		startTest("ReportTest_pyeniywyfn");
	}

	@Test
	public void pneyiywyfn()
	{
		hasPass().hasError().hasIgnore().hasWarn();
		startTest("ReportTest_pneyiywyfn");
	}

	@Test
	public void pyeyiywyfn()
	{
		hasPass().hasError().hasIgnore().hasWarn();
		startTest("ReportTest_pyeyiywyfn");
	}
}
