package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.MetaFeature;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class FeatureMetaInfoIntegrationTest extends BaseIntegrationTest
{
	@Override
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList((Feature) new MetaFeature());
	}

	@Test
	public void test()
	{
		TestResults res = startTest();

		Assert.assertEquals(6, res.getNumTestsSelected());
		Assert.assertEquals(6, res.getMetrics().getNumberOfTestsPassed());
	}
}
