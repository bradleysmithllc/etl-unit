package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureMetaInfo;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ClassListenerCallbacks extends BaseIntegrationTest
{
	private RuntimeSupport runtimeSupport;

	private final AtomicInteger begin = new AtomicInteger(0);
	private final AtomicInteger end = new AtomicInteger(0);

	private final MyNullClassListener myNullClassListener = new MyNullClassListener();

	private class MyNullClassListener extends NullClassListener {
		@Override
		public void begin(ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
			begin.incrementAndGet();
		}

		@Override
		public void end(ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
			end.incrementAndGet();
		}

		@Override
		public void begin(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
			begin.addAndGet(1000);
		}

		@Override
		public void end(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
			end.addAndGet(1000);
		}

		@Override
		public void begin(ETLTestClass cl, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
			begin.addAndGet(1000000);
		}

		@Override
		public void end(ETLTestClass cl, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
			end.addAndGet(1000000);
		}
	}

	private class AF extends AbstractFeature {
		@Override
		public String getFeatureName() {
			return "af";
		}

		@Override
		public FeatureMetaInfo getMetaInfo() {
			return null;
		}

		@Override
		public ClassListener getListener() {
			return myNullClassListener;
		}
	};

	private AF af = new AF();

	@Override
	protected List<Feature> getTestFeatures() {
		return Arrays.asList((Feature) af);
	}

	@Inject
	public void receiveRuntimeManager(RuntimeSupport rm)
	{
		runtimeSupport = rm;
	}

	@Test
	public void runWithInjections()
	{
		startTest();

		Assert.assertNotNull(runtimeSupport);

		// assert that begins == ends and that begin is greater than 0
		Assert.assertNotEquals(0, begin.get());
		Assert.assertEquals(begin.get(), end.get());
	}
}
