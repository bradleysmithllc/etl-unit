package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.junit.Assert;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

public class LayoutIntegrationTest extends BaseIntegrationTest
{
	private RuntimeSupport runtimeSupport2;

	@Inject
	public void receiveRuntimeManager(RuntimeSupport rm)
	{
		runtimeSupport2 = rm;
	}

	@Test
	public void runWithInjections()
	{
		startTest("layout");

		Assert.assertNotNull(runtimeSupport2);
	}

	@Override
	protected void inspectTestRootPreTests()
	{
		File src = runtimeSupport.getTestSourceDirectory();

		ETLTestPackage defaultPackage = ETLTestPackageImpl.getDefaultPackage();

		Assert.assertTrue(new File(src, "test.etlunit").exists());

		File pack1 = runtimeSupport.getTestSourceDirectory(defaultPackage.getSubPackage("pack1"));

		Assert.assertTrue(new File(pack1, "test.etlunit").exists());

		File subpack1 = runtimeSupport.getTestSourceDirectory(defaultPackage.getSubPackage("pack1.subpack1"));

		Assert.assertTrue(new File(subpack1, "test.etlunit").exists());

		File feat1 = runtimeSupport.getTestResourceDirectory("feat1");

		Assert.assertTrue(new File(feat1, "file1").exists());

		File feat2 = runtimeSupport.getTestResourceDirectory("feat2");

		Assert.assertTrue(new File(feat2, "file1").exists());

		List<ETLTestPackage> packs = runtimeSupport.getTestPackages();

		Assert.assertEquals(3, packs.size());

		Assert.assertSame(ETLTestPackageImpl.getDefaultPackage(), packs.get(0));
		Assert.assertEquals("pack1", packs.get(1).getPackageName());
		Assert.assertEquals("pack1.subpack1", packs.get(2).getPackageName());

		File fsrc = runtimeSupport.getFeatureSourceDirectory("database");
		Assert.assertTrue(new File(fsrc, "file").exists());

		fsrc = runtimeSupport.getFeatureSourceDirectory("file");
		Assert.assertTrue(new File(fsrc, "l1").exists());
		Assert.assertTrue(new File(new File(fsrc, "sub"), "l2").exists());
	}
}
