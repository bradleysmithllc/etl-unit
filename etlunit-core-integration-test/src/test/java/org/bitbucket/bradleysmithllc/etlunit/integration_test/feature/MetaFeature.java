package org.bitbucket.bradleysmithllc.etlunit.integration_test.feature;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.junit.Assert;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.boo.BooHandler;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.boo.BooRequest;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.multihomed.multi.MultiHandler;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.multihomed.multi.MultiRequest;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.multihomed.none.NoneHandler;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.multihomed.none.NoneRequest;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.multihomed.single.SingleHandler;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.multihomed.single.SingleRequest;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.multihomed.test_sql.TestSqlHandler;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.multihomed.test_sql.TestSqlRequest;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.yah.YahHandler;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.feature.json.meta.yah.YahRequest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

public class MetaFeature extends AbstractFeature
{
	private final class ListenToMe extends NullClassListener implements
			BooHandler,
			YahHandler,
			MultiHandler,
			SingleHandler,
			NoneHandler,
			TestSqlHandler
	{
		@Override
		public action_code boo(BooRequest booop, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext vcontext, ExecutionContext econtext)
		{
			Assert.assertEquals("boo", op.getOperationName());
			Assert.assertEquals(0, op.getOrdinal());
			return action_code.handled;
		}

		@Override
		public action_code yah(YahRequest booop, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext vcontext, ExecutionContext econtext)
		{
			Assert.assertEquals("yah", op.getOperationName());
			Assert.assertEquals(0, op.getOrdinal());
			return action_code.handled;
		}

		@Override
		public action_code multi(MultiRequest booop, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext vcontext, ExecutionContext econtext)
		{
			Assert.assertEquals("pass-multi", booop.getMulti());
			Assert.assertEquals("multihomed", op.getOperationName());
			Assert.assertEquals(0, op.getOrdinal());
			return action_code.handled;
		}

		@Override
		public action_code none(NoneRequest booop, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext vcontext, ExecutionContext econtext)
		{
			Assert.assertEquals("multihomed", op.getOperationName());
			Assert.assertEquals(0, op.getOrdinal());
			return action_code.handled;
		}

		@Override
		public action_code single(SingleRequest booop, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext vcontext, ExecutionContext econtext)
		{
			Assert.assertEquals("pass-single", booop.getSingle());
			Assert.assertEquals("multihomed", op.getOperationName());
			Assert.assertEquals(0, op.getOrdinal());
			return action_code.handled;
		}

		@Override
		public action_code testSql(TestSqlRequest booop, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext vcontext, ExecutionContext econtext)
		{
			Assert.assertEquals("pass-test-sql", booop.getTestSql());
			Assert.assertEquals("multihomed", op.getOperationName());
			Assert.assertEquals(0, op.getOrdinal());
			return action_code.handled;
		}
	}

	private final ListenToMe listenToMe = new ListenToMe();

	@Override
	public String getFeatureName()
	{
		return "meta";
	}

	@Override
	public ClassListener getListener()
	{
		return listenToMe;
	}
}
