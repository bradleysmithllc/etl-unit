package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.stream.JsonWriter;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.ClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.TagDirector;
import org.bitbucket.bradleysmithllc.etlunit.TestClasses;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.tags.TagRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.junit.Test;

import javax.inject.Inject;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ContextVariablesIntegrationTest extends BaseIntegrationTest
{
	@Test
	public void runWithInjections()
	{
		startTest();
	}

	//@Test
	public void tt() throws IOException {
		File dir = new File("/Users/bsmith/git/etl-unit/etlunit-core-integration-test/src/test/resources/integration_tests/ContextVariablesIntegrationTest/src");
		File src = new File("/Users/bsmith/git/etl-unit/etlunit-core-integration-test/src/test/resources/integration_tests/ContextVariablesIntegrationTest/src/test_1_1.etlunit");

		String srcTxt = FileUtils.readFileToString(src);

		for (int i = 1; i < 10; i++)
		{
			for (int j = 1; j < 10; j++)
			{
				String testName = "test_" + i + "_" + j;

				String destTxt = srcTxt.replace("test_1_1", testName);
				File dest = new File(dir, testName + ".etlunit");
				FileUtils.write(dest, destTxt);
			}
		}
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed) {
		String tmpi = null;
		try {
			tmpi = VelocityUtil.writeTemplate("${clsvar}", context);
		} catch (Exception e) {
			throw new RuntimeException("");
		}
			if (!tmpi.equals("cls_" + mt.getTestClass().getName()))
			{
				throw new RuntimeException(tmpi + "<>" + mt.getQualifiedName());
			}
	}
}
