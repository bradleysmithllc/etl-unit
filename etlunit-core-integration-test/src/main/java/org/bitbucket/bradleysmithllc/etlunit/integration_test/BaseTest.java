package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ETLTestVM;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.Collections;
import java.util.List;

public class BaseTest
{
	public static final boolean ECHO_LOG = false;

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	protected File root;
	protected File tmp;
	protected File rpt;
	protected File src;
	protected File genSrc;
	protected File cfg;
	protected File rsc;
	protected File rfrnc;
	protected ETLTestVM etlTestVM;
	protected RuntimeSupport runtimeSupport;
	protected Log applicationLog;

	@Inject
	public final void receiveRuntimeSupport(RuntimeSupport support) {
		runtimeSupport = support;
	}

	@Inject
	public final void receiveApplicationLog(@Named("applicationLog") Log applicationLog) {
		this.applicationLog = applicationLog;
	}

	protected List<Feature> getTestFeatures() {
		return Collections.EMPTY_LIST;
	}

	protected List<RuntimeOption> getRuntimeOptionOverrides() {
		return null;
	}

	protected File useStaticTestRoot() {
		return null;
	}

	protected boolean cleanWorkspace() {
		return false;
	}
}
