package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class FileMatcher {
/*
	static Matcher<File> containsExactText(File expectedFile){
		return new TypeSafeMatcher<File>(){
			String failure;
			public boolean matchesSafely(File underTest){
				//create readers for each/convert to strings
				BufferedReader brexpect = new BufferedReader(new FileReader(expectedFile));

				try
				{
				//Your implementation here, something like:
				String line;
				while ((line = expected.readLine()) != null) {
					Matcher<?> equalsMatcher = CoreMatchers.equalTo(line);
					String actualLine = actual.readLine();
					if (!equalsMatcher.matches(actualLine){
						failure = equalsMatcher.describeFailure(actualLine);
						return false;
					}
				}
				//record failures for uneven lines
				}
			}

			public String describeFailure(File underTest)
			{
				return failure;
			}

			@Override
			public void describeTo(Description description) {
			}
		};
	}
	 */
}
