package org.bitbucket.bradleysmithllc.etlunit.integration_test;

/*
 * #%L
 * etlunit-core-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.google.inject.Injector;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.ClassUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.UserDirectedClassDirectorFeature;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.debug.ConsoleFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.debug.RunAllFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.regexp.PackNameExpression;
import org.bitbucket.bradleysmithllc.etlunit.test_support.MockProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.test_support.ScriptMockCallback;
import org.bitbucket.bradleysmithllc.etlunit.test_support.TestTempFileNamingPolicy;
import org.bitbucket.bradleysmithllc.etlunit.util.*;
import org.junit.Assert;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class BaseIntegrationTest extends BaseTest {
	private final Set<String> testMarks = new HashSet<String>();

	private TestResults results;
	private int executorCount = 1;

	protected String identifier;

	private final String thisClassName;

	protected final Map<String, Object> velocityContext = new HashMap<>();

	protected BaseIntegrationTest() {
		thisClassName = ClassUtils.getShortClassName(getClass());
	}

	protected synchronized void markTestMilestone(String milestone)
	{
		if (!testMarks.add(milestone))
		{
			throw new AssertionError("Milestone already achieved {" + milestone + "}");
		}
	}

	protected List<String> requireMilestones()
	{
		return Collections.EMPTY_LIST;
	}

	protected TestResults startTest() {
		executorCount = 1;
		return startTest(makeTestId());
	}

	String makeTestId() {
		return getClass().getSimpleName();
	}

	protected TestResults startTest(String testId) {
		try {
			Configuration.ignoreUserHome(true);

			testMarks.clear();
			TestResults testResults = startTestImpl(testId, 1);
			checkMilestones();

			if (multiPassSafe()) {
				// run the test again with 10 executors
				testMarks.clear();
				executorCount = 10;
				testResults = startTestImpl(testId, 2);
				checkMilestones();
			}

			return testResults;
		} finally {
			Configuration.ignoreUserHome(false);
		}
	}

	private void checkMilestones()
	{
		List<String> mslist = requireMilestones();

		for (String ms : mslist)
		{
			if (!testMarks.remove(ms))
			{
				throw new AssertionError("Milestone {" + ms + "} not met");
			}
		}

		if (!testMarks.isEmpty())
		{
			throw new AssertionError("Milestones met but not expected: " + testMarks);
		}
	}

	private TestResults startTestImpl(String testId, int pass) {
		if (testId == null || testId.trim().equals("")) {
			throw new IntegrationTestException("Must provide a valid test id");
		}

		initializeTests();

		String classKey = thisClassName;

		if (identifier != null) {
			classKey += "_" + identifier;
		}

		if (ECHO_LOG) {
			System.setProperty("org.bitbucket.bradleysmithllc.etlunit.feature.logging.echo", "true");
		} else {
			System.clearProperty("org.bitbucket.bradleysmithllc.etlunit.feature.logging.echo");
		}

		String resourceRoot = "/integration_tests/" + testId;

		URL url = getClass().getResource(resourceRoot);

		if (url == null) {
			throw new IntegrationTestException("Integration test root '" + resourceRoot + "' not present.");
		}

		try {
			File intRoot = new File(url.toURI());

			System.out.println("Processing integration test '" + testId + "' in root " + intRoot);

			File confRoot = new File(intRoot, "conf");
			File confJson = new File(confRoot, "etlunit.json");

			if (root == null || cleanWorkspace()) {
				root = useStaticTestRoot();

				if (root == null) {
					File tempFile;

					String prop = System.getProperty("projectBuildDirectory");

					File target = new File("target");

					if (prop != null) {
						tempFile = new File(prop, "etlunit-test").getAbsoluteFile();
					} else if (target.exists()) {
						tempFile = new File(target, "etlunit-test").getAbsoluteFile();
					} else {
						tempFile = temporaryFolder.getRoot();
					}

					root = new File(tempFile, getClass().getName().replace('.', '_') + ".working");
				}

				System.out.println("Using test root '" + root.getAbsolutePath() + "'");

				src = new FileBuilder(root).subdir("src").subdir("test").subdir("etlunit").file();
				cfg = new FileBuilder(root).subdir("src").subdir("test").subdir("resources").subdir("config").file();
				rsc = new FileBuilder(root).subdir("src").subdir("main").file();
				tmp = new FileBuilder(root).subdir("target").subdir("etlunit-temp").mkdirs().file();
				rpt = new FileBuilder(root).subdir("target").file();
				genSrc = new FileBuilder(rpt).subdir("generated-sources").file();
				rfrnc = new FileBuilder(rsc).subdir("reference").file();

				// create a test source root
				if (root.exists()) {
					IOUtils.purge(root, true);
				}

				//assertTrue(root.mkdirs());
				assertTrue(src.mkdirs());
				assertTrue(cfg.mkdirs());
				assertTrue(tmp.mkdirs());
				assertTrue(rsc.mkdirs());
				assertTrue(genSrc.mkdirs());
				assertTrue(rfrnc.mkdirs());
			}

			assertTrue(src.exists());
			assertTrue(cfg.exists());
			assertTrue(rsc.exists());
			assertTrue(tmp.exists());
			assertTrue(genSrc.exists());
			assertTrue(rfrnc.exists());

			// copy all proprties into the velocity context
			velocityContext.clear();

			velocityContext.put("JsonEscaper", JsonEscaper.class);
			velocityContext.put("junit_temp_dir", temporaryFolder.getRoot());

			// store unix paths
			velocityContext.put("root_dir", JsonEscaper.escape(EtlUnitStringUtils.convertFilePathToUnix(root)));
			velocityContext.put("tmp_dir", JsonEscaper.escape(EtlUnitStringUtils.convertFilePathToUnix(tmp)));
			velocityContext.put("rpt_dir", JsonEscaper.escape(EtlUnitStringUtils.convertFilePathToUnix(rpt)));
			velocityContext.put("src_dir", JsonEscaper.escape(EtlUnitStringUtils.convertFilePathToUnix(src)));
			velocityContext.put("gen_src_dir", JsonEscaper.escape(EtlUnitStringUtils.convertFilePathToUnix(genSrc)));
			velocityContext.put("cfg_dir", JsonEscaper.escape(EtlUnitStringUtils.convertFilePathToUnix(cfg)));
			velocityContext.put("rsc_dir", JsonEscaper.escape(EtlUnitStringUtils.convertFilePathToUnix(rsc)));
			velocityContext.put("identifier", JsonEscaper.escape(identifier));
			velocityContext.put("test_class_name", JsonEscaper.escape(thisClassName));

			JSonBuilderProxy bpf =
					new JSonBuilderProxy()
							.object()
							.key("project-root-directory")
							.value(root.getAbsolutePath())
							.key("test-sources-directory")
							.value(src.getAbsolutePath())
							.key("vendor-binary-directory")
							.value(tmp.getAbsolutePath())
							.key("configuration-directory")
							.value(cfg.getAbsolutePath())
							.key("resource-directory")
							.value(rsc.getAbsolutePath())
							.key("reference-directory")
							.value(rfrnc.getAbsolutePath())
							.key("temp-directory")
							.value(tmp.getAbsolutePath())
							.key("reports-directory")
							.value(rpt.getAbsolutePath())
							.key("generated-source-directory")
							.value(genSrc.getAbsolutePath())
							.key("runtimeSupport")
							.object()
							.key("processExecutor")
							.value(LoggingProcessExecutor.class.getName())
							.endObject()
							.key("project-name")
							.value("feature-test")
							.key("project-version")
							.value("1.0-test")
							.key("project-user")
							.value("buildUser")
							.key("project-uid")
							.value("buildUid")
							.key("features")
							.object()
							.endObject()
							.key("options")
							.object()
							.key("etlunit")
							.object()
							.key("executorCount")
							.object()
							.key("integer-value").value(getExecutorCount())
							.endObject()
							.endObject()
							.endObject()
							.endObject();

			String s = bpf.toString();
			ETLTestValueObject etlValue = ETLTestParser.loadObject(s);

			if (confJson.exists()) {
				etlValue = etlValue.merge(ETLTestParser.loadObject(FileUtils.readFileToString(confJson)), ETLTestValueObject.merge_type.right_merge, ETLTestValueObject.merge_policy.recursive);
			}

			// write out prettily and run through velocity
			File resultFile = new File(cfg, confJson.getName());

			String jsonContent = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(etlValue.getJsonNode());

			String newJsonConfig = VelocityUtil.writeTemplate(jsonContent, velocityContext);

			FileUtils.write(resultFile, newJsonConfig);

			etlTestVM = new ETLTestVM(
					new Configuration(ETLTestParser.loadObject(FileUtils.readFileToString(resultFile)))
			);

			RuntimeSupport runtimeSupport1 = etlTestVM.getRuntimeSupport();
			runtimeSupport1.activateTempFileNamingPolicy(new TestTempFileNamingPolicy());

			List<RuntimeOption> ro = getRuntimeOptionOverrides();

			if (ro != null) {
				runtimeSupport1.overrideRuntimeOptions(ro);
			}

			velocityContext.put("runtimeSupport", runtimeSupport1);
			prepareTestContext();

			etlTestVM.addFeature(new ConsoleFeatureModule());
			etlTestVM.addFeature(new RunAllFeatureModule());
			// add another feature to provide a context callback
			etlTestVM.addFeature(new DebugCallbacker());

			String testSpec = testSpecification();

			if (testSpec != null)
			{
				etlTestVM.addFeature(new UserDirectedClassDirectorFeature("base-integration-test-selector", testSpec));
			}

			List<Feature> features = getTestFeatures();

			if (features != null) {
				Iterator<Feature> it = features.iterator();

				while (it.hasNext()) {
					etlTestVM.addFeature(it.next());
				}
			}

			etlTestVM.installFeatures();

			applicationLog.info("Using working root: " + root.getAbsolutePath());

			URL processUrl = getClass().getResource("/" + classKey);
			if (processUrl != null) {
				runtimeSupport.installProcessExecutor(new MockProcessExecutor(new ScriptMockCallback(classKey)));
			}

			File srcRoot = new File(intRoot, "src");

			if (srcRoot.exists()) {
				// scan the src directory
				deepscanSrc(srcRoot, ETLTestPackageImpl.getDefaultPackage(), null, scan_type.src);
			}

			File rsrcRoot = new File(intRoot, "feature_src");

			if (rsrcRoot.exists()) {
				// scan the src directory
				deepscanSrc(rsrcRoot, null, null, scan_type.feature_src);
			}

			inspectTestRootPreTests();
			prepareTest();
			TestResults results = etlTestVM.runTests();
			postTest();

			assertions(results, pass);

			// check assertions
			File resultsFile = new FileBuilder(intRoot).subdir("assertions").name("results.json").file();

			if (resultsFile.exists()) {
				JsonNode node = JsonLoader.fromFile(resultsFile);

				JsonNode count = node.get("num-tests-selected");

				if (count != null && !count.isNull()) {
					Assert.assertEquals("Incorrect number of tests selected", count.asInt(), results.getNumTestsSelected());
				}

				count = node.get("num-tests-run");

				if (count != null && !count.isNull()) {
					Assert.assertEquals("Incorrect number of tests run", count.asInt(), results.getMetrics().getNumberOfTestsRun());
				}

				count = node.get("num-tests-passed");

				if (count != null && !count.isNull()) {
					Assert.assertEquals("Incorrect number of tests passed", count.asInt(), results.getMetrics().getNumberOfTestsPassed());
				}

				count = node.get("num-assertion-failures");

				if (count != null && !count.isNull()) {
					Assert.assertEquals("Incorrect number of tests failed", count.asInt(), results.getMetrics().getNumberOfAssertionFailures());
				}

				count = node.get("num-warnings");

				if (count != null && !count.isNull()) {
					Assert.assertEquals("Incorrect number of warnings", count.asInt(), results.getMetrics().getNumberOfWarnings());
				}

				count = node.get("num-errors");

				if (count != null && !count.isNull()) {
					Assert.assertEquals("Incorrect number of errors", count.asInt(), results.getMetrics().getNumberOfErrors());
				}

				count = node.get("num-ignored");

				if (count != null && !count.isNull()) {
					Assert.assertEquals("Incorrect number ignored", count.asInt(), results.getMetrics().getNumberIgnored());
				}
			}

			File assertionFile = new FileBuilder(intRoot).subdir("assertions").subdir("files").file();

			if (assertionFile.exists()) {
				File generSourcesAssertion = new FileBuilder(assertionFile).subdir("generated_src").file();

				if (generSourcesAssertion.exists()) {
					deepCompare(generSourcesAssertion, genSrc);
				}

				File testAssertion = new FileBuilder(assertionFile).subdir("test_src").file();

				if (testAssertion.exists()) {
					deepCompare(testAssertion, src);
				}
			}

			return results;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String testSpecification() {
		return null;
	}

	protected void assertions(TestResults results, int pass) throws Exception {
	}

	protected void prepareTestContext() {
	}

	/**
	 * Recusively compare the assertionDir to the actualDir.
	 *
	 * @param assertionDir
	 * @param actualDir
	 */
	private void deepCompare(File assertionDir, final File actualDir) {
		assertionDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				//  check for a corresponding file on the actual side
				File actual = new File(actualDir, pathname.getName());

				if (!actual.exists()) {
					throw new AssertionError("Assertion file does not exist on the actual side: " + pathname);
				}

				if (pathname.isDirectory()) {
					if (actual.isDirectory()) {
						deepCompare(pathname, actual);
					} else {
						throw new AssertionError("Assertion file is a directory, but the actual is not: " + pathname);
					}
				} else if (pathname.isFile()) {
					if (actual.isFile()) {
						// compare files
						try {
							// before comparing, copy and process as a velocity template so the assertion files can
							// use symbolic references
							String template = FileUtils.readFileToString(pathname);

							String newExpected = VelocityUtil.writeTemplate(template, velocityContext);

							File tempFile = runtimeSupport.createTempFile(pathname.getName() + ".VELTEMP");

							try {
								FileUtils.write(tempFile, newExpected);

								LineIterator liactu = FileUtils.lineIterator(actual);

								try {
									LineIterator liexp = FileUtils.lineIterator(tempFile);

									try {
										int lineNo = 0;

										while (liactu.hasNext() && liexp.hasNext()) {
											lineNo++;

											String act = liactu.next();
											String exp = liexp.next();

											if (!act.equals(exp)) {
												throw new AssertionError(
														String.format(
																"Files do not match.  Expected '%s', actual '%s':  Line [%d] expected '%s'; actual '%s'",
																pathname.getName(),
																actual.getName(),
																lineNo,
																exp,
																act
														)
												);
											}
										}

										if (liactu.hasNext()) {
											throw new AssertionError(
													String.format(
															"Files do not match.  Expected '%s', actual '%s':  Actual has more lines beyond [%d]",
															pathname.getName(),
															actual.getName(),
															lineNo
													)
											);
										}

										if (liexp.hasNext()) {
											throw new AssertionError(
													String.format(
															"Files do not match.  Expected '%s', actual '%s':  Expected has more lines beyond [%d]",
															pathname.getName(),
															actual.getName(),
															lineNo
													)
											);
										}
									} finally {
										liexp.close();
									}
								} finally {
									liactu.close();
								}
							} finally {
								tempFile.delete();
							}
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					} else {
						throw new AssertionError("Assertion file is a file, but the actual is not: " + pathname);
					}
				}

				return false;
			}
		});
	}

	protected void prepareTest() {
	}

	protected void postTest() {
	}

	protected void inspectTestRootPreTests() {
	}

	enum scan_type {
		src,
		feature_src
	}

	private void deepscanSrc(File intRoot, final ETLTestPackage package_, final String path, final scan_type stype) {
		intRoot.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				if (pathname.isDirectory()) {
					PackNameExpression pne = new PackNameExpression(pathname.getName());

					if (pne.matches()) {
						// if we are in a path, this is illegal
						if (path != null) {
							throw new IllegalStateException("Cannot nest a package within a path: " + pathname.getAbsolutePath());
						}

						// this is a package - recurse into
						deepscanSrc(pathname,
								package_.getSubPackage(pne.getPackName()),
								path,
								stype);
					} else {
						// recurse into this without looking for packages
						deepscanSrc(pathname,
								package_,
								path == null ? pathname.getName() : (path + "/" + pathname.getName()),
								stype);
					}
				} else {
					try {
						// process this file to the correct location
						switch (stype) {
							case src:
								if (path != null) {
									createResource(package_, path, pathname.getName(), FileUtils.readFileToString(pathname));
								} else {
									createSource(package_, pathname.getName(), FileUtils.readFileToString(pathname));
								}
								break;
							case feature_src:
								// split the first directory off as the feature name
								if (path == null || path.equals("")) {
									throw new IntegrationTestException("Cannot load feature source files without a feature directory");
								}

								int index = path.indexOf("/");

								String feature = path;

								if (index != -1) {
									feature = path.substring(0, index);
								}

								String subPath = null;

								if (!feature.equals(path)) {
									subPath = path.substring(index + 1);
								}

								createFeatureSource(feature, subPath, pathname.getName(), FileUtils.readFileToString(pathname));
								break;
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				return false;
			}
		});
	}

	private void assertTrue(boolean delete) {
		if (!delete) {
			throw new IllegalStateException("");
		}
	}

	private class DebugCallbacker extends AbstractFeature {
		int operationNumber = 0;
		int testNumber = 0;

		@Override
		public StatusReporter getStatusReporter() {
			return new NullStatusReporterImpl() {
				@Override
				public void testCompleted(ETLTestMethod method, CompletionStatus status) {
					assertMethodCompletionStatus(method, status, testNumber, operationNumber);
				}
			};
		}

		@Override
		public ClassListener getListener() {
			return new NullClassListener() {
				@Override
				public void begin(ETLTestClass cl, VariableContext context, int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning {
					testNumber = 0;
					assertVariableContextPre(cl, context);
				}

				@Override
				public void begin(ETLTestMethod mt, VariableContext context, int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning {
					operationNumber = 0;
					assertVariableContextPre(mt, context, testNumber);
					assertVariableContextPre(mt, context);
					testNumber++;
				}

				@Override
				public void end(ETLTestMethod mt, VariableContext context, int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning {
					assertVariableContextPost(mt, context, testNumber, operationNumber);
					assertVariableContextPost(mt, context, operationNumber);
				}

				@Override
				public void end(ETLTestOperation op, ETLTestValueObject parameters, VariableContext vcontext, ExecutionContext econtext, int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning {
					assertVariableContextOperation(op, parameters, vcontext, operationNumber);
					operationNumber++;
				}

				@Override
				public void end(ETLTestClass cl, VariableContext context, int executor)
						throws TestAssertionFailure, TestExecutionError, TestWarning {
					assertVariableContextPost(cl, context, testNumber);
				}
			};
		}

		@Override
		public void initialize(Injector inj) {
			inj.injectMembers(BaseIntegrationTest.this);
		}

		@Override
		public String getFeatureName() {
			return "debug-caller";
		}

		@Override
		public long getPriorityLevel() {
			return Long.MAX_VALUE;
		}
	}

	protected final void createVendorConfig(String vendorName, String name, String content) throws IOException {
		File file = new FileBuilder(runtimeSupport.getFeatureConfigurationDirectory(vendorName)).name(name).file();

		try {
			FileUtils.write(file, VelocityUtil.writeTemplate(content, velocityContext));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected final void createTemp(String name, String path, String content) throws IOException {
		FileBuilder file = new FileBuilder(runtimeSupport.getTempDirectory());

		if (path != null) {
			file = file.subdir(path).mkdirs();
		}
		file.name(name);

		try {
			FileUtils.write(file.file(), VelocityUtil.writeTemplate(content, velocityContext));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected final void createTemp(String name, String content) throws IOException {
		createTemp(name, null, content);
	}

	protected final void createGeneratedSource(String feature, String name, String content) throws IOException {
		createGeneratedSource(feature, null, name, content);
	}

	protected final File getGeneratedSource(String feature, String subdir, String name) throws IOException {
		FileBuilder fb = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory(feature));
		if (subdir != null) {
			fb = fb.subdir(subdir).mkdirs();
		}

		fb = fb.name(name);

		return fb.file();
	}

	protected final void createGeneratedSource(String feature, String subdir, String name, String content)
			throws IOException {
		try {
			FileUtils.write(getGeneratedSource(feature, subdir, name), VelocityUtil.writeTemplate(content, velocityContext));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected final void createSource(String name, String content) throws IOException {
		createSource(null, name, content);
	}

	protected final void createSource(ETLTestPackage package_, String name, String content) throws IOException {
		try {
			FileUtils.write(getSource(package_, name), VelocityUtil.writeTemplate(content, velocityContext));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected final File getSource(ETLTestPackage package_, String name) throws IOException {
		FileBuilder fb = new FileBuilder(
				package_ != null ? runtimeSupport.getTestSourceDirectory(package_) : runtimeSupport.getTestSourceDirectory()
		);

		File dir = fb.mkdirs().file();

		File file = new File(dir, name);

		return file;
	}

	protected final void createFeatureSource(String feature, String name, String content) throws IOException {
		createFeatureSource(feature, null, name, content);
	}

	protected final void createFeatureSource(String feature, String path, String name, String content) throws IOException {
		try {
			FileUtils.write(getFeatureSource(feature, path, name), VelocityUtil.writeTemplate(content, velocityContext));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected final File getFeatureSource(String feature, String path, String name) throws IOException {
		FileBuilder fb = new FileBuilder(runtimeSupport.getFeatureSourceDirectory(feature));

		if (path != null) {
			fb = fb.subdir(path);
		}

		File dir = fb.mkdirs().file();

		File file = new File(dir, name);

		return file;
	}

	protected final void createResource(ETLTestPackage _package, String relativePath, String name, String content)
			throws IOException {
		File
				featSource =
				new FileBuilder(runtimeSupport.getTestResourceDirectory(_package, relativePath)).mkdirs().file();

		File file = new File(featSource, name);

		try {
			FileUtils.write(file, VelocityUtil.writeTemplate(content, velocityContext));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected void assertMethodCompletionStatus(ETLTestMethod method, StatusReporter.CompletionStatus status, int testNumber, int operationNumber) {
	}

	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, int operationNumber) {
	}

	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context) {
	}

	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context, int methodNumber) {
	}

	protected void assertVariableContextPre(ETLTestClass cl, VariableContext context)
	{
	}

	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int methodNumber, int operationsProcessed)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed)
			throws TestAssertionFailure, TestExecutionError, TestWarning
	{
	}

	protected void assertVariableContextPost(ETLTestClass cl, VariableContext context, int testsProcessed) {
	}

	/**
	 * If true, the test will be run twice, once with a single executor and once with multiple.
	 *
	 * @return
	 */
	protected boolean multiPassSafe() {
		return true;
	}

	protected int getExecutorCount() {
		return executorCount;
	}

	protected void initializeTests() {
	}
}
