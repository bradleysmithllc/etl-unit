package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileProducer;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

public class OptionsIntegrationTest extends BaseIntegrationTest
{
	private FileRuntimeSupport fileRuntimeSupport;

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport support)
	{
		fileRuntimeSupport = support;
		fileRuntimeSupport.registerFileProducer(new FileProducer()
		{
			public String getName()
			{
				return "testProducer";
			}

			public File locateFile(String reference, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, null, null, vcontext);
			}

			public File locateFileWithClassifier(String reference, String classifier, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, classifier, null, vcontext);
			}

			public File locateFileWithContext(String reference, String context, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, null, context, vcontext);
			}

			public File locateFileWithClassifierAndContext(String reference, String classifier, String context, VariableContext vcontext)
			{
				String fileName = reference + "_" + classifier + "_" + context + ".txt";

				return new File(tmp, fileName);
			}
		});
	}

	@Override
	protected void prepareTest()
	{
		try
		{
			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("TEST", null), new StringBuffer("YYYYMMDIPOS_TYPE...........<"));
		}
		catch (IOException e)
		{
			Assert.fail(e.toString());
		}
	}

	private static final Object[][] tests = {
			{
					"TEST_NO_DIFF",
					"ASSERT_TEST_NO_DIFF",
					DataFileSchema.format_type.fixed
			},
			{
					"TEST_EXCLUDE",
					"ASSERT_TEST_EXCLUDE",
					DataFileSchema.format_type.fixed
			},
			{
					"TEST_INCLUDE",
					"ASSERT_TEST_INCLUDE",
					DataFileSchema.format_type.fixed
			},
			{
					"TEST_INFERRED",
					"ASSERT_TEST_INFERRED",
					DataFileSchema.format_type.fixed
			},
			{
					"TEST_SQL",
					"ASSERT_TEST_SQL",
					DataFileSchema.format_type.fixed
			},
			{
					"TEST_SQL_POLY",
					"ASSERT_TEST_SQL_POLY",
					DataFileSchema.format_type.delimited
			},
			{
					"TEST_SQL_FULL",
					"ASSERT_TEST_SQL_FULL",
					DataFileSchema.format_type.fixed
			}
	};

	@Test
	public void testFileFeature() throws IOException
	{
		startTest();

		for (Object[] spec : tests)
		{
			if (true) continue;
			File actualAssertionFile = fileRuntimeSupport.getAssertionFile((String) spec[0], (DataFileSchema.format_type) spec[2]);
			File expectedAssertionFile = fileRuntimeSupport.getAssertionFile((String) spec[1], (DataFileSchema.format_type) spec[2]);

			Assert.assertTrue((String) spec[0], actualAssertionFile.exists());
			Assert.assertTrue((String) spec[1], expectedAssertionFile.exists());

			Assert.assertEquals((String) spec[0], FileUtils.readFileToString(expectedAssertionFile), FileUtils.readFileToString(actualAssertionFile));
		}
	}
}
