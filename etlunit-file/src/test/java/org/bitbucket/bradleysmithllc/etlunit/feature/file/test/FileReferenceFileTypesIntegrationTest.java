package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileProducer;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

public class FileReferenceFileTypesIntegrationTest extends FileTypesIntegrationTest
{
	private FileRuntimeSupport fileRuntimeSupport;

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport support)
	{
		fileRuntimeSupport = support;
		fileRuntimeSupport.registerFileProducer(new FileProducer()
		{
			public String getName()
			{
				return "testProducer";
			}

			public File locateFile(String reference, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, null, null, vcontext);
			}

			public File locateFileWithClassifier(String reference, String classifier, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, classifier, null, vcontext);
			}

			public File locateFileWithContext(String reference, String context, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, null, context, vcontext);
			}

			public File locateFileWithClassifierAndContext(String reference, String classifier, String context, VariableContext vcontext)
			{
				String fileName = reference + "_" + classifier + "_" + context + ".txt";

				return new File(tmp, fileName);
			}
		});
	}

	@Override
	protected void prepareTest()
	{
		try
		{
			makeTestFile("test_base", "YYYYMMDDPOS_TYPE...........<");
			makeTestFile("test_base_ref", "YYYYMMDD");
			makeTestFile("alpha_all", "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

			// Test files for use with the reference file testing
			makeTestFile("a");
			makeTestFile("a_cp");
			makeTestFile("at");
			makeTestFile("af");
			makeTestFile("ag");
			makeTestFile("ga");
			makeTestFile("az");
			makeTestFile("av-cls");
			makeTestFile("av-vrs");
			makeTestFile("av-cls-vrs");
			makeTestFile("a_remote");
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private void makeTestFile(String a) throws IOException
	{
		makeTestFile(a, "A");
	}

	private void makeTestFile(String a, String content) throws IOException
	{
		IOUtils.writeBufferToFile(
				fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile(a, null),
				new StringBuffer(content)
		);
	}

	@Test
	public void testFileFeature()
	{
		startTest();
	}
}
