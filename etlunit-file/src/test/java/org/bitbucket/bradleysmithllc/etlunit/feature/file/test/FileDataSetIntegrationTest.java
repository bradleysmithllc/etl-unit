package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileProducer;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileStageHandler;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileDataSetIntegrationTest extends BaseIntegrationTest
{
	private FileRuntimeSupport fileRuntimeSupport;

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport support)
	{
		fileRuntimeSupport = support;
		fileRuntimeSupport.registerFileProducer(new FileProducer()
		{
			public String getName()
			{
				return "testProducer";
			}

			public File locateFile(String reference, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, null, null, vcontext);
			}

			public File locateFileWithClassifier(String reference, String classifier, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, classifier, null, vcontext);
			}

			public File locateFileWithContext(String reference, String context, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, null, context, vcontext);
			}

			public File locateFileWithClassifierAndContext(String reference, String classifier, String context, VariableContext vcontext)
			{
				String fileName = reference + "_" + classifier + "_" + context + ".txt";

				File file = new File(tmp, fileName);
				try
				{
					FileUtils.write(file, "FNDFNDFNPOSPOSPOSPOSPOSPOSPO");
				}
				catch (IOException e)
				{
					throw new RuntimeException(e);
				}

				return file;
			}
		});
	}

	@Test
	public void testFileFeature()
	{
		startTest();
	}

	@Override
	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, int operationNumber)
	{
		if (op.getTestMethod().getName().equals("dataset"))
		{
			ETLTestValueObject query = obj.query("data-set-name");
			Assert.assertNotNull(query);

			Assert.assertEquals("ds_stage", query.getValueAsString());

			// verify that file0 is staged
			//ETLTestValueObject fcontext = FileFeatureModule.getFileContext(obj, context);

			String file0Key = FileStageHandler.makeDebugContextKey("file0", null, null, null, "file0");
			Assert.assertNotNull(context.query(file0Key));

			String file1Key = FileStageHandler.makeDebugContextKey("file1", null, "var1", null, "file1");
			Assert.assertNotNull(context.query(file1Key));
			Assert.assertEquals("file1", context.query("var1").getValueAsString());

			String file2Key = FileStageHandler.makeDebugContextKey("file2", null, "var2", "context2", "filebud");
			Assert.assertNotNull(context.query(file2Key));
			Assert.assertEquals("filebud", context.query("var2").getValueAsString());

			String file3Key = FileStageHandler.makeDebugContextKey("file3", null, null, "context3", "file3");
			Assert.assertNotNull(context.query(file3Key));

			String file4Key = FileStageHandler.makeDebugContextKey("file4", "classi4", "var4", "context4", "file4");
			Assert.assertNotNull(context.query(file4Key));
			Assert.assertEquals("file4", context.query("var4").getValueAsString());

			String file5Key = FileStageHandler.makeDebugContextKey("file5", "classii5", null, "context5", "file5");
			Assert.assertNotNull(context.query(file5Key));

			String file6Key = FileStageHandler.makeDebugContextKey("file6", "classiii6", "var6", null, "file6");
			Assert.assertNotNull(context.query(file6Key));
			Assert.assertEquals("file6", context.query("var6").getValueAsString());

			String file7Key = FileStageHandler.makeDebugContextKey("file7", "classiv7", null, null, "filebud7");
			Assert.assertNotNull(context.query(file7Key));

			// in every case this should be a list
			//Assert.assertNotNull(fcontext);
			//Assert.assertEquals(ETLTestValueObject.value_type.list, fcontext.getValueType());

//			List<ETLTestValueObject> valueAsList = fcontext.getValueAsList();
		}
	}
}
