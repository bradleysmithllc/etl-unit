package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileConstants;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileProducer;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

public class FileAssertionIntegrationTest extends BaseIntegrationTest
{
	private FileRuntimeSupport fileRuntimeSupport;

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport support)
	{
		fileRuntimeSupport = support;
		fileRuntimeSupport.registerFileProducer(new FileProducer()
		{
			public String getName()
			{
				return "testProducer";
			}

			public File locateFile(String reference, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, null, null, vcontext);
			}

			public File locateFileWithClassifier(String reference, String classifier, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, classifier, null, vcontext);
			}

			public File locateFileWithContext(String reference, String context, VariableContext vcontext)
			{
				return locateFileWithClassifierAndContext(reference, null, context, vcontext);
			}

			public File locateFileWithClassifierAndContext(String reference, String classifier, String context, VariableContext vcontext)
			{
				String fileName = reference + "_" + classifier + "_" + context + ".txt";

				return new File(tmp, fileName);
			}
		});
	}

	@Override
	protected void prepareTest()
	{
		try
		{
			File testProducerFile = fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("delimited_target_sample.out", null);
			FileUtils.write(testProducerFile, "#POS_NAME|INPUT|ISNUM|ISINT|ISUNSIGNEDINT|ISSMALLIINT|ISUNSIGNEDSMALLIINT|INT_OUTPUT|UNSIGNEDINT_OUTPUT|SMALLINT_OUTPUT|UNSIGNEDSMALLINT_OUTPUT|NUM_OUTPUT\n" +
					"Ordermatic|1 11|N|N|N|N|N|||||\n" +
					"Ordermatic|05|Y|Y|Y|Y|Y|5|5|5|5|5.00\n" +
					"Ordermatic|5|Y|Y|Y|Y|Y|5|5|5|5|5.00\n" +
					"Ordermatic|0.01|Y|N|N|N|N|||||0.01");

			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("TEST_BASE_BCP", null), new StringBuffer("YYYYMMDIPOS_TYPE...........<"));

			File file = fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("TEST_BASE", null);
			IOUtils.writeBufferToFile(file, new StringBuffer("YYYYMMDIPOS_TYPE...........<"));

			File copy = fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("TEST_BASE_II", null);
			FileUtils.copyFile(file, copy);

			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("TEST_PASS_CLCI", null), new StringBuffer("YYYYMMDDPOS_TYPE...........<"));
			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("TEST_PASS", null), new StringBuffer("YYYYMMDDPOS_TYPE...........<"));
			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("TEST_PASS_POS", null), new StringBuffer("POS_TYPE...........<"));

			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("EXISTS", null), new StringBuffer("1"));
			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("EMPTY", null), new StringBuffer(""));
			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("EMPTY_WITH_REF", null), new StringBuffer("#BLAH|DATA"));
			IOUtils.writeBufferToFile(fileRuntimeSupport.getRegisteredProducer("testProducer").locateFile("TEST_BASE_assert_assertWithRefs_file_refs_[default]", null), new StringBuffer("#BLAH|DATA"));
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testFileFeature()
	{
		startTest();
	}

	@Override
	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, int operationNumber)
	{
		if (op.getQualifiedName().equals("file_assert_test_reference_order.testImplicitMatchToMasterFileName.assert.0"))
		{
			String schema = context.getValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA).getValueAsString();
			Assert.assertEquals("test_base.fml", schema);
		}
		else if (op.getQualifiedName().equals("file_assert_test_reference_order.testExplicitMatch.assert.0"))
		{
			String schema = context.getValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA).getValueAsString();
			Assert.assertEquals("test-specific.fml", schema);
		}
	}
}
