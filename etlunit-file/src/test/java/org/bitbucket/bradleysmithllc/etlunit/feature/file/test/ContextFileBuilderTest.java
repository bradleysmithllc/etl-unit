package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.file.ContextFile;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.ContextFileBuilder;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;

public class ContextFileBuilderTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void defaultF()
	{
		File f = new File("fi");
		ContextFile cf = new ContextFileBuilder(f, "qwerty").create();

		Assert.assertEquals(f, cf.getFile());
		Assert.assertEquals("qwerty", cf.getFileName());
		Assert.assertEquals("qwerty", cf.getDestinationName());
	}

	@Test
	public void context()
	{
		File f = new File("fi");
		ContextFile cf = new ContextFileBuilder(f, "qwerty").withContextName("cn").create();

		Assert.assertEquals("cn", cf.getContextName());
	}

	@Test
	public void classifier()
	{
		File f = new File("fi");
		ContextFile cf = new ContextFileBuilder(f, "qwerty").withClassifier("cl").create();

		Assert.assertEquals("cl", cf.getClassifierName());
	}

	@Test
	public void variableName()
	{
		File f = new File("fi");
		ContextFile cf = new ContextFileBuilder(f, "qwerty").withVariableName("vn").create();

		Assert.assertEquals("vn", cf.getVariableName());
	}

	@Test
	public void destinationName()
	{
		File f = new File("fi");
		ContextFile cf = new ContextFileBuilder(f, "qwerty").withDestinationName("dn").create();

		Assert.assertEquals("dn", cf.getDestinationName());
		Assert.assertEquals("qwerty", cf.getFileName());
	}

	@Test
	public void revertDestinationNameWithNull()
	{
		File f = new File("fi");
		ContextFileBuilder contextFileBuilder = new ContextFileBuilder(f, "qwerty").withDestinationName("dn");

		ContextFile cf = contextFileBuilder.create();

		Assert.assertEquals("dn", cf.getDestinationName());
		Assert.assertEquals("qwerty", cf.getFileName());

		cf = contextFileBuilder.withDestinationName(null).create();

		Assert.assertEquals("qwerty", cf.getDestinationName());
		Assert.assertEquals("qwerty", cf.getFileName());
	}

	@Test
	public void nullFilePath()
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Cannot assign null to file path");
		new ContextFileBuilder(null, "qwerty");
	}

	@Test
	public void nullFileName()
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Cannot assign null to file name");
		new ContextFileBuilder(new File("."), null);
	}
}
