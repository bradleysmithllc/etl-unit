package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class DataFileSchemaJsonTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void testScaleSerialized() throws IOException
	{
		File schema = temporaryFolder.newFile();
		FileUtils.write(schema, "{\"flat-file\":{\"format-type\":\"fixed\",\"row-delimiter\":\"\\n\"" +
				",\"null-token\":\"null\",\"columns\":[{\"id\":\"pos_type\",\"length\":20,\"scale\":10,\"basic-type\":\"string\",\"type\":\"VARCHAR\"}],\"orderBy\": [\"pos_type\"],\"primaryKey\": [\"pos_type\"]}}");

		DataFileManager dfm = new DataFileManagerImpl(temporaryFolder.newFolder());

		DataFileSchema ffs = dfm.loadDataFileSchema(schema, "id");

		// serialize and test
		String json = ffs.toJsonString();

		JsonNode jsonNodeAct = JsonLoader.fromString(json);
		JsonNode jsonNodeExp = JsonLoader.fromFile(schema);

		Assert.assertEquals("", jsonNodeExp, jsonNodeAct);
	}
}
