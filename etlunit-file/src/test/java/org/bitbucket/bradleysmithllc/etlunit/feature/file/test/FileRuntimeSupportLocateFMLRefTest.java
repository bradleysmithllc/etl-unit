package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupportImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.*;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeRef;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

public class FileRuntimeSupportLocateFMLRefTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private RuntimeSupport runtimeSupport;
	private FileRuntimeSupport fileRuntimeSupport;

	private File classpathRoot;
	private File remoteUrlRoot;

	/**
	 * Create a tree:
	 * [default]
	 * - pack1
	 * - subpack1
	 * - pack2
	 * - subpack2
	 */
	private ETLTestPackage defaultPackage;

	private ETLTestPackage package1;
	private ETLTestPackage subPackage1;

	private ETLTestPackage package2;
	private ETLTestPackage subPackage2;
	private ETLTestClass testClassPack1;
	private ETLTestClass testClassPack2;
	private ETLTestClass testClassSubPack1;
	private ETLTestClass testClassSubPack2;
	private MapLocal mapLocal;

	@Before
	public void setupSupport() throws IOException, ParseException
	{
		BasicRuntimeSupport brs = new BasicRuntimeSupport(temporaryFolder.newFolder());
		brs.setApplicationLogger(new PrintWriterLog());

		runtimeSupport = brs;

		FileRuntimeSupportImpl fimpl = new FileRuntimeSupportImpl();
		fimpl.receiveRuntimeSupport(runtimeSupport);
		fimpl.receiveApplicationLog(runtimeSupport.getApplicationLog());
		fimpl.receiveDataFileManager(new DataFileManagerImpl(runtimeSupport.createAnonymousTempFolder()));
		fimpl.receiveReferenceFileTypeManager(new ReferenceFileTypeManagerImpl());

		fileRuntimeSupport = fimpl;

		// add some defaults
		FileFeatureModuleConfiguration ffmc = new FileFeatureModuleConfiguration();

		ReferenceFileTypes referenceFileTypes = new ReferenceFileTypes();
		ffmc.setReferenceFileTypes(referenceFileTypes);

		ReferenceFileTypesProperty value = new ReferenceFileTypesProperty();
		value.setDefaultVersion("VERS2");
		referenceFileTypes.getAdditionalProperties().put("typeWithDefault", value);

		// set a default for the type
		value = new ReferenceFileTypesProperty();
		value.setDefaultVersion("VERS2");
		value.setDefaultClassifier("CLS");
		referenceFileTypes.getAdditionalProperties().put("typeWithDefault2", value);

		value = new ReferenceFileTypesProperty();
		value.setDefaultClassifier("CLS_II");
		referenceFileTypes.getAdditionalProperties().put("typeWithDefault3", value);

		// now set a default for the type with classifier
		Classifiers classifiers = new Classifiers();
		ClassifiersProperty classifiersProperty = new ClassifiersProperty();
		classifiers.getAdditionalProperties().put("UNK", classifiersProperty);
		classifiersProperty.setDefaultVersion("VERS1");
		value.setClassifiers(classifiers);

		ObjectMapper om = new ObjectMapper();

		ffmc = om.readValue(
				IOUtils.toString(getClass().getClassLoader().getResource("fileRuntimeSupportLocateFMLRefTestConfig.json")),
				FileFeatureModuleConfiguration.class
		);

		fimpl.receiveFileFeatureModuleConfiguration(ffmc);

		mapLocal = new MapLocal();
		defaultPackage = ETLTestPackageImpl.getDefaultPackage();
		package1 = defaultPackage.getSubPackage("pack1");
		package2 = defaultPackage.getSubPackage("pack2");

		subPackage1 = package1.getSubPackage("subpack1");
		subPackage2 = package2.getSubPackage("subpack2");

		testClassPack1 = ETLTestParser.load("class test { @Test test() {}}", package1).get(0);
		testClassPack2 = ETLTestParser.load("class test { @Test test() {}}", package2).get(0);
		testClassSubPack1 = ETLTestParser.load("class test { @Test test() {}}", subPackage1).get(0);
		testClassSubPack2 = ETLTestParser.load("class test { @Test test() {}}", subPackage2).get(0);

		mapLocal.setCurrentlyProcessingTestClass(testClassPack1);
		brs.setMapLocal(mapLocal);

		classpathRoot = temporaryFolder.newFolder();
		remoteUrlRoot = new FileBuilder(classpathRoot).subdir("reference").subdir("file").subdir("fml").mkdirs().file();

		URLClassLoader loader = new URLClassLoader(new URL[]{classpathRoot.toURL()});
		Thread.currentThread().setContextClassLoader(loader);
	}

	@Test
	public void notFound() throws TestExecutionError
	{
		Assert.assertEquals(FileRuntimeSupport.fmlSearchType.notFound, fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("file"), null).getFmlSearchType());
	}

	@Test
	public void idLocalFoundInSubPackage() throws IOException, TestExecutionError
	{
		createLocalFml(package1, "file");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("file"), null);
		Assert.assertEquals(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult.getFmlSearchType());
	}

	@Test
	public void idLocalFoundInDefaultPackage() throws IOException, TestExecutionError
	{
		createLocalFml(defaultPackage, "file");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("file"), null);
		Assert.assertEquals(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult.getFmlSearchType());
	}

	@Test
	public void idRemoteFoundInSubPackage() throws IOException, TestExecutionError
	{
		createRemoteFml(package1, "file");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("file"), null);
		Assert.assertEquals(FileRuntimeSupport.fmlSearchType.remoteOnly, schemaQueryResult.getFmlSearchType());
	}

	@Test
	public void idRemoteFoundInDefaultPackage() throws IOException, TestExecutionError
	{
		createRemoteFml(defaultPackage, "file");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("file"), null);
		Assert.assertEquals(FileRuntimeSupport.fmlSearchType.remoteOnly, schemaQueryResult.getFmlSearchType());
	}

	@Test
	public void idWithClassifierLocalFoundInSubPackage() throws IOException, TestExecutionError
	{
		createLocalFml(package1, "file");
		createLocalFml(package1, "file~CLS");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndClassifier("file", "CLS"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "file~CLS");
	}

	@Test
	public void idWithClassifierLocalFoundInDifferentPackages() throws IOException, TestExecutionError
	{
		createLocalFml(package1, "file");
		createLocalFml(defaultPackage, "file~CLS");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndClassifier("file", "CLS"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "file~CLS");
	}

	@Test
	public void idWithClassifierRemoteFoundInSubPackage() throws IOException, TestExecutionError
	{
		createLocalFml(package1, "file");
		createRemoteFml(package1, "file~cls");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndClassifier("file", "cls"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.remoteOnly, schemaQueryResult, "file~cls");
	}

	@Test
	public void idWithClassifierRemoteFoundInDifferentPackages() throws IOException, TestExecutionError
	{
		createLocalFml(package1, "file");
		createRemoteFml(defaultPackage, "file~cls");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndClassifier("file", "cls"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.remoteOnly, schemaQueryResult, "file~cls");
	}

	@Test
	public void idWithVersionLocalFoundInDifferentPackages() throws IOException, TestExecutionError
	{
		createLocalFml(package1, "file");
		createLocalFml(defaultPackage, "file+VERS");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndVersion("file", "VERS"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "file+VERS");
	}

	@Test
	public void idWithVersionRemoteFoundInSubPackage() throws IOException, TestExecutionError
	{
		createLocalFml(package1, "file");
		createRemoteFml(package1, "file+vers");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndVersion("file", "vers"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.remoteOnly, schemaQueryResult, "file+vers");
	}

	@Test
	public void idWithVersionRemoteFoundInDifferentPackages() throws IOException, TestExecutionError
	{
		createLocalFml(package1, "file");
		createRemoteFml(defaultPackage, "file+vers");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndVersion("file", "vers"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.remoteOnly, schemaQueryResult, "file+vers");
	}

	@Test
	public void idWithDefaultVersion() throws IOException, TestExecutionError
	{
		createLocalFml(defaultPackage, "typewithdefault+vers1");
		createRemoteFml(defaultPackage, "typewithdefault+vers2");
		createLocalFml(defaultPackage, "typewithdefault+vers3");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("typewithdefault"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.remoteOnly, schemaQueryResult, "typewithdefault+vers2");
	}

	@Test
	public void idWithClassifierAndDefaultVersion() throws IOException, TestExecutionError
	{
		createLocalFml(defaultPackage, "typeWithDefault3~UNK+VERS1");
		createRemoteFml(defaultPackage, "typeWithDefault3~UNK+VERS2");
		createLocalFml(defaultPackage, "typeWithDefault3~UNK+VERS3");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndClassifier("typeWithDefault3", "UNK"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "typeWithDefault3~UNK+VERS1");
	}

	@Test
	public void defaultRemoteNoLocal() throws IOException, TestExecutionError
	{
		createLocalFml(defaultPackage, "defaultRemoteNoLocal+VERSD");
		createLocalFml(defaultPackage, "defaultRemoteNoLocal");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.remoteRefWithId("defaultRemoteNoLocal"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "defaultRemoteNoLocal+VERSD");

		schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("defaultRemoteNoLocal"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "defaultRemoteNoLocal");
	}

	@Test
	public void defaultLocalNoRemote() throws IOException, TestExecutionError
	{
		createLocalFml(defaultPackage, "defaultLocalNoRemote+REM");
		createLocalFml(defaultPackage, "defaultLocalNoRemote");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.remoteRefWithId("defaultLocalNoRemote"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "defaultLocalNoRemote");

		schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("defaultLocalNoRemote"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "defaultLocalNoRemote+REM");
	}

	@Test
	public void defaultLocalAndRemote() throws IOException, TestExecutionError
	{
		createLocalFml(defaultPackage, "defaultLocalAndRemote+VERSB");
		createLocalFml(defaultPackage, "defaultLocalAndRemote+VERSA");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.remoteRefWithId("defaultLocalAndRemote"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "defaultLocalAndRemote+VERSB");

		schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithId("defaultLocalAndRemote"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "defaultLocalAndRemote+VERSA");
	}

	@Test
	public void idWithDefaultClassifier() throws IOException, TestExecutionError
	{
		createLocalFml(defaultPackage, "typeWithDefault3~CLS_II+VERS4");
		createRemoteFml(defaultPackage, "typeWithDefault3+VERS4");
		createLocalFml(defaultPackage, "typeWithDefault3~CLS_II");
		FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult = fileRuntimeSupport.locateReferenceFileType(ReferenceFileTypeRef.refWithIdAndVersion("typeWithDefault3", "VERS4"), null);
		assertIdAndType(FileRuntimeSupport.fmlSearchType.localOnly, schemaQueryResult, "typeWithDefault3~CLS_II+VERS4");
	}

	private void assertIdAndType(FileRuntimeSupport.fmlSearchType localOnly, FileRuntimeSupport.DataFileSchemaQueryResult schemaQueryResult, String id)
	{
		Assert.assertEquals(localOnly, schemaQueryResult.getFmlSearchType());
		Assert.assertEquals(id.toLowerCase(), schemaQueryResult.getDataFileSchema().getId());
	}

	private void createRemoteFml(ETLTestPackage pack, String file) throws IOException
	{
		FileBuilder dfs = new FileBuilder(remoteUrlRoot);

		if (!pack.isDefaultPackage())
		{
			for (String p : pack.getPackagePath())
			{
				dfs.subdir(p);
			}
		}

		dfs.mkdirs().name(file + ".fml");
		createFml(dfs.file());
	}

	private void createLocalFml(ETLTestPackage pack, String file) throws IOException
	{
		File dfs = fileRuntimeSupport.getReferenceFileSchema(pack, file);
		createFml(dfs);
	}

	private void switchToPackage(ETLTestClass cls)
	{
		mapLocal.setCurrentlyProcessingTestClass(cls);
	}

	private void createFml(File dir) throws IOException
	{

		FileUtils.write(dir, "{\n" +
				"\t\"flat-file\":\n" +
				"\t{\n" +
				"\t\t\"format-type\": \"fixed\",\n" +
				"\t\t\"row-delimiter\": \"\\n\",\n" +
				"\t\t\"columns\":\n" +
				"\t\t[\n" +
				"\t\t\t{\"id\": \"FILE_NAME_DATE\",  \"length\": 8},\n" +
				"\t\t\t{\"id\": \"POS_TYPE\", \"length\": 20}\n" +
				"\t\t]\n" +
				"\t}\n" +
				"}");
	}
}
