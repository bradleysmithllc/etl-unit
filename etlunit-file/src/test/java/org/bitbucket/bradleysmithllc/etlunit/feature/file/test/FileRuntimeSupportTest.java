package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupportImpl;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.RequestedFmlNotFoundException;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchemaImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeRef;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

public class FileRuntimeSupportTest {
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void queryForTypeAllNull() throws ParseException {
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(null, null));
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(null, FileRuntimeSupport.schemaType.source));
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(null, FileRuntimeSupport.schemaType.target));
	}

	@Test
	public void queryForTypeNull() throws ParseException {
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(ETLTestParser.loadBareObject(""), null).getQuery());
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(ETLTestParser.loadBareObject(""), FileRuntimeSupport.schemaType.source).getQuery());
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(ETLTestParser.loadBareObject(""), FileRuntimeSupport.schemaType.target).getQuery());
	}

	@Test
	public void queryForTypeSourceSupplied() throws ParseException {
		ETLTestValueObject parameters = ETLTestParser.loadBareObject("source-reference-file-type: 'fml'");
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(parameters, null).getQuery());
		Assert.assertEquals(FileRuntimeSupportImpl.queryResult.generic, FileRuntimeSupportImpl.getQueryForType(parameters, null).getQueryResult());
		Assert.assertEquals(FileRuntimeSupportImpl.queryResult.source, FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQueryResult());
		Assert.assertEquals("fml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQuery().getValueAsString());
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.target).getQuery());
		Assert.assertEquals(FileRuntimeSupportImpl.queryResult.target, FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.target).getQueryResult());
	}

	@Test
	public void queryForTypeTargetSupplied() throws ParseException {
		ETLTestValueObject parameters = ETLTestParser.loadBareObject("target-reference-file-type: 'fml'");
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(parameters, null).getQuery());
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQuery());
		Assert.assertEquals("fml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.target).getQuery().getValueAsString());
	}

	@Test
	public void queryForTypeSourceAndTargetSupplied() throws ParseException {
		ETLTestValueObject parameters = ETLTestParser.loadBareObject("source-reference-file-type: 'sfml', target-reference-file-type: 'tfml'");
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(parameters, null).getQuery());
		Assert.assertEquals("sfml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQuery().getValueAsString());
		Assert.assertEquals("tfml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.target).getQuery().getValueAsString());
	}

	@Test
	public void queryForTypeGenericSupplied() throws ParseException {
		ETLTestValueObject parameters = ETLTestParser.loadBareObject("reference-file-type: 'gfml'");
		Assert.assertEquals("gfml", FileRuntimeSupportImpl.getQueryForType(parameters, null).getQuery().getValueAsString());
		Assert.assertEquals(FileRuntimeSupportImpl.queryResult.generic, FileRuntimeSupportImpl.getQueryForType(parameters, null).getQueryResult());
		Assert.assertEquals("gfml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQuery().getValueAsString());
		Assert.assertEquals(FileRuntimeSupportImpl.queryResult.generic, FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQueryResult());
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.target).getQuery());
	}

	@Test
	public void queryForTypeGenericAndSourceSupplied() throws ParseException {
		ETLTestValueObject parameters = ETLTestParser.loadBareObject("reference-file-type: 'gfml', source-reference-file-type: 'sfml'");
		Assert.assertEquals("gfml", FileRuntimeSupportImpl.getQueryForType(parameters, null).getQuery().getValueAsString());
		Assert.assertEquals("sfml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQuery().getValueAsString());
		Assert.assertEquals(FileRuntimeSupportImpl.queryResult.source, FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQueryResult());
		Assert.assertNull(FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.target).getQuery());
	}

	@Test
	public void queryForTypeGenericAndTargetSupplied() throws ParseException {
		ETLTestValueObject parameters = ETLTestParser.loadBareObject("reference-file-type: 'gfml', target-reference-file-type: 'tfml'");
		Assert.assertEquals("gfml", FileRuntimeSupportImpl.getQueryForType(parameters, null).getQuery().getValueAsString());
		Assert.assertEquals("gfml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQuery().getValueAsString());
		Assert.assertEquals("tfml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.target).getQuery().getValueAsString());
	}

	@Test
	public void queryForTypeGenericAndTargetAndSourceSupplied() throws ParseException {
		ETLTestValueObject parameters = ETLTestParser.loadBareObject("reference-file-type: 'gfml', target-reference-file-type: 'tfml', source-reference-file-type: 'sfml'");
		Assert.assertEquals("gfml", FileRuntimeSupportImpl.getQueryForType(parameters, null).getQuery().getValueAsString());
		Assert.assertEquals("sfml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.source).getQuery().getValueAsString());
		Assert.assertEquals("tfml", FileRuntimeSupportImpl.getQueryForType(parameters, FileRuntimeSupport.schemaType.target).getQuery().getValueAsString());
	}

	@Test
	public void queryFRS() throws ParseException, RequestedFmlNotFoundException {
		FileRuntimeSupport frs = new FileRuntimeSupportImpl();

		ETLTestValueObject parameters = ETLTestParser.loadBareObject("reference-file-type: 'gfml', target-reference-file-type: 'tfml', source-reference-file-type: 'sfml'");
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, null));
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.source));
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.target));

		parameters = ETLTestParser.loadBareObject("target-reference-file-type: 'tfml', source-reference-file-type: 'sfml'");
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, null));
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.source));
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.target));

		parameters = ETLTestParser.loadBareObject("source-reference-file-type: 'sfml'");
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, null));
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.source));
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.target));

		parameters = ETLTestParser.loadBareObject("target-reference-file-type: 'sfml'");
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, null));
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.source));
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.target));

		parameters = ETLTestParser.loadBareObject("reference-file-type: 'tfml', source-reference-file-type: 'sfml'");
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, null));
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.source));
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.target));

		parameters = ETLTestParser.loadBareObject("reference-file-type: 'tfml', target-reference-file-type: 'sfml'");
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, null));
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.source));
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.target));

		parameters = ETLTestParser.loadBareObject("reference-file-type: 'tfml'");
		Assert.assertTrue(frs.isSchemaDefinedForCurrentTest(parameters, null));
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.source));
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.target));

		parameters = ETLTestParser.loadBareObject("");
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, null));
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.source));
		Assert.assertFalse(frs.isSchemaDefinedForCurrentTest(parameters, FileRuntimeSupport.schemaType.target));
	}

	@Test
	public void insertListsIncludeMatchNotFound() throws TestExecutionError {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Column not found: d");
		DataFileSchemaImpl.intersectLists(Arrays.asList("A", "b", "C"), Arrays.asList("d"), true);
	}

	@Test
	public void insertListsIncludeMatch() throws TestExecutionError {
		Assert.assertEquals(Arrays.asList("A"), DataFileSchemaImpl.intersectLists(Arrays.asList("A", "b", "C"), Arrays.asList("A"), true));
	}

	@Test
	public void insertListsIncludeCase() throws TestExecutionError {
		test(Arrays.asList("A"), Arrays.asList("A", "b", "C"), Arrays.asList("a"), true);
	}

	private void test(List<String> expect, List<String> reference, List<String> request, boolean inclusive) throws TestExecutionError {
		Assert.assertEquals(expect, DataFileSchemaImpl.intersectLists(new ArrayList<>(reference), new ArrayList<>(request), inclusive));
	}

	@Test
	public void insertListsExcludeMatchNotFound() throws TestExecutionError {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Invalid columns list: [d]");
		DataFileSchemaImpl.intersectLists(Arrays.asList("A", "b", "C"), Arrays.asList("d"), false);
	}

	@Test
	public void insertListsExcludeMatch() throws TestExecutionError {
		Assert.assertEquals(Arrays.asList("b", "C"), DataFileSchemaImpl.intersectLists(Arrays.asList("A", "b", "C"), Arrays.asList("A"), false));
	}

	@Test
	public void insertListsExcludeCase() throws TestExecutionError {
		Assert.assertEquals(Arrays.asList("b", "C"), DataFileSchemaImpl.intersectLists(Arrays.asList("A", "b", "C"), Arrays.asList("a"), false));
	}

	@Test
	public void insertLists2() throws TestExecutionError {
		Assert.assertEquals(
				Arrays.asList("POS_NAME", "ISNUM"),
			DataFileSchemaImpl.intersectLists(Arrays.asList("POS_NAME", "INPUT", "ISNUM"), Arrays.asList("pos_name", "isNum"), true)
		);
	}

	private final FileRuntimeSupport fileRuntimeSupport = new FileRuntimeSupportImpl();

	@Test
	public void refTypeDecodeNull() {
		Assert.assertNull(fileRuntimeSupport.resolveOperandReferenceFileType(null));
	}

	@Test
	public void refTypeDecodeBadType() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Not a reference file type");
		Assert.assertNull(fileRuntimeSupport.resolveOperandReferenceFileType(new ArrayList<String>()));
	}

	@Test
	public void refTypeDecodeSimpleString() {
		Assert.assertEquals(ReferenceFileTypeRef.refWithId("Hello"), fileRuntimeSupport.resolveOperandReferenceFileType("Hello"));
	}

	@Test
	public void refTypeDecodeMapId() {
		Map<String, String> map = new HashMap<>();

		map.put("id", "Hello");
		Assert.assertEquals(
			ReferenceFileTypeRef.refWithId("Hello"), fileRuntimeSupport.resolveOperandReferenceFileType(map));
	}

	@Test
	public void refTypeDecodeMapClassifier() {
		Map<String, String> map = new HashMap<>();

		map.put("classifier", "Hello");
		Assert.assertEquals(
			ReferenceFileTypeRef.refWithIdAndClassifier(null, "Hello"), fileRuntimeSupport.resolveOperandReferenceFileType(map));
	}

	@Test
	public void refTypeDecodeMapVersion() {
		Map<String, String> map = new HashMap<>();

		map.put("version", "Hello");
		Assert.assertEquals(
			ReferenceFileTypeRef.refWithIdAndVersion(null, "Hello"), fileRuntimeSupport.resolveOperandReferenceFileType(map));
	}

	@Test
	public void refTypeDecodeMapIdVersion() {
		Map<String, String> map = new HashMap<>();

		map.put("id", "Iddd");
		map.put("version", "Vers");
		Assert.assertEquals(
				ReferenceFileTypeRef.refWithIdAndVersion("Iddd", "Vers"), fileRuntimeSupport.resolveOperandReferenceFileType(map));
	}

	@Test
	public void refTypeDecodeMapIdClassifier() {
		Map<String, String> map = new HashMap<>();

		map.put("id", "Idd");
		map.put("classifier", "Class");
		Assert.assertEquals(
				ReferenceFileTypeRef.refWithIdAndClassifier("Idd", "Class"), fileRuntimeSupport.resolveOperandReferenceFileType(map));
	}

	@Test
	public void refTypeDecodeMapClassifierVersion() {
		Map<String, String> map = new HashMap<>();

		map.put("version", "Versi");
		map.put("classifier", "Clss");
		Assert.assertEquals(
				ReferenceFileTypeRef.refWithIdClassifierAndVersion(null, "Clss", "Versi"), fileRuntimeSupport.resolveOperandReferenceFileType(map));
	}

	@Test
	public void refTypeDecodeMapIdClassifierVersion() {
		Map<String, String> map = new HashMap<>();

		map.put("id", "Idi");
		map.put("version", "Vrsi");
		map.put("classifier", "Cls");
		Assert.assertEquals(
				ReferenceFileTypeRef.refWithIdClassifierAndVersion("Idi", "Cls", "Vrsi"), fileRuntimeSupport.resolveOperandReferenceFileType(map));
	}
}
