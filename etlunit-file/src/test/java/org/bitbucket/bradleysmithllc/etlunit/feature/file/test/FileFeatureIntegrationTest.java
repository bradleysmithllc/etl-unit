package org.bitbucket.bradleysmithllc.etlunit.feature.file.test;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.ContextFile;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

public class FileFeatureIntegrationTest extends BaseIntegrationTest
{
	private FileRuntimeSupport fileRuntimeSupport;

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport support)
	{
		fileRuntimeSupport = support;
	}

	@Test
	public void testFileFeature()
	{
		TestResults results = startTest("feature_test");

		Assert.assertEquals(1, results.getNumTestsSelected());
		Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, results.getMetrics().getNumberOfErrors());
		Assert.assertEquals(0, results.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());
	}

	@Override
	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, int operationNumber) {
		FileContext fcontext = fileRuntimeSupport.getFileContext(obj, context);

		List<ContextFile> valueAsList = fcontext.getContextFileList();

		switch (operationNumber)
		{
			case 0:
			{
				// there should be one file in the default context
				Assert.assertEquals(1, valueAsList.size());
				break;
			}
			case 1:
			{
				// there should be two files in the default context
				Assert.assertEquals(2, valueAsList.size());

				// additionally, the second element must have a context variable declared named var1
				Assert.assertEquals(context.getValue("var1").getValueAsString(), valueAsList.get(1).getFileName());
				break;
			}
			case 2:
			{
				// there should be one file in the named context
				Assert.assertEquals(1, valueAsList.size());

				// additionally, the second element must have a context variable declared named var2
				Assert.assertEquals("filebud", context.getValue("var2").getValueAsString());

				// this one has a destination-name
				Assert.assertEquals("filebud", valueAsList.get(0).getDestinationName());
				break;
			}
			case 3:
			{
				// there should be two files in the named context
				Assert.assertEquals(2, valueAsList.size());
				break;
			}
			case 4:
			{
				// there should be one file in the named context
				Assert.assertEquals(3, valueAsList.size());

				// additionally, the second element must have a context variable declared named var1
				Assert.assertEquals(context.getValue("var4").getValueAsString(), valueAsList.get(2).getFileName());

				// this one has a classifier
				Assert.assertEquals("classi", valueAsList.get(2).getClassifierName());
				break;
			}
			case 5:
			{
				// there should be four files in the default context
				Assert.assertEquals(4, valueAsList.size());

				// this one has a classifier
				Assert.assertEquals("classii", valueAsList.get(3).getClassifierName());
				break;
			}
			case 6:
			{
				// there should be three files in the named context
				Assert.assertEquals(3, valueAsList.size());

				// additionally, the second element must have a context variable declared named var1
				Assert.assertEquals(context.getValue("var6").getValueAsString(), valueAsList.get(2).getFileName());

				// this one has a classifier
				Assert.assertEquals("classiii", valueAsList.get(2).getClassifierName());
				break;
			}
			case 7:
			{
				// there should be four files in the named context
				Assert.assertEquals(4, valueAsList.size());

				// this one has a classifier
				Assert.assertEquals("classiv", valueAsList.get(3).getClassifierName());

				// this one has a destination-name
				Assert.assertEquals("filebud", valueAsList.get(3).getDestinationName());

				// create classifiers here.
				/*
				ETLTestValueObject container = FileFeatureModule.getFileContextContainer(obj, context);

				new ETLTestValueObjectBuilder(container)
					.key("destination-classifiers")
						.object()
							.key("classiv")
							.value(tmp.getAbsolutePath())
							.key("target")
							.value(new FileBuilder(tmp).subdir("2").file().getAbsolutePath())
						.endObject()
					.endObject();
				 */
				break;
			}
			case 8:
			{
				// there should be four more files in the default context
				Assert.assertEquals(8, valueAsList.size());

				Assert.assertEquals("list_file.txt", valueAsList.get(4).getFile().getName());
				Assert.assertEquals("lfile0", valueAsList.get(5).getFile().getName());
				Assert.assertEquals("lfile1", valueAsList.get(6).getFile().getName());
				Assert.assertEquals("lfile2", valueAsList.get(7).getFile().getName());
				break;
			}
			case 9:
			{
				// there should be three files in the named context
				Assert.assertEquals(4, valueAsList.size());

				Assert.assertEquals("list_file.txt", valueAsList.get(0).getFile().getName());
				Assert.assertEquals("lfile0", valueAsList.get(1).getFile().getName());
				Assert.assertEquals("lfile1", valueAsList.get(2).getFile().getName());
				Assert.assertEquals("lfile2", valueAsList.get(3).getFile().getName());

				// validate the variable names
				Assert.assertEquals("list_file.txt", context.getValue("varBase").getValueAsString());
				Assert.assertEquals("lfile0", context.getValue("varBase-2").getValueAsString());
				Assert.assertEquals("lfile1", context.getValue("varBase-3").getValueAsString());
				Assert.assertEquals("lfile2", context.getValue("varBase-4").getValueAsString());

				break;
			}
			case 10:
			{
				// there should be one file in the default context
				Assert.assertEquals(9, valueAsList.size());
				Assert.assertEquals("file01", valueAsList.get(8).getFileName());
				break;
			}
			case 11:
			{
				// there should be one file in the default context
				Assert.assertEquals(10, valueAsList.size());
				Assert.assertEquals("file01", valueAsList.get(9).getFileName());
				break;
			}
		}
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed) {
		Assert.assertEquals(12, operationsProcessed);
	}

	@Override
	protected void assertVariableContextPost(ETLTestClass cl, VariableContext context, int testsProcessed) {
		Assert.assertEquals(1, testsProcessed);
	}
}
