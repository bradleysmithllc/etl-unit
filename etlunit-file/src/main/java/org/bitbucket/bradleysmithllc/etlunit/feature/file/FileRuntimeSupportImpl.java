package org.bitbucket.bradleysmithllc.etlunit.feature.file;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.*;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeRef;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.RequestedFileTypeNotFoundException;
import org.bitbucket.bradleysmithllc.etlunit.metadata.*;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.json.validator.ResourceNotFoundException;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.sql.Ref;
import java.util.*;

public class FileRuntimeSupportImpl implements FileRuntimeSupport {
	public static final String DEFAULT = "default";
	private RuntimeSupport runtimeSupport;

	private Map<String, FileProducer> fileProducerMapLocal = new HashMap<String, FileProducer>();
	//private ThreadLocal<Map<String, FileProducer>> fileProducerMapLocal = ThreadLocal.withInitial(() -> new HashMap<String, FileProducer>());

	private DataFileManager dataFileManager;
	private DiffManager diffManager;
	private Log applicationLog;
	private MetaDataManager metaDataManager;
	private MetaDataContext fmlContext;
	private MetaDataContext classpathFmlContext;
	private FileFeatureModuleConfiguration fileFeatureModuleConfiguration;
	private ReferenceFileTypeManager referenceFileTypeManager;

	// 'shortcut' map with versions
	private Map<String, String> defaultLocalVersions = new HashMap<String, String>();
	private Map<String, String> defaultRemoteVersions = new HashMap<String, String>();
	private Map<String, String> defaultLocalClassifiers = new HashMap<String, String>();
	private Map<String, String> defaultRemoteClassifiers = new HashMap<String, String>();

	@Inject
	public void receiveReferenceFileTypeManager(ReferenceFileTypeManager manager) {
		referenceFileTypeManager = manager;
	}

	@Inject
	public void receiveFileFeatureModuleConfiguration(FileFeatureModuleConfiguration config) {
		fileFeatureModuleConfiguration = config;

		// populate the shortcut map
		ReferenceFileTypes refs = fileFeatureModuleConfiguration.getReferenceFileTypes();

		if (refs != null) {
			Map<String, ReferenceFileTypesProperty> refNames = refs.getAdditionalProperties();

			for (Map.Entry<String, ReferenceFileTypesProperty> refName : refNames.entrySet()) {
				String refId = refName.getKey();

				ReferenceFileTypesProperty refProp = refName.getValue();

				// check for default for this file type
				String defVers = refProp.getDefaultVersion();

				if (defVers != null) {
					defaultLocalVersions.put(refId.toLowerCase(), defVers);
				}

				defVers = refProp.getDefaultRemoteVersion();

				if (defVers != null) {
					defaultRemoteVersions.put(refId.toLowerCase(), defVers);
				}

				String defClass = refProp.getDefaultClassifier();

				if (defClass != null) {
					defaultLocalClassifiers.put(refId.toLowerCase(), defClass);
				}

				defClass = refProp.getDefaultRemoteClassifier();

				if (defClass != null) {
					defaultRemoteClassifiers.put(refId.toLowerCase(), defClass);
				}

				// check for classifiers
				Classifiers classProp = refProp.getClassifiers();

				if (classProp != null) {
					Map<String, ClassifiersProperty> classProps = classProp.getAdditionalProperties();

					for (Map.Entry<String, ClassifiersProperty> className : classProps.entrySet()) {
						String classifier = className.getKey();

						ClassifiersProperty value = className.getValue();
						String defaultVersion = value.getDefaultVersion();

						if (defaultVersion != null) {
							defaultLocalVersions.put((refId + "~" + classifier).toLowerCase(), defaultVersion);
						}

						defaultVersion = value.getDefaultRemoteVersion();
						if (defaultVersion != null) {
							defaultRemoteVersions.put((refId + "~" + classifier).toLowerCase(), defaultVersion);
						}
					}
				}
			}
		}
	}

	@Inject
	public void receiveMetaDataManager(MetaDataManager manager) {
		metaDataManager = manager;

		fmlContext = metaDataManager.getMetaData().getOrCreateContext("file");
		classpathFmlContext = metaDataManager.getMetaData().getOrCreateContext("remote-file");
	}

	@Inject
	public void receiveApplicationLog(@Named("applicationLog") Log log) {
		applicationLog = log;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport runtime) {
		runtimeSupport = runtime;
	}

	@Inject
	public void receiveDiffManager(DiffManager manager) {
		diffManager = manager;
	}

	@Inject
	public void receiveDataFileManager(DataFileManager dfm) {
		dataFileManager = dfm;
	}

	@Override
	public File getDataFileForCurrentTest(String name) {
		return getDataFile(runtimeSupport.getCurrentlyProcessingTestPackage(), name);
	}

	public File getDataFile(ETLTestPackage package_, String name) {
		Pair<ETLTestPackage, String> results = runtimeSupport.processPackageReference(package_, name);

		return runtimeSupport.resolveFile(new File(getDataFileDir(results.getLeft()), runtimeSupport.processReference(results.getRight())));
	}

	public File getDataFile(String name) {
		return getDataFile(null, name);
	}

	@Override
	public File getAssertionFileForCurrentTest(String name, DataFileSchema.format_type format) {
		return getAssertionFile(runtimeSupport.getCurrentlyProcessingTestPackage(), name, format);
	}

	@Override
	public File getAssertionFileDirForCurrentTest() {
		return getAssertionFileDir(runtimeSupport.getCurrentlyProcessingTestPackage());
	}

	public File getAssertionFileDir(ETLTestPackage package_) {
		return new FileBuilder(runtimeSupport.getTestResourceDirectory(package_, "data")).mkdirs().file();
	}

	public File getAssertionFileDir() {
		return getDataFileDir(null);
	}

	@Override
	public File getAssertionFile(ETLTestPackage package_, String name, DataFileSchema.format_type format) {
		return runtimeSupport.resolveFile(new FileBuilder(getAssertionFileDir(package_)).mkdirs().name(runtimeSupport.processReference(name) + "." + format.name()).file());
	}

	public File getAssertionFile(String name, DataFileSchema.format_type format) {
		return getAssertionFile(null, name, format);
	}

	@Override
	public File getGeneratedDataFileForCurrentTest(ETLTestOperation op, String ext) {
		return getGeneratedDataFile(runtimeSupport.getCurrentlyProcessingTestPackage(), op, ext);
	}

	public File getGeneratedDataFile(ETLTestOperation op, String ext) {
		return getGeneratedDataFile(null, op, ext);
	}

	@Override
	public File getGeneratedDataFile(ETLTestPackage package_, ETLTestOperation op, String ext) {
		FileBuilder fb = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory("file")).subdir("generated-data");

		packIt(fb, package_);

		fb.subdir(op.getTestClass().getName()).subdir(op.getTestMethod().getName()).subdir(op.getOrdinal() + "_" + op.getOperationName());

		String name = UUID.randomUUID().toString();
		if (ext != null) {
			name = name + "." + ext;
		}

		fb.name(name);

		return fb.mkdirsToFile().file();
	}

	private void packIt(FileBuilder fb, ETLTestPackage package_) {
		if (package_ != null && !package_.isDefaultPackage()) {
			for (String path : package_.getPackagePath()) {
				fb = fb.subdir(path);
			}

			fb.mkdirs();
		}
	}

	@Override
	public File getGeneratedFileSchemaFileForCurrentTest(ETLTestOperation op, String name) {
		return getGeneratedFileSchemaFile(runtimeSupport.getCurrentlyProcessingTestPackage(), op, name);
	}

	@Override
	public File getGeneratedFileSchemaFile(ETLTestPackage _package, ETLTestOperation op, String name) {
		FileBuilder fb = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory("file")).subdir("fml");

		packIt(fb, _package);

		fb.subdir(op.getTestClass().getName()).subdir(op.getTestMethod().getName()).subdir(op.getOrdinal() + "_" + op.getOperationName());

		fb.name(name + ".fml");

		return fb.mkdirsToFile().file();
	}

	public File getReferenceFile(String path, String name) {
		return runtimeSupport.getReferenceFile(path, name);
	}

	@Override
	public File getDataFileDirForCurrentTest() {
		return getDataFileDir(runtimeSupport.getCurrentlyProcessingTestPackage());
	}

	@Override
	public File getReferenceFileSchemaDir(ETLTestPackage package_) {
		FileBuilder fb = new FileBuilder(runtimeSupport.getReferenceDirectory("file/fml"));

		packIt(fb, package_);

		return fb.mkdirs().file();
	}

	@Override
	public File getReferenceFileSchema(ETLTestPackage package_, String name) {

		FileBuilder file = new FileBuilder(getReferenceFileSchemaDir(package_));
		String[] nameStr = name.split("/");

		if (nameStr.length > 1) {
			for (int i = 0; i < nameStr.length - 1; i++) {
				file.subdir(nameStr[i]);
			}
		}

		file = file.mkdirs().name(nameStr[nameStr.length - 1] + ".fml");
		return runtimeSupport.resolveFile(file.file());
	}

	public File getReferenceFileSchemaDir() {
		return getReferenceFileSchema(null);
	}

	public File getReferenceFileSchema(String name) {
		return getReferenceFileSchema(null, name);
	}

	@Override
	public DataFileSchemaQueryResult locateReferenceFileSchemaForCurrentTest(
			ETLTestValueObject parameters,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, ResourceNotFoundException, TestExecutionError {
		return locateReferenceFileSchema(runtimeSupport.getCurrentlyProcessingTestPackage(), parameters, null, refType, searchNames);
	}

	@Override
	public boolean isSchemaDefinedForCurrentTest(ETLTestValueObject parameters, schemaType type) {
		if (parameters == null) {
			return false;
		}

		if (type != null) {
			switch (type) {
				case source:
					ETLTestValueObject query = parameters.query("source-reference-file-type");
					return query != null && !query.isNull();
				case target:
					ETLTestValueObject query1 = parameters.query("target-reference-file-type");
					return query1 != null && !query1.isNull();
				default:
					return false;
			}
		} else {
			ETLTestValueObject query = parameters.query("reference-file-type");
			return query != null && !query.isNull();
		}
	}

	@Override
	public ETLTestValueObject getSchemaDefinedForCurrentTest(ETLTestValueObject parameters, schemaType type) {
		if (parameters == null) {
			return null;
		}

		if (type != null) {
			switch (type) {
				case source:
					ETLTestValueObject query = parameters.query("source-reference-file-type");
					return query != null && !query.isNull() ? query : null;
				case target:
					ETLTestValueObject query1 = parameters.query("target-reference-file-type");
					return query1 != null && !query1.isNull() ? query1 : null;
				default:
					return null;
			}
		} else {
			ETLTestValueObject query = parameters.query("reference-file-type");
			return query != null && !query.isNull() ? query : null;
		}
	}

	@Override
	public DataFileSchemaQueryResult locateReferenceFileSchemaForCurrentTest(
			ETLTestValueObject parameters,
			schemaType type,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError {
		return locateReferenceFileSchema(runtimeSupport.getCurrentlyProcessingTestPackage(), parameters, type, refType, searchNames);
	}

	public DataFileSchemaQueryResult locateReferenceFileSchema(
			ETLTestPackage package_,
			ETLTestValueObject parameters,
			schemaType type,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, ResourceNotFoundException, TestExecutionError {
		applicationLog.info("Searching for an fml for type: " + type);

		boolean localResult = false;
		boolean specificResult = true;

		// initialize to empty so it can flow through the method
		ReferenceFileTypeRef ref = refType == ReferenceFileTypeRef.ref_type.local ? ReferenceFileTypeRef.refWithId(null) : ReferenceFileTypeRef.remoteRefWithId(null);

		// try the explicit reference
		if (parameters != null) {
			// this can be either a plain reference file type, or in the case of a source or target request we have to look for one of those first
			QueryRes qres = getQueryForType(parameters, type);

			if (qres.getQueryResult() == queryResult.generic) {
				specificResult = false;
			}

			ETLTestValueObject query = qres.getQuery();

			if (query != null) {
				ref = ReferenceFileTypeRef.refFromRequest(query, refType);

				// if an id is given, try to load this and fail if not found
				if (ref.hasId()) {
					// we have to use at least an identifier - use the specific first
					applicationLog.info("Searching for local reference file schema: " + ref);

					// one-trick pony.  No fallback here.
					DataFileSchemaQueryResult dataQuery = locateReferenceFileType(ref, type);

					fmlSearchType searchType = dataQuery.getFmlSearchType();
					if (searchType != FileRuntimeSupport.fmlSearchType.notFound) {
						// alter the search type to reflect the correct enum.  The result is (local|remote)Explicit(Specific|Generic)Query
						StringBuilder queryEnum = new StringBuilder(searchType == FileRuntimeSupport.fmlSearchType.remoteOnly ? "remote" : "local");

						queryEnum.append("Explicit");

						if (specificResult) {
							queryEnum.append("Specific");
						} else {
							queryEnum.append("Generic");
						}

						queryEnum.append("Query");

						return new DataFileSchemaQueryResult(FileRuntimeSupport.fmlSearchType.valueOf(queryEnum.toString()), dataQuery.getDataFileSchema());
					} else {
						// bail
						throw new RequestedFmlNotFoundException();
					}
				}
			}
		}

		// loop through the search path (if provided).  Use the reference type created earlier if it is not null
		if (searchNames != null) {
			int i = 0;
			for (String searchName : searchNames) {
				applicationLog.info("Searching for reference file schema[" + i++ + "]: " + searchName);

				ref = ref.withId(searchName);

				DataFileSchemaQueryResult result = locateReferenceFileType(ref, type);

				if (result.getFmlSearchType() != fmlSearchType.notFound) {
					// this is the one.  Check for local or remote
					localResult = result.getFmlSearchType() == fmlSearchType.localOnly;

					// determine the correct enumeration to use
					StringBuilder stb = new StringBuilder();

					stb.append(localResult ? "local" : "remote");

					stb.append("ImplicitName");

					stb.append("Query");

					String enumName = stb.toString();
					fmlSearchType searchType = fmlSearchType.valueOf(enumName);

					return new DataFileSchemaQueryResult(
							searchType, result.getDataFileSchema()
					);
				}
			}
		}

		// if the user specified a classifier or version, this is an error
		if (ref.hasClassifier() || ref.hasVersion()) {
			throw new RequestedFmlNotFoundException();
		}

		return new DataFileSchemaQueryResult(fmlSearchType.notFound);
	}

	/**
	 * Checks local packages and classpaths until an fml is found or we reach the top level
	 *
	 * @param package_
	 * @param name
	 * @return
	 */
	private DataFileSchemaQueryResult searchPackagesForReferenceFileSchema(ETLTestPackage package_, String name) {
		name = name.toLowerCase();

		// walk up the packages and grab the first schema you find.
		do {
			applicationLog.info("Searching for fml " + name + " in package " + package_.getPackageName());
			File schema = getReferenceFileSchema(package_, name);

			if (schema.exists()) {
				applicationLog.info("Found fml " + name + " in package " + package_.getPackageName());
				return new DataFileSchemaQueryResult(
						fmlSearchType.localOnly, dataFileManager.loadDataFileSchema(schema, name), schema
				);
			}

			StringBuilder resourcePath = new StringBuilder("reference/file/fml");

			if (!package_.isDefaultPackage()) {
				for (String path : package_.getPackagePath()) {
					resourcePath.append("/").append(path);
				}
			}

			resourcePath.append("/").append(name).append(".fml");
			applicationLog.info("Searching classpath for explicit reference file schema: " + resourcePath);

			try {
				DataFileSchemaQueryResult dataFileSchemaQueryResult = new DataFileSchemaQueryResult(
						fmlSearchType.remoteOnly, dataFileManager.loadDataFileSchemaFromResource(resourcePath.toString(), name)
				);
				applicationLog.info("Found explicit reference file schema in classpath: " + resourcePath);
				return dataFileSchemaQueryResult;
			} catch (ResourceNotFoundException exc) {
			}

			applicationLog.info("Did not find fml " + name + " in package " + package_.getPackageName());
			package_ = package_.getParentPackage();
		}
		while (package_ != null);

		return new DataFileSchemaQueryResult(fmlSearchType.notFound);
	}

	public enum queryResult {
		base_source,
		source,
		target,
		generic
	}

	public static class QueryRes {
		private final queryResult queryResult;
		private final ETLTestValueObject query;

		QueryRes(FileRuntimeSupportImpl.queryResult queryResult, ETLTestValueObject query) {
			this.queryResult = queryResult;
			this.query = query;
		}

		public FileRuntimeSupportImpl.queryResult getQueryResult() {
			return queryResult;
		}

		public ETLTestValueObject getQuery() {
			return query;
		}
	}

	public static QueryRes getQueryForType(ETLTestValueObject parameters, schemaType type) {
		if (parameters == null) {
			return null;
		}

		queryResult rs = null;

		ETLTestValueObject queryObj = null;

		if (type != null) {
			switch (type) {
				case base_source:
					queryObj = parameters.query("base-source-reference-file-type");
					rs = queryResult.base_source;
					break;
				case source:
					queryObj = parameters.query("source-reference-file-type");
					rs = queryResult.source;
					break;
				case target:
					queryObj = parameters.query("target-reference-file-type");
					rs = queryResult.target;
					break;
			}
		}

		if (queryObj == null && (type == null || type == schemaType.source)) {
			// fall back to using the generic type attribute
			ETLTestValueObject configQueryObj = parameters.query("reference-file-types");
			queryObj = parameters.query("reference-file-type");
			rs = queryResult.generic;
		}

		return new QueryRes(rs, queryObj);
	}

	@Override
	public DataFileSchemaQueryResult locateReferenceFileSchema(
			ETLTestPackage package_,
			ETLTestValueObject parameters,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError {
		return locateReferenceFileSchema(package_, parameters, null, refType, searchNames);
	}

	public DataFileSchemaQueryResult locateReferenceFileSchema(
			ETLTestValueObject parameters,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, ResourceNotFoundException, TestExecutionError {
		return locateReferenceFileSchema(ETLTestPackageImpl.getDefaultPackage(), parameters, null, searchNames);
	}

	@Override
	public DataFileSchemaQueryResult locateReferenceFileSchema(
			ETLTestValueObject parameters,
			schemaType type,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError {
		return locateReferenceFileSchema(ETLTestPackageImpl.getDefaultPackage(), parameters, type, refType, searchNames);
	}

	public void persistDataFileSchema(DataFileSchema schema) throws IOException {
		persistDataFileSchema(ETLTestPackageImpl.getDefaultPackage(), schema);
	}

	public void persistDataFileSchema(ETLTestPackage package_, DataFileSchema schema) throws IOException {
		File file = getReferenceFileSchema(package_, schema.getId());

		IOUtils.writeBufferToFile(file, new StringBuffer(schema.toJsonString()));
	}

	@Override
	public ReferenceFileTypeRef resolveOperandReferenceFileType(Object in) {
		if (in == null) {
			return null;
		}

		if (in instanceof String) {
			return ReferenceFileTypeRef.refWithId((String) in);
		} else if (in instanceof Map) {
			Map map = (Map) in;

			return ReferenceFileTypeRef.nullableRefWithIdClassifierAndVersion(
				(String) map.get("id"),
				(String) map.get("classifier"),
				(String) map.get("version")
			);
		} else {
			throw new IllegalArgumentException("Not a reference file type");
		}
	}

	public void persistGeneratedDataFileSchema(ETLTestOperation op, DataFileSchema schema) throws IOException {
		persistGeneratedDataFileSchema(ETLTestPackageImpl.getDefaultPackage(), op, schema);
	}

	public void persistGeneratedDataFileSchema(ETLTestPackage package_, ETLTestOperation op, DataFileSchema schema) throws IOException {
		File file = getGeneratedFileSchemaFile(package_, op, schema.getId());

		IOUtils.writeBufferToFile(file, new StringBuffer(schema.toJsonString()));
	}

	@Override
	public void persistGeneratedDataFileSchemaForCurrentTest(ETLTestOperation op, DataFileSchema schema) throws IOException {
		persistGeneratedDataFileSchema(runtimeSupport.getCurrentlyProcessingTestPackage(), op, schema);
	}

	@Override
	public DataFileSchemaQueryResult locateReferenceFileType(ReferenceFileTypeRef ref, schemaType type) throws TestExecutionError {
		applicationLog.info("Searching for an fml by reference: " + ref);

		// if there are defaults, check for any that need to be applied
		if (ref.hasId()) {
			if (!ref.hasClassifier()) {
				String key = ref.getId().toLowerCase();

				if (ref.getRefType() == ReferenceFileTypeRef.ref_type.local) {
					if (defaultLocalClassifiers.containsKey(key)) {
						ref = ref.withClassifier(defaultLocalClassifiers.get(key));
						applicationLog.info("Searching for an fml by reference with classifier: " + ref);
					}
				} else {
					if (defaultRemoteClassifiers.containsKey(key)) {
						ref = ref.withClassifier(defaultRemoteClassifiers.get(key));
						applicationLog.info("Searching for an fml by reference with classifier: " + ref);
					}
				}
			}

			if (!ref.hasVersion()) {
				String key = ref.getId();
				if (ref.hasClassifier()) {
					key += "~" + ref.getClassifier();
				}

				key = key.toLowerCase();

				if (ref.getRefType() == ReferenceFileTypeRef.ref_type.local) {
					if (defaultLocalVersions.containsKey(key)) {
						ref = ref.withVersion(defaultLocalVersions.get(key));
						applicationLog.info("Searching for an fml by reference with version: " + ref);
					}
				} else {
					if (defaultRemoteVersions.containsKey(key)) {
						ref = ref.withVersion(defaultRemoteVersions.get(key));
						applicationLog.info("Searching for an fml by reference with version: " + ref);
					}
				}
			}
		}

		// give the reference file type manager the first shot
		try {
			applicationLog.info("Reference file type manager wants a shot: " + ref);
			String url = referenceFileTypeManager.generateResourcePathForRef(ref);

			DataFileSchemaQueryResult res = searchPackagesForReferenceFileSchema(runtimeSupport.getCurrentlyProcessingTestPackage(), url);

			if (res.getFmlSearchType() == fmlSearchType.notFound) {
				throw new TestExecutionError("", FileConstants.ERR_LOAD_FML);
			}

			res = new DataFileSchemaQueryResult(res.getFmlSearchType(), narrowIfRequired(res.getDataFileSchema(), ref));
			recordMetaData(res, type);

			applicationLog.info("Reference file type manager found it: " + ref);

			return res;
		} catch (RequestedFileTypeNotFoundException e) {
			applicationLog.info("Reference file type manager did not find reference: " + ref);
		} catch (RuntimeException exc) {
			throw new TestExecutionError("Error loading reference type :" + ref, FileConstants.ERR_LOAD_FML, exc);
		}

		String nameToSearchFor = ref.getId() + (ref.hasClassifier() ? ("~" + ref.getClassifier()) : "") + (ref.hasVersion() ? ("+" + ref.getVersion()) : "");
		DataFileSchemaQueryResult referenceFileSchema = searchPackagesForReferenceFileSchema(runtimeSupport.getCurrentlyProcessingTestPackage(), nameToSearchFor);

		applicationLog.info("Explicit local reference file schema[" + nameToSearchFor + "]: " + referenceFileSchema);

		if (referenceFileSchema != null && referenceFileSchema.getFmlSearchType() != fmlSearchType.notFound) {
			// record what we found
			String foundId = referenceFileSchema.getDataFileSchema().getId();
			recordMetaData(referenceFileSchema, type);

			DataFileSchema narrowedDataFileSchema = narrowIfRequired(referenceFileSchema.getDataFileSchema(), ref);
			String narrowedId = narrowedDataFileSchema.getId();

			referenceFileSchema = new DataFileSchemaQueryResult(referenceFileSchema.getFmlSearchType(), narrowedDataFileSchema);

			// record again if narrowed
			if (!foundId.equals(narrowedId)) {
				recordMetaData(referenceFileSchema, type);
			}

			return referenceFileSchema;
		}

		return new DataFileSchemaQueryResult(fmlSearchType.notFound);
	}

	@Override
	public DataFileSchema narrowIfRequired(DataFileSchema dfs, ReferenceFileTypeRef ref) {
		// narrow the schema if requested
		if (ref.getColumnListMode() != ReferenceFileTypeRef.column_list_mode.none) {
			applicationLog.info("Narrowing reference file type to match request");

			// make a fully-inclusive list of column names
			switch (ref.getColumnListMode()) {
				case include:
					dfs = dfs.createSubViewIncludingColumns(ref.getColumns(), "synthetic-incl-" + dfs.getId()).materialize();
					break;
				case exclude:
					dfs = dfs.createSubViewExcludingColumns(ref.getColumns(), "synthetic-excl-" + dfs.getId()).materialize();
					break;
				case none:
					break;
			}

			applicationLog.info("Narrowing yielded " + dfs.getId());
		}

		return dfs;
	}

	private void recordMetaData(DataFileSchemaQueryResult dataFileSchema, schemaType type) {
		if (metaDataManager == null) {
			return;
		}

		MetaDataPackageContext pc = fmlContext.createPackageContext(runtimeSupport.getCurrentlyProcessingTestPackage(), MetaDataPackageContext.path_type.feature_source);

		String refType = "reference-file-type";

		if (type != null) {
			switch (type) {
				case base_source:
					refType = "base-" + refType;
					break;
				case source:
					refType = "source-" + refType;
					break;
				case target:
					refType = "target-" + refType;
					break;
			}
		}

		try {
			File schemaSource = dataFileSchema.getSchemaSource();
			boolean synth = false;

			if (schemaSource == null) {
				synth = true;
				schemaSource = new File(runtimeSupport.getGeneratedSourceDirectoryForCurrentTest("file/fml"), dataFileSchema.getDataFileSchema().getId() + ".fml");
				// persist
				dataFileManager.saveDataFileSchema(dataFileSchema.getDataFileSchema(), schemaSource);
			}

			MetaDataArtifact art = pc.createArtifact(runtimeSupport.getCurrentlyProcessingTestOperation().getQualifiedName() + "/file/fml" + (synth ? "/synth" : ""), schemaSource.getParentFile());
			art.createContent(schemaSource.getName()).referencedByCurrentTest(refType);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void persistDataFileSchemaForCurrentTest(DataFileSchema schema) throws IOException {
		persistDataFileSchema(runtimeSupport.getCurrentlyProcessingTestPackage(), schema);
	}

	public File getDataFileDir(ETLTestPackage package_) {
		return new FileBuilder(runtimeSupport.getTestResourceDirectory(package_, "files")).mkdirs().file();
	}

	public File getDataFileDir() {
		return getDataFileDir(ETLTestPackageImpl.getDefaultPackage());
	}

	public void registerFileProducer(FileProducer producer) {
		if (fileProducerMapLocal.containsKey(producer.getName())) {
			// Changed this to ignore because file producers work across all threads,
			// so there might be many parallel tests running which try to register the same producers
			return;
			//throw new IllegalArgumentException("File producer [" + producer.getName() + "] already registered as class [" + fileProducerMapLocal.get(producer.getName()).getClass() + "]");
		}

		if (fileProducerMapLocal.size() == 0) {
			fileProducerMapLocal.put(DEFAULT, producer);
		}

		fileProducerMapLocal.put(producer.getName(), producer);
	}

	public boolean hasProducerRegistered(String name) {
		if (name == null) {
			name = DEFAULT;
		}

		return fileProducerMapLocal.containsKey(name);
	}

	public FileProducer getRegisteredProducer(String name) {
		if (name == null) {
			name = DEFAULT;
		}

		if (!fileProducerMapLocal.containsKey(name)) {
			throw new IllegalArgumentException("File producer [" + name + "] not registered.");
		}

		return fileProducerMapLocal.get(name);
	}

	public List<ETLTestPackage> getReferenceFileSchemaPackages() {
		return runtimeSupport.getReferencePackages("file/fml");
	}

	public List<String> getReferenceFileSchemasForPackage(ETLTestPackage package_) {
		File fmlRef = runtimeSupport.getReferenceDirectory(package_ == null ? "file/fml" : ("file/fml/" + package_));

		final List<String> schemas = new ArrayList<String>();

		fmlRef.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				if (pathname.isFile() && IOUtils.fileHasExtension(pathname, "fml")) {
					schemas.add(IOUtils.removeExtension(pathname));
				}

				return false;
			}
		});

		return schemas;
	}

	public List<File> getReferenceFileSchemaFilesForPackage(ETLTestPackage package_) {
		File fmlRef = runtimeSupport.getReferenceDirectory(package_.isDefaultPackage() ? "file/fml" : ("file/fml/" + package_.getPackageName()));

		final List<File> schemas = new ArrayList<File>();

		fmlRef.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				if (pathname.isFile() && IOUtils.fileHasExtension(pathname, "fml")) {
					schemas.add(pathname);
				}

				return false;
			}
		});

		return schemas;
	}

	@Override
	public List<String> getReferenceFileSchemasForPackageForCurrentTest() {
		return getReferenceFileSchemasForPackage(runtimeSupport.getCurrentlyProcessingTestPackage());
	}

	@Override
	public File getReferenceFileSchemaForCurrentTest(String name) {
		return getReferenceFileSchema(runtimeSupport.getCurrentlyProcessingTestPackage(), name);
	}

	public void finishCreate() {
		List<ETLTestPackage> packs = getReferenceFileSchemaPackages();

		for (ETLTestPackage pack : packs) {
			MetaDataPackageContext pc = fmlContext.createPackageContext(pack, MetaDataPackageContext.path_type.feature_source);

			try {
				MetaDataArtifact art = pc.createArtifact("fml", getReferenceFileSchemaDir(pack));

				art.populateAllFromDir();

			} catch (IOException e) {
				throw new RuntimeException();
			}
		}
	}

	@Override
	public boolean processDataSetAssertion(
			DataSetAssertionRequest request
	) throws TestAssertionFailure, TestExecutionError {
		MetaDataArtifact pcontextArtifact;
		MetaDataArtifactContent artContent = null;// check for the operation type:
		DataSetAssertionRequest.assertMode assertionMode = request.getAssertMode();

		String userFailureId = request.getFailureId();

		/**
		 * Step I - resolve actual schema.  It is optional at this point but will be required for all but assert exists and not-exists
		 */

		/**
		 * Rule I.1 and I.2 - explicit reference file types
		 */

		DataFileSchema explicitActualReferenceFileType = null;

		List<String> searchNames = new ArrayList<>();

		try {
			/**
			 * According to I.3 and I.4, use the actual schema name first, then the backup (generic) name.
			 */
			searchNames.add(request.getActualSchemaName());
			searchNames.add(request.getActualBackupSchemaName());

			DataFileSchemaQueryResult dresult = locateReferenceFileSchemaForCurrentTest(request.getOperationParameters(), schemaType.source, ReferenceFileTypeRef.ref_type.remote, searchNames.toArray(new String[searchNames.size()]));

			explicitActualReferenceFileType = dresult.getDataFileSchema();

			fmlSearchType fmlSearchType = dresult.getFmlSearchType();
			switch (fmlSearchType) {
				case localExplicitGenericQuery:
					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA_MATCHING_RULE, "I.2." + fmlSearchType.name());
					break;
				case remoteExplicitGenericQuery:
					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA_MATCHING_RULE, "I.2." + fmlSearchType.name());
					break;
				case localExplicitSpecificQuery:
					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA_MATCHING_RULE, "I.1." + fmlSearchType.name());
					break;
				case remoteExplicitSpecificQuery:
					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA_MATCHING_RULE, "I.1." + fmlSearchType.name());
					break;
				case localImplicitNameQuery:
					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA_MATCHING_RULE, "I.3." + fmlSearchType.name());
					break;
				case remoteImplicitNameQuery:
					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA_MATCHING_RULE, "I.3." + fmlSearchType.name());
					break;
				case notFound:
					break;
			}

			if (explicitActualReferenceFileType == null) {
				applicationLog.info("Could not locate data file source schema - looking for schema from request");

				/**
				 * I.5 - defer to the user supplied schema
				 */
				explicitActualReferenceFileType = request.getSourceSchema();

				if (explicitActualReferenceFileType != null) {
					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA_MATCHING_RULE, "I.5");
					applicationLog.info("Request provided source schema: " + explicitActualReferenceFileType.getId());
				}
			}
		} catch (RequestedFmlNotFoundException e) {
			/**
			 * This is an error - I.1 or I.2
			 */
			throw new TestExecutionError("Error resolving Flat File Schema", FileConstants.ERR_LOAD_FML, e);
		}

		if (assertionMode != DataSetAssertionRequest.assertMode.equals) {
			// these cases only require a single actual file - no expected
			// source (actual) fml has been determined.
			DataFile actualDatafile = null;
			try {
				actualDatafile = request.getActualDataset(explicitActualReferenceFileType, explicitActualReferenceFileType, explicitActualReferenceFileType, explicitActualReferenceFileType);
			} catch (IOException e) {
				throw new TestExecutionError("Error loading actual data file", FileConstants.ERR_LOAD_DATA_FILE, e);
			}
			File actualDataset = actualDatafile.getFile();

			switch (assertionMode) {
				case equals:
					// this is the usual case and is handled outside this switch
					break;
				case exists:
					// target file must exist.  It's a failure if it does not.
					if (!actualDataset.exists()) {
						String failureId = FileConstants.FAIL_FILE_NOT_EXIST;

						if (userFailureId != null) {
							failureId = userFailureId;
						}

						throw new TestAssertionFailure(actualDataset.getAbsolutePath(), failureId);
					}

					return true;
				case not_exists:
					// target file must not exist.  It's a failure if it does.
					if (actualDataset.exists()) {
						String failureId = FileConstants.FAIL_FILE_EXISTS;

						if (userFailureId != null) {
							failureId = userFailureId;
						}

						throw new TestAssertionFailure(actualDataset.getAbsolutePath(), failureId);
					}

					return true;
				case empty:
					// target file must exist and be empty.  It's a failure if it does not exist or contains data.  The simple check
					// below catches both empty and not empty scenarios.
				case not_empty:
					// target file must exist and contain data.  It's a failure if it does not exist or does not contain data.
					simpleCheckContents(actualDataset, explicitActualReferenceFileType, request, request.getVariableContext());

					return true;
			}
		}

		/**
		 * I.6 - abort if no source schema found
		 */

		if (explicitActualReferenceFileType == null) {
			throw new TestExecutionError("", FileConstants.ERR_MISSING_SOURCE_FILE_REFERENCE_TYPE);
		}

		applicationLog.info("Resolved data file source schema: " + explicitActualReferenceFileType.getId());

		/**
		 * Load the target (expected) schema.  II
		 */
		DataFileSchema explicitExpectedReferenceFileType = null;

		searchNames.clear();
		searchNames.add(request.getExpectedSchemaName());
		searchNames.add(request.getExpectedBackupSchemaName());

		try {
			/**
			 * II.1
			 */
			if (isSchemaDefinedForCurrentTest(request.getOperationParameters(), FileRuntimeSupport.schemaType.target)) {
				// Why not allow for a search path here?
				DataFileSchemaQueryResult dresult = locateReferenceFileSchemaForCurrentTest(request.getOperationParameters(), FileRuntimeSupport.schemaType.target, ReferenceFileTypeRef.ref_type.local, searchNames.toArray(new String[searchNames.size()]));

				if (dresult.getFmlSearchType() != fmlSearchType.notFound) {
					explicitExpectedReferenceFileType = dresult.getDataFileSchema();

					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_TARGET_SCHEMA_MATCHING_RULE, "II.1." + dresult.getFmlSearchType().name());

					applicationLog.info("Resolved data file target schema: " + explicitExpectedReferenceFileType.getId());
				}
			}

			/**
			 * II.2 - use the target name.  Not an error if it isn't found
			 */
			if (explicitExpectedReferenceFileType == null) {
				DataFileSchemaQueryResult dresult = locateReferenceFileSchemaForCurrentTest(request.getOperationParameters(), schemaType.target, ReferenceFileTypeRef.ref_type.local, searchNames.toArray(new String[searchNames.size()]));

				if (dresult.getFmlSearchType() != fmlSearchType.notFound) {
					explicitExpectedReferenceFileType = dresult.getDataFileSchema();

					request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_TARGET_SCHEMA_MATCHING_RULE, "II.2." + dresult.getFmlSearchType().name());
				}
			}
		} catch (IllegalArgumentException exc) {
			throw new TestExecutionError("Error loading Flat File Schema", FileConstants.ERR_LOAD_FML, exc);
		} catch (RequestedFmlNotFoundException exc) {
			/**
			 * II.1 an error if specified but not resolved.
			 */
			throw new TestExecutionError("Error resolving target Flat File Schema", FileConstants.ERR_LOAD_FML, exc);
		}

		/**
		 * II.3 - set target to source if not resolved
		 */
		if (explicitExpectedReferenceFileType == null) {
			explicitExpectedReferenceFileType = explicitActualReferenceFileType;

			/* Look for a target-reference-file-type.  Only do narrowing operation if the reference file type is specified.
			 * If reference-file-type is used, the source has already been narrowed. */
			ETLTestValueObject query = getSchemaDefinedForCurrentTest(request.getOperationParameters(), schemaType.target);

			if (query != null) {
				ReferenceFileTypeRef ref = ReferenceFileTypeRef.refFromRequest(query, ReferenceFileTypeRef.ref_type.local);
				explicitExpectedReferenceFileType = narrowIfRequired(explicitExpectedReferenceFileType, ref);
			}

			request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_TARGET_SCHEMA_MATCHING_RULE, "II.3");
		}

		/**
		 * IV - create a new sub schema using the column list
		 */
		DataSetAssertionRequest.columnListMode columnListMode = request.getColumnListMode();

		if (columnListMode == null) {
			columnListMode = DataSetAssertionRequest.columnListMode.exclude;
		}

		Set<String> list = request.getColumnList();

		if (request.hasColumnList() && list == null) {
			throw new TestExecutionError("Must specify column-list when the column-list-mode attribute is specified", FileConstants.ERR_STATE_COLUMN_MODE_STATE);
		}

		DataFileSchema referenceFileType = explicitExpectedReferenceFileType;

		if (list != null && list.size() != 0) {
			List<String> actualList = new ArrayList<String>(explicitExpectedReferenceFileType.getLogicalColumnNames());

			try {
				switch (columnListMode) {
					case include:
						// our list only includes mentioned columns
						actualList = DataFileSchemaImpl.intersectLists(actualList, list, true);
						break;
					case exclude:
						// start with all columns, and remove the ones that are called out
						actualList = DataFileSchemaImpl.intersectLists(actualList, list, false);
						break;
				}
			} catch (IllegalArgumentException exc) {
				throw new TestExecutionError(exc.getMessage(), FileConstants.ERR_VIRTUAL_SCHEMA_COLUMNS);
			}

			try {
				referenceFileType = explicitActualReferenceFileType.createSubViewIncludingColumns(actualList, "synthetic-" + explicitActualReferenceFileType.getId() + "-delimited-" + request.getTestOperation().getQualifiedName() + "-target", DataFileSchema.format_type.delimited);
			} catch (IllegalArgumentException exc) {
				throw new TestExecutionError("Error creating reference schema from target", FileConstants.ERR_VIRTUAL_SCHEMA_COLUMNS, exc);
			}
		}

		/**
		 * IV.3 - materialize schema as necessary
		 */
		/*
		if (referenceFileType.isView())
		{
			explicitExpectedReferenceFileType = referenceFileType.materialize();
			// blank out the key columns because the view could cause them to fail.
			explicitExpectedReferenceFileType.setKeyColumns(Collections.EMPTY_LIST);

			// reset the delimiters for the standard delimited file used in assertion data
			explicitExpectedReferenceFileType.setColumnDelimiter(dataFileManager.getDefaultColumnDelimiter());
			explicitExpectedReferenceFileType.setRowDelimiter(dataFileManager.getDefaultRowDelimiter());
			explicitExpectedReferenceFileType.setNullToken(dataFileManager.getDefaultNullToken());
		}
		else
		{
			// copy and use updated delimiters
			explicitExpectedReferenceFileType = referenceFileType.materialize();

			// reset the delimiters for the standard delimited file used in assertion data
			explicitExpectedReferenceFileType.setColumnDelimiter(dataFileManager.getDefaultColumnDelimiter());
			explicitExpectedReferenceFileType.setRowDelimiter(dataFileManager.getDefaultRowDelimiter());
			explicitExpectedReferenceFileType.setNullToken(dataFileManager.getDefaultNullToken());
		}
		 */

		/**
		 * V Intersect source with target
		 */
		DataFileSchema implicitActualReferenceFileType = explicitActualReferenceFileType;

		if (!explicitActualReferenceFileType.getId().equals(referenceFileType.getId())) {
			// verify that the source is 100% superset of target and throw error if it is not
			List<String> sourceColumns = explicitActualReferenceFileType.getLogicalColumnNames();
			List<String> referenceColumns = referenceFileType.getLogicalColumnNames();

			if (!sourceColumns.containsAll(referenceColumns)) {
				throw new TestExecutionError("Source schema does not contain a full superset of reference columns", FileConstants.ERR_VIRTUAL_SCHEMA_COLUMNS);
			}

			implicitActualReferenceFileType = explicitActualReferenceFileType.createSubViewIncludingColumns(referenceFileType.getLogicalColumnNames(), "synthetic-" + explicitActualReferenceFileType.getId() + "-delimited-" + request.getTestOperation().getQualifiedName() + "-source", DataFileSchema.format_type.delimited);
		}

		if (implicitActualReferenceFileType.getLogicalColumns().size() == 0) {
			throw new TestExecutionError("Schema must have at least one column for extraction", FileConstants.ERR_PREPARE_FML);
		}

		File fileGen = runtimeSupport.getGeneratedSourceDirectory("file");

		// store this in a unique enough path not to be duplicated by other runners
		File mkdirs = new FileBuilder(fileGen).subdir("synthetic-fml").subdir(runtimeSupport.getCurrentlyProcessingTestMethod().getQualifiedName()).mkdirs().file();

		try {
			FileUtils.forceMkdir(mkdirs);

			File file = new File(mkdirs, referenceFileType.getId() + ".fml");

			FileUtils.touch(file);

			IOUtils.writeStringToFile(file, referenceFileType.toJsonString());

			file = new File(mkdirs, implicitActualReferenceFileType.getId() + ".fml");

			FileUtils.touch(file);

			IOUtils.writeStringToFile(file, implicitActualReferenceFileType.toJsonString());
		} catch (IOException exc) {
			throw new TestExecutionError("Error Preparing intersection fml: " + exc.toString(), FileConstants.ERR_PREPARE_FML, exc);
		}

		/**
		 * III - Store the schemas for validation.
		 */
		applicationLog.info("Resolved data file target schema: " + explicitExpectedReferenceFileType.getId());
		request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_REFERENCE_TARGET_SCHEMA, explicitExpectedReferenceFileType.getId());
		request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_TARGET_SCHEMA, referenceFileType.getId());
		request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_SOURCE_SCHEMA, implicitActualReferenceFileType.getId());
		request.getVariableContext().declareAndSetStringValue(FileConstants.ASSERT_FILE_REFERENCE_SOURCE_SCHEMA, explicitActualReferenceFileType.getId());

		// source (actual) fml has been determined.
		DataFile actualDatafile = null;
		try {
			actualDatafile = request.getActualDataset(
					explicitActualReferenceFileType,
					implicitActualReferenceFileType,
					explicitExpectedReferenceFileType,
					referenceFileType
			);
		} catch (IOException e) {
			throw new TestExecutionError("Error loading actual data file", FileConstants.ERR_LOAD_DATA_FILE, e);
		}
		File actualDataset = actualDatafile.getFile();

		/** It's okay for the expected source file not to exist if refresh assertion data is enabled
		 *  since the source file will be refreshed prior to the assertion being tested
		 */
		DataFile expectedDatafile = request.getExpectedDataset(
				explicitActualReferenceFileType,
				implicitActualReferenceFileType,
				explicitExpectedReferenceFileType,
				referenceFileType
		);
		File expectedDataset = expectedDatafile.getFile();

		if (!expectedDataset.exists() && !request.refreshAssertionData()) {
			throw new TestExecutionError("Target file does not exist: " + expectedDataset, FileConstants.ERR_TARGET_FILE_MISSING);
		}

		if (actualDataset == null || !actualDataset.exists()) {
			throw new TestExecutionError("Source file does not exist: " + actualDataset, FileConstants.ERR_SOURCE_FILE_MISSING);
		}

		try {
			MetaDataPackageContext metaDataPackageContext = request.getMetaDataPackageContext();
			if (metaDataPackageContext != null) {
				pcontextArtifact = metaDataPackageContext.createArtifact("data", getAssertionFileDirForCurrentTest());
				artContent = pcontextArtifact.createContent(expectedDataset.getName());
				artContent.referencedByCurrentTest("assertion-target-file");
			}
		} catch (IOException e) {
			throw new TestExecutionError("Error creating meta data artifact", e);
		}

		/**
		 * VI, VII - Copy actual data set into local, using explicitSource as the reader and implicitSource as the writer
		 */
		// create a delimited local copy of the remote file
		File localExpected = getGeneratedDataFileForCurrentTest(request.getTestOperation(), "expected");
		File localActual = getGeneratedDataFileForCurrentTest(request.getTestOperation(), "actual");

		try {
			// copy both files - source and target, into a local delimited copy
			DataFile actualView = dataFileManager.loadDataFile(localActual, referenceFileType);

			applicationLog.info("Caching local copy of actual/source " + actualDataset.getAbsolutePath() + " to " + localActual.getAbsolutePath());

			/**
			 * VI.1 - copy actual (source)
			 */
			dataFileManager.copyDataFile(actualDatafile, actualView);

			// if the schema is a view, close and re-open the data file with the materialized schema
			if (referenceFileType.isView()) {
				referenceFileType = ((DataFileSchemaView) referenceFileType).materialize();
				referenceFileType.setKeyColumns(Collections.EMPTY_LIST);

				actualView = dataFileManager.loadDataFile(localActual, referenceFileType);
			}

			DataFile expected = dataFileManager.loadDataFile(expectedDataset, explicitExpectedReferenceFileType);

			// before processing the expected (target), refresh the data if necessary
			// create the assertion refresh file from the explicit target fml - not the reference one
			if (request.refreshAssertionData()) {
				/**
				 * This needs to be changed to honor the target schema, if any.
				 */
				// copy from the local target, since it has the schema the source will be validated against
				applicationLog.info("Refreshing assertion data " + expectedDataset.getName() + " from " + localActual.getAbsolutePath() + " using destination schema " + explicitExpectedReferenceFileType.getId());

				if (artContent != null) {
					artContent.referencedByCurrentTest("assertion-refresh-file");
				}

				dataFileManager.copyDataFile(actualDatafile, expected, new CopyOptionsBuilder().withHonorDateDiff(DataFileManager.CopyOptions.honor_date_diff.yes).options());

				// notify the caller that the data was refreshed in case any repackaging is needed
				request.processRefreshedAssertionData(expected);
			}

			/**
			 * Load the assertion file from the local storage.  Use the supplied reference
			 * format to access the assertion source, and then narrow it using the view
			 * if it was different than the source originally.
			 */

			/**
			 * Expected schema is already resolved.  Read the explicit schema and write the implicit schema
			 */
			DataFile expectedView = dataFileManager.loadDataFile(localExpected, referenceFileType);

			applicationLog.info("Caching local copy of expected/target " + expectedDataset.getAbsolutePath() + " to " + localExpected.getAbsolutePath());

			/**
			 * VII.1 - create copy
			 */
			dataFileManager.copyDataFile(expected, expectedView);

			/**
			 * VII - compare
			 */
			List<FileDiff> diffResults = dataFileManager.diff(expectedView, actualView);

			String failureId = FileConstants.FAIL_FILE_DIFF;

			if (userFailureId != null) {
				failureId = userFailureId;
			}

			if (diffResults.size() != 0) {
				dataFileManager.report(
						diffManager,
						request.getTestMethod(),
						request.getTestOperation(),
						failureId,
						diffResults,
						expectedDataset.getName(),
						actualDataset.getName()
				);

				throw new TestAssertionFailure("File : " + expectedDataset.getAbsolutePath() + " does not match : " + actualDataset.getAbsolutePath(), failureId);
			}
		} catch (IOException e) {
			throw new TestExecutionError("Error comparing files: " + e.toString(), FileConstants.ERR_FILE_COMPARE, e);
		} catch (DataFileMismatchException dfme) {
			throw new TestExecutionError("", FileConstants.ERR_DATA_FILE_MISMATCH_EXCEPTION, dfme);
		}

		return false;
	}

	private void simpleCheckContents(File target, DataFileSchema explicitSourceReferenceFileType, DataSetAssertionRequest operation, VariableContext context) throws TestAssertionFailure, TestExecutionError {
		String failureId = operation.getAssertMode() == DataSetAssertionRequest.assertMode.not_empty ? FileConstants.FAIL_FILE_EMPTY : FileConstants.FAIL_FILE_NOT_EMPTY;

		if (operation.getFailureId() != null) {
			failureId = operation.getFailureId();
		}

		if (!target.exists()) {
			throw new TestExecutionError(target.getAbsolutePath(), FileConstants.ERR_TARGET_FILE_MISSING);
		}

		try {
			if (explicitSourceReferenceFileType == null) {
				// rethrow so that the normal empty / not empty handlers can handle
				throw new ResourceNotFoundException();
			}

			// open the file and count the lines
			DataFile df = dataFileManager.loadDataFile(target, explicitSourceReferenceFileType);

			DataFileReader data = df.reader();

			int rows = 0;

			try {
				Iterator<DataFileReader.FileRow> it = data.iterator();

				while (it.hasNext()) {
					it.next();
					rows++;
				}
			} finally {
				data.dispose();
			}

			switch (operation.getAssertMode()) {
				case empty:
					if (rows != 0) {
						throw new TestAssertionFailure(target.getAbsolutePath() + " contains " + rows + " rows of data", failureId);
					}
					break;
				case not_empty:
					if (rows == 0) {
						throw new TestAssertionFailure(target.getAbsolutePath() + " contains no data", failureId);
					}
					break;
			}
		} catch (IOException exc) {
			throw new TestExecutionError("", FileConstants.ERR_FILE_COMPARE, exc);
		} catch (ResourceNotFoundException exc) {
			// fallback - just check the file length
			switch (operation.getAssertMode()) {
				case empty:
					if (target.length() != 0) {
						throw new TestAssertionFailure(target.getAbsolutePath(), failureId);
					}
					break;
				case not_empty:
					if (target.length() == 0) {
						throw new TestAssertionFailure(target.getAbsolutePath(), failureId);
					}
					break;
			}
		}
	}

	@Override
	public File getSourceScriptForCurrentTest(String scriptName, String use) {
		ETLTestPackage package_ = runtimeSupport.getCurrentlyProcessingTestPackage();

		return getSourceScript(package_, scriptName, use);
	}

	@Override
	public File getSourceScript(ETLTestPackage package_, String scriptName, String use) {
		Pair<ETLTestPackage, String> results = runtimeSupport.processPackageReference(package_, scriptName);

		File sql = runtimeSupport.resolveFile(new FileBuilder(getSourceScriptsDirectory(results.getLeft())).name(runtimeSupport.processReference(results.getRight()) + ".sql").file());

		if (sql != null && runtimeSupport.isTestActive()) {
			MetaDataPackageContext dataPackageContext = fmlContext.createPackageContext(results.getLeft(), MetaDataPackageContext.path_type.test_source);
			MetaDataArtifact art;

			try {
				art = dataPackageContext.createArtifact("sql", getSourceScriptsDirectory(results.getLeft()));
				art.createContent(results.getRight() + ".sql").referencedByCurrentTest(use);
			} catch (IOException e) {
				throw new IllegalStateException("Error getting data context", e);
			}
		}

		return sql;
	}

	@Override
	public File getSourceScriptsDirectory(ETLTestPackage package_) {
		return runtimeSupport.getTestResourceDirectory(package_, "sql");
	}

	@Override
	public FileContext getFileContext(ETLTestValueObject param, VariableContext vcontext) {
		String contextId = FileFeatureModule.getFileContextName(param);

		String contextName = ObjectUtils.firstNonNull(contextId, FileContext.DEFAULT_CONTEXT_NAME);

		return new ETLTestValueObjectFileContext(contextName, FileFeatureModule.getFileContext(param, vcontext));
	}

	@Override
	public File getSourceScriptsDirectoryForCurrentTest() {
		return getSourceScriptsDirectory(runtimeSupport.getCurrentlyProcessingTestPackage());
	}
}
