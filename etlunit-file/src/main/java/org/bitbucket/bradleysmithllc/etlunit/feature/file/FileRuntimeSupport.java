package org.bitbucket.bradleysmithllc.etlunit.feature.file;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFile;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeRef;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface FileRuntimeSupport
{
	FileContext getFileContext(ETLTestValueObject param, VariableContext vcontext);
	File getSourceScriptsDirectoryForCurrentTest();
	File getSourceScriptsDirectory(ETLTestPackage package_);
	File getSourceScript(ETLTestPackage package_, String scriptName, String use);
	File getSourceScriptForCurrentTest(String scriptName, String use);

	File getDataFileForCurrentTest(String name);

	File getDataFile(ETLTestPackage package_, String name);
	File getDataFile(String name);

	File getAssertionFileForCurrentTest(String name, DataFileSchema.format_type format);
	File getAssertionFileDirForCurrentTest();
	File getAssertionFileDir(ETLTestPackage package_);

	File getAssertionFile(ETLTestPackage package_, String name, DataFileSchema.format_type format);
	File getAssertionFile(String name, DataFileSchema.format_type format);

	File getGeneratedFileSchemaFileForCurrentTest(ETLTestOperation op, String name);
	File getGeneratedFileSchemaFile(ETLTestPackage _package, ETLTestOperation op, String name);

	File getGeneratedDataFile(ETLTestPackage package_, ETLTestOperation op, String ext);
	File getGeneratedDataFile(ETLTestOperation op, String ext);
	File getGeneratedDataFileForCurrentTest(ETLTestOperation op, String ext);

	File getReferenceFile(String path, String name);

	File getDataFileDirForCurrentTest();

	File getDataFileDir(ETLTestPackage package_);
	File getDataFileDir();

	void registerFileProducer(FileProducer producer);
	FileProducer getRegisteredProducer(String name);
	boolean hasProducerRegistered(String name);

	List<ETLTestPackage> getReferenceFileSchemaPackages();
	List<String> getReferenceFileSchemasForPackage(ETLTestPackage package_);
	List<File> getReferenceFileSchemaFilesForPackage(ETLTestPackage package_);

	List<String> getReferenceFileSchemasForPackageForCurrentTest();

	File getReferenceFileSchemaForCurrentTest(
			String name
	);

	File getReferenceFileSchemaDir(ETLTestPackage package_);

	/**
	 * Locate a reference file schema by package and name.  The resolved object is passed through
	 * runtimeSupport.resolveObject to handle case-name mismatches.
	 * @param package_
	 * @param name
	 * @return
	 */
	File getReferenceFileSchema(
			ETLTestPackage package_,
			String name
	);

	File getReferenceFileSchemaDir();

	File getReferenceFileSchema(
			String name
	);

	/**
	 * This is the search order for reference file types.
	 * 	A - The Source is a remote reference file type
	 * 	B - The Target is a local reference file type.
	 *
	 * 	I	 	- Source (actual) schema resolution:
	 * 		1 - Look for explicit source reference file type.  If supplied and not found, an error occurs.
	 * 		2 - Look for a generic reference file type.  If supplied and not found, an error occurs.
	 * 		3 - Use the user-supplied name of the object.  If not found, continue.
	 * 		4 - Use the (optional) generified name as supplied by the request.
	 * 		5 - Use the (optional) supplied schema.
	 * 		6 - Abort if no schema found thus far.
	 *
	 * 	II 	- Target (expected) schema resolution:
	 *  	1 - Look for explicit target reference file type.  If supplied and not found, an error occurs.
	 *  	2 - Use the user-supplied name of the object.  If not found, continue.
	 *    3 - Use the same schema as the source.  If a reference file type is defined for either
	 *        target or generic, it must be honored.  Specifically, this refers to the column lists.
	 *
	 *  III - Once resolved, store the source and target base schemas into a context variable for validation.
	 *
	 *  IV 	- Process any include / exclude columns on the target schema.  This is the reference schema.
	 *    1 - If the result is a 0-column schema, it is an error.
	 *    2 - If any columns referenced in the include / exclude list are not present in the target, it is an error
	 *
	 *  V   - Create a view of the source schema intersecting the target.
	 *    1 - If the source is not a complete superset of that target, it is an error.
	 *
	 *  VI	- Copy the source data set into a local copy based on the view created in V.
	 *  	1 - original source schema is the source, source schema with reduced columns (V) is the target.
	 *
	 *  VII	-	Copy the target data set into a local copy based on an intersection with the view created in IV.
	 *  	1 - target schema is the source, target schema with reduced columns (IV) is the target.
	 *
	 * 	VIII	-	Compare two local datasets using the target schema (IV).
	 */
	boolean processDataSetAssertion(
			DataSetAssertionRequest request
	) throws TestAssertionFailure, TestExecutionError;

	enum schemaType {base_source, source, target}

	boolean isSchemaDefinedForCurrentTest(
			ETLTestValueObject parameters,
			schemaType type
	) throws RequestedFmlNotFoundException;

	ETLTestValueObject getSchemaDefinedForCurrentTest(
			ETLTestValueObject parameters,
			schemaType type
	) throws RequestedFmlNotFoundException;

	DataFileSchemaQueryResult locateReferenceFileSchemaForCurrentTest(
			ETLTestValueObject parameters,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError;

	DataFileSchemaQueryResult locateReferenceFileSchemaForCurrentTest(
			ETLTestValueObject parameters,
			schemaType type,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError;

	DataFileSchemaQueryResult locateReferenceFileSchema(
		ETLTestPackage package_,
		ETLTestValueObject parameters,
		ReferenceFileTypeRef.ref_type refType,
		String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError;

	DataFileSchemaQueryResult locateReferenceFileSchema(
			ETLTestPackage package_,
			ETLTestValueObject parameters,
			schemaType type,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError;

	DataFileSchemaQueryResult locateReferenceFileSchema(
			ETLTestValueObject parameters,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError;

	DataFileSchemaQueryResult locateReferenceFileSchema(
			ETLTestValueObject parameters,
			schemaType type,
			ReferenceFileTypeRef.ref_type refType,
			String... searchNames
	) throws RequestedFmlNotFoundException, TestExecutionError;

	void persistDataFileSchema(DataFileSchema schema) throws IOException;
	void persistDataFileSchema(ETLTestPackage package_, DataFileSchema schema) throws IOException;

	ReferenceFileTypeRef resolveOperandReferenceFileType(Object in);

	DataFileSchema narrowIfRequired(DataFileSchema dfs, ReferenceFileTypeRef ref);

	void persistDataFileSchemaForCurrentTest(DataFileSchema schema) throws IOException;

	void persistGeneratedDataFileSchema(ETLTestOperation op, DataFileSchema schema) throws IOException;
	void persistGeneratedDataFileSchema(ETLTestPackage package_, ETLTestOperation op, DataFileSchema schema) throws IOException;
	void persistGeneratedDataFileSchemaForCurrentTest(ETLTestOperation op, DataFileSchema schema) throws IOException;

	enum fmlSearchType {
		/**
		 * Found in the local project, using the base 'reference-file-type' attribute
		 */
		localExplicitGenericQuery,
		/**
		 * Found in the local project, using the base '<source|target>-reference-file-type' attribute
		 */
		localExplicitSpecificQuery,
		/**
		 * Found in the classpath, using the base 'reference-file-type' attribute
		 */
		remoteExplicitGenericQuery,
		/**
		 * Found in the classpath, using the base '<source|target>-reference-file-type' attribute
		 */
		remoteExplicitSpecificQuery,
		/**
		 * Found in the local project using the name given by the client
		 */
		localImplicitNameQuery,
		/**
		 * Found in the classpath using the name given by the client
		 */
		remoteImplicitNameQuery,
		/**
		 * Intermediate result.  Found locally
		 */
		localOnly,
		/**
		 * Intermediate result.  Found on classpath
		 */
		remoteOnly,
		/**
		 * Not found
		 */
		notFound
	}

	interface DataSetAssertionRequest
	{
		void processRefreshedAssertionData(DataFile expected) throws TestExecutionError;

		enum columnListMode {
			exclude,
			include
		}

		enum assertMode
		{
			equals,
			exists,
			not_exists,
			empty,
			not_empty
		}

		FileRuntimeSupportImpl.DataSetAssertionRequest.assertMode getAssertMode();
		/**
		 * The name to use for locating the data set that matches I - 3 - the user specified name
		 */
		String getActualSchemaName();

		/**
		 * The name to use for locating the data set that matches I - 4 - generified name
		 */
		String getActualBackupSchemaName();

		/**
		 * The name to use for locating the data set that matches II - 2 - the user specified name
		 */
		String getExpectedSchemaName();

		/**
		 * The name to use for locating the data set that matches II - 3 - generified name
		 */
		String getExpectedBackupSchemaName();

		boolean hasColumnList();
		FileRuntimeSupportImpl.DataSetAssertionRequest.columnListMode getColumnListMode();
		Set<String> getColumnList();
		String getFailureId();

		DataFile getExpectedDataset(
			DataFileSchema explicitActualSchema,
			DataFileSchema actualEffectiveSchema,
			DataFileSchema explicitExpectedSchema,
			DataFileSchema expectedEffectiveSchema
		);

		DataFile getActualDataset(
				DataFileSchema explicitActualSchema,
				DataFileSchema actualEffectiveSchema,
				DataFileSchema explicitExpectedSchema,
				DataFileSchema expectedEffectiveSchema
		) throws IOException, TestExecutionError;

		ETLTestMethod getTestMethod();
		ETLTestOperation getTestOperation();
		ETLTestValueObject getOperationParameters();
		VariableContext getVariableContext();
		MetaDataPackageContext getMetaDataPackageContext();

		/**
		 * Determines assertion direction.  If set to true the data is refreshed to local before comparison.
		 * @return
		 */
		boolean refreshAssertionData();

		/**
		 * Schema to use if no other is found through the usual search methods
		 * @return The source schema, or null if there is none.
		 */
		DataFileSchema getSourceSchema();
	}

	class DataFileSchemaQueryResult
	{
		private final fmlSearchType fmlSearchType;
		private final DataFileSchema dataFileSchema;
		private final File schemaSource;

		protected DataFileSchemaQueryResult(FileRuntimeSupport.fmlSearchType fmlSearchType, DataFileSchema dataFileSchema) {
			this(fmlSearchType, dataFileSchema, null);
		}

		protected DataFileSchemaQueryResult(FileRuntimeSupport.fmlSearchType fmlSearchType, DataFileSchema dataFileSchema, File source) {
			this.fmlSearchType = fmlSearchType;
			this.dataFileSchema = dataFileSchema;
			schemaSource = source;
		}

		protected DataFileSchemaQueryResult(FileRuntimeSupport.fmlSearchType fmlSearchType) {
			this(fmlSearchType, null, null);
		}

		public FileRuntimeSupport.fmlSearchType getFmlSearchType() {
			return fmlSearchType;
		}

		public DataFileSchema getDataFileSchema() {
			return dataFileSchema;
		}

		@Override
		public String toString() {
			return "DataFileSchemaQueryResult{" +
					"fmlSearchType=" + fmlSearchType +
					", dataFileSchema=" + (dataFileSchema != null ? dataFileSchema.getId() : "null") +
					", schemaSource=" + schemaSource +
					'}';
		}

		public File getSchemaSource() {
			return schemaSource;
		}
	}

	DataFileSchemaQueryResult locateReferenceFileType(ReferenceFileTypeRef ref, schemaType type) throws TestExecutionError;
}
