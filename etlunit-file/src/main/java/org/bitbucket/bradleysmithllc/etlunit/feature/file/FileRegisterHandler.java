package org.bitbucket.bradleysmithllc.etlunit.feature.file;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.name.Named;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.register.RegisterHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.register.RegisterRequest;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObjectImpl;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;

import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileRegisterHandler extends NullClassListener implements RegisterHandler
{
	private FileRuntimeSupport fileRuntimeSupport;

	private Log applicationLog;

	private DataFileManager dataFileManager;

	@Inject
	public void receiveDataFileManager(DataFileManager dfm)
	{
		dataFileManager = dfm;
	}

	@Inject
	public void receiveApplicationLog(@Named("applicationLog") Log log)
	{
		this.applicationLog = log;
	}

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport support)
	{
		fileRuntimeSupport = support;
	}

	@Override
	public action_code register(RegisterRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		// check the producer
		String prod = request.getProducer();

		FileProducer regProd = null;

		if (fileRuntimeSupport.hasProducerRegistered(prod))
		{
			regProd = fileRuntimeSupport.getRegisteredProducer(prod);

			if (regProd.getClass() != ProducerImpl.class)
			{
				throw new TestExecutionError("Producer [" + prod + "] already registered - this operation will break existing tests.", FileConstants.ERR_REGISTER_PRODUCER);
			}
		}
		else
		{
			ProducerImpl regProdImpl = new ProducerImpl(ObjectUtils.firstNotNull(prod, "fileProducerDefault"));
			regProd = regProdImpl;
			System.out.println("New impl");
			fileRuntimeSupport.registerFileProducer(regProd);
		}

		ProducerImpl pi = (ProducerImpl) regProd;

		pi.addPublication(
			request.getFile(),
			request.getClassifier(),
			request.getContext(),
			ObjectUtils.firstNotNull(request.getRegisteredName(), request.getFile()),
			variableContext
		);

		return action_code.handled;
	}

	private class ProducerImpl implements FileProducer
	{
		private final String name;

		private ProducerImpl(String name)
		{
			this.name = name;
		}

		@Override
		public String getName()
		{
			return name;
		}

		@Override
		public File locateFile(String reference, VariableContext vcontext)
		{
			return locateFileWithClassifierAndContext(reference, null, null, vcontext);
		}

		@Override
		public File locateFileWithClassifier(String reference, String classifier, VariableContext vcontext)
		{
			return locateFileWithClassifierAndContext(reference, classifier, null, vcontext);
		}

		@Override
		public File locateFileWithContext(String reference, String context, VariableContext vcontext)
		{
			return locateFileWithClassifierAndContext(reference, null, context, vcontext);
		}

		@Override
		public File locateFileWithClassifierAndContext(String reference, String classifier, String context, VariableContext vcontext)
		{
			String key = getKey(classifier, context, reference);

			//Publication pub = publications().get(key);

			if (!vcontext.hasVariableBeenDeclared(key))
			{
				throw new IllegalArgumentException("File [" + key + "] not registered");
			}

			ETLTestValueObject etl = vcontext.getValue(key);
			Publication pub = (Publication) etl.getValueAsPojo();

			return fileRuntimeSupport.getDataFileForCurrentTest(pub.file);
		}

		class Publication
		{
			private final String file;
			private final String classifier;
			private final String context;
			private final String runtimeName;

			Publication(String file, String classifier, String context, String runtimeName)
			{
				this.file = file;
				this.classifier = classifier;
				this.context = context;
				this.runtimeName = runtimeName;
			}
		}

		/**
		 * Add a file to the list of publications supported by this particular producer.
		 * @param file
		 * @param classifier
		 * @param context
		 * @param runtimeName
		 * @param variableContext
		 */
		public void addPublication(String file, String classifier, String context, String runtimeName, VariableContext variableContext) throws TestExecutionError
		{
			String key = getKey(classifier, context, runtimeName);

			if (variableContext.hasVariableBeenDeclared(key))
			{
				throw new TestExecutionError("Runtime name [" + key + "] already published", FileConstants.ERR_REGISTER_FILE_NAME_DUPLICATED);
			}

			Publication pub = new Publication(file, classifier, context, runtimeName);

			variableContext.declareAndSetValue(key, new ETLTestValueObjectImpl(pub));
			//publications().put(key, );
		}

		private String getKey(String classifier, String context, String runtimeName)
		{
			return new StringBuilder(ObjectUtils.firstNotNull(context, "[[DefaultContext]]"))
						.append(".")
						.append(ObjectUtils.firstNotNull(classifier, "[[DefaultClassifier]]"))
						.append(".")
						.append(runtimeName)
						.toString();
		}
	}
}
