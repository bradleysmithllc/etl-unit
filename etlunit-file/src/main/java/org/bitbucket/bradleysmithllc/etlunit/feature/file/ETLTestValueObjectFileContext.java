package org.bitbucket.bradleysmithllc.etlunit.feature.file;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.util.ArrayList;
import java.util.List;

public class ETLTestValueObjectFileContext implements FileContext
{
	private final String contextName;
	private final List<ContextFile> contextFileList = new ArrayList<ContextFile>();

	public ETLTestValueObjectFileContext(String contextName, ETLTestValueObject fileContext)
	{
		this.contextName = contextName;

		if (fileContext != null)
		{
			List<ETLTestValueObject> valueAsList = fileContext.getValueAsList();

			for (ETLTestValueObject value : valueAsList)
			{
				contextFileList.add(ContextFile.parse(value.getJsonNode()));
			}
		}
	}

	@Override
	public String getContextName()
	{
		return contextName == null ? DEFAULT_CONTEXT_NAME : contextName;
	}

	@Override
	public boolean isDefaultContext()
	{
		return contextName != null;
	}

	@Override
	public List<ContextFile> getContextFileList()
	{
		return contextFileList;
	}
}
