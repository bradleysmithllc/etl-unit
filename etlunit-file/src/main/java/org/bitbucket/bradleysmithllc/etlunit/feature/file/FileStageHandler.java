package org.bitbucket.bradleysmithllc.etlunit.feature.file;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Inject;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file._assert.assert_data_set.AssertDataSetHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file._assert.assert_data_set.AssertDataSetRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file._assert.assert_file.AssertFileHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file._assert.assert_file.AssertFileRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.stage.stage_data_set.StageDataSetHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.stage.stage_data_set.StageDataSetRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.stage.stage_data_set_list_file.StageDataSetListFileHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.stage.stage_data_set_list_file.StageDataSetListFileRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.stage.stage_file.StageFileHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.stage.stage_file.StageFileRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.stage.stage_list_file.StageListFileHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.json.file.stage.stage_list_file.StageListFileRequest;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.metadata.*;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileStageHandler extends NullClassListener implements StageFileHandler, StageListFileHandler, StageDataSetHandler, StageDataSetListFileHandler
{
	private RuntimeSupport runtimeSupport;
	private FileRuntimeSupport fileRuntimeSupport;
	private FileFeatureModule fileFeatureModule;
	private MetaDataContext fileMetaContext;

	@javax.inject.Inject
	public void receiveMetaDataManager(MetaDataManager manager)
	{
		fileMetaContext = manager.getMetaData().getOrCreateContext("file");
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport runtimeSupport)
	{
		this.runtimeSupport = runtimeSupport;
	}

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport sup)
	{
		fileRuntimeSupport = sup;
	}

	@Inject
	public void receiveFileFeatureModule(FileFeatureModule mod)
	{
		fileFeatureModule = mod;
	}

	public action_code stageFile(StageFileRequest operation, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		return stageFile(operation, null, null, mt, op, obj, context, econtext);
	}

	private action_code stageFile(StageFileRequest operation, String listFile1, File sourceFile, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		Pair<ETLTestPackage, String> results;

		if (operation.getFile() == null)
		{
			if (listFile1 == null)
			{
				throw new IllegalArgumentException("Either one of list-file or file must be specified");
			}

			results = runtimeSupport.processPackageReference(runtimeSupport.getCurrentlyProcessingTestPackage(), listFile1);
		} else {
			results = runtimeSupport.processPackageReference(runtimeSupport.getCurrentlyProcessingTestPackage(), operation.getFile());
		}

		List<String> fileNames = new ArrayList<String>();
		fileNames.add(results.getRight());

		if (listFile1 != null)
		{
			// blast all names contained in the list file into the context
			File source = fileRuntimeSupport.getDataFile(results.getLeft(), results.getRight());

			if (!source.exists())
			{
				throw new TestExecutionError(listFile1, FileConstants.ERR_SOURCE_FILE_MISSING);
			}

			try
			{
				BufferedReader bufferedReader = new BufferedReader(new FileReader(source));

				try
				{
					String str = null;

					while ((str = bufferedReader.readLine()) != null)
					{
						fileNames.add(str);
					}
				}
				finally
				{
					bufferedReader.close();
				}
			}
			catch (IOException e)
			{
				throw new TestExecutionError("List file does not exist", e);
			}
		}

		String classifier = operation.getClassifier();

		MetaDataContext fcontext = fileFeatureModule.getFileMetaContext();

		MetaDataPackageContext pcontext = fcontext.createPackageContext(results.getLeft(), MetaDataPackageContext.path_type.test_source);

		MetaDataArtifact pcontextArtifact;

		String var = operation.getVariableName();

		for (int i = 0; i < fileNames.size(); i++)
		{
			String fileName = fileNames.get(i);

			try
			{
				pcontextArtifact = pcontext.createArtifact("file", fileRuntimeSupport.getDataFileDir(results.getLeft()));
				MetaDataArtifactContent artContent = pcontextArtifact.createContent(fileName);
				artContent.referencedByCurrentTest(classifier != null ? classifier : "stage-source-file");
			}
			catch (IOException e)
			{
				throw new TestExecutionError("Error creating meta data artifact", e);
			}

			File source = fileRuntimeSupport.getDataFile(results.getLeft(), fileName);

			if (!source.exists())
			{
				throw new TestExecutionError(fileName, FileConstants.ERR_SOURCE_FILE_MISSING);
			}

			ContextFileBuilder cfb = new ContextFileBuilder(source, fileName);

			if (classifier != null)
			{
				cfb.withClassifier(classifier);
			}

			String destinationName = operation.getDestinationName();

			if (destinationName != null)
			{
				cfb.withDestinationName(destinationName);
			}

			// Set the file variable.  For list files this will be @var for the first, and @var-2..n for the second on (1-based numbering)
			String actualDestinationName = ObjectUtils.firstNonNull(destinationName, fileName);

			if (var != null)
			{
				cfb.withVariableName(var);
				context.declareAndSetStringValue(var + (i == 0 ? "" : ("-" + (i + 1))), actualDestinationName);
			}

			// post a debug message to the context

			String contextKey =
				makeDebugContextKey(fileName, classifier, var, operation.getContextName(), actualDestinationName);

			context.declareAndSetStringValue(contextKey, "staged");

			ETLTestValueObject co = FileFeatureModule.getFileContext(obj, context);

			String jsonString = cfb.create().toJsonNode().toString();

			if (co != null)
			{
				// this will be an array of objects.  We need to add another to the end
				try
				{
					co.getValueAsList().add(ETLTestParser.loadObject(jsonString));
				}
				catch (ParseException e)
				{
					throw new IllegalArgumentException(e);
				}
			}
			else
			{
				FileFeatureModule.setFileContext(obj, context, "[" + jsonString + "]");
			}
		}

		return action_code.handled;
	}

	public static String makeDebugContextKey(
		String fileName,
		String classifier,
		String variableName,
		String contextName,
		String actualDestinationName
	)
	{
		return "FileContext[stage(" + fileName + "->"
			+ ObjectUtils.firstNonNull(classifier, "[noclassifier]") + "->"
			+ ObjectUtils.firstNonNull(variableName, "[novariable]") + "->"
			+ ObjectUtils.firstNonNull(contextName, "[nocontext]") + "->"
			+ actualDestinationName + ")]";
	}

	@Override
	public void beginPackage(ETLTestPackage name, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		// register the files for this package as meta data
		MetaDataPackageContext pc = fileMetaContext.createPackageContextForCurrentTest(MetaDataPackageContext.path_type.test_source);

		try
		{
			MetaDataArtifact af = pc.createArtifact("file", fileRuntimeSupport.getDataFileDirForCurrentTest());

			// let the meta data pull in all files
			af.populateAllFromDir();
		}
		catch (IOException e)
		{
			throw new TestExecutionError("Error getting file meta data", e);
		}
	}

	@Override
	public action_code stageDataSet(StageDataSetRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		// TODO: Revisit this to allow reference file types
		StageFileRequest sfr = new StageFileRequest();
		sfr.setClassifier(request.getClassifier());
		sfr.setContextName(request.getContextName());
		sfr.setDestinationName(request.getDestinationName());
		sfr.setVariableName(request.getVariableName());
		sfr.setFile(request.getFile());

		return stageFile(sfr, null, new File(request.getSourceFilePath()), testMethod, testOperation, valueObject, variableContext, executionContext);
	}

	@Override
	public action_code stageListFile(StageListFileRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		// TODO: Revisit this to allow reference file types
		StageFileRequest sfr = new StageFileRequest();
		sfr.setClassifier(request.getClassifier());
		sfr.setContextName(request.getContextName());
		sfr.setDestinationName(request.getDestinationName());
		sfr.setVariableName(request.getVariableName());

		return stageFile(sfr, request.getListFile(), null, testMethod, testOperation, valueObject, variableContext, executionContext);
	}

	@Override
	public action_code stageDataSetListFile(StageDataSetListFileRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning
	{
		return action_code.handled;
	}
}
