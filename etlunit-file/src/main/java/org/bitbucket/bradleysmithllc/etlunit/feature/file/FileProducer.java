package org.bitbucket.bradleysmithllc.etlunit.feature.file;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;

import java.io.File;

public interface FileProducer
{
	String getName();

	File locateFile(String reference, VariableContext vcontext);
	File locateFileWithClassifier(String reference, String classifier, VariableContext vcontext);
	File locateFileWithContext(String reference, String context, VariableContext vcontext);
	File locateFileWithClassifierAndContext(String reference, String classifier, String context, VariableContext vcontext);
}
