package org.bitbucket.bradleysmithllc.etlunit.feature.file;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;

public class ContextFileBuilder
{
	private final File path;
	private final String fileName;
	private String variableName;
	private String destinationName;
	private String classifier;
	private String contextName;

	public ContextFileBuilder(File path, String fileName)
	{
		this.path = path;
		this.fileName = fileName;
		destinationName = fileName;

		if (path == null)
		{
			throw new IllegalArgumentException("Cannot assign null to file path");
		}

		if (fileName == null)
		{
			throw new IllegalArgumentException("Cannot assign null to file name");
		}
	}

	public ContextFileBuilder withVariableName(String vn)
	{
		variableName = vn;
		return this;
	}

	public ContextFileBuilder withClassifier(String cl)
	{
		classifier = cl;
		return this;
	}

	public ContextFileBuilder withContextName(String cn)
	{
		contextName = cn;
		return this;
	}

	public ContextFileBuilder withDestinationName(String dn)
	{
		if (dn == null)
		{
			destinationName = fileName;
		}
		else
		{
			destinationName = dn;
		}

		return this;
	}

	public ContextFile create()
	{
		return new ContextFile(
			path, fileName, destinationName, variableName, classifier, contextName
		);
	}
}
