package test;

/*
 * #%L
 * java-unit-executor-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.execute.DatabaseInfo;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.ETLUnitDatabase;
import org.h2.util.JdbcUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class IdentityInsert {
	@ETLUnitDatabase(connectionId = "edw")
	public DatabaseInfo subDatabaseInfo = new DatabaseInfo();

	@Test
	public void execute() throws SQLException {
		Connection coo = subDatabaseInfo.openConnection();

		try {
			Statement st = coo.createStatement();

			try {
				st.executeUpdate("Insert into TABLE_WITH_IDENTITY(ID2) VALUES (1)");
				st.executeUpdate("Insert into TABLE_WITH_IDENTITY(ID2) VALUES (2)");
				st.executeUpdate("Insert into TABLE_WITH_IDENTITY(ID2) VALUES (3)");
				st.executeUpdate("Insert into TABLE_WITH_IDENTITY(ID2) VALUES (4)");
			} finally {
				st.close();
			}
		} finally {
			coo.close();
		}
	}

	@Test
	public void currentDatesTimes() throws SQLException {
		Connection coo = subDatabaseInfo.openConnection();

		try {
			Statement st = coo.createStatement();

			try {
				st.executeUpdate("Insert into table_with_dates(ID, DT, TS, TM) VALUES (1, current_date, current_timestamp, current_time)");
			} finally {
				st.close();
			}
		} finally {
			coo.close();
		}
	}

	@Test
	public void insertCurrentDateWithClassifiers() throws SQLException {
		Connection coo = subDatabaseInfo.openConnection();

		try {
			Statement st = coo.createStatement();

			try {
				st.executeUpdate("Insert into TABLE_WITH_TIMESTAMP2(ID, TS) VALUES (1, current_timestamp)");
				st.executeUpdate("Insert into TABLE_WITH_DATE2(ID, TS) VALUES (1, current_date)");
			} finally {
				st.close();
			}
		} finally {
			coo.close();
		}
	}

	@Test
	public void insertCurrentDate() throws SQLException {
		Connection coo = subDatabaseInfo.openConnection();

		try {
			Statement st = coo.createStatement();

			try {
				st.executeUpdate("Insert into TABLE_WITH_TIMESTAMP(ID, TS) VALUES (1, current_timestamp)");
				st.executeUpdate("Insert into TABLE_WITH_DATE(ID, TS) VALUES (1, current_date)");
			} finally {
				st.close();
			}
		} finally {
			coo.close();
		}
	}
}
