package test;

/*
 * #%L
 * java-unit-executor-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.bitbucket.bradleysmithllc.etlunit.ETLTestVM;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.DatabaseInfo;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.ETLUnitDatabase;
import org.bitbucket.bradleysmithllc.etlunit.logging.LoggingSetup;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class LoggerTest {
	@Test
	public void execute() throws SQLException {
		LoggingSetup.setupLoggingToFile(new File("/tmp/log4j"));
		LogManager.getLogger(LoggerTest.class).info("Log4j tested...");
		LoggerFactory.getLogger(LoggerTest.class).info("Slf4j tested...");
		Logger.getLogger(LoggerTest.class.getSimpleName()).info("Java Util Logging tested...");
	}
}
