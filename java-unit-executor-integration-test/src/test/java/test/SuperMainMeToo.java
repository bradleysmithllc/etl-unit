package test;

/*
 * #%L
 * java-unit-executor-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.execute.DatabaseInfo;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.ETLUnitDatabase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SuperMainMeToo extends DuperMainMeToo {
	@ETLUnitDatabase(connectionId = "edw-def")
	public DatabaseInfo superDatabaseInfo = new DatabaseInfo();

	protected boolean supsBefore = false;

	@Before
	public void beforeSuper() {
		Assert.assertNotNull(superDatabaseInfo.jdbcUrl());
		supsBefore = true;
		order += "super,";
	}

	@After
	public void afterSuper() {
		order += "super,";
		Assert.assertEquals("{duper,super,sub}{sub,super,", order);
	}

	@Test
	public void testsAreNotInherited() {
		Assert.fail();
	}
}
