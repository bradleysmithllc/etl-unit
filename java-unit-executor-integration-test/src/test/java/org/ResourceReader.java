package org;

/*
 * #%L
 * java-unit-executor-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ResourceReader {
	@Test
	public void testClassesResolve() throws Exception {
		writeTargetResource("target/classes/org/test_classes_resolve.txt", "FAIL");
		writeTargetResource("target/test-classes/org/test_classes_resolve.txt", "SUCCESS");
		readResource("test_classes_resolve.txt");
	}

	@Test
	public void classesResolve() throws Exception {
		writeTargetResource("target/classes/org/classes_resolve.txt", "FAIL");
		readResource("classes_resolve.txt");
	}

	@Test
	public void testResolve() throws Exception {
		writeTargetResource("target/classes/org/test_resolve.txt", "FAIL_CLASSES");
		readResource("test_resolve.txt");
	}

	@Test
	public void mainResolve() throws Exception {
		readResource("main_resolve.txt");
	}

	@Test
	public void readTestResource() throws Exception {
		readResource("test_resource.txt");
	}

	@Test
	public void readMainResource() throws Exception {
		readResource("main_resource.txt");
	}

	@Test
	public void readTargetClassesResource() throws Exception {
		writeTargetResource("target/classes/org/classes_resource.txt", "TARGET_CLASSES_RESOURCE");
		readResource("classes_resource.txt");
	}

	@Test
	public void readTargetTestClassesResource() throws Exception {
		writeTargetResource("target/test-classes/org/test_classes_resource.txt", "TARGET_TEST_CLASSES_RESOURCE");
		readResource("test_classes_resource.txt");
	}

	private void writeTargetResource(String s, String target_classes_resource) throws IOException {
		File targetFile = new File(s);

		if (System.getProperty("maven-project-root") != null ) {
			targetFile = new File(System.getProperty("maven-project-root"), s);
		}

		FileUtils.write(targetFile, target_classes_resource);
	}

	private void readResource(String name) throws Exception {
		URL url = ResourceReader.class.getResource(name);

		String msg = IOUtils.readURLToString(url);
		throw new TestExecutionError(msg, msg);
	}
}
