package org;

/*
 * #%L
 * java-executor-integration-test
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.execute.DatabaseInfo;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.ETLUnitDatabase;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

public class EdwIIWithMode {
	@ETLUnitDatabase(connectionId = "edw-ii", mode = "lkp")
	public DatabaseInfo databaseInfo = new DatabaseInfo();

	@Test
	public void executeTest() throws SQLException {
		Assert.assertNotNull(databaseInfo.jdbcUrl());

		// connect and query a table
		EdwDefDefaultMode.query(databaseInfo);
	}
}
