package org.bitbucket.bradleysmithllc.etlunit.feature.oracle_database;

/*
 * #%L
 * etlunit-oracle-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.BaseDatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.NullResultSetClient;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Table;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class OracleDatabaseImplementation extends BaseDatabaseImplementation
{
	public String getImplementationId()
	{
		return "oracle";
	}

	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException
	{
		switch (op)
		{
			case createDatabase:
				doCreateDatabase(request.getInitializeRequest());
				break;
			case materializeViews:
			{
				try {
					doMaterializeViews(request.getInitializeRequest());
				} catch (IOException e) {
					throw new RuntimeException(e);
				} catch (TestExecutionError testExecutionError) {
					throw new RuntimeException(testExecutionError);
				}
				break;
			}
		}

		return null;
	}

	private void doMaterializeViews(InitializeRequest initializeRequest) throws IOException, TestExecutionError {
		URL url = getClass().getResource("/oracle_materializeView.vm");
		final String materialize = IOUtils.readURLToString(url);

		final Map<String, String> context = new HashMap<String, String>();
		// grab the renaming strategy for views
		final DatabaseConnection dc = initializeRequest.getConnection();

		String renStrat = dc.getDatabaseProperties().get("view-rename-strategy");
		if (renStrat == null || renStrat.trim().equals(""))
		{
			renStrat = "_V";
		}

		context.put("view-rename-strategy", renStrat);

		jdbcClient.useResultSetScriptResource(
				dc,
				initializeRequest.getMode(),
				new NullResultSetClient() {
					@Override
					public void next(Connection conn, ResultSet st, DatabaseConnection connection, String mode, database_role id) throws Exception {
						// issue a rename for the view
						String viewName = st.getString(1);
						applicationLog.info("Found View [" + viewName + "]");
						context.put("view-name", viewName);

						jdbcClient.useResultSetScript(connection, mode, null, VelocityUtil.writeTemplate(materialize, context), id);
					}
				},
				"oracle_selectViews.vm"
		);
	}

	private void doCreateDatabase(InitializeRequest initializeRequest)
	{
		Exception[] res = executeScripts(initializeRequest.getConnection(), initializeRequest.getMode(), new String[]{"oracle_kill_login", "oracle_kill_table_space", "oracle_kill_temp_table_space", "oracle_create_schema"}, database_role.sysadmin);

		// make sure the create script did not fail.
		if (res[3] != null)
		{
			throw new IllegalStateException("Error creating schema", res[3]);
		}
	}

	@Override
	public String getJdbcUrl(DatabaseConnection dc, String mode, database_role id)
	{
		String serviceName = "";

		Map<String, String> databaseProperties = dc.getDatabaseProperties();

		if (databaseProperties != null)
		{
			String oserviceName = databaseProperties.get("service-name");
			if (oserviceName != null)
			{
				serviceName = "/" + oserviceName;
			}
		}

		return "jdbc:oracle:thin:@//" + dc.getServerName() + ":" + String.valueOf(dc.getServerPort() == -1 ? 1521 : dc.getServerPort()) + serviceName;
	}

	@Override
	public Class getJdbcDriverClass()
	{
		return oracle.jdbc.OracleDriver.class;
	}

	@Override
	protected String getLoginNameImpl(DatabaseConnection dc, String mode) {
		String login = IOUtils.hashToLength(getDatabaseName(dc, mode), 28).toUpperCase();
		return login;
	}

	@Override
	public String getDatabaseName(DatabaseConnection dc, String mode) {
		// basic as possible due to the excessively short oracle db names
		return "EU__" + runtimeSupport.digestIdentifier(dc.getId() + "_" + (mode == null ? "_def_" : mode) + "_" + runtimeSupport.getProjectUID());
	}

	@Override
	public String getPasswordImpl(DatabaseConnection dc, String mode) {
		String pass = IOUtils.hashToLength(getDatabaseName(dc, mode), 28).toUpperCase();
		return pass;
	}

	public String getDefaultSchema(DatabaseConnection databaseConnection, String mode)
	{
		return getLoginName(databaseConnection, mode, database_role.database);
	}

	// all tables are visible since we only see our own schema
	@Override
	public boolean isTableTestVisible(DatabaseConnection dc, String mode, Table table) {
		return table.getType() == Table.type.view || table.getType() == Table.type.table;
	}
}
