package org.bitbucket.bradleysmithllc.etlunit.feature.stage;

/*
 * #%L
 * etlunit-feature-archetype
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.stage.json.stage.stage.StageHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.stage.json.stage.stage.StageRequest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.DataSet;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetContainer;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetUtils;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;

import javax.inject.Inject;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@FeatureModule
public class StageFeatureModule extends AbstractFeature
{
	private static final List<String> prerequisites = Arrays.asList("logging");
	private RuntimeSupport runtimeSupport;
	private Listener listener = new Listener();
	private DataFileManager dataFileManager;

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport manager)
	{
		runtimeSupport = manager;
	}

	@Inject
	public void receiveDataFileManager(DataFileManager manager)
	{
		dataFileManager = manager;
	}

	@Override
	public List<String> getPrerequisites()
	{
		return prerequisites;
	}

	@Override
	public ClassListener getListener()
	{
		return listener;
	}

	class Listener extends NullClassListener implements StageHandler
	{
		public action_code stage(StageRequest operation, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			String _dsName = operation.getDataSetName();
			String dsID = operation.getDataSetId();

			ETLTestPackage _package = runtimeSupport.getCurrentlyProcessingTestPackage();

			Pair<ETLTestPackage, String> results = runtimeSupport.processPackageReference(_package, _dsName);

			File dataSetRoot = new FileBuilder(runtimeSupport.getTestSourceDirectory(results.getLeft())).subdir("dataset").mkdirs().file();

			File ds = new File(dataSetRoot, runtimeSupport.processReference(results.getRight()) + ".dataset");

			if (!ds.exists())
			{
				throw new TestExecutionError("Missing data set - " + ds.getAbsolutePath(), "ERR_MISSING_DATA_SET");
			}

			// open and spin through
			try
			{
				FileReader fileReader = new FileReader(ds);

				try
				{
					ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager, fileReader);

					// if an id is specified, only use that one
					Boolean ignoreDataSetProperties = operation.getIgnoreDataSetProperties();
					boolean ignoreDSProperties = ignoreDataSetProperties == null ? false : ignoreDataSetProperties.booleanValue();

					if (dsID != null)
					{
						try
						{
							DataSet dds = rdsc.locate(dsID);

							broadcast(dds, op, obj, context, econtext, ignoreDSProperties, results.getRight());
						}
						catch (IllegalArgumentException exc)
						{
							throw new TestExecutionError("Missing data set", "ERR_MISSING_DATA_SET_ID", exc);
						}
					}
					else
					{
						// spin through all of them
						while (rdsc.hasNext())
						{
							DataSet dds = rdsc.next();

							broadcast(dds, op, obj, context, econtext, ignoreDSProperties, results.getRight());
						}
					}
				}
				finally
				{
					fileReader.close();
				}
			}
			catch (IOException exc)
			{
				throw new TestExecutionError("Error processing data set", "ERR_IO_ERR", exc);
			}

			return action_code.handled;
		}

		private void broadcast(DataSet dds, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, boolean ignoreDataSetProperties, String dsName) throws TestAssertionFailure, TestExecutionError, TestWarning, IOException
		{
			// create new value
			try
			{
				ETLTestValueObject thisObj = null;

				if (ignoreDataSetProperties)
				{
					// just send the raw properties along - but remote ignore-data-set-properties
					thisObj = obj;
				}
				else
				{
					// merge the data set properties
					thisObj = obj.merge(
							ETLTestParser.loadObject(dds.getProperties().toString()),
							ETLTestValueObject.merge_type.right_merge,
							ETLTestValueObject.merge_policy.recursive
					);
				}

				ETLTestValueObjectBuilder thisObjBuil = new ETLTestValueObjectBuilder(thisObj);

				if (thisObjBuil.hasKey("ignore-data-set-properties"))
				{
					thisObjBuil.removeKey("ignore-data-set-properties");
				}

				// remove id property mandatory for the data set
				String dsId = null;
				
				if (thisObjBuil.hasKey("id"))
				{
					dsId = thisObj.query("id").getValueAsString();
					thisObjBuil.removeKey("id");
				}

				// remove optional data set id
				if (thisObjBuil.hasKey("data-set-id"))
				{
					thisObjBuil.removeKey("data-set-id");
				}

				// remove mandatory data set name
				thisObjBuil.removeKey("data-set-name");

				// map the extracted data into a temporary file and pass the path on to the next operation
				String dsFullName = dsName + (dsId == null ? "" : ("_" + dsId));
				File dataSet = runtimeSupport.createAnonymousTempFileWithPrefix(dsFullName);

				thisObjBuil.key("source-file-path").value(new ETLTestValueObjectImpl(dataSet.getAbsolutePath()));

				ReaderDataSetUtils.extract(dds, dataSet);

				// check for recursion
				ETLTestValueObject parameters = thisObjBuil.toObject();
				if (parameters.getValueAsMap().size() == 1)
				{
					throw new TestExecutionError("Recursive stage call", "ERR_RECURSIVE_STAGE");
				}

				ETLTestOperation sibOp = op.createSibling("stage", parameters);

				try
				{
					econtext.process(sibOp, context);
				}
				catch(TestExecutionError tee)
				{
					// rethrow so it is clear where the error occurred
					throw new TestExecutionError("Error in data set stage operation {" + dsFullName + "}", "ERR_STAGE_OPERATION", tee);
				}
			}
			catch (ParseException e)
			{
				throw new TestExecutionError("Error parsing properties", "ERR_PARSE_ERROR", e);
			}
		}
	}
}
