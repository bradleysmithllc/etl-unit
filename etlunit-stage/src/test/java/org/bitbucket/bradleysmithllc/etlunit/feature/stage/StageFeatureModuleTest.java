package org.bitbucket.bradleysmithllc.etlunit.feature.stage;

/*
 * #%L
 * etlunit-feature-archetype
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class StageFeatureModuleTest extends BaseIntegrationTest
{
	int opCount = 0;

	@Override
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList(new Feature[]
				{
						new TestFeature()
				});
	}

	@Test
	public void test()
	{
		startTest();
	}

	class TestFeature extends AbstractFeature
	{
		@Override
		public long getPriorityLevel()
		{
			return Long.MAX_VALUE;
		}

		@Override
		public ClassListener getListener()
		{
			return new TestListener();
		}

		@Override
		public FeatureMetaInfo getMetaInfo()
		{
			return new FeatureMetaInfo()
			{
				@Override
				public String getFeatureName()
				{
					return "testiiiii";
				}

				@Override
				public String getFeatureConfiguration()
				{
					return null;
				}

				@Override
				public JsonSchema getFeatureConfigurationValidator()
				{
					return null;
				}

				@Override
				public JsonNode getFeatureConfigurationValidatorNode()
				{
					return null;
				}

				@Override
				public Map<String, FeatureOperation> getExportedOperations()
				{
					return null;
				}

				@Override
				public Map<String, FeatureAnnotation> getExportedAnnotations()
				{
					return null;
				}

				@Override
				public boolean isInternalFeature()
				{
					return false;
				}

				@Override
				public String getFeatureUsage()
				{
					return null;
				}

				@Override
				public List<RuntimeOptionDescriptor> getOptions()
				{
					return null;
				}

				@Override
				public String getDescribingClassName()
				{
					return null;
				}

				@Override
				public Feature getDescribing()
				{
					return null;
				}
			};
		}
	}

	@Override
	protected void prepareTest()
	{
		opCount = 0;
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed)
	{
		if (mt.getName().equals("dataSetAllIds"))
		{
			Assert.assertEquals(4, opCount);
		}
	}

	private class TestListener extends NullClassListener
	{
		@Override
		public action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			if (obj.query("error") != null)
			{
				markTestMilestone(mt.getName());
				return action_code.defer;
			}

			boolean checkBody = false;
			String ordinal = "";

			if (mt.getName().equals("testSetPropertiesWin"))
			{
				Assert.assertEquals("test", obj.query("a").getValueAsString());
				markTestMilestone(mt.getName());
			}
			else if (mt.getName().equals("dataSetPropertiesWin"))
			{
				Assert.assertEquals("dataset", obj.query("a").getValueAsString());
				markTestMilestone(mt.getName());
			}
			else if (mt.getName().equals("dataSetId"))
			{
				ordinal = obj.query("ordinal").getValueAsString();
				Assert.assertEquals("20000", ordinal);
				checkBody = true;
				markTestMilestone(mt.getName());
			}
			else if (mt.getName().equals("dataSetAllIds"))
			{
				opCount++;
				int iordinal = op.getOrdinal();
				ordinal = obj.query("ordinal").getValueAsString();
				Assert.assertEquals(String.valueOf(iordinal), ordinal);
				checkBody = true;
			}

			ETLTestValueObject sfp = obj.query("source-file-path");

			Assert.assertNotNull("Missing source file path!", sfp);

			if (checkBody)
			{
				// open up the source file path attribute and compare to the ordinal
				File sourceFile = new File(sfp.getValueAsString());
				Assert.assertTrue(sourceFile.exists());
				try
				{
					String trim = FileUtils.readFileToString(sourceFile).trim();
					Assert.assertEquals(ordinal, trim);
				}
				catch (IOException e)
				{
					throw new AssertionError(e);
				}
			}

			return action_code.handled;
		}
	}

	@Override
	protected List<String> requireMilestones()
	{
		return Arrays.asList(
				"dataSetId",
				"dataSetPropertiesWin",
				"dataSetErr",
				"testSetPropertiesWin"
		);
	}
}
