package org.bitbucket.bradleysmithllc.etlunit.feature.informatica;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileProducer;

import javax.inject.Inject;
import java.io.File;

public class InformaticaFileProducer implements FileProducer
{
	private InformaticaConfiguration informaticaConfiguration;
	private InformaticaRuntimeSupport informaticaRuntimeSupport;

	@Inject
	public void setInformaticaRuntimeSupport(InformaticaRuntimeSupport support)
	{
		informaticaRuntimeSupport = support;
	}

	@Inject
	public void receiveInformaticaConfiguration(InformaticaConfiguration ic)
	{
		informaticaConfiguration = ic;
	}

	public String getName()
	{
		return "informatica";
	}

	public File locateFile(String reference, VariableContext vcontext)
	{
		return locateFileWithClassifierAndDomain(reference, null, informaticaConfiguration.getDefaultDomain());
	}

	public File locateFileWithClassifier(String reference, String classifier, VariableContext vcontext)
	{
		return locateFileWithClassifierAndDomain(reference, classifier, informaticaConfiguration.getDefaultDomain());
	}

	public File locateFileWithContext(String reference, String context, VariableContext vcontext)
	{
		return locateFileWithClassifierAndDomain(reference, null, informaticaConfiguration.getDomains().get(context));
	}

	public File locateFileWithClassifierAndContext(String reference, String classifier, String context, VariableContext vcontext)
	{
		return locateFileWithClassifierAndDomain(reference, classifier, informaticaConfiguration.getDomains().get(context));
	}

	private File locateFileWithClassifierAndDomain(String reference, String classifier, InformaticaDomain domain)
	{
		// grab the remote workspace root
		if (domain == null)
		{
			domain = informaticaConfiguration.getDefaultDomain();
		}

		File subLocation = null;

		// grab the sub directory by classifier
		if (classifier == null || classifier.equals("target"))
		{
			subLocation = informaticaRuntimeSupport.getTgtFilesRemote(domain);
		}
		else if (classifier.equals("source"))
		{
			subLocation = informaticaRuntimeSupport.getSrcFilesRemote(domain);
		}
		else if (classifier.equals("lookup"))
		{
			subLocation = informaticaRuntimeSupport.getLkpFilesRemote(domain);
		}
		else
		{
			throw new IllegalArgumentException("Invalid classifier: " + classifier);
		}

		if (!subLocation.exists())
		{
			throw new IllegalStateException("Directory no longer exists: " + subLocation.getAbsolutePath());
		}

		return new File(subLocation, reference);
	}
}
