package org.bitbucket.bradleysmithllc.etlunit.feature.informatica;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface InformaticaConstants
{

	String ERR_CREATE_FOLDER = "ERR_CREATE_FOLDER";
	String ERR_DELETE_FOLDER = "ERR_DELETE_FOLDER";
	String ERR_CREATE_CONNECTION = "ERR_CREATE_CONNECTION";
	String ERR_DELETE_CONNECTION = "ERR_DELETE_CONNECTION";
	String ERR_CREATE_PRM_FILE = "ERR_CREATE_PRM_FILE";
	String ERR_INVALID_FILE_CLASSIFIER = "ERR_INVALID_FILE_CLASSIFIER";
	String ERR_STAGE_FILE = "ERR_STAGE_FILE";
	String ERR_CREATE_CLIENT = "ERR_CREATE_CLIENT";
	String ERR_EXECUTE_WORKFLOW = "ERR_EXECUTE_WORKFLOW";
	String ERR_EXPORT_WORKFLOW = "ERR_EXPORT_WORKFLOW";
	String ERR_LOAD_WORKFLOW_OFF_FORCE_ENABLED = "ERR_LOAD_WORKFLOW_OFF_FORCE_ENABLED";
}
