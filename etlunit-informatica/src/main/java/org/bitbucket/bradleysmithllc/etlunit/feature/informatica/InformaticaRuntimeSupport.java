package org.bitbucket.bradleysmithllc.etlunit.feature.informatica;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;

import java.io.File;
import java.io.IOException;

public interface InformaticaRuntimeSupport
{
	void enableLoadWorkflows();

	File getInformaticaSourceDirectory();
	File getInformaticaSourceDirectory(String folder);
	File getInformaticaWorkflowParameterDirectory(String folder);

	File getInformaticaWorkflow(String folder, String name);
	File getInformaticaWorkflowParameterTemplate(String folder, String workflowName);

	File getSrcFilesRemote(InformaticaDomain domain);
	File getLkpFilesRemote(InformaticaDomain domain);
	File getTgtFilesRemote(InformaticaDomain domain);
	File getBadFilesRemote(InformaticaDomain domain);
	File getSessionLogFilesRemote(InformaticaDomain domain);

	File getWorkflowLogFilesRemote(InformaticaDomain domain);

	File getParmFilesRemote(InformaticaDomain domain);

	String makeExecutorObjectName(String realFolderName);

	File getWorkspaceFolder(InformaticaDomain domain, RemoteFile.fileType type);

	/**
	 * Returns whether this workflow is believed to be current in the repository
	 * @return
	 */
	boolean checkWorkflowRepositoryStatus(InformaticaRepository informaticaRepository, String folder, String workflow) throws IOException;

	/**
	 * Updates the status of this workflow as being current
	 * @param folder
	 * @param workflow
	 */
	void updateWorkflowRepositoryStatus(InformaticaRepository informaticaRepository, String folder, String workflow);

	boolean checkObjectRepositoryStatus(InformaticaRepository informaticaRepository, String type, String name) throws IOException;

	boolean checkObjectRepositoryStatus(InformaticaRepository informaticaRepository, String type, String name, boolean container) throws IOException;

	void updateObjectRepositoryStatus(InformaticaRepository informaticaRepository, String type, String name);
	void updateObjectRepositoryStatus(InformaticaRepository informaticaRepository, String type, String name, boolean container);

	/**
	 * Wipe's out the flag file.
	 */
	void clearWorkflowRepositoryStatuses();

	File getBaseTagDir();

	File getBaseTagDirForRepository(InformaticaRepository informaticaRepository);

	File getTagFileForWorkflow(InformaticaRepository informaticaRepository, String folder, String workflow);

	File getTagFileForObject(InformaticaRepository informaticaRepository, String type, String name);
}
