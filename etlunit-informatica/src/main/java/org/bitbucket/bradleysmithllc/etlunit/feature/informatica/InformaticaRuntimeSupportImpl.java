package org.bitbucket.bradleysmithllc.etlunit.feature.informatica;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;

public class InformaticaRuntimeSupportImpl implements InformaticaRuntimeSupport {
	private RuntimeSupport runtimeSupport;

	@Inject
	@Named("informatica.loadWorkflows")
	private RuntimeOption loadWorkflows;

	@Override
	public void enableLoadWorkflows()
	{
		if (loadWorkflows != null)
		{
			throw new IllegalStateException();
		}

		loadWorkflows = new RuntimeOption();
		loadWorkflows.setEnabled(true);
	}

	@Inject
	public void setRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	public File getInformaticaSourceDirectory() {
		return runtimeSupport.getFeatureSourceDirectory("informatica");
	}

	public File getInformaticaSourceDirectory(String folder) {
		return new FileBuilder(getInformaticaSourceDirectory()).subdir(folder).mkdirs().file();
	}

	public File getInformaticaWorkflowParameterDirectory(String folder) {
		return new FileBuilder(getInformaticaSourceDirectory(folder)).subdir("ParameterFiles").mkdirs().file();
	}

	public File getInformaticaWorkflow(String folder, String workflowName) {
		return runtimeSupport.resolveFile(new FileBuilder(getInformaticaSourceDirectory(folder)).name(workflowName + ".xml").file());
	}

	public File getInformaticaWorkflowParameterTemplate(String folder, String workflowName) {
		return runtimeSupport.resolveFile(new FileBuilder(getInformaticaWorkflowParameterDirectory(folder)).name(workflowName + ".prm").file());
	}

	public File getSrcFilesRemote(InformaticaDomain domain) {
		return getWorkspaceFolder(domain, RemoteFile.fileType.sourceFiles);
	}

	public File getLkpFilesRemote(InformaticaDomain domain) {
		return getWorkspaceFolder(domain, RemoteFile.fileType.lookupFiles);
	}

	public File getTgtFilesRemote(InformaticaDomain domain) {
		return getWorkspaceFolder(domain, RemoteFile.fileType.targetFiles);
	}

	public File getBadFilesRemote(InformaticaDomain domain) {
		return getWorkspaceFolder(domain, RemoteFile.fileType.badFiles);
	}

	public File getSessionLogFilesRemote(InformaticaDomain domain) {
		return getWorkspaceFolder(domain, RemoteFile.fileType.sessionLog);
	}

	@Override
	public File getWorkflowLogFilesRemote(InformaticaDomain domain) {
		return getWorkspaceFolder(domain, RemoteFile.fileType.workflowLog);
	}

	public File getParmFilesRemote(InformaticaDomain domain) {
		return getWorkspaceFolder(domain, RemoteFile.fileType.parameterFiles);
	}

	@Override
	public String makeExecutorObjectName(String realFolderName) {
		if (loadWorkflows.isEnabled())
		{
			return "__" + runtimeSupport.getProjectUser() + "_" + runtimeSupport.getProjectUID() + "_" + realFolderName;
		}

		return realFolderName;
	}

	@Override
	public File getWorkspaceFolder(InformaticaDomain domain, RemoteFile.fileType type) {
		// domain is fixed now (hopefully) - so defer to it.
		return domain.getWorkspaceDir(type);
	}

	@Override
	public boolean checkWorkflowRepositoryStatus(InformaticaRepository informaticaRepository, String folder, String workflow) {
		try {
			// at this point, if the base has not changed, look for an xml file for the workflow on this executor
			// grab a flag file for the base workflow definition
			File flagFile = getTagFileForWorkflow(informaticaRepository, folder, workflow);

			if (!flagFile.exists()) {
				return false;
			}

			// compare the crc of the xml file to what is stored in the file
			// stored file
			File xmlFile = getInformaticaWorkflow(folder, workflow);

			long baseCrc = FileUtils.checksumCRC32(xmlFile);

			// contents of flag file
			long repoCrc = Long.parseLong(FileUtils.readFileToString(flagFile));

			// return if the checksums are equal
			return baseCrc == repoCrc;
		} catch (IOException exc) {
			runtimeSupport.getApplicationLog().severe("Bad internal error", exc);
		}

		return false;
	}

	@Override
	public void updateWorkflowRepositoryStatus(InformaticaRepository informaticaRepository, String folder, String workflow) {
		// check the base definition
		File flagFile = getTagFileForWorkflow(informaticaRepository, folder, workflow);

		File xmlFile = getInformaticaWorkflow(folder, workflow);

		try {
			long baseCrc = FileUtils.checksumCRC32(xmlFile);

			FileUtils.write(flagFile, String.valueOf(baseCrc));
		} catch (IOException exc) {
			runtimeSupport.getApplicationLog().severe("Cant update flag file", exc);
		}
	}

	@Override
	public boolean checkObjectRepositoryStatus(InformaticaRepository informaticaRepository, String type, String name) throws IOException {
		return checkObjectRepositoryStatus(informaticaRepository, type, name, false);
	}

	@Override
	public boolean checkObjectRepositoryStatus(InformaticaRepository informaticaRepository, String type, String name, boolean container) throws IOException {
		File tagFileForObject = getTagFileForObject(informaticaRepository, type, name);

		if (tagFileForObject.exists())
		{
			if (container)
			{
				if (!tagFileForObject.isDirectory())
				{
					FileUtils.forceDelete(tagFileForObject);
				}
			}
			else
			{
				if (tagFileForObject.isDirectory())
				{
					FileUtils.forceDelete(tagFileForObject);
				}
			}
		}

		return tagFileForObject.exists();
	}

	@Override
	public void updateObjectRepositoryStatus(InformaticaRepository informaticaRepository, String type, String name) {
		updateObjectRepositoryStatus(informaticaRepository, type, name, false);
	}

	@Override
	public void updateObjectRepositoryStatus(InformaticaRepository informaticaRepository, String type, String name, boolean container) {
		try {
			File tagFileForObject = getTagFileForObject(informaticaRepository, type, name);
			if (!container)
			{
				FileUtils.touch(tagFileForObject);
			}
			else
			{
				FileUtils.forceMkdir(tagFileForObject);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void clearWorkflowRepositoryStatuses() {
		try {
			FileUtils.deleteDirectory(getBaseTagDir());
		} catch (IOException e) {
			runtimeSupport.getApplicationLog().severe("Error clearing flag files", e);
		}
	}

	@Override
	public File getBaseTagDir() {
		return new FileBuilder(runtimeSupport.getGeneratedSourceDirectory("informatica")).subdir("repositoryTags").mkdirs().file();
	}

	@Override
	public File getBaseTagDirForRepository(InformaticaRepository informaticaRepository) {
		return new FileBuilder(getBaseTagDir()).subdir(informaticaRepository.getQualifiedName()).mkdirs().file();
	}

	@Override
	public File getTagFileForWorkflow(InformaticaRepository informaticaRepository, String folder, String workflow) {
		return new FileBuilder(getTagFileForObject(informaticaRepository, "folder", folder)).name(makeExecutorObjectName(workflow) + ".chk").file();
	}

	@Override
	public File getTagFileForObject(InformaticaRepository informaticaRepository, String type, String folder) {
		FileBuilder fb = new FileBuilder(getBaseTagDirForRepository(informaticaRepository)).mkdirs().name(makeExecutorObjectName(folder) + "." + type + "Tag");

		return fb.file();
	}
}
