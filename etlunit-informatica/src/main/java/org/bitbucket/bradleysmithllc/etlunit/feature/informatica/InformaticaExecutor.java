package org.bitbucket.bradleysmithllc.etlunit.feature.informatica;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.extend.Extender;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.ContextFile;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.execute.Context;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.execute.ExecuteHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.*;
import org.bitbucket.bradleysmithllc.etlunit.util.regexp.WorkflowNameExpression;
import org.bitbucket.bradleysmithllc.etlunit.util.regexp.WorkflowTagExpression;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InformaticaExecutor implements Extender, ExecuteHandler {
	public static final String INFORMATICA_DISCOVERED_WORKFLOWS = "discoveredWorkflows";

	public static final String INFORMATICA_CREATED_FOLDERS = "createdFolders";
	public static final String INFORMATICA_LOADED_WORKFLOWS = "loadedWorkflows";
	public static final String INFORMATICA_EXPORTED_WORKFLOWS = "exportedWorkflows";
	public static final String INFORMATICA_CREATED_CONNECTIONS = "createdConnections";

	private Log applicationLog;
	private final InformaticaFeatureModule informaticaFeature;
	private final File informaticaResourceDir;
	private RuntimeSupport runtimeSupport;
	private FileRuntimeSupport fileRuntimeSupport;

	@Inject
	@Named("informatica.loadWorkflows")
	private RuntimeOption loadWorkflows = null;

	@Inject
	@Named("informatica.exportWorkflows")
	private RuntimeOption exportWorkflows = null;

	@Inject
	@Named("informatica.prepareWorkspace")
	private RuntimeOption prepareWorkspace = null;

	private LogFileManager logFileManager;

	private InformaticaRuntimeSupport informaticaRuntimeSupport;
	private static final String INFORMATICA_CONTEXTS = "INFORMATICA_CONTEXTS";

	public InformaticaExecutor(InformaticaFeatureModule informaticaFeature, File informaticaResourceDir) {
		this.informaticaFeature = informaticaFeature;
		this.informaticaResourceDir = informaticaResourceDir;
	}

	@Inject
	public void setInformaticaRuntimeSupport(InformaticaRuntimeSupport support) {
		informaticaRuntimeSupport = support;
	}

	@Inject
	public void setLogFileManager(LogFileManager logFileManager) {
		this.logFileManager = logFileManager;
	}

	@Inject
	public void setLogger(@Named("applicationLog") Log log) {
		applicationLog = log;
	}

	@Inject
	public void setRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	@Inject
	public void setFileRuntimeSupport(FileRuntimeSupport runtimeSupport) {
		this.fileRuntimeSupport = runtimeSupport;
	}

	public ClassResponder.action_code process(ETLTestMethod mt, ETLTestOperation etlTestOperation, ETLTestValueObject etlTestValueObject, VariableContext context, ExecutionContext executionContext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return action_code.defer;
	}

	public Feature getFeature() {
		return informaticaFeature;
	}

	private void declareConnectionsIfNeeded(org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.execute.ExecuteRequest operation, ETLTestValueObject etlTestValueObject, ETLTestValueObject infaConnections, VariableContext context) throws TestExecutionError {
		VariableContext topper = context.getTopLevelScope();

		// search for the database.connections path
		if (!context.hasVariableBeenDeclared("database")) {
			// nothing to do
			return;
		}

		ETLTestValueObject database = context.getValue("database");

		if (database != null) {
			ETLTestValueObject connections = database.query("connections");

			if (connections != null) {
				InformaticaRepository ipc = informaticaFeature.repositoryForRequest(etlTestValueObject);

				// iterate through, and create connections in informatica as required
				for (Map.Entry<String, ETLTestValueObject> connEntry : connections.getValueAsMap().entrySet()) {
					// each element here is a connection id mapped to a mode
					String connId = connEntry.getKey();
					ETLTestValueObject value = connEntry.getValue();

					for (Map.Entry<String, ETLTestValueObject> modeEntry : value.getValueAsMap().entrySet()) {
						// here the id is the mode name.  This can be null if it is 'modeless'
						String mode = modeEntry.getKey();
						ETLTestValueObject modeValue = modeEntry.getValue();

						// grab the required information
						String dbname = modeValue.query("database-name").getValueAsString();
						String login = modeValue.query("login-name").getValueAsString();
						String password = modeValue.query("password").getValueAsString();
						String serverAddr = modeValue.query("server-address").getValueAsString();

						// check for a sql-server instance name
						String instanceName = null;

						ETLTestValueObject instNode = modeValue.query("sql-server.instance-name");

						if (instNode != null)
						{
							instanceName = instNode.getValueAsString();
						}

						String domainName = null;

						ETLTestValueObject domNode = modeValue.query("sql-server.domain-name");

						if (domNode != null)
						{
							domainName = domNode.getValueAsString();
						}

						String variableName = "informatica_connections_" + connId + "_" + mode;
						if (infaConnections.query(variableName) == null) {
							new ETLTestValueObjectBuilder(infaConnections).key(variableName).value("true");

							// execute informatica create statement, and place a context variable pointing
							// to the connection name
							// let the database name take care of scoping project and version, we will prepend the user name

							// also, we replace '-' with '_'
							String connectionName =
									new StringBuilder("__")
											.append(runtimeSupport.getProjectUser())
											.append("@")
											.append(connId)
											.append("_")
											.append(mode)
											.append("_")
											.append(runtimeSupport.getProjectUID()).toString();

							connectionName = EtlUnitStringUtils.sanitize(connectionName, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_@", '_');

							applicationLog.info("Creating informatica connection for id = '" + connId + "', mode = '" + mode + "', databaseName = '" + dbname + "', login = '" + login + "', password = '" + password + "', server-name = '" + serverAddr + "', relationalName = '" + connectionName + "'");

							try {
								// create the connection, and store in the variable context  This is hard-coded for sqlserver
								// now, so it will need to be changed when a second database is introduced
								InformaticaRepositoryClient repoclient = ipc.getInformaticaRepositoryClient();

								// first delete the connection, then create.  Do not fail if the connection does not exist
								repoclient.deleteConnection(connectionName, false);
								repoclient.createConnection(
									new ConnectionDetails(
											connectionName,
											new SqlServerConnectionDetails()
													.withServerName(serverAddr)
													.withDatabaseName(dbname)
													.withDatabaseUserName(login)
													.withDatabasePassword(password)
													.withInstanceName(instanceName)
													.withAdDomain(domainName)
									)
								);

								// this is dicey since some might have already been created
								Map<String, ETLTestValueObject> databaseMap;
								Map<String, ETLTestValueObject> connectionsMap;

								if (!topper.hasVariableBeenDeclared("informatica")) {
									databaseMap = new HashMap<String, ETLTestValueObject>();

									ETLTestValueObject obj = new ETLTestValueObjectImpl(databaseMap);

									topper.declareAndSetValue("informatica", obj);

									// create the variable for connections
									connectionsMap = new HashMap<String, ETLTestValueObject>();
									obj = new ETLTestValueObjectImpl(connectionsMap);

									databaseMap.put("connections", obj);
								} else {
									ETLTestValueObject informatica = topper.getValue("informatica");
									connectionsMap = informatica.query("connections").getValueAsMap();
								}

								// look for this connection id
								Map<String, ETLTestValueObject> connectionMap;
								if (connectionsMap.containsKey(connId)) {
									connectionMap = connectionsMap.get(connId).getValueAsMap();
								} else {
									connectionMap = new HashMap<String, ETLTestValueObject>();
									ETLTestValueObject obj = new ETLTestValueObjectImpl(connectionMap);
									connectionsMap.put(connId, obj);
								}

								connectionMap.put(mode, new ETLTestValueObjectImpl(connectionName));
							} catch (Exception e) {
								applicationLog.severe("Error creating relational connection", e);
								throw new RuntimeException(e);
							}
						}
					}
				}
			}
		}

		// at this point, check for a context request.  If found, we need to create all variables to match the requested ones
		Context contextc = operation.getContext();

		if (contextc != null) {
			Map<String, String> map = contextc.getAdditionalProperties();

			if (map != null) {
				for (Map.Entry<String, String> entry : map.entrySet()) {
					// This is a simple aliasing process, and a bit of a workaround.
					// the name is a context variable name, the value is another variable name to set the 'name' context variable to.
					String name = entry.getKey();
					String value = entry.getValue();

					applicationLog.info("Aliasing [" + name + "] to [" + value + "]");
					context.declareAndSetValue(name, context.query(value));
				}
			}
		}
	}

	public action_code execute(final org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.execute.ExecuteRequest operation, ETLTestMethod mt, final ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		InformaticaIntegrationService integrationService = informaticaFeature.integrationServiceForRequest(obj);
		InformaticaRepository informaticaRepository = informaticaFeature.repositoryForRequest(obj);

		InformaticaDomain informaticaDomain = integrationService.getInformaticaDomain();

		// grab the domain object
		ETLTestValueObject repositoryContainer = getInformaticaRepositoryContainer(context, informaticaRepository, informaticaDomain);

		// handle checking for connections here, since the method-level broadcast might be in any order
		// since these connections are created by the database module and there is no explicit dependency
		// between these two
		declareConnectionsIfNeeded(operation, obj, repositoryContainer.query(INFORMATICA_CREATED_CONNECTIONS), context);

		final String folder = operation.getFolder();
		final String workflow = operation.getWorkflow();

		// determine where the param file and the workflow export file are
		File xmlFile = informaticaRuntimeSupport.getInformaticaWorkflow(folder, workflow);

		File paramFileTemplate = informaticaRuntimeSupport.getInformaticaWorkflowParameterTemplate(folder, workflow);

		// record references to the workflow and parameter files
		MetaDataContext infaMc = informaticaFeature.getInformaticaMetaContext();
		MetaDataPackageContext pc = infaMc.createPackageContext(ETLTestPackageImpl.getDefaultPackage(), MetaDataPackageContext.path_type.feature_source);

		try {
			MetaDataArtifact art = pc.createArtifact(folder, informaticaRuntimeSupport.getInformaticaSourceDirectory(folder));

			// snatch the workflow
			art.createContent(xmlFile.getName()).referencedByCurrentTest("workflow");

			art = pc.createArtifact(folder + "/ParameterFiles", informaticaRuntimeSupport.getInformaticaWorkflowParameterDirectory(folder));
			art.createContent(paramFileTemplate.getName()).referencedByCurrentTest("workflow-parameter-template");
		} catch (IOException e) {
			throw new TestExecutionError("Metadata error", e);
		}

		if (!paramFileTemplate.exists()) {
			try {
				IOUtils.writeBufferToFile(paramFileTemplate, new StringBuffer("[Global]"));
			} catch (IOException e) {
				throw new TestExecutionError("Error creating parameter file template", InformaticaConstants.ERR_CREATE_PRM_FILE, e);
			}
		}

		// set standard variables -
		File srcFiles = informaticaRuntimeSupport.getSrcFilesRemote(informaticaDomain);
		File lkpFiles = informaticaRuntimeSupport.getLkpFilesRemote(informaticaDomain);
		File tgtFiles = informaticaRuntimeSupport.getTgtFilesRemote(informaticaDomain);
		File badFiles = informaticaRuntimeSupport.getBadFilesRemote(informaticaDomain);
		File sessionLogFiles = informaticaRuntimeSupport.getSessionLogFilesRemote(informaticaDomain);
		File workflowLogFiles = informaticaRuntimeSupport.getWorkflowLogFilesRemote(informaticaDomain);
		File parmlib = informaticaRuntimeSupport.getParmFilesRemote(informaticaDomain);

		// clear all before proceeding
		try {
			FileUtils.cleanDirectory(srcFiles);
			FileUtils.cleanDirectory(lkpFiles);
			FileUtils.cleanDirectory(tgtFiles);
			FileUtils.cleanDirectory(badFiles);
			FileUtils.cleanDirectory(sessionLogFiles);
			FileUtils.cleanDirectory(workflowLogFiles);
			FileUtils.cleanDirectory(parmlib);
		} catch (IOException exc) {
			throw new TestExecutionError("Error clearing workspace", TestConstants.ERR_IO_ERROR, exc);
		}

		// process staged files
		// look in either the default or user-specified context, and any files listed move them into the
		// appropriate location
		FileContext fileContext = fileRuntimeSupport.getFileContext(obj, context);

		Map<RemoteFile.fileType, List<RemoteFile>> remotes = new HashMap<RemoteFile.fileType, List<RemoteFile>>();

		if (fileContext != null) {
			// in order to facilitate downstream file operations, this module
			// needs to store locations for our classifiers in the file context
			// so that these paths are available to the other processes.
			// if the destination-classifiers has already been defined, then do nothing
			/*
			if (fileContext.query("destination-classifiers") == null) {
				ETLTestValueObject builtObject = new ETLTestValueObjectBuilder(fileContext)
						.key("destination-classifiers")
						.object()
						.key("source")
						.value(srcFiles.getAbsolutePath())
						.key("lookup")
						.value(lkpFiles.getAbsolutePath())
						.key("target")
						.value(tgtFiles.getAbsolutePath())
						.endObject()
						.toObject();
			}
			 */

			Map<String, String> nameMap = new HashMap<String, String>();
			// process files to the working root by classification.  source is default
			for (ContextFile object : fileContext.getContextFileList()) {
				String filePath = object.getFile().getAbsolutePath();

				String classifier = null;
				String classifierObj = object.getClassifierName();

				if (classifierObj != null) {
					classifier = classifierObj;
				}

				File source = new File(filePath);
				String destName = source.getName();
				String destNameObj = object.getDestinationName();

				if (destNameObj != null) {
					destName = destNameObj;
				}

				// post the sucker
				try {
					RemoteFile rf = null;

					if (classifier == null || classifier.equals("source")) {
						// nothing to do - just here for validation
						rf = RemoteFile.newRemoteFile(source, destName, RemoteFile.fileType.sourceFiles);
					} else if (classifier.equals("target")) {
						rf = RemoteFile.newRemoteFile(source, destName, RemoteFile.fileType.targetFiles);
					} else if (classifier.equals("lookup")) {
						rf = RemoteFile.newRemoteFile(source, destName, RemoteFile.fileType.lookupFiles);
					} else {
						throw new TestExecutionError("Invalid file classification: '" + classifier + "'.  Classifier must be one of [source, target, lookup].", InformaticaConstants.ERR_INVALID_FILE_CLASSIFIER);
					}

					List<RemoteFile> remoteFileList = remotes.get(rf.getType());

					if (remoteFileList == null) {
						remoteFileList = new ArrayList<RemoteFile>();
						remotes.put(rf.getType(), remoteFileList);
					}

					// check for duplicates first
					String key = rf.toString();
					if (nameMap.containsKey(key))
					{
						throw new IllegalArgumentException("Duplicate file names not allowed in the same classifier {" + key + "}");
					}

					nameMap.put(key, "");

					remoteFileList.add(rf);
				} catch (IOException e) {
					throw new TestExecutionError("Error copying stage file to remote: source = '" + source + "'", InformaticaConstants.ERR_STAGE_FILE, e);
				}
			}
		}


		try {
			RemoteFile.copyRemoteToLocal(remotes, informaticaDomain);
		} catch (IOException e) {
			throw new TestExecutionError("", e);
		}

		String task = operation.getTask();
		String realWorkflowName = informaticaRuntimeSupport.makeExecutorObjectName(workflow);
		String realFolderName = informaticaRuntimeSupport.makeExecutorObjectName(folder);

		setContext(context, "informaticaWorkflowName", realWorkflowName);
		setContext(context, "informaticaFolderName", realFolderName);

		if (task != null)
		{
			setContext(context, "informaticaTaskName", task);
		}
		else
		{
			setContext(context, "informaticaTaskName", "");
		}

		if (operation.getRunInstanceName() != null)
		{
			setContext(context, "informaticaInstanceName", operation.getRunInstanceName());
		}
		else
		{
			setContext(context, "informaticaInstanceName", "");
		}


		File parm = new File(parmlib, paramFileTemplate.getName());

		try {
			// create a log for this stuff
			/*
			File dir = new FileBuilder(runtimeSupport.getTempRoot()).subdir(op.getQualifiedName()).mkdirs().file();

			if (dir.exists())
			{
				FileUtils.cleanDirectory(dir);
			}
			else
			{
				dir.mkdirs();
			}

			File vcontext = new File(dir, "variableContext.json");
			try
			{
				FileUtils.write(vcontext, context.getTopLevelScope().getJsonNode().toString());
			}
			catch(IllegalArgumentException exc)
			{
				throw new TestExecutionError("", InformaticaConstants.ERR_CREATE_PRM_FILE, exc);
			}
			 */

			String prm = IOUtils.readFileToString(paramFileTemplate);

			/*
			File parmTemp = new File(dir, "parameterTemplate.PRM");
			FileUtils.write(parmTemp, prm);
			*/

			String processedTemplate = VelocityUtil.writeTemplate(prm, context, informaticaRuntimeSupport.getInformaticaWorkflowParameterDirectory(folder));

			/*
			File parmProc = new File(dir, "parameterProcessed.PRM");
			FileUtils.write(parmProc, processedTemplate);
			*/

			IOUtils.writeBufferToFile(parm, new StringBuffer(processedTemplate));

			applicationLog.info("Created parm file " + parm.getAbsolutePath());
		} catch (TestExecutionError e) {
			throw e;
		} catch (Exception e) {
			throw new TestExecutionError("Error creating parameter file", InformaticaConstants.ERR_CREATE_PRM_FILE, e);
		}

		applicationLog.info("Running workflow " + workflow + " using template " + paramFileTemplate.getAbsolutePath() + " and xml definition " + xmlFile.getAbsolutePath());

		final String projectGlom = EtlUnitStringUtils.sanitize(runtimeSupport.getProjectUser() + "_" + runtimeSupport.getProjectUID(), '_');

		// grab the project version
		try {
			if (exportWorkflows.isEnabled()) {
				InformaticaRepositoryClient repoClient = informaticaRepository.getInformaticaRepositoryClient();

				String key = "[[Exported[" + folder + "/" + workflow + "]]]";

				// prevent parallel threads from colliding during export.  Use the canonical workflow and folder name
				// to minimize unnecessary blocking
				synchronized (key.intern())
				{
					if (!context.getTopLevelScope().hasVariableBeenDeclared(key)) {
						applicationLog.info("Exporting " + folder + "/" + workflow);

						try {
							repoClient.exportWorkflowToXml(workflow, folder, xmlFile);
						} catch (InformaticaError err) {
							throw new TestExecutionError("Could not export workflow " + folder + "/" + workflow, InformaticaConstants.ERR_EXPORT_WORKFLOW, err);
						}

						context.getTopLevelScope().declareVariable(key);
					}
				}
			}

			// determine if we should load the workflow and create folders, etc.
			// First, load workflows must be enabled,
			// Then check for force initialize, next check the status
			// flag for the workflow.
			boolean refreshWorkflowXml = false;

			InformaticaRepositoryClient repoClient = informaticaRepository.getInformaticaRepositoryClient();

			ETLTestValueObject workflowList = repositoryContainer.query(INFORMATICA_LOADED_WORKFLOWS);
			boolean workflowHasNotBeenUpdated = workflowList.query(realFolderName + "/" + realWorkflowName) == null;

			if (loadWorkflows.isEnabled()) {
				if (informaticaRuntimeSupport.checkWorkflowRepositoryStatus(informaticaRepository, folder, workflow)) {
					if (workflowHasNotBeenUpdated)
					{
						// tag file thinks we are up to date - query the folder just to make sure
						List<String> repoWorkflowList = repoClient.listWorkflows(realFolderName);

						if (repoWorkflowList == null || !repoWorkflowList.contains(realWorkflowName)) {
							applicationLog.info("Preparing the repository folders due to workflow missing in repository.");
							refreshWorkflowXml = true;
						}
					}
				} else {
					applicationLog.info("Preparing the repository folders due to updated workflow definition.");
					refreshWorkflowXml = true;
				}
			} else {
				applicationLog.info("Ignoring repository folders.");
			}

			if (refreshWorkflowXml) {
				applicationLog.info("Preparing the repository folders");

				// load the folders as needed
				List<String> flist = InformaticaRepositoryClientImpl.getFolderList(xmlFile);

				ETLTestValueObject folderList = repositoryContainer.query(INFORMATICA_CREATED_FOLDERS);

				for (String ifolder : flist) {
					// check to see if this has been created yet
					makeFolder(ifolder, repoClient, informaticaRepository, folderList);
				}

				// load the workflow into the repository
				// check to see if this has been loaded yet
				//String workName = synFolderName + "_______" + synWorkflowName;

				final MapList<String, String> discoveredWorkflows = repositoryContainer.query(INFORMATICA_DISCOVERED_WORKFLOWS).getValueAsType();

				// update the xml
				String rawXML = FileUtils.readFileToString(xmlFile);

				// update the workflow names
				WorkflowTagExpression wte = new WorkflowTagExpression(rawXML);

				rawXML = wte.replaceAll(new WorkflowTagExpression.RegExpIterator() {
					@Override
					public String replaceMatch(WorkflowTagExpression e) {
						// each match here is for a workflow tag.  Find the name attribute and replace
						String tag = e.getTagText();

						WorkflowNameExpression wne = new WorkflowNameExpression(tag);

						return wne.replaceAll(new WorkflowNameExpression.RegExpIterator() {
							@Override
							public String replaceMatch(WorkflowNameExpression e) {
								String workName = informaticaRuntimeSupport.makeExecutorObjectName(e.getWorkflowName());

								discoveredWorkflows.getOrCreate(e.getWorkflowName()).add(workName);

								applicationLog.info("Found workflow [" + e.getWorkflowName() + "], renaming to [" + workName + "]");

								return " NAME = \"" + workName + "\"";
							}
						});
					}
				});

				// write this new xml to a temp file
				File newXML = runtimeSupport.createTempFile(realFolderName + "_" + realWorkflowName + ".xml");

				FileUtils.write(newXML, rawXML);

				String folderPrefix = "__" + projectGlom;
				applicationLog.info("Importing workflow xml [" + realFolderName + "/" + realWorkflowName + "] into integration service [" + integrationService.getIntegrationServiceName() + "] using prefix: " + folderPrefix);

				repoClient.importWorkflowFromXml(folderPrefix, integrationService, newXML);

				if (workflowHasNotBeenUpdated)
				{
					new ETLTestValueObjectBuilder(workflowList).key(realFolderName + "/" + realWorkflowName).value("true");
				}

				// update the status flag
				informaticaRuntimeSupport.updateWorkflowRepositoryStatus(informaticaRepository, folder, workflow);
			} else {
				applicationLog.info("Not preparing the repository folders.");
			}

			InformaticaExecutionClient informaticaIntegrationServiceClient = informaticaFeature.getIntegrationServiceClient(integrationService);

			ParameterFile pmf = new ParameterFile(parm);

			Map<RemoteFile.fileType, List<RemoteFile>> responseRemotes;

			InformaticaExecutionResult informaticaExecutionResult;

			if (task != null) {
				if (operation.getRunInstanceName() != null) {
					applicationLog.info("Executing task " + realFolderName + "/" + realWorkflowName + "." + task + " (using run instance name: '" + operation.getRunInstanceName() + "')");
					informaticaExecutionResult = informaticaIntegrationServiceClient.executeWorkflowTask(realWorkflowName, operation.getRunInstanceName(), task, realFolderName, pmf, remotes);
				} else {
					applicationLog.info("Executing task " + realFolderName + "/" + realWorkflowName + "." + task);
					informaticaExecutionResult = informaticaIntegrationServiceClient.executeWorkflowTask(realWorkflowName, task, realFolderName, pmf, remotes);
				}
			} else {
				if (operation.getRunInstanceName() != null) {
					applicationLog.info("Executing workflow " + realFolderName + "/" + realWorkflowName + " (using run instance name: '" + operation.getRunInstanceName() + "')");
					informaticaExecutionResult = informaticaIntegrationServiceClient.executeWorkflow(realWorkflowName, operation.getRunInstanceName(), realFolderName, pmf, remotes);
				} else {
					applicationLog.info("Executing workflow " + realFolderName + "/" + realWorkflowName);
					informaticaExecutionResult = informaticaIntegrationServiceClient.executeWorkflow(realWorkflowName, realFolderName, pmf, remotes);
				}
			}

			if (informaticaExecutionResult != null) {
				// always handle remote files before any failures . . .
				responseRemotes = informaticaExecutionResult.getWorkSpaceFiles();

				String logName = operation.getLogName();

				if (logName == null) {
					logName = operation.getWorkflow() + (operation.getTask() != null ? ("." + operation.getTask()) : "");
				}

				if (responseRemotes != null) {
					for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> remoteEntry : responseRemotes.entrySet()) {
						RemoteFile.fileType type = remoteEntry.getKey();
						List<RemoteFile> remoteFileList = remoteEntry.getValue();

						for (RemoteFile remoteFile : remoteFileList) {
							try {
								applicationLog.info("Processing remote file : " + remoteFile);

								File destFile = new File(informaticaRuntimeSupport.getWorkspaceFolder(informaticaDomain, type), remoteFile.getFileName());

								applicationLog.info("Caching log file: " + remoteFile + " to " + destFile);

								FileUtils.copyFile(remoteFile.getFile(), destFile);

								if (mt != null) {
									logFileManager.addLogFile(mt, op, destFile, type.name(), logName);
								} else {
									logFileManager.addLogFile(op, destFile, type.name(), logName);
								}
							} catch (Exception e) {
								applicationLog.severe("Error extracting remote file", e);
							}
						}
					}
				}

				// check for a failure to propagate
				if (informaticaExecutionResult.getResult() != InformaticaResponseDTO.result.okay) {
					throw new TestExecutionError(
							"Error executing workflow: " + informaticaExecutionResult.getFailureExc(),
							ObjectUtils.firstNotNull(operation.getErrorId(), InformaticaConstants.ERR_EXECUTE_WORKFLOW)
					);
				}
			}
		} catch (TestExecutionError e) {
			throw e;
		} catch (Exception e) {
			throw new TestExecutionError("Error executing workflow", ObjectUtils.firstNotNull(operation.getErrorId(), InformaticaConstants.ERR_EXECUTE_WORKFLOW), e);
		}

		return ClassResponder.action_code.handled;
	}

	private void setContext(VariableContext context, String workflowName, String realWorkflowName) {
		// record workflow and folder names in the context
		if (!context.hasVariableBeenDeclared(workflowName))
		{
			context.declareVariable(workflowName);
		}

		context.setStringValue(workflowName, realWorkflowName);
	}

	public static ETLTestValueObject getInformaticaRepositoryContainer(VariableContext context, InformaticaRepository informaticaRepository, InformaticaDomain informaticaDomain) {
		ETLTestValueObject container = getInformaticaContainer(context, true);

		if (container.query(informaticaDomain.getDomainName()) == null) {
			new ETLTestValueObjectBuilder(container).key(informaticaDomain.getDomainName()).value(new ETLTestValueObjectImpl(new HashMap()));
		}

		ETLTestValueObject domainContainer = container.query(informaticaDomain.getDomainName());
		ETLTestValueObject repositoryContainer = null;

		// do the same for the repositories
		if (domainContainer.query(informaticaRepository.getRepositoryName()) == null) {
			new ETLTestValueObjectBuilder(domainContainer).key(informaticaRepository.getRepositoryName()).value(new ETLTestValueObjectImpl(new HashMap()));

			// create the list for created folders, workflows, etc.
			repositoryContainer = domainContainer.query(informaticaRepository.getRepositoryName());
			new ETLTestValueObjectBuilder(repositoryContainer)
					.key(INFORMATICA_CREATED_FOLDERS)
					.value(new ETLTestValueObjectImpl(new HashMap<String, ETLTestValueObject>()))
					.key(INFORMATICA_DISCOVERED_WORKFLOWS)
					.value(new ETLTestValueObjectImpl(new HashMapArrayList<String, String>()))
					.key(INFORMATICA_LOADED_WORKFLOWS)
					.value(new ETLTestValueObjectImpl(new HashMap<String, ETLTestValueObject>()))
					.key(INFORMATICA_EXPORTED_WORKFLOWS)
					.value(new ETLTestValueObjectImpl(new HashMap<String, ETLTestValueObject>()))
					.key(INFORMATICA_CREATED_CONNECTIONS)
					.value(new ETLTestValueObjectImpl(new HashMap<String, ETLTestValueObject>()));
		} else {
			repositoryContainer = domainContainer.query(informaticaRepository.getRepositoryName());
		}
		return repositoryContainer;
	}

	private void makeFolder(String folderName, InformaticaRepositoryClient client, InformaticaRepository repository, ETLTestValueObject folderList) throws Exception {
		// if the tag file exists we believe it is good.  Otherwise, delete and create the folder
		String executorFolder = informaticaRuntimeSupport.makeExecutorObjectName(folderName);

		boolean repositoryFolderTagUpToDate = informaticaRuntimeSupport.checkObjectRepositoryStatus(repository, "folder", folderName, true);
		boolean folderHasNotBeenUpdated = folderList.query(executorFolder) == null;
		boolean mustRefresh = !repositoryFolderTagUpToDate;

		if (repositoryFolderTagUpToDate && folderHasNotBeenUpdated) {
			applicationLog.info("Verifying informatica folder really exists: " + executorFolder);

			// check to make sure the folder actually exists
			List<String> realFolders = client.listFolders();

			if (realFolders == null || !realFolders.contains(executorFolder)) {
				// in this case, we have a tag stating the folder exists, but we don't really see it.  Force refresh
				mustRefresh = true;
			}
		}

		if (mustRefresh) {
			applicationLog.info("Creating informatica folder: " + executorFolder);

			// create folder - don't care if it exists already
			client.createFolder(executorFolder);

			informaticaRuntimeSupport.updateObjectRepositoryStatus(repository, "folder", folderName, true);

			if (folderHasNotBeenUpdated) {
				// store it here so that it will be in the queue
				new ETLTestValueObjectBuilder(folderList).key(executorFolder).value("true");
			}
		} else {
			applicationLog.info("Informatica folder up to date: " + executorFolder);
		}
	}

	public static ETLTestValueObject getInformaticaContainer(VariableContext context, boolean create) {
		VariableContext topper = context.getTopLevelScope();

		if (!topper.hasVariableBeenDeclared(INFORMATICA_CONTEXTS)) {
			if (create) {
				topper.declareAndSetValue(INFORMATICA_CONTEXTS, new ETLTestValueObjectImpl(new HashMap()));
			} else {
				return null;
			}
		}

		return topper.getValue(INFORMATICA_CONTEXTS);
	}

	public String coalesce(String... val1) {
		for (String str : val1) {
			if (str != null) {
				return str;
			}
		}

		return null;
	}
}
