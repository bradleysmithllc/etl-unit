package org.bitbucket.bradleysmithllc.etlunit.feature.informatica;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Module;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@FeatureModule
public class InformaticaFeatureModule extends AbstractFeature {
	private static final List<String> prerequisites = Arrays.asList("logging", "file");
	private File etlBinDir;

	private org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.InformaticaFeatureModuleConfiguration informaticaFeatureModuleConfiguration;

	private InformaticaClassListener informaticaClassListener;
	private InformaticaConfiguration informaticaConfiguration;

	private RuntimeSupport runtimeSupport;
	private FileRuntimeSupport fileRuntimeSupport;

	private File etlDomainFile;

	@Inject
	@Named("informatica.clientOutOfProcess")
	private RuntimeOption clientOutOfProcess = null;

	private MetaDataManager metaDataManager;
	private MetaDataContext informaticaMetaContext;

	@Inject
	public void receiveMetaDataManager(MetaDataManager manager)
	{
		metaDataManager = manager;
		informaticaMetaContext = metaDataManager.getMetaData().getOrCreateContext("informatica");
	}

	@Override
	public void dispose()
	{
		for (Map.Entry<String, InformaticaDomain> domain : informaticaConfiguration.getDomains().entrySet())
		{
			try
			{
				domain.getValue().dispose();
			}
			catch(Exception exc)
			{
				System.err.println(exc);
			}
		}
	}

	@Inject
	public void receiveFileRuntimeSupport(FileRuntimeSupport support)
	{
		fileRuntimeSupport = support;
	}

	@Inject
	public void receiveInformaticaConfiguration(org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.InformaticaFeatureModuleConfiguration configuration)
	{
		informaticaFeatureModuleConfiguration = configuration;
	}

	@Override
	protected Injector preCreateSub(Injector inj1)
	{
		etlDomainFile = new File(runtimeSupport.getFeatureConfigurationDirectory("informatica"), "domains.infa");

		if (!etlDomainFile.exists()) {
			try {
				IOUtils.writeBufferToFile(etlDomainFile, new StringBuffer(""));
			} catch (IOException e) {
				applicationLog.severe("", e);
				throw new IllegalArgumentException("Could not create infa domain file path = '" + etlDomainFile.getAbsolutePath() + "', message = '" + e.toString() + "'");
			}
		}

		informaticaConfiguration = postCreate(new InformaticaConfiguration(featureConfiguration.getJsonNode(), runtimeSupport, etlDomainFile));

		final InformaticaRuntimeSupport irs = postCreate(new InformaticaRuntimeSupportImpl());

		return inj1.createChildInjector(new Module()
		{
			public void configure(Binder binder)
			{
				binder.bind(InformaticaRuntimeSupport.class).toInstance(irs);
				binder.bind(InformaticaConfiguration.class).toInstance(informaticaConfiguration);
			}
		});
	}

	@Override
	public void initialize(Injector inj1) {
		fileRuntimeSupport.registerFileProducer(postCreate(new InformaticaFileProducer()));

		if (featureConfiguration == null) {
			throw new IllegalArgumentException("Missing informatica configuration");
		}

		ETLTestValueObject value = configuration.query("vendor-binary-directory");

		if (value == null) {
			throw new IllegalArgumentException("Configuration property 'vendor-binary-directory' not present");
		}

		etlBinDir = new File(value.getValueAsString());

		File resourceDirectory = runtimeSupport.getFeatureSourceDirectory("informatica");

		applicationLog.info("Using resource directory: " + resourceDirectory.getAbsolutePath());

		InformaticaExecutor exe = postCreate(new InformaticaExecutor(this, resourceDirectory));
		informaticaClassListener = postCreate(new InformaticaClassListener(this, exe));
	}

	@Override
	public ClassListener getListener() {
		return informaticaClassListener;
	}

	@Override
	public List<String> getPrerequisites() {
		return prerequisites;
	}

	@Inject
	public void setRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	/*
	String integrationService(InformaticaRepository repository, ETLTestValueObject etlTestValueObject) throws TestExecutionError {
		ETLTestValueObject is = etlTestValueObject.query("informatica-integration-service");

		String effectiveIs = effectiveRepository.getDefaultIntegrationService();

		if (is != null) {
			String isValueAsString = is.getValueAsString();
			if (!effectiveRepository.getIntegrationServices().contains(isValueAsString)) {
				throw new IllegalArgumentException("Integration service " + isValueAsString + " not found in repository " + effectiveRepository.getRepositoryName() + " in domain " + effectiveDomain.getDomainName());
			}

			effectiveIs = isValueAsString;
		}
	}
	 */

	InformaticaIntegrationService integrationServiceForRequest(ETLTestValueObject etlTestValueObject) throws TestExecutionError
	{
		ETLTestValueObject domain = etlTestValueObject.query("informatica-domain");
		ETLTestValueObject is = etlTestValueObject.query("informatica-integration-service");

		InformaticaDomain effectiveDomain = informaticaConfiguration.getDefaultDomain();

		if (domain != null) {
			String domainValueAsString = domain.getValueAsString();
			effectiveDomain = informaticaConfiguration.getDomains().get(domainValueAsString);

			if (effectiveDomain == null) {
				throw new IllegalArgumentException("Domain named " + domainValueAsString + " not found in Informatica configuration");
			}
		}

		InformaticaIntegrationService effectiveIs = effectiveDomain.getDefaultIntegrationService();

		if (is != null) {
			String isValueAsString = is.getValueAsString();
			effectiveIs = effectiveDomain.getIntegrationService(isValueAsString);
			if (effectiveIs == null) {
				throw new IllegalArgumentException("Integration service " + isValueAsString + " not found in domain " + effectiveDomain.getDomainName());
			}
		}

		return effectiveIs;
	}

	InformaticaRepository repositoryForRequest(ETLTestValueObject etlTestValueObject) throws TestExecutionError
	{
		ETLTestValueObject domain = etlTestValueObject.query("informatica-domain");
		ETLTestValueObject repository = etlTestValueObject.query("informatica-repository");

		InformaticaDomain effectiveDomain = informaticaConfiguration.getDefaultDomain();

		if (domain != null) {
			String domainValueAsString = domain.getValueAsString();
			effectiveDomain = informaticaConfiguration.getDomains().get(domainValueAsString);

			if (effectiveDomain == null) {
				throw new IllegalArgumentException("Domain named " + domainValueAsString + " not found in Informatica configuration");
			}
		}

		InformaticaRepository effectiveRepository = effectiveDomain.getDefaultRepository();

		if (repository != null) {
			String repositoryValueAsString = repository.getValueAsString();
			effectiveRepository = effectiveDomain.getRepository(repositoryValueAsString);

			if (effectiveRepository == null) {
				throw new IllegalArgumentException("Repository named " + repositoryValueAsString + " not found in Informatica configuration for domain " + effectiveDomain.getDomainName());
			}
		}

		return effectiveRepository;
	}

	File getBinDir(InformaticaDomain domain)
	{
		// get the client version for the binary
		String ver = domain.getClientVersion();
		// construct a path to the power center binaries using a well-known path combined with the version number

		return new FileBuilder(etlBinDir).subdir("PowerCenter").subdir(ver).subdir("server").subdir("bin").file();
	}

	public InformaticaExecutionClient getIntegrationServiceClient(InformaticaIntegrationService informaticaIntegrationService) throws Exception
	{
		// before defaulting to the int svc client, look for a web services hub.  If found, use that
		InformaticaRepository repo = informaticaIntegrationService.getInformaticaRepository();

		InformaticaWebServicesHub iwsh = repo.getDefaultWebServicesHub();

		if (iwsh != null)
		{
			return iwsh.getInformaticaWebServicesHubClient(informaticaIntegrationService);
		}
		else
		{
			return informaticaIntegrationService.getInformaticaIntegrationServiceClient();
		}
	}

	public InformaticaConfiguration getInformaticaConfiguration() {
		return informaticaConfiguration;
	}

	public MetaDataContext getInformaticaMetaContext() {
		return informaticaMetaContext;
	}
}
