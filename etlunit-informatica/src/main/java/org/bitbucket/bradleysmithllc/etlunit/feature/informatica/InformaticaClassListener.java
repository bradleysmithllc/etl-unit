package org.bitbucket.bradleysmithllc.etlunit.feature.informatica;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.create_informatica_connection.CreateInformaticaConnectionRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.create_informatica_folder.CreateInformaticaFolderRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.delete_informatica_connection.DeleteInformaticaConnectionRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.delete_informatica_folder.DeleteInformaticaFolderRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.execute.ExecuteHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.json.informatica.execute.ExecuteRequest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public class InformaticaClassListener extends NullClassListener implements ExecuteHandler {
	private final InformaticaFeatureModule informaticaFeatureModule;
	private final InformaticaExecutor informaticaExecutor;
	private InformaticaConfiguration informaticaConfiguration;
	private InformaticaRuntimeSupport informaticaRuntimeSupport;
	private RuntimeSupport runtimeSupport;

	@Inject
	@Named("informatica.forceInitializeRepository")
	private RuntimeOption forceInitializeRepository = null;

	@Inject
	@Named("informatica.loadWorkflows")
	private RuntimeOption loadWorkflows;

	@Inject
	@Named("informatica.cleanRepository")
	private RuntimeOption cleanRepository;

	private Log applicationLog;

	public InformaticaClassListener(InformaticaFeatureModule informaticaFeatureModule, InformaticaExecutor exe) {
		this.informaticaFeatureModule = informaticaFeatureModule;
		informaticaExecutor = exe;
	}

	@Inject
	public void receiveInformaticaRuntimeSupport(InformaticaRuntimeSupport support) {
		informaticaRuntimeSupport = support;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support) {
		runtimeSupport = support;
	}

	@Inject
	public void receiveInformaticaConfiguration(InformaticaConfiguration icon) {
		informaticaConfiguration = icon;
	}

	@Override
	public void begin(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// before each method, perform a deep cleanse of the working root
		InformaticaConfiguration config = informaticaFeatureModule.getInformaticaConfiguration();

		Map<String, InformaticaDomain> domMap = config.getDomains();

		Collection<InformaticaDomain> domSet = domMap.values();

		for (InformaticaDomain domain : domSet) {
			File workingRoot = domain.getLocalizedWorkingRoot();

			if (workingRoot.exists()) {
				applicationLog.info("Purging remote workspace at path: " + workingRoot);
				IOUtils.purge(workingRoot, true);
				applicationLog.info("Done");
			}
		}
	}

	@Inject
	public void setLogger(@Named("applicationLog") Log logger) {
		this.applicationLog = logger;
	}

	@Override
	public action_code process(ETLTestMethod mt, ETLTestOperation etlTestOperation, ETLTestValueObject etlTestValueObject, VariableContext context, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		InformaticaRepository informaticaRepository = informaticaFeatureModule.repositoryForRequest(etlTestValueObject);

		if (etlTestOperation.getOperationName().equals("createInformaticaFolder")) {
			CreateInformaticaFolderRequest req = getRequestObject(etlTestValueObject, CreateInformaticaFolderRequest.class);
			InformaticaRepositoryClient ipc = informaticaRepository.getInformaticaRepositoryClient();

			try {
				ipc.createFolder(req.getFolder());
			} catch (Exception e) {
				applicationLog.severe("Failed to execute Informatica Command", e);
				throw new TestExecutionError("Failed to execute Informatica Command", InformaticaConstants.ERR_CREATE_FOLDER, e);
			}

			return action_code.handled;
		}

		if (etlTestOperation.getOperationName().equals("deleteInformaticaFolder")) {
			DeleteInformaticaFolderRequest req = getRequestObject(etlTestValueObject, DeleteInformaticaFolderRequest.class);
			InformaticaRepositoryClient ipc = informaticaRepository.getInformaticaRepositoryClient();

			try {
				ipc.deleteFolder(req.getFolder());
			} catch (Exception e) {
				throw new TestExecutionError("Failed to execute Informatica Command", InformaticaConstants.ERR_DELETE_FOLDER, e);
			}

			return action_code.handled;
		}

		if (etlTestOperation.getOperationName().equals("createInformaticaConnection")) {
			CreateInformaticaConnectionRequest req = getRequestObject(etlTestValueObject, CreateInformaticaConnectionRequest.class);
			InformaticaRepositoryClient ipc = informaticaRepository.getInformaticaRepositoryClient();

			try {
				ipc.createConnection(
					new ConnectionDetails(
						req.getRelationalConnectionName(),
						new SqlServerConnectionDetails()
								.withServerName(req.getServerName())
								.withDatabaseName(req.getDatabaseName())
								.withDatabaseUserName(req.getDatabaseUserName())
								.withDatabasePassword(req.getDatabasePassword())
					)
				);
			} catch (Exception e) {
				throw new TestExecutionError("Failed to execute Informatica Command", InformaticaConstants.ERR_CREATE_CONNECTION, e);
			}

			return action_code.handled;
		}

		if (etlTestOperation.getOperationName().equals("deleteInformaticaConnection")) {
			DeleteInformaticaConnectionRequest req = getRequestObject(etlTestValueObject, DeleteInformaticaConnectionRequest.class);
			InformaticaRepositoryClient ipc = informaticaRepository.getInformaticaRepositoryClient();

			try {
				ipc.deleteConnection(
						req.getRelationalConnectionName()
				);
			} catch (Exception e) {
				throw new TestExecutionError("Failed to execute Informatica Command", InformaticaConstants.ERR_DELETE_CONNECTION, e);
			}

			return action_code.handled;
		}

		return action_code.defer;
	}

	@Override
	public action_code execute(ExecuteRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext)
			throws TestAssertionFailure, TestExecutionError, TestWarning {
		return informaticaExecutor.execute(request, mt, testOperation, valueObject, variableContext, executionContext);
	}

	public <T> T getRequestObject(ETLTestValueObject obj, Class<T> type) throws TestExecutionError {
		ObjectMapper objm = new ObjectMapper();

		try {
			String content = obj.getJsonNode().toString();

			applicationLog.info("Deserializing request object: " + content + " into type " + type);

			return objm.readValue(content, type);
		} catch (IOException e) {
			throw new TestExecutionError("Error converting object to request", e);
		}
	}

	@Override
	public void beginTests(VariableContext context, int executorId) {
		applicationLog.info(cleanRepository.isEnabled() ? "Cleaning repository." : "Leaving turds in repository.");
		applicationLog.info(loadWorkflows.isEnabled() ? "Loading workflows from project" : "Using workflows resident in informatica repository");

		MetaDataContext mc = informaticaFeatureModule.getInformaticaMetaContext();
		MetaDataPackageContext pc = mc.createPackageContext(ETLTestPackageImpl.getDefaultPackage(), MetaDataPackageContext.path_type.feature_source);

		// create a package, artifact and contents for the informatica workflows and parameter files
		File infaRoot = informaticaRuntimeSupport.getInformaticaSourceDirectory();

		try {
			for (File infaFolder : infaRoot.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					// we don't want .svn directories
					return pathname.isDirectory() && !pathname.getName().startsWith(("."));
				}
			})) {
				// each entry here is an informatica folder with workflows and a ParameterFiles directory
				final MetaDataArtifact art = pc.createArtifact(infaFolder.getName(), infaFolder);

				// grab the contents
				infaFolder.listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						if (pathname.getName().toLowerCase().endsWith(".xml")) {
							art.createContent(pathname.getName());
						}

						return false;
					}
				});

				File informaticaWorkflowParameterDirectory = informaticaRuntimeSupport.getInformaticaWorkflowParameterDirectory(infaFolder.getName());
				final MetaDataArtifact artParm = pc.createArtifact(infaFolder.getName() + "/ParameterFiles", informaticaWorkflowParameterDirectory);

				// grab the contents
				informaticaWorkflowParameterDirectory.listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						if (pathname.getName().toLowerCase().endsWith(".prm")) {
							artParm.createContent(pathname.getName());
						}

						return false;
					}
				});
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	// this is where we clean the repo if needed
	@Override
	public void endTests(VariableContext context, int executorId) {
		InformaticaRepositoryClientImpl.resetClientCount();

		if (cleanRepository.isEnabled()) {
			ETLTestValueObject container = InformaticaExecutor.getInformaticaContainer(context, false);

			if (container != null) {
				Map<String, ETLTestValueObject> map = container.getValueAsMap();

				for (Map.Entry<String, ETLTestValueObject> entry : map.entrySet()) {
					applicationLog.info("Purging temporary informatica folders for domain: " + entry.getKey());

					InformaticaDomain domain = informaticaConfiguration.getDomain(entry.getKey());

					Map<String, ETLTestValueObject> mapRepo = entry.getValue().getValueAsMap();

					for (Map.Entry<String, ETLTestValueObject> entryRepo : mapRepo.entrySet()) {
						applicationLog.info("Purging temporary informatica folders for repository: " + entryRepo.getKey());

						InformaticaRepository repository = domain.getRepository(entryRepo.getKey());

						InformaticaRepositoryClient client = null;
						try {
							client = repository.getInformaticaRepositoryClient();

							if (loadWorkflows.isEnabled()) {
								applicationLog.info("Purging temporary informatica folders.");

								Map<String, ETLTestValueObject> fMap = entryRepo.getValue().query(InformaticaExecutor.INFORMATICA_CREATED_FOLDERS).getValueAsMap();

								for (Map.Entry<String, ETLTestValueObject> folderEntry : fMap.entrySet()) {
									applicationLog.info("Purging temporary informatica folder: " + folderEntry.getKey());

									client.deleteFolder(folderEntry.getKey());
								}
							} else {
								applicationLog.info("Nothing to clean up for repository " + repository.getQualifiedName() + " since load workflows is turned off.");
							}

							Map<String, ETLTestValueObject> fMap = entryRepo.getValue().query(InformaticaExecutor.INFORMATICA_CREATED_CONNECTIONS).getValueAsMap();

							for (Map.Entry<String, ETLTestValueObject> connectionEntry : fMap.entrySet()) {
								applicationLog.info("Purging temporary informatica folder: " + connectionEntry.getKey());

								client.deleteFolder(connectionEntry.getKey());
							}
						} catch (Exception testExecutionError) {
							throw new RuntimeException(testExecutionError);
						}
					}
				}
			}

			applicationLog.info("Purging connection objects.");
		} else {
			applicationLog.info("Not cleaning repository.");
		}
	}

	@Override
	public void endTests(VariableContext context) {
		if (cleanRepository.isEnabled()) {
			applicationLog.info("Clearing repository status flags due to clean repository setting.");
			informaticaRuntimeSupport.clearWorkflowRepositoryStatuses();
		}
	}

	@Override
	public void beginTests(VariableContext context) throws TestExecutionError {
		// if force initialize is enabled, but load workflows is off, this is an error
		if (forceInitializeRepository.isEnabled() && !loadWorkflows.isEnabled())
		{
			throw new TestExecutionError("Force initialize repository is enabled but load workflows is disabled", InformaticaConstants.ERR_LOAD_WORKFLOW_OFF_FORCE_ENABLED);
		}

		if (forceInitializeRepository.isEnabled()) {
			applicationLog.info("Clearing repository status flags due to force initialize setting.");
			informaticaRuntimeSupport.clearWorkflowRepositoryStatuses();
		}
	}
}
