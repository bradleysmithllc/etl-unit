package org.bitbucket.util.regexp.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.logging.LoggingSetup;
import org.bitbucket.bradleysmithllc.etlunit.util.regexp.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class RegexpTest {
	@Test
	public void testInfaConcurrentWorkflowExpression()
	{
		InfaConcurrentWorkflowExpression exp = InfaConcurrentWorkflowExpression.match("sdasd");

		Assert.assertFalse(exp.matches());
		Assert.assertFalse(exp.hasNext());

		exp = InfaConcurrentWorkflowExpression.match("Connected to Integration Service: [dev_is].\r\n"
						+ "Starting workflow [wkf_LOAD_STORE_DAILY_LABOR_PAYCLOCK]\r\n"
						+ "Waiting for workflow [wkf_LOAD_STORE_DAILY_LABOR_PAYCLOCK] to complete\r\n"
						+ "ERROR: Workflow [POS:wkf_LOAD_STORE_DAILY_LABOR_PAYCLOCK[version 16]]: Could not acquire the execute lock for Workflow [wkf_LOAD_STORE_DAILY_LABOR_PAYCLOCK] [].  Please check the Integration Service log for more information.\r\n"
						+ "INFO: Disconnecting from Integration Service\r\n");

		Assert.assertFalse(exp.matches());
		Assert.assertTrue(exp.hasNext());
		Assert.assertEquals(exp.getWorkflowName(), "LOAD_STORE_DAILY_LABOR_PAYCLOCK");
		Assert.assertFalse(exp.hasNext());
	}

	@Test
	public void testPowermart()
	{
		String s = "<POWERMART CREATION_DATE=\"11/30/2010 12:00:00\" REPOSITORY_VERSION=\"179.88\">".replaceAll("<POWERMART CREATION_DATE=\"\\d{1,2}/\\d{1,2}/\\d{4} \\d{1,2}:\\d{1,2}:\\d{1,2}\" REPOSITORY_VERSION=\"(\\d{1,4}.\\d{1,9})\">", "<POWERMART CREATION_DATE=\"11/30/2010 12:00:00\" REPOSITORY_VERSION=\"$1\">");

		System.out.println(s);
	}

	@Test
	public void testPmRepExpr()
	{
		InfaExportResultExpression iere = InfaExportResultExpression.match("Exported 13 object(s) - 0 Error(s), - 0 Warning(s)");

		Assert.assertTrue(iere.matches());

		Assert.assertEquals(13, iere.getObjects());
		Assert.assertEquals(0, iere.getErrors());
		Assert.assertEquals(0, iere.getWarnings());

		iere = InfaExportResultExpression.match("Exported 1 object(s) - 12 Error(s), - 9 Warning(s)");

		Assert.assertTrue(iere.matches());

		Assert.assertEquals(1, iere.getObjects());
		Assert.assertEquals(12, iere.getErrors());
		Assert.assertEquals(9, iere.getWarnings());

		InfaServerNameExpression isne = InfaServerNameExpression.match("<WORKFLOW DESCRIPTION =\"\" ISENABLED =\"YES\" ISRUNNABLESERVICE =\"NO\" ISSERVICE =\"NO\" ISVALID =\"YES\" NAME =\"wkf_LOAD_ITMMVMT_AUD\" REUSABLE_SCHEDULER =\"NO\" SCHEDULERNAME =\"Scheduler\" SERVERNAME =\"dev_is\" SERVER_DOMAINNAME =\"Domain_etldev01\" SUSPEND_ON_ERROR =\"NO\" TASKS_MUST_RUN_ON_SERVER =\"NO\" VERSIONNUMBER =\"5\">");

		Assert.assertTrue(isne.hasNext());
		Assert.assertEquals("dev_is", isne.getInformaticaIntegrationService());
		Assert.assertFalse(isne.hasNext());

		Assert.assertEquals("<WORKFLOW DESCRIPTION =\"\" ISENABLED =\"YES\" ISRUNNABLESERVICE =\"NO\" ISSERVICE =\"NO\" ISVALID =\"YES\" NAME =\"wkf_LOAD_ITMMVMT_AUD\" REUSABLE_SCHEDULER =\"NO\" SCHEDULERNAME =\"Scheduler\" SERVERNAME =\"svn_is\" SERVER_DOMAINNAME =\"Domain_etldev01\" SUSPEND_ON_ERROR =\"NO\" TASKS_MUST_RUN_ON_SERVER =\"NO\" VERSIONNUMBER =\"5\">", isne.replaceAll("SERVERNAME =\"svn_is\""));

		InfaDomainNameExpression idne = InfaDomainNameExpression.match("<WORKFLOW DESCRIPTION =\"\" ISENABLED =\"YES\" ISRUNNABLESERVICE =\"NO\" ISSERVICE =\"NO\" ISVALID =\"YES\" NAME =\"wkf_LOAD_ITMMVMT_AUD\" REUSABLE_SCHEDULER =\"NO\" SCHEDULERNAME =\"Scheduler\" SERVERNAME =\"dev_is\" SERVER_DOMAINNAME =\"Domain_etldev01\" SUSPEND_ON_ERROR =\"NO\" TASKS_MUST_RUN_ON_SERVER =\"NO\" VERSIONNUMBER =\"5\">", "Domain_etldev01");

		Assert.assertTrue(idne.hasNext());
		Assert.assertEquals("Domain_etldev01", idne.getdomainNameParameter());
		Assert.assertFalse(idne.hasNext());
	}

	@Test
	public void testFolderShortcutNames()
	{
		FolderShortcutExpression fse = FolderShortcutExpression.match("FOLDERNAME =\"SHARED_EDW\"");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getShortcutFolderName());

		fse = FolderShortcutExpression.match("FOLDERNAME=\"SHARED_EDW\"");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getShortcutFolderName());

		fse = FolderShortcutExpression.match("FOLDERNAME= \"SHARED_EDW\"");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getShortcutFolderName());
		fse = FolderShortcutExpression.match("FOLDERNAME = \"SHARED_EDW\"");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getShortcutFolderName());
	}

	@Test
	public void testFolderNames()
	{
		FolderNameExpression fse = FolderNameExpression.match("<FOLDER NAME=\"SHARED_EDW\" GROUP=\"\" OWNER=\"Administrator\" SHARED=\"SHARED\" DESCRIPTION=\"\" PERMISSIONS=\"rwx---r--\" UUID=\"6745aa29-d1d0-4ff3-a696-1bfea2539316\">");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getFolderName());

		fse = FolderNameExpression.match("<FOLDER NAME=\"EDW_POS_TICKET\" GROUP=\"\" OWNER=\"Administrator\" SHARED=\"NOTSHARED\" DESCRIPTION=\"\" PERMISSIONS=\"rwx---r--\" UUID=\"7155f0e9-f99f-4aac-a9fe-80498f3be3cb\">");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("EDW_POS_TICKET", fse.getFolderName());
	}

	@Test
	public void testTestNames()
	{
		class TestName
		{
			String testText;
			String className;
			String methodName;

			TestName(String text)
			{
				this(text, null, null);
			}

			TestName(String text, String cls)
			{
				this(text, cls, null);
			}

			TestName(String text, String cls, String methd)
			{
				testText = text;
				className = cls;
				methodName = methd;
			}
		}

		TestName [] tests = {
				new TestName("1", "1"),
				new TestName("_", "_"),
				new TestName("1_1", "1_1"),
				new TestName("1_a", "1_a"),
				new TestName("class.method", "class", "method"),

				new TestName("stg", "stg"),
				new TestName("^stg", "^stg"),
				new TestName("^stg$", "^stg$"),
				new TestName("stg$", "stg$"),

				new TestName("stg.stg", "stg", "stg"),
				new TestName("stg.^stg", "stg", "^stg"),
				new TestName("stg.^stg$", "stg", "^stg$"),

				new TestName("^stg.stg", "^stg", "stg"),
				new TestName("^stg.^stg", "^stg", "^stg"),
				new TestName("^stg.^stg$", "^stg", "^stg$"),
				new TestName("^stg.stg$", "^stg", "stg$"),

				new TestName("^stg$.stg", "^stg$", "stg"),
				new TestName("^stg$.^stg", "^stg$", "^stg"),
				new TestName("^stg$.^stg$", "^stg$", "^stg$"),
				new TestName("^stg$.stg$", "^stg$", "stg$"),

				new TestName("stg$.stg", "stg$", "stg"),
				new TestName("stg$.^stg", "stg$", "^stg"),
				new TestName("stg$.^stg$", "stg$", "^stg$"),
				new TestName("stg$.stg$", "stg$", "stg$"),
		};

		for (int i = 0; i < tests.length; i++)
		{
			TestNameExpression tne = new TestNameExpression(tests[i].testText);

			Assert.assertEquals(tests[i].className != null, tne.matches());

			String it = tne.group();

			if (tests[i].className != null)
			{
				Assert.assertTrue(tne.hasClassName());
				Assert.assertEquals(tests[i].className, tne.getClassName());
			}

			if (tests[i].methodName != null)
			{
				Assert.assertTrue(tne.hasTestName());
				Assert.assertEquals(tests[i].methodName, tne.getTestName());
			}
		}
	}

	@Test
	public void testFolderExists()
	{
		InfaCreateFolderExistsExpression icfee = new InfaCreateFolderExistsExpression("A folder with name __BI_1701_POS_BSMITH_WS already exists in repository ci_pc_repo. Try again!", "__BI_1701_POS_BSMITH_WS", "ci_pc_repo");

		Assert.assertTrue(icfee.matches());

		icfee = new InfaCreateFolderExistsExpression("A folder with name __BI_1701_POS_BSMITH_WS already exists in repository dev_pc_repo. Try again!", "__BI_1701_POS_BSMITH_WS", "dev_pc_repo");

		Assert.assertTrue(icfee.matches());

		icfee = new InfaCreateFolderExistsExpression("A folder with name __BI_1701_POS_BSMITH_WS already exists in repository qa_pc_repo. Try again!", "__BI_1701_POS_BSMITH_WS", "qa_pc_repo");

		Assert.assertTrue(icfee.matches());
	}

	@Test
	public void testTestFolders()
	{
		TestFolderNameExpression tfne = new TestFolderNameExpression("__POS");

		Assert.assertTrue(tfne.matches());
		Assert.assertEquals(tfne.getFolderName(), "__POS");

		tfne = new TestFolderNameExpression("_POS");

		Assert.assertFalse(tfne.matches());

		tfne = new TestFolderNameExpression("POS__POS");

		Assert.assertFalse(tfne.matches());

		tfne = new TestFolderNameExpression("POS__POS\r\n__POS");

		Assert.assertTrue(tfne.hasNext());
		Assert.assertEquals(tfne.getFolderName(), "__POS");
	}

	@Test
	public void workflowTag()
	{
		WorkflowTagExpression wte = new WorkflowTagExpression("<WORKFLOW DESCRIPTION =\"\" ISENABLED =\"YES\" ISRUNNABLESERVICE =\"NO\" ISSERVICE =\"NO\" ISVALID =\"YES\" NAME = \"wkf_INT_CONVERSION_TEST\" REUSABLE_SCHEDULER =\"NO\" SCHEDULERNAME = \"Scheduler\" SUSPEND_ON_ERROR =\"NO\" TASKS_MUST_RUN_ON_SERVER =\"NO\" VERSIONNUMBER =\"1\">");

		Assert.assertTrue(wte.matches());
	}

	@Test
	public void workflowName()
	{
		WorkflowNameExpression wte = new WorkflowNameExpression(" NAME = \"wkf_INT_CONVERSION_TEST\"");
		Assert.assertTrue(wte.matches());

		wte = new WorkflowNameExpression("NAME = \"wkf_INT_CONVERSION_TEST\"");
		Assert.assertFalse(wte.matches());

		wte = new WorkflowNameExpression("SCHEDULERNAME = \"wkf_INT_CONVERSION_TEST\"");
		Assert.assertFalse(wte.matches());
	}
}
