package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaFeatureModule;
import org.bitbucket.bradleysmithllc.json.validator.*;
import org.junit.Test;

import java.util.List;

public class SchemaTest
{
	/*
	@Test
	public void executeSchema() throws JsonSchemaValidationException
	{
		List<JsonSchema> schemae = new InformaticaFeatureModule().getMetaInfo().getExportedOperations().get("execute").getSignatures().get(0).getValidator();

		JsonSchema schema = schemae.get(0);

		JsonValidator vlad = new JsonValidator(schema, new ClasspathSchemaResolver(this));

		vlad.validate(
			JsonUtils.loadJson(
					"{" +
						"\"folder\":\"EDW_REPLICATION\"," +
						"\"default-domain\":\"DOM_DEV\"," +
						"\"domains\":{" +
							"\"DOM_DEV\":{" +
								"\"username\":\"bsmith\"," +
								"\"connectivity-host\":\"ETLDEV02\"," +
								"\"repositories\":[" +
									"\"REP_SVC_DEV\"" +
								"]," +
							"\"integration-services\":[\"INT_SVC_UT_DEV\"]," +
							"\"working-root\":\"\\\\\\\\etldev01\\\\unit_test\\\\etldev02\"," +
								"\"client-version\":\"9.1.0hf4\"," +
								"\"password-encrypted\":\"VOUB+D1yyhzCU+TZ+2F+yQ==\"," +
								"\"connectivity-port\":6005" +
							"}," +
							"\"Domain_etldev01\":{" +
								"\"username\":\"bsmith\"," +
								"\"connectivity-host\":\"etldev01\"," +
								"\"repositories\":[" +
									"\"dev_pc_repo\"" +
								"]," +
							"\"integration-services\":[\"dev_ci_is\"]," +
							"\"working-root\":\"\\\\\\\\etldev01\\\\unit_test\\\\etldev01\"," +
								"\"client-version\":\"8.5.1hf2\"," +
								"\"password-encrypted\":\"HWLg5d3NBaoN7z8Bqsdopg==\"," +
								"\"connectivity-port\":6001" +
							"}" +
						"}," +
						"\"context\":{" +
							"\"ods-connection\":\"informatica.connections.pos_alx_mart.tgt\"" +
						"}," +
						"\"context-name\": \"blah\"," +
						"\"workflow\":\"wkf_REPLICATE_COMBO_RULES\"" +
					"}"));

		System.out.println("");
	}
	 */
}
