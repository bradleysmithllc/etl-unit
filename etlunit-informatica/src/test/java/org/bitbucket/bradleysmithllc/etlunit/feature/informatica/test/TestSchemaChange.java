package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;

public class TestSchemaChange
{
	@Test
	public void test() throws IOException, ProcessingException {
		JsonNode jnode = JsonLoader.fromString("{}");

		// look for a validator
		JsonNode validatorNode = JsonLoader.fromString(IOUtils.toString(getClass().getResource("/org/bitbucket/bradleysmithllc/etlunit/feature/informatica/InformaticaFeatureModule.configuration.validator.jsonSchema")));

		// create a schema loader
		JsonSchemaFactory fact = JsonSchemaFactory.byDefault();

		JsonSchema res = fact.getJsonSchema(validatorNode);

		ProcessingReport results = res.validate(jnode);
	}
}
