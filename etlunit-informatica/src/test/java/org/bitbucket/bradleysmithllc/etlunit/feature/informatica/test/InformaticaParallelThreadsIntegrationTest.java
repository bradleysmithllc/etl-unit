package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogManagerRuntimeSupportImpl;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class InformaticaParallelThreadsIntegrationTest extends BaseIntegrationTest {
	private InformaticaRuntimeSupport informaticaRuntimeSupport;
	private InformaticaConfiguration informaticaConfiguration;
	private int failCount = 0;

	@Inject
	public void receiveInformaticaRuntimeSupport(InformaticaRuntimeSupport irs)
	{
		informaticaRuntimeSupport = irs;
	}

	@Inject
	public void receiveInformaticaConfiguration(InformaticaConfiguration irs)
	{
		informaticaConfiguration = irs;
	}

	@Test
	public void defaultOptions() throws IOException {
		startTest();
		Assert.assertEquals(0, failCount);
	}

	@Override
	protected void assertMethodCompletionStatus(ETLTestMethod method, StatusReporter.CompletionStatus status, int testNumber, int operationNumber) {
		File parmlib = informaticaRuntimeSupport.getParmFilesRemote(informaticaConfiguration.getDefaultDomain());

		String name = method.getTestClass().getName();
		String expected = "cls_" + name + "-mtd_" + name + "-" + name;

		try {
			String actual = FileUtils.readFileToString(new File(parmlib, "wrk.PRM"));
			if (!actual.equals(expected))
			{
				failCount++;
				System.out.println("expected: '" + expected + "', actual: '" + actual + "'" + " [" + method.getTestClass().getQualifiedName() + "]");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected boolean multiPassSafe() {
		return false;
	}

	@Override
	protected int getExecutorCount() {
		return 25;
	}
}
