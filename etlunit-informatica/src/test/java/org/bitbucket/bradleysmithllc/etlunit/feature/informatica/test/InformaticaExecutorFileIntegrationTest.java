package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.util.Arrays;
import java.util.List;

public class InformaticaExecutorFileIntegrationTest extends BaseIntegrationTest
{
	private FileRuntimeSupport fileRuntimeSupport;
	private InformaticaRuntimeSupport informaticaRuntimeSupport;
	private InformaticaConfiguration informaticaConfiguration;

	private int assertCount = 0;

	@Inject
	public void setFileRuntimeSupport(InformaticaConfiguration fileRuntimeSupport)
	{
		this.informaticaConfiguration = fileRuntimeSupport;
	}

	@Inject
	public void setFileRuntimeSupport(FileRuntimeSupport fileRuntimeSupport)
	{
		this.fileRuntimeSupport = fileRuntimeSupport;
	}

	@Inject
	public void setFileRuntimeSupport(InformaticaRuntimeSupport fileRuntimeSupport)
	{
		this.informaticaRuntimeSupport = fileRuntimeSupport;
	}

	@Override
	protected void initializeTests()
	{
		assertCount = 0;
	}

	@Test
	public void runTest()
	{
		startTest();
	}

	@Override
	protected List<String> requireMilestones()
	{
		return Arrays.asList(new String[]{
			"[default].test.run.stage.0",
			"[default].test.run.stage.1",
			"[default].test.run.stage.2",
			"[default].test.run.stage.3",
			"[default].test.run.stage.4",
			"[default].test.run.stage.5",
			"[default].test.run.stage.6",
			"[default].test.run.stage.7",
			"[default].test.run.execute.8",
			"[default].test.run.execute.9",
			"[default].test.listTest.stage.0",
			"[default].test.listTest.stage.1",
			"[default].test.listTest.execute.2",
			"[default].test.listTest.execute.3"
		});
	}

	@Override
	protected boolean multiPassSafe()
	{
		return false;
	}

	@Override
	protected void assertions(TestResults results, int pass)
	{
		// make sure assertions ran
		Assert.assertEquals(2, results.getNumTestsSelected());
		Assert.assertEquals(2, results.getMetrics().getNumberOfTestsPassed());
	}

	@Override
	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext context, int operationNumber)
	{
		markTestMilestone(op.getQualifiedName());
		File srcFiles = informaticaRuntimeSupport.getSrcFilesRemote(informaticaConfiguration.getDefaultDomain());
		File lkpFiles = informaticaRuntimeSupport.getLkpFilesRemote(informaticaConfiguration.getDefaultDomain());
		File tgtFiles = informaticaRuntimeSupport.getTgtFilesRemote(informaticaConfiguration.getDefaultDomain());

		if (op.getTestMethod().getName().equals("run"))
		{
			switch (operationNumber)
			{
				case 8:
				{
					// assert file publications
					Assert.assertTrue(new FileBuilder(srcFiles).name("file1").file().exists());
					Assert.assertTrue(new FileBuilder(srcFiles).name("file3").file().exists());
					Assert.assertTrue(new FileBuilder(lkpFiles).name("file2").file().exists());
					Assert.assertTrue(new FileBuilder(tgtFiles).name("file4").file().exists());

					Assert.assertFalse(new FileBuilder(srcFiles).name("file5").file().exists());
					Assert.assertFalse(new FileBuilder(srcFiles).name("file7").file().exists());
					Assert.assertFalse(new FileBuilder(lkpFiles).name("file6").file().exists());
					Assert.assertFalse(new FileBuilder(tgtFiles).name("file8").file().exists());
					break;
				}
				case 9:
				{
					// assert file publications
					Assert.assertFalse(new FileBuilder(srcFiles).name("file1").file().exists());
					Assert.assertFalse(new FileBuilder(srcFiles).name("file3").file().exists());
					Assert.assertFalse(new FileBuilder(lkpFiles).name("file2").file().exists());
					Assert.assertFalse(new FileBuilder(tgtFiles).name("file4").file().exists());

					Assert.assertTrue(new FileBuilder(srcFiles).name("file5").file().exists());
					Assert.assertTrue(new FileBuilder(srcFiles).name("file7").file().exists());
					Assert.assertTrue(new FileBuilder(lkpFiles).name("file6").file().exists());
					Assert.assertTrue(new FileBuilder(tgtFiles).name("file8").file().exists());
					break;
				}
			}
		}
		else
		{
			switch (operationNumber)
			{
				case 2:
				{
					// assert file publications
					Assert.assertTrue(new FileBuilder(srcFiles).name("list-file").file().exists());
					Assert.assertTrue(new FileBuilder(srcFiles).name("list-file1").file().exists());

					Assert.assertFalse(new FileBuilder(srcFiles).name("clist-file").file().exists());
					Assert.assertFalse(new FileBuilder(srcFiles).name("clist-file1").file().exists());
					break;
				}
				case 3:
				{
					// assert file publications
					Assert.assertFalse(new FileBuilder(srcFiles).name("list-file").file().exists());
					Assert.assertFalse(new FileBuilder(srcFiles).name("list-file1").file().exists());

					Assert.assertTrue(new FileBuilder(srcFiles).name("clist-file").file().exists());
					Assert.assertTrue(new FileBuilder(srcFiles).name("clist-file1").file().exists());
					break;
				}
			}
		}
	}
}
