package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class InformaticaProcessClientTest {
/*
	private final LinkedList<ProcessDescription> queue = new LinkedList<ProcessDescription>();
	BasicRuntimeSupport rs = null;

	@Before
	public void setupRuntime() throws IOException {
		rs = new BasicRuntimeSupport(File.createTempFile("PPP", "PPP").getParentFile());
		rs.setLogger(new PrintWriterLog());

		queue.clear();

		rs.installProcessExecutor(new ProcessExecutor() {
			public ProcessFacade execute(ProcessDescription processBuilder) {
				queue.offer(processBuilder);

				return new ProcessFacade() {
					public ProcessDescription getDescriptor() {
						return null;
					}

					public void waitForCompletion() {
					}

					public int getCompletionCode() {
						return 0;
					}

					public void kill() {
					}

					public void waitForStreams()
					{
					}

					public Writer writer() {
						return null;
					}

					public BufferedReader getReader() {
						return null;
					}

					public BufferedReader getErrorReader() {
						return null;
					}

					public StringBuffer getInputBuffered() throws IOException {
						return new StringBuffer();
					}

					public StringBuffer getErrorBuffered() throws IOException {
						return new StringBuffer();
					}

					public OutputStream getOutputStream() {
						return null;
					}

					public InputStream getInputStream() {
						return null;
					}

					public InputStream getErrorStream() {
						return null;
					}
				};
			}
		});
	}

	@Test
	public void whatever() throws Exception {
		InformaticaRepositoryClient ipc = client();

		ipc.connect();

		Assert.assertEquals(1, queue.size());
		verifyConnect(queue.pop(), "pmrep");
	}

	@Test
	public void implicitConnect() throws Exception {
		InformaticaRepositoryClient ipc = client();
		ipc.createFolder("folder");
		Assert.assertEquals(2, queue.size());

		queue.clear();
		ipc = client();
		ipc.deleteFolder("folder");
		Assert.assertEquals(2, queue.size());

		verifyConnect(queue.pop(), "pmrep");

		queue.clear();
		ipc = client();
		ipc.createConnection("server", "database", "user", "password", "relational");
		Assert.assertEquals(2, queue.size());

		verifyConnect(queue.pop(), "pmrep");

		queue.clear();
		ipc = client();
		ipc.deleteConnection("relational", "database");
		Assert.assertEquals(2, queue.size());

		verifyConnect(queue.pop(), "pmrep");

		queue.clear();
		ipc = client();
		ipc.deleteConnection("relational", "database");
		Assert.assertEquals(2, queue.size());

		verifyConnect(queue.pop(), "pmrep");

		queue.clear();
		ipc = client();
		ipc.createFolder("folder", true);
		ipc.deleteTemporaryFolders();
		Assert.assertEquals(3, queue.size());

		verifyConnect(queue.pop(), "pmrep");

		queue.clear();
		ipc = client();
		ipc.executeWorkflow("workflow", "folder", new File("parm"));
		Assert.assertEquals(1, queue.size());

		queue.clear();
		ipc = client();
		ipc.executeWorkflowTask("workflow", "task", "folder", new File("parm"));
		Assert.assertEquals(1, queue.size());
/*
		queue.clear();
		ipc = client();
		ipc.exportWorkflowToXml("workflow", "folder", new File("output"));
		Assert.assertEquals(2, queue.size());

		queue.clear();
		ipc = client();
		ipc.exportAllWorkflowsToXml();
		Assert.assertEquals(1, queue.size());
*/
/*
	}

	private void verifyConnect(ProcessDescription pop, String tool) {
		Assert.assertTrue(pop.getCommand().endsWith("pmcmddir" + File.separatorChar + tool));

		Assert.assertEquals("connect", pop.getArguments().get(0));

		verifyOption("r", "repository", pop);
		verifyOption("d", "domain", pop);
		verifyOption("n", "user", pop);
		String value = getOption("X", pop);

		Assert.assertNotNull(value);

		Assert.assertEquals("password", pop.getEnvironment().get(value));
		Assert.assertTrue(pop.getEnvironment().get("INFA_DOMAINS_FILE").endsWith("domains.infa"));
	}

	private String getOption(String x, ProcessDescription pop) {
		Iterator<String> it = pop.getArguments().iterator();

		while (it.hasNext())
		{
			String str = it.next();

			if (str.startsWith("-"))
			{
				// option
				if (str.substring(1).equals(x))
				{
					return it.next();
				}
				else
				{
					// skip the value portion of this name-value pair
					it.next();
				}
			}
		}

		return null;
	}

	private void verifyOption(String r, String repository, ProcessDescription pop) {
		Iterator<String> it = pop.getArguments().iterator();
		
		while (it.hasNext())
		{
			String str = it.next();

			if (str.startsWith("-"))
			{
				// option
				if (str.substring(1).equals(r))
				{
					Assert.assertEquals(repository, it.next());
				}
				else
				{
					// skip the value portion of this name-value pair
					it.next();
				}
			}
		}
	}

	private InformaticaRepositoryClient client()
	{
		return new InformaticaRepositoryClientImpl(
			"domain",
			"repository",
			"integration_service",
			"user",
			"password",
			new File("pmcmddir"),
			new File("configdir", "domains.infa"),
			rs,
			new PrintWriterLog()
		);
	}
*/
}
