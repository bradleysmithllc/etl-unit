package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.LoggingProcessExecutor;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class InformaticaFeatureTest extends BaseFeatureModuleTest {
	@BeforeClass
	public static void setTestMode()
	{
		ContextBase.initiate(true);
	}

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private InformaticaRuntimeSupport informaticaRuntimeSupport;

	private boolean loadWorks = true;
	private boolean cleanRepository = false;
	private boolean exportWorks = false;

	@Inject
	public void receiveInformaticaRuntimeSupport(InformaticaRuntimeSupport irs)
	{
		informaticaRuntimeSupport = irs;
	}

	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("informatica")
							.object()
								.key("default-domain")
								.value("Domain_etldev01")
								.key("domains")
									.object()
										.key("Domain_etldev01")
											.object()
												.key("working-root")
												.value(tmp.getAbsolutePath())
												.key("client-version")
												.value("ver1")
												.key("username")
												.value("iuser")
												.key("password-encrypted")
												.value("passwd")
												.key("repositories")
													.object()
														.key("repo")
															.object()
																.key("integration-services")
																.value(Arrays.asList("is"))
															.endObject()
													.endObject()
											.endObject()
										.key("Domain_etldev02")
											.object()
												.key("working-root")
												.value(tmp.getAbsolutePath())
												.key("client-version")
												.value("ver1")
												.key("username")
												.value("iuser")
												.key("password-encrypted")
												.value("passwd")
												.key("repositories")
													.object()
														.key("repo")
															.object()
																.key("integration-services")
																.value(Arrays.asList("is"))
															.endObject()
													.endObject()
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides()
	{
		return Arrays.asList(
			new RuntimeOption("informatica.loadWorkflows", loadWorks),
			new RuntimeOption("informatica.exportWorkflows", exportWorks),
			new RuntimeOption("informatica.cleanRepository", cleanRepository)
		);
	}

	@Override
	protected List<Feature> getTestFeatures() {
		return Arrays.asList((Feature) new InformaticaFeatureModule());
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSource("infa.etlunit",
			"class test_infa {" +
				"var inputfile1 = '';" +
				"var db = {conn: {src: 'source', tgt: 'target'}};" +
				"@Test folders() {" +
					"createInformaticaFolder(folder: 'pamphlet');" +
					"deleteInformaticaFolder(folder: 'pamphlet2');" +
				"}" +
				"@Test connections() {" +
					"createInformaticaConnection(serverName: 'server', databaseName: 'database', databaseUserName: 'user', databasePassword: 'password', relationalConnectionName: 'connection');" +
					"createInformaticaConnection(informatica-domain: 'Domain_etldev02', serverName: 'server', databaseName: 'database', databaseUserName: 'user', databasePassword: 'password', relationalConnectionName: 'connection');" +
					"deleteInformaticaConnection(relationalConnectionName: 'connection');" +
					"deleteInformaticaConnection(informatica-domain: 'Domain_etldev02', relationalConnectionName: 'connection');" +
				"}" +
				"@Test executeWorkflow() {" +
					"set(variable: 'inputfile1', value: 'Test.txt');" +
					"execute(folder: 'pamphlet', workflow: 'wkf_RUN_ME');" +
					"execute(informatica-domain: 'Domain_etldev02', folder: 'pamphlet', workflow: 'wkf_RUN_ME');" +
					"execute(folder: 'pamphlet', workflow: 'wkf_RUN_ME');" +
				"}" +
			"}"
		);

		// create a paramater file template
		createResource("informatica", "pamphlet/ParameterFiles", "wkf_RUN_ME.PRM", "[Global]\n$Parameter=${inputfile1}\n$Parameter2=${db.conn.src}");
		File wkfFile = informaticaRuntimeSupport.getInformaticaWorkflow("pamphlet", "wkf_RUN_ME");

		IOUtils.writeBufferToFile(wkfFile, new StringBuffer("<FOLDER NAME=\"EDW_POS_TICKET\" GROUP=\"\" OWNER=\"Administrator\" SHARED=\"NOTSHARED\" DESCRIPTION=\"\" PERMISSIONS=\"rwx---r--\" UUID=\"7155f0e9-f99f-4aac-a9fe-80498f3be3cb\">"));
	}

	@Override
	protected void prepareTest() {
		LoggingProcessExecutor loggingProcessExecutor = new LoggingProcessExecutor();
		etlTestVM.getRuntimeSupport().installProcessExecutor(loggingProcessExecutor);
	}

	//@Test
	public void defaultOptions() throws IOException {
		exportWorks = false;
		loadWorks = true;

		TestResults res = startTest("DEFAULT_OPTIONS");

		Assert.assertEquals(3, res.getMetrics().getNumberOfTestsPassed());

		File prmFileSource = new FileBuilder(rsc).subdir("informatica").subdir("pamphlet").subdir("ParameterFiles").name("wkf_RUN_ME.PRM").file();
		Assert.assertTrue(prmFileSource.exists());

		prmFileSource = temporaryFolder.newFile();

		FileUtils.write(prmFileSource, "[Global]\n$Parameter=Test.txt\n$Parameter2=source");

		ParameterFile pfSource = new ParameterFile(prmFileSource);

		File parmfile = new FileBuilder(tmp).subdir("builduid/ParameterFiles").name("__builduser_builduid_pamphlet.wkf_RUN_ME.PRM").file();
		Assert.assertTrue(parmfile.exists());

		ParameterFile pfTarget = new ParameterFile(parmfile);

		Assert.assertEquals(0, pfSource.compareTo(pfTarget));
	}

	//@Test
	public void loadWorkflowFalseWithCleanRepo() throws IOException {
		exportWorks = false;
		loadWorks = false;
		cleanRepository = true;
		TestResults res = startTest("LOAD_WORKFLOW_FALSE_CLEAN_REPOSITORY");
	}

	//@Test
	public void cleanRepository() throws IOException {
		exportWorks = false;
		loadWorks = true;
		cleanRepository = true;
		TestResults res = startTest("CLEAN_REPOSITORY");
	}

	//@Test
	public void loadWorkflow() throws IOException {
		exportWorks = false;
		loadWorks = false;
		cleanRepository = false;
		TestResults res = startTest("LOAD_WORKFLOW_FALSE");
	}

	//@Test
	public void exportWorkflow() throws IOException {
		loadWorks = true;
		exportWorks = true;
		cleanRepository = false;
		TestResults res = startTest("EXPORT_WORKFLOW_TRUE");
	}

	//@Test
	public void bothWorkflowOptions() throws IOException {
		loadWorks = false;
		exportWorks = true;
		cleanRepository = false;
		TestResults res = startTest("BOTH_WORKFLOW_OPTIONS");
	}
}
