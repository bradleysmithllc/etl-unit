package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaExecutor;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObjectBuilder;
import org.junit.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Arrays;

public class InformaticaListFoldersBeforeCreateTest extends BaseIntegrationTest {
	private InformaticaRuntimeSupport informaticaRuntimeSupport;
	private InformaticaConfiguration informaticaConfiguration;

	@Inject
	public void receiveInformaticaRuntimeSupport(InformaticaRuntimeSupport irs)
	{
		informaticaRuntimeSupport = irs;
	}

	@Inject
	public void receiveInformaticaConfiguration(InformaticaConfiguration irs)
	{
		informaticaConfiguration = irs;
	}

	/**
	 * Run as if for the first time.  Since there is no tag file, it will just create the folder.
	 * @throws IOException
	 */
	@Test
	public void cleanRun() throws IOException {
		createWorkflowTag = false;
		createWorkflowList = false;
		createFolderTag = false;
		createFolderContextVariable = false;
		createFolderList = false;

		startTest("ILFBCT_CleanRun");
	}

	/**
	 * Run as if for the second and subsequent time.  Create the tag file so the module
	 * has to list folders.
	 * @throws IOException
	 */
	@Test
	public void secondRun() throws IOException {
		createWorkflowTag = false;
		createWorkflowList = false;
		createFolderTag = true;
		createFolderContextVariable = false;
		createFolderList = false;

		startTest("ILFBCT_SecondRun");
	}

	/**
	 * Run with the tag and context variable set.
	 * has to list folders.
	 * @throws IOException
	 */
	@Test
	public void thirdRun() throws IOException {
		createWorkflowTag = false;
		createWorkflowList = false;
		createFolderTag = true;
		createFolderContextVariable = true;
		createFolderList = false;

		startTest("ILFBCT_ThirdRun");
	}

	/**
	 * Run with the tag set, but not the context.  Provide a
	 * folder list which includes the one here so that it
	 * does not create the folder.
	 *
	 * @throws IOException
	 */
	@Test
	public void fourthRun() throws IOException {
		createWorkflowTag = false;
		createWorkflowList = false;
		createFolderTag = true;
		createFolderContextVariable = false;
		createFolderList = true;

		startTest("ILFBCT_FourthRun");
	}

	/**
	 * Run as if for the second and subsequent time, but with the workflow tag set.
	 * Create the tag file so the module has to list folders.
	 *
	 * @throws IOException
	 */
	@Test
	public void secondRunWkfTag() throws IOException {
		createWorkflowTag = true;
		createWorkflowList = false;
		createFolderTag = true;
		createFolderContextVariable = false;
		createFolderList = false;

		startTest("ILFBCT_SecondRunWkfTag");
	}

	/**
	 * Run as if for the second and subsequent time, but with the workflow tag set.
	 * Create the tag file so the module has to list folders.
	 *
	 * @throws IOException
	 */
	@Test
	public void secondRunWkfTagWkfList() throws IOException {
		createWorkflowTag = true;
		createWorkflowList = true;
		createFolderTag = true;
		createFolderContextVariable = false;
		createFolderList = false;

		startTest("ILFBCT_SecondRunWkfTagWkfList");
	}

	/**
	 * Workflow tag set.
	 *
	 * @throws IOException
	 */
	@Test
	public void wkfTagOnly() throws IOException {
		createWorkflowTag = true;
		createWorkflowList = false;
		createFolderTag = false;
		createFolderContextVariable = false;
		createFolderList = false;

		startTest("ILFBCT_wkfTagOnly");
	}

	/**
	 * Workflow tag set.
	 *
	 * @throws IOException
	 */
	@Test
	public void wkfTagFolderTagFolderContext() throws IOException {
		createWorkflowTag = true;
		createWorkflowList = false;
		createFolderTag = true;
		createFolderContextVariable = true;
		createFolderList = false;

		startTest("ILFBCT_wkfTagFolderTagFolderContext");
	}

	/**
	 * Workflow tag set.
	 *
	 * @throws IOException
	 */
	@Test
	public void wkfTagFolderTagFolderList() throws IOException {
		createWorkflowTag = true;
		createWorkflowList = false;
		createFolderTag = true;
		createFolderContextVariable = false;
		createFolderList = true;

		startTest("ILFBCT_wkfTagFolderTagFolderList");
	}

	/**
	 * Workflow tag set.
	 *
	 * @throws IOException
	 */
	@Test
	public void wkfTagFolderTagFolderContextFolderList() throws IOException {
		createWorkflowTag = true;
		createWorkflowList = false;
		createFolderTag = true;
		createFolderContextVariable = true;
		createFolderList = true;

		startTest("ILFBCT_wkfTagFolderTagFolderContextFolderList");
	}


	private boolean createFolderTag = false;
	private boolean createFolderContextVariable = false;
	private boolean createFolderList = false;
	private boolean createWorkflowTag = false;
	private boolean createWorkflowList = false;

	@Override
	protected void prepareTest() {
		InformaticaDomain defaultDomain = informaticaConfiguration.getDefaultDomain();
		InformaticaRepository defaultRepository = defaultDomain.getDefaultRepository();

		if (createFolderTag)
		{
			informaticaRuntimeSupport.updateObjectRepositoryStatus(
					defaultRepository,
				"folder",
				"EDW_POS_TICKET",
				true
			);
		}

		String key = defaultRepository.getQualifiedName() + ".listFolders";
		if (createFolderList)
		{
			System.getProperties().put(key, Arrays.asList("__builduser_buildUid_0_EDW_POS_TICKET"));
		}
		else
		{
			System.getProperties().remove(key);
		}

		key = defaultRepository.getQualifiedName() + ".listWorkflows";

		if (createWorkflowList)
		{
			System.getProperties().put(key, Arrays.asList("__builduser_buildUid_0_wkf_RUN_ME"));
		}
		else
		{
			System.getProperties().remove(key);
		}

		if (createWorkflowTag)
		{
			informaticaRuntimeSupport.updateWorkflowRepositoryStatus(defaultRepository, "pamphlet", "wkf_RUN_ME");
		}
	}

	@Override
	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context) {
		if (createFolderContextVariable)
		{
			InformaticaDomain defaultDomain = informaticaConfiguration.getDefaultDomain();
			ETLTestValueObject repositoryContainer = InformaticaExecutor.getInformaticaRepositoryContainer(
					context,
					defaultDomain.getDefaultRepository(),
					defaultDomain
			);

			ETLTestValueObject folderList = repositoryContainer.query(InformaticaExecutor.INFORMATICA_CREATED_FOLDERS);
			new ETLTestValueObjectBuilder(folderList).key("__builduser_buildUid_0_EDW_POS_TICKET").value("true");
		}
	}

	@Override
	protected boolean multiPassSafe() {
		return false;
	}
}
