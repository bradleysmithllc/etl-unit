package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class InformaticaExecutorControlIntegrationTest extends BaseIntegrationTest {
	private InformaticaRuntimeSupport informaticaRuntimeSupport;

	/*Grab this so we can get to the generated parm files
	 */
	@Inject
	public void setInformaticaRuntimeSupport(InformaticaRuntimeSupport support)
	{
		informaticaRuntimeSupport = support;
	}

	private InformaticaConfiguration informaticaConfiguration;

	/*Grab this so we can get to the domain configuration
	 */
	@Inject
	public void setInformaticaConfiguration(InformaticaConfiguration configuration)
	{
		informaticaConfiguration = configuration;
	}

	@Test
	public void runTest() throws IOException {
		ContextBase.initiate(true);

		startTest();
	}

	@Override
	protected void assertions(TestResults results, int pass) throws Exception {
		// get a handle to the control file that was created
		File control = getInfaWorking("__builduser_buildUid_0_folder___builduser_buildUid_0_wrk.xmlanon_6.control.xml.anon_9anon_10");

		// validate existence
		Assert.assertTrue(control.exists());

		//validate contents
		String contents = IOUtils.readFileToString(control);

		if (pass == 1)
		{
			// look for our two folder declarations.  Only look the first time since the project uid will change for multiple threads.
			Assert.assertTrue(contents.indexOf("<FOLDERMAP SOURCEFOLDERNAME='SHARED_EDW' SOURCEREPOSITORYNAME='svn_pc_repo' TARGETFOLDERNAME='__builduser_buildUid_0_SHARED_EDW' TARGETREPOSITORYNAME='rep' />") != -1);
			Assert.assertTrue(contents.indexOf("<FOLDERMAP SOURCEFOLDERNAME='EDW_POS_TICKET' SOURCEREPOSITORYNAME='svn_pc_repo' TARGETFOLDERNAME='__builduser_buildUid_0_EDW_POS_TICKET' TARGETREPOSITORYNAME='rep' />") != -1);
		}

		File parmlib = informaticaRuntimeSupport.getParmFilesRemote(informaticaConfiguration.getDefaultDomain());

		File [] parms = parmlib.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				if (pathname.getName().endsWith(".PRM"))
				{
					try {
						ParameterFile pfActual = new ParameterFile(pathname);

						File prmFile = temporaryFolder.newFile();

						FileUtils.write(prmFile, "[Global]\n$Parameter=$$#include#parse$$\n");

						ParameterFile pfExpected = new ParameterFile(prmFile);

						Assert.assertEquals(0, pfExpected.compareTo(pfActual));

						return true;
					} catch (IOException e) {
						Assert.fail();
					}
				}

				return false;
			}
		});

		Assert.assertNotNull(parms);
		Assert.assertEquals(1, parms.length);
	}

	public File getInfaWorking(String s) {
		return getInfaWorking(runtimeSupport, s);
	}

	public static File getInfaWorking(RuntimeSupport runtimeSupport, String s) {
		File[] f = runtimeSupport.getTempDirectory().listFiles((file) -> {return file.getName().startsWith(s);});

		if (f.length == 0) {
			throw new IllegalArgumentException("File not found " + s);
		}

		if (f.length > 1) {
			throw new IllegalArgumentException("TMI " + s);
		}

		return f[0];
	}
}
