package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.DomainDefaults;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.DomainsProperty;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.Repositories;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.RepositoriesProperty;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaRuntimeSupportImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class WorkflowRefreshTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private RuntimeSupport runtimeSupport;
	private InformaticaRuntimeSupport informaticaRuntimeSupport;
	private InformaticaDomain informaticaDomain;
	private InformaticaRepository repository;

	@Before
	public void createStuff() throws IOException {
		BasicRuntimeSupport brs = new BasicRuntimeSupport(temporaryFolder.newFolder());
		runtimeSupport = brs;

		brs.setApplicationLogger(new PrintWriterLog());
		brs.setUserLogger(new PrintWriterLog());


		InformaticaRuntimeSupportImpl irs = new InformaticaRuntimeSupportImpl();
		informaticaRuntimeSupport = irs;

		irs.setRuntimeSupport(runtimeSupport);

		DomainsProperty domains = new DomainsProperty();
		domains.setClientVersion("1.0");
		domains.setUsername("user");
		domains.setPasswordEncrypted("pass");

		Repositories repositories = new Repositories();
		domains.setRepositories(repositories);

		repositories.getAdditionalProperties().put("REP_SVC_DEV", new RepositoriesProperty());

		informaticaDomain = new InformaticaDomain(
			"DOM_DEV",
			domains,
			new DomainDefaults(),
			runtimeSupport,
			temporaryFolder.newFile()
		);

		repository = informaticaDomain.getRepository("REP_SVC_DEV");
		informaticaRuntimeSupport.enableLoadWorkflows();
	}

	@Test
	public void workflowInitial() throws IOException {
		// touch an xml file and let's check / update it's status
		File wkf = informaticaRuntimeSupport.getInformaticaWorkflow("folder", "workflow");

		FileUtils.touch(wkf);

		// no tag file yet
		Assert.assertFalse(informaticaRuntimeSupport.checkWorkflowRepositoryStatus(repository, "folder", "workflow"));

		// update the status
		informaticaRuntimeSupport.updateWorkflowRepositoryStatus(repository, "folder", "workflow");

		// now we're good
		Assert.assertTrue(informaticaRuntimeSupport.checkWorkflowRepositoryStatus(repository, "folder", "workflow"));
	}

	@Test
	public void workflowChanged() throws IOException {
		// touch an xml file and let's check / update it's status
		File wkf = informaticaRuntimeSupport.getInformaticaWorkflow("folder", "workflow");

		FileUtils.touch(wkf);

		// update the status
		informaticaRuntimeSupport.updateWorkflowRepositoryStatus(repository, "folder", "workflow");

		// now we're good
		Assert.assertTrue(informaticaRuntimeSupport.checkWorkflowRepositoryStatus(repository, "folder", "workflow"));

		// touch the wkf to change it's checksum
		FileUtils.write(wkf, "l");
		Assert.assertFalse(informaticaRuntimeSupport.checkWorkflowRepositoryStatus(repository, "folder", "workflow"));
		informaticaRuntimeSupport.updateWorkflowRepositoryStatus(repository, "folder", "workflow");

		// good again
		Assert.assertTrue(informaticaRuntimeSupport.checkWorkflowRepositoryStatus(repository, "folder", "workflow"));
	}
}
