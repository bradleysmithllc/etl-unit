package org.bitbucket.bradleysmithllc.etlunit.feature.informatica.test;

/*
 * #%L
 * etlunit-informatica
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.informatica.InformaticaRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class InformaticaFeatureIntegrationTest extends BaseIntegrationTest {
	private InformaticaRuntimeSupport informaticaRuntimeSupport;

	private boolean forceInitializeRepository = false;
	private boolean loadWorks = true;
	private boolean cleanRepository = false;
	private boolean exportWorks = false;

	@Inject
	public void receiveInformaticaRuntimeSupport(InformaticaRuntimeSupport irs)
	{
		informaticaRuntimeSupport = irs;
	}

	@Override
	protected List<RuntimeOption> getRuntimeOptionOverrides()
	{
		return Arrays.asList(
			new RuntimeOption("informatica.loadWorkflows", loadWorks),
			new RuntimeOption("informatica.exportWorkflows", exportWorks),
			new RuntimeOption("informatica.cleanRepository", cleanRepository),
			new RuntimeOption("informatica.forceInitializeRepository", forceInitializeRepository)
		);
	}

	@Test
	public void defaultOptions() throws IOException {
		exportWorks = false;
		loadWorks = true;

		startTest("IFIT_DO");

		File srcPrm = temporaryFolder.newFile();

		FileUtils.write(srcPrm, "[Global]\n" +
				"$Parameter=Test.txt\n" +
				"$Parameter2=source\n" +
				"[section]\n" +
				"\n" +
				"$Parameter=Value");

		File parmfile = new FileBuilder(tmp).subdir("infa_working02").subdir("buildUid_0").subdir("Domain_etldev02").subdir("ParameterFiles").name("wkf_RUN_ME.PRM").file();

		Assert.assertEquals(0, new ParameterFile(srcPrm).compareTo(new ParameterFile(parmfile)));

		parmfile = new FileBuilder(tmp).subdir("infa_working01").subdir("buildUid_0").subdir("Domain_etldev01").subdir("ParameterFiles").name("wkf_RUN_ME_TOO.PRM").file();
		Assert.assertTrue(parmfile.exists());

		Assert.assertEquals(0, new ParameterFile(srcPrm).compareTo(new ParameterFile(parmfile)));
	}

	@Test
	public void loadWorkflowFalseWithCleanRepo() throws IOException {
		exportWorks = false;
		loadWorks = false;
		cleanRepository = true;
		startTest("IFIT_LWF_CR");
	}

	@Test
	public void cleanRepository() throws IOException {
		exportWorks = false;
		loadWorks = true;
		cleanRepository = true;
		startTest("IFIT_CR");
	}

	/**
	 * All options are off
	 * @throws IOException
	 */
	@Test
	public void loadWorkflow() throws IOException {
		exportWorks = false;
		loadWorks = false;
		cleanRepository = false;
		startTest("IFIT_LWF");
	}

	@Test
	public void exportWorkflow() throws IOException {
		loadWorks = true;
		exportWorks = true;
		cleanRepository = false;
		startTest("IFIT_EWT");
	}

	/**
	 * Load workflow off, clean repository off, but export workflows is on.
	 * @throws IOException
	 */
	@Test
	public void bothWorkflowOptions() throws IOException {
		loadWorks = false;
		exportWorks = true;
		cleanRepository = false;
		startTest("IFIT_BWO");
	}

	/**
	 * Load is off, orce is on.  Execute's must fail.  Not a good state here,
	 * it just silently fails.
	 * @throws IOException
	 */
	@Test(expected = RuntimeException.class)
	public void invalidOptions() throws IOException {
		loadWorks = false;
		exportWorks = false;
		cleanRepository = false;
		forceInitializeRepository = true;
		startTest("IFIT_IO");
	}

	/**
	 * load on, force on, run tests twice.  Must get full cycle twice.
	 * @throws IOException
	 */
	@Test
	public void force() throws IOException {
		loadWorks = true;
		exportWorks = false;
		cleanRepository = false;
		forceInitializeRepository = true;
		startTest("IFIT_FORCE");
		startTest("IFIT_FORCE");
	}

	/**
	 * load on, force off, run tests twice.  Must get full cycle first time, partial next.
	 * @throws IOException
	 */
	@Test
	public void loadNoForce() throws IOException {
		loadWorks = true;
		exportWorks = false;
		cleanRepository = false;
		forceInitializeRepository = false;
		startTest("IFIT_LOAD_1");

		// need to clear out all mockAgent stuff
		FileUtils.deleteDirectory(runtimeSupport.getGeneratedSourceDirectory("mockAgent"));
		// need to set the list workflow so that it marks it as loaded

		System.getProperties().put("Domain_etldev01.repo.listWorkflows", Arrays.asList("__builduser_buildUid_0_wkf_RUN_ME"));

		startTest("IFIT_LOAD_2");

		System.getProperties().remove("Domain_etldev01.repo.listWorkflows");
	}

	/**
	 * load on, force off, run tests twice - changing the workflow run order.
	 * @throws IOException
	 */
	@Test
	public void loadMultipleWorkflowsDifferentFolders() throws IOException {
		loadWorks = true;
		exportWorks = false;
		cleanRepository = false;
		forceInitializeRepository = false;
		startTest("IFIT_LMW1");

		// need to clear out all mockAgent stuff
		FileUtils.deleteDirectory(runtimeSupport.getGeneratedSourceDirectory("mockAgent"));

		startTest("IFIT_LMW2");
	}

	/**
	 * This test evaluates multiple-run behavior and so isn't suited to this.  Also, the assertions won't match because the project uid changes.
	 * @return
	 */
	@Override
	protected boolean multiPassSafe() {
		return false;
	}
}
