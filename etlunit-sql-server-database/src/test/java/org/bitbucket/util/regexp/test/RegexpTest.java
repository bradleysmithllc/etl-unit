package org.bitbucket.util.regexp.test;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.regexp.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.Assert;
import org.junit.Test;

public class RegexpTest {
	@Test
	public void testSam()
	{
		Pattern p = Pattern.compile("Build EDW Data Layer::Unit Tests #\\d{1,10} fail(ed|ing)( \\(tests failed: \\d{1,4}( \\(\\d{1,4} new\\))?, passed: \\d{1,4}\\))?");

		Matcher m = p.matcher("Build EDW Data Layer::Unit Tests #289 failed (tests failed: 260 (231 new), passed: 0)");
		Assert.assertTrue(m.matches());

		m = p.matcher("Build EDW Data Layer::Unit Tests #289 failing (tests failed: 3, passed: 0)");
		Assert.assertTrue(m.matches());

		m = p.matcher("Build EDW Data Layer::Unit Tests #285 failed");
		Assert.assertTrue(m.matches());
	}

	@Test
	public void testConstraint()
	{
		TableConstraintExpression tce = TableConstraintExpression.match("ALTER TABLE TST_TGT.SURVEY_SECTION_SCORE\r\n\tADD CONSTRAINT  SECTION_INCLUDES_SCORE FOREIGN KEY (SURVEY_TYPE_ID,SECTION_ID) REFERENCES TST_TGT.SURVEY_SECTION(SURVEY_TYPE_ID,SECTION_ID)\r\n\tON DELETE NO ACTION\r\n\tON UPDATE NO ACTION\r\ngo");

		Assert.assertTrue(tce.matches());
		Assert.assertEquals("SURVEY_SECTION_SCORE", tce.getTableName());
		Assert.assertEquals("SECTION_INCLUDES_SCORE", tce.getConstraintName());
		Assert.assertEquals("SURVEY_SECTION", tce.getForeignTableName());

		tce = TableConstraintExpression.match("ALTER TABLE TST_TGT.PRODUCT ADD CONSTRAINT PRODUCT_SIZE_CLASIFIES_PRODUCT FOREIGN KEY (PRODUCT_SIZE_ID) REFERENCES TST_TGT.PRODUCT_SIZE(PRODUCT_SIZE_ID)	ON DELETE CASCADE ON UPDATE NO ACTION GO");

		Assert.assertTrue(tce.matches());
		Assert.assertEquals("PRODUCT", tce.getTableName());
		Assert.assertEquals("PRODUCT_SIZE_CLASIFIES_PRODUCT", tce.getConstraintName());
		Assert.assertEquals("PRODUCT_SIZE", tce.getForeignTableName());
	}

	@Test
	public void testPowermart()
	{
		String s = "<POWERMART CREATION_DATE=\"11/30/2010 12:00:00\" REPOSITORY_VERSION=\"179.88\">".replaceAll("<POWERMART CREATION_DATE=\"\\d{1,2}/\\d{1,2}/\\d{4} \\d{1,2}:\\d{1,2}:\\d{1,2}\" REPOSITORY_VERSION=\"(\\d{1,4}.\\d{1,9})\">", "<POWERMART CREATION_DATE=\"11/30/2010 12:00:00\" REPOSITORY_VERSION=\"$1\">");

		System.out.println(s);
	}

	@Test
	public void testSchemaNames()
	{
		SchemaNameExpression sne = new SchemaNameExpression("CREATE SCHEMA SUBSET");

		Assert.assertTrue(sne.matches());
		Assert.assertEquals("SUBSET", sne.getSchemaName());
	}

	@Test
	public void testTestNames()
	{
		class TestName
		{
			String testText;
			String className;
			String methodName;

			TestName(String text)
			{
				this(text, null, null);
			}

			TestName(String text, String cls)
			{
				this(text, cls, null);
			}

			TestName(String text, String cls, String methd)
			{
				testText = text;
				className = cls;
				methodName = methd;
			}
		}

		TestName [] tests = {
				new TestName("1", "1"),
				new TestName("_", "_"),
				new TestName("1_1", "1_1"),
				new TestName("1_a", "1_a"),
				new TestName("class.method", "class", "method"),

				new TestName("stg", "stg"),
				new TestName("^stg", "^stg"),
				new TestName("^stg$", "^stg$"),
				new TestName("stg$", "stg$"),

				new TestName("stg.stg", "stg", "stg"),
				new TestName("stg.^stg", "stg", "^stg"),
				new TestName("stg.^stg$", "stg", "^stg$"),

				new TestName("^stg.stg", "^stg", "stg"),
				new TestName("^stg.^stg", "^stg", "^stg"),
				new TestName("^stg.^stg$", "^stg", "^stg$"),
				new TestName("^stg.stg$", "^stg", "stg$"),

				new TestName("^stg$.stg", "^stg$", "stg"),
				new TestName("^stg$.^stg", "^stg$", "^stg"),
				new TestName("^stg$.^stg$", "^stg$", "^stg$"),
				new TestName("^stg$.stg$", "^stg$", "stg$"),

				new TestName("stg$.stg", "stg$", "stg"),
				new TestName("stg$.^stg", "stg$", "^stg"),
				new TestName("stg$.^stg$", "stg$", "^stg$"),
				new TestName("stg$.stg$", "stg$", "stg$"),
		};

		for (int i = 0; i < tests.length; i++)
		{
			TestNameExpression tne = new TestNameExpression(tests[i].testText);

			Assert.assertEquals(tests[i].className != null, tne.matches());

			String it = tne.group();

			if (tests[i].className != null)
			{
				Assert.assertTrue(tne.hasClassName());
				Assert.assertEquals(tests[i].className, tne.getClassName());
			}

			if (tests[i].methodName != null)
			{
				Assert.assertTrue(tne.hasTestName());
				Assert.assertEquals(tests[i].methodName, tne.getTestName());
			}
		}
	}

	@Test
	public void testSchemaMatch()
	{
		SchemaMatchExpression sme = new SchemaMatchExpression("AGG.match", "AGG");

		Assert.assertTrue(sme.hasNext());

		sme = new SchemaMatchExpression("DBO.AGG.match", "AGG");

		Assert.assertTrue(sme.hasNext());

		sme = new SchemaMatchExpression("DBO.PSP_AGG.match", "AGG");

		Assert.assertFalse(sme.hasNext());
	}

	@Test
	public void testRecordSetCount()
	{
		ResultSetCountExpression rsc = new ResultSetCountExpression("-----------\r\n      63937");

		Assert.assertTrue(rsc.matches());

		Assert.assertEquals(rsc.getRecordCount(), 63937);
	}

	@Test
	public void testProperty()
	{
		PropertyExpression pe = PropertyExpression.match("Name: Value");

		Assert.assertTrue(pe.matches());

		Assert.assertEquals(pe.getPropertyName(), "Name");
		Assert.assertEquals(pe.getPropertyDefaultValue(), "Value");

		pe = PropertyExpression.match("Name_1 : Value_2_3");

		Assert.assertTrue(pe.matches());

		Assert.assertEquals(pe.getPropertyName(), "Name_1");
		Assert.assertEquals(pe.getPropertyDefaultValue(), "Value_2_3");

		pe = PropertyExpression.match("2_Name_1:3_Value_2_3");

		Assert.assertTrue(pe.matches());

		Assert.assertEquals(pe.getPropertyName(), "2_Name_1");
		Assert.assertEquals(pe.getPropertyDefaultValue(), "3_Value_2_3");

		pe = PropertyExpression.match("2_Name_1: $bean.Informal");

		Assert.assertTrue(pe.matches());

		Assert.assertEquals(pe.getPropertyName(), "2_Name_1");
		Assert.assertEquals(pe.getPropertyDefaultValue(), "$bean.Informal");

		pe = PropertyExpression.match("2_Name_1: ${bean.Informal}");

		Assert.assertTrue(pe.matches());

		Assert.assertEquals(pe.getPropertyName(), "2_Name_1");
		Assert.assertEquals(pe.getPropertyDefaultValue(), "${bean.Informal}");
	}

	@Test
	public void testColumnNames()
	{
		FormatFileColumnExpression ffce = new FormatFileColumnExpression("COLUMN SOURCE=\"1\" NAME=\"FOOD_OR_BEVERAGE_ID\" xsi:type=\"SQLINT\"");

		Assert.assertTrue(ffce.matches());
		Assert.assertEquals("FOOD_OR_BEVERAGE_ID", ffce.getColumnName());
		Assert.assertEquals(1, ffce.getColumnOffset());
	}

	@Test
	public void testColumnMultipleNames()
	{
		FormatFileColumnExpression ffce = new FormatFileColumnExpression("<ROW>\n" +
			"  <COLUMN SOURCE=\"1\" NAME=\"FOOD_OR_BEVERAGE_ID\" xsi:type=\"SQLINT\"/>\n" +
			"  <COLUMN SOURCE=\"2\" NAME=\"DESCRIPTION\" xsi:type=\"SQLVARYCHAR\"/>\n" +
			"  <COLUMN SOURCE=\"3\" NAME=\"POPULATION_DATE\" xsi:type=\"SQLDATETIME\"/>\n" +
			" </ROW>");

		Assert.assertTrue(ffce.hasNext());
		Assert.assertEquals("FOOD_OR_BEVERAGE_ID", ffce.getColumnName());
		Assert.assertEquals(1, ffce.getColumnOffset());
		Assert.assertTrue(ffce.hasNext());
		Assert.assertEquals("DESCRIPTION", ffce.getColumnName());
		Assert.assertEquals(2, ffce.getColumnOffset());
		Assert.assertTrue(ffce.hasNext());
		Assert.assertEquals("POPULATION_DATE", ffce.getColumnName());
		Assert.assertEquals(3, ffce.getColumnOffset());
	}

	@Test
	public void testColumnListExpression()
	{
		ColumnListExpression cle = new ColumnListExpression("COL1\rCOL2\nCOL3\r\nCOL4\r\n\n\r\n\r\n\rCOL5");

		Assert.assertTrue(cle.hasNext());
		Assert.assertEquals("COL1", cle.getColumnName());
		Assert.assertTrue(cle.hasNext());
		Assert.assertEquals("COL2", cle.getColumnName());
		Assert.assertTrue(cle.hasNext());
		Assert.assertEquals("COL3", cle.getColumnName());
		Assert.assertTrue(cle.hasNext());
		Assert.assertEquals("COL4", cle.getColumnName());
		Assert.assertTrue(cle.hasNext());
		Assert.assertEquals("COL5", cle.getColumnName());
		Assert.assertFalse(cle.hasNext());
	}
}
