package org.bitbucket.util.regexp.test;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database.SqlServerDatabaseImplementation;
import org.junit.Assert;
import org.junit.Test;

public class PauseTest
{
	@Test
	public void pauseRange()
	{
		long total = 0L;

		for (int i = 0; i < 1000; i++)
		{
			long pt = SqlServerDatabaseImplementation.pauseTime(100);

			Assert.assertTrue(pt >= 0L);
			Assert.assertTrue(pt <= 1000L);

			total += pt;
		}

		Assert.assertTrue(total > 0L);
		Assert.assertTrue(total < 100000L);

		Assert.assertTrue(total >= 40000L && total <= 60000L);
	}
}
