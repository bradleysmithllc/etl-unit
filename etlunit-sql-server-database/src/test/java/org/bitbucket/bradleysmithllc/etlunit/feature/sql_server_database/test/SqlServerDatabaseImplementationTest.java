package org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database.test;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.util.PlainDatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database.SqlServerDatabaseImplementation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SqlServerDatabaseImplementationTest
{
	SqlServerDatabaseImplementation sql;
	private BasicRuntimeSupport runtimeSupport;

	@Before
	public void setupImpl()
	{
		sql = new SqlServerDatabaseImplementation();
		runtimeSupport = new BasicRuntimeSupport();
		sql.receiveRuntimeSupport(runtimeSupport);
	}

	@Test
	public void testAdminUrl()
	{
		PlainDatabaseConnection dbc = new PlainDatabaseConnection();
		dbc.setAdminUserName("admin");
		dbc.setAdminUserPassword("adminpass");
		dbc.setServerName("server");
		dbc.setServerPort(1000);

		String url = sql.getJdbcUrl(dbc, null, DatabaseImplementation.database_role.sysadmin);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1000/master", url);
	}

	@Test
	public void testAdminUrlWithInstance()
	{
		PlainDatabaseConnection dbc = new PlainDatabaseConnection();
		dbc.setAdminUserName("admin");
		dbc.setAdminUserPassword("adminpass");
		dbc.setServerName("server");
		dbc.setServerPort(1001);
		dbc.setProperty("instance-name", "instancen");

		String url = sql.getJdbcUrl(dbc, null, DatabaseImplementation.database_role.sysadmin);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1001/master;instance=instancen", url);
	}

	@Test
	public void testAdminUrlWithDomain()
	{
		PlainDatabaseConnection dbc = new PlainDatabaseConnection();
		dbc.setAdminUserName("admin");
		dbc.setAdminUserPassword("adminpass");
		dbc.setServerName("server");
		dbc.setServerPort(1002);
		dbc.setProperty("domain-name", "addomain");

		String url = sql.getJdbcUrl(dbc, null, DatabaseImplementation.database_role.sysadmin);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1002/master;useNTLMv2=true;domain=addomain", url);
	}

	@Test
	public void testAdminUrlWithDomainAndInstance()
	{
		PlainDatabaseConnection dbc = new PlainDatabaseConnection();
		dbc.setAdminUserName("admin");
		dbc.setAdminUserPassword("adminpass");
		dbc.setServerName("server");
		dbc.setServerPort(1003);
		dbc.setProperty("domain-name", "addomain");
		dbc.setProperty("instance-name", "iname");

		String url = sql.getJdbcUrl(dbc, null, DatabaseImplementation.database_role.sysadmin);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1003/master;instance=iname;useNTLMv2=true;domain=addomain", url);
	}

	@Test
	public void portNumbers()
	{
		PlainDatabaseConnection dbc = new PlainDatabaseConnection();
		dbc.setId("dbid");
		dbc.setAdminUserName("admin");
		dbc.setAdminUserPassword("adminpass");
		dbc.setServerName("server");
		dbc.setServerPort(1003);

		String url = sql.getJdbcUrl(dbc, null, DatabaseImplementation.database_role.sysadmin);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1003/master", url);

		dbc.setServerPort(1002);

		url = sql.getJdbcUrl(dbc, null, DatabaseImplementation.database_role.sysadmin);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1002/master", url);

		dbc.setServerPort(1001);

		url = sql.getJdbcUrl(dbc, "mode", DatabaseImplementation.database_role.sysadmin);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1001/master", url);

		dbc.setServerPort(1000);

		url = sql.getJdbcUrl(dbc, null, DatabaseImplementation.database_role.database);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1000/__builduser_proj_1_0_dbid_" + runtimeSupport.getProjectUID(), url);

		dbc.setServerPort(999);

		url = sql.getJdbcUrl(dbc, "mode", DatabaseImplementation.database_role.database);
		Assert.assertEquals("jdbc:jtds:sqlserver://server:999/__builduser_proj_1_0_dbid_mode_" + runtimeSupport.getProjectUID(), url);
	}
}
