package org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database.test;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Before;

import java.io.*;
import java.util.LinkedList;

public class BaseCmd {
	protected final LinkedList<ProcessDescription> queue = new LinkedList<ProcessDescription>();
	BasicRuntimeSupport rs = null;
	protected File root;

	@Before
	public void setupRuntime() throws IOException {
		rs = new BasicRuntimeSupport(File.createTempFile("PPP", "PPP").getParentFile());
		rs.setApplicationLogger(new PrintWriterLog());
		rs.getTempDirectory().mkdirs();

		root = new FileBuilder(rs.getTempDirectory()).subdir("rootBin").subdir("SqlServer").subdir("2008_R2_x86").mkdirs().file();

		File sql = new File(root, "sqlcmd.exe");
		File bcp = new File(root, "bcp.exe");

		IOUtils.writeBufferToFile(sql, new StringBuffer(""));
		IOUtils.writeBufferToFile(bcp, new StringBuffer(""));

		queue.clear();

		rs.installProcessExecutor(new ProcessExecutor() {
			public ProcessFacade execute(ProcessDescription processBuilder) {
				queue.offer(processBuilder);

				return new ProcessFacade() {
					public ProcessDescription getDescriptor() {
						return null;
					}

					public void waitForCompletion() {
					}

					public int getCompletionCode() {
						return 0;
					}

					public void kill() {
					}

					public void waitForStreams()
					{
					}

					@Override
					public void waitForOutputStreamsToComplete() {
					}

					public Writer getWriter() {
						return null;
					}

					public BufferedReader getReader() {
						return null;
					}

					public BufferedReader getErrorReader() {
						return null;
					}

					public StringBuffer getInputBuffered() throws IOException {
						return new StringBuffer();
					}

					public StringBuffer getErrorBuffered() throws IOException {
						return new StringBuffer();
					}

					public OutputStream getOutputStream() {
						return null;
					}

					public InputStream getInputStream() {
						return null;
					}

					public InputStream getErrorStream() {
						return null;
					}
				};
			}
		});
	}
}
