package org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database.test;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database.BcpCmd;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class BcpCmdTest extends BaseCmd {
	@Test
	public void testScriptExecute() throws Exception {
		File colExport = rs.createGeneratedSourceFile("sql-server", "destination_col_export.columns");
		IOUtils.writeBufferToFile(colExport, new StringBuffer("A"));

		BcpCmd cmd = new BcpCmd(root, rs, "server", "database", "\t", "\n");

		cmd.exportSql("sql", new File("destination"));

		// one for sqlcmd, one for bcpcmd, one for the export proc to load, and one to export it
		Assert.assertEquals(4, queue.size());
	}
}
