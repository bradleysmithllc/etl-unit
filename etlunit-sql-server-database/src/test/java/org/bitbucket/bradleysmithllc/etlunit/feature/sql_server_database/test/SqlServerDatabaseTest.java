package org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database.test;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database.SqlServerDatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.util.Incomplete;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@FeatureModule
public class SqlServerDatabaseTest extends BaseFeatureModuleTest {
	@Override
	protected List<Feature> getTestFeatures() {
		return Arrays.asList(
			(Feature) new SqlServerDatabaseFeatureModule()
		);
	}

	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("database-definitions")
									.object()
										.key("edw")
											.object()
												.key("server-name")
												.value("localhost")
												.key("implementation-id")
												.value("sql-server")
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		createSource("sql.bitbucket", "class sql {" +
			"@Database(id: 'edw') " +
			"@Test run() {" +
				"stage(connection-id: 'edw', source: 'FILE', target-table: 'TABLE');" +
				"extract(connection-id: 'edw', source-table: 'FILE', target: 'TABLE');" +
				"extract(connection-id: 'edw', sql: 'SELECT * FROM ME', target: 'TABLE_LITERAL_SQL');" +
				"extract(connection-id: 'edw', sql-script: 'sql_script.sql', target: 'TABLE_FILE_SQL');" +
				"execute(connection-id: 'edw', sql: 'SELECT * FROM ME');" +
				"execute(connection-id: 'edw', sql-script: 'sql_script.sql');" +
			"}" +
		"}"
		);

		createSource("data", "FILE.delimited", "");
		createSource("data", "FormatFiles", "FILE.fmt", "");

		createSource("data", "TABLE.delimited", "");
		createSource("data", "FormatFiles", "TABLE.fmt", "<ROW>\n" +
			"  <COLUMN SOURCE=\"1\" NAME=\"FOOD_OR_BEVERAGE_ID\" xsi:type=\"SQLINT\"/>\n" +
			"  <COLUMN SOURCE=\"2\" NAME=\"DESCRIPTION\" xsi:type=\"SQLVARYCHAR\"/>\n" +
			"  <COLUMN SOURCE=\"3\" NAME=\"POPULATION_DATE\" xsi:type=\"SQLDATETIME\"/>\n" +
			" </ROW>");

		createSource("sql", "sql_script.sql", "sql-db.ddl-script");

		createTemp("TABLE.delimited.source", "1|\t,\t|2");
		createTemp("TABLE_LITERAL_SQL.delimited.source", "1|\t,\t|2");
		createTemp("TABLE_FILE_SQL.delimited.source", "1|\t,\t|2");

		createGeneratedSource("sql-server", "TABLE_LITERAL_SQL_delimited_source_col_export.columns", "COL");
		createGeneratedSource("sql-server", "TABLE_FILE_SQL_delimited_source_col_export.columns", "COL");
	}

	@Incomplete
	@Test
	public void run()
	{
		// TODO this needs to be figured out.
		//TestResults results = startTest();

		//Assert.assertEquals(1, results.getNumTestsSelected());
		//Assert.assertEquals(1, results.getMetrics().getNumberOfTestsPassed());
		//Assert.assertEquals(0, results.getMetrics().getNumberOfWarnings());
	}
}
