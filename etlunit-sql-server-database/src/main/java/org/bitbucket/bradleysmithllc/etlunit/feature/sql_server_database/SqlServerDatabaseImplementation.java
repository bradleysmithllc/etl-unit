package org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.inject.Inject;
import javax.inject.Named;

import net.sourceforge.jtds.jdbc.Driver;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Table;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObjectBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class SqlServerDatabaseImplementation extends BaseDatabaseImplementation {
	private Log applicationLog;
	private DatabaseRuntimeSupport databaseRuntimeSupport;

	@Inject
	public void setDatabaseFeatureModule(DatabaseRuntimeSupport databaseFeatureModule) {
		this.databaseRuntimeSupport = databaseFeatureModule;
	}

	private Map<Connection, Table> identityStatus = new HashMap<Connection, Table>();

	@Override
	public void prepareConnectionForInsert(Connection connection, Table target, DatabaseConnection dc, String mode) throws Exception {
		applicationLog.info("Disabling identity insert for " + target.getQualifiedName());

		Table currIdent = identityStatus.get(connection);
		Statement st = connection.createStatement();

		if (currIdent != null) {
			String sql = "SET IDENTITY_INSERT " + escapeQualifiedIdentifier(currIdent) + " OFF";

			try {
				st.execute(sql);
			} catch (SQLException exc) {
				applicationLog.severe("Error disabling identity insert for " + currIdent.getQualifiedName() + " using sql: " + sql, exc);
			}
		}

		if (target.hasIdentityColumns()) {
			applicationLog.info("Enabling identity insert for " + target.getQualifiedName());

			String sql = "SET IDENTITY_INSERT " + escapeQualifiedIdentifier(target) + " ON";

			try {
				st.execute(sql);
				identityStatus.put(connection, target);
			} catch (SQLException exc) {
				applicationLog.severe("Error enabling identity insert for " + target.getQualifiedName() + " using sql: " + sql, exc);
			} finally {
				st.close();
			}
		} else {
			applicationLog.info("Identity insert not required for " + target.getQualifiedName());
		}
	}

	@Inject
	public void setLogger(@Named("applicationLog") Log log) {
		this.applicationLog = log;
	}

	public String getImplementationId() {
		return "sql-server";
	}

	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException {
		switch (op) {
			case createDatabase:
				doCreateDatabase(request.getInitializeRequest());
				break;

			case materializeViews:
				doMaterializeViews(request.getInitializeRequest());
				break;
		}

		return null;
	}

	public String getDefaultSchema(DatabaseConnection dc, String mode) {
		return "DBO";
	}

	private void doMaterializeViews(InitializeRequest initializeRequest) {
		URL url_materialize_views = getClass().getResource("/sqlserver_materialize_views.vm");

		Map<String, String> map = new HashMap<String, String>();
		DatabaseConnection databaseConnection = initializeRequest.getConnection();
		String mode = initializeRequest.getMode();

		try {
			// materialize the views into tables
			String materializeText = IOUtils.readURLToString(url_materialize_views);

			String script = VelocityUtil.writeTemplate(materializeText, map);

			String scriptName = "Materialize_Views_" + getDatabaseName(databaseConnection, mode) + ".sql";

			File scriptFile = runtimeSupport.createGeneratedSourceFile(getImplementationId(), scriptName);

			IOUtils.writeBufferToFile(scriptFile, new StringBuffer(script));

			executeScript(script, databaseConnection, mode, false);
		} catch (Exception e) {
			// TODO:  Get the appropriate exceptions added to this interface method
			throw new IllegalArgumentException(e);
		}
	}

	private void executeScript(String script, DatabaseConnection databaseConnection, String mode, boolean master) throws IOException, TestExecutionError {
		final SQLAggregator sqlagg = databaseRuntimeSupport.resolveSQLRef(script);

		jdbcClient.useStatement(databaseConnection, mode, new JDBCClient.StatementClient() {
			public void connection(Connection conn, Statement st, DatabaseConnection connection, String mode, database_role id) throws Exception {
				SQLAggregator.Aggregator statementAggregator = sqlagg.getStatementAggregator();
				while (statementAggregator.hasNext()) {
					st.execute(statementAggregator.next().getLine());
				}
			}
		}, master ? database_role.sysadmin : database_role.database);
	}

	private void doCreateDatabase(InitializeRequest initializeRequest) {
		URL url_kill = getClass().getResource("/sqlserver_kill_db.vm");
		URL url_create = getClass().getResource("/sqlserver_create_db.vm");

		boolean successful = false;
		int attempts = 0;

		while (!successful && attempts < 10) {
			attempts++;
			applicationLog.info("Creating SqlServer database container.  Pass [" + attempts + "]");
			try {
				String killText = IOUtils.readURLToString(url_kill);

				Map<String, String> map = new HashMap<String, String>();
				DatabaseConnection databaseConnection = initializeRequest.getConnection();
				String mode = initializeRequest.getMode();
				map.put("databaseLogin", getLoginName(databaseConnection, mode));
				map.put("databaseName", getDatabaseName(databaseConnection, mode));
				map.put("databaseUsername", getLoginName(databaseConnection, mode));
				map.put("databasePassword", getPassword(databaseConnection, mode));

				// store AD info in case it is required
				map.put("usesADAuth", String.valueOf(usesADAuth(databaseConnection)));
				map.put("adminUsername", databaseConnection.getAdminUserName());
				map.put("adDomain", getAdDomain(databaseConnection));

				String script = VelocityUtil.writeTemplate(killText, map);

				String scriptName = "Kill_Database_" + getDatabaseName(databaseConnection, mode) + ".sql";

				File scriptFile = runtimeSupport.createGeneratedSourceFile(getImplementationId(), scriptName);

				IOUtils.writeBufferToFile(scriptFile, new StringBuffer(script));

				// execute the script through sqlcmd
				executeScript(script, databaseConnection, mode, true);

				String createText = IOUtils.readURLToString(url_create);

				script = VelocityUtil.writeTemplate(createText, map);

				scriptName = "Create_Database_" + getDatabaseName(databaseConnection, mode) + ".sql";

				scriptFile = runtimeSupport.createGeneratedSourceFile(getImplementationId(), scriptName);

				IOUtils.writeBufferToFile(scriptFile, new StringBuffer(script));

				executeScript(script, databaseConnection, mode, true);

				successful = true;
			} catch (SQLException e) {
				applicationLog.severe("Unexpected SqlServer error.  Aborting.", e);
				// in all other cases, just fail
				throw new IllegalStateException(e);
			} catch (TestExecutionError e) {
				// Special case - look for the exception locking database 'model'
				if (e.getErrorId().equals(DatabaseConstants.ERR_SQL_FAILURE) && e.getCause().getMessage().contains("Could not obtain exclusive lock on database 'model'")) {
					// pause and try again
					applicationLog.severe("SqlServer model database contention.  Pausing before retrying.");
					pause();
				} else {
					applicationLog.severe("Unexpected SqlServer error.  Aborting.", e);
					// in all other cases, just fail
					throw new IllegalStateException(e);
				}
			} catch (Exception e) {
				// TODO:  Get the appropriate exceptions added to this interface method
				applicationLog.severe("Unexpected java exception.  Aborting.", e);
				throw new IllegalStateException(e);
			}
		}

		if (!successful) {
			throw new IllegalStateException("Could not create database after [" + attempts + "] attempts");
		}

		applicationLog.info("Created SqlServer database container in [" + attempts + "] attempts.");
	}

	private static void pause() {
		try {
			Thread.sleep(pauseTime(1000));
		} catch (InterruptedException e) {
		}
	}

	public static long pauseTime(int scale) {
		return (long) ((Math.random() * Long.MAX_VALUE) % scale);
	}

	private void doDropConstraints(InitializeRequest prepareRequest) {
		URL url = getClass().getResource("/sqlserver_drop_constraints.vm");

		try {
			String purgeText = IOUtils.readURLToString(url);
			DatabaseConnection databaseConnection = prepareRequest.getConnection();
			String mode = prepareRequest.getMode();
			String databaseName = getDatabaseName(databaseConnection, mode);

			String scriptName = "Drop_Constraints_" + databaseName + ".sql";

			File scriptFile = runtimeSupport.createGeneratedSourceFile(getImplementationId(), scriptName);

			IOUtils.writeBufferToFile(scriptFile, new StringBuffer(purgeText));

			// execute the script through sqlcmd
			executeScript(purgeText, databaseConnection, mode, false);
		} catch (Exception e) {
			// TODO:  Get the appropriate exceptions added to this interface method
			throw new IllegalStateException("", e);
		}
	}

	@Override
	public String getJdbcUrl(DatabaseConnection dc, String mode, database_role id) {
		String serverPort = dc.getServerPort() != -1 ? (":" + String.valueOf(dc.getServerPort())) : "";

		String url;

		if (id == database_role.sysadmin) {
			url = "jdbc:jtds:sqlserver://" + dc.getServerName() + serverPort + "/master";
		} else {
			url = "jdbc:jtds:sqlserver://" + dc.getServerName() + serverPort + "/" + getDatabaseName(dc, mode);
		}

		Map<String, String> props = dc.getDatabaseProperties();

		if (props != null) {
			String iname = props.get("instance-name");

			if (iname != null) {
				url = url + ";instance=" + iname;
			}

			String idomain = getAdDomain(dc);

			if (idomain != null) {
				url = url + ";useNTLMv2=true;domain=" + idomain;
			}
		}

		return url;
	}

	private String getInstanceName(DatabaseConnection dc) {
		Map<String, String> props = dc.getDatabaseProperties();

		if (props != null) {
			String iinstance = props.get("instance-name");

			if (iinstance != null) {
				return iinstance;
			}
		}

		return null;
	}

	private String getAdDomain(DatabaseConnection dc) {
		Map<String, String> props = dc.getDatabaseProperties();

		if (props != null) {
			String idomain = props.get("domain-name");

			if (idomain != null) {
				return idomain;
			}
		}

		return null;
	}

	private boolean usesADAuth(DatabaseConnection dc) {
		return getAdDomain(dc) != null;
	}

	/**
	 * In the case of AD authentication, do not create SQL users.  Use the AD user for all authentication.
	 */
	@Override
	public String getLoginName(DatabaseConnection dc, String mode, database_role id) {
		return usesADAuth(dc) ? dc.getAdminUserName() : super.getLoginName(dc, mode, id);
	}

	@Override
	protected String getPasswordImpl(DatabaseConnection dc, String mode) {
		if (usesADAuth(dc))
		{
			return dc.getAdminPassword();
		}

		return super.getPasswordImpl(dc, mode);
	}

	@Override
	public Class getJdbcDriverClass() {
		return Driver.class;
	}

	@Override
	public boolean isUserSchema(DatabaseConnection dc, String schemaName) {
		return schemaName != null && (schemaName.equalsIgnoreCase("SYS"));
	}

	@Override
	public void propagateImplementationProperties(DatabaseConnection databaseConnection, String mode, ETLTestValueObjectBuilder builder) {
		// add in the ad auth flag
		boolean usesADAuth = usesADAuth(databaseConnection);
		builder.key("usesAdAuth").value(String.valueOf(usesADAuth));

		// add in the domain name if available
		if (usesADAuth)
		{
			builder.key("domain-name").value(getAdDomain(databaseConnection));
		}

		// add in the instance name if present
		String instanceName = getInstanceName(databaseConnection);

		if (instanceName != null)
		{
			builder.key("instance-name").value(instanceName);
		}
	}

	protected String openEscapeChar() {
		return "[";
	}

	protected String closeEscapeChar() {
		return "]";
	}
}
