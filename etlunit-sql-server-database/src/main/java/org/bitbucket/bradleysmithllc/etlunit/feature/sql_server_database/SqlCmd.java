package org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;

import java.io.File;

public class SqlCmd {
	private final File sqlCmd;
	private final RuntimeSupport runtimeSupport;

	public SqlCmd(File sqlBinDir, RuntimeSupport runtimeSupport) {
		this.sqlCmd = new File(sqlBinDir, "sqlcmd.exe");

		if (!sqlCmd.exists())
		{
			throw new IllegalArgumentException("sqlcmd.exe not found");
		}

		this.runtimeSupport = runtimeSupport;
	}

	public void executeScript(
		String serverName,
		String databaseName,
		File scriptFile
	) throws Exception
	{
		ProcessDescription pb = new ProcessDescription(
			sqlCmd.getAbsolutePath())
			.argument("-b")
			.argument("-E")
			.argument("-S")
			.argument(serverName)
			.argument("-d")
			.argument(databaseName)
			.argument("-i")
			.argument(scriptFile.getAbsolutePath());

		ProcessFacade res = runtimeSupport.execute(pb);
		res.waitForCompletion();

		if (res.getCompletionCode() != 0)
		{
			throw new TestExecutionError("Could not execute script: " + scriptFile.getAbsolutePath() + " : " + res.getInputBuffered());
		}
	}
}
