package org.bitbucket.bradleysmithllc.etlunit.feature.sql_server_database;

/*
 * #%L
 * etlunit-sql-server-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.inject.Inject;
import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@FeatureModule
public class SqlServerDatabaseFeatureModule extends AbstractFeature {
	private static final List<String> prerequisites = Arrays.asList("logging", "database");
	private DatabaseFeatureModule databaseFeatureModule;

	@Inject
	public void setDatabaseFeature(DatabaseFeatureModule feature)
	{
		databaseFeatureModule = feature;
	}

	@Override
	public void initialize(Injector inj) {
		ETLTestValueObject value = configuration.query("vendor-binary-directory");

		if (value == null)
		{
			throw new IllegalArgumentException("Configuration property 'vendor-binary-directory' not present");
		}

		File path = new FileBuilder(value.getValueAsString()).subdir("SqlServer").subdir("2008_R2_x86").file();

		databaseFeatureModule.addDatabaseImplementation(postCreate(new SqlServerDatabaseImplementation()));
	}

	@Override
	public List<String> getPrerequisites() {
		return prerequisites;
	}

	public String getFeatureName() {
		return "sql-server-database";
	}
}
