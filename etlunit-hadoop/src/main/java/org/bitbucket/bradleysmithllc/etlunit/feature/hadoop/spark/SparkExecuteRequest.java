package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkJob;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkProxy;

import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SparkExecuteRequest extends SparkProcessorRequest {
	public static final String EOM = "================";
	private final SparkProxy proxyClass;
	private final String job;
	private final Map<String, File> roots;

	public SparkExecuteRequest(SparkProxy proxyClass, String job, Map<String, File> roots) {
		this.proxyClass = proxyClass;
		this.job = job;
		this.roots = roots;
	}

	@Override
	void sendBody(PrintWriter printWriter) {
		sendTagged(printWriter, proxyClass.getClass().getName());
		sendTagged(printWriter, job);

		for (Map.Entry<String, File> root : roots.entrySet())
		{
			sendTagged(printWriter, root.getKey());
			sendTagged(printWriter, root.getValue().getAbsolutePath());
		}

		sendTagged(printWriter, EOM);
	}

	@Override
	public type type() {
		return type.execute;
	}

	public static SparkProcessorRequest read(BufferedReader reader) throws Exception {
		String proxyClassName = readNextTagged(reader);
		String jobId = readNextTagged(reader);

		SparkProxy sp = (SparkProxy) Thread.currentThread().getContextClassLoader().loadClass(proxyClassName).newInstance();
		SparkJob sj = sp.sparkJob(jobId);

		Map<String, File> rootsMap = new HashMap<>();

		String line = null;

		while ((line = readNextTagged(reader)) != null)
		{
			if (line.equals(EOM))
			{
				break;
			}

			String path = readNextTagged(reader);

			rootsMap.put(line, new File(path));
		}

		SparkExecuteRequest sparkExecuteRequest = new SparkExecuteRequest(
				sp,
				jobId,
				rootsMap
		);

		System.out.println(EOM);
		System.out.println("Received execute request: " + sparkExecuteRequest);
		System.out.println(EOM);

		return sparkExecuteRequest;
	}

	public SparkProxy proxy() {
		return proxyClass;
	}

	public SparkJob job() {
		return proxy().sparkJob(job);
	}

	public Map<String, File> roots() {
		return roots;
	}

	@Override
	public String toString() {
		return "SparkExecuteRequest{" +
				"proxyClass=" + proxyClass +
				", job=" + job +
				", roots=" + roots +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SparkExecuteRequest that = (SparkExecuteRequest) o;
		return Objects.equals(proxyClass, that.proxyClass) &&
				Objects.equals(job, that.job) &&
				Objects.equals(roots(), that.roots());
	}

	@Override
	public int hashCode() {

		return Objects.hash(proxyClass, job, roots());
	}
}
