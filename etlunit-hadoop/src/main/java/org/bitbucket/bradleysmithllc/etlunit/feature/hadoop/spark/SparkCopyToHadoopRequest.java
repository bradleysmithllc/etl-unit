package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class SparkCopyToHadoopRequest extends SparkProcessorRequest {
	public enum Format {
		avro,
		parquet,
		csv,
		orc,
		json;

		private String sparkFormat;

		public String sparkFormat() {
			return ObjectUtils.firstNotNull(sparkFormat, name());
		}

		Format() {
			this(null);
		}

		Format(String s) {
			sparkFormat = s;
		}
	}

	private final File srcDataFile;
	private final File srcSchema;
	private final String srcSchemaId;
	private final File dstFile;
	private final Format format;

	public SparkCopyToHadoopRequest(File srcDataFile, File srcSchema, String srcSchemaId, File dstFile, Format format) {
		this.srcDataFile = srcDataFile;
		this.srcSchema = srcSchema;
		this.dstFile = dstFile;
		this.format = format;
		this.srcSchemaId = srcSchemaId;
	}

	public String getSrcSchemaId() {
		return srcSchemaId;
	}

	public File getSrcDataFile() {
		return srcDataFile;
	}

	public File getSrcSchema() {
		return srcSchema;
	}

	public File getDstFile() {
		return dstFile;
	}

	public Format getFormat() {
		return format;
	}

	@Override
	public type type() {
		return type.copyToHadoop;
	}

	@Override
	void sendBody(PrintWriter printWriter) {
		sendTagged(printWriter, getSrcDataFile().getAbsolutePath());
		sendTagged(printWriter, getSrcSchema().getAbsolutePath());
		sendTagged(printWriter, getSrcDataFile().getAbsolutePath());
		sendTagged(printWriter, getDstFile().getAbsolutePath());
		sendTagged(printWriter, getFormat().name());
	}

	public static SparkProcessorRequest read(BufferedReader reader) throws IOException {
		return new SparkCopyToHadoopRequest(
				new File(readNextTagged(reader)),
				new File(readNextTagged(reader)),
				readNextTagged(reader),
				new File(readNextTagged(reader)),
				SparkCopyToHadoopRequest.Format.valueOf(readNextTagged(reader))
		);
	}

	@Override
	public String toString() {
		return "CopyRecord{" +
				"srcDataFile=" + srcDataFile +
				", srcSchema=" + srcSchema +
				", srcSchemaId='" + srcSchemaId + '\'' +
				", dstFile=" + dstFile +
				", format=" + format +
				'}';
	}
}
