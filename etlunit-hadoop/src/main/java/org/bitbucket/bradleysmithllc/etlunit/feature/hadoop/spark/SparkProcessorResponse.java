package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class SparkProcessorResponse {
	public enum type {
		done,
		doneWithFailure
	}

	private final type _type;
	private final String message;

	public SparkProcessorResponse(type _t, String fm) {
		_type = _t;
		message = fm;
	}

	public SparkProcessorResponse() {
		this(type.done, "none");
	}

	public SparkProcessorResponse(String failureMessage) {
		this(type.doneWithFailure, failureMessage);
	}

	public type type() {
		return _type;
	}

	public String message() {
		return message;
	}

	public void send(PrintWriter systemOutWriter) {
		SparkProcessorRequest.sendTagged(systemOutWriter, _type.name());
		SparkProcessorRequest.sendTagged(systemOutWriter, URLEncoder.encode(message));

		systemOutWriter.flush();
	}

	public static SparkProcessorResponse read(BufferedReader reader) throws IOException {
		String name = SparkProcessorRequest.readNextTagged(reader);
		type type = SparkProcessorResponse.type.valueOf(name);

		String message = URLDecoder.decode(SparkProcessorRequest.readNextTagged(reader));

		return new SparkProcessorResponse(type, message);
	}

	@Override
	public String toString() {
		return "SparkProcessorResponse{" +
				"_type=" + _type +
				", message='" + message + '\'' +
				'}';
	}
}
