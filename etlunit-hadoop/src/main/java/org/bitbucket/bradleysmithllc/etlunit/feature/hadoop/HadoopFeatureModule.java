package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-feature-archetype
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import com.google.inject.name.Named;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileConstants;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.FileRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.file.RequestedFmlNotFoundException;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.HadoopFeatureModuleConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop._assert.AssertHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop._assert.AssertRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop._assert.DatasetProperties;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop._assert.HdfsSource;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.execute.ExecuteHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.execute.ExecuteRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.execute_spark.ExecuteSparkHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.execute_spark.ExecuteSparkRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage_hadoop.StageHadoopHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage_hadoop.StageHadoopRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkGenerator;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkJob;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkProxy;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.workspace.Workspace;
import org.bitbucket.bradleysmithllc.etlunit.feature.workspace.WorkspaceRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.*;
import org.bitbucket.bradleysmithllc.etlunit.io.file.reference_file_type.ReferenceFileTypeRef;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;
import org.codehaus.plexus.util.FileUtils;

import javax.inject.Inject;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.*;

@FeatureModule
public class HadoopFeatureModule extends AbstractFeature {
	private static final List<String> prerequisites = Arrays.asList("logging", "workspace", "database");
	private final HadoopFeatureListener listener = new HadoopFeatureListener(this);
	private String defaultProxyName = null;
	private String defaultJobName = null;

	private final Map<String, String> proxies = new HashMap<String, String>();

	HadoopFeatureModuleConfiguration.SparkProcess sparkJobExecutor = null;
	private MetaDataManager metaDataManager;
	private MetaDataContext dataMetaContext;
	private MetaDataContext executeMetaContext;
	String sparkGeneratorName;

	@Inject
	public void receiveMetaDataManager(MetaDataManager manager) {
		metaDataManager = manager;
		dataMetaContext = metaDataManager.getMetaData().getOrCreateContext("data");
		executeMetaContext = metaDataManager.getMetaData().getOrCreateContext("execute");
	}

	@Override
	public List<String> getPrerequisites() {
		return prerequisites;
	}

	@Inject
	public void receiveConfiguration(HadoopFeatureModuleConfiguration conf) {
		sparkJobExecutor = conf.getSparkProcess();

		if (sparkJobExecutor == null) {
			sparkJobExecutor = HadoopFeatureModuleConfiguration.SparkProcess.OUT_OF_PROCESS;
		}

		defaultJobName = conf.getDefaultSparkJob();
		defaultProxyName = conf.getDefaultSparkProxy();

		for (Map.Entry<String, String> entry : conf.getSparkProxies().getAdditionalProperties().entrySet()) {
			proxies.put(entry.getKey(), entry.getValue());
		}

		sparkGeneratorName = ObjectUtils.firstNotNull(conf.getSparkGenerator(), ExternalizableSparkGenerator.class.getName());
	}

	@Override
	public void initialize(Injector inj) {
		postCreate(listener);
	}

	@Override
	public ClassListener getListener() {
		return listener;
	}

	public String resolveProxy(String id) throws TestExecutionError {
		String effectiveName = ObjectUtils.firstNotNull(id, defaultProxyName);

		if (effectiveName == null) {
			throw new TestExecutionError("No spark-proxy specified and no default set", HadoopConstants.ERR_MISSING_SPARK_PROXY);
		}

		if (!proxies.containsKey(effectiveName)) {
			throw new TestExecutionError("Requested spark-proxy not configured", HadoopConstants.ERR_INVALID_SPARK_PROXY_ID);
		}

		return proxies.get(effectiveName);
	}

	public String defaultJobName() {
		return defaultJobName;
	}

	public MetaDataContext dataMetaContext() {
		return dataMetaContext;
	}
}

class HadoopFeatureListener extends NullClassListener implements StageHandler, StageHadoopHandler, AssertHandler, ExecuteHandler, ExecuteSparkHandler {
	private final HadoopFeatureModule parent;
	private WorkspaceRuntimeSupport workspaceRuntimeSupport;

	private RuntimeSupport runtimeSupport;
	private FileRuntimeSupport fileRuntimeSupport;

	private DataFileManager dataFileManager;

	public SparkProcessor sparkProcessor() throws IOException, TestExecutionError {
		return sparkProcessor(true);
	}

	private SparkProcessor sparkProcessor = null;

	public synchronized SparkProcessor inprocessSparkProcessor() throws IOException, TestExecutionError {
		if (sparkProcessor == null) {
			sparkProcessor = new SparkProcessor(generator());
		}

		return sparkProcessor;
	}

	private SparkGenerator generator() throws TestExecutionError {
		try {
			Class<?> cls = Thread.currentThread().getContextClassLoader().loadClass(parent.sparkGeneratorName);

			if (!SparkGenerator.class.isAssignableFrom(cls)) {
				throw new TestExecutionError("Spark Generator class '" + parent.sparkGeneratorName + "' does not implement SparkGenerator", HadoopConstants.ERR_SPARK_GENERATOR_DOES_NOT_IMPLEMENT_INTERFACE);
			}

			Constructor<?> constructor = cls.getConstructor(RuntimeSupport.class);
			Object sparkGenerator = constructor.newInstance(runtimeSupport);
			return (SparkGenerator) sparkGenerator;
		} catch (TestExecutionError e) {
			throw e;
		} catch (Throwable e) {
			throw new TestExecutionError("Spark Generator class '" + parent.sparkGeneratorName + "' cannot be loaded", HadoopConstants.ERR_LOADING_SPARK_GENERATOR, e);
		}
	}

	public SparkProcessor sparkProcessor(boolean createOnDemand) throws IOException, TestExecutionError {
		// create a singleton spark processor on demand
		SparkProcessor sparkProcessor = runtimeSupport.retrieveFromExecutor(parent, "sparkProcessor", SparkProcessor.class);

		if (sparkProcessor == null && createOnDemand) {
			runtimeSupport.getApplicationLog().info("Initializing Spark Processor");

			if (parent.sparkJobExecutor == HadoopFeatureModuleConfiguration.SparkProcess.OUT_OF_PROCESS) {
				runtimeSupport.getApplicationLog().info("Using out-of-process spark processor");
				sparkProcessor = new SparkProcessor(runtimeSupport, generator());
			} else {
				// for running in process, we can only have 1 instance of the spark processor.
				runtimeSupport.getApplicationLog().info("Using in-process spark processor");
				sparkProcessor = inprocessSparkProcessor();
			}

			runtimeSupport.storeInExecutor(parent, "sparkProcessor", sparkProcessor);
		}

		// check to see if we are currently processing a test and add the spark log file as a dependency
		ETLTestMethod cptm = runtimeSupport.getCurrentlyProcessingTestMethod();

		if (cptm != null) {
			File plf = sparkProcessor.processLogFile();

			if (plf != null) {
				MetaDataPackageContext dataPackageContext = parent.dataMetaContext().createPackageContextForCurrentTest(MetaDataPackageContext.path_type.external_source);
				try {
					MetaDataArtifact dataPackageContextArtifact = dataPackageContext.createArtifact("process-log", plf.getParentFile());
					dataPackageContextArtifact.createContent(plf.getName()).referencedByCurrentTest("process-log");
				} catch (IOException e) {
					throw new TestExecutionError("Error creating meta data context", e);
				}
			}
		}

		return sparkProcessor;
	}

	@Override
	public void endTests(VariableContext context) {
		if (sparkProcessor != null) {
			try {
				sparkProcessor.dispose();
			} catch (IOException e) {
				runtimeSupport.getApplicationLog().severe("Failure disposing spark processor", e);
			}
		}
	}

	@Override
	public void endTests(VariableContext context, int executorId) {
		// only dispose processors in this end if we are running OOP
		if (parent.sparkJobExecutor == HadoopFeatureModuleConfiguration.SparkProcess.OUT_OF_PROCESS) {
			try {
				SparkProcessor sparkProcessor = sparkProcessor(false);

				if (sparkProcessor != null) {
					sparkProcessor.dispose();
				}
			} catch (Exception e) {
				runtimeSupport.getApplicationLog().severe("Failure disposing spark processor", e);
			} finally {
				runtimeSupport.removeStoredFromExecutor(parent, "sparkProcessor");
			}
		}
	}

	public HadoopFeatureListener(HadoopFeatureModule hadoopFeatureModule) {
		parent = hadoopFeatureModule;
	}

	@Inject
	@Named("hadoop.refreshAssertionData")
	private RuntimeOption refreshAssertionData = null;

	@Inject
	public void receiveWorkspaceRuntimeSupport(WorkspaceRuntimeSupport wsr) {
		workspaceRuntimeSupport = wsr;
	}

	@Inject
	public void receiveDataFileManager(DataFileManager manager) {
		dataFileManager = manager;
	}

	@Inject
	public void workspaceFeatureModule(WorkspaceRuntimeSupport wrs) {
		workspaceRuntimeSupport = wrs;
	}

	@Inject
	public void runtimeSupport(RuntimeSupport rs) {
		runtimeSupport = rs;
	}

	@Inject
	public void fileRuntimeSupport(FileRuntimeSupport rs) {
		fileRuntimeSupport = rs;
	}

	@Override
	public void begin(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// clear out any hdfs mount points.  This should never happen but since this listener is persistent it is safe.
		if (context.hasVariableBeenDeclared("hdfs")) {
			context.removeVariable("hdfs");
		}

		context.declareVariable("hdfs");

		List<String> roots = new ArrayList<String>();

		List<ETLTestAnnotation> mtalist = mt.getAnnotations("@Hdfs");
		List<ETLTestAnnotation> clalist = mt.getTestClass().getAnnotations("@Hdfs");

		List<ETLTestAnnotation> cmbList = new ArrayList<ETLTestAnnotation>(mtalist);
		cmbList.addAll(clalist);

		// for each annotation, grab the id (which defaults to 'hdfs' if not specified).
		for (ETLTestAnnotation annotation : cmbList) {
			ETLTestValueObject props = annotation.getValue();

			String id = "hdfs";
			List<String> modeList = new ArrayList<String>();

			if (props != null) {
				ETLTestValueObject idVal = props.query("id");

				if (idVal != null) {
					id = idVal.getValueAsString();
				}

				ETLTestValueObject modes = props.query("modes");
				ETLTestValueObject hmode = props.query("mode");

				if (modes != null) {
					for (String mode : modes.getValueAsListOfStrings()) {
						modeList.add(mode);
					}
				}

				if (hmode != null) {
					modeList.add(hmode.getValueAsString());
				}

				if (modes == null && hmode == null) {
					modeList.add("");
				}
			} else {
				modeList.add("");
			}

			// create each id / mode combination of hdfs roots
			for (String mode : modeList) {
				String hdfsRoot = id;

				if (!mode.equals("")) {
					hdfsRoot += "." + mode;
				}

				if (roots.contains(hdfsRoot)) {
					throw new TestExecutionError("Hdfs id '" + hdfsRoot + "' decalred multiple times", HadoopConstants.ERR_HDFS_ROOTS_NOT_UNIQUE);
				}

				roots.add(hdfsRoot);
			}

		}

		try {
			Workspace workspace = workspaceRuntimeSupport.currentWorkspace();

			for (String root : roots) {
				workspace.mkPartition(root);
			}
		} catch (IOException exc) {
			throw new TestExecutionError("", HadoopConstants.ERR_PREPARE_HDFS_WORKSPACES, exc);
		}
	}

	// go through the annotations and configuration and determine the most applicable spark proxy and job
	// for this text
	private Pair<String, String> loadSparkInfo(ETLTestMethod mt) throws TestExecutionError {
		// only one annotation per class or method makes sense, but one class and method is reasonable.
		List<ETLTestAnnotation> mtalist = mt.getAnnotations("@Hadoop");

		if (mtalist.size() > 1) {
			throw new TestExecutionError("Only one Hadoop annotation may be specified at the method level", HadoopConstants.ERR_MULTIPLE_METHOD_HADOOP_ANNOTATIONS);
		}

		List<ETLTestAnnotation> clalist = mt.getTestClass().getAnnotations("@Hadoop");

		if (clalist.size() > 1) {
			throw new TestExecutionError("Only one Hadoop annotation may be specified at the class level", HadoopConstants.ERR_MULTIPLE_CLASS_HADOOP_ANNOTATIONS);
		}

		List<ETLTestAnnotation> alist = new ArrayList<ETLTestAnnotation>(clalist);
		alist.addAll(mtalist);

		String proxyName = null;
		String jobName = parent.defaultJobName();

		// use a for loop here even though there can only be 0..1 records
		for (ETLTestAnnotation proxy : alist) {
			ETLTestValueObject pv = proxy.getValue().query("spark-proxy");

			if (pv != null) {
				proxyName = pv.getValueAsString();
			}

			ETLTestValueObject jv = proxy.getValue().query("spark-job");

			if (jv != null) {
				jobName = jv.getValueAsString();
			}
		}

		return new ImmutablePair<String, String>(proxyName, jobName);
	}

	private File workspaceRoot(String root) {
		Workspace workspace = workspaceRuntimeSupport.currentWorkspace();

		File froot = workspace.openPartition(root);

		if (!froot.exists()) {
			return null;
		}

		return froot;
	}

	@Override
	public void end(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		context.removeVariable("hdfs");
	}

	@Override
	public action_code _assert(final AssertRequest request, final ETLTestMethod testMethod, final ETLTestOperation testOperation, ETLTestValueObject valueObject, final VariableContext variableContext, final ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError {
		// update this to use the source (actual) schema.
		AssertRequest.AssertionMode am = request.getAssertionMode();
		final FileRuntimeSupport.DataSetAssertionRequest.assertMode assertionMode;

		if (am == null) {
			assertionMode = FileRuntimeSupport.DataSetAssertionRequest.assertMode.equals;
		} else {
			switch (am) {
				case EQUALS:
					assertionMode = FileRuntimeSupport.DataSetAssertionRequest.assertMode.equals;
					break;
				case EXISTS:
					assertionMode = FileRuntimeSupport.DataSetAssertionRequest.assertMode.exists;
					break;
				case NOT_EXISTS:
					assertionMode = FileRuntimeSupport.DataSetAssertionRequest.assertMode.not_exists;
					break;
				case EMPTY:
					assertionMode = FileRuntimeSupport.DataSetAssertionRequest.assertMode.empty;
					break;
				case NOT_EMPTY:
					assertionMode = FileRuntimeSupport.DataSetAssertionRequest.assertMode.not_empty;
					break;
				default:
					throw new IllegalStateException("WT");
			}
		}

		/* Acceptable scenarios.
		 *  1 - Assert equals or not equals with a reference file type on the hdfs source
		 *  2 - Hdfs source format RAW and assertion mode is exists, not exists, empty, not empty
		 */
		//Add asserts here to enforce that for assert equals or not equals a reference
		//		file type is required.
		if (
				assertionMode == FileRuntimeSupport.DataSetAssertionRequest.assertMode.equals
						&
						(fileRuntimeSupport.resolveOperandReferenceFileType(request.getHdfsSource().getReferenceFileType()) == null)
		) {
			throw new TestExecutionError("Assert mode 'equals' requires a reference file type on the hdfs source", HadoopConstants.ERR_ASSERT_EQUALS_REQUIRES_FILE_TYPE);
		}

		final HdfsSource.Format format = ObjectUtils.firstNotNull(request.getHdfsSource().getFormat(), HdfsSource.Format.RAW);

		// Acceptable scenario (2)
		if (format == HdfsSource.Format.RAW && request.getAssertionMode() == AssertRequest.AssertionMode.EQUALS) {
			throw new TestExecutionError("RAW format cannot be used with equals assertion mode.", HadoopConstants.ERR_ASSERT_MODE_FORMAT_MISMATCH);
		}

		// grab the source workspace
		String rootId = ObjectUtils.firstNotNull(request.getHdfsSource().getHdfsId(), "hdfs");
		String mode = request.getHdfsSource().getMode();

		String qualifiedRootId = rootId + (mode != null ? ("." + mode) : "");

		Workspace wksp = workspaceRuntimeSupport.currentWorkspace();
		final File partition = wksp.openPartition(qualifiedRootId);

		// determine the source for the assertion
		final AssertionSource assertionSource;

		try {
			assertionSource = resolveAssertionSource(request, partition, assertionMode, format, testOperation);
		} catch (IOException e) {
			throw new TestExecutionError("Resolving assertion source", HadoopConstants.ERR_IO_ERR_WRITING_FILES, e);
		}

		fileRuntimeSupport.processDataSetAssertion(new FileRuntimeSupport.DataSetAssertionRequest() {
			@Override
			public void processRefreshedAssertionData(DataFile expected) {
			}

			@Override
			public assertMode getAssertMode() {
				return assertionMode;
			}

			@Override
			public String getActualSchemaName() {
				return request.getHdfsSource().getPath();
			}

			@Override
			public String getActualBackupSchemaName() {
				ReferenceFileTypeRef referenceFileTypeRef = fileRuntimeSupport.resolveOperandReferenceFileType(request.getHdfsSource().getReferenceFileType());

				if (referenceFileTypeRef != null) {
					return referenceFileTypeRef.getId();
				} else {
					return getActualSchemaName();
				}
			}

			@Override
			public String getExpectedSchemaName() {
				return request.getTarget();
			}

			@Override
			public String getExpectedBackupSchemaName() {
				return getExpectedSchemaName();
			}

			@Override
			public boolean hasColumnList() {
				return false;
			}

			@Override
			public columnListMode getColumnListMode() {
				return columnListMode.exclude;
			}

			@Override
			public Set<String> getColumnList() {
				return Collections.emptySet();
			}

			@Override
			public String getFailureId() {
				return request.getFailureId();
			}

			@Override
			public DataFile getExpectedDataset(
					DataFileSchema explicitActualSchema,
					DataFileSchema actualEffectiveSchema,
					DataFileSchema explicitExpectedSchema,
					DataFileSchema expectedEffetiveSchema
			) {
				return dataFileManager.loadDataFile(fileRuntimeSupport.getDataFileForCurrentTest(request.getTarget()), expectedEffetiveSchema);
			}

			@Override
			public DataFile getActualDataset(
					DataFileSchema explicitActualSchema,
					DataFileSchema actualEffectiveSchema,
					DataFileSchema explicitExpectedSchema,
					DataFileSchema expectedEffectiveSchema
			) throws IOException, TestExecutionError {
				return dataFileManager.loadDataFile(
						assertionSource.dataFile,
						(getAssertMode() == assertionMode.equals && request.getHdfsSource().getFormat() != HdfsSource.Format.CSV) ? dataFileManager.loadDataFileSchema(assertionSource.dataFileSchema, "hadoop-src") : actualEffectiveSchema);
			}

			@Override
			public ETLTestMethod getTestMethod() {
				return testMethod;
			}

			@Override
			public ETLTestOperation getTestOperation() {
				return testOperation;
			}

			@Override
			public ETLTestValueObject getOperationParameters() {
				return testOperation.getOperands();
			}

			@Override
			public VariableContext getVariableContext() {
				return variableContext;
			}

			@Override
			public MetaDataPackageContext getMetaDataPackageContext() {
				return null;
			}

			@Override
			public boolean refreshAssertionData() {
				return refreshAssertionData.isEnabled();
			}

			@Override
			public DataFileSchema getSourceSchema() {
				if (assertionSource.dataFileSchema != null) {
					return dataFileManager.loadDataFileSchema(assertionSource.dataFileSchema, "synth-hadoop-" + request.getHdfsSource().getPath());
				}

				return null;
			}
		});

		return action_code.handled;
	}

	final class AssertionSource {
		File dataFile;
		File dataFileSchema;
	}

	private AssertionSource resolveAssertionSource(AssertRequest request, File partition, FileRuntimeSupport.DataSetAssertionRequest.assertMode assertionMode, HdfsSource.Format format, ETLTestOperation testOperation) throws IOException, TestExecutionError {
		// check the assert mode.  In the case of anything but equals, just return the file as is.
		// for equals, determine the format and request spark to convert it to delimited and use that for comparison

		AssertionSource as = new AssertionSource();

		as.dataFile = new File(partition, request.getHdfsSource().getPath());

		switch (assertionMode) {
			case equals:
				runtimeSupport.getApplicationLog().info("Converting Hadoop files in format [" + format + "] to csv");
				// convert from the existing format into a delimited format matching our schema.
				SparkProcessor sp = sparkProcessor();

				File destDir = as.dataFile;

				if (format != HdfsSource.Format.CSV) {
					if (format == HdfsSource.Format.RAW) {
						throw new TestExecutionError("Hdfs Source format must resolve to something other than raw.", HadoopConstants.ERR_HDFS_SOURCE_REQUIRES_FORMAT);
					}

					destDir = runtimeSupport.createAnonymousTempFolder();

					// pass null in for the schema and schema ID which means pull the schema from the spark data frame
					SparkCopyFromHadoopRequest sprequest = new SparkCopyFromHadoopRequest(
							as.dataFile,
							destDir,
							null,//sch,
							null,//actualEffectiveSchema.getId(),
							SparkCopyToHadoopRequest.Format.valueOf(format.value())
					);

					DatasetProperties datasetProperties = request.getHdfsSource().getDatasetProperties();

					if (datasetProperties != null) {
						for (Map.Entry<String, String> entry : datasetProperties.getAdditionalProperties().entrySet()) {
							sprequest.addDatasetOption(entry.getKey(), entry.getValue());
						}
					}

					SparkProcessorResponse response = sp.send(sprequest);

					File genDir = runtimeSupport.getGeneratedSourceDirectory(parent.getFeatureName());

					as.dataFileSchema = new File(genDir, testOperation.getQualifiedName() + ".fml");

					FileUtils.fileWrite(as.dataFileSchema, response.message());

					runtimeSupport.getApplicationLog().info("Wrote inferred schema to '" + as.dataFileSchema.getAbsolutePath() + "'");

					// at this point the resulting files are in destDir.  Look for _SUCCESS
					if (!new File(destDir, "_SUCCESS").exists()) {
						throw new IOException("Spark did not write out delimited files");
					}
				}

				File[] files = destDir.listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						String name = pathname.getName();
						return name.endsWith(".csv");
					}
				});

				as.dataFile = runtimeSupport.createAnonymousTempFile();

				runtimeSupport.getApplicationLog().info("Aggregating output files to temp file: " + as.dataFile);

				for (File file : files) {
					IOUtils.copyFiles(file, as.dataFile, true);
				}

				break;
			case exists:
				// in these cases - use the '_SUCCESS' marker if we are looking at a hadoop output file
			case not_exists:
				switch (format) {
					case RAW:
						break;
					case AVRO:
					case JSON:
					case PARQUET:
					case CSV:
						as.dataFile = new File(as.dataFile, "_SUCCESS");
				}
				break;
			case empty:
				break;
			case not_empty:
				break;
		}

		runtimeSupport.getApplicationLog().info("Using actual file path: " + as.dataFile);

		return as;
	}

	@Override
	public action_code executeSpark(ExecuteSparkRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return executeImpl(request.getSparkProxy(), request.getSparkJob(), testMethod, testOperation, valueObject, variableContext, executionContext);
	}

	@Override
	public action_code execute(ExecuteRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, final VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return executeImpl(request.getSparkProxy(), request.getSparkJob(), testMethod, testOperation, valueObject, variableContext, executionContext);
	}

	private action_code executeImpl(String proxyName, String jobId, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, final VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		Pair<String, String> proxyJobInfo = loadSparkInfo(testMethod);

		// use the operation proxy, then the method proxy, then the class proxy, then the default, in that order
		String proxy = parent.resolveProxy(
				ObjectUtils.firstNotNull(
						proxyName,
						proxyJobInfo.getLeft()
				)
		);

		variableContext.declareAndSetStringValue("spark-proxy", proxy);

		// use the operation job, then the method job, then the class job, then the default, in that order
		String jobName =
				ObjectUtils.firstNotNull(
						jobId,
						proxyJobInfo.getRight()
				);

		if (jobName == null) {
			throw new TestExecutionError("Could not resolve spark-job name", HadoopConstants.ERR_MISSING_SPARK_JOB);
		}

		variableContext.declareAndSetStringValue("spark-job", jobName);

		if (proxy == null) {
			throw new TestExecutionError(proxy, HadoopConstants.ERR_UNSPECIFIED_SPARK_PROXY);
		}

		Object proxyObj = null;
		SparkProxy proxyInt = null;

		try {
			runtimeSupport.getApplicationLog().info("Using spark proxy [" + proxy + "]");
			proxyObj = Thread.currentThread().getContextClassLoader().loadClass(proxy).newInstance();
		} catch (Throwable e) {
			throw new TestExecutionError("Error in ContextClassLoader().loadClass for spark proxy", HadoopConstants.ERR_LOADING_SPARK_PROXY, e);
		}

		if (SparkProxy.class.isAssignableFrom(proxyObj.getClass())) {
			proxyInt = (SparkProxy) proxyObj;
		} else {
			throw new TestExecutionError("Specified proxy does not implement " + SparkProxy.class.getName(), HadoopConstants.ERR_SPARK_PROXY_DOES_NOT_IMPLEMENT_INTERFACE);
		}

		SparkJob job = null;

		try {
			job = proxyInt.sparkJob(jobName);

			if (job == null) {
				throw new TestExecutionError("Proxy rejected spark-job '" + jobName + "'", HadoopConstants.ERR_INVALID_SPARK_JOB);
			}
		} catch (TestExecutionError e) {
			throw e;
		} catch (Throwable e) {
			throw new TestExecutionError("Error in spark proxy sparkJob()", HadoopConstants.ERR_IN_SPARK_PROXY_GET_JOB, e);
		}

		// one last sanity check to ensure that the job is ready to go
		final Workspace workspace = workspaceRuntimeSupport.currentWorkspace();

		try {
			if (!workspace.partitionKeys().containsAll(job.requiredFilesystems())) {
				throw new TestExecutionError("Spark job requires roots " + job.requiredFilesystems() + " - at least one was missing (available " + workspace.partitionKeys() + ")", HadoopConstants.ERR_IN_SPARK_JOB_MISSING_REQUIRED_FS_ROOTS);
			}
		} catch (TestExecutionError err) {
			throw err;
		} catch (Throwable thr) {
			throw new TestExecutionError("Error verifying environment for spark job", HadoopConstants.ERR_VERIFY_SPARK_JOB, thr);
		}

		try {
			// get a spark processor to handle the request.
			SparkProcessor sp = sparkProcessor();

			Map<String, File> roots = new HashMap<>();

			for (String key : workspace.partitionKeys()) {
				roots.put(key, workspace.openPartition(key));
			}

			sp.send(new SparkExecuteRequest(proxyInt, jobName, roots));
		} catch (TestExecutionError e) {
			throw e;
		} catch (Throwable e) {
			throw new TestExecutionError("Spark job failed", HadoopConstants.ERR_RUNNING_SPARK_JOB, e);
		}

		return action_code.handled;
	}

	@Override
	public action_code stage(StageRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestExecutionError {
		// determine source schema first and foremost, if the request is not for a RAW stage
		DataFile sourceDF = null;

		ETLTestPackage _package = runtimeSupport.getCurrentlyProcessingTestPackage();
		String _dataSetName = request.getSourceFile();

		Pair<ETLTestPackage, String> results = runtimeSupport.processPackageReference(_package, _dataSetName);

		File sourceDataFile = fileRuntimeSupport.getDataFile(results.getLeft(), results.getRight());

		if (!sourceDataFile.exists()) {
			throw new TestExecutionError("Source file '" + sourceDataFile.getAbsolutePath() + "' not found.", HadoopConstants.ERR_SOURCE_FILE_NOT_FOUND);
		}

		MetaDataPackageContext dataPackageContext = parent.dataMetaContext().createPackageContext(results.getLeft(), MetaDataPackageContext.path_type.test_source);
		try {
			MetaDataArtifact dataPackageContextArtifact = dataPackageContext.createArtifact("file", sourceDataFile.getParentFile());
			dataPackageContextArtifact.createContent(sourceDataFile.getName()).referencedByCurrentTest("stage-data");
		} catch (IOException e) {
			throw new TestExecutionError("Error creating meta data context", e);
		}

		//if (request.getHdfsTarget().getFormat() != null && request.getHdfsTarget().getFormat() != HdfsTarget.Format.RAW) {
			String srcSchemaId = ObjectUtils.firstNotNull(request.getReferenceFileType(), request.getSourceFile());

			DataFileSchema srcDfs = null;
			try {
				FileRuntimeSupport.DataFileSchemaQueryResult dfsqr = fileRuntimeSupport.locateReferenceFileSchemaForCurrentTest(valueObject, ReferenceFileTypeRef.ref_type.local, srcSchemaId);

				if (dfsqr.getFmlSearchType() == FileRuntimeSupport.fmlSearchType.notFound) {
					// if there is a reference file type attribute, this is an instant error
					if (request.getReferenceFileType() != null) {
						throw new TestExecutionError(srcSchemaId, HadoopConstants.ERR_SOURCE_SCHEMA_NOT_FOUND);
					}

					sourceDF = dataFileManager.loadDataFile(sourceDataFile, null);
				} else {
					srcDfs = dfsqr.getDataFileSchema();

					// store in the variable context for verification
					variableContext.declareAndSetStringValue("hadoop-src-fml-search-type", dfsqr.getFmlSearchType().name());
					variableContext.declareAndSetStringValue("hadoop-src-fml-id", dfsqr.getDataFileSchema().getId());

					sourceDF = dataFileManager.loadDataFile(sourceDataFile, srcDfs);
				}
			} catch (TestExecutionError e) {
				throw e;
			} catch (RequestedFmlNotFoundException e) {
				// fall back to no schema.  This error will be thrown in the process target
				throw new TestExecutionError(srcSchemaId, HadoopConstants.ERR_SOURCE_SCHEMA_NOT_FOUND);
			}

		//} else {
			//sourceDF = dataFileManager.loadDataFile(sourceDataFile, dataFileManager.createDataFileSchema(sourceDataFile.getName()));
		//}

		// now iterate over the choice of target / targets
		HdfsTarget target = request.getHdfsTarget();

		if (target != null) {
			// process a stage request with target specified.
			processStageTarget(
					valueObject.query("hdfs-target"),
					variableContext,
					sourceDF,
					request.getHdfsFileName(),
					request.getHdfsPath(),
					target
			);
		}

		HdfsTargets tgts = request.getHdfsTargets();

		Map<String, HdfsTargetsProperty> addTgts = tgts != null ? tgts.getAdditionalProperties() : null;

		if (addTgts != null) {
			if (!addTgts.isEmpty()) {
				for (Map.Entry<String, HdfsTargetsProperty> tgt : tgts.getAdditionalProperties().entrySet()) {
					// convert this target object into the single one that we use, then pass one
					processStageTarget(
							valueObject.query("hdfs-targets." + tgt.getKey()),
							variableContext,
							sourceDF,
							request.getHdfsFileName(),
							request.getHdfsPath(),
							convertSingleTarget(tgt.getKey(), tgt.getValue())
					);
				}
			} else {
				throw new TestExecutionError("Hdfs-targets must not be empty if supplied", HadoopConstants.ERR_HDFS_TARGETS_EMPTY);
			}
		}

		// if target and targets are unspecified fail
		if (
				target == null && tgts == null
		) {
			processStageTarget(
					valueObject,
					variableContext,
					sourceDF,
					request.getHdfsFileName(),
					request.getHdfsPath(),
					new HdfsTarget()
			);
		}

		return action_code.handled;
	}

	private HdfsTarget convertSingleTarget(String hdfsId, HdfsTargetsProperty tgt) {
		HdfsTarget t = new HdfsTarget();

		HdfsTargetsProperty.Format tgtFormat = tgt.getFormat();

		if (tgtFormat != null) {
			t.setFormat(HdfsTarget.Format.valueOf(tgtFormat.name()));
		}

		t.setHdfsId(hdfsId);
		t.setMode(tgt.getMode());
		t.getModes().addAll(tgt.getModes());
		t.setName(tgt.getName());
		t.setPath(tgt.getPath());
		t.setReferenceFileType(tgt.getReferenceFileType());

		return t;
	}

	private void processStageTarget(
			ETLTestValueObject testMethod,
			VariableContext variableContext,
			DataFile sourceDataFile,
			String hdfsTargetFileName,
			String targetFilePath,
			HdfsTarget target
	) throws TestExecutionError {
		String targetReferenceFileType = ObjectUtils.firstNotNull(
				target.getReferenceFileType(),
				sourceDataFile.getDataFileSchema() != null ? sourceDataFile.getDataFileSchema().getId() : null
		);

		String targetPath = ObjectUtils.firstNotNull(target.getPath(), targetFilePath);
		String targetFileName = ObjectUtils.firstNotNull(
				target.getName(),
				hdfsTargetFileName,
				sourceDataFile.getFile().getName()
		);
		String targetHdfsId = ObjectUtils.firstNotNull(target.getHdfsId(), "hdfs");
		HdfsTarget.Format targetFormat = ObjectUtils.firstNotNull(target.getFormat(), HdfsTarget.Format.RAW);

		List<String> modes = new ArrayList<>(target.getModes());

		if (target.getMode() != null) {
			modes.add(target.getMode());
		}

		// validate the target path / name attributes at the onset.
		// Rules 	- RAW | csv will use an optional target name with a required path.
		//				- parquet | avro | orc | json use a required path only, name is disallowed.

		if (
				targetFormat != HdfsTarget.Format.RAW &&
						targetFormat != HdfsTarget.Format.CSV &&
						hdfsTargetFileName != null
		) {
			throw new TestExecutionError("Filename not allowed with target format not in (csv, raw)", HadoopConstants.ERR_TARGET_FILE_NAME_NOT_ALLOWED);
		}

		if (modes.isEmpty() && target.getMode() == null) {
			stageTarget(testMethod, new StageTarget(
							sourceDataFile,
							targetReferenceFileType,
							targetFormat,
							targetHdfsId,
							null,
							targetPath,
							targetFileName
					),
					variableContext
			);
		} else {
			for (String mode : modes) {
				stageTarget(testMethod, new StageTarget(
								sourceDataFile,
								targetReferenceFileType,
								targetFormat,
								targetHdfsId,
								mode,
								targetPath,
								targetFileName
						),
						variableContext
				);
			}
		}
	}

	@Override
	public action_code stageHadoop(StageHadoopRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return stage(StageUtil.convertHadoopRequest(request), testMethod, testOperation, valueObject, variableContext, executionContext);
	}

	class StageTarget {
		final DataFile sourceDataFile;
		final String targetReferenceFileType;
		final HdfsTarget.Format targetFormat;
		final String targetHdfsId;
		final String targetMode;
		final String targetPath;
		final String targetFileName;

		StageTarget(DataFile sourceDataFile, String targetReferenceFileType, HdfsTarget.Format targetFormat, String targetHdfsId, String targetMode, String targetPath, String targetFileName) {
			this.sourceDataFile = sourceDataFile;
			this.targetReferenceFileType = targetReferenceFileType;
			this.targetFormat = targetFormat;
			this.targetHdfsId = targetHdfsId;
			this.targetMode = targetMode;
			this.targetPath = targetPath;
			this.targetFileName = targetFileName;
		}
	}

	private action_code stageTarget(ETLTestValueObject testValueObject, StageTarget request, VariableContext context) throws TestExecutionError {
		// by default, use the schema which matches the source file name.  If the source reference file type
		// is specified, then use that instead.
		String tgtSchemaId = request.targetReferenceFileType;

		// determine the workspace to use
		String hdfsId = request.targetHdfsId + (request.targetMode == null ? "" : ("." + request.targetMode));

		HdfsTarget.Format format = request.targetFormat;

		File wroot = null;

		try {
			wroot = workspaceRoot(hdfsId);
		} catch (IllegalArgumentException exc) {
			throw new TestExecutionError("Hdfs root " + hdfsId + " not found", HadoopConstants.ERR_UNPREPARED_HDFS_ROOT, exc);
		}

		// build/create the target path
		String hdfsPath = makeWorkspacePath(request.targetPath);

		String hdfsName = request.targetFileName;
		FileBuilder fb = new FileBuilder(wroot).subdirs(hdfsPath).mkdirs();

		// straight up - if the format is raw - just move the file over with a binary copyToHadoop and we are done.
		if (format == HdfsTarget.Format.RAW) {
			File destFile = fb.name(hdfsName).file();

			runtimeSupport.getApplicationLog().info("Staging raw file in hdfs root '" + hdfsId + "', path '" + hdfsPath + "', filename '" + hdfsName + "', workspace path '" + destFile.getAbsolutePath() + "'");
			try {
				FileUtils.copyFile(request.sourceDataFile.getFile(), destFile);
				return action_code.handled;
			} catch (IOException exc) {
				throw new TestExecutionError("Copying the intermediate file", HadoopConstants.ERR_IO_ERR_WRITING_FILES, exc);
			}
		} else {
			// verify that a reference file type was found
			if (request.sourceDataFile.getDataFileSchema() == null) {
				throw new TestExecutionError("No source schema supplied", HadoopConstants.ERR_SOURCE_SCHEMA_NOT_FOUND);
			}
		}

		// load where the fml should be
		DataFileSchema tgtDfs = null;

		try {
			FileRuntimeSupport.DataFileSchemaQueryResult dfsqr = fileRuntimeSupport.locateReferenceFileSchemaForCurrentTest(testValueObject, ReferenceFileTypeRef.ref_type.local, tgtSchemaId);

			if (dfsqr.getFmlSearchType() == FileRuntimeSupport.fmlSearchType.notFound) {
				throw new TestExecutionError(tgtSchemaId, HadoopConstants.ERR_TARGET_SCHEMA_NOT_FOUND);
			}

			tgtDfs = dfsqr.getDataFileSchema();

			// store in the variable context for verification
			context.declareAndSetStringValue("hadoop-" + hdfsId + "-tgt-fml-search-type", dfsqr.getFmlSearchType().name());
			context.declareAndSetStringValue("hadoop-" + hdfsId + "-tgt-fml-id", dfsqr.getDataFileSchema().getId());
		} catch (TestExecutionError e) {
			throw e;
		} catch (RequestedFmlNotFoundException e) {
			throw new TestExecutionError(tgtSchemaId, HadoopConstants.ERR_TARGET_SCHEMA_NOT_FOUND);
		}

		DataFile sourceDataFileLocal = request.sourceDataFile;

		// copyToHadoop the input data file into the target schema using a temporary target to remove
		// comments and possibly change schema
		File intermediateTarget = runtimeSupport.createAnonymousTempFile();
		DataFile tgtDataFile = dataFileManager.loadDataFile(intermediateTarget, tgtDfs);

		try {
			runtimeSupport.getApplicationLog().info("Preparing file '" + request.sourceDataFile.getFile().getAbsolutePath() + "' for stage in location '" + intermediateTarget.getAbsolutePath() + "'");
			dataFileManager.copyDataFile(request.sourceDataFile, tgtDataFile, new CopyOptionsBuilder().writeHeader(false).missingColumnPolicy(DataFileManager.CopyOptions.missing_column_policy.use_default).options());
		} catch (DataFileMismatchException exc) {
			throw new TestExecutionError("Could not prepare source file for workspace", FileConstants.ERR_DATA_FILE_MISMATCH_EXCEPTION, exc);
		} catch (Throwable exc) {
			throw new TestExecutionError("Could not prepare source file for workspace", HadoopConstants.ERR_IO_ERR_WRITING_FILES, exc);
		}

		sourceDataFileLocal = tgtDataFile;

		File destFile = null;

		// for delimited use the target path + file just like raw
		if (format == HdfsTarget.Format.CSV) {
			destFile = fb.name(hdfsName).file();
		} else {
			destFile = fb.file();
		}

		runtimeSupport.getApplicationLog().info("Staging file in hdfs root '" + hdfsId + "', path '" + hdfsPath + "', filename '" + hdfsName + "', workspace path '" + destFile.getAbsolutePath() + "'");

		File schF = runtimeSupport.createAnonymousTempFile();

		try {
			FileUtils.fileWrite(schF, tgtDfs.toJsonString());
		} catch (IOException e) {
			throw new TestExecutionError("Crap happens", HadoopConstants.ERR_IO_ERR_WRITING_FILES, e);
		}

		copyFileToRoot(sourceDataFileLocal.getFile(), schF, destFile, format);

		return action_code.handled;
	}

	private String makeWorkspacePath(String hdfsPath) {
		if (hdfsPath.startsWith("/")) {
			return hdfsPath.substring(1);
		}

		return hdfsPath;
	}

	private void copyFileToRoot(File source, File sourceSchema, File target, HdfsTarget.Format format) throws TestExecutionError {
		try {
			if (format == HdfsTarget.Format.CSV) {
				// just do a file copy here.  File is either already raw or it has been converted to the target file type
				FileUtils.copyFile(source, target);
			} else {
				SparkProcessor sp = sparkProcessor();

				SparkCopyToHadoopRequest.Format dstFormat = null;

				switch (format) {
					case PARQUET:
						dstFormat = SparkCopyToHadoopRequest.Format.parquet;
						break;
					case AVRO:
						dstFormat = SparkCopyToHadoopRequest.Format.avro;
						break;
					case ORC:
						dstFormat = SparkCopyToHadoopRequest.Format.orc;
						break;
					case JSON:
						dstFormat = SparkCopyToHadoopRequest.Format.json;
						break;
					case RAW:
					case CSV:
						throw new TestExecutionError("", "");
				}

				sp.send(new SparkCopyToHadoopRequest(
						source,
						sourceSchema,
						"srcSch",
						target,
						dstFormat
				));
			}
		} catch (IOException e) {
			throw new TestExecutionError("Failure copying data", HadoopConstants.ERR_IO_ERR_WRITING_FILES, e);
		}
	}
}
