package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkGenerator;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.JavaForker;

import java.io.*;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.regex.Pattern;

public class SparkProcessor {
	private final RuntimeSupport runtimeSupport;
	private final PrintWriter processWriter;
	private final BufferedReader processReader;
	private final ProcessFacade process;

	private final BlockingQueue<SparkProcessorRequest> in_queue;
	private final BlockingQueue<SparkProcessorResponse> out_queue;

	private final Semaphore disposeLatch = new Semaphore(0);

	private final File processLogFile;

	public SparkProcessor(SparkGenerator sparkGenerator) throws IOException {
		runtimeSupport = null;
		processWriter = null;
		process = null;
		processReader = null;
		processLogFile = null;

		SparkMain sm = new SparkMain(sparkGenerator);
		in_queue = sm.in_queue();
		out_queue = sm.out_queue();

		send(new SparkReadyRequest());
	}

	public SparkProcessor(RuntimeSupport runtimeSupport, SparkGenerator sparkGenerator) throws IOException {
		this.runtimeSupport = runtimeSupport;

		JavaForker javaForker = new JavaForker(runtimeSupport);
		//javaForker.propogateSystemProperties();

		File anonymousTempFolder = runtimeSupport.createAnonymousTempFolder();
		// just as a precaution, fully delete the folder before using
		FileUtils.forceDelete(anonymousTempFolder);

		// now recreate.
		FileUtils.forceMkdir(anonymousTempFolder);

		javaForker.setWorkingDirectory(anonymousTempFolder);

		// override this one
		javaForker.addSystemProperty("user.dir", anonymousTempFolder.getAbsolutePath());

		javaForker.setMainClass(SparkMain.class);

		// adjust the classpath if running in maven
		String property = System.getProperty("etlunit.maven.plugin.classpath");
		if (property != null) {
			String[] pathElements = property.split(Pattern.quote(File.pathSeparator));

			for (String pathElement : pathElements) {
				javaForker.addClasspathEntry(new File(pathElement));
			}
		}

		processLogFile = new FileBuilder(
				runtimeSupport.getGeneratedSourceDirectory("process-log"))
				.subdir(runtimeSupport.getRunIdentifier()).name("SparkProcessor_out_" + runtimeSupport.getExecutorId() + ".txt").file();

		runtimeSupport.getApplicationLog().info("Spark processor logging to " + processLogFile);

		javaForker.setOutput(processLogFile);
		javaForker.addArgument(sparkGenerator.getClass().getName());
		javaForker.addArgument(runtimeSupport.getGeneratedSourceDirectory("spark-home").getAbsolutePath());

		process = javaForker.startProcess();
		process.waitForStreams();

		processWriter = new PrintWriter(process.getWriter());
		processReader = process.getReader();

		in_queue = new LinkedBlockingDeque<>();
		out_queue = new LinkedBlockingDeque<>();

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (disposeLatch.availablePermits() == 0) {
					try {
						SparkProcessorRequest request = in_queue.poll(200, TimeUnit.MILLISECONDS);

						if (request != null) {
							request.send(processWriter);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}).start();

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (disposeLatch.availablePermits() == 0) {
						SparkProcessorResponse spr = SparkProcessorResponse.read(processReader);

						out_queue.add(spr);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();

		in_queue.add(new SparkReadyRequest());
		SparkProcessorResponse res = null;

		try {
			res = out_queue.poll(300, TimeUnit.SECONDS);

			if (res == null) {
				process.kill();

				throw new IOException("Sub process terminated");
			} else if (res.type() != SparkProcessorResponse.type.done) {
				process.kill();

				throw new IOException("Sub process did not respond properly");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public synchronized SparkProcessorResponse send(SparkProcessorRequest rec) throws IOException {
		in_queue.add(rec);

		try {
			SparkProcessorResponse rsp = out_queue.poll(300, TimeUnit.SECONDS);

			if (rsp.type() != SparkProcessorResponse.type.done) {
				throw new IOException(rsp.message());
			}

			return rsp;
		} catch (InterruptedException exc) {
			throw new IOException(exc.toString());
		}
	}

	public File processLogFile() {
		return processLogFile;
	}

	/*Make this re-entrant for lazy reasons*/
	public synchronized void dispose() throws IOException {
		if (disposeLatch.availablePermits() == 0)
		{
			disposeLatch.release(1000);

			in_queue.add(new SparkExitRequest());
			try {
				out_queue.poll(300, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				throw new IOException(e);
			}

			if (process != null) {
				process.waitForCompletion();
				process.kill();
			}
		}
	}
}
