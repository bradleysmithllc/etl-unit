package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.JobConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkGenerator;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkJob;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManagerImpl;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileSchema;
import scala.collection.Iterator;

import java.io.*;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.concurrent.*;

public class SparkMain implements Runnable {
	private static final Map<DataType, DataFileSchema.Column.basic_type> BASIC_TYPES_MAP = new HashMap<>();
	private static final Map<DataType, String> JDBC_TYPES_MAP = new HashMap<>();
	private static final Map<String, DataType> TYPES_TO_JDBC_MAP = new HashMap<>();
	private static List<DataType> SPARK_TYPE_LIST = new ArrayList<>();

	static {
		// load up the spark type map
		mapType(DataTypes.BinaryType, "BINARY", DataFileSchema.Column.basic_type.string);
		mapType(DataTypes.LongType, "BIGINT", DataFileSchema.Column.basic_type.integer);
		mapType(DataTypes.FloatType, "FLOAT", DataFileSchema.Column.basic_type.numeric);
		mapType(DataTypes.DoubleType, "DOUBLE", DataFileSchema.Column.basic_type.numeric);
		mapType(DataTypes.BooleanType, "BOOLEAN", DataFileSchema.Column.basic_type.integer);
		mapType(DataTypes.IntegerType, "INTEGER", DataFileSchema.Column.basic_type.integer);
		mapType(DataTypes.TimestampType, "TIMESTAMP", DataFileSchema.Column.basic_type.string);
		mapType(DataTypes.DateType, "DATE", DataFileSchema.Column.basic_type.string);
		mapType(DataTypes.ByteType, "TINYINT", DataFileSchema.Column.basic_type.string);
		mapType(DataTypes.ShortType, "SMALLINT", DataFileSchema.Column.basic_type.integer);
		mapType(DataTypes.NullType, "VARCHAR", DataFileSchema.Column.basic_type.string);
		mapType(DataTypes.CalendarIntervalType, "VARCHAR", DataFileSchema.Column.basic_type.string);
		mapType(DataTypes.StringType, "VARCHAR", DataFileSchema.Column.basic_type.string);
	}

	private static void mapType(DataType binaryType, String jdbcType, DataFileSchema.Column.basic_type integer) {
		SPARK_TYPE_LIST.add(binaryType);
		BASIC_TYPES_MAP.put(binaryType, integer);
		JDBC_TYPES_MAP.put(binaryType, jdbcType);
		TYPES_TO_JDBC_MAP.put(jdbcType, binaryType);
	}

	private static PrintWriter systemOutWriter = new PrintWriter(new OutputStreamWriter(System.out));

	private final BlockingQueue<SparkProcessorRequest> in_queue;
	private final BlockingQueue<SparkProcessorResponse> out_queue;
	private final SparkGenerator sparkGenerator;

	private final CountDownLatch startWaitLatch = new CountDownLatch(1);
	private final Semaphore exitWaitSemaphore = new Semaphore(0);
	private DataFileManager dataFileManager;

	public SparkMain(SparkGenerator sparkGenerator)
	{
		this.sparkGenerator = sparkGenerator;
		in_queue =	new LinkedBlockingQueue<>();
		out_queue = new LinkedBlockingQueue<>();

		new Thread(this).start();

		try {
			startWaitLatch.await();
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}
	}

	public BlockingQueue<SparkProcessorRequest> in_queue() {
		return in_queue;
	}

	public BlockingQueue<SparkProcessorResponse> out_queue() {
		return out_queue;
	}

	public void run() {
		dataFileManager = null;
		File tmp = null;

		try {
			dataFileManager = new DataFileManagerImpl(File.createTempFile("pre", "post"));
			tmp = File.createTempFile("ARB", "DRB");
			tmp.delete();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		String currDir = System.getProperty("user.dir");

		System.setProperty("user.dir", tmp.getAbsolutePath());

		final SparkSession sparkSession = sparkGenerator.sparkSession();

		// enter request loop - release start semaphore.
		startWaitLatch.countDown();

		System.setProperty("user.dir", currDir);

		try {
			while (exitWaitSemaphore.availablePermits() == 0)
			{
				SparkProcessorRequest request = in_queue.poll(200, TimeUnit.MILLISECONDS);

				if (request == null)
				{
					continue;
				}

				//systemOutWriter.println("Request received: " + request.toString());

				try {
					if (request.type() == SparkProcessorRequest.type.ready) {
						respond(new SparkProcessorResponse());
					}
					else if (request.type() == SparkProcessorRequest.type.exit) {
						//systemOutWriter.println("EXIT received");

						sparkSession.close();

						// break out of the thread
						respond(new SparkProcessorResponse());
						return;
					}
					else if (request.type() == SparkProcessorRequest.type.copyToHadoop) {
						SparkCopyToHadoopRequest copyRequest = (SparkCopyToHadoopRequest) request;

						DataFileSchema dfs = dataFileManager.loadDataFileSchema(copyRequest.getSrcSchema(), copyRequest.getSrcSchemaId());

						StructType st = getStructType(dfs);

						Dataset<Row> ff =
								sparkSession.read()
										.option("delimiter", dfs.getColumnDelimiter())
										.option("nullValue", dfs.getNullToken())
										.option("mode", "FAILFAST")
										.schema(st)
										.format("csv")
										.load(copyRequest.getSrcDataFile().toURI().toString());

						ff.write().mode(SaveMode.Overwrite).format(copyRequest.getFormat().sparkFormat()).save(copyRequest.getDstFile().toURI().toString());

						// signal success
						respond(new SparkProcessorResponse());
					} else if (request.type() == SparkProcessorRequest.type.copyFromHadoop) {
						SparkCopyFromHadoopRequest copyRequest = (SparkCopyFromHadoopRequest) request;

						DataFrameReader dfr = sparkSession.read().format(copyRequest.getFormat().sparkFormat());

						System.err.println("Applying dataset options: " +copyRequest.datasetOptions().size());

						// apply all the options provided
						for (Map.Entry<String, String> entry : copyRequest.datasetOptions().entrySet())
						{
							System.err.println("Applying dataset option: " + entry.getKey() + " -> " + entry.getValue());
							dfr = dfr.option(entry.getKey(), entry.getValue());
						}

						DataFileSchema dfs;

						if (copyRequest.hasSchema()) {
							// in the case that there is no schema, infer schema from the data set and pass it on
							dfs = dataFileManager.loadDataFileSchema(copyRequest.getDstSchema(), copyRequest.getDstSchemaId());

							// sanity check.  Cannot write to delimited files which have a line terminator that isn't
							// linefeed.
							if (!dfs.getRowDelimiter().equals("\n")) {
								throw new IllegalArgumentException("Only newline can be used as the line terminator in csv files.");
							}

							StructType st = getStructType(dfs);

							dfr = dfr.schema(st);
						}
						else {
							dfs = dataFileManager.createDataFileSchema("synth-spark-schema");
							dfs.setFormatType(DataFileSchema.format_type.delimited);
							// use tab delimitation by default.
							dfs.setColumnDelimiter("\t");
							dfs.setNullToken("null");
						}

						Dataset<Row> df = dfr.load(copyRequest.getSrcDir().toURI().toString());

						System.out.println("Tabbing: '" + dfs.getColumnDelimiter() + "'");

						df.write().format("csv")
							.option("delimiter", dfs.getColumnDelimiter())
							.option("nullValue", dfs.getNullToken())
							// try to use a predictable timezone format
							.option("dateFormat", "yyyy-MM-dd")
							.option("timestampFormat", "yyyy-MM-dd HH:mm:ss.SSS")
							.mode(SaveMode.Overwrite).save(copyRequest.getDstDataDir().toURI().toString());

						if (!copyRequest.hasSchema()) {
							StructType schema = df.schema();

							convertToFML(schema, dfs);
						}

						// signal success
						respond(new SparkProcessorResponse(SparkProcessorResponse.type.done, dfs.toJsonString()));
					} else if (request.type() == SparkProcessorRequest.type.execute) {
						final SparkExecuteRequest er = (SparkExecuteRequest) request;

						SparkJob sj = er.job();

						sj.execute(new JobConfiguration() {
							@Override
							public Set<String> roots() {
								return er.roots().keySet();
							}

							@Override
							public File fileSystemRoot(String id) {
								return er.roots().get(id);
							}

							@Override
							public SparkSession sparkSession() {
								return sparkSession;
							}
						});

						// signal success
						respond(new SparkProcessorResponse());
					} else {
						// signal Failure
						respond(new SparkProcessorResponse("DoneWithFailure: Unhandled request type " + request.type()));
					}
				} catch (Throwable exc) {
					exc.printStackTrace();

					// signal Failure
					respond(new SparkProcessorResponse("DoneWithFailure: " + exc.toString()));
				}
			}
		} catch (InterruptedException exc) {
			exc.printStackTrace();

			// signal Failure
			respond(new SparkProcessorResponse("DoneWithFailure: " + exc.toString()));
		}
		finally
		{
			exitWaitSemaphore.release(1000);
		}
	}

	private void respond(SparkProcessorResponse sparkProcessorResponse) {
		//systemOutWriter.println("Sending response {" + sparkProcessorResponse + "}");
		out_queue.add(sparkProcessorResponse);
	}

	public static void convertToFML(StructType schema, DataFileSchema dfs) {
		Iterator<StructField> it = schema.iterator();

		while (it.hasNext())
		{
			StructField fld = it.next();

			DataType type = fld.dataType();
			String name = fld.name();

			DataFileSchema.Column column = dfs.createColumn(name);

			// default everything to string
			column.setTypeAnnotation(DataTypes.StringType.typeName());
			column.setType("VARCHAR");
			column.setBasicType(DataFileSchema.Column.basic_type.string);

			// if we find a better match, use it.
			for (DataType spark_type : SPARK_TYPE_LIST) {
				if (spark_type.sameType(type))
				{
					column.setTypeAnnotation(spark_type.typeName());
					column.setType(JDBC_TYPES_MAP.get(spark_type));
					column.setBasicType(BASIC_TYPES_MAP.get(spark_type));
				}
			}

			dfs.addColumn(column);
		}
	}

	public static void main(String[] argv) throws Exception {
		Class<?> loadClass = Thread.currentThread().getContextClassLoader().loadClass(argv[0]);
		Constructor<?> constructor = loadClass.getConstructor(RuntimeSupport.class);

		System.out.println(System.getProperties());

		SparkGenerator sparkGenerator = (SparkGenerator) constructor.newInstance(new BasicRuntimeSupport(new File(argv[1])));

		final SparkMain spm = new SparkMain(sparkGenerator);

		try
		{
			spm.startWaitLatch.await(120, TimeUnit.SECONDS);
		}
		catch(InterruptedException exc)
		{
			systemOutWriter.println("Spark thread did not start within 120 seconds.");
			System.exit(-1);
		}

		systemOutWriter.println("Starting streams . . .");

		// start threads to read requests from the stdin and write responses to stdout
		new Thread(new Runnable() {
			@Override
			public void run() {
				BufferedReader breader = new BufferedReader(new InputStreamReader(System.in));

				try {
					while (true) {
						SparkProcessorRequest request = RequestLoader.receive(breader);
						spm.in_queue.add(request);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (spm.exitWaitSemaphore.availablePermits() == 0) {
						SparkProcessorResponse response = spm.out_queue.poll(200, TimeUnit.MILLISECONDS);

						if (response != null)
						{
							response.send(systemOutWriter);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(-1);
				}
			}
		}).start();

		//systemOutWriter.println("Waiting for processor to exit");

		// wait for shutdown and exit cleanly
		spm.exitWaitSemaphore.acquire();

		//systemOutWriter.println("All done");
		System.exit(0);
	}

	public static StructType getStructType(DataFileSchema dfs) {
		List<StructField> stf = new ArrayList<StructField>();

		for (DataFileSchema.Column col : dfs.getLogicalColumns()) {
			DataType dt = null;

			// try the type annotation first of all
			if (col.hasTypeAnnotation())
			{
				for (DataType type : SPARK_TYPE_LIST)
				{
					if (type.typeName().equals(col.getTypeAnnotation()))
					{
						dt = type;
					}
				}
			}

			// annotation didn't work, try the JDBC type
			if (dt == null && col.hasType())
			{
				dt = TYPES_TO_JDBC_MAP.get(col.getType());
			}

			// type annotation and jdbc type lookups failed, use the basic type.  This is not-nullable so one of the
			// three cases will match.
			if (dt == null)
			{
				switch (col.getBasicType()) {
					case string:
						dt = DataTypes.StringType;
						break;
					case numeric:
						dt = DataTypes.DoubleType;
						break;
					case integer:
						dt = DataTypes.IntegerType;
						break;
				}
			}

			stf.add(new StructField(col.getId(), dt, true, Metadata.empty()));
		}

		return new StructType(stf.toArray(new StructField[stf.size()]));
	}
}
