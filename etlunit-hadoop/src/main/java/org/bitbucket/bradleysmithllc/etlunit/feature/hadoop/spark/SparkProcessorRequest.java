package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintWriter;

public abstract class SparkProcessorRequest {
	private static final String TAG_BEGIN = "<\t<\t[[<";
	private static final String TAG_END = ">]]\t>\t>";

	public enum type {
		copyToHadoop,
		copyFromHadoop,
		execute,
		exit,
		ready
	}

	public abstract type type();

	public final void send(PrintWriter printWriter)
	{
		// send a tag to identify this as a real request
		// tag all communication to try to make it visible
		sendTagged(printWriter, getClass().getSimpleName());

		sendBody(printWriter);

		printWriter.flush();
	}

	static String readNextTagged(BufferedReader reader) throws IOException {
		String line = null;

		while ((line = reader.readLine()) != null)
		{
			if (line.startsWith(TAG_BEGIN) && line.endsWith(TAG_END))
			{
				String substring = line.substring(TAG_BEGIN.length(), line.length() - TAG_END.length());
				return substring;
			}
		}

		throw new EOFException();
	}

	public static void sendTagged(PrintWriter printWriter, String msg) {
		printWriter.println(tag(msg));
	}

	static String tag(String msg) {
		return TAG_BEGIN + msg + TAG_END;
	}

	void sendBody(PrintWriter printWriter) {
	}
}
