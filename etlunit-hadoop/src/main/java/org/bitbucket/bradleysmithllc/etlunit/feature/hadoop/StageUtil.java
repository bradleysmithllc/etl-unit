package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.StageRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage_hadoop.HdfsTarget;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage_hadoop.HdfsTargets;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage_hadoop.HdfsTargetsProperty;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage_hadoop.StageHadoopRequest;

import java.util.Map;

public class StageUtil {
	public static StageRequest convertHadoopRequest(StageHadoopRequest request) {
		StageRequest sr = new StageRequest();

		// copyToHadoop high-level attributes
		sr.setSourceFile(request.getSourceFile());
		sr.setHdfsFileName(request.getHdfsFileName());
		sr.setHdfsPath(request.getHdfsPath());
		sr.setReferenceFileType(request.getReferenceFileType());

		// copyToHadoop the top-level target object.  This is where naming gets hairy.
		HdfsTarget hdfsTarget = request.getHdfsTarget();

		if (hdfsTarget != null)
		{
			org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.HdfsTarget newHdfsTarget = new org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.HdfsTarget();

			newHdfsTarget.setHdfsId(hdfsTarget.getHdfsId());
			newHdfsTarget.setMode(hdfsTarget.getMode());
			newHdfsTarget.getModes().addAll(hdfsTarget.getModes());
			newHdfsTarget.setName(hdfsTarget.getName());
			newHdfsTarget.setPath(hdfsTarget.getPath());
			newHdfsTarget.setReferenceFileType(hdfsTarget.getReferenceFileType());

			HdfsTarget.Format format = hdfsTarget.getFormat();

			if (format != null)
			{
				newHdfsTarget.setFormat(org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.HdfsTarget.Format.fromValue(format.value()));
			}

			sr.setHdfsTarget(newHdfsTarget);
		}

		HdfsTargets targets = request.getHdfsTargets();

		if (targets != null)
		{
			org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.HdfsTargets newHdfsTargets = new org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.HdfsTargets();

			for (Map.Entry<String, HdfsTargetsProperty> targetEntry : targets.getAdditionalProperties().entrySet())
			{
				String key = targetEntry.getKey();
				HdfsTargetsProperty value = targetEntry.getValue();

				org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.HdfsTargetsProperty newTargetProperty = new org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.HdfsTargetsProperty();

				// copyToHadoop properties over
				newTargetProperty.setHdfsId(value.getHdfsId());
				newTargetProperty.setMode(value.getMode());
				newTargetProperty.setName(value.getName());
				newTargetProperty.setPath(value.getPath());
				newTargetProperty.setReferenceFileType(value.getReferenceFileType());
				newTargetProperty.getModes().addAll(value.getModes());

				HdfsTargetsProperty.Format format = value.getFormat();

				if (format != null)
				{
					newTargetProperty.setFormat(
							org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.json.hadoop.stage.HdfsTargetsProperty.Format.fromValue(
									value.getFormat().value()
							)
					);
				}

				newHdfsTargets.getAdditionalProperties().put(key, newTargetProperty);
			}

			sr.setHdfsTargets(newHdfsTargets);
		}

		return sr;
	}
}
