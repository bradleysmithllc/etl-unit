package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.SparkGenerator;

import java.io.File;
import java.io.IOException;

public class ExternalizableSparkGenerator implements SparkGenerator {
	private final File workingDirectory;

	public ExternalizableSparkGenerator(RuntimeSupport runtimeSupport) {
		this(runtimeSupport.createAnonymousTempFolder());
	}

	public ExternalizableSparkGenerator(File working) {
		workingDirectory = working;
	}

	@Override
	public SparkSession sparkSession() {
		if (workingDirectory.exists() && !workingDirectory.isDirectory())
		{
			try {
				FileUtils.forceDelete(workingDirectory);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (!workingDirectory.exists())
		{
			try {
				FileUtils.forceMkdir(workingDirectory);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		SparkConf sparkConf = makeSparkConf();

		sparkConf.set("spark.master", "local");
		sparkConf.set("spark.ui.enabled", "false");
		sparkConf.set("spark.driver.host", "localhost");

		sparkConf.set("spark.sql.warehouse.dir", workingDirectory.getAbsolutePath());
		sparkConf.set("spark.derby.system.home", workingDirectory.getAbsolutePath());
		sparkConf.set("derby.system.home", workingDirectory.getAbsolutePath());

		// provide a home for spark.
		sparkConf.setSparkHome(workingDirectory.getAbsolutePath());

		return SparkSession.builder()
				.config(sparkConf)
				.enableHiveSupport()
				.getOrCreate();
	}

	protected SparkConf makeSparkConf()
	{
		return new SparkConf();
	}
}
