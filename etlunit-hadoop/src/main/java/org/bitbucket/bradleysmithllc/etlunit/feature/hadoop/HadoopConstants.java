package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-file
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface HadoopConstants
{
	String ERR_SOURCE_SCHEMA_NOT_FOUND = "ERR_SOURCE_SCHEMA_NOT_FOUND";
	String ERR_HDFS_ROOTS_NOT_UNIQUE = "ERR_HDFS_ROOTS_NOT_UNIQUE";
	String ERR_IO_ERR_WRITING_FILES = "ERR_IO_ERR_WRITING_FILES";
	String ERR_UNPREPARED_HDFS_ROOT = "ERR_UNPREPARED_HDFS_ROOT";
	String ERR_LOADING_SPARK_PROXY = "ERR_LOADING_SPARK_PROXY";
	String ERR_UNSPECIFIED_SPARK_PROXY = "ERR_UNSPECIFIED_SPARK_PROXY";
	String ERR_MULTIPLE_METHOD_HADOOP_ANNOTATIONS = "ERR_MULTIPLE_METHOD_HADOOP_ANNOTATIONS";
	String ERR_MULTIPLE_CLASS_HADOOP_ANNOTATIONS = "ERR_MULTIPLE_CLASS_HADOOP_ANNOTATIONS";
	String ERR_MISSING_SPARK_PROXY = "ERR_MISSING_SPARK_PROXY";
	String ERR_INVALID_SPARK_PROXY_ID = "ERR_INVALID_SPARK_PROXY_ID";
	String ERR_RUNNING_SPARK_JOB = "ERR_RUNNING_SPARK_JOB";
	String ERR_INVALID_SPARK_JOB = "ERR_INVALID_SPARK_JOB";
	String ERR_MISSING_SPARK_JOB = "ERR_MISSING_SPARK_JOB";
	String ERR_START_SPARK_PROCESSOR = "ERR_START_SPARK_PROCESSOR";
	String ERR_TARGET_SCHEMA_NOT_FOUND = "ERR_TARGET_SCHEMA_NOT_FOUND";
	String ERR_SPARK_PROXY_DOES_NOT_IMPLEMENT_INTERFACE = "ERR_SPARK_PROXY_DOES_NOT_IMPLEMENT_INTERFACE";
	String ERR_SOURCE_FILE_NOT_FOUND = "ERR_SOURCE_FILE_NOT_FOUND";
	String ERR_IN_SPARK_PROXY_GET_JOB = "ERR_IN_SPARK_PROXY_GET_JOB";
	String ERR_IN_SPARK_JOB_MISSING_REQUIRED_FS_ROOTS = "ERR_IN_SPARK_JOB_MISSING_REQUIRED_FS_ROOTS";
	String ERR_VERIFY_SPARK_JOB = "ERR_VERIFY_SPARK_JOB";
	String ERR_PREPARE_HDFS_WORKSPACES = "ERR_PREPARE_HDFS_WORKSPACES";
	String ERR_LOADING_SPARK_GENERATOR = "ERR_LOADING_SPARK_GENERATOR";
	String ERR_SPARK_GENERATOR_DOES_NOT_IMPLEMENT_INTERFACE = "ERR_SPARK_GENERATOR_DOES_NOT_IMPLEMENT_INTERFACE";
	String ERR_TARGET_FILE_NAME_NOT_ALLOWED = "ERR_TARGET_FILE_NAME_NOT_ALLOWED";
	String ERR_HDFS_TARGET_NOT_SPECIFIED = "ERR_HDFS_TARGET_NOT_SPECIFIED";
	String ERR_HDFS_TARGETS_EMPTY = "ERR_HDFS_TARGET_NOT_SPECIFIED";
	String ERR_ASSERT_MODE_FORMAT_MISMATCH = "ERR_ASSERT_MODE_FORMAT_MISMATCH";
	String ERR_ASSERT_EQUALS_REQUIRES_FILE_TYPE = "ERR_ASSERT_EQUALS_REQUIRES_FILE_TYPE";
	String ERR_HDFS_SOURCE_REQUIRES_FORMAT = "ERR_HDFS_SOURCE_REQUIRES_FORMAT";
}
