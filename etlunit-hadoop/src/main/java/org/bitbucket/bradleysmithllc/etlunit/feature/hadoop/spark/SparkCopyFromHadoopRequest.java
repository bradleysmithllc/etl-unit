package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class SparkCopyFromHadoopRequest extends SparkProcessorRequest {
	private final File srcDir;

	private final File dstDataDir;
	private final File dstSchema;
	private final String dstSchemaId;
	private final SparkCopyToHadoopRequest.Format format;
	private final Map<String, String> datasetOptions = new HashMap<>();

	public SparkCopyFromHadoopRequest(
		File srcDir,
		File dstDataDir,
		File dstSchema,
		String dstSchemaId,
		SparkCopyToHadoopRequest.Format format
	) {
		this.srcDir = srcDir;
		this.dstDataDir = dstDataDir;
		this.dstSchema = dstSchema;
		this.dstSchemaId = dstSchemaId;
		this.format = format;
	}

	public void addDatasetOption(String key, String value) {
		datasetOptions.put(key, value);
	}

	public File getSrcDir() {
		return srcDir;
	}

	public File getDstDataDir() {
		return dstDataDir;
	}

	public boolean hasSchema() {
		return dstSchema != null;
	}

	public Map<String, String> datasetOptions() {
		return datasetOptions;
	}

	public File getDstSchema() {
		return dstSchema;
	}

	public String getDstSchemaId() {
		return dstSchemaId;
	}

	public SparkCopyToHadoopRequest.Format getFormat() {
		return format;
	}

	@Override
	public type type() {
		return type.copyFromHadoop;
	}

	@Override
	void sendBody(PrintWriter printWriter) {
		sendTagged(printWriter, getSrcDir().getAbsolutePath());
		sendTagged(printWriter, getDstDataDir().getAbsolutePath());

		if (hasSchema())
		{
			sendTagged(printWriter, getDstSchema().getAbsolutePath());
			sendTagged(printWriter, getDstSchemaId());
		}
		else
		{
			sendTagged(printWriter, "<<null>>");
			sendTagged(printWriter, "<<null>>");
		}

		sendTagged(printWriter, getFormat().name());

		// write out the dataset options
		for (Map.Entry<String, String> entry : datasetOptions.entrySet())
		{
			sendTagged(printWriter, entry.getKey());
			sendTagged(printWriter, entry.getValue());
		}

		sendTagged(printWriter, "<<eom>>");
	}

	public static SparkProcessorRequest read(BufferedReader reader) throws IOException {
		File srcDirFile = new File(readNextTagged(reader));
		File dstDataDirFile = new File(readNextTagged(reader));

		String schemaPath = readNextTagged(reader);
		String schema = readNextTagged(reader);

		File schemaFile = null;
		String schemaId = null;

		if (!schemaPath.equals("<<null>>"))
		{
			schemaFile = new File(schemaPath);
			schemaId = schema;
		}

		SparkCopyFromHadoopRequest cfhr =  new SparkCopyFromHadoopRequest(
				srcDirFile,
				dstDataDirFile,
				schemaFile,
				schemaId,
				SparkCopyToHadoopRequest.Format.valueOf(readNextTagged(reader))
		);

		// look up the dataset options
		String nextKey = readNextTagged(reader);

		while (!nextKey.equals("<<eom>>"))
		{
			cfhr.addDatasetOption(nextKey, readNextTagged(reader));

			nextKey = readNextTagged(reader);
		}

		return cfhr;
	}
}
