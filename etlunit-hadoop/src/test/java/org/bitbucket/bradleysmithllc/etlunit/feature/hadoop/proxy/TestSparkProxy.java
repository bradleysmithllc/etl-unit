package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.Collections;
import java.util.List;

public class TestSparkProxy implements SparkProxy {
	@Override
	public SparkJob sparkJob(final String id) {
		if (id.startsWith("exc")) {
			throw new RuntimeException("Asked to fail - " + id);
		}
		else if (id.startsWith("test")) {
			return new SparkJob() {
				@Override
				public String id() {
					if (id.contains("id()"))
					{
						throw new RuntimeException("Asked Job.id() to fail - " + id);
					}

					return id;
				}

				@Override
				public List<String> requiredFilesystems() {
					if (id.contains("requiredFilesystems()"))
					{
						throw new RuntimeException("Asked Job.requiredFilesystems() to fail - " + id);
					}

					return Collections.emptyList();
				}

				@Override
				public void execute(JobConfiguration jc) {
					if (id.contains("execute()"))
					{
						throw new RuntimeException("Asked Job.execute() to fail - " + id);
					}
				}
			};
		}
		else if (id.startsWith("good")) {
			return new SparkJob() {
				@Override
				public String id() {
					return id;
				}

				@Override
				public List<String> requiredFilesystems() {
					return Collections.emptyList();
				}

				@Override
				public void execute(JobConfiguration jc) {
					for (String root : jc.roots())
					{
						File fr = jc.fileSystemRoot(root);

						if (!fr.exists())
						{
							throw new IllegalStateException(root);
						}
					}
				}
			};
		} else if (id.startsWith("inv")) {
			return null;
		} else if (id.startsWith("fail")) {
			return new SparkJob() {
				@Override
				public String id() {
					return id;
				}

				@Override
				public List<String> requiredFilesystems() {
					return Collections.emptyList();
				}

				@Override
				public void execute(JobConfiguration jc) {
					throw new RuntimeException("Catch me if you dare . . .");
				}
			};
		} else throw new RuntimeException(id);
	}
}
