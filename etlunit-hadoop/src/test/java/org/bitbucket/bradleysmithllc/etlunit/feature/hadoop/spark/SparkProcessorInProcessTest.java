package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.NoisySparkProxy;
import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.TestSparkProxy;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;

public class SparkProcessorInProcessTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	private SparkProcessor sp;
	private File file;
	private File dir;

	@Before
	public void derby() throws IOException {
		System.setProperty("derby.system.home", temporaryFolder.newFolder().getAbsolutePath());

		if (!inProcess()) {
			Log l = new PrintWriterLog();

			BasicRuntimeSupport runtimeSupport = new BasicRuntimeSupport();
			runtimeSupport.setApplicationLogger(l);
			runtimeSupport.setUserLogger(l);
			sp = new SparkProcessor(runtimeSupport, new ExternalizableSparkGenerator(runtimeSupport));
		} else {
			sp = new SparkProcessor(new ExternalizableSparkGenerator(temporaryFolder.newFolder()));
		}

		file = temporaryFolder.newFile();
		dir = temporaryFolder.newFolder();

		FileUtils.write(file, "1\t2\t3");
	}

	boolean inProcess() {
		return true;
	}

	@Test
	public void life() throws Exception {
		TestSparkProxy tsp = new TestSparkProxy();

		for (int i = 0; i < 10; i++) {
			for (SparkCopyToHadoopRequest.Format fmt : SparkCopyToHadoopRequest.Format.values()) {
				SparkCopyToHadoopRequest cp = new SparkCopyToHadoopRequest(
						file,
						getResourceFile("/integration_tests/HadoopFeatureTest/feature_src/reference/file/fml/doobie-two.fml"),
						"doobie-two",
						new File(dir, fmt.name() + i),
						fmt
				);

				sp.send(cp);
			}

			sp.send(new SparkExecuteRequest(tsp, "good-job", Collections.EMPTY_MAP));
		}

		sp.dispose();
	}

	@Test
	public void copyDataTypes() throws Exception {
		// update this schema to include all jdbc / spark types//...
		File fml = getResourceFile("/data_types/schema.fml");
		File smpl = getResourceFile("/data_types/sample");

		for (SparkCopyToHadoopRequest.Format fmt : Arrays.asList(
				SparkCopyToHadoopRequest.Format.csv,
				SparkCopyToHadoopRequest.Format.parquet,
				SparkCopyToHadoopRequest.Format.orc,
				SparkCopyToHadoopRequest.Format.json
			)
		) {
			File dstDir = new File(dir, fmt.name());
			SparkCopyToHadoopRequest cp = new SparkCopyToHadoopRequest(
					smpl,
					fml,
					"data_types",
					dstDir,
					fmt
			);

			sp.send(cp);

			SparkCopyFromHadoopRequest frm = new SparkCopyFromHadoopRequest(
					dstDir,
					temporaryFolder.newFolder(),
					fml,
					"data_types",
					fmt
			);

			SparkProcessorResponse resp = sp.send(frm);

			System.out.println(resp.message());
		}

		sp.dispose();
	}

	private File getResourceFile(String s) throws IOException {
		File fml = temporaryFolder.newFile();

		URL url = getClass().getResource(s);

		FileUtils.copyURLToFile(url, fml);

		return fml;
	}

	@Test
	public void chattyProxy() throws Exception {
		NoisySparkProxy tsp = new NoisySparkProxy();

		for (int i = 0; i < 10; i++) {
			sp.send(new SparkExecuteRequest(tsp, "good-job", Collections.EMPTY_MAP));
		}

		sp.dispose();
	}

	@Test
	public void justExit() throws Exception {
		sp.dispose();
	}
}
