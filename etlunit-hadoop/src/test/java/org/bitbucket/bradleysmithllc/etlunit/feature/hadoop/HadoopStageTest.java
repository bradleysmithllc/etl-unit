package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.TestWarning;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.workspace.Workspace;
import org.bitbucket.bradleysmithllc.etlunit.feature.workspace.WorkspaceRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.FileFilter;
import java.util.List;

public class HadoopStageTest extends BaseIntegrationTest {

	private WorkspaceRuntimeSupport workspaceRuntimeSupport;

	@Inject
	public void receiveWorkspaceRuntimeSupport(WorkspaceRuntimeSupport wrs) {
		workspaceRuntimeSupport = wrs;
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed) throws TestAssertionFailure, TestExecutionError, TestWarning {
		List<String> descriptions = HadoopProxyTest.grabDescriptions(mt);

		//grab the workspace using the hdfs id.
		Workspace workspace = workspaceRuntimeSupport.currentWorkspace();

		for (String description : descriptions) {
			// the description tells us what to verify
			String[] parts = description.split("\\|\\|");

			if (parts.length == 2) {
				String hdfsId = parts[0];
				String hdfsPath = parts[1];

				// build the path
				File partition = workspace.openPartition(hdfsId);

				File file = new File(partition, hdfsPath);
				if (!file.exists()) {
					throw new TestAssertionFailure("Path does not exist {" + description + "}", "ASSERT_PATH_ERROR");
				}
			} else if (parts.length == 3) {
				final String format = parts[0];
				String partition = parts[1];
				final String path = parts[2];

				// grab the partition
				File pd = workspace.openPartition(partition);

				// append the path
				FileBuilder fbuilder = new FileBuilder(pd).subdirs(path);

				File dir = fbuilder.file();

				// verify the _SUCCESS flag first
				File success = fbuilder.name("_SUCCESS").file();

				if (!success.exists())
				{
					throw new TestAssertionFailure(partition + ":" + path, "FAILURE_SUCCESS_MARKER_NOT_FOUND");
				}

				// verify that there is a least one 'part-&.<format>' file
				File [] lists = dir.listFiles(new FileFilter(){
					@Override
					public boolean accept(File pathname) {
						String name = pathname.getName();

						return name.startsWith("part-") && name.endsWith("." + format);
					}
				});

				if (lists.length == 0)
				{
					throw new TestAssertionFailure(partition + ":" + path, "FAILURE_PART_FILES_NOT_FOUND");
				}
			}
			else {
				throw new TestExecutionError("Bad spec", "ERR_BAD_TEST_SPEC");
			}
		}
	}

	@Test
	public void run() {
		startTest();
	}
}
