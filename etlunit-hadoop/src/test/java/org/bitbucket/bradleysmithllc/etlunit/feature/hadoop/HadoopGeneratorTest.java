package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseMultipleIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestAnnotation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class HadoopGeneratorTest extends BaseMultipleIntegrationTest
{
	@Test
	public void generatorWrongInterface()
	{
		startTest();
	}

	@Test
	public void generatorInvalidClass()
	{
		startTest();
	}

	@Override
	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext variableContext, int operationNumber) {
		matchDescription(op, variableContext, "spark-generator");
	}

	protected static void matchDescription(ETLTestOperation op, VariableContext variableContext, String attr) {
		// grab the documentation annotation off of the root and verify the value
		String desc = grabDescription(op);

		if (desc != null)
		{
			String valueAsString = variableContext.getValue(attr).getValueAsString();
			Assert.assertEquals(desc, valueAsString);
		}
	}

	static String grabDescription(ETLTestOperation op) {
		List<String> l = grabDescriptions(op.getTestMethod());

		if (l.size() > 0)
		{
			return l.get(0);
		}

		return null;
	}

	static List<String> grabDescriptions(ETLTestMethod op) {
		// grab the documentation annotation off of the root and verify the value
		List<ETLTestAnnotation> annotations = op.getAnnotations("@Description");

		List<String> descs = new ArrayList<>();

		if (annotations.size() > 0)
		{
			for (ETLTestAnnotation dann : annotations)
			{
				descs.add(dann.getValue().query("description").getValueAsString());
			}
		}

		return descs;
	}
}
