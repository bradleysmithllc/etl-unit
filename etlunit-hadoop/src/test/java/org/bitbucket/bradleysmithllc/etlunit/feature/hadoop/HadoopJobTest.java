package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseMultipleIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Test;

public class HadoopJobTest extends BaseMultipleIntegrationTest
{
	@Test
	public void rootValidation()
	{
		startTest();
	}

	@Test
	public void defaultJobInvalid()
	{
		startTest();
	}

	/*
	* Test the rules for finding the job name:  operation, method, class, default */
	@Test
	public void jobResolutionPath()
	{
		startTest();
	}

	@Override
	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext variableContext, int operationNumber) {
		HadoopProxyTest.matchDescription(op, variableContext, "spark-job");
	}
}
