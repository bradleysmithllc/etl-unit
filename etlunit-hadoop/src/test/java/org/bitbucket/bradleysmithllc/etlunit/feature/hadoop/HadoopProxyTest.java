package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseMultipleIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestAnnotation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class HadoopProxyTest extends BaseMultipleIntegrationTest
{
	@Test
	public void run()
	{
		checkProxy = true;
		startTest();
	}

	private boolean checkProxy = false;

	@Test
	public void defaultProxyInvalid()
	{
		startTest();
	}

	@Test
	public void noProxy()
	{
		startTest();
	}

	@Test
	public void proxyWrongInterface()
	{
		startTest();
	}

	/*
	* Test the rules for finding the proxy class:  operation, method, class, default */
	@Test
	public void proxyResolutionPath()
	{
		startTest();
	}

	@Override
	protected void assertVariableContextOperation(ETLTestOperation op, ETLTestValueObject obj, VariableContext variableContext, int operationNumber) {
		if (checkProxy) {matchDescription(op, variableContext, "spark-proxy");}
	}

	protected static void matchDescription(ETLTestOperation op, VariableContext variableContext, String attr) {
		// grab the documentation annotation off of the root and verify the value
		String desc = grabDescription(op);

		if (desc != null)
		{
			String valueAsString = variableContext.getValue(attr).getValueAsString();
			Assert.assertEquals(desc, valueAsString);
		}
	}

	static String grabDescription(ETLTestOperation op) {
		List<String> l = grabDescriptions(op.getTestMethod());

		if (l.size() > 0)
		{
			return l.get(0);
		}

		return null;
	}

	static List<String> grabDescriptions(ETLTestMethod op) {
		// grab the documentation annotation off of the root and verify the value
		List<ETLTestAnnotation> annotations = op.getAnnotations("@Description");

		List<String> descs = new ArrayList<>();

		if (annotations.size() > 0)
		{
			for (ETLTestAnnotation dann : annotations)
			{
				ETLTestValueObject qu = dann.getValue().query("description");
				if (qu.getValueType() == ETLTestValueObject.value_type.quoted_string)
				{
					descs.add(qu.getValueAsString());
				}
				else
				{
					descs.addAll(qu.getValueAsListOfStrings());
				}
			}
		}

		return descs;
	}
}
