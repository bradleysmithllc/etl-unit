package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.spark.sql.DataFrameWriter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class CopySparkProxy implements SparkProxy {
	@Override
	public SparkJob sparkJob(final String id) {
		return new SparkJob() {
			@Override
			public String id() {
				return id;
			}

			@Override
			public List<String> requiredFilesystems() {
				return Arrays.asList("hdfs");
			}

			@Override
			public void execute(JobConfiguration jc) {
				// copyToHadoop from /usr/data/input to /usr/data/output using the id as the format
				Dataset<Row> df = jc.sparkSession().read().option("delimiter", "\t").format("csv").load(
						new File(jc.fileSystemRoot("hdfs"), "usr/data/input").toURI().toString()
				);

				df.show();

				if (df.count() > 0)
				{
					df = df.select(
							df.col("_c0").as("column_1"),
							df.col("_c1").as("column_2"),
							df.col("_c2").as("column_3")
					);
				}

				DataFrameWriter<Row> ndf = df.write().format(id);

				ndf.save(
					new File(jc.fileSystemRoot("hdfs"), "usr/data/output").toURI().toString()
				);
			}
		};
	}
}
