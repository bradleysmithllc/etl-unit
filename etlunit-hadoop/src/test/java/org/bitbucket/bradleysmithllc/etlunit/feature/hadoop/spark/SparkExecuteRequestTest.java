package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.spark;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy.TestSparkProxy;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SparkExecuteRequestTest {
	@Test
	public void serDe() throws Exception {
		TestSparkProxy proxyClass = new TestSparkProxy();
		test(new SparkExecuteRequest(
				proxyClass,
				"good",
				Collections.EMPTY_MAP
		));
	}

	@Test
	public void serDeEmptyMap() throws Exception {
		Map<String, File> roots = new HashMap<>();

		TestSparkProxy proxyClass = new TestSparkProxy();
		test(new SparkExecuteRequest(
				proxyClass,
				"good",
				roots
		));
	}

	@Test
	public void serDeMap() throws Exception {
		Map<String, File> roots = new HashMap<>();

		roots.put("hdfs1", File.createTempFile("PPP", "LLL"));
		roots.put("hdfs2", File.createTempFile("PPP", "LLL"));
		roots.put("hdfs3", File.createTempFile("PPP", "LLL"));
		roots.put("hdfs4", File.createTempFile("PPP", "LLL"));
		roots.put("hdfs5", File.createTempFile("PPP", "LLL"));

		TestSparkProxy proxyClass = new TestSparkProxy();
		test(new SparkExecuteRequest(
				proxyClass,
				"good",
				roots
		));
	}

	private void test(SparkExecuteRequest ser) throws Exception {
		StringWriter stringWriter = new StringWriter();
		PrintWriter out = new PrintWriter(stringWriter);

		ser.send(out);
		out.close();

		SparkProcessorRequest req = RequestLoader.receive(new BufferedReader(new StringReader(stringWriter.toString())));

		Assert.assertEquals(ser.proxy().getClass(), ((SparkExecuteRequest) req).proxy().getClass());
		Assert.assertEquals(ser.job().getClass(), ((SparkExecuteRequest) req).job().getClass());
		Assert.assertEquals(ser.roots(), ((SparkExecuteRequest) req).roots());
	}
}
