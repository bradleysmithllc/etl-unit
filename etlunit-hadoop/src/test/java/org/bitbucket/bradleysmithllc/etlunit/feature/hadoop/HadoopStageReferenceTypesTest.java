package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.workspace.WorkspaceRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

public class HadoopStageReferenceTypesTest extends BaseIntegrationTest {

	private WorkspaceRuntimeSupport workspaceRuntimeSupport;

	@Inject
	public void receiveWorkspaceRuntimeSupport(WorkspaceRuntimeSupport wrs) {
		workspaceRuntimeSupport = wrs;
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed) throws TestAssertionFailure {
		List<String> descriptions = HadoopProxyTest.grabDescriptions(mt);

		// every line of a description is a variable test
		for (String description : descriptions)
		{
			String[] dirs = description.split("\\|\\|");

			ETLTestValueObject act = context.getValue(dirs[0]);

			String valueAsString = act.getValueAsString();
			if (!valueAsString.equals(dirs[1]))
			{
				throw new TestAssertionFailure(dirs[0] + ": " + dirs[1] + " != " + valueAsString, "FAIL_CONTEXT_VARIABLE_ASSRT");
			}
		}
	}

	@Override
	protected String testSpecification() {
		return null;
	}

	@Test
	public void run() {
		startTest();
	}
}
