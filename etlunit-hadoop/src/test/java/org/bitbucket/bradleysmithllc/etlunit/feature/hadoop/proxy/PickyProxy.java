package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop.proxy;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class PickyProxy implements SparkProxy {
	@Override
	public SparkJob sparkJob(final String id) {
		return new SparkJob() {
			@Override
			public String id() {
				return id;
			}

			@Override
			public List<String> requiredFilesystems() {
				return Arrays.asList(id.split("\\|"));
			}

			@Override
			public void execute(JobConfiguration jc) {
				for (String req : requiredFilesystems())
				{
					File fsroot = jc.fileSystemRoot(req);

					if (fsroot == null)
					{
						throw new IllegalStateException("Job '" + id + " missing fs root '" + req + "'");
					}

					if (!fsroot.exists())
					{
						throw new IllegalStateException("Job '" + id + " fs root '" + req + "' does not exist.");
					}
				}
			}
		};
	}
}
