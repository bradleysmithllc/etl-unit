package org.bitbucket.bradleysmithllc.etlunit.feature.hadoop;

/*
 * #%L
 * etlunit-hadoop
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.TestWarning;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.workspace.Workspace;
import org.bitbucket.bradleysmithllc.etlunit.feature.workspace.WorkspaceRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestAnnotation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

public class HadoopFeatureTest extends BaseIntegrationTest {
	private WorkspaceRuntimeSupport workspaceRuntimeSupport;

	@Inject
	public void receiveWorkspaceuntimeSupport(WorkspaceRuntimeSupport wrs) {
		workspaceRuntimeSupport = wrs;
	}

	@Test
	public void runWithInjections() {
		startTest();
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed)
			throws TestAssertionFailure, TestExecutionError, TestWarning {
		List<ETLTestAnnotation> ann = mt.getAnnotations("@Description");

		if (ann.size() > 0) {
			// verify the result file
			for (ETLTestAnnotation anno : ann) {
				String spec = anno.getValue().query("description").getValueAsString();

				// grab the workspace
				Workspace wrk = workspaceRuntimeSupport.currentWorkspace();

				// locate the hdfs location
				String[] parts = spec.split("\\|\\|");

				FileBuilder fb = new FileBuilder(wrk.openPartition(parts[0]));

				if (parts.length > 1)
				{
					// part one is the workspace id, part two is the path to the file
					fb = fb.subdirs(parts[1]);
				}

				if (!fb.file().exists()) {
					throw new TestAssertionFailure(spec, "ASSERT_TGT_FILE_DOES_NOT_EXIST");
				}
			}
		}
	}
}
