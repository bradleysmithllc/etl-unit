package org.bitbucket.bradleysmithllc.etlunit.feature.execute.test;

/*
 * #%L
 * etlunit-execute
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.inject.Inject;

import org.bitbucket.bradleysmithllc.etlunit.ClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.ExecutionContext;
import org.bitbucket.bradleysmithllc.etlunit.NullClassDirector;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.ExecuteFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.extend.Extender;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExecutorFeatureModuleTest extends BaseFeatureModuleTest {

	private RunnerAbstractFeature null_runner;
	private RunnerAbstractFeature abc_runner;
	private RunnerAbstractFeature def_runner;
	private RunnerAbstractFeature ghi_runner;

	@Test
	public void theExecutorWhichAcceptsTheTestShouldRunIt() throws IOException {
		//startTest();

		//Assert.assertEquals("[1, 4]", null_runner.getRunids());
		//Assert.assertEquals("[2]", abc_runner.getRunids());
		//Assert.assertEquals("[3]", def_runner.getRunids());
		//Assert.assertEquals("[]", ghi_runner.getRunids());
	}

	@Override
	protected List<Feature> getTestFeatures() {
		null_runner = new RunnerAbstractFeature("null-runner");

		abc_runner = new RunnerAbstractFeature("abc-runner");

		def_runner = new RunnerAbstractFeature("def-runner");

		ghi_runner = new RunnerAbstractFeature("ghi-runner");

		return Arrays.asList((Feature) new ExecuteFeatureModule(), null_runner, abc_runner, def_runner, ghi_runner);
	}

	@Override
	protected void setUpSourceFiles() throws IOException {
		// create a test with the org.bitbucket.bradleysmithllc.etlunit.feature.org.bitbucket.bradleysmithllc.etlunit.feature.execute.test operations we want to respond to
		StringBuffer stb = new StringBuffer("class test_me {@Test executeMany() {execute(runner: 'null-runner', id: '1');execute(runner: 'abc-runner', id: '2');execute(runner: 'def-runner', id: '3');execute(runner: 'null-runner', id: '4');}}");
		createSource("test.etlunit", stb.toString());
	}

	private static class RunnerAbstractFeature extends AbstractFeature {
		private final List<String> runids = new ArrayList<String>();
		private final String name;

		private RunnerAbstractFeature(String name) {
			this.name = name;
		}

		public String getRunids()
		{
			return runids.toString();
		}

		@Inject
		public void installExecuteFeature(ExecuteFeatureModule executeFeatureModule)
		{
			// ugly cast
			executeFeatureModule.extend(new Extender() {
				public boolean canHandleRequest(ETLTestOperation etlTestOperation, ETLTestValueObject etlTestValueObject, VariableContext context, ExecutionContext executionContext) {
					ETLTestValueObject runner = etlTestValueObject.query("runner");
					ETLTestValueObject id = etlTestValueObject.query("id");

					return
						runner != null && runner.getValueAsString().equals(getFeatureName()) && id != null;
				}

				public ClassResponder.action_code process(ETLTestMethod mt, ETLTestOperation etlTestOperation, ETLTestValueObject etlTestValueObject, VariableContext context, ExecutionContext executionContext, int executor) {
					ETLTestValueObject runner = etlTestValueObject.query("runner");

					Assert.assertNotNull(runner);
					Assert.assertEquals(getFeatureName(), runner.getValueAsString());

					ETLTestValueObject id = etlTestValueObject.query("id");

					runids.add(id.getValueAsString());

					return ClassResponder.action_code.handled;
				}

				public Feature getFeature() {
					return RunnerAbstractFeature.this;
				}
			});
		}

		@Override
		public List<String> getPrerequisites() {
			return Arrays.asList("execute");
		}

		@Override
		public ClassDirector getDirector() {
			return new NullClassDirector(){
				@Override
				public response_code accept(ETLTestClass cl) {
					return response_code.accept;
				}

				@Override
				public response_code accept(ETLTestMethod mt) {
					return response_code.accept;
				}

				@Override
				public response_code accept(ETLTestOperation op) {
					return response_code.accept;
				}
			};
		}

		public String getFeatureName() {
			return name;
		}
	}
}
