package org.bitbucket.bradleysmithllc.etlunit.feature.execute.test;

/*
 * #%L
 * etlunit-execute
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.ApacheProcessRedirectExecutor;
import org.bitbucket.bradleysmithllc.etlunit.io.StreamProcessExecutor;
import org.junit.Test;

public class JavaExecutorIntegrationTest extends BaseIntegrationTest {
	@Override
	protected void prepareTest() {
		runtimeSupport.installProcessExecutor(new StreamProcessExecutor());
	}

	@Test
	public void run() {
		// perhaps the process executor is not active?
		//while (true)
		{
			TestResults res = startTest();

			if (res.getMetrics().getNumberOfAssertionFailures() != 0)
			{
				return;
			}
		}

		/*
		Assert.assertEquals(6, MainMe.tests.size());

		for (Map.Entry<String, Map> entry : MainMe.tests.entrySet())
		{
			Assert.assertEquals("true", entry.getValue().get("etlunit.active"));
		}

		Map t = MainMe.tests.get("run_java.run_empty_properties_okay.execute.0");
		Assert.assertEquals("true", t.get("etlunit.active"));
		Assert.assertEquals("run_java", t.get("etlunit.test.class"));
		Assert.assertEquals("run_empty_properties_okay", t.get("etlunit.test.method"));
		Assert.assertEquals("execute", t.get("etlunit.test.operation"));
		Assert.assertEquals("0", t.get("etlunit.test.operation.ordinal"));
		Assert.assertEquals(0, ((String [] )t.get(MainMe.ARGS_KEY)).length);

		// verify args
		t = MainMe.tests.get("run_java.run_with_args.execute.0");
		String [] args = (String [] )t.get(MainMe.ARGS_KEY);

		Assert.assertEquals("a", args[0]);
		Assert.assertEquals("b", args[1]);
		Assert.assertEquals("c", args[2]);
		Assert.assertEquals("d", args[3]);

		t = MainMe.tests.get("run_java.run.execute.2");
		Assert.assertEquals("b", t.get("a"));

		t = MainMe.tests.get("run_java.run_with_properties.execute.0");
		Assert.assertEquals("no", t.get("djanky"));
		 */
	}
}
