package test;

/*
 * #%L
 * etlunit-execute
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.*;

public class MainMe
{
	public static void main(String [] args) throws IOException {
		JsonWriter writer = new JsonWriter(new OutputStreamWriter(System.out));
		writer.setIndent("  ");

		writer.beginObject();

		writer.name("arguments").beginArray();

		for (String arg : args)
		{
			writer.value(arg);
		}

		writer.endArray();

		writer.name("system-properties").beginObject();

		Set<Object> propertySet = System.getProperties().keySet();

		ArrayList<Object> arrayList = new ArrayList<>(propertySet);

		ArrayList<String> sortedList = new ArrayList<>();

		for (Object propName : arrayList) {
			sortedList.add(propName.toString());
		}

		Collections.sort(sortedList);

		for (String key : sortedList)
		{
			if (key.startsWith("etlunit"))
			{
				writer.name(key).value(System.getProperty(key));
			}
		}

		writer.endObject();

		writer.endObject();
		writer.close();
		System.out.flush();

		Assert.assertEquals("true", System.getProperty("etlunit.active"));

		int exitCode = 0;

		String property = System.getProperty("etlunit.requested.exit.code");
		if (property != null)
		{
			exitCode = Integer.parseInt(property);
		}

		System.exit(exitCode);
	}

	public void notTest() throws Throwable {
	}

	@Test
	public void pass() throws Throwable {
	}

	@Test
	public void fail() throws Throwable {
		throw new Throwable();
	}

	@Test
	private void privat() {}

	@Test
	protected void protect() {}

	@Test
	public static void stat() {}

	@Test
	public void junitFrameworkAssertFail() {
		throw new junit.framework.AssertionFailedError("Failed");
	}

	@Test
	public void orgJunitAssertFail() {
		Assert.fail("Failure");
	}

	@Test
	public void etlunitAssertFail() throws TestAssertionFailure {
		throw new TestAssertionFailure("I failed", "ETLUNIT_ASSERTION_FAILURE");
	}

	@Test
	public void etlunitTestExecutionError() throws TestExecutionError {
		throw new TestExecutionError("I died", "ETLUNIT_EXECUTION_ERROR");
	}

	@Test
	public void javaAssertFail() throws TestAssertionFailure {
		throw new AssertionError("Whatever");
	}
}
