package org.bitbucket.bradleysmithllc.etlunit.feature.execute;

/*
 * #%L
 * etlunit-execute
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.json.java_unit_executor.execute.ExecuteHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.json.java_unit_executor.execute.ExecuteRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.json.java_unit_executor.execute_java_unit.ExecuteJavaUnitHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.execute.json.java_unit_executor.execute_java_unit.ExecuteJavaUnitRequest;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

public class JavaUnitExecutor extends NullClassListener implements ExecuteHandler, ExecuteJavaUnitHandler {
	private Log applicationLog;
	private MetaDataManager metaDataManager;
	private MetaDataContext javaMetaContext;

	@Inject
	public void setLogger(@Named("applicationLog") Log log)
	{
		applicationLog = log;
	}

	@Inject
	public void receiveMetaDataManager(MetaDataManager manager)
	{
		metaDataManager = manager;
		javaMetaContext = metaDataManager.getMetaData().getOrCreateContext("java-unit");
	}

	@Override
	public action_code execute(ExecuteRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return executeUnit(
			request.getJavaUnitClass(),
				request.getJavaUnitMethod(),
				testMethod,
				variableContext
		);
	}

	private action_code executeUnit(String requestJavaUnitClass, String requestJavaUnitMethod, ETLTestMethod testMethod, VariableContext variableContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// determine the class and method names
		String className = getClassName(testMethod.getTestClass().getAnnotations("@JavaUnit"), testMethod.getAnnotations("@JavaUnit"), requestJavaUnitClass);
		String thisMethodName = requestJavaUnitMethod;

		// lookup the test class
		try {
			Class cl = Thread.currentThread().getContextClassLoader().loadClass(className);

			// create a new instance
			Object obj = cl.newInstance();

			Method method = cl.getDeclaredMethod(thisMethodName);

			if (!Modifier.isPublic(method.getModifiers())) {
				throw new TestExecutionError("Java method must not be static: " + className, ExecuteConstants.ERR_JAVA_UNIT_METHOD_MUST_BE_PUBLIC);
			}

			if (!methodHasAnnotationType(method, "org.junit.Test")) {
				throw new TestExecutionError("Java method must be a junit @Test: " + className, ExecuteConstants.ERR_JAVA_UNIT_MISSING_TEST_ANNOTATION);
			}

			if (Modifier.isStatic(method.getModifiers())) {
				throw new TestExecutionError("Java method must not be static: " + className, ExecuteConstants.ERR_JAVA_UNIT_STATIC_METHOD);
			}

			// look for injection points
			injectFields(cl, obj, variableContext);

			// check for junit Before methods
			executeAllAnnotated(cl, obj, "org.junit.Before", true);

			// execute method
			method.invoke(obj);

			// execute all After..
			executeAllAnnotated(cl, obj, "org.junit.After", false);
			return action_code.handled;
		} catch (ClassNotFoundException e) {
			throw new TestExecutionError("Java class not found: " + className, ExecuteConstants.ERR_JAVA_UNIT_CLASS_NOT_FOUND, e);
		} catch (IllegalAccessException e) {
			throw new TestExecutionError("Java class not accessible: " + className, ExecuteConstants.ERR_JAVA_UNIT_CLASS_NOT_ACCESSIBLE, e);
		} catch (InstantiationException e) {
			throw new TestExecutionError("Java class could not be created: " + className, ExecuteConstants.ERR_JAVA_UNIT_CLASS_COULD_NOT_BE_INSTANTIATED, e);
		} catch (NoSuchMethodException e) {
			throw new TestExecutionError("Java class method not found: " + className + "." + thisMethodName, ExecuteConstants.ERR_JAVA_UNIT_CLASS_METHOD_NOT_FOUND, e);
		} catch (InvocationTargetException e) {
			// etlunit-mappable errors
			if (e.getCause() instanceof AssertionError) {
				throw new TestAssertionFailure(e.getCause().getMessage(), ExecuteConstants.FAIL_JAVA_UNIT_JAVA_ASSERTION);
			} else if (e.getCause() instanceof TestExecutionError) {
				throw (TestExecutionError) e.getCause();
			} else if (e.getCause() instanceof TestAssertionFailure) {
				throw (TestAssertionFailure) e.getCause();
			}

			throw new TestExecutionError("Java method failed: " + className + "." + thisMethodName, ExecuteConstants.ERR_JAVA_UNIT_METHOD_FAILED, e);
		}
	}

	private void injectFields(Class cl, Object obj, VariableContext variableContext) throws TestExecutionError {
		for (Field field : FieldUtils.getAllFieldsList(cl)) {
			ETLUnitDatabase annot = field.getAnnotation(ETLUnitDatabase.class);

			if (annot == null) {
				continue;
			}

			applicationLog.info("Field has ETLUnitDatabase annotation: " + field.getName());

			// assert that it is public
			if (!Modifier.isPublic(field.getModifiers())) {
				throw new TestExecutionError("ETLUnitDatabase-annotated field must be public: " + field.getName(), ExecuteConstants.ERR_JAVA_UNIT_PROPERTY_MUST_BE_PUBLIC);
			}

			String connectionId = annot.connectionId();
			String mode = annot.mode();

			applicationLog.info("Field wants connection info for connection id " + connectionId + " and mode " + mode);

			// first grab the connections
			if (!variableContext.hasVariableBeenDeclared("database")) {
				throw new TestExecutionError("Database support not present", ExecuteConstants.ERR_JAVA_UNIT_DATABASE_SUPPORT_MISSING);
			}

			ETLTestValueObject connections = variableContext.query("database.connections");

			if (connections == null) {
				throw new TestExecutionError("Database connections not found: " + connectionId, ExecuteConstants.ERR_JAVA_UNIT_DATABASE_CONNECTIONS_NOT_FOUND);
			}

			// first grab the connection
			ETLTestValueObject result = connections.query(connectionId);
			ETLTestValueObject modeConnection = null;

			if (result == null) {
				throw new TestExecutionError("Database connection id not found: " + connectionId, ExecuteConstants.ERR_JAVA_UNIT_DATABASE_CONNECTION_NOT_FOUND);
			} else if (!mode.equals("")) {
				// grab only the mode requested
				modeConnection = result.query(mode);

				if (modeConnection == null) {
					throw new TestExecutionError("Database connection id " + connectionId + " does not declare mode: " + mode, ExecuteConstants.ERR_JAVA_UNIT_DATABASE_CONNECTION_MODE_NOT_FOUND);
				}
			} else {
				// grab the first declared entry
				Map<String, ETLTestValueObject> modeMap = result.getValueAsMap();

				// if there is only one, grab it.  If there is more than one, this is an error because behavior
				// will be undefined
				if (modeMap.size() == 1) {
					for (Map.Entry<String, ETLTestValueObject> entry : modeMap.entrySet()) {
						modeConnection = entry.getValue();
					}
				} else {
					throw new TestExecutionError("Database connection id declares more than one mode.  Mode selection is ambiguous: " + connectionId, ExecuteConstants.ERR_JAVA_UNIT_DATABASE_CONNECTION_MODE_AMBIGUOUS);
				}
			}

			// database->connections->{id}->{mode}->jdbc-url

			// find the database definition
			String jdbcUrl = modeConnection.queryRequired("jdbc-url").getValueAsString();

			String loginName = null;

			ETLTestValueObject jdbcUsername = modeConnection.queryRequired("login-name");
			if (!jdbcUsername.isNull()) {
				loginName = jdbcUsername.getValueAsString();
			}

			String password = null;

			ETLTestValueObject jdbcPassword = modeConnection.queryRequired("password");
			if (!jdbcPassword.isNull()) {
				password = jdbcPassword.getValueAsString();
			}

			// field must be type DatabaseInfo
			if (!DatabaseInfo.class.equals(field.getType())) {
				throw new TestExecutionError("Field must be of type DatabaseInfo: " + field.getName(), ExecuteConstants.ERR_JAVA_UNIT_WRONG_TYPE);
			}

			// now assign the url to the field
			try {
				applicationLog.info("Injecting jdbc url into db info: " + field.getDeclaringClass().getSimpleName() + "." + field.getName() + " url " + jdbcUrl);
				DatabaseInfo dbInfo = (DatabaseInfo) field.get(obj);
				dbInfo.withJdbcUrl(jdbcUrl).withJdbcUsername(loginName).withJdbcPassword(password);
				applicationLog.info("Provided");
			} catch (IllegalAccessException e) {
				throw new TestExecutionError("Could not set field value: " + field.getName(), ExecuteConstants.ERR_JAVA_UNIT_ASSIGN_VALUE);
			}
		}
	}

	private void executeAllAnnotated(Class cl, Object instance, String annotationTypeName, boolean topDown) throws TestExecutionError, InvocationTargetException, IllegalAccessException {
		for (Method method : getAccessibleMethods(cl, topDown)) {
			// make sure it is annotated
			if (methodHasAnnotationType(method, annotationTypeName)) {
				// make sure it is public and no-arg
				if (!Modifier.isPublic(method.getModifiers())) {
					throw new TestExecutionError("Java unit lifecycle method not public: " + cl + "." + method, ExecuteConstants.ERR_JAVA_UNIT_LIFECYCLE_METHOD_NOT_PUBLIC);
				} else if (Modifier.isStatic(method.getModifiers())) {
					throw new TestExecutionError("Java unit lifecycle method cannot be static: " + cl + "." + method, ExecuteConstants.ERR_JAVA_UNIT_LIFECYCLE_STATIC);
				}

				method.invoke(instance);
			}
		}
	}

	private Iterable<? extends Method> getAccessibleMethods(Class cl, boolean topDown) {
		List<Method> methodList = new ArrayList<>();

		while (!cl.equals(Object.class)) {
			methodList.addAll(Arrays.asList(cl.getDeclaredMethods()));

			cl = cl.getSuperclass();
		}

		// do this in reverse order - calling super methods before sub methods

		if (topDown) {
			Collections.reverse(methodList);
		}
		return methodList;
	}

	private String getClassName(List<ETLTestAnnotation> classAnn, List<ETLTestAnnotation> methodAnn, String operationJavaUnitClass) throws TestExecutionError {
		if (operationJavaUnitClass != null) {
			// operation first
			return operationJavaUnitClass;
		} else if (methodAnn.size() == 1) {
			// next method
			return methodAnn.get(0).getValue().queryRequired("java-unit-class").getValueAsString();
		} else if (classAnn.size() == 1) {
			// next method
			return classAnn.get(0).getValue().queryRequired("java-unit-class").getValueAsString();
		} else {
			throw new TestExecutionError("Could not determine java unit class", "ERR_MISSING_JAVA_UNIT_CLASS");
		}
	}

	@Override
	public void begin(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		verifyAnnotaions(mt.getAnnotations("@JavaUnit"), mt.getQualifiedName());
	}

	@Override
	public void begin(ETLTestClass cl, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		verifyAnnotaions(cl.getAnnotations("@JavaUnit"), cl.getQualifiedName());
	}

	private void verifyAnnotaions(List<ETLTestAnnotation> annotations, String identifier) throws TestExecutionError {
		if (annotations.size() > 1) {
			throw new TestExecutionError("@JavaUnit annotation may not be duplicated on a test class: " + identifier, "ERR_DUPLICATED_ANNOTATION");
		}
	}

	private boolean methodHasAnnotationType(Method method, String annotationTypeName) {
		applicationLog.info("Searching for annotation named " + annotationTypeName + " on method " + method.getName());

		Annotation[] annot = method.getDeclaredAnnotations();

		applicationLog.info("Found " + annot.length + " annotations");

		for (Annotation anno : annot) {
			String name = anno.annotationType().getName();
			applicationLog.info("Annotation type " + name + " found");
			if (name.equals(annotationTypeName)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public action_code executeJavaUnit(ExecuteJavaUnitRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		return executeUnit(
				null,
				testMethod.getName(),
				testMethod,
				variableContext
		);
	}
}
