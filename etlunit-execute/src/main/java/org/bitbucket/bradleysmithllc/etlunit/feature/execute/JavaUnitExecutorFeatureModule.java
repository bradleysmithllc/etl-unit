package org.bitbucket.bradleysmithllc.etlunit.feature.execute;

/*
 * #%L
 * etlunit-execute
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;

import java.util.Arrays;
import java.util.List;

public class JavaUnitExecutorFeatureModule extends AbstractFeature
{
	private static final List<String> prerequisites = Arrays.asList("execute");

	private final JavaUnitExecutor javaExecutor = new JavaUnitExecutor();

	@Override
	public List<String> getPrerequisites() {
		return prerequisites;
	}

	@Override
	public void initialize(Injector inj)
	{
		postCreate(javaExecutor);
	}

	@Override
	public ClassListener getListener()
	{
		return javaExecutor;
	}
}
