package org.bitbucket.bradleysmithllc.etlunit.feature.execute;

/*
 * #%L
 * etlunit-execute
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.inject.Inject;
import javax.inject.Named;

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.extend.Extender;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassResponder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RuntimeExecutor implements Extender {
	private RuntimeSupport runtimeSupport;
	private Log applicationLog;
	private final Feature executeFeature;

	public RuntimeExecutor(Feature executeFeature) {
		this.executeFeature = executeFeature;
	}

	@Inject
	public void setLogger(@Named("applicationLog") Log log)
	{
		applicationLog = log;
	}
	
	@Inject
	public void addRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
	}

	public boolean canHandleRequest(ETLTestOperation etlTestOperation, ETLTestValueObject etlTestValueObject, VariableContext context, ExecutionContext executionContext) {
		return
			etlTestValueObject != null
				&&
				etlTestValueObject.query("executable") != null;
	}

	public ClassResponder.action_code process(ETLTestMethod mt, ETLTestOperation etlTestOperation, ETLTestValueObject etlTestValueObject, VariableContext context, ExecutionContext executionContext, int executor) {
		// executable is required,
		// arguments, redirect stderr and working directory are optional
		String exe = etlTestValueObject.query("executable").getValueAsString();
		List<String> args = new ArrayList<String>();

		ETLTestValueObject argvalue = etlTestValueObject.query("argument-list");

		if (argvalue != null)
		{
			args.addAll(argvalue.getValueAsListOfStrings());
		}

		File working = null;

		argvalue = etlTestValueObject.query("working-directory");

		if (argvalue != null)
		{
			working = new File(etlTestValueObject.getValueAsString());
		}

		Boolean redirect = null;

		argvalue = etlTestValueObject.query("redirect-error-stream");

		if (argvalue != null)
		{
			redirect = etlTestValueObject.getValueAsBoolean() ? Boolean.TRUE : Boolean.FALSE;
		}

		ProcessDescription pd = new ProcessDescription(exe).arguments(args);

		if (working != null)
		{
			pd.workingDirectory(working);
		}

		ProcessFacade facade = null;
		try {
			facade = runtimeSupport.execute(pd);

			facade.waitForCompletion();

			applicationLog.info("Process completed with code [" + facade.getCompletionCode() + "]");
			applicationLog.info("Process output [" + facade.getInputBuffered() + "]");
		} catch (IOException e) {
			applicationLog.severe(e.toString(), e);
		}

		return ClassResponder.action_code.handled;
	}

	public Feature getFeature() {
		return executeFeature;
	}
}
