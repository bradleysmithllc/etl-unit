package org.bitbucket.bradleysmithllc.etlunit.feature.execute;

/*
 * #%L
 * etlunit-execute
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface ExecuteConstants
{
	String ERR_JAVA_UNIT_CLASS_NOT_FOUND = "ERR_JAVA_UNIT_CLASS_NOT_FOUND";
	String ERR_JAVA_UNIT_CLASS_NOT_ACCESSIBLE = "ERR_JAVA_UNIT_CLASS_NOT_ACCESSIBLE";
	String ERR_JAVA_UNIT_CLASS_COULD_NOT_BE_INSTANTIATED = "ERR_JAVA_UNIT_CLASS_COULD_NOT_BE_INSTANTIATED";
	String ERR_JAVA_UNIT_CLASS_METHOD_NOT_FOUND = "ERR_JAVA_UNIT_CLASS_METHOD_NOT_FOUND";
	String ERR_JAVA_UNIT_METHOD_FAILED = "ERR_JAVA_UNIT_METHOD_FAILED";
	String ERR_JAVA_UNIT_STATIC_METHOD = "ERR_JAVA_UNIT_STATIC_METHOD";
	String ERR_JAVA_UNIT_MISSING_TEST_ANNOTATION = "ERR_JAVA_UNIT_MISSING_TEST_ANNOTATION";
	String ERR_JAVA_UNIT_LIFECYCLE_METHOD_NOT_PUBLIC = "ERR_JAVA_UNIT_LIFECYCLE_METHOD_NOT_PUBLIC";
	String ERR_JAVA_UNIT_LIFECYCLE_STATIC = "ERR_JAVA_UNIT_LIFECYCLE_STATIC";
	String ERR_JAVA_UNIT_METHOD_MUST_BE_PUBLIC = "ERR_JAVA_UNIT_METHOD_MUST_BE_PUBLIC";
	String FAIL_JAVA_UNIT_JAVA_ASSERTION = "FAIL_JAVA_UNIT_JAVA_ASSERTION";

	String ERR_JAVA_UNIT_DATABASE_SUPPORT_MISSING = "ERR_JAVA_UNIT_DATABASE_SUPPORT_MISSING";
	String ERR_JAVA_UNIT_DATABASE_CONNECTION_NOT_FOUND = "ERR_JAVA_UNIT_DATABASE_CONNECTION_NOT_FOUND";
	String ERR_JAVA_UNIT_DATABASE_CONNECTIONS_NOT_FOUND = "ERR_JAVA_UNIT_DATABASE_CONNECTIONS_NOT_FOUND";
	String ERR_JAVA_UNIT_DATABASE_CONNECTION_MODE_AMBIGUOUS = "ERR_JAVA_UNIT_DATABASE_CONNECTION_MODE_AMBIGUOUS";
	String ERR_JAVA_UNIT_DATABASE_CONNECTION_MODE_NOT_FOUND = "ERR_JAVA_UNIT_DATABASE_CONNECTION_MODE_NOT_FOUND";
	String ERR_JAVA_UNIT_WRONG_TYPE = "ERR_JAVA_UNIT_WRONG_TYPE";
	String ERR_JAVA_UNIT_PROPERTY_MUST_BE_PUBLIC = "ERR_JAVA_UNIT_PROPERTY_MUST_BE_PUBLIC";
	String ERR_JAVA_UNIT_ASSIGN_VALUE = "ERR_JAVA_UNIT_ASSIGN_VALUE";

	String ERR_JAVA_MAIN_NOT_PUBLIC = "ERR_JAVA_MAIN_NOT_PUBLIC";
	String ERR_JAVA_MAIN_NOT_STATIC = "ERR_JAVA_MAIN_NOT_STATIC";
	String ERR_JAVA_CLASS_NOT_FOUND = "ERR_JAVA_CLASS_NOT_FOUND";
	String ERR_JAVA_MAIN_METHOD_NOT_FOUND = "ERR_JAVA_MAIN_METHOD_NOT_FOUND";
	String ERR_JAVA_EXECUTION_ERROR = "ERR_JAVA_EXECUTION_ERROR";
	String ERR_JAVA_EXECUTION_ERROR_BAD_RETURN_CODE = "ERR_JAVA_EXECUTION_ERROR_BAD_RETURN_CODE";
}
