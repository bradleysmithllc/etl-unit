package org.bitbucket.bradleysmithllc.etlunit.feature.execute;

/*
 * #%L
 * etlunit-execute
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseInfo {
	private String jdbcUsername = "";
	private String jdbcPassword = "";
	private String jdbcUrl = "";

	public String jdbcUsername() {
		return jdbcUsername;
	}

	public DatabaseInfo withJdbcUsername(String jdbcUsername) {
		this.jdbcUsername = jdbcUsername;
		return this;
	}

	public String jdbcPassword() {
		return jdbcPassword;
	}

	public DatabaseInfo withJdbcPassword(String jdbcPassword) {
		this.jdbcPassword = jdbcPassword;
		return this;
	}

	public String jdbcUrl() {
		return jdbcUrl;
	}

	public DatabaseInfo withJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
		return this;
	}

	public Connection openConnection() throws SQLException {
		return DriverManager.getConnection(jdbcUrl(), jdbcUsername(), jdbcPassword());
	}
}
