package org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion;

/*
 * #%L
 * etlunit-log-assertion
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.io.ApacheProcessStreamsExecutor;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;

import org.junit.Test;
import org.junit.Assert;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import java.io.IOException;

public class LogAssertionFeatureTest extends BaseIntegrationTest {
	private LogFileManager logFileManager;

	@Inject
	public void receiveLogFileManager(LogFileManager manager)
	{
		logFileManager = manager;
	}

	@Test
	public void test()
	{
		startTest();
	}

	@Override
	protected void assertVariableContextPre(ETLTestMethod mt, VariableContext context) {
		// check that the method name in the context matches the actual method name
		ETLTestMethod contextMethod = (ETLTestMethod) context.getValue("etlunit_test_method").getValueAsPojo();

		Assert.assertNotNull("Where is my test method??", contextMethod);

		Assert.assertEquals("ETLTest Method does not match", contextMethod.getQualifiedName(), mt.getQualifiedName());

		Assert.assertEquals("ETLTest Method does not match", mt.getName(), context.contextualize("${etlunit_test_method.name}"));
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed) {
		if (mt.getQualifiedName().equals("[default].beforeAndAfter.test"))
		{
			List<LogFileManager.LogFile> logs = logFileManager.getLogFilesByETLTestMethod(mt);

			// there should be three of these
			Assert.assertEquals(3, logs.size());
		}
	}

	@Override
	protected void prepareTest() {
		runtimeSupport.installProcessExecutor(new ApacheProcessStreamsExecutor());
	}
}
