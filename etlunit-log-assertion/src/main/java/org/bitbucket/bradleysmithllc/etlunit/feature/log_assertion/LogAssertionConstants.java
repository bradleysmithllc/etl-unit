package org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion;

/*
 * #%L
 * etlunit-log-assertion
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface LogAssertionConstants
{

	String ERR_LOG_ASSERTION_MISSING_EXPECTED_MESSAGE = "ERR_LOG_ASSERTION_MISSING_EXPECTED_MESSAGE";
	String ERR_LOG_ASSERTION_DUPLICATE_EXPECTED_MESSAGE = "ERR_LOG_ASSERTION_DUPLICATE_EXPECTED_MESSAGE";
	String ERR_LOG_ASSERTION_COULD_NOT_LOCATE_EXPECTED_LOG_FILE = "ERR_LOG_ASSERTION_COULD_NOT_LOCATE_EXPECTED_LOG_FILE";
	String ERR_LOG_ASSERTION_NO_LOGS = "ERR_LOG_ASSERTION_NO_LOGS";
	String ERR_LOG_ASSERTION_LOG_NOT_FOUND = "ERR_LOG_ASSERTION_LOG_NOT_FOUND";
	String ERR_LOG_ASSERTION_COULD_READ_LOG_FILE = "ERR_LOG_ASSERTION_COULD_READ_LOG_FILE";

	String FAIL_LOG_ASSERTION_LOG_FILE_DOES_NOT_MATCH = "FAIL_LOG_ASSERTION_LOG_FILE_DOES_NOT_MATCH";
	String ERR_LOG_ASSERTION_BAD_ANNOTATION = "ERR_LOG_ASSERTION_BAD_ANNOTATION";
	String ERR_LOG_ASSERTION_BAD_ANNOTATION_MISSING_OPTIONS = "ERR_LOG_ASSERTION_BAD_ANNOTATION_MISSING_OPTIONS";
	String ERR_LOGS_ASSERTION_BAD_EXPECTS = "ERR_LOGS_ASSERTION_BAD_EXPECTS";
}
