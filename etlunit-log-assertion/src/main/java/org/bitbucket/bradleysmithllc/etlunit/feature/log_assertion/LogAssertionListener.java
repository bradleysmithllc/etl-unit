package org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion;

/*
 * #%L
 * etlunit-log-assertion
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion.json.log_assertion.assert_log.AssertLogHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion.json.log_assertion.assert_log.AssertLogRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion.json.log_assertion.assert_logs.AssertLogsHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion.json.log_assertion.assert_logs.AssertLogsRequest;
import org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion.json.log_assertion.assert_logs.ExpectedLog;
import org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion.json.log_assertion.assert_logs.LogDefaults;
import org.bitbucket.bradleysmithllc.etlunit.feature.logging.LogFileManager;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataArtifact;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataPackageContext;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class LogAssertionListener extends NullClassListener implements AssertLogHandler, AssertLogsHandler {
	private LogAssertionRuntimeSupport logAssertionRuntimeSupport;
	private LogFileManager logFileManager;
	private final LogAssertionFeature logAssertionFeature;

	public LogAssertionListener(LogAssertionFeature logAssertionFeature) {
		this.logAssertionFeature = logAssertionFeature;
	}

	@Inject
	public void receiveLogAssertionRuntimeSupport(LogAssertionRuntimeSupport support) {
		logAssertionRuntimeSupport = support;
	}

	@Inject
	public void receiveLogFileManager(LogFileManager manager) {
		logFileManager = manager;
	}

	@Override
	public action_code assertLog(AssertLogRequest request, ETLTestMethod mt, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		assertLogImpl(request, ObjectUtils.firstNotNull(mt, testOperation.getTestMethod()), variableContext);

		return action_code.handled;
	}

	public void assertLogImpl(AssertLogRequest request, ETLTestMethod testMethod, VariableContext variableContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		String expectedLogFile = request.getExpectedLogFile();
		String expectedLogExpression = request.getExpectedLogExpression();

		if (expectedLogExpression == null && expectedLogFile == null) {
			throw new TestExecutionError("Log assertion requires exactly one of expected-log-expression or expected-log-file", LogAssertionConstants.ERR_LOG_ASSERTION_MISSING_EXPECTED_MESSAGE);
		}

		if (expectedLogExpression != null && expectedLogFile != null) {
			throw new TestExecutionError("Log assertion requires exactly one of expected-log-expression or expected-log-file", LogAssertionConstants.ERR_LOG_ASSERTION_DUPLICATE_EXPECTED_MESSAGE);
		}

		// resolve the text to compare
		String checkText = null;

		if (expectedLogExpression != null) {
			checkText = expectedLogExpression;
		} else {
			try {
				File file = logAssertionRuntimeSupport.locateLogExpressionFileForCurrentTest(expectedLogFile);
				checkText = FileUtils.readFileToString(file);

				MetaDataContext logMetaContext = logAssertionFeature.getLogMetaContext();

				MetaDataPackageContext packContext = logMetaContext.createPackageContextForCurrentTest(MetaDataPackageContext.path_type.test_source);

				MetaDataArtifact packArt = packContext.createArtifact("log", file);
				packArt.createContent(file.getName()).referencedByCurrentTest("log-assertion");
			} catch (IOException e) {
				throw new TestExecutionError("Could not read expected log file source [" + expectedLogFile + "]", LogAssertionConstants.ERR_LOG_ASSERTION_COULD_NOT_LOCATE_EXPECTED_LOG_FILE);
			}
		}

		checkText = variableContext.contextualize(checkText);

		LogFileManager.LogFile lf = getLogFile(request, testMethod, logFileManager);

		try {
			String logFileText = variableContext.contextualize(FileUtils.readFileToString(lf.getFile()));

			AssertLogRequest.AssertionMode mode = request.getAssertionMode();

			String failureId = request.getFailureId();

			if (failureId == null) {
				failureId = LogAssertionConstants.FAIL_LOG_ASSERTION_LOG_FILE_DOES_NOT_MATCH;
			}

			// default mode is log-contains since it will be rare for an entire log to be predictable
			if (mode == null) {
				mode = AssertLogRequest.AssertionMode.CONTAINS;
			}

			switch (mode) {
				case EQUALS:
					if (!checkText.equals(logFileText)) {
						throw new TestAssertionFailure("Log file [" + lf.getFile().getName() + "] does not equal expectation", failureId);
					}

					break;
				case MATCHES: {
					Pattern p = Pattern.compile(checkText);

					if (!p.matcher(logFileText).matches()) {
						throw new TestAssertionFailure("Log file [" + lf.getFile().getName() + "] does not match expectated pattern", failureId);
					}

					break;
				}
				case CONTAINS: {
					if (logFileText.indexOf(checkText) == -1) {
						throw new TestAssertionFailure("Log file [" + lf.getFile().getName() + "] does not contain expectation", failureId);
					}

					break;
				}
				case CONTAINS_PATTERN: {
					Pattern p = Pattern.compile(checkText);

					if (!p.matcher(logFileText).find()) {
						throw new TestAssertionFailure("Log file [" + lf.getFile().getName() + "] does not contain expectation", failureId);
					}

					break;
				}
			}
		} catch (IOException e) {
			throw new TestExecutionError("Could not read actual log file source [" + expectedLogFile + "]", LogAssertionConstants.ERR_LOG_ASSERTION_COULD_READ_LOG_FILE);
		}
	}

	private LogFileManager.LogFile getLogFile(AssertLogRequest request, ETLTestMethod testMethod, LogFileManager logFileManager) throws TestExecutionError {
		// start off with a list of all logs created in this method
		List<LogFileManager.LogFile> logs = logFileManager.getLogFilesByETLTestMethod(testMethod);

		if (logs == null || logs.size() == 0) {
			throw new TestExecutionError("", LogAssertionConstants.ERR_LOG_ASSERTION_NO_LOGS);
		}

		// look for  file name pattern
		String namePattern = request.getLogNamePattern();
		Pattern nameP = null;

		if (namePattern != null) {
			nameP = Pattern.compile(namePattern);
		}

		// determine the traits we are looking for.  If there is no classifier we just go in order
		String classifier = request.getClassifier();

		// iterate backwards
		for (int i = logs.size() - 1; i >= 0; i--) {
			LogFileManager.LogFile logFile = logs.get(i);

			if (classifier == null || logFile.getClassifier().equals(classifier)) {
				// double-check in case a name pattern was supplied
				if (nameP != null) {
					if (
							!nameP.matcher(logFile.getFile().getName()).find()
									&&
									!nameP.matcher(logFile.getOriginalPath().getName()).find()
							) {
						continue;
					}
				}

				// this one'll do
				return logFile;
			}
		}

		throw new TestExecutionError("Could not match log request to log file manager", LogAssertionConstants.ERR_LOG_ASSERTION_LOG_NOT_FOUND);
	}

	@Override
	public void end(ETLTestMethod mt, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// check for log assertion annotations.  Handle multiple even though it doesn't really make sense
		List<ETLTestAnnotation> annots = mt.getAnnotations("@LogAssertion");

		for (ETLTestAnnotation annot : annots) {
			processLogFileAssertion(mt, context, annot);
		}
	}

	private void processLogFileAssertion(ETLTestMethod mt, VariableContext context, ETLTestAnnotation annot) throws TestExecutionError, TestWarning, TestAssertionFailure {
		ETLTestValueObject obj = annot.getValue();

		ETLTestValueObject exp = obj.query("expected-log");
		ETLTestValueObject exps = obj.query("expected-logs");

		// sanity check
		if (exp != null && exps != null) {
			throw new TestExecutionError("Specify only one log option for @LogAssertion", LogAssertionConstants.ERR_LOG_ASSERTION_BAD_ANNOTATION);
		} else if (exp == null && exps == null) {
			throw new TestExecutionError("Specify a log option for @LogAssertion", LogAssertionConstants.ERR_LOG_ASSERTION_BAD_ANNOTATION_MISSING_OPTIONS);
		}

		if (exp != null) {
			processLogAssertion(mt, context, exp);
		}


		if (exps != null) {
			for (ETLTestValueObject la : exps.getValueAsList()) {
				processLogAssertion(mt, context, la);
			}
		}
	}

	private void processLogAssertion(ETLTestMethod mt, VariableContext context, ETLTestValueObject exp) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// convert into a json object so I can deserialize into the AssertLog
		JsonNode json = exp.getJsonNode();

		ObjectMapper om = new ObjectMapper();

		AssertLogRequest assertLogRequest = null;
		try {
			assertLogRequest = om.readValue(json.toString(), AssertLogRequest.class);
		} catch (IOException e) {
			throw new TestExecutionError("Could not deserialize annotation", TestConstants.ERR_IO_ERROR, e);
		}

		// pass on to the assertLog handler
		assertLogImpl(assertLogRequest, mt, context);
	}

	@Override
	public action_code assertLogs(AssertLogsRequest request, ETLTestMethod testMethod, ETLTestOperation testOperation, ETLTestValueObject valueObject, VariableContext variableContext, ExecutionContext executionContext) throws TestAssertionFailure, TestExecutionError, TestWarning {
		// check for conflicting expects
		if (request.getExpectedLog() != null && request.getExpectedLogs().size() != 0) {
			throw new TestExecutionError("Expected log and Expected logs cannot both be specified", LogAssertionConstants.ERR_LOGS_ASSERTION_BAD_EXPECTS);
		}

		if (request.getExpectedLog() == null && request.getExpectedLogs().size() == 0) {
			throw new TestExecutionError("Specify one of expected log or logs", LogAssertionConstants.ERR_LOGS_ASSERTION_BAD_EXPECTS);
		}

		AssertLogsRequest.AssertionMode am = request.getAssertionMode();

		if (am == null) {
			// match all is default
			am = AssertLogsRequest.AssertionMode.MATCH_ALL;
		}

		// grab the single log option
		String thisFailureId = request.getFailureId();

		ExpectedLog el = request.getExpectedLog();

		List<ETLTestValueObject> list = new ArrayList<ETLTestValueObject>();

		if (el != null) {
			list.add(valueObject.query("expected-log"));
		}

		ETLTestValueObject logs = valueObject.query("expected-logs");

		if (logs != null) {
			list.addAll(logs.getValueAsList());
		}

		List<String> failureList = new ArrayList<String>();

		LogDefaults requestLogDefaults = request.getLogDefaults();
		ETLTestValueObject logDefaults = null;

		if (requestLogDefaults != null)
		{
			logDefaults = valueObject.query("log-defaults");
		}

		for (ETLTestValueObject la : list) {
			try {
				if (logDefaults != null)
				{
					// create a merged request object which contains these options
					la = la.merge(logDefaults, ETLTestValueObject.merge_type.left_merge, ETLTestValueObject.merge_policy.recursive);
				}

				processLogAssertion(testMethod, variableContext, la);

				// in the case of match any, we are done
				if (am == AssertLogsRequest.AssertionMode.MATCH_ANY)
				{
					return action_code.handled;
				}
			} catch (TestAssertionFailure taf) {
				// preserve the ids
				Collections.addAll(failureList, taf.getFailureIds());
			}
		}

		// match any failed to match any
		if (failureList.size() != 0)
		{
			if (thisFailureId != null)
			{
				throw new TestAssertionFailure("An assertion failed", thisFailureId);
			}
			else
			{
				throw new TestAssertionFailure("An assertion failed", failureList.toArray(new String [failureList.size()]));
			}
		}

		return action_code.handled;
	}

	@Override
	public void beginPackage(ETLTestPackage name, VariableContext context, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning {
		MetaDataContext logMetaContext = logAssertionFeature.getLogMetaContext();

		MetaDataPackageContext packContext = logMetaContext.createPackageContext(name, MetaDataPackageContext.path_type.test_source);

		try {
			MetaDataArtifact packArt = packContext.createArtifact("log", logAssertionRuntimeSupport.locateLogExpressionDirectoryForPackage(name));
			packArt.populateAllFromDir();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
