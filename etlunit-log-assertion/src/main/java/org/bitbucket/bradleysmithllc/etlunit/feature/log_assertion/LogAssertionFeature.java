package org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion;

/*
 * #%L
 * etlunit-log-assertion
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Module;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataContext;
import org.bitbucket.bradleysmithllc.etlunit.metadata.MetaDataManager;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

@FeatureModule
public class LogAssertionFeature extends AbstractFeature {
	private static final List<String> prerequisites = Arrays.asList("logging");
	private MetaDataManager metaDataManager;

	private final LogAssertionListener logAssertionListener = new LogAssertionListener(this);
	private final LogAssertionRuntimeSupport logAssertionRuntimeSupport = new LogAssertionRuntimeSupportImpl();
	private MetaDataContext logMetaContext;

	@Inject
	public void receiveMetaDataManager(MetaDataManager manager)
	{
		metaDataManager = manager;
		logMetaContext = metaDataManager.getMetaData().getOrCreateContext("log");
	}

	@Override
	protected Injector preCreateSub(Injector inj) {
		return inj.createChildInjector(new Module(){
			@Override
			public void configure(Binder binder) {
				binder.bind(LogAssertionRuntimeSupport.class).toInstance(logAssertionRuntimeSupport);
			}
		});
	}

	@Override
	public void initialize(Injector inj) {
		postCreate(logAssertionListener);
		postCreate(logAssertionRuntimeSupport);
	}

	@Override
	public List<String> getPrerequisites() {
		return prerequisites;
	}

	@Override
	public ClassListener getListener() {
		return logAssertionListener;
	}

	public MetaDataContext getLogMetaContext() {
		return logMetaContext;
	}
}
