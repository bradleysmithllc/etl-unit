package org.bitbucket.bradleysmithllc.etlunit.feature.log_assertion;

/*
 * #%L
 * etlunit-log-assertion
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackageImpl;

import javax.inject.Inject;
import java.io.File;

public class LogAssertionRuntimeSupportImpl implements LogAssertionRuntimeSupport
{
	private RuntimeSupport runtimeSupport;

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport support)
	{
		runtimeSupport = support;
	}

	@Override
	public File locateLogExpressionFile(String name) {
		return locateLogExpressionFile(ETLTestPackageImpl.getDefaultPackage(), name);
	}

	@Override
	public File locateLogExpressionFile(ETLTestPackage _package, String name) {
		return new FileBuilder(locateLogExpressionDirectoryForPackage(_package)).name(name + ".expr").file();
	}

	@Override
	public File locateLogExpressionFileForCurrentTest(String name) {
		return locateLogExpressionFile(runtimeSupport.getCurrentlyProcessingTestPackage(), name);
	}

	@Override
	public File locateLogExpressionDirectoryForPackage(ETLTestPackage name) {
		return new FileBuilder(runtimeSupport.getTestResourceDirectory(name, "log")).file();
	}

	@Override
	public File locateLogExpressionDirectoryForCurrentPackage() {
		return locateLogExpressionDirectoryForPackage(runtimeSupport.getCurrentlyProcessingTestPackage());
	}
}
