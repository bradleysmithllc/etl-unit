package org.bitbucket.bradleysmithllc.etlunit.maven.test;

/*
 * #%L
 * etlunit-feature-compiler Maven Mojo
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JPackage;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.bitbucket.bradleysmithllc.etlunit.maven.CustomGenerationConfig;
import org.bitbucket.bradleysmithllc.etlunit.maven.FeatureCompilerMojo;
import org.bitbucket.bradleysmithllc.etlunit.maven.PojoSchema;
import org.bitbucket.bradleysmithllc.etlunit.util.JsonSchemaResolver;
import org.jsonschema2pojo.*;
import org.jsonschema2pojo.rules.RuleFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class MojoTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	//@Test
	public void mojoCompiles() throws MojoExecutionException, URISyntaxException {
		FeatureCompilerMojo mojo = new FeatureCompilerMojo();

		File in = new File("/Users/bsmith/git/etl-unit/etlunit-feature-compiler/src/test/resources/");

		File out = new File(in.getParentFile().getParentFile().getParentFile(), "target/generated-test-sources/feature-compiler");
		out.mkdirs();

		mojo.setInputDirectory(in);
		mojo.setOutputDirectory(out);

		System.out.println(in.getAbsolutePath());

		mojo.execute();
	}

	/*
	@Test
	public void mojoDeserializes() throws IOException {

		ObjectMapper om = new ObjectMapper();

		String content = "{\"base\": \"Howdy!\", \"twiller\": {\"simple-name\": \"simpleton\"}}";
		WhileRequest obj = om.readValue(content, WhileRequest.class);

		System.out.println(obj);

		Gson gson = new GsonBuilder()
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
				.create();

		WhileRequest confObj = gson.fromJson(content, WhileRequest.class);

		System.out.println(confObj);
	}
	 */

	@Test
	public void schema2Pojo() throws Exception {
		JCodeModel codeModel = new JCodeModel();

		URL source = getClass().getClassLoader().getResource("org/bitbucket/bradleysmithllc/etlunit/featureOperations.jsonSchema");

		GenerationConfig generationConfig = new CustomGenerationConfig();

		RuleFactory ruleFactory = new RuleFactory(
				generationConfig,
			new Jackson2Annotator(generationConfig),
			new SchemaStore()
		);

		SchemaGenerator schemaGenerator = new SchemaGenerator();

		new SchemaMapper(ruleFactory, schemaGenerator).generate(codeModel, "FeatureOperationSchema", "com.example.operation", source);

		File out = new File("/tmp", "target/generated-test-sources/feature-compiler");

		FileUtils.deleteDirectory(out);

		out.mkdirs();

		JsonNode json = JsonSchemaResolver.resolveSchemaReference(getClass().getClassLoader().getResource("org/bitbucket/bradleysmithllc/etlunit/Configuration.jsonSchema"));

		File dest = temporaryFolder.newFile();
		FileUtils.write(dest, json.toString());

		new SchemaMapper(ruleFactory, schemaGenerator).generate(codeModel, "Configuration", "com.example.config", dest.toURL());

		codeModel.build(out);
	}

	@Test
	public void schema2PojoEmpty() throws IOException {
		JCodeModel codeModel = new JCodeModel();

		File objFile = temporaryFolder.newFile("temp");

		FileUtils.write(objFile, "{\"type\": \"object\"}");

		GenerationConfig generationConfig = new CustomGenerationConfig();

		RuleFactory ruleFactory = new RuleFactory(
				generationConfig,
				new Jackson2Annotator(generationConfig),
				new SchemaStore()
		);

		SchemaGenerator schemaGenerator = new SchemaGenerator();

		new SchemaMapper(ruleFactory, schemaGenerator).generate(codeModel, "EmptySchema", "com.example.operation", objFile.toURI().toURL());

		File out = new File("/tmp", "target/generated-test-sources/feature-compiler");

		FileUtils.deleteDirectory(out);

		out.mkdirs();

		codeModel.build(out);
	}

	@Test
	public void schema2PojoSimple() throws IOException {
		JCodeModel codeModel = new JCodeModel();
		JPackage _package = codeModel._package("it.pkg.json");

		JsonNode node = JsonLoader.fromString("{\"type\":\"object\",\"properties\":{\"useful-module\":{\"type\":\"string\"}}}");

		GenerationConfig generationConfig = new CustomGenerationConfig();

		RuleFactory ruleFactory = new RuleFactory(
				generationConfig,
				new Jackson2Annotator(generationConfig),
				new SchemaStore()
		);

		ruleFactory.getSchemaRule().apply("featureFeatureModuleConfiguration", node, null, _package, new PojoSchema(null, node));

		File out = new File("/tmp", "target/generated-test-sources/feature-compiler");

		FileUtils.deleteDirectory(out);

		out.mkdirs();

		codeModel.build(out);
	}
}
