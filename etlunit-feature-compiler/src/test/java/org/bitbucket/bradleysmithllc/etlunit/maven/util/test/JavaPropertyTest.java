package org.bitbucket.bradleysmithllc.etlunit.maven.util.test;

/*
 * #%L
 * etlunit-feature-compiler Maven Mojo
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Assert;
import org.bitbucket.bradleysmithllc.etlunit.maven.util.JavaProperty;
import org.junit.Test;

public class JavaPropertyTest
{
	@Test
	public void testPropertyName()
	{
		Assert.assertEquals("test", JavaProperty.makePropertyName("test"));
		Assert.assertEquals("testStore", JavaProperty.makePropertyName("testStore"));
		Assert.assertEquals("testStore", JavaProperty.makePropertyName("test-store"));
	}

	@Test
	public void testProperPropertyName()
	{
		Assert.assertEquals("Test", JavaProperty.makeProperPropertyName("test"));
		Assert.assertEquals("TestStore", JavaProperty.makeProperPropertyName("testStore"));
		Assert.assertEquals("TestStore", JavaProperty.makeProperPropertyName("test-store"));
	}

	enum values
	{
		a,b,c,d,e,f,aA
	}

	class Ser
	{
		values v1 = values.a;
		values v2 = values.b;
		values v3 = values.c;
		values v4 = values.d;
		values v5 = values.e;
		values v6 = values.f;
		values v7 = values.aA;
		values v8 = null;
	}

	@Test
	public void testGson()
	{
		Gson gson = new GsonBuilder()
			.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
			.create();

		String json = gson.toJson(new Ser());

		System.out.println(json);
		Ser ser = gson.fromJson(json, Ser.class);

		Assert.assertEquals(values.a, ser.v1);
		Assert.assertEquals(values.b, ser.v2);
		Assert.assertEquals(values.c, ser.v3);
		Assert.assertEquals(values.d, ser.v4);
		Assert.assertEquals(values.e, ser.v5);
		Assert.assertEquals(values.f, ser.v6);
		Assert.assertEquals(values.aA, ser.v7);
		Assert.assertNull(ser.v8);

		json = "{\"v1\":\"a\",\"v2\":\"b\",\"v3\":\"c\",\"v4\":\"d\",\"v5\":\"e\",\"v6\":\"f\",\"v7\":\"aA\"}";
		ser = gson.fromJson(json, Ser.class);

		Assert.assertEquals(values.a, ser.v1);
		Assert.assertEquals(values.b, ser.v2);
		Assert.assertEquals(values.c, ser.v3);
		Assert.assertEquals(values.d, ser.v4);
		Assert.assertEquals(values.e, ser.v5);
		Assert.assertEquals(values.f, ser.v6);
		Assert.assertEquals(values.aA, ser.v7);
		Assert.assertNull(ser.v8);
	}
}
