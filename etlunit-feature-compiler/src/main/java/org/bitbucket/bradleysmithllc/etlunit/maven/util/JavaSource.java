package org.bitbucket.bradleysmithllc.etlunit.maven.util;

/*
 * #%L
 * etlunit-feature-compiler Maven Mojo
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.sun.codemodel.*;

import java.util.ArrayList;
import java.util.List;

public class JavaSource {
	private final String className;
	private final String id;

	private final List<JavaProperty> properties = new ArrayList<JavaProperty>();

	public JavaSource(String className) {
		this(className, null);
	}

	public JavaSource(String className, String id) {
		this.className = className;
		this.id = id;
	}

	public void addProperty(JavaProperty property)
	{
		properties.add(property);
	}

	public JDefinedClass toSource(JClassContainer cm) throws JClassAlreadyExistsException
	{
		JDefinedClass dc = cm._class(className);

		if (id != null)
		{
			JFieldVar idp = dc.field(JMod.FINAL | JMod.PUBLIC | JMod.STATIC, String.class, "identitifer");
			idp.init(JExpr.lit(id));
		}

		for (JavaProperty property : properties)
		{
			property.append(dc);
		}

		return dc;
	}
}
