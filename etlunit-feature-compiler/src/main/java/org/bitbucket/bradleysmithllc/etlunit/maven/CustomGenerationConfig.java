package org.bitbucket.bradleysmithllc.etlunit.maven;

/*
 * #%L
 * etlunit-feature-compiler Maven Mojo
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.jsonschema2pojo.AnnotationStyle;
import org.jsonschema2pojo.DefaultGenerationConfig;
import org.jsonschema2pojo.SourceType;

public class CustomGenerationConfig extends DefaultGenerationConfig {
	@Override
	public char[] getPropertyWordDelimiters() {
		return new char[]{'-', '_'};
	}

	@Override
	public boolean isUseLongIntegers() {
		return true;
	}

	@Override
	public boolean isIncludeHashcodeAndEquals() {
		return true;
	}

	@Override
	public boolean isIncludeToString() {
		return true;
	}

	@Override
	public AnnotationStyle getAnnotationStyle() {
		return AnnotationStyle.JACKSON2;
	}

	@Override
	public boolean isIncludeJsr303Annotations() {
		return true;
	}

	@Override
	public SourceType getSourceType() {
		return SourceType.JSONSCHEMA;
	}
}
