package org.apache.test;

/*
 * #%L
 * etlunit-application-assembly
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.junit.Assert;
import org.junit.Test;

public class VersionsTest {
	@Test
	public void test() {
		int res = new DefaultArtifactVersion("1.2.1.1").compareTo(new DefaultArtifactVersion("1.1.8.7.6.7.8.9"));
		Assert.assertTrue(res > 0);
	}
}
