package org.bitbucket.bradleysmithllc.etlunit.launcher;

/*
 * #%L
 * etlunit-application-assembly
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.graph.DependencyFilter;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.DependencyResult;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transport.file.FileTransporterFactory;
import org.eclipse.aether.transport.http.HttpTransporterFactory;
import org.eclipse.aether.util.artifact.JavaScopes;
import org.eclipse.aether.util.filter.DependencyFilterUtils;

import java.io.File;
import java.net.URL;
import java.util.*;

public class AetherTransitiveDependencies {

	private static final DefaultServiceLocator locator = MavenRepositorySystemUtils.newServiceLocator();
	private static final RepositorySystem system = newRepositorySystem(locator);
	private static final RepositorySystemSession session = newSession(system);
	private static final RemoteRepository central = new RemoteRepository.Builder("central", "default", "https://repo1.maven.org/maven2/").build();

	public static void resolve(Artifact artifact, List<Artifact> artifacts) throws Exception {
		CollectRequest collectRequest = new CollectRequest(new Dependency(artifact, JavaScopes.COMPILE), Collections.singletonList(central));
		DependencyFilter filter = DependencyFilterUtils.classpathFilter(JavaScopes.COMPILE);
		DependencyRequest request = new DependencyRequest(collectRequest, filter);
		DependencyResult result = system.resolveDependencies(session, request);

		for (ArtifactResult artifactResult : result.getArtifactResults()) {
			artifacts.add(artifactResult.getArtifact());
		}
	}

	private static RepositorySystem newRepositorySystem(DefaultServiceLocator locator) {
		locator.addService(RepositoryConnectorFactory.class, BasicRepositoryConnectorFactory.class);
		locator.addService(TransporterFactory.class, FileTransporterFactory.class);
		locator.addService(TransporterFactory.class, HttpTransporterFactory.class);
		return locator.getService(RepositorySystem.class);
	}

	private static RepositorySystemSession newSession(RepositorySystem system) {
		DefaultRepositorySystemSession session = MavenRepositorySystemUtils.newSession();
		LocalRepository localRepo = new LocalRepository(new File(new File(System.getProperty("user.home")), ".m2/repository"));
		session.setLocalRepositoryManager(system.newLocalRepositoryManager(session, localRepo));
		return session;
	}

	public static void main(String... argv) throws Exception {
		resolve(new DefaultArtifact("org.bitbucket.bradleysmithllc.etlunit:etlunit-jline-cli:4.5.4-eu"), new ArrayList<>());
	}
}
