package org.bitbucket.bradleysmithllc.etlunit.feature._assert;

/*
 * #%L
 * etlunit-feature-archetype
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.ExecutionContext;
import org.bitbucket.bradleysmithllc.etlunit.TestAssertionFailure;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.TestWarning;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.*;
import org.bitbucket.bradleysmithllc.etlunit.integration_test.BaseIntegrationTest;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestValueObject;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class AssertFeatureModuleTest extends BaseIntegrationTest
{
	int opCount = 0;
	int opIdCount = 0;

	@Override
	protected List<Feature> getTestFeatures()
	{
		return Arrays.asList(new Feature[]
				{
						new TestFeature()
				});
	}

	@Test
	public void test()
	{
		startTest();
		Assert.assertEquals(4, opCount);
		Assert.assertEquals(1, opIdCount);
	}

	class TestFeature extends AbstractFeature
	{
		@Override
		public long getPriorityLevel()
		{
			return Long.MAX_VALUE;
		}

		@Override
		public ClassListener getListener()
		{
			return new TestListener();
		}

		@Override
		public FeatureMetaInfo getMetaInfo()
		{
			return new FeatureMetaInfo()
			{
				@Override
				public String getFeatureName()
				{
					return "testiiiii";
				}

				@Override
				public String getFeatureConfiguration()
				{
					return null;
				}

				@Override
				public JsonSchema getFeatureConfigurationValidator()
				{
					return null;
				}

				@Override
				public JsonNode getFeatureConfigurationValidatorNode()
				{
					return null;
				}

				@Override
				public Map<String, FeatureOperation> getExportedOperations()
				{
					return null;
				}

				@Override
				public Map<String, FeatureAnnotation> getExportedAnnotations()
				{
					return null;
				}

				@Override
				public boolean isInternalFeature()
				{
					return false;
				}

				@Override
				public String getFeatureUsage()
				{
					return null;
				}

				@Override
				public List<RuntimeOptionDescriptor> getOptions()
				{
					return null;
				}

				@Override
				public String getDescribingClassName()
				{
					return null;
				}

				@Override
				public Feature getDescribing()
				{
					return null;
				}
			};
		}
	}

	@Override
	protected void prepareTest()
	{
		opCount = 0;
		opIdCount = 0;
	}

	@Override
	protected void assertVariableContextPost(ETLTestMethod mt, VariableContext context, int operationsProcessed)
	{
		if (mt.getName().equals("dataSet"))
		{
			Assert.assertEquals(4, opCount);
		}
		else if (mt.getName().equals("dataSetId"))
		{
			Assert.assertEquals(1, opIdCount);
		}
	}

	private class TestListener extends NullClassListener
	{
		@Override
		public action_code process(@Nullable ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext, int executor) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			if (obj.query("fail") != null)
			{
				throw new TestExecutionError("", null, null);
			}

			ETLTestValueObject pobj = obj.query("target-file-path");
			File path = new File(pobj.getValueAsString());

			boolean refresh = false;
			pobj = obj.query("refresh");

			if (pobj != null)
			{
				refresh = pobj.getValueAsBoolean();

				if (refresh)
				{
					try
					{
						FileUtils.write(path, op.getQualifiedName());
					}
					catch (IOException e)
					{
						Assert.fail();
					}

					AssertFeatureModule.signalRefreshAssertionData(op, context);
				}
			}

			if (!refresh && !path.exists())
			{
				AssertFeatureModule.requireDataSet(context);
			}

			if (mt.getName().equals("error"))
			{
				markTestMilestone(mt.getName());
				return action_code.defer;
			}

			if (mt.getName().equals("testSetPropertiesWin"))
			{
				Assert.assertEquals("test", obj.query("a").getValueAsString());
				markTestMilestone(mt.getName());
			}
			else if (mt.getName().equals("dataSetPropertiesWin"))
			{
				Assert.assertEquals("dataset", obj.query("a").getValueAsString());
				markTestMilestone(mt.getName());
			}
			else if (mt.getName().equals("dataSetId"))
			{
				Assert.assertNotNull(obj.query("target-file-path"));
				opIdCount++;
				markTestMilestone(mt.getName());
			}
			else if (mt.getName().equals("dataSet"))
			{
				opCount++;
				int ordinal = op.getOrdinal();
				String expectedOrdinal = String.valueOf(ordinal);
				Assert.assertEquals(expectedOrdinal, obj.query("ordinal").getValueAsString());

				try
				{
					Assert.assertEquals(expectedOrdinal, FileUtils.readFileToString(path));
				}
				catch (IOException e)
				{
					Assert.fail(e.toString());
				}
			}

			return action_code.handled;
		}
	}

	@Override
	protected List<String> requireMilestones()
	{
		return Arrays.asList(
				"dataSetId",
				"dataSetPropertiesWin",
				"testSetPropertiesWin",
				"error"
		);
	}

	@Override
	protected boolean multiPassSafe()
	{
		return false;
	}
}
