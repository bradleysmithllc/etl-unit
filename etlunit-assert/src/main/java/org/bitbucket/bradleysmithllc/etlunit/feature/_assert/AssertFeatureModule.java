package org.bitbucket.bradleysmithllc.etlunit.feature._assert;

/*
 * #%L
 * etlunit-feature-archetype
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.context.VariableContext;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature._assert.json._assert._assert.AssertHandler;
import org.bitbucket.bradleysmithllc.etlunit.feature._assert.json._assert._assert.AssertRequest;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.io.file.DataFileManager;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.DataSet;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSet;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetContainer;
import org.bitbucket.bradleysmithllc.etlunit.io.file.dataset.ReaderDataSetUtils;
import org.bitbucket.bradleysmithllc.etlunit.listener.ClassListener;
import org.bitbucket.bradleysmithllc.etlunit.listener.NullClassListener;
import org.bitbucket.bradleysmithllc.etlunit.parser.*;
import org.bitbucket.bradleysmithllc.etlunit.util.ObjectUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@FeatureModule
public class AssertFeatureModule extends AbstractFeature
{
	private static final List<String> prerequisites = Arrays.asList("logging");
	private Listener listener = new Listener();
	private Log applicationLog;

	private RuntimeSupport runtimeSupport;
	private DataFileManager dataFileManager;

	@Inject
	public void setApplicationLog(@Named("applicationLog") Log log)
	{
		applicationLog = log;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport manager)
	{
		runtimeSupport = manager;
	}

	@Inject
	public void receiveDataFileManager(DataFileManager manager)
	{
		dataFileManager = manager;
	}

	class Listener extends NullClassListener implements AssertHandler
	{
		public action_code _assert(AssertRequest operation, ETLTestMethod mt, ETLTestOperation op, ETLTestValueObject obj, VariableContext context, ExecutionContext econtext) throws TestAssertionFailure, TestExecutionError, TestWarning
		{
			String dsName = operation.getDataSetName();
			String dsID = operation.getDataSetId();

			// cache refresh requests
			Map<ETLTestValueObject, File> refreshRequests = new HashMap<ETLTestValueObject, File>();

			// open the data set by name
			File dataSetRoot = new FileBuilder(runtimeSupport.getCurrentTestSourceDirectory()).subdir("dataset").mkdirs().file();

			File ds = new File(dataSetRoot, dsName + ".dataset");

			if (ds.exists())
			{
				// open and spin through
				try
				{
					FileReader fileReader = new FileReader(ds);

					try
					{
						ReaderDataSetContainer rdsc = new ReaderDataSetContainer(dataFileManager, fileReader);

						Boolean bIgnoreDataSetProperties = operation.getIgnoreDataSetProperties();
						boolean ignoreDataSetProperties = bIgnoreDataSetProperties == null ? false : bIgnoreDataSetProperties.booleanValue();

						// if an id is specified, only use that one
						if (dsID != null)
						{
							try
							{
								DataSet dds = rdsc.locate(dsID);

								broadcast(dds, op, obj, context, econtext, ignoreDataSetProperties, dsID, dsName, refreshRequests);
							}
							catch (IllegalArgumentException exc)
							{
								throw new TestExecutionError("Missing data set", "ERR_MISSING_DATA_SET_ID", exc);
							}
						}
						else
						{
							// spin through all of them
							while (rdsc.hasNext())
							{
								DataSet dds = rdsc.next();

								broadcast(dds, op, obj, context, econtext, ignoreDataSetProperties, null, dsName, refreshRequests);
							}
						}
					}
					finally
					{
						fileReader.close();
					}
				}
				catch (IOException exc)
				{
					throw new TestExecutionError("Error processing data set", "ERR_IO_ERR", exc);
				}
			}
			else
			{
				// just broadcast and see if the handler wants to create the file
				try
				{
					broadcast(null, op, obj, context, econtext, true, dsID, dsName, refreshRequests);
				}
				catch (IOException e)
				{
					throw new TestExecutionError("Error processing data set", "ERR_IO_ERR", e);
				}
			}

			// check for need to rebuild data set
			if (refreshRequests.size() != 0)
			{
				try
				{
					refreshDataSet(ds, refreshRequests);
				}
				catch(IOException exc)
				{
					throw new TestExecutionError("Error processing data set", "ERR_IO_ERR", exc);
				}
			}

			return action_code.handled;
		}

		private void broadcast(
				DataSet dds,
				ETLTestOperation op,
				ETLTestValueObject obj,
				VariableContext context,
				ExecutionContext econtext,
				boolean ignoreDataSetProperties,
				String dataSetId,
				String dsName1,
				Map<ETLTestValueObject, File> refreshRequests) throws TestAssertionFailure, TestExecutionError, TestWarning, IOException
		{
			String actualDSName = runtimeSupport.processReference(dsName1);

			if (context.hasVariableBeenDeclared("target-data-set-name"))
			{
				context.setStringValue("target-data-set-name", actualDSName);
			}
			else
			{
				context.declareAndSetStringValue("target-data-set-name", actualDSName);
			}

			try
			{
				// create new value
				ETLTestValueObject thisObj = null;

				if (ignoreDataSetProperties)
				{
					// just send the raw properties along - but remove ignore-data-set-properties
					thisObj = obj;
				}
				else
				{
					// merge the data set properties
					thisObj = obj.merge(
							ETLTestParser.loadObject(dds.getProperties().toString()),
							ETLTestValueObject.merge_type.right_merge,
							ETLTestValueObject.merge_policy.recursive
					);
				}

				ETLTestValueObjectBuilder thisObjBuil = new ETLTestValueObjectBuilder(thisObj);

				// remove id property mandatory for the data set
				String dsId = null;

				if (thisObjBuil.hasKey("id"))
				{
					dsId = thisObj.query("id").getValueAsString();
					thisObjBuil.removeKey("id");
				}

				if (thisObjBuil.hasKey("ignore-data-set-properties"))
				{
					thisObjBuil.removeKey("ignore-data-set-properties");
				}

				if (thisObjBuil.hasKey("data-set-id"))
				{
					thisObjBuil.removeKey("data-set-id");
				}

				// remove mandatory data set name
				thisObjBuil.removeKey("data-set-name");

				// map the extracted data into a temporary file and pass the path on to the next operation
				String dsFullName = actualDSName + (dsId == null ? "" : ("_" + dsId));
				File dataSet = runtimeSupport.createAnonymousTempFileWithPrefix(dsFullName);

				thisObjBuil.key("target-file-path").value(new ETLTestValueObjectImpl(dataSet.getAbsolutePath()));

				// check for recursion
				ETLTestValueObject parameters = thisObjBuil.toObject();
				if (parameters.getValueAsMap().size() == 1)
				{
					throw new TestExecutionError("Recursive assert call", "ERR_RECURSIVE_ASSERT");
				}

				if (dds != null)
				{
					ReaderDataSetUtils.extract(dds, dataSet);
				}

				ETLTestOperation sibOp = op.createSibling("assert", parameters);

				try
				{
					econtext.process(sibOp, context);
					// check for signal to refresh data
					if (context.hasVariableBeenDeclared(sibOp.getQualifiedName() + ".refresh-assertion-data"))
					{
						// remove target-file-path
						ETLTestValueObjectBuilder etlTestValueObjectBuilder = new ETLTestValueObjectBuilder(thisObj).removeKey("target-file-path");

						if (dataSetId != null || dsId != null)
						{
							etlTestValueObjectBuilder.key("id").value(ObjectUtils.firstNotNull(dataSetId, dsId));
						}

						refreshRequests.put(etlTestValueObjectBuilder.toObject(), dataSet);
					}
				}
				catch (TestExecutionError tee)
				{
					// trap missing data set and unwrap it
					String errorId = tee.getErrorId();
					if (errorId != null && errorId.equals("ERR_MISSING_DATA_SET"))
					{
						throw tee;
					}

					// rethrow so it is clear where the error occurred
					throw new TestExecutionError("Error in data set assert operation {" + dsFullName + "}", "ERR_ASSERT_OPERATION", tee);
				}
			}
			catch (ParseException e)
			{
				throw new TestExecutionError("Error parsing properties", "ERR_PARSE_ERROR", e);
			}
		}
	}

	/**
					* There is a lot of ambiguity here, but this policy will determine behavior
					* 1 - If source file exists and an ID is present, replace the data set in place
					* 2 - If source file exists and there is no ID, overwrite the file
					* 3 - If source file does not exist, write to the file
					* */
	private void refreshDataSet(File ds, Map<ETLTestValueObject, File> refreshRequests) throws IOException
	{
		applicationLog.info("Refreshing assertion data");

		for (Map.Entry<ETLTestValueObject, File> refreshRequest : refreshRequests.entrySet())
		{
			final ETLTestValueObject jsNode = refreshRequest.getKey();
			ETLTestValueObject idNode = jsNode.query("id");
			final String id;
			final File source = refreshRequest.getValue();

			if (idNode != null && idNode.getValueType() == ETLTestValueObject.value_type.quoted_string)
			{
				id = idNode.getValueAsString();
			}
			else
			{
				id = null;
			}

			if (id == null || !ds.exists())
			{
				// just drop on top of the file
				FileUtils.write(ds, ReaderDataSetUtils.toFormattedJsonString(jsNode.getJsonNode()));
				FileUtils.write(ds, ReaderDataSet.token, true);

				FileReader freader = new FileReader(source);

				try
				{
					BufferedReader bread = new BufferedReader(freader);
					FileWriter fwriter = new FileWriter(ds, true);

					try
					{
						BufferedWriter output = new BufferedWriter(fwriter);

						try
						{
							IOUtils.copy(bread, output);
							output.write(ReaderDataSet.token);
						}
						finally
						{
							output.flush();
						}
					}
					finally
					{
						fwriter.close();
					}
				}
				finally
				{
					freader.close();
				}
			}
			else
			{
				FileReader freader = new FileReader(ds);

				try
				{
					BufferedReader bread = new BufferedReader(freader);
					File temp = runtimeSupport.createAnonymousTempFile();
					FileWriter fwriter = new FileWriter(temp);

					try
					{
						BufferedWriter output = new BufferedWriter(fwriter);

						try
						{
							ReaderDataSetUtils.copy(new ReaderDataSetContainer(dataFileManager, bread), output, new ReaderDataSetUtils.CopyVisitor()
							{
								@Override
								public JsonNode getProperties(DataSet sourceDataSet, int dsIndex)
								{
									// compare id to my id
									JsonNode props = sourceDataSet.getProperties();
									JsonNode pid = props.get("id");

									if (pid != null && pid.getNodeType() == JsonNodeType.STRING)
									{
										String tid = pid.asText();

										if (tid.equals(id))
										{
											// swap properties for mine
											return jsNode.getJsonNode();
										}
									}

									return null;
								}

								@Override
								public Reader read(DataSet set, int dsIndex) throws IOException
								{
									// compare id to my id
									JsonNode props = set.getProperties();
									JsonNode pid = props.get("id");

									if (pid != null && pid.getNodeType() == JsonNodeType.STRING)
									{
										String tid = pid.asText();

										if (tid.equals(id))
										{
											// swap properties for mine
											return new FileReader(source);
										}
									}

									return null;
								}

								@Override
								public void close(Reader reader, DataSet set, int dsIndex) throws IOException
								{
									reader.close();
								}

								@Override
								public boolean includeDataSet(DataSet dataSet, int dsIndex)
								{
									return true;
								}
							});
						}
						finally
						{
							output.flush();
						}
					}
					finally
					{
						fwriter.close();
					}
					// delete source and rename temp to source
					FileUtils.forceDelete(ds);
					temp.renameTo(ds);
				}
				finally
				{
					freader.close();
				}
			}
		}
	}

	@Override
	public List<String> getPrerequisites()
	{
		return prerequisites;
	}

	@Override
	public ClassListener getListener()
	{
		return listener;
	}

	public static void signalRefreshAssertionData(ETLTestOperation op, VariableContext context)
	{
		context.declareAndSetStringValue(op.getQualifiedName() + ".refresh-assertion-data", "true");
	}

	public static void requireDataSet(VariableContext context) throws TestExecutionError
	{
		throw new TestExecutionError("Data set is missing {" + context.getValue("target-data-set-name").getValueAsString(), "ERR_MISSING_DATA_SET");
	}
}
