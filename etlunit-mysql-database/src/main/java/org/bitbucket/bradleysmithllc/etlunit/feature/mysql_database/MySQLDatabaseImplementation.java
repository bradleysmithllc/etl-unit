package org.bitbucket.bradleysmithllc.etlunit.feature.mysql_database;

/*
 * #%L
 * etlunit-mysql-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.mysql.jdbc.Driver;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.BaseDatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.NullResultSetClient;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class MySQLDatabaseImplementation extends BaseDatabaseImplementation {
	public String getImplementationId() {
		return "mysql";
	}

	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException {
		switch (op) {
			case createDatabase: {
				InitializeRequest initializeRequest = request.getInitializeRequest();
				try {
					executeScript(
							IOUtils.readURLToString(MySQLDatabaseImplementation.class.getResource("/mysql_killDatabase.vm")),
							initializeRequest.getConnection(),
							initializeRequest.getMode(),
							database_role.sysadmin
					);
				} catch (Exception testExecutionError) {
					throw new RuntimeException("Error initializing database", testExecutionError);
				}

				try {
					executeScript(
							IOUtils.readURLToString(MySQLDatabaseImplementation.class.getResource("/mysql_killUser.vm")),
							initializeRequest.getConnection(),
							initializeRequest.getMode(),
							database_role.sysadmin
					);
				} catch (Exception testExecutionError) {
					// this may not be an error - ignore.
				}

				try {
					executeScript(
							IOUtils.readURLToString(MySQLDatabaseImplementation.class.getResource("/mysql_createDatabase.vm")),
							initializeRequest.getConnection(),
							initializeRequest.getMode(),
							database_role.sysadmin
					);
				} catch (Exception testExecutionError) {
					throw new RuntimeException("Error initializing database", testExecutionError);
				}
				break;
			}
			case preCreateSchema:
				break;
			case postCreateSchema:
				break;
			case completeDatabaseInitialization:
				break;
			case materializeViews: {
				InitializeRequest initializeRequest = request.getInitializeRequest();
				try {
					URL url = getClass().getResource("/mysql_materializeView.vm");
					final String materialize = IOUtils.readURLToString(url);

					final Map<String, String> context = new HashMap<String, String>();
					// grab the renaming strategy for views
					final DatabaseConnection dc = initializeRequest.getConnection();

					String renStrat = dc.getDatabaseProperties().get("view-rename-strategy");
					if (renStrat == null || renStrat.trim().equals(""))
					{
						renStrat = "_V";
					}

					context.put("view-rename-strategy", renStrat);

					jdbcClient.useResultSetScriptResource(
							dc,
							initializeRequest.getMode(),
							new NullResultSetClient() {
								@Override
								public void next(Connection conn, ResultSet st, DatabaseConnection connection, String mode, database_role id) throws Exception {
									// issue a rename for the view
									String viewName = st.getString(1);
									applicationLog.info("Found View [" + viewName + "]");
									context.put("view-name", viewName);

									jdbcClient.useResultSetScript(connection, mode, null, VelocityUtil.writeTemplate(materialize, context), id);
								}
							},
							"mysql_selectViews.vm"
					);
				} catch (Exception testExecutionError) {
					throw new RuntimeException("Error initializing database", testExecutionError);
				}
				break;
			}
			case dropConstraints:
				break;
			case preCleanTables:
				break;
			case postCleanTables:
				break;
		}

		return null;
	}

	@Override
	public String getJdbcUrl(DatabaseConnection dc, String mode, database_role id) {
		if (id == database_role.database) {
			return "jdbc:mysql://" + dc.getServerName() + "/" + getDatabaseName(dc, mode);
		} else {
			return "jdbc:mysql://" + dc.getServerName() + "/";
		}
	}

	/**
	 * MySQL only handles 64-character database names.
	 *
	 * @param dc
	 * @param mode
	 * @return
	 */
	@Override
	public String getDatabaseName(DatabaseConnection dc, String mode) {
		return "__" + runtimeSupport.digestIdentifier(dc.getId() + "_" + (mode == null ? "_def_" : mode) + "_" + runtimeSupport.getProjectUID());
	}

	/**
	 * Defer to the login name for the password
	 *
	 * @param dc
	 * @param mode
	 * @return
	 */
	@Override
	protected String getPasswordImpl(DatabaseConnection dc, String mode) {
		return getLoginNameImpl(dc, mode);
	}

	/**
	 * Mysql limits users to 16 characters.  Defer to the project UID
	 *
	 * @param dc
	 * @param mode
	 * @return
	 */
	@Override
	protected String getLoginNameImpl(DatabaseConnection dc, String mode) {
		return IOUtils.hashToLength(getDatabaseName(dc, mode), 16);
	}

	@Override
	public Class getJdbcDriverClass() {
		return Driver.class;
	}

	public String getDefaultSchema(DatabaseConnection databaseConnection, String mode) {
		return getDatabaseName(databaseConnection, mode);
	}

	@Override
	public String escapeChar() {
		return "`";
	}
}
