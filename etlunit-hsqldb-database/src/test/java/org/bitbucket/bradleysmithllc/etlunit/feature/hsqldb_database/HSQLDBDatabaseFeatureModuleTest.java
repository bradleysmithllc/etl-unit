package org.bitbucket.bradleysmithllc.etlunit.feature.hsqldb_database;

/*
 * #%L
 * etlunit-hsqldb-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;

import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.JSonBuilderProxy;
import org.junit.Test;
import org.junit.Assert;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import java.io.IOException;

public class HSQLDBDatabaseFeatureModuleTest extends BaseFeatureModuleTest {
	public List<Feature> getTestFeatures()
	{
		return Arrays.asList((Feature) new HSQLDBDatabaseFeatureModule());
	}

	@Override
	protected JSonBuilderProxy getConfigurationOverrides() {
		return new JSonBuilderProxy()
			.object()
				.key("features")
					.object()
						.key("database")
							.object()
								.key("default-connection-id")
								.value("db")
								.key("database-definitions")
									.object()
										.key("db")
											.object()
												.key("implementation-id")
												.value("hsqldb")
												.key("user-name")
												.value("user")
												.key("password")
												.value("pass")
												.key("schema-scripts")
												.value(Arrays.asList("schema.ddl"))
											.endObject()
									.endObject()
							.endObject()
					.endObject()
			.endObject();
	}

	public boolean echoLog()
	{
		return false;
	}

	public void setUpSourceFiles() throws IOException
	{
		createSource("database1.etlunit", "@Database(id: 'db') class test1 {" +
			"@Test a_sqlserver_oracle(){" +
				"stage(source: 'TEST_GM', target-table: 'TEST');\n" +
				//"stage(source: 'TEST_2_GM', target-table: 'TEST');\n" +
				"stage(source: 'TEST_3_GM', target-table: 'TEST');\n" +
				"extract(source-table: 'TEST', target: 'TEST_EXTRACT');\n" +
				"assert(source-table: 'TEST', target: 'TEST_EXTRACT');\n" +
			"}}");

		URL url = getClass().getResource("/schema.ddl");

		createResource("hsqldb", "db", "schema.ddl", IOUtils.readURLToString(url));

		createSource(null, "data", "TEST_GM.delimited",
			"/*-- ID\tNAME\tLINE_COUNT\tOPTIONAL --*/\n" +
			"/*-- INTEGER\tVARCHAR\tINTEGER\tINTEGER --*/\n" +
			"1\tTEXTI\t5\t17\n" +
			"2\tTEXTII\t8\t15"
		);
		/* This does not currently work, it needs to get fixed in the core and database modules
		createSource(null, "data", "TEST_2_GM.delimited",
				"3\tITEXT\t6\t1\n" +
				"4\tIITEXT\t9\t1"
		);
		*/
		createSource(null, "data", "TEST_3_GM.delimited",
			"/*-- ID\tNAME\tLINE_COUNT\tOPTIONAL --*/\n" +
				"/*-- INTEGER\tVARCHAR\tINTEGER\tINTEGER --*/\n" +
				"5\tITEXTI\t7\tnull\n" +
				"6\tIITEXTII\t10\tnull"
		);
	}

	@Test
	public void test() throws IOException
	{
		TestResults result = startTest();

		Assert.assertEquals(1, result.getNumTestsSelected());
		Assert.assertEquals(1, result.getMetrics().getNumberOfTestsPassed());

		File f = getSource(null, "data", "TEST_EXTRACT.delimited");

		Assert.assertTrue(f.exists());

		String actualText = IOUtils.readFileToString(f);

		URL url = getClass().getResource("/TEST_EXTRACT.delimited");

		Assert.assertNotNull(url);

		String expectedText = IOUtils.readURLToString(url);

		Assert.assertEquals(expectedText, actualText);
	}
}
