package org.bitbucket.bradleysmithllc.etlunit.feature.hsqldb_database;

/*
 * #%L
 * etlunit-hsqldb-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.inject.Injector;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseFeatureModule;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

@FeatureModule
public class HSQLDBDatabaseFeatureModule extends AbstractFeature {
	private static final List<String> prerequisites = Arrays.asList("logging", "database");
	private DatabaseFeatureModule databaseFeatureModule;
	private HSQLDBDatabaseImplementation hsqldbDatabaseImplementation;

	@Inject
	public void setDatabaseFeatureModule(DatabaseFeatureModule module)
	{
		databaseFeatureModule = module;
	}

	@Override
	public List<String> getPrerequisites() {
		return prerequisites;
	}

	@Override
	public void dispose()
	{
		hsqldbDatabaseImplementation.dispose();
	}

	@Override
	public void initialize(Injector inj)
	{
		hsqldbDatabaseImplementation = postCreate(new HSQLDBDatabaseImplementation());
		databaseFeatureModule.addDatabaseImplementation(hsqldbDatabaseImplementation);
	}
}
