package org.bitbucket.bradleysmithllc.etlunit.feature.hsqldb_database;

/*
 * #%L
 * etlunit-hsqldb-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.BaseDatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.db.Schema;
import org.hsqldb.jdbc.JDBCDriver;

public class HSQLDBDatabaseImplementation extends BaseDatabaseImplementation
{
	@Override
	protected boolean useInformationSchemaConstraints() {
		return false;
	}

	@Override
	public boolean isUserSchema(DatabaseConnection dc, String schemaName) {
		return super.isUserSchema(dc, schemaName) && !schemaName.equalsIgnoreCase("system_lobs");
	}

	public database_state getDatabaseState(DatabaseConnection databaseConnection, String s)
	{
		return database_state.fail;
	}

	public String getImplementationId()
	{
		return "hsqldb";
	}

	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException
	{
		return null;
	}

	@Override
	public String getJdbcUrl(DatabaseConnection dc, String mode, database_role id)
	{
		return "jdbc:hsqldb:mem:/" + getDatabaseName(dc, mode) + ";shutdown=true";
	}

	@Override
	public Class getJdbcDriverClass()
	{
		return JDBCDriver.class;
	}

	public String getDefaultSchema(DatabaseConnection databaseConnection, String mode)
	{
		return "PUBLIC";
	}

	@Override
	protected String createScriptToMaterializeViewToTable(String sourceSchema, String sourceView, String targetSchema, String targetName) {
		return "CREATE TABLE " + targetSchema + "." + targetName + " AS (SELECT * FROM " + sourceSchema + "." + sourceView + " WHERE 1 = 0) WITH DATA;";
	}
}
