package org.bitbucket.bradleysmithllc.etlunit.maven.test;

/*
 * #%L
 * etlunit-maven
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.StatusCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class Dummy
{
	@Test
	public void testForCi() throws IOException, GitAPIException
	{
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		File gitDir = new File("/Users/bsmith/git/etl-unit/.git");
		System.out.println(gitDir.exists());
		Repository repository = builder.findGitDir() // scan up the file system tree
		  .build();

		String head = repository.getFullBranch();
		if (head.startsWith("refs/heads/")) {
		        // Print branch name with "refs/heads/" stripped.
		        System.out.println("Current branch is " + repository.getBranch());
		}

		ObjectId id = repository.resolve(repository.getFullBranch());
		System.out.println("Branch " + repository.getBranch() + " points to " + id.name());

		Git git = new Git(repository);

		StatusCommand status = git.status();

		Status stc = status.call();

		System.out.println(stc.isClean());
	}
}
