package org.bitbucket.bradleysmithllc.etlunit.maven.test;

/*
 * #%L
 * etlunit-maven
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.maven.ETLUnitMojo;
import org.junit.Assert;
import org.junit.Test;

public class TestUserProfileProperty
{
	@Test
	public void setProfileOne()
	{
		ETLUnitMojo.setUserProfile("profile");
		String[] userProfiles = ETLUnitMojo.getUserProfiles();
		Assert.assertNotNull(userProfiles);
		Assert.assertEquals(1, userProfiles.length);
		Assert.assertEquals("profile", userProfiles[0]);
	}

	@Test
	public void setProfilesOne()
	{
		ETLUnitMojo.setUserProfiles(new String[]{"profile"});
		String[] userProfiles = ETLUnitMojo.getUserProfiles();
		Assert.assertNotNull(userProfiles);
		Assert.assertEquals(1, userProfiles.length);
		Assert.assertEquals("profile", userProfiles[0]);
	}

	@Test
	public void setProfileMany()
	{
		ETLUnitMojo.setUserProfile("profile:hi");
		String[] userProfiles = ETLUnitMojo.getUserProfiles();
		Assert.assertNotNull(userProfiles);
		Assert.assertEquals(2, userProfiles.length);
		Assert.assertEquals("profile", userProfiles[0]);
		Assert.assertEquals("hi", userProfiles[1]);
	}

	@Test
	public void setProfilesMany()
	{
		ETLUnitMojo.setUserProfiles(new String[]{
			"profile",
			"hi"
		});
		String[] userProfiles = ETLUnitMojo.getUserProfiles();
		Assert.assertNotNull(userProfiles);
		Assert.assertEquals(2, userProfiles.length);
		Assert.assertEquals("profile", userProfiles[0]);
		Assert.assertEquals("hi", userProfiles[1]);
	}

	@Test
	public void setProfileNull()
	{
		ETLUnitMojo.setUserProfile(null);
		String[] userProfiles = ETLUnitMojo.getUserProfiles();
		Assert.assertNotNull(userProfiles);
		Assert.assertEquals(0, userProfiles.length);
	}

	@Test
	public void setProfilesNull()
	{
		ETLUnitMojo.setUserProfiles(null);
		String[] userProfiles = ETLUnitMojo.getUserProfiles();
		Assert.assertNotNull(userProfiles);
		Assert.assertEquals(0, userProfiles.length);
	}
}
