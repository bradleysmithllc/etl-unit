package org.bitbucket.bradleysmithllc.etlunit.maven;

/*
 * #%L
 * etlunit-maven
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.NullStatusReporterImpl;
import org.bitbucket.bradleysmithllc.etlunit.StatusReporter;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.util.HashMapArrayList;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.MapList;
import org.bitbucket.bradleysmithllc.etlunit.util.EtlUnitStringUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

public class SurefireStatusReporter extends NullStatusReporterImpl {
		private MapList<ETLTestClass, TestResult> currentTests = new HashMapArrayList<ETLTestClass, TestResult>();

		private int numFailures;
		private int numErrors;
		private final File targetDirectory;

		SurefireStatusReporter(File target)
		{
			targetDirectory = target;
		}

	private void publishReport() {
		for (Map.Entry<ETLTestClass, List<TestResult>> classEntry : currentTests.entrySet())
		{
			String qname = classEntry.getKey().getQualifiedName();

			StringBuffer buffer = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
			buffer.append("<testsuite failures=\"" + numFailures + "\" time=\"" + (0d / 1000.0d)  + "\" errors=\"" + numErrors + "\" skipped=\"0\" tests=\"" + currentTests.size() + "\" name=\"" + qname + "\">\n");

			buffer.append("<properties />\n");

			for (TestResult result : classEntry.getValue())
			{
				buffer.append("<testcase time=\"" + (result.elapsedTime / 1000.0d) + "\" classname=\"" + qname + "\" name=\"" + result.testName + "\">\n");

				if (result.testResult == TestResult.result.error)
				{
					buffer.append("\t<error type=\"" + EtlUnitStringUtils.sanitize(result.failureMessage, '_') + "\"><![CDATA[" + EtlUnitStringUtils.sanitize(result.failureMessage, '_') + "]]></error>\n");
				}
				else if (result.testResult == TestResult.result.fail)
				{
					buffer.append("\t<failure message=\"" + EtlUnitStringUtils.sanitize(result.failureMessage, '_') + "\" type=\"" + EtlUnitStringUtils.sanitize(result.failureMessage, '_') + "\"><![CDATA[" + result.failureMessage + "]]></failure>\n");
				}

				buffer.append("</testcase>\n");
			}

			buffer.append("</testsuite>");

			try {
				IOUtils.writeBufferToFile(new File(targetDirectory, "TEST-" + qname + ".xml"), buffer);
			} catch (IOException e) {
				throw new RuntimeException("");
			}
		}
	}

	@Override
	public synchronized void testCompleted(ETLTestMethod method, StatusReporter.CompletionStatus status) {
		TestResult result = new TestResult();

		result.testName = method.getName();

		result.testResult = TestResult.result.pass;

		switch(status.getTestResult())
		{
			case success:
				break;
			case error:
				result.testResult = TestResult.result.error;

				StringWriter stringWriter = new StringWriter();
				PrintWriter writer = new PrintWriter(stringWriter);

				for (TestExecutionError err : status.getErrors())
				{
					err.printStackTrace(writer);
				}

				result.failureMessage = status.getErrors().toString() + "\n" + stringWriter.toString();

				numErrors++;
				break;
			case failure:
				result.testResult = TestResult.result.fail;
				result.failureMessage = status.getAssertionFailures().toString();
				numFailures++;
				break;
		}

		currentTests.getOrCreate(method.getTestClass()).add(result);
	}

	@Override
	public void testsCompleted() {
		publishReport();
	}
}

final class TestResult {
	enum result {
		error,
		fail,
		pass
	}

	result testResult;
	String testName;
	long elapsedTime;
	String failureMessage;
}
