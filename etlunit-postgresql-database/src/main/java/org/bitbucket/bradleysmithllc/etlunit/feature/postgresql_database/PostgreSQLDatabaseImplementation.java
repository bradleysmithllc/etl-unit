package org.bitbucket.bradleysmithllc.etlunit.feature.postgresql_database;

/*
 * #%L
 * etlunit-postgresql-database
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.BaseDatabaseImplementation;
import org.bitbucket.bradleysmithllc.etlunit.feature.database.DatabaseConnection;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.ResourceUtils;
import org.postgresql.Driver;

import java.io.IOException;

public class PostgreSQLDatabaseImplementation extends BaseDatabaseImplementation {
	public String getImplementationId() {
		return "postgresql";
	}

	public Object processOperationSub(operation op, OperationRequest request) throws UnsupportedOperationException {
		switch (op) {
			case createDatabase:
				InitializeRequest initializeRequest = request.getInitializeRequest();
				try {
					executeScript(
							IOUtils.readURLToString(PostgreSQLDatabaseImplementation.class.getResource("/postgresql_killDatabase.vm")),
							initializeRequest.getConnection(),
							initializeRequest.getMode(),
							database_role.sysadmin
					);
				} catch (Exception testExecutionError) {
					throw new RuntimeException("Error killing database", testExecutionError);
				}

				try {
					executeScript(
							IOUtils.readURLToString(PostgreSQLDatabaseImplementation.class.getResource("/postgresql_killUser.vm")),
							initializeRequest.getConnection(),
							initializeRequest.getMode(),
							database_role.sysadmin
					);
				} catch (Exception testExecutionError) {
					// this may not be an error - ignore.
				}

				try {
					executeScript(
							IOUtils.readURLToString(PostgreSQLDatabaseImplementation.class.getResource("/postgresql_createDatabase.vm")),
							initializeRequest.getConnection(),
							initializeRequest.getMode(),
							database_role.sysadmin
					);
				} catch (Exception testExecutionError) {
					throw new RuntimeException("Error initializing database", testExecutionError);
				}
				break;
			case preCreateSchema:
				break;
			case postCreateSchema:
				break;
			case completeDatabaseInitialization:
				break;
			case materializeViews:
				break;
			case dropConstraints:
				break;
			case preCleanTables:
				break;
			case resetIdentities:
				break;
			case postCleanTables:
				break;
		}

		return null;
	}

	@Override
	protected boolean useInformationSchemaConstraints() {
		return false;
	}

	@Override
	public String getJdbcUrl(DatabaseConnection dc, String mode, database_role id) {
		switch (id) {
			case sysadmin:
				int sp = dc.getServerPort();
				String jdbcUrl = "jdbc:postgresql://" + dc.getServerName() + (sp != -1 ? (":" + sp) : "") + "/postgres";
				applicationLog.info("Using postgres url " + jdbcUrl);
				return jdbcUrl;
			default:
			case ping:
			case database:
				sp = dc.getServerPort();
				jdbcUrl = "jdbc:postgresql://" + dc.getServerName() + (sp != -1 ? (":" + sp + "/") : "/") + getDatabaseName(dc, mode);
				applicationLog.info("Using postgres url " + jdbcUrl);
				return jdbcUrl;
		}
	}

	@Override
	public Class getJdbcDriverClass() {
		return Driver.class;
	}

	public String getDefaultSchema(DatabaseConnection databaseConnection, String mode) {
		return "PUBLIC";
	}

	@Override
	public boolean isUserSchema(DatabaseConnection dc, String schemaName) {
		if (super.isUserSchema(dc, schemaName)) {
			return !schemaName.equalsIgnoreCase("pg_catalog");
		}

		return false;
	}

	@Override
	protected String createScriptToRenameTableTo(String sourceSchema, String sourceView, String targetSchema, String targetName) {
		if (!sourceSchema.equalsIgnoreCase(targetSchema)) {
			throw new IllegalArgumentException("Postgres does not support renaming a table across schemas");
		}

		return new StringBuilder("ALTER TABLE ").append(sourceSchema).append(".").append(sourceView).append(" RENAME TO ").append(targetName).toString();
	}

	@Override
	protected String createScriptToRenameViewTo(String sourceSchema, String sourceView, String targetSchema, String targetName) {
		if (!sourceSchema.equalsIgnoreCase(targetSchema)) {
			throw new IllegalArgumentException("Postgres does not support renaming a view across schemas");
		}

		return new StringBuilder("ALTER VIEW ").append(sourceSchema).append(".").append(sourceView).append(" RENAME TO ").append(targetName).toString();
	}

	@Override
	protected String getPasswordImpl(DatabaseConnection dc, String mode) {
		return getDatabaseName(dc, mode);
	}

	@Override
	public String getDatabaseName(DatabaseConnection dc, String mode) {
		return IOUtils.hashToLength(super.getDatabaseName(dc, mode), 63);
	}

	@Override
	protected String getLoginNameImpl(DatabaseConnection dc, String mode) {
		return getDatabaseName(dc, mode);
	}
}
