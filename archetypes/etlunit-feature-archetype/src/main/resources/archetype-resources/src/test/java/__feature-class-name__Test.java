#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

/*
 * #%L
 * etlunit-feature-archetype
 * %%
 * Copyright (C) 2010 - 2023 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.TestResults;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.Feature;
import org.bitbucket.bradleysmithllc.etlunit.feature.AbstractFeature;
import org.bitbucket.bradleysmithllc.etlunit.test_support.BaseFeatureModuleTest;

import org.junit.Test;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;

import java.io.IOException;

public class ${feature-class-name}Test extends BaseFeatureModuleTest {
	public List<Feature> getTestFeatures()
	{
		return Arrays.asList((Feature) new ${feature-class-name}());
	}

	public boolean echoLog()
	{
		return false;
	}

	public void setUpSourceFiles() throws IOException
	{
		createSource("test.etlunit", "class test { @Test test_me() {test(option: \"NiceValue\");}}");
	}

	@Test
	public void test()
	{
		TestResults result = startTest();

		Assert.assertEquals(1, result.getNumTestsSelected());
		Assert.assertEquals(1, result.getMetrics().getNumberOfTestsPassed());
		Assert.assertEquals(0, result.getMetrics().getNumberOfAssertionFailures());
		Assert.assertEquals(0, result.getMetrics().getNumberOfWarnings());
		Assert.assertEquals(0, result.getMetrics().getNumberOfErrors());
	}
}
